/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables, IMSizeModeLayoutJson } from 'jimu-core';
import { IMSidebarConfig, SidebarType, CollapseSides } from './config';
interface LayoutItemProps {
    itemStyle?: any;
    innerLayouts: IMSizeModeLayoutJson;
    style?: any;
    className?: string;
}
export interface SidebarProps {
    widgetId: string;
    direction: SidebarType;
    theme: IMThemeVariables;
    config: IMSidebarConfig;
    firstLayouts: IMSizeModeLayoutJson;
    secondLayouts: IMSizeModeLayoutJson;
    sidebarVisible?: boolean;
}
interface State {
    deltaSize: number;
    isResizing: boolean;
}
export declare abstract class BaseSidebarLayout extends React.PureComponent<SidebarProps, State> {
    ref: HTMLElement;
    splitRef: HTMLElement;
    domSize: number;
    interactable: Interact.Interactable;
    layoutItemComponent: React.ComponentClass<LayoutItemProps & {
        collapsed?: boolean;
    }>;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    abstract bindSplitHandler: () => void;
    removeSplitHandler: () => void;
    toggleSidebar: (e: any) => void;
    calSidebarSize(): any;
    createCollapsibleSide(layouts: IMSizeModeLayoutJson, side: CollapseSides): JSX.Element;
    splitStyle(isDesignMode: boolean): import("jimu-core").SerializedStyles;
    createController(): JSX.Element;
    createNormalSide(layouts: IMSizeModeLayoutJson, side: CollapseSides): JSX.Element;
    render(): JSX.Element;
}
export {};
