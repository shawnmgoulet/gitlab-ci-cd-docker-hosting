import { SidebarControllerPositions, ICON_TYPE } from './config';
export declare const PREDEFINED_TOGGLE_STYLE: {
    left: {
        default: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
        rect: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                        opacity: number;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
    };
    right: {
        default: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
        rect: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                        opacity: number;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
    };
    top: {
        default: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
        rect: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                        opacity: number;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
    };
    bottom: {
        default: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
        rect: {
            icon: ICON_TYPE;
            offsetX: number;
            offsetY: number;
            position: SidebarControllerPositions;
            iconSize: number;
            width: number;
            height: number;
            color: {
                normal: {
                    icon: {
                        useTheme: boolean;
                        color: string;
                    };
                    bg: {
                        useTheme: boolean;
                        color: string;
                        opacity: number;
                    };
                };
                hover: {
                    bg: {
                        useTheme: boolean;
                        color: string;
                    };
                };
            };
            expandStyle: {
                style: {
                    borderRadius: string;
                };
            };
            collapseStyle: {
                style: {
                    borderRadius: string;
                };
            };
        };
    };
};
