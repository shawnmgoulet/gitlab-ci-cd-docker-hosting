/// <reference types="react" />
import { ImmutableObject } from 'jimu-core';
import { BorderStyle } from 'jimu-ui';
export interface SidebarControllerStyle {
    style: React.CSSProperties;
}
export interface ColorDef {
    useTheme?: boolean;
    color?: string;
    opacity?: number;
}
export interface ToggleBtnColor {
    normal: {
        icon: ColorDef;
        bg: ColorDef;
    };
    hover?: {
        icon?: ColorDef;
        bg?: ColorDef;
    };
}
export interface SidebarConfig {
    direction: SidebarType;
    collapseSide: CollapseSides;
    overlay: boolean;
    size: string;
    resizable: boolean;
    toggleBtn: {
        visible: boolean;
        icon: ICON_TYPE;
        template?: string;
        offsetX?: number;
        offsetY?: number;
        position?: SidebarControllerPositions;
        color: ToggleBtnColor;
        iconSize: number;
        width: number;
        height: number;
        expandStyle?: SidebarControllerStyle;
        collapseStyle?: SidebarControllerStyle;
    };
    defaultState: number;
    divider: {
        visible: boolean;
        lineStyle?: BorderStyle;
    };
    firstPanelStyle?: any;
    secondPanelStyle?: any;
}
export declare type IMSidebarConfig = ImmutableObject<SidebarConfig>;
export declare enum CollapseSides {
    First = "FIRST",
    Second = "SECOND"
}
export declare enum SidebarControllerPositions {
    Start = "START",
    Center = "CENTER",
    End = "END"
}
export declare enum SidebarType {
    Horizontal = "HORIZONTAL",
    Vertical = "VERTICAL"
}
export declare enum ICON_TYPE {
    Left = "LEFT",
    Right = "RIGHT",
    Up = "UP",
    Down = "DOWN"
}
