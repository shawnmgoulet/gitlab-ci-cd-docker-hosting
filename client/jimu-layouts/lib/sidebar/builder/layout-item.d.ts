/// <reference types="react" />
import { React, IMSizeModeLayoutJson } from 'jimu-core';
interface Props {
    collapsed?: boolean;
    itemStyle?: any;
    innerLayouts: IMSizeModeLayoutJson;
    style?: any;
    className?: string;
}
export default class SidebarLayoutItem extends React.PureComponent<Props> {
    render(): JSX.Element;
}
export {};
