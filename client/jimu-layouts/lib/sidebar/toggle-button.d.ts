/// <reference types="react" />
/** @jsx jsx */
import { React, SerializedStyles, IMThemeVariables } from 'jimu-core';
import { ToggleBtnColor, ICON_TYPE } from './config';
export interface SidebarControllerProps {
    expanded: boolean;
    icon: ICON_TYPE;
    iconSize: number;
    color: ToggleBtnColor;
    width: number;
    height: number;
    style: any;
    theme: IMThemeVariables;
}
export declare class SidebarController extends React.PureComponent<SidebarControllerProps> {
    getStyle(): SerializedStyles;
    render(): JSX.Element;
}
