/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables } from 'jimu-core';
import { SettingChangeFunction } from 'jimu-for-builder';
import { LinearUnit } from 'jimu-ui';
import { IMSidebarConfig, SidebarType, CollapseSides } from './config';
interface Props {
    widgetId: string;
    config: IMSidebarConfig;
    onSettingChange: SettingChangeFunction;
    formatMessage: (id: string) => string;
}
export default class Setting extends React.PureComponent<Props> {
    selectedToggleBtnStyle: string;
    constructor(props: any);
    dockHorizontalLeft: () => void;
    dockHorizontalRight: () => void;
    dockVerticalTop: () => void;
    dockVerticalBottom: () => void;
    updateDockside(direction: SidebarType, side: CollapseSides): void;
    updateToggleBtnStyles(style: any): import("seamless-immutable").ImmutableObject<import("./config").SidebarConfig>;
    updateDefaultState: (e: any) => void;
    updateOverlay: (e: any) => void;
    updateResizable: (e: any) => void;
    updateSize: (value: LinearUnit) => void;
    updateToggleBtn(prop: string, value: any): void;
    updateControllerPos: (e: any) => void;
    updateControllerOffsetX: (e: any) => void;
    updateControllerOffsetY: (e: any) => void;
    updateControllerWidth: (e: any) => void;
    updateControllerHeight: (e: any) => void;
    updateControllerIconSize: (e: any) => void;
    updateFirstPanelBg: (bg: any) => void;
    updateSecondPanelBg: (bg: any) => void;
    updateDividerStyle: (borderStyle: any) => void;
    updateDividerVisible: (e: any) => void;
    updateToggleBtnVisible: (e: any) => void;
    getStyle(builderTheme: IMThemeVariables): import("jimu-core").SerializedStyles;
    useDefaultToggleStyle: () => void;
    useRectToggleStyle: () => void;
    getDocksideStyle(builderTheme: IMThemeVariables): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
