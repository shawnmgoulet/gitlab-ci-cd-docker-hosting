/// <reference types="react" />
import { React, ReactRedux, IMWidgetJson } from 'jimu-core';
import { LayoutItemProps, StateToLayoutItemProps, LayoutItemDisplaySetting } from './types';
import { IconTexProps } from './icon-text';
interface OwnProps {
    onMouseEnter?: (e: any) => void;
    onMouseLeave?: (e: any) => void;
    onClick?: (e: any) => void;
    displaySetting?: LayoutItemDisplaySetting;
    active?: boolean;
}
interface StateToProps {
    widgetJson: IMWidgetJson;
}
declare class WidgetRenderer extends React.PureComponent<LayoutItemProps & StateToLayoutItemProps & OwnProps & StateToProps> {
    static defaultProps: Partial<OwnProps>;
    _onClick: (e: any) => void;
    genarateIconTextProps: (isDesignMode: boolean) => IconTexProps;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof WidgetRenderer, Pick<OwnProps, "onClick" | "active" | "onMouseEnter" | "onMouseLeave" | "displaySetting"> & LayoutItemProps & OwnProps>;
export default _default;
