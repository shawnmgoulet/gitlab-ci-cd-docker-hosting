/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, LayoutItemConstructorProps, IMSizeModeLayoutJson } from 'jimu-core';
import { PageContextProps, LayoutContextProps } from 'jimu-layouts/common';
interface DropAreaProps {
    layouts: IMSizeModeLayoutJson;
    isPlaceholder?: boolean;
    highlightDragover?: boolean;
    className?: string;
    style?: any;
    isRepeat?: boolean;
    onToggleDragoverEffect?: (isDragover: boolean, draggingItem?: LayoutItemConstructorProps) => void;
    onDragEnter?: (draggingItem: LayoutItemConstructorProps) => void;
    onDragOver?: (draggingItem: LayoutItemConstructorProps, draggingElement: HTMLElement, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
    onDragLeave?: (draggingItem: LayoutItemConstructorProps) => void;
    onDrop?: (draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
    innerRef?: (ref: HTMLDivElement) => void;
}
interface StateToDropAreaProps {
    layoutId: string;
}
declare class DropArea extends React.PureComponent<DropAreaProps & StateToDropAreaProps> {
    ref: HTMLElement;
    interactable: Interact.Interactable;
    h5DragOverHandler: any;
    h5DraggingItem: LayoutItemConstructorProps;
    isDragover: boolean;
    pageContext: PageContextProps;
    layoutContext: LayoutContextProps;
    static defaultProps: Partial<DropAreaProps>;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    private isDraggingItemAccepted;
    private initInteractive;
    private initH5Dragging;
    private isH5DragElementAccepted;
    private onDragEnter;
    private onDragLeave;
    private onH5DragOver;
    private onDraggingOver;
    private updateDraggingOverEffect;
    private onDrop;
    setRef: (ref: HTMLDivElement) => void;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof DropArea, Pick<DropAreaProps, "className" | "style" | "layouts" | "innerRef" | "onDragEnter" | "onDragLeave" | "onDragOver" | "onDrop" | "isPlaceholder" | "isRepeat" | "highlightDragover" | "onToggleDragoverEffect"> & DropAreaProps>;
export default _default;
