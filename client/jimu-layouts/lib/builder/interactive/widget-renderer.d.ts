/// <reference types="react" />
/// <reference types="@emotion/core" />
import { ToolItemConfig, LayoutItemProps } from '../../types';
declare const _default: import("react").ComponentClass<import("./rnd-decorator").RndOptions & LayoutItemProps & {
    widgetId?: string;
}, any>;
export default _default;
export declare function widgetRndWithToolItems(menuItems: Array<ToolItemConfig | Array<ToolItemConfig>>): React.ComponentClass<LayoutItemProps>;
