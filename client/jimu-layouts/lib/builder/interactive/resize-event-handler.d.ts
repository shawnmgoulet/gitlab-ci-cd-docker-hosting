export interface ResizeHandlerProps {
    layoutItemId: string;
    onResizeStart?: (id: string, initWidth: number, initHeight: number) => void;
    onResizing?: (id: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd?: (id: string, x: number, y: number, dw: number, dh: number) => void;
}
export declare const GLOBAL_RESIZING_CLASS_NAME = "interactjs-resizing";
export declare function bindResizeHandler(interactable: Interact.Interactable, resizeHandlerProps: ResizeHandlerProps): Interact.Interactable;
