/// <reference types="react" />
/// <reference types="@emotion/core" />
import { LayoutItemProps, ToolbarConfig } from '../../types';
declare const _default: import("react").ComponentClass<import("./rnd-decorator").RndOptions & LayoutItemProps & {
    widgetId?: string;
}, any>;
export default _default;
export declare function sectionRndWithToolItems(menuItems: ToolbarConfig): React.ComponentClass<LayoutItemProps>;
