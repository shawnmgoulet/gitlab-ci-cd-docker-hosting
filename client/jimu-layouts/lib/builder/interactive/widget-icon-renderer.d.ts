/// <reference types="react" />
/// <reference types="@emotion/core" />
declare const _default: import("react").ComponentClass<import("./rnd-decorator").RndOptions & import("../../types").LayoutItemProps & {
    widgetId?: string;
}, any>;
export default _default;
