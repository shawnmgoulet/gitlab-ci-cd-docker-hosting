/// <reference types="react" />
import { React, IMLayoutItemJson } from 'jimu-core';
import { LayoutItemProps, ToolbarConfig } from 'jimu-layouts/common';
declare type Props = LayoutItemProps & {
    layoutItem: IMLayoutItemJson;
    top?: boolean;
    left?: boolean;
    right?: boolean;
    bottom?: boolean;
    disableResizing?: boolean;
    isDragging?: boolean;
    isResizing?: boolean;
};
export declare function withSelect(toolItems: ToolbarConfig): (WrappedComponent: React.ComponentClass<LayoutItemProps, any>) => React.ComponentClass<Props, any>;
export {};
