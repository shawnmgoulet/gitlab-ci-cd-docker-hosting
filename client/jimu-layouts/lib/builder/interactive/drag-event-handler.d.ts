import { LayoutItemType } from 'jimu-core';
export declare const GLOBAL_DRAGGING_CLASS_NAME = "interactjs-dragging";
export declare const GLOBAL_H5_DRAGGING_CLASS_NAME = "exb-h5-dragging";
export interface DragHandlerProps {
    rootLayoutId: string;
    layoutId: string;
    layoutItemId: string;
    itemType: LayoutItemType;
    useDragHandler: boolean;
    dragHandlerClassName?: string;
    restrict: () => boolean;
    onDragStart?: (id: string) => void;
    onDragging?: (id: string, dx: number, dy: number, outOfBoundary: boolean) => void;
    onDragEnd?: (id: string, dx: number, dy: number, outOfBoundary: boolean) => void;
}
export declare function bindDragHandler(interactable: Interact.Interactable, dragHandlerProps: DragHandlerProps): Interact.Interactable;
