/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables, IMLayoutItemJson } from 'jimu-core';
import { ToolbarConfig } from 'jimu-layouts/common';
interface Props {
    layoutId: string;
    layoutItem: IMLayoutItemJson;
    builderTheme: IMThemeVariables;
    positionType: 'left' | 'center' | 'right';
    size: number;
    iconSize: number;
    className: string;
    menuItems: ToolbarConfig;
}
export declare class ContextMenu extends React.PureComponent<Props> {
    ref: HTMLDivElement;
    getPositionStyle(): import("jimu-core").SerializedStyles;
    getStyle(): import("jimu-core").SerializedStyles;
    formatMessage(msg: string): string;
    render(): JSX.Element;
}
export {};
