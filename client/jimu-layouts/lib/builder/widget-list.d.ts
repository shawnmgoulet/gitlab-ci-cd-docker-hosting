/// <reference types="react" />
/** @jsx jsx */
import { React, LayoutItemConstructorProps, IMThemeVariables } from 'jimu-core';
interface State {
    widgetInfoList: LayoutItemConstructorProps[];
}
interface Props {
    itemsPerRow?: number;
    closeOnClickOutside: boolean;
    builderTheme: IMThemeVariables;
    referenceElement?: HTMLElement;
    onItemSelect: (item: LayoutItemConstructorProps) => void;
    onClose: () => void;
}
export declare class WidgetList extends React.PureComponent<Props, State> {
    contentRef: HTMLElement;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    private handleOutsideClick;
    private loadWidgetInfos;
    createWidgetCard(widgetInfo: LayoutItemConstructorProps): JSX.Element;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
