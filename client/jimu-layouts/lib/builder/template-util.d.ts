import { IMAppConfig } from 'jimu-core';
import { LayoutTemplate } from 'jimu-layouts/common';
interface WidgetIdMapping {
    [key: string]: string;
}
export declare function createWidgetFromTemplate(appConfig: IMAppConfig, templateJson: LayoutTemplate, widgetId: string, widgetMapping: WidgetIdMapping): Promise<{
    appConfig: IMAppConfig;
    newWidgetId: string;
}>;
export declare function createSectionFromTemplate(appConfig: IMAppConfig, templateJson: LayoutTemplate, sectionId: string, widgetMapping: WidgetIdMapping): Promise<{
    appConfig: IMAppConfig;
    newSectionId: string;
}>;
export declare function createLayoutFromTemplate(appConfig: IMAppConfig, templateJson: LayoutTemplate, layoutId: string, widgetMapping: WidgetIdMapping): Promise<{
    appConfig: IMAppConfig;
    newLayoutId: string;
}>;
export declare function createViewFromTemplate(appConfig: IMAppConfig, templateJson: LayoutTemplate, viewId: string, widgetMapping: WidgetIdMapping): Promise<{
    appConfig: IMAppConfig;
    newViewId: string;
}>;
export declare function createPageFromTemplate(appConfig: IMAppConfig, templateJson: LayoutTemplate, pageId: string, widgetMapping: WidgetIdMapping): Promise<{
    appConfig: IMAppConfig;
    newPageId: string;
}>;
export declare function applyPageTemplateToHeader(appConfig: IMAppConfig, templatePageJson: LayoutTemplate): Promise<{
    appConfig: IMAppConfig;
}>;
export declare function applyPageTemplateToFooter(appConfig: IMAppConfig, templatePageJson: LayoutTemplate): Promise<{
    appConfig: IMAppConfig;
}>;
export declare function applyPageTemplateToBody(appConfig: IMAppConfig, pageId: string, templatePageJson: LayoutTemplate): Promise<{
    appConfig: IMAppConfig;
}>;
export {};
