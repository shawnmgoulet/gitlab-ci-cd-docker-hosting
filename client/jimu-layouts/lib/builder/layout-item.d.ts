/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux } from 'jimu-core';
import { PageContextProps, LayoutItemProps, WidgetProps, SectionProps, StateToLayoutItemProps, ToolbarConfig } from 'jimu-layouts/common';
export interface OwnProps {
    toolItems?: ToolbarConfig;
    contextMenuItems?: ToolbarConfig;
}
export interface DragEventHandlers {
    restrict?: boolean;
    useDragHandler?: boolean;
    onDragStart?: (layoutItemId: string) => void;
    onDragging?: (layoutItemId: string, dx: number, dy: number, outOfBoundary: boolean) => void;
    onDragEnd?: (layoutItemId: string, dx: number, dy: number, outOfBoundary: boolean) => void;
}
export interface ResizeEventHandlers {
    top?: boolean;
    left?: boolean;
    right?: boolean;
    bottom?: boolean;
    resizable?: boolean;
    keepAspectRatio?: boolean;
    aspectRatio?: number;
    onResizeStart?: (layoutItemId: string, initWidth: number, initHeight: number) => void;
    onResizing?: (layoutItemId: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd?: (layoutItemId: string, x: number, y: number, dw: number, dh: number) => void;
}
declare type LayoutItemInBuilderProps = LayoutItemProps & DragEventHandlers & ResizeEventHandlers & OwnProps;
declare type StateToItemProps = StateToLayoutItemProps & WidgetProps & SectionProps;
declare class LayoutItemInBuilder extends React.PureComponent<LayoutItemInBuilderProps & StateToItemProps> {
    ref: HTMLElement;
    interactable: Interact.Interactable;
    pageContext: PageContextProps;
    componentDidMount(): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    /**
     * Enable inline editing mode for the widget
     *
     * @memberof LayoutItemInBuilder
     */
    doubleClick: (e: any) => void;
    onClick: (e: any) => void;
    createContextMenu(): JSX.Element;
    switchSetting: () => void;
    getStyle(): import("jimu-core").SerializedStyles;
    canDrag(): boolean;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof LayoutItemInBuilder, Pick<LayoutItemProps & DragEventHandlers & ResizeEventHandlers & OwnProps & StateToLayoutItemProps & WidgetProps & SectionProps, "className" | "style" | "bottom" | "left" | "right" | "top" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "onDragEnd" | "onDragStart" | "selectable" | "showDefaultTools" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio" | "restrict" | "useDragHandler" | "onDragging" | "keepAspectRatio" | "onResizeStart" | "onResizing" | "onResizeEnd" | "toolItems" | "contextMenuItems"> & LayoutItemProps>;
export default _default;
