import { LayoutContextToolProps } from 'jimu-core';
export declare function getAppMode(): any;
export declare function isLockLayout(): any;
export declare const deleteMenuItem: {
    icon: any;
    title: (props: {
        formatMessage: (id: string) => string;
    }) => string;
    visible: (props: LayoutContextToolProps) => string | boolean;
    onClick: (props: LayoutContextToolProps) => void;
};
export declare const pendingMenuItem: {
    icon: any;
    title: (props: {
        formatMessage: (id: string) => string;
    }) => string;
    visible: () => boolean;
    onClick: (props: LayoutContextToolProps) => void;
};
export declare const settingMenuItem: {
    icon: any;
    title: (props: {
        formatMessage: (id: string) => string;
    }) => string;
    onClick: (props: LayoutContextToolProps) => void;
};
