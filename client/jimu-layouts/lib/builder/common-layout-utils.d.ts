/// <reference types="seamless-immutable" />
import { Immutable, IMLayoutJson, LayoutInfo, IMAppLayouts, IMAppConfig, IMLayoutItemJson } from 'jimu-core';
import { LayoutNode } from 'jimu-layouts/common';
/**
 * Remove the item id from its parent's content
 * @param layoutInfo
 * @param deleteFromLayout
 * @param layouts
 */
export declare function removeItemFromParent(layoutInfo: LayoutInfo, deleteFromLayout: boolean, layouts: IMAppLayouts): IMAppLayouts;
export declare function selectLayoutItem(layoutInfo: LayoutInfo, layouts: IMAppLayouts): Immutable.ImmutableObject<import("jimu-core").AppLayouts>;
export declare function createEmptyLayoutItem(layoutId: string, layouts: IMAppLayouts): {
    layoutInfo: LayoutInfo;
    layouts: IMAppLayouts;
};
export declare function findLayoutItem(appConfig: IMAppConfig, layoutInfo: LayoutInfo): IMLayoutItemJson;
export declare function getMaximumLayoutItemId(layoutMap: IMLayoutJson): number;
export declare function findLowestCommonAncestor(node1: LayoutNode, node2: LayoutNode): {
    ancestor: LayoutNode;
    itemIdForNode1: string;
    itemIdForNode2: string;
};
export declare function findNodeInLayoutTree(rootNode: LayoutNode, layoutId: string): LayoutNode;
export declare function generateLayoutTree(rootLayoutId: string, parentDepth?: number): LayoutNode;
