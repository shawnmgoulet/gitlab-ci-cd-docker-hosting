/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables, IMLayoutItemJson, IntlShape } from 'jimu-core';
import { ToolItemConfig, ToolbarConfig, PageContextProps } from 'jimu-layouts/common';
declare type Props = {
    layoutId: string;
    layoutItem: IMLayoutItemJson;
    top?: boolean;
    left?: boolean;
    right?: boolean;
    bottom?: boolean;
    resizable?: boolean;
    forbidToolbar?: boolean;
    toolItems?: ToolbarConfig;
    showDefaultTools?: boolean;
    selected: boolean;
    isInlineEditing: boolean;
    isFunctionalWidget: boolean;
    hasEmbeddedLayout: boolean;
    isSection: boolean;
};
interface State {
    virtualElement: {
        getBoundingClientRect: () => ClientRect;
        clientWidth: number;
        clientHeight: number;
    };
}
interface IntlProps {
    intl: IntlShape;
}
export declare class SelectWrapper extends React.PureComponent<Props & IntlProps, State> {
    ref: HTMLElement;
    pageContext: PageContextProps;
    cornerHandle: any;
    sideHandle: any;
    constructor(props: any);
    componentDidMount(): void;
    private isNumberEqual;
    private isRectEqual;
    componentDidUpdate(): void;
    createToolbar(): JSX.Element;
    formatMessage: (id: string) => any;
    getExtensionTools(): ToolItemConfig[];
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
    enableResizing(isActive: boolean): {
        top: boolean;
        right: boolean;
        bottom: boolean;
        left: boolean;
        topRight: boolean;
        bottomRight: boolean;
        bottomLeft: boolean;
        topLeft: boolean;
    };
    actionHandlers(enableResizing: any, builderTheme: IMThemeVariables): any[];
}
declare const _default: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default _default;
