/// <reference types="react" />
/** @jsx jsx */
import { React, PageJson, ReactRedux, ImmutableObject, BrowserSizeMode, IMThemeVariables, IMHeaderJson, IMFooterJson, LayoutInfo } from 'jimu-core';
import { PageContextProps } from 'jimu-layouts/common';
import { ContextMenuPosition } from '../types';
interface PageProps {
    pageId: string;
    /**
     * store pages that have been rendered
     *
     * Only one page will be visible, all other pages will be hidden
     *  */
    renderedPages: ImmutableObject<{
        [pageId: string]: boolean;
    }>;
}
interface StateToProps {
    pages: ImmutableObject<{
        [pageId: string]: PageJson;
    }>;
    header: IMHeaderJson;
    headerVisible?: boolean;
    footer: IMFooterJson;
    footerVisible?: boolean;
    mainSizeMode: BrowserSizeMode;
    browserSizeMode: BrowserSizeMode;
    isDesignMode: boolean;
    theme: IMThemeVariables;
    lockLayout: boolean;
}
interface State {
    showSelectTip: boolean;
    editHeader: boolean;
    editFooter: boolean;
    editBody: boolean;
    contextMenuPosition?: ContextMenuPosition;
}
export declare class PageRenderer extends React.PureComponent<PageProps & StateToProps, State> {
    static displayName: string;
    pageRef: HTMLElement;
    headerRef: HTMLElement;
    headerResizerRef: HTMLElement;
    footerRef: HTMLElement;
    footerResizerRef: HTMLElement;
    headerInteractable: Interact.Interactable;
    footerInteractable: Interact.Interactable;
    pageContext: PageContextProps;
    activeLayoutInfo: LayoutInfo;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    bindResizeHandler: () => void;
    removeResizeHandler: () => void;
    clearSelection: (e: any) => void;
    render(): JSX.Element;
    editHeader: () => void;
    editFooter: () => void;
    editBody: () => void;
    renderHeader(): JSX.Element;
    renderFooter(): JSX.Element;
    renderPageBody(renderedPageId: string): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof PageRenderer, Pick<PageProps & StateToProps, "pageId" | "renderedPages"> & PageProps>;
export default _default;
