/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { PageContextProps, LayoutItemProps, WidgetProps } from 'jimu-layouts/common';
declare type Props = LayoutItemProps & WidgetProps & {
    isDesignMode: boolean;
};
export declare class WidgetRendererInBuilder extends React.PureComponent<Props> {
    pageContext: PageContextProps;
    mask(): JSX.Element;
    render(): JSX.Element;
}
export {};
