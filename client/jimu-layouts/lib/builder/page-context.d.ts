/// <reference types="react" />
import { React, IMThemeVariables, BrowserSizeMode } from 'jimu-core';
export declare const PageContext: React.Context<PageContextProps>;
export declare type PageContextProps = {
    isDesignMode: boolean;
    lockLayout?: boolean;
    rootLayoutId?: string;
    isHeader?: boolean;
    isFooter?: boolean;
    pageId?: string;
    builderTheme?: IMThemeVariables;
    theme?: IMThemeVariables;
    mainSizeMode: BrowserSizeMode;
    browserSizeMode: BrowserSizeMode;
    viewOnly?: boolean;
    maxWidth?: number;
};
