/// <reference types="react" />
import { React, IMLayoutItemJson, IMSizeModeLayoutJson, LayoutItemConstructorProps, LayoutContextToolProps, IMLayoutJson, AppConfig, ImmutableArray, LayoutInfo, IMViewJson, LayoutItemJson, BoundingBox } from 'jimu-core';
export interface Position {
    x?: number;
    y?: number;
    w: number;
    h: number;
}
export interface CommonLayoutSetting {
    className?: string;
    style?: any;
    order?: number;
}
export interface CommonLayoutItemSetting {
    lockParent?: boolean;
    style?: any;
}
export interface FixedLayoutSetting extends CommonLayoutSetting {
    gridSize?: number;
}
export interface FlowLayoutSetting extends CommonLayoutSetting {
    gutter?: number;
}
export interface FlowLayoutItemSetting extends CommonLayoutItemSetting {
    heightMode?: 'auto' | 'fit' | 'fixed';
    width?: number;
    offsetX?: number;
    offsetY?: number;
}
export interface FixedLayoutItemSetting extends CommonLayoutItemSetting {
    autoProps: {
        left?: boolean;
        right?: boolean;
        top?: boolean;
        bottom?: boolean;
        width?: boolean;
        height?: boolean;
    };
    hCenter?: boolean;
    vCenter?: boolean;
    fullsize?: boolean;
}
export declare const enum OrderAdjustType {
    BringForward = 0,
    SendBackward = 1,
    BringToFront = 2,
    SendToBack = 3
}
export interface LayoutProps {
    layouts: IMSizeModeLayoutJson;
    className?: string;
    style?: any;
    isInSection?: boolean;
    isInWidget?: boolean;
    isRepeat?: boolean;
    isPageItem?: boolean;
    visible?: boolean;
    itemDraggable?: boolean;
    itemResizable?: boolean;
    itemSelectable?: boolean;
    droppable?: boolean;
    showDefaultTools?: boolean;
    isItemAccepted?: (item: LayoutItemConstructorProps, isPlaceholder: boolean) => boolean;
    onItemClick?: (e: any, widgetId: string) => void;
    ignoreMinHeight?: boolean;
}
export interface LayoutItemProps {
    layoutId: string;
    layoutItemId: string;
    draggable?: boolean;
    resizable?: boolean;
    selectable?: boolean;
    forbidContextMenu?: boolean;
    forbidToolbar?: boolean;
    showDefaultTools?: boolean;
    className?: string;
    style?: any;
    onClick?: (e: any, layoutInfo: LayoutInfo) => void;
    onDoubleClick?: (e: any, layoutInfo: LayoutInfo) => void;
    forceAspectRatio?: boolean;
    aspectRatio?: number;
}
export interface StateToLayoutProps {
    layout: IMLayoutJson;
}
export interface StateToLayoutItemProps {
    layoutItem: IMLayoutItemJson;
    selected?: boolean;
}
export interface WidgetProps {
    widgetId: string;
    isClassLoaded: boolean;
    isInlineEditing: boolean;
    isFunctionalWidget: boolean;
    hasEmbeddedLayout: boolean;
    supportInlineEditing: boolean;
    supportRepeat: boolean;
    widgetStyle: any;
}
export interface SectionProps {
    sectionId: string;
    views: ImmutableArray<string>;
    activeView: IMViewJson;
}
export interface ToolItemConfig {
    icon?: React.ComponentClass<React.SVGAttributes<SVGElement>>;
    title?: string | ((props: LayoutContextToolProps & {
        formatMessage: (id: string) => string;
    }) => string);
    size?: number;
    label?: string | ((props: LayoutContextToolProps & {
        formatMessage: (id: string) => string;
    }) => string);
    disabled?: boolean | ((props: LayoutContextToolProps) => boolean);
    checked?: boolean | ((props: LayoutContextToolProps) => boolean);
    visible?: boolean | ((props: LayoutContextToolProps) => boolean);
    rotate?: number;
    className?: string;
    onClick?: (props: LayoutContextToolProps, evt?: React.MouseEvent<any>) => void;
    settingPanel?: React.ComponentClass;
    widgetId?: string;
}
export declare type ToolbarConfig = (ToolItemConfig | ToolItemConfig[])[];
export declare enum ContextMenuPosition {
    TopLeft = 0,
    TopCenter = 1,
    TopRight = 2,
    RightTop = 3,
    RightCenter = 4,
    RightBottom = 5,
    BottomLeft = 6,
    BottomCenter = 7,
    BottomRight = 8,
    LeftTop = 9,
    LeftCenter = 10,
    LeftBottom = 11
}
export interface DropHandlers {
    onDragEnter?: (draggingItem: LayoutItemConstructorProps) => void;
    onDragOver?: (draggingItem: LayoutItemConstructorProps, draggingElement: HTMLElement, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
    onDragLeave?: (draggingItem: LayoutItemConstructorProps) => void;
    onDrop?: (draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
}
export declare const enum LayoutTemplateType {
    Widget = "WIDGET",
    Section = "SECTION",
    Page = "PAGE",
    Header = "HEADER",
    Footer = "FOOTER"
}
export interface LayoutTemplate {
    type: LayoutTemplateType;
    config: Partial<AppConfig>;
    widgetId?: string;
    sectionId?: string;
    pageId?: string;
}
export declare const COLS_IN_ONE_ROW = 12;
export declare const CONTEXT_MENU_ITEM_SIZE = 28;
export declare const CONTEXT_MENU_ICON_SIZE = 16;
export interface LayoutNode {
    layoutId: string;
    parentNode?: LayoutNode;
    itemIdInParent?: string;
    depth: number;
    children?: LayoutNode[];
}
export interface LayoutItemSettingProps {
    layoutId: string;
    layoutItem: LayoutItemJson;
    style: any;
    isLockLayout: boolean;
    formatMessage: (id: string) => string;
    onSettingChange: (layoutInfo: LayoutInfo, setting: any) => void;
    onStyleChange: (layoutInfo: LayoutInfo, style: any) => void;
    onPosChange: (layoutInfo: LayoutInfo, bbox: BoundingBox) => void;
}
