/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux } from 'jimu-core';
import { LayoutItemProps } from '../../types';
import { StateToLayoutItemProps } from 'jimu-layouts/common';
declare type GridItemProps = LayoutItemProps & StateToLayoutItemProps;
declare class GridItem extends React.PureComponent<GridItemProps> {
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof GridItem, Pick<GridItemProps, "className" | "style" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "selectable" | "showDefaultTools" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio"> & LayoutItemProps>;
export default _default;
