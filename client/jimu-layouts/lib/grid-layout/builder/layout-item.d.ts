/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, IMSizeModeLayoutJson, LayoutItemConstructorProps } from 'jimu-core';
import { LayoutItemProps } from '../../types';
import { StateToLayoutItemProps } from 'jimu-layouts/common';
interface OwnProps {
    onItemDragStart: (id: any) => void;
    onItemDragEnd: (id: any) => void;
}
interface State {
    dragoverSide: 'top' | 'bottom' | 'left' | 'right';
}
declare type GridItemProps = LayoutItemProps & StateToLayoutItemProps & OwnProps;
declare class GridItem extends React.PureComponent<GridItemProps, State> {
    fakeLayouts: IMSizeModeLayoutJson;
    constructor(props: any);
    hasEmbedLayout(): any;
    onItemDragStart: () => void;
    onItemDragEnd: () => void;
    onDragOver: (draggingItem: LayoutItemConstructorProps, draggingElement: HTMLElement, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
    onDrop: (draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect, clientX: number, clientY: number) => void;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof GridItem, Pick<GridItemProps, "className" | "style" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "selectable" | "showDefaultTools" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio" | "onItemDragStart" | "onItemDragEnd"> & LayoutItemProps & OwnProps>;
export default _default;
