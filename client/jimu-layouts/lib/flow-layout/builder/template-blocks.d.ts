/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables } from 'jimu-core';
interface OwnProps {
    onSelectBlock: (index: number) => void;
}
export declare class TemplateBlocks extends React.PureComponent<OwnProps> {
    theme: IMThemeVariables;
    getStyle(): import("jimu-core").SerializedStyles;
    getBlockStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
