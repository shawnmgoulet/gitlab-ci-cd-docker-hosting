/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, IMLayoutItemJson, IMSizeModeLayoutJson, IMThemeVariables, LayoutItemConstructorProps } from 'jimu-core';
import { LayoutItemProps, PageContextProps, ToolbarConfig } from 'jimu-layouts/common';
import { FlowLayoutItemSetting } from '../../types';
import { StateToFlowItemProps } from '../layout-utils';
interface OwnProps {
    layouts: IMSizeModeLayoutJson;
    layoutItem: IMLayoutItemJson;
    builderTheme: IMThemeVariables;
    index: number;
    total: number;
    gutter: number;
    editingSectionId?: string;
    children?: any;
    onDropAtBoundary: (draggingItem: LayoutItemConstructorProps, itemRect: ClientRect, insertIndex: number) => void;
}
interface State {
    isResizing: boolean;
    dh: number;
    showTemplatePopup: boolean;
    referenceElem: HTMLElement;
}
declare class FlowLayoutItem extends React.PureComponent<LayoutItemProps & StateToFlowItemProps & OwnProps, State> {
    domRect: ClientRect;
    state: State;
    fakeTopLayouts: IMSizeModeLayoutJson;
    fakeBottomLayouts: IMSizeModeLayoutJson;
    minHeight: number;
    initHeight: number;
    pageContext: PageContextProps;
    contextMenus: ToolbarConfig;
    reference: HTMLDivElement;
    constructor(props: any);
    onResizeStart: (id: string, initWidth: number, initHeight: number) => void;
    onResizing: (id: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd: (id: string, x: number, y: number, dw: number, dh: number, shiftKey?: boolean) => void;
    dropAtTop: (draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect) => void;
    dropAtBottom: (draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect) => void;
    calHeight(itemSetting: FlowLayoutItemSetting): {
        height: number;
        minHeight?: undefined;
    } | {
        height: string;
        minHeight?: undefined;
    } | {
        minHeight: number;
        height: any;
    };
    toggleTemplatePopup: () => void;
    removeLayoutItem: () => void;
    moveup: () => void;
    movedown: () => void;
    createContextMenu(): JSX.Element;
    switchSetting: () => void;
    onTemplateBlockSelected: (template: any) => void;
    onClick: (e: any) => void;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof FlowLayoutItem, Pick<LayoutItemProps & StateToFlowItemProps & OwnProps, "className" | "children" | "style" | "layouts" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "index" | "builderTheme" | "gutter" | "selectable" | "showDefaultTools" | "editingSectionId" | "layoutItem" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio" | "total" | "onDropAtBoundary"> & LayoutItemProps & OwnProps>;
export default _default;
