/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables } from 'jimu-core';
interface Props {
    itemsPerRow?: number;
    referenceElement: HTMLElement;
    builderTheme: IMThemeVariables;
    onItemSelect: (blockIndex: number) => void;
    onClose: () => void;
}
export declare class BlockList extends React.PureComponent<Props> {
    contentRef: HTMLElement;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    private handleOutsideClick;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
