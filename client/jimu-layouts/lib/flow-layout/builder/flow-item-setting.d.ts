/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { FlowLayoutItemSetting, LayoutItemSettingProps } from 'jimu-layouts/common';
import { LinearUnit } from 'jimu-ui';
export default class FlowItemSetting extends React.PureComponent<LayoutItemSettingProps> {
    itemSetting: FlowLayoutItemSetting;
    updateWidth: (value: LinearUnit) => void;
    updateOffsetX: (e: any) => void;
    updateOffsetY: (e: any) => void;
    updateHeightMode: (e: any) => void;
    render(): JSX.Element;
}
