import { LayoutTemplate } from 'jimu-layouts/common';
export declare function addTemplateRow(layoutId: string, layoutItemId: string, layoutTemplate: LayoutTemplate): Promise<void>;
