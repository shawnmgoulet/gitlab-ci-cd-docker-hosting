/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { LayoutProps, StateToLayoutProps } from 'jimu-layouts/common';
interface OwnProps {
    intl: IntlShape;
}
declare const _default: React.ForwardRefExoticComponent<Pick<Pick<LayoutProps & StateToLayoutProps & OwnProps, "className" | "style" | "intl" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & LayoutProps, "className" | "style" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<Pick<LayoutProps & StateToLayoutProps & OwnProps, "className" | "style" | "intl" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & LayoutProps>;
};
export default _default;
