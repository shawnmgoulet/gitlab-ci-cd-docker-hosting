/// <reference types="react" />
import { React, ReactRedux, IMLayoutItemJson } from 'jimu-core';
import { LayoutItemProps, FlowLayoutItemSetting } from 'jimu-layouts/common';
import { StateToFlowItemProps } from '../layout-utils';
interface OwnProps {
    index: number;
    layoutItem: IMLayoutItemJson;
    gutter: number;
}
declare class FlowLayoutItem extends React.PureComponent<LayoutItemProps & StateToFlowItemProps & OwnProps> {
    calHeight(itemSetting: FlowLayoutItemSetting): {
        height: string;
        minHeight?: undefined;
    } | {
        minHeight: number;
        height: any;
    };
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof FlowLayoutItem, Pick<LayoutItemProps & StateToFlowItemProps & OwnProps, "className" | "style" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "index" | "gutter" | "selectable" | "showDefaultTools" | "layoutItem" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio"> & LayoutItemProps & OwnProps>;
export default _default;
