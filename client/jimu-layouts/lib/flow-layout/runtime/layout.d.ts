/// <reference types="react" />
import { React, ReactRedux } from 'jimu-core';
import { LayoutProps, StateToLayoutProps } from 'jimu-layouts/common';
import { FlowLayoutSetting } from '../../types';
declare class Layout extends React.PureComponent<LayoutProps & StateToLayoutProps> {
    _createItem(itemId: string, index: number, layoutSetting: FlowLayoutSetting): JSX.Element;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof Layout, Pick<LayoutProps & StateToLayoutProps, "className" | "style" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & LayoutProps>;
export default _default;
