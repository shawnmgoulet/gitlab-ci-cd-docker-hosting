import { ReduxStoreExtension } from 'jimu-core/lib/extension-spec/extension-spec';
import { globalLayoutReducer } from './reducers/global-reducers';
import { GlobalLayoutActions } from './actions/global-layout-actions';
export default class GlobalLayoutReduxStoreExtension implements ReduxStoreExtension {
    id: string;
    getActions(): GlobalLayoutActions[];
    getInitLocalState(): {
        default: {
            0: {
                id: string;
                pos: {
                    x: number;
                    y: number;
                    w: number;
                    h: number;
                };
                type: number;
            };
        };
    };
    getReducer(): typeof globalLayoutReducer;
    getStoreKey(): string;
}
