/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { IconTextSize } from './types';
export interface TextProps {
    show: boolean;
    text: string;
}
export interface IconTextType {
    text: TextProps;
    image: ImageProps;
}
export interface IconTexProps {
    active?: boolean;
    selected?: boolean;
    className?: string;
    size: IconTextSize;
    iconText: IconTextType;
    style?: React.CSSProperties;
    onMouseEnter?: (e: any) => void;
    onMouseLeave?: (e: any) => void;
    onClick?: (e: React.MouseEvent) => void;
}
export declare class _IconText extends React.PureComponent<IconTexProps & {
    theme: ThemeVariables;
}> {
    static defaultProps: Partial<IconTexProps>;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const IconText: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<IconTexProps & React.RefAttributes<_IconText>, "ref" | "key"> & Partial<Pick<IconTexProps & React.RefAttributes<_IconText>, "onClick" | "style" | "size" | "active" | "className" | "selected" | "onMouseEnter" | "onMouseLeave" | "iconText">> & Partial<Pick<Partial<IconTexProps>, never>>, "theme">>;
interface ImageProps {
    className?: string;
    src: string;
    shape: 'circle' | 'rectangle';
    color?: string;
    activeColor?: string;
    width?: number;
    height?: number;
    active?: boolean;
}
export {};
