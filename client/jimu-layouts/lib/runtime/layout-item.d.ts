/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux } from 'jimu-core';
import { LayoutItemProps, WidgetProps, SectionProps, StateToLayoutItemProps } from 'jimu-layouts/common';
declare type StateToItemProps = StateToLayoutItemProps & WidgetProps & SectionProps;
declare class LayoutItem extends React.PureComponent<LayoutItemProps & StateToItemProps> {
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof LayoutItem, Pick<LayoutItemProps & StateToLayoutItemProps & WidgetProps & SectionProps, "className" | "style" | "layoutId" | "layoutItemId" | "draggable" | "onClick" | "onDoubleClick" | "selectable" | "showDefaultTools" | "resizable" | "forbidContextMenu" | "forbidToolbar" | "forceAspectRatio" | "aspectRatio"> & LayoutItemProps>;
export default _default;
