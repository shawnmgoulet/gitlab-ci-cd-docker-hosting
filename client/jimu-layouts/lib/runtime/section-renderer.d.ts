/// <reference types="react" />
import { React } from 'jimu-core';
import { LayoutItemProps, SectionProps } from 'jimu-layouts/common';
export declare class SectionRenderer extends React.PureComponent<LayoutItemProps & SectionProps> {
    render(): JSX.Element;
}
