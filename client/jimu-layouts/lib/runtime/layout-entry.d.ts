/// <reference types="react" />
import { React, ReactRedux } from 'jimu-core';
import { LayoutProps, StateToLayoutProps } from 'jimu-layouts/common';
declare class LayoutEntry extends React.PureComponent<LayoutProps & StateToLayoutProps> {
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof LayoutEntry, Pick<LayoutProps & StateToLayoutProps, "className" | "style" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & LayoutProps>;
export default _default;
