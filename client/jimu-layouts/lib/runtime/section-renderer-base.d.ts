/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableObject } from 'jimu-core';
import { LayoutItemProps, SectionProps, LayoutProps } from 'jimu-layouts/common';
interface OwnProps {
    layoutEntryComponent: React.ComponentClass<LayoutProps>;
}
interface State {
    /**
     * store views that have been rendered
     *
     * Only one view will be visible, all other views will be hidden
     *  */
    renderedViews: ImmutableObject<{
        [viewId: string]: boolean;
    }>;
}
export declare class SectionRendererBase extends React.PureComponent<LayoutItemProps & SectionProps & OwnProps, State> {
    constructor(props: any);
    componentDidUpdate(): void;
    renderSectionContent(): JSX.Element;
    renderView(viewId: string): JSX.Element;
    getBackgroundStyle(): {
        backgroundImage: string;
        backgroundColor: string;
        backgroundPosition: string;
        backgroundRepeat: string;
        backgroundSize: string;
    };
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
