/// <reference types="react" />
/** @jsx jsx */
import { React, PageJson, ImmutableObject, ReactRedux, BrowserSizeMode, IMThemeVariables, IMHeaderJson, IMFooterJson } from 'jimu-core';
import { PageContextProps } from 'jimu-layouts/common';
interface PageProps {
    pageId: string;
    /**
     * store pages that have been rendered
     *
     * Only one page will be visible, all other pages will be hidden
     *  */
    renderedPages: ImmutableObject<{
        [pageId: string]: boolean;
    }>;
}
interface StateToProps {
    pages: ImmutableObject<{
        [pageId: string]: PageJson;
    }>;
    header: IMHeaderJson;
    headerVisible?: boolean;
    footer: IMFooterJson;
    footerVisible?: boolean;
    browserSizeMode: BrowserSizeMode;
    mainSizeMode: BrowserSizeMode;
    isDesignMode: boolean;
    theme: IMThemeVariables;
}
declare class PageRenderer extends React.PureComponent<PageProps & StateToProps> {
    static displayName: string;
    pageContext: PageContextProps;
    constructor(props: any);
    render(): JSX.Element;
    renderHeader(): JSX.Element;
    renderFooter(): JSX.Element;
    renderPageBody(renderedPageId: string, theme: IMThemeVariables): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof PageRenderer, Pick<PageProps & StateToProps, "pageId" | "renderedPages"> & PageProps>;
export default _default;
