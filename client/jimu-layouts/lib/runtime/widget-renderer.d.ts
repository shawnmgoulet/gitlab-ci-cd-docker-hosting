/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { LayoutItemProps, WidgetProps } from 'jimu-layouts/common';
export declare class WidgetRenderer extends React.PureComponent<LayoutItemProps & WidgetProps> {
    private loadWidgetClass;
    componentDidMount(): void;
    componentDidUpdate(): void;
    renderWidgetContent(): JSX.Element;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
