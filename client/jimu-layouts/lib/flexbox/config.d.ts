import { ImmutableObject } from 'seamless-immutable';
import { BorderStyle, BoxShadowStyle, FourSidesUnit, BackgroundStyle } from 'jimu-ui';
export interface FlexboxConfig {
    min: number;
    space: number;
    style: {
        padding?: {
            number: Array<number>;
            unit: string;
        };
        background?: BackgroundStyle;
        justifyContent?: string;
        alignItems?: string;
        border?: BorderStyle;
        borderRadius?: FourSidesUnit;
        boxShadow?: BoxShadowStyle;
    };
}
export declare type IMFlexboxConfig = ImmutableObject<FlexboxConfig>;
export declare enum FlexboxType {
    Row = 0,
    Column = 1
}
