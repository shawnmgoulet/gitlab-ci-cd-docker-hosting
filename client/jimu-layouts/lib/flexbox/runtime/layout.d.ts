/// <reference types="react" />
import { React, ReactRedux } from 'jimu-core';
import { IMFlexboxConfig, FlexboxType } from '../config';
import { LayoutProps, StateToLayoutProps } from 'jimu-layouts/common';
interface OwnProps {
    config: IMFlexboxConfig;
    direction: FlexboxType;
    start?: number;
    end?: number;
    activeIds?: string;
}
declare type FlexLayoutProps = LayoutProps & StateToLayoutProps & OwnProps;
declare class Layout extends React.PureComponent<FlexLayoutProps> {
    createItem(itemId: string, index: number, layoutStyle: any): JSX.Element;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof Layout, Pick<FlexLayoutProps, "className" | "style" | "direction" | "end" | "start" | "visible" | "layouts" | "config" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight" | "activeIds"> & LayoutProps & OwnProps>;
export default _default;
