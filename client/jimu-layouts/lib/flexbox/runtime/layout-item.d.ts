/// <reference types="react" />
import { React, IMLayoutItemJson } from 'jimu-core';
import { FlexboxType } from '../config';
import { LayoutItemProps } from 'jimu-layouts/common';
interface OwnProps {
    layoutItem: IMLayoutItemJson;
    activeIds?: string;
    index: number;
    space: number;
    direction: FlexboxType;
    alignItems?: string;
    children?: any;
}
export default class FlexboxItem extends React.PureComponent<LayoutItemProps & OwnProps> {
    render(): JSX.Element;
}
export {};
