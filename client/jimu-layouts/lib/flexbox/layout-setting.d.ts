/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { SettingChangeFunction } from 'jimu-for-builder';
import { IMFlexboxConfig } from './config';
interface Props {
    widgetId: string;
    config: IMFlexboxConfig;
    onSettingChange: SettingChangeFunction;
}
export default class WidgetSetting extends React.PureComponent<Props> {
    _updateSpace: (e: any) => void;
    _updatePadding: (value: any) => void;
    _updateJustifyContent: (e: any) => void;
    _updateAlignItems: (e: any) => void;
    render(): JSX.Element;
}
export {};
