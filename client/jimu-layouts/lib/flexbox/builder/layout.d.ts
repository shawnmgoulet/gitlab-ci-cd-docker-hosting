/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, LayoutItemJson, LayoutItemConstructorProps, IMThemeVariables } from 'jimu-core';
import * as SVG from 'svg.js';
import { IMFlexboxConfig, FlexboxType } from '../config';
import { LayoutProps, StateToLayoutProps, DropHandlers } from 'jimu-layouts/common';
declare type FlexboxLayoutProps = LayoutProps & {
    start?: number;
    end?: number;
    direction: FlexboxType;
    config: IMFlexboxConfig;
    activeIds?: string;
};
interface State {
    isDragover: boolean;
}
declare class Layout extends React.PureComponent<FlexboxLayoutProps & StateToLayoutProps, State> implements DropHandlers {
    ref: HTMLElement;
    guideDragOverRef: HTMLElement;
    guideDragOverDraw: SVG.Doc;
    dropArea: SVG.Rect;
    boundingRect: ClientRect;
    isDragging: boolean;
    childRects: Array<ClientRect & {
        id: string;
    }>;
    domRect: ClientRect;
    resizingRect: ClientRect;
    referenceId: string;
    theme: IMThemeVariables;
    builderTheme: IMThemeVariables;
    state: State;
    constructor(props: any);
    componentDidMount(): void;
    _onItemResizeStart(id: string): void;
    _onItemResizing: () => void;
    _onItemResizeEnd(id: string, x: number, y: number, dw: number, dh: number, layoutItem: LayoutItemJson): void;
    onDragOver(draggingItem: LayoutItemConstructorProps, draggingElement: HTMLElement, containerRect: Partial<ClientRect>, itemRect: Partial<ClientRect>): void;
    toggleDragoverEffect(value: boolean): void;
    onDragEnter(): void;
    onDragLeave(): void;
    onDrop(draggingItem: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect): void;
    _collectBounds(id: string): (ClientRect & {
        id: string;
    })[];
    _createItem(itemId: string, index: number, layoutStyle: any): JSX.Element;
    _filterContent(): any[] | import("seamless-immutable").ImmutableArray<string>;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof Layout, Pick<LayoutProps & {
    start?: number;
    end?: number;
    direction: FlexboxType;
    config: import("seamless-immutable").ImmutableObject<import("../config").FlexboxConfig>;
    activeIds?: string;
} & StateToLayoutProps, "className" | "style" | "direction" | "end" | "start" | "visible" | "layouts" | "config" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight" | "activeIds"> & LayoutProps & {
    start?: number;
    end?: number;
    direction: FlexboxType;
    config: import("seamless-immutable").ImmutableObject<import("../config").FlexboxConfig>;
    activeIds?: string;
}>;
export default _default;
