/// <reference types="react" />
import { React } from 'jimu-core';
import { LinearUnit } from 'jimu-ui';
import { LayoutItemSettingProps } from 'jimu-layouts/common';
export default class FlexboxItemSetting extends React.PureComponent<LayoutItemSettingProps> {
    _updateFitContainer: (e: any) => void;
    _updateBBox: (prop: string, value: LinearUnit) => void;
    _updateAutoSetting: (prop: string, value: boolean) => void;
    _createBBoxPropSetting(bbox: any, label: string, prop: string, key: number): JSX.Element;
    render(): JSX.Element;
}
