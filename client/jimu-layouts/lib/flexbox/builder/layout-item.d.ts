/// <reference types="react" />
import { React, LayoutItemJson, IMLayoutItemJson } from 'jimu-core';
import { FlexboxType } from '../config';
import { LayoutItemProps, StateToLayoutItemProps } from 'jimu-layouts/common';
interface OwnProps {
    layoutItem: IMLayoutItemJson;
    activeIds?: string;
    direction: FlexboxType;
    index: number;
    space: number;
    editingSectionId?: string;
    alignItems?: string;
    children?: any;
    onResizeStart: (id: string) => void;
    onResizing: (id: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd: (id: string, x: number, y: number, dw: number, dh: number, layoutItem: LayoutItemJson) => void;
}
interface State {
    isResizing: boolean;
    dw: number;
    dh: number;
}
export default class FlexboxItem extends React.PureComponent<LayoutItemProps & StateToLayoutItemProps & OwnProps, State> {
    initWidth: number;
    initHeight: number;
    state: State;
    onResizeStart: (id: string, initW: number, initH: number) => void;
    onResizing: (id: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd: (id: string, x: number, y: number, dw: number, dh: number, shiftKey?: boolean) => void;
    isStretchInCrossAxis(): boolean;
    render(): JSX.Element;
}
export {};
