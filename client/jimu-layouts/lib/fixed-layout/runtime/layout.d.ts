/// <reference types="react" />
import { React, ReactRedux, LayoutTransformFunc, BrowserSizeMode } from 'jimu-core';
import { LayoutProps, StateToLayoutProps } from '../../types';
interface OwnProps {
    minHeight?: number;
    ignoreMinHeight?: boolean;
}
declare class FixedLayoutViewer extends React.PureComponent<LayoutProps & StateToLayoutProps & OwnProps> {
    layoutTransform: LayoutTransformFunc;
    browserSizeMode: BrowserSizeMode;
    constructor(props: any);
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof FixedLayoutViewer, Pick<LayoutProps & StateToLayoutProps & OwnProps, "className" | "style" | "minHeight" | "visible" | "layouts" | "isItemAccepted" | "isInSection" | "isInWidget" | "isRepeat" | "isPageItem" | "itemDraggable" | "itemResizable" | "itemSelectable" | "droppable" | "showDefaultTools" | "onItemClick" | "ignoreMinHeight"> & LayoutProps & OwnProps>;
export default _default;
