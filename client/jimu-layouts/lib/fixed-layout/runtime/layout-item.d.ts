/// <reference types="react" />
import { React, IMBoundingBox, IMLayoutItemJson } from 'jimu-core';
import { LayoutItemProps } from '../../types';
interface OwnProps {
    transformedBBox?: IMBoundingBox;
    index: number;
    layoutItem: IMLayoutItemJson;
}
export default class FixedLayoutItem extends React.PureComponent<LayoutItemProps & OwnProps> {
    isRowWidget(): boolean;
    render(): JSX.Element;
}
export {};
