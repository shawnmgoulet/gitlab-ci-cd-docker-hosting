/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { FixedLayoutItemSetting } from '../../types';
import { LinearUnit } from 'jimu-ui';
import { LayoutItemSettingProps } from 'jimu-layouts/common';
export default class FloatingItemSetting extends React.PureComponent<LayoutItemSettingProps> {
    itemSetting: FixedLayoutItemSetting;
    updateBBox: (prop: string, value: LinearUnit) => void;
    updateAutoSetting: (prop: string, value: boolean) => void;
    updateLockParent: (e: any) => void;
    _createBBoxPropSetting(bbox: any, prop: string, key: number): JSX.Element;
    render(): JSX.Element;
}
