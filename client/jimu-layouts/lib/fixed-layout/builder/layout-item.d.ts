/// <reference types="react" />
import { React, LayoutItemJson } from 'jimu-core';
import { LayoutItemProps, FixedLayoutItemSetting } from '../../types';
interface OwnProps {
    layoutItem: LayoutItemJson;
    offsetX?: number;
    offsetY?: number;
    dw?: number;
    dh?: number;
    index: number;
    initRect?: ClientRect;
    containerRect?: ClientRect;
    onResizeStart: (id: string) => void;
    onResizing: (id: string, x: number, y: number, dw: number, dh: number) => void;
    onResizeEnd: (id: string, x: number, y: number, dw: number, dh: number, layoutItem: LayoutItemJson, itemSetting: FixedLayoutItemSetting) => void;
}
interface State {
    isResizing?: boolean;
    dx?: number;
    dy?: number;
}
export default class FixedItem extends React.PureComponent<LayoutItemProps & OwnProps, State> {
    state: State;
    itemSetting: FixedLayoutItemSetting;
    onResizeEnd: (id: string, dx: number, dy: number, dw: number, dh: number) => void;
    isRowWidget(): boolean;
    render(): JSX.Element;
}
export {};
