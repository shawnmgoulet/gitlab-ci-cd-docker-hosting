import { LayoutInfo } from 'jimu-core';
export declare function bringForward(layoutInfo: LayoutInfo): void;
export declare function bringToFront(layoutInfo: LayoutInfo): void;
export declare function sendBackward(layoutInfo: LayoutInfo): void;
export declare function sendToBack(layoutInfo: LayoutInfo): void;
