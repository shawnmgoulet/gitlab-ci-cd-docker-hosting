/// <reference types="seamless-immutable" />
import { Selection, IMAppLayouts } from 'jimu-core';
export declare function selectElement(selection: Selection, layouts: IMAppLayouts): import("seamless-immutable").ImmutableObject<import("jimu-core").AppLayouts>;
