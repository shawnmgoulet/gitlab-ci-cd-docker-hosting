import { ArcGISDataSourceFactory } from './lib/data-source';
export default ArcGISDataSourceFactory;
export * from './lib/data-source';
