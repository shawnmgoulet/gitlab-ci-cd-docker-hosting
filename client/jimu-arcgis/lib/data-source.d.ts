import { DataSource, DataSourceConstructorOptions, DataSourceFactory } from 'jimu-core';
import { DataSourceTypes } from './data-sources/common';
import { MapDataSource } from './data-sources/map-data-source';
import { WebMapDataSource } from './data-sources/webmap-data-source';
import { WebSceneDataSource } from './data-sources/webscene-data-source';
import { FeatureLayerDataSource } from './data-sources/feature-layer-data-source';
import { FeatureDataRecord } from './data-sources/feature-record';
export declare class ArcGISDataSourceFactory implements DataSourceFactory {
    createDataSource(options: DataSourceConstructorOptions): DataSource;
}
export { MapDataSource, WebMapDataSource, WebSceneDataSource, FeatureLayerDataSource, DataSourceTypes as ArcGISDataSourceTypes, DataSourceTypes, FeatureDataRecord };
