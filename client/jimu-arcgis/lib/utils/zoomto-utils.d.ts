export declare function zoomToGraphics(view: __esri.View, graphics: __esri.Graphic[]): Promise<any>;
export declare function zoomToLayer(view: __esri.View, layer: __esri.Layer, graphics?: __esri.Graphic[]): Promise<any>;
export declare function zoomToExtent(view: __esri.View, extent: __esri.Extent): Promise<void>;
