import { JimuMapView, JimuMapViewConstructorOptions } from './views/jimu-map-view';
export declare class JimuMapViewGroup {
    jimuMapViews: {
        [id: string]: JimuMapView;
    };
    constructor();
    addJimuMapView(jimuMapView: JimuMapView): void;
    setJimuMapView(jimuMapView: JimuMapView): void;
    removeJimuMapView(jimuMapView: JimuMapView): void;
    getActiveJimuMapView(): JimuMapView;
}
export declare class MapViewManager {
    static _instance: MapViewManager;
    static getInstance(): MapViewManager;
    private jimuMapViewGroups;
    getJimuMapViewById(id: string): JimuMapView;
    getJimuMapViewGroup(mapWidgetId: string): JimuMapViewGroup;
    createJimuMapView(jimuMapViewConstructorOptions: JimuMapViewConstructorOptions): Promise<JimuMapView>;
    addJimuMapView(jimuMapView: JimuMapView): void;
    setJimuMapView(jimuMapView: JimuMapView): void;
    destroyJimuMapView(jimuMapViewId: string): void;
}
