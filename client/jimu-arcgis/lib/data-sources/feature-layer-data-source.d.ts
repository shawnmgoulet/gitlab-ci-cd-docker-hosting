/// <reference types="seamless-immutable" />
import { AbstractQueriableDataSource, Immutable, QueryResult, CodedValue, QueryOptions } from 'jimu-core';
import { DataSourceTypes } from './common';
import { LayerDataSource, LayerDataSourceConstructorOptions } from './layer-data-source';
import { FeatureDataRecord } from './feature-record';
import { MapDataSource } from './map-data-source';
import { ILayerDefinition } from '@esri/arcgis-rest-types';
import { JimuFeatureLayerView } from '../views';
export interface FeatureLayerDataSourceConstructorOptions extends LayerDataSourceConstructorOptions {
    layer?: __esri.FeatureLayer;
}
export declare class FeatureLayerDataSource extends AbstractQueriableDataSource implements LayerDataSource {
    layer: __esri.FeatureLayer;
    url: string;
    type: DataSourceTypes.FeatureLayer;
    FeatureLayer: typeof __esri.FeatureLayer;
    jimuFeatureLayerViews: {
        [jimuFeatureLayerViewId: string]: JimuFeatureLayerView;
    };
    constructor(options: FeatureLayerDataSourceConstructorOptions);
    ready(): Promise<void>;
    doQuery(query: __esri.Query | __esri.QueryProperties): Promise<QueryResult>;
    doQueryCount(query: __esri.Query | __esri.QueryProperties): Promise<number>;
    doQueryById(id: string): Promise<FeatureDataRecord>;
    mergeQueryParams(baseQuery: __esri.Query | __esri.QueryProperties, newQuery: __esri.Query | __esri.QueryProperties): __esri.Query | __esri.QueryProperties;
    getConfigQueryParams(): __esri.QueryProperties;
    getRealQueryParams(query: any, flag: 'query' | 'load', options?: QueryOptions): __esri.Query | __esri.QueryProperties;
    private getOrderBy;
    fetchSchema(): Promise<Immutable.ImmutableObject<{}>>;
    getMapDataSource: () => MapDataSource;
    getLayerDefinition(): ILayerDefinition;
    getFieldCodedValueList(jimuFieldName: string, record?: FeatureDataRecord): CodedValue[];
    load(query: any): Promise<void>;
    selectRecordById(id: string): void;
    selectRecordsByIds(ids: string[]): void;
    addJimuFeatureLayerView(jimuFeatureLayerView: JimuFeatureLayerView): void;
    removeJimuFeatureLayerView(jimuFeatureLayerView: JimuFeatureLayerView): void;
}
