import { AbstractDataSource, DataSourceConstructorOptions } from 'jimu-core';
import { MapDataSource } from './map-data-source';
export interface LayerDataSourceConstructorOptions extends DataSourceConstructorOptions {
    layer?: __esri.Layer;
}
export declare class LayerDataSource extends AbstractDataSource {
    layer: __esri.Layer;
    getMapDataSource(): MapDataSource;
}
