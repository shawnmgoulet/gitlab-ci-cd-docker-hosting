export declare enum DataSourceTypes {
    Map = "MAP",
    WebMap = "WEB_MAP",
    WebScene = "WEB_SCENE",
    FeatureLayer = "FEATURE_LAYER"
}
export declare function fixLayerId(layerId: string): string;
