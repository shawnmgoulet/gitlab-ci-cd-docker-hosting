import { DataSource, AbstractDataRecord, IntlShape } from 'jimu-core';
export declare class FeatureDataRecord extends AbstractDataRecord {
    feature: __esri.Graphic;
    dataSource: DataSource;
    constructor(feature: __esri.Graphic, dataSource: DataSource, isBeforeMappingData?: boolean);
    getData(): any;
    getFormattedFieldValue(jimuFieldName: string, intl: IntlShape): string;
    getDataBeforeMapping(): __esri.Graphic;
    toJson(): any;
    getId(): string;
    setId(id: string): void;
    getGeometry(): any;
}
