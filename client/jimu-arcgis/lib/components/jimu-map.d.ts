import * as React from 'react';
import { MapDataSource } from '../data-source';
interface Props {
    style?: any;
    itemId?: string;
    mapDataSource?: MapDataSource;
    onMapLoaded?: (mapView: typeof __esri.MapView) => void;
}
export default class JimuMap extends React.PureComponent<Props, any> {
    mapContainer: any;
    componentDidMount(): void;
    render(): JSX.Element;
}
export {};
