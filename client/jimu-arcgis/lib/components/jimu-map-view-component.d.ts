/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, ReactRedux, ImmutableArray, IMJimuMapViewInfo, ImmutableObject } from 'jimu-core';
import { JimuMapView } from '../views/jimu-map-view';
import { MapViewManager } from '../mapview-manager';
interface ViewRenderFunction {
    (views: {
        [viewId: string]: JimuMapView;
    }): React.ReactNode;
}
interface ExtraProps {
    viewInfos: ImmutableObject<{
        [jimuMapViewId: string]: IMJimuMapViewInfo;
    }>;
}
interface Props {
    useMapWidgetIds: ImmutableArray<string>;
    children?: ViewRenderFunction | React.ReactNode;
    onViewsCreate?: (views: {
        [viewId: string]: JimuMapView;
    }) => void;
    onActiveViewChange?: (activeView: JimuMapView, previousActiveViewId: string) => void;
}
interface State {
    activeViewId: string;
    isActiveViewCreated: boolean;
    areAllViewsCreated: boolean;
}
export declare class _JimuMapViewComponent extends React.PureComponent<Props & ExtraProps, State> {
    viewManager: MapViewManager;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props & ExtraProps, prevState: State): void;
    onViewInfosChange: (infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>, useMapWidgetIds: import("seamless-immutable").ImmutableArray<string>) => void;
    onViewsCreate: (views: {
        [viewId: string]: JimuMapView;
    }) => void;
    getActiveViewId: () => string;
    getWhetherAllViewsCreated: (viewIds: string[], infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>) => boolean;
    getWhetherViewCreated: (viewId: any, infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>) => boolean;
    getViewIdsFromUseMapWidgetIds: (useMapWidgetIds: import("seamless-immutable").ImmutableArray<string>, infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>) => string[];
    getViewIdsFromMapWidgetId: (mapWidgetId: string, infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>) => string[];
    getViews: (viewIds: string[]) => {
        [viewId: string]: JimuMapView;
    };
    render(): {};
}
export declare const JimuMapViewComponent: ReactRedux.ConnectedComponentClass<typeof _JimuMapViewComponent, Pick<Props & ExtraProps, "children" | "useMapWidgetIds" | "onViewsCreate" | "onActiveViewChange"> & Props>;
export {};
