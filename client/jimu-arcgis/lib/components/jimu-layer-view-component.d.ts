/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, ImmutableObject, IMJimuMapViewInfo, ImmutableArray } from 'jimu-core';
import { JimuLayerView, IMJimuLayerViewInfo } from 'jimu-arcgis';
export interface JimuLayerViewInfo {
    jimuMapViewId?: string;
    jimuLayerId?: string;
    datasourceId?: string;
    rootDatasourceId?: string;
}
export declare type IMJimuLayerViewInfo = ImmutableObject<JimuLayerViewInfo>;
interface State {
}
interface Props {
    useMapWidgetIds: ImmutableArray<string>;
    jimuLayerViewInfo: IMJimuLayerViewInfo;
    onLayerViewCreated?: (jimuLayerView: JimuLayerView) => void;
    onLayerViewFailed?: (err: any) => void;
}
interface ExtraProps {
    viewInfos: ImmutableObject<{
        [jimuMapViewId: string]: IMJimuMapViewInfo;
    }>;
}
export declare class _JimuLayerViewComponent extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props & ExtraProps, prevState: State): void;
    getJimuMapViewIdsFromUseMapWidgetIds: (useMapWidgetIds: import("seamless-immutable").ImmutableArray<string>, infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>, rootDatasourceId: string) => string[];
    getJimuMapViewIdsFromMapWidgetId: (mapWidgetId: string, infos: import("seamless-immutable").ImmutableObject<{
        [jimuMapViewId: string]: import("seamless-immutable").ImmutableObject<import("jimu-core").JimuMapViewInfo>;
    }>, rootDatasourceId: string) => string[];
    render(): any;
}
export declare const JimuLayerViewComponent: ReactRedux.ConnectedComponentClass<typeof _JimuLayerViewComponent, Pick<Props & ExtraProps, "useMapWidgetIds" | "jimuLayerViewInfo" | "onLayerViewCreated" | "onLayerViewFailed"> & Props>;
export {};
