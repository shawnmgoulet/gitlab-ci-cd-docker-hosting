import { JimuLayerView, JimuLayerViewConstructorOptions } from './jimu-layer-view';
import { LayerDataSource } from '../../data-sources/layer-data-source';
export interface JimuFeatureLayerViewOptions extends JimuLayerViewConstructorOptions {
    view: __esri.FeatureLayerView;
}
export declare class JimuFeatureLayerView extends JimuLayerView {
    view: __esri.FeatureLayerView;
    highLightHandle: any;
    updateWatchHandle: any;
    features: __esri.Graphic[];
    isReservePopup: boolean;
    constructor(options: JimuFeatureLayerViewOptions);
    /**
     * this queries from client
     * @param query
     */
    doQuery(query: any): Promise<__esri.Graphic[]>;
    doQueryById(id: string): Promise<__esri.Graphic>;
    mergeQuery(baseQuery: __esri.Query | __esri.QueryProperties, newQuery: __esri.Query | __esri.QueryProperties): __esri.Query | __esri.QueryProperties;
    setDefinitionExpressionForLayer(query: __esri.Query | __esri.QueryProperties): void;
    highLightSelectedFeatures(): void;
    highLightFeatures(features: __esri.Graphic[] | number[]): void;
    clearHighLight(): void;
    selectRecordById(id: string): void;
    selectRecordsByIds(ids: string[]): void;
    getViewForMap(): __esri.MapView | __esri.SceneView;
    handleFeatureNavigationAtPopUp(id: string): void;
    moveFeatureToCenter(id: string): Promise<void>;
    getCenterPoint(geometry: __esri.Geometry): __esri.Point;
    getSelectedRecordIds(): string[];
    getIdField(): string;
    getLayerDataSource(): LayerDataSource;
    destroy(): void;
}
