export declare enum LayerTypes {
    BaseDynamicLayer = "base-dynamic",
    BaseElevationLayer = "base-elevation",
    BaseTileLayer = "base-tile",
    BuildingSceneLayer = "building-scene",
    CSVLayer = "csv",
    ElevationLayer = "elevation",
    FeatureLayer = "feature",
    GeoJSONLayer = "geojson",
    GeoRSSLayer = "geo-rss",
    GraphicsLayer = "graphics",
    GroupLayer = "group",
    ImageryLayer = "imagery",
    IntegratedMeshLayer = "integrated-mesh",
    KMLLayer = "kml",
    MapImageLayer = "map-image",
    MapNotesLayer = "map-notes",
    PointCloudLayer = "point-cloud",
    SceneLayer = "scene",
    TileLayer = "tile",
    UnknownLayer = "unknown",
    UnsupportedLayer = "unsupported",
    VectorTileLayer = "vector-tile",
    WMSLayer = "wms",
    WMTSLayer = "wmts",
    WebTileLayer = "web-tile"
}
export interface JimuLayerViewConstructorOptions {
    view: __esri.LayerView;
    type: string;
    layerDataSourceId: string;
    jimuMapViewId: string;
    jimuLayerId: string;
}
export declare class JimuLayerView {
    id: string;
    view: __esri.LayerView;
    type: string;
    layerDataSourceId: string;
    jimuMapViewId: string;
    jimuLayerId: string;
    constructor(options: JimuLayerViewConstructorOptions);
    selectRecordsByIds(ids: string[]): void;
    destroy(): void;
}
