import { JimuMapViewStatus } from 'jimu-core';
import { JimuLayerView } from './layers/jimu-layer-view';
export interface JimuMapViewConstructorOptions {
    mapWidgetId: string;
    isActive?: boolean;
    datasourceId: string;
    view: __esri.MapView | __esri.SceneView;
}
export declare class JimuMapView {
    id: string;
    mapWidgetId: string;
    isActive?: boolean;
    datasourceId: string;
    view: __esri.MapView | __esri.SceneView;
    status: JimuMapViewStatus;
    jimuLayerViews?: {
        [jimuLayerViewId: string]: JimuLayerView;
    };
    private jimuLayerViewLoadPromises;
    private isClickedNoPopUpFeature;
    constructor(options: JimuMapViewConstructorOptions);
    setIsActive(isActive: boolean): void;
    private initView;
    private onClick;
    private getJimuLayerViewId;
    private clearAllJimuLayerViewsSelectRecord;
    createJimuLayerViews(): Promise<JimuLayerView[]>;
    private addJimuLayerView;
    whenJimuMapViewLoaded(): Promise<JimuMapView>;
    private whenAllJimuLayerViewsLoaded;
    getJimuLayerViewLoadPromiseById(jimuLayerViewId: string): Promise<JimuLayerView>;
    destroy(): void;
}
