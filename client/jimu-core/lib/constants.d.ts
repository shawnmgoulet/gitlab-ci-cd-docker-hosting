export declare const BREAK_POINTS: number[];
export declare const DEFAULT_VIEWPORT_SIZE: {
    DESKTOP: {
        width: number;
        height: number;
    };
    PAD: {
        width: number;
        height: number;
    };
    PHONE: {
        width: number;
        height: number;
    };
};
export declare const DEFAULT_PAGE_WIDTH = 960;
export declare const SCREEN_RESOLUTIONS: {};
