import { ExpressionPart, IMUseDataSource, ImmutableArray, Expression, ExpressionPartGoup } from '../../index';
export declare function getUseDataSourceFromExpParts(parts: ExpressionPart[] | ImmutableArray<ExpressionPart>): ImmutableArray<IMUseDataSource>;
export declare function mergeUseDataSources(u1?: ImmutableArray<IMUseDataSource>, u2?: ImmutableArray<IMUseDataSource>): ImmutableArray<IMUseDataSource>;
export declare function getWhetherExpressionValid(e: Expression): boolean;
export declare function getWhetherFunctionIncludeChildExpression(parts: ExpressionPartGoup[]): boolean;
export declare function getWhetherFunctionIncludeFieldParam(parts: ExpressionPartGoup[]): boolean;
export declare function getWhetherFieldInDs(dsId: string, jimuFieldName: string, alias: string): boolean;
export declare function getChildExpression(parts: ExpressionPartGoup[]): {
    [index: string]: Expression;
};
