import * as React from 'react';
import { ImmutableObject } from 'seamless-immutable';
import { IMCustomThemeJson, PageJson } from './types/app-config';
import { IMThemeVariables } from './types/theme';
import { UrlParameters } from './types/url-parameters';
import { IntlShape } from 'react-intl';
interface StateProps {
    dispatch?: any;
    appConfigLoaded: boolean;
    customTheme: IMCustomThemeJson;
    themeName: string;
    appPath: string;
    queryObject: UrlParameters;
    dynamicModules: {
        [moduleId: string]: boolean;
    };
    themeVariables: IMThemeVariables;
    pages: ImmutableObject<{
        [pageId: string]: PageJson;
    }>;
    hasWaitingServiceWorker: boolean;
    isRTL: boolean;
}
interface IntlProps {
    intl: IntlShape;
}
declare const _default: React.FunctionComponent<import("react-intl").WithIntlProps<Pick<StateProps & IntlProps, "intl"> & IntlProps>> & {
    WrappedComponent: React.ComponentType<Pick<StateProps & IntlProps, "intl"> & IntlProps>;
};
export default _default;
