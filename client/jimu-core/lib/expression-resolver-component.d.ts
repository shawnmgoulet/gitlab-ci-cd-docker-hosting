/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { ImmutableArray, IntlShape, Immutable, React } from '../index';
import { IMExpressionMap, IMExpression } from './types/expression';
import { IMDataSourceInfo } from './types/state';
import { DataSource, DataRecord } from './data-source/ds-base-types';
import { UseDataSource } from './types/app-config';
interface MultipleExpressionResolvedResults {
    [expressionId: string]: string;
}
interface MultipleExpressionResolvedErrors {
    [expressionId: string]: {
        errorCode: ExpressionResolverErrorCode;
    };
}
interface ResolverRenderFunction {
    (resolvedResults: MultipleExpressionResolvedResults | string, errorCode: MultipleExpressionResolvedErrors | {
        errorCode: ExpressionResolverErrorCode;
    }): React.ReactNode;
}
export declare enum ExpressionResolverErrorCode {
    Failed = "RESOLVE_FAILED",
    NotMatched = "DATA_SOURCES_IN_EXPRESSION_CANNOT_MATCH_DATA_SOURCES_IN_USE"
}
interface Props {
    /**
     * data sources that widget used,
     * if data source of one expression part is not in useDataSources, the part will not be resolved and will only return ''
     */
    useDataSources: ImmutableArray<UseDataSource>;
    /**
     * one or more expressions
     */
    expression: IMExpressionMap | IMExpression;
    /**
     * records to resolve some kinds of expression parts, e.g. field part,
     * if do not pass records, will use the first item of selected records array to resolve expression
     */
    records?: {
        [dataSourceId: string]: DataRecord;
    };
    children?: ResolverRenderFunction | React.ReactNode;
    onSuccess?: (resolvedResults: MultipleExpressionResolvedResults | string) => void;
    onError?: (errorCode: MultipleExpressionResolvedErrors | {
        errorCode: ExpressionResolverErrorCode;
    }) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    resolvedResults: MultipleExpressionResolvedResults | string;
    errors: MultipleExpressionResolvedErrors | {
        errorCode: ExpressionResolverErrorCode;
    };
    infos: {
        [dataSourceId: string]: IMDataSourceInfo;
    };
}
export declare class _ExpressionResolverComponent extends React.PureComponent<Props & ExtraProps, State> {
    dataSources: {
        [dataSourceId: string]: DataSource;
    };
    __unmount: boolean;
    __resolveCount: number;
    constructor(props: Props & ExtraProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(prevProps: Props & ExtraProps, prevState: State): void;
    getWhetherIsMultipleExpression: (expression: Immutable.ImmutableObject<import("jimu-core").Expression> | Immutable.ImmutableObject<import("jimu-core").ExpressionMap>) => boolean;
    resolveExpressions: (expression: Immutable.ImmutableObject<import("jimu-core").Expression> | Immutable.ImmutableObject<import("jimu-core").ExpressionMap>, defaultRecords: {
        [dataSourceId: string]: DataRecord;
    }, useDataSources: Immutable.ImmutableArray<UseDataSource>, currentResolveCount: number) => Promise<string>;
    resolveSingleExpression: (expression: Immutable.ImmutableObject<import("jimu-core").Expression>, records: {
        [dataSourceId: string]: DataRecord;
    }) => Promise<string>;
    onDataSourceCreated: (dss: {
        [dataSourceId: string]: DataSource;
    }) => void;
    onDataSourceInfoChange: (infos: {
        [dataSourceId: string]: Immutable.ImmutableObject<import("jimu-core").DataSourceInfo>;
    }) => void;
    getWhetherExpressionDssInUseDss: (expression: Immutable.ImmutableObject<import("jimu-core").Expression>) => boolean;
    getWhetherDsIdInUseDss: (dsId: string) => boolean;
    getRecords: (useDataSources: Immutable.ImmutableArray<UseDataSource>, defaultRecords: {
        [dataSourceId: string]: DataRecord;
    }) => {
        [dataSourceId: string]: DataRecord;
    };
    getSingleRecord: (dataSourceId: string, defaultRecords: {
        [dataSourceId: string]: DataRecord;
    }) => DataRecord;
    getSingleSelectedRecord: (dataSource: DataSource) => DataRecord;
    render(): JSX.Element;
}
export declare const ExpressionResolverComponent: React.ForwardRefExoticComponent<Pick<Props & ExtraProps, "children" | "expression" | "useDataSources" | "onError" | "onSuccess" | "records"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<Props & ExtraProps>;
};
export {};
