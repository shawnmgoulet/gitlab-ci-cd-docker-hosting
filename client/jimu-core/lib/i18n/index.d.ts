import { ImmutableArray } from 'seamless-immutable';
import { IntlShape } from 'react-intl';
export declare function getLocaleToLoad(locale: string, supportedLocales: string[] | ImmutableArray<string>): string;
/**
 * Get the user's preferred language from the browser.
 *
 * @param navigator https://developer.mozilla.org/en-US/docs/Web/API/Navigator
 */
export declare function getBrowserLanguage(navigator: Navigator): any;
/**
 *
 * @param locale user's current locale
 * @param supportedLocales a list of locales that supported by the library or widget
 */
export declare function getSupportedLocale(locale: string, supportedLocales?: string[] | ImmutableArray<string>): string;
export declare function isSameLanguage(locale1: string, locale2: string): boolean;
export interface I18nMessages {
    [index: string]: string;
}
/**
 * @param baseUrl base URL of library or widget translation files
 * @param locale user's current locale
 */
export declare function loadLocaleMessages(baseUrl: string, locale: string): Promise<I18nMessages>;
export declare function isRTLLocale(locale: any): boolean;
/**
 * @param widgetId if no widgetId, means frameworks'
 * We always use app's state, so builder-self should not call this function.
 * if widgetId is passed in, We always use the widget's i18n messages, not use widget's setting i18n messages
 */
export declare function getIntl(widgetId?: string): IntlShape;
