import DataSourceManager from '../data-source-manager';
import { IMDataSourceJson, IMDataSourceSchema, IMReversedDataSourceSchema } from '../types/app-config';
import { IntlShape } from 'react-intl';
import { IMDataSourceInfo } from '../types/state';
import { EsriDateFormats } from '../types/common';
import { IGeometry, GeometryType } from '@esri/arcgis-rest-types';
export declare enum DataSourceStatus {
    NotCreated = "NOT_CREATED",
    Created = "CREATED",
    CreateError = "CREATE_ERROR",
    Unloaded = "UNLOADED",
    Loading = "LOADING",
    Loaded = "LOADED",
    LoadError = "LOAD_ERROR",
    Saving = "SAVING",
    Saved = "SAVED",
    SaveError = "SAVE_ERROR",
    Closed = "CLOSED",
    Connecting = "CONNECTING",
    Connected = "CONNECTED",
    Closing = "CLOSING"
}
export declare enum DataSourceTypes {
    SimpleLocal = "SIMPLE_LOCAL",
    CSV = "CSV",
    FeatureQuery = "FEATURE_QUERY",
    FeatureSet = "FEATURE_SET"
}
export interface DataSourceFactory {
    createDataSource: (options: DataSourceConstructorOptions) => DataSource;
}
export interface DataRecord {
    dataSource: DataSource;
    /**
     * data should be plain object, and the format is {key: value}, the key is "jimuFieldName"
     *
     * The data before mapping uses this schema: {fieldName: value}, we need to return this schema: {jimuFieldName: value}
     */
    getData: () => any;
    getDataBeforeMapping: () => any;
    getFormattedData: (intl: IntlShape) => any;
    getFieldValue: (jimuFieldName: string) => any;
    getFormattedFieldValue: (jimuFieldName: string, intl: IntlShape) => string;
    convertBeforeMappingDataToData: (beforeMappingData: any) => any;
    /**
     * return the data's JSON format, used to serialize data, the key is "jimuFieldName"
     */
    toJson: () => any;
    getId: () => string;
    setId: (id: string) => void;
    getGeometry: () => IGeometry;
}
export interface DataRecordSet {
    records: DataRecord[];
    fields?: string[];
    dataSource?: DataSource;
}
export interface QueryResult {
    queryParams: any;
    records: DataRecord[];
    fields?: string[];
}
export declare enum QueryScope {
    InAllData = "IN_ALL_DATA",
    InConfigView = "IN_CONFIG-VIEW",
    InRuntimeView = "IN_RUNTIME_VIEW"
}
export interface QueryOptions {
    scope: QueryScope;
}
export interface DataSource {
    /**
     * these 3 props are in app config.
     * put them here for easy use.
     */
    id: string;
    label: string;
    type: string;
    dataSourceJson: IMDataSourceJson;
    dataSourceManager: DataSourceManager;
    /**
     * the dataset data source should create all its child data sources in its constructor
     */
    isDataSourceSet: boolean;
    /**
     * null means it's root datasource.
     */
    parentDataSource: DataSource;
    /**
     * The data schema is in "dataSourceJson",
     *    * For data source, and use "jimuName" as key, and save the actual field name in the object
     *    * For data source set, use "jimuChildId" as key, and save the actual child id in the object
     * In many cases, we need to use the actual "childId" or "fieldName" to access info, so we can use this method to get a reverse schema for easy use.
     */
    getReversedConfigSchema: () => IMReversedDataSourceSchema;
    getRecord: (index: number) => DataRecord;
    getRecordById: (id: string) => DataRecord;
    getRecords: () => DataRecord[];
    setRecords: (records: DataRecord[]) => void;
    getSelectedRecords?: () => DataRecord[];
    getSelectedRecordIndexes?: () => number[];
    getSelectedRecordIds?: () => string[];
    nextRecord?: () => DataRecord;
    prevRecord?: () => DataRecord;
    selectRecord?: (index: number) => void;
    selectRecords?: (indexes: number[]) => void;
    /**
     * jsonData may come from SSR, the data uses jimuFieldName as key
     */
    setJsonData: (jsonData: any[]) => void;
    /**
     * if the record does not exist in the datasource, add it.
     */
    selectRecordById?: (id: string, record?: DataRecord) => void;
    selectRecordsByIds?: (ids: string[], records?: DataRecord[]) => void;
    clearData?: () => void;
    /**
     * the schema returned here is the merge of the configured schema and fetched schema
     */
    getSchema: () => IMDataSourceSchema;
    setSchema: (schema: IMDataSourceSchema) => void;
    /**
     * featch data schema from actual data source. If it's statistic, we don't fetch schema
     */
    fetchSchema: () => Promise<IMDataSourceSchema>;
    getFetchedSchema: () => IMDataSourceSchema;
    setFetchedSchema: (schema: IMDataSourceSchema) => void;
    getStatus: () => DataSourceStatus;
    setStatus?: (status: DataSourceStatus) => void;
    getVersion: () => number;
    addVersion?: () => void;
    getIdField: () => string;
    destroy: () => void;
    ready: () => Promise<void | any>;
    getInfo: () => IMDataSourceInfo;
    getRootDataSource: () => DataSource;
    /**
     * return null means the ds is a non-spatial ds.
     */
    getGeometryType: () => GeometryType;
    /**
     * The followings are for data source set only.
     */
    getChildDataSources?: () => DataSource[];
    /**
     * The child id is differrent with the data source id.
     */
    getChildDataSource?: (jimuChildId: string) => DataSource;
    /**
     * Use the child id to get the full data source id.
     */
    getFullChildDataSourceId?: (jimuChildId: string) => string;
    getJimuChildId?: (childId: string) => string[];
    /**
     * For most of output data source, it has an origin data sources that it's generated from.
     * For the configured data source, return null
     */
    getOriginDataSources?: () => DataSource[];
}
export interface QueriableDataSource extends DataSource {
    url: string;
    /**
     * do query against the service and update the internal data records.
     * @param query
     */
    load(query: any): Promise<void>;
    /**
     * do not use filter configured in ds
     * @param id
     * @param refresh
     */
    loadById(id: string): Promise<DataRecord>;
    /**
     * get the current query param of the current records.
     */
    getCurrentQueryParams(): any;
    getCurrentQueryId(): string;
    getConfigQueryParams(): any;
    /**
     * return merge the new query to base query using AND
     * @param baseQuery
     * @param newQuery
     */
    mergeQueryParams(baseQuery: any, newQuery: any): any;
    /**
     * when do query/load, we do not fire the query request directly, instead, we'll consider the datasource's config/current query param.
     *  For load: we'll AND the configured query param if it has, and we don't consider the options
     *  For query, we'll AND the current query param and the configured query param it it has, and we'll consider the options.
     *
     * @param query
     * @param flag
     */
    getRealQueryParams(query: any, flag: 'query' | 'load', options?: QueryOptions): any;
    /**
     * do query against the service only, do NOT update the internal data records.
     * Will do query based on the data source's current query.
     *
     * To query count, please use `queryCount`
     * @param query
     */
    query(query: any, options?: QueryOptions): Promise<QueryResult>;
    queryById(id: string): Promise<DataRecord>;
    queryCount(query: any, options?: QueryOptions): Promise<number>;
    addRecord?: (record: DataRecord) => Promise<DataRecord>;
    updateRecord?: (record: DataRecord) => Promise<DataRecord>;
    deleteRecord?: (index: number) => Promise<void>;
    getSaveStatus: () => DataSourceStatus;
    setSaveStatus?: (status: DataSourceStatus) => void;
}
export interface LoadableDataSource extends DataSource {
    url: string;
    /**
     * load all data records.
     */
    load(): Promise<DataRecord[]>;
    loadFromLocal?(): Promise<DataRecord[]>;
}
export interface DataSourceConstructorOptions {
    dataSourceJson: IMDataSourceJson;
    dataSourceManager?: DataSourceManager;
    parentDataSource?: DataSource;
    childId?: string;
    jimuChildId?: string;
}
export declare abstract class AbstractDataSource implements DataSource {
    id: string;
    label: string;
    type: string;
    dataSourceJson: IMDataSourceJson;
    private fetchedSchema;
    private schema;
    private reverseSchema;
    dataSourceManager: DataSourceManager;
    isDataSourceSet: boolean;
    parentDataSource: DataSource;
    protected records: DataRecord[];
    getSchema(): IMDataSourceSchema;
    setSchema(schema: IMDataSourceSchema): void;
    fetchSchema(): Promise<IMDataSourceSchema>;
    getFetchedSchema(): IMDataSourceSchema;
    setFetchedSchema(fetchedSchema: IMDataSourceSchema): void;
    getGeometryType(): GeometryType;
    constructor({ dataSourceJson, parentDataSource, dataSourceManager, childId, jimuChildId }: DataSourceConstructorOptions);
    getReversedConfigSchema(): IMReversedDataSourceSchema;
    setRecords(records: DataRecord[]): void;
    setJsonData(data: any[]): void;
    /**
     * @param schema
     */
    private getOneReversedConfigSchema;
    getStatus(): DataSourceStatus;
    setStatus(status: DataSourceStatus): void;
    getVersion(): number;
    addVersion(): void;
    getRecords(): DataRecord[];
    getSelectedRecords(): DataRecord[];
    getSelectedRecordIndexes(): number[];
    getSelectedRecordIds(): string[];
    nextRecord(): DataRecord;
    prevRecord(): DataRecord;
    getRecord(index: number): DataRecord;
    getRecordById(id: string): DataRecord;
    selectRecord(index: number): void;
    selectRecords(indexes: number[]): void;
    selectRecordById(id: string, record?: DataRecord): void;
    selectRecordsByIds(ids: string[], records?: DataRecord[]): void;
    getInfo(): IMDataSourceInfo;
    clearData(): void;
    getIdField(): string;
    getFullChildDataSourceId(jimuChildId: string): string;
    getChildDataSources(): DataSource[];
    getChildDataSource(jimuChildId: string): DataSource;
    getJimuChildId(childId: string): string[];
    getRootDataSource(): DataSource;
    ready(): Promise<void | any>;
    getOriginDataSources(): DataSource[];
    destroy(): void;
    private changeUrlByIndexes;
    private changeUrlByIds;
    private changeUrl;
    private getNewUrlStr;
}
/**
 * @ignore
 */
export declare enum QueryType {
    ByQueryParam = "BY_QUERY_PARAM",
    ById = "BY_ID"
}
/**
 * @ignore
 */
export interface CheckDoQueryOptions {
    queryType: QueryType;
    queryValue: any;
    currentQuery: any;
    refresh: boolean;
}
export declare abstract class AbstractQueriableDataSource extends AbstractDataSource implements QueriableDataSource {
    url: string;
    private currentQueryParams;
    private queryId;
    private resultFields;
    constructor(options: DataSourceConstructorOptions);
    getCurrentQueryParams(): any;
    getCurrentQueryId(): string;
    getResultFields(): string[];
    getRealQueryParams(query: any, flag: 'query' | 'load', options?: QueryOptions): any;
    load(query: any): Promise<void>;
    query(query: any, options?: QueryOptions): Promise<QueryResult>;
    queryCount(query: any, options?: QueryOptions): Promise<number>;
    loadById(id: string): Promise<DataRecord>;
    queryById(id: string): Promise<DataRecord>;
    selectRecordById(id: string, record?: DataRecord): void;
    selectRecordsByIds(ids: string[], records?: DataRecord[]): void;
    addRecord?(record: DataRecord): Promise<DataRecord>;
    updateRecord?(record: DataRecord): Promise<DataRecord>;
    deleteRecord?(index: number): Promise<void>;
    getSaveStatus(): DataSourceStatus;
    setSaveStatus(status: DataSourceStatus): void;
    protected doAdd?(record: DataRecord): Promise<DataRecord>;
    protected doUpdateRecord?(record: DataRecord): Promise<DataRecord>;
    protected doDeleteRecord?(index: number): Promise<void>;
    abstract doQuery(query: any): Promise<QueryResult>;
    abstract doQueryCount(query: any): Promise<number>;
    abstract doQueryById(id: string): Promise<DataRecord>;
    abstract mergeQueryParams(baseQuery: any, newQuery: any): any;
    abstract getConfigQueryParams(): any;
    loadFromLocal(query: any): Promise<DataRecord[]>;
    queryFromLocal?(query: any): Promise<DataRecord[]>;
}
export declare abstract class AbstractLoadableDataSource extends AbstractDataSource implements LoadableDataSource {
    url: string;
    load(): Promise<DataRecord[]>;
    loadFromLocal(): Promise<DataRecord[]>;
    abstract doLoad(): Promise<DataRecord[]>;
    doLoadFromLocal?(): Promise<DataRecord[]>;
}
/**
 * @ignore
 */
export declare class SimpleLocalDataSource extends AbstractDataSource {
    updateAllRecoreds(records: DataRecord[]): void;
    addRecord(record: DataRecord): DataRecord;
    updateRecord(record: DataRecord): void;
    deleteRecord(index: number): void;
}
export declare abstract class AbstractDataRecord implements DataRecord {
    dataSource: DataSource;
    abstract getData(): any;
    abstract getDataBeforeMapping(): any;
    abstract toJson(): any;
    abstract getId(): string;
    abstract setId(id: string): void;
    abstract getGeometry(): IGeometry;
    getFieldValue(jimuFieldName: string): any;
    getFormattedFieldValue(jimuFieldName: string, intl: IntlShape): string;
    convertBeforeMappingDataToData(dataBeforeMapping: any): {};
    convertDataToDataBeforeMapping(data: any): {};
    getFormattedData(intl: IntlShape): {};
    formatDateField(value: any, esriDateFormat: EsriDateFormats, intl: IntlShape): string;
    _formatDate: (date: Date, pattern: string, intl: IntlShape) => string;
    _formatTime: (date: Date, pattern: string, intl: IntlShape) => string;
    formatNumberField(value: number, places: number, digitSeparator: boolean, intl: IntlShape): string;
}
/**
 * @ignore
 */
export declare class SimpleDataRecord extends AbstractDataRecord {
    private data;
    dataSource: DataSource;
    /**
     * beforeMappingData: is the data from the real data source, such as query from remote service/database.
     * data is the data used in Exb.
     *
     * The beforeMappingData uses this schema: {fieldName: value}, we'll reverse it to {jimuFieldName: value}, which is called data and will be used in exb.
     *
     */
    constructor(data: any, dataSource: DataSource, isBeforeMappingData?: boolean);
    getData(): any;
    getDataBeforeMapping(): {};
    toJson(): any;
    getId(): any;
    setId(): void;
    getGeometry(): any;
}
/**
 * @ignore
 */
export declare class SimpleDataRecordSet implements DataRecordSet {
    records: DataRecord[];
    fields?: string[];
    dataSource?: DataSource;
    constructor(options: DataRecordSet);
}
/**
 * @ignore
 */
export declare class CsvDataSource extends AbstractLoadableDataSource {
    type: DataSourceTypes;
    constructor(options: DataSourceConstructorOptions);
    doLoad(): Promise<DataRecord[]>;
}
export interface CodedValue {
    value: string | number;
    label: string;
}
