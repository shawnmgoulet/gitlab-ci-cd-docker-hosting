import { IField, ILayerDefinition, FieldType } from '@esri/arcgis-rest-types';
import { FieldSchema } from '../types/app-config';
import { CodedValue, CheckDoQueryOptions } from './ds-base-types';
export interface DisplayValueObj {
    value: any;
    displayValue: string;
    isCodedValueOrSubtype: boolean;
}
export declare function convertFieldToJimuField(field: IField, fieldInfo: any): FieldSchema;
export declare function checkDoQuery(options: CheckDoQueryOptions): boolean;
export declare function isNumericalField(fieldType: FieldType): boolean;
export declare function getFieldInfoByFieldName(fieldInfos: Array<IField>, fieldName: string): IField;
export declare function getDisplayValueForCodedValueOrSubtype(layerDefinition: ILayerDefinition, fieldName: string, record: any): DisplayValueObj;
export declare function getDisplayValueForCodedValueOrSubtypeBatch(layerDefinition: ILayerDefinition, fieldName: string, recordList: any[]): DisplayValueObj[];
export declare function getCodedValueListForCodedValueOrSubtypeField(layerDefinition: ILayerDefinition, fieldName: string, record?: any): CodedValue[];
export declare function isHostedService(url: string): boolean;
export * from './sql-utils';
