import { DataSourceConstructorOptions, DataSourceTypes, AbstractDataSource } from './ds-base-types';
import { FeatureDataRecord } from './feature-record';
export interface FeatureSetDataSourceConstructorOptions extends DataSourceConstructorOptions {
    records: FeatureDataRecord[];
}
export declare class FeatureSetDataSource extends AbstractDataSource {
    type: DataSourceTypes.FeatureSet;
    constructor(options: FeatureSetDataSourceConstructorOptions);
    getIdField(): string;
    setJsonData(data: any[]): void;
    addRecord(record: FeatureDataRecord): void;
}
