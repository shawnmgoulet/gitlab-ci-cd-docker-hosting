import { ILayerDefinition, GeometryType } from '@esri/arcgis-rest-types';
import * as Immutable from 'seamless-immutable';
import { IQueryFeaturesOptions } from '@esri/arcgis-rest-feature-layer';
import { AbstractQueriableDataSource, DataSourceConstructorOptions, DataSourceTypes, QueryResult, CodedValue, QueryOptions } from './ds-base-types';
import { FeatureDataRecord } from './feature-record';
export declare class FeatureQueryDataSource extends AbstractQueriableDataSource {
    portalUrl?: string;
    itemId?: string;
    layerId?: string;
    layerDefinition: ILayerDefinition;
    url: string;
    type: DataSourceTypes.FeatureQuery;
    constructor(options: DataSourceConstructorOptions);
    getIdField(): string;
    getGeometryType(): GeometryType;
    setJsonData(data: any[]): void;
    doQuery(queryOptions: IQueryFeaturesOptions): Promise<QueryResult>;
    doQueryCount(queryOptions: IQueryFeaturesOptions): Promise<number>;
    private _q;
    doQueryById(id: string): Promise<FeatureDataRecord>;
    getConfigQueryParams(): IQueryFeaturesOptions;
    private getOrderBy;
    mergeQueryParams(baseQuery: IQueryFeaturesOptions, newQuery: IQueryFeaturesOptions): IQueryFeaturesOptions;
    getRealQueryParams(query: any, flag: 'query' | 'load', options?: QueryOptions): IQueryFeaturesOptions;
    addRecord(record: FeatureDataRecord): Promise<any>;
    fetchSchema(): Promise<Immutable.ImmutableObject<{}>>;
    getLayerDefinition(): ILayerDefinition;
    getFieldCodedValueList(jimuFieldName: string, record?: FeatureDataRecord): CodedValue[];
}
