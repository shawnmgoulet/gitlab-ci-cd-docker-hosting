import { IMSqlExpression, SqlResult, SqlExpression } from '../types/sql-expression';
import { DataSource } from './ds-base-types';
export declare function getArcGISSQL(sqlExprObj: IMSqlExpression, dataSource: DataSource): SqlResult;
export declare function parserSQL(sql: string): SqlExpression;
export declare function getUTCTimes(date: Date): number;
export declare function getLocalTimes(date: Date): number;
