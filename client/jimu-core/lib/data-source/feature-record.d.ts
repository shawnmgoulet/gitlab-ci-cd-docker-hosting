import { IFeature } from '@esri/arcgis-rest-types';
import { AbstractDataRecord } from './ds-base-types';
import { FeatureQueryDataSource } from './feature-query-data-source';
import { FeatureSetDataSource } from './feature-set-data-source';
import { IntlShape } from 'react-intl';
export declare class FeatureDataRecord extends AbstractDataRecord {
    feature: IFeature;
    dataSource: FeatureQueryDataSource | FeatureSetDataSource;
    constructor(feature: IFeature, dataSource: FeatureQueryDataSource | FeatureSetDataSource, isBeforeMappingData?: boolean);
    getData(): {
        [key: string]: any;
    };
    getFormattedFieldValue(jimuFieldName: string, intl: IntlShape): string;
    getDataBeforeMapping(): {};
    getOriginData(): IFeature;
    toJson(): IFeature;
    getId(): string;
    setId(id: string): void;
    getGeometry(): import("@esri/arcgis-rest-types").IGeometry;
}
