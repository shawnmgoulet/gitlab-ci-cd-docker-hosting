declare const _default: {
    loading: string;
    ok: string;
    cancel: string;
    notPublishError: string;
    versionUpdateMsg: string;
    doUpdate: string;
    search: string;
    message_StringSelectionChange: string;
    message_ExtentChange: string;
    message_DataRecordsSelectionChange: string;
    message_DataRecordSetCreate: string;
    message_DataRecordSetUpdate: string;
    message_SelectDataRecord: string;
    dataAction_ExportJson: string;
    messageAction_SelectDataRecord: string;
    messageAction_FilterDataRecord: string;
};
export default _default;
