import { UserSession, ICredential, IUser } from '@esri/arcgis-rest-auth';
import { HostAuthListenMessage } from './post-message';
interface IDeferred<T> {
    promise: Promise<T>;
    resolve: (v: T) => void;
    reject: (v: any) => void;
}
export interface PortalInfo {
    portalUrl: string;
    clientId: string;
}
export interface SigninInfo {
    desUrl: string;
    fromUrl: string;
    popup: boolean;
    sessionDefer: IDeferred<UserSession>;
}
export declare enum SessionChangedReasonType {
    ArcGISJSSync = "ARCGIS_JS",
    OtherWindowSync = "OTHER_WINDOW",
    AddOrUpdate = "ADD_OR_UPDATE",
    Remove = "REMOVE"
}
export default class SessionManager {
    static instance: SessionManager;
    /**
     * The key is combined with urlKey and customBaseUrl for normal portal,
     * but if the customBaseUrl is a map url(start with maps|mapsqa|mapsdevext),
     * the customBaseUrl's prefix will be replaced by www|qaext|devext.
     *
     * urlKey is the first word of a url, such as 'www',
     * and customBaseUrl is a url part before 'sharing/rest' and after urlkey
     *
     * example1:
     * url: https://beijing.mapsqa.arcgis.com
     * urlKey: beijing
     * customBaseUrl: mapsqa.arcgis.com
     * key: qaext.arcgis.com
     *
     * example2:
     * url: http://private.test.com
     * urlKey: private
     * customBaseUrl: test.com
     * key: private.test.com
     *  */
    private _sessions;
    private _mainPortalInfo;
    private _waitingForHostAuth;
    private _arcgisJSIM;
    private _isSigning;
    private _signInQueue;
    private _onSessionChangedCallbacks;
    getMainSession(): UserSession;
    getSessions(): Array<UserSession>;
    clearSessions(): void;
    syncSessionsFromOtherWindow(sessions: Array<UserSession>): void;
    private _onSessionChanged;
    static getInstance(): SessionManager;
    constructor();
    setArcgisJsIM(IM: __esri.IdentityManager): void;
    getArcgisJsIM(): __esri.IdentityManager;
    /**
     * because there is no clientId in esri_auth, so pass in here
     * @param clientId
     */
    initFromCookie(portalInfo?: PortalInfo): void;
    updateFromCookie(): void;
    private _initSignInCookie;
    getUserInfo(session: UserSession): Promise<IUser>;
    /**
     * After get portal self, the portalUrl may change, such as:
     * from `www.arcgis.com` to `esridevbeijing.maps.arcgis.com`
     * @param portalUrl
     */
    setSessionPortalUrl(session: UserSession, portalUrl: string): boolean;
    /**
     * because when init from cookie, the client id may be null, so set here
     * @param clientId
     */
    setClientId(clientId: string): void;
    setMainPortalUrl(portalUrl: string): void;
    setMainPortal(portalInfo: PortalInfo): void;
    getMainPortal(): PortalInfo;
    removeSession(session: UserSession): boolean;
    private _syncToOtherWindowSessionManager;
    private _removeSessionByKey;
    addOrReplaceSession(session: UserSession): boolean;
    private _addOrReplaceSession;
    private _setSession;
    getSessionByUrl(url: string): UserSession;
    private _getKeyFromUrl;
    private _getPortalUrlInfo;
    private _getSessionFromCookieJson;
    private _getSessionFromCookieString;
    private getPortalFromCookie;
    private _checkCookie;
    private _checkSession;
    readCookie(cookieName: string): string[];
    private writeCookieForSession;
    private _getCookieFromSession;
    private removeCookie;
    addFromArcGisJSCredential(credential: ICredential): boolean;
    private _getHostname;
    private _getArcgisKey;
    private _isArcgisUrl;
    private _getLoginDomain;
    signIn(fromUrl?: string, popup?: boolean, desUrl?: string): Promise<UserSession>;
    addSessionChangeListener(listener: (session: Array<UserSession>, reasonType: SessionChangedReasonType) => void): void;
    private _executSignIn;
    handleAuthError: (error: any, popup?: boolean) => Promise<UserSession>;
    signOut(): void;
    syncHostAuth(message: HostAuthListenMessage): void;
    defer<T>(): IDeferred<T>;
}
export {};
