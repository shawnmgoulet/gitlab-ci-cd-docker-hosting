import * as React from 'react';
import { IMDataSourceInfo } from './types/state';
import { DataSource } from './data-source/ds-base-types';
import { UseDataSource } from './types/app-config';
import { ImmutableArray } from 'jimu-core';
interface DataRenderFunction {
    (dss: {
        [dataSourceId: string]: DataSource;
    }, infos: {
        [dataSourceId: string]: IMDataSourceInfo;
    }): React.ReactNode;
}
export interface MultipleDataSourceComponentProps {
    useDataSources?: ImmutableArray<UseDataSource>;
    queries?: {
        [dataSourceId: string]: any;
    };
    children?: DataRenderFunction | React.ReactNode;
    onDataSourceInfoChange?: (infos: {
        [dataSourceId: string]: IMDataSourceInfo;
    }) => void;
    onDataSourceCreated?: (dss: {
        [dataSourceId: string]: DataSource;
    }) => void;
}
interface ExtraProps {
    [dsId: string]: IMDataSourceInfo;
}
declare type AllProps = MultipleDataSourceComponentProps & ExtraProps;
export declare class MultipleDataSourceComponentInner extends React.PureComponent<AllProps, {}> {
    handleDataSourceCreated: () => void;
    handleDataSourceInfoChange: () => void;
    getDataSources: () => {};
    getDataSourcesInfo: () => {};
    hasLoadedDataSources: () => boolean;
    render(): JSX.Element | JSX.Element[];
}
declare const _default: import("react-redux").ConnectedComponentClass<typeof MultipleDataSourceComponentInner, Pick<AllProps, never> & MultipleDataSourceComponentProps>;
export default _default;
