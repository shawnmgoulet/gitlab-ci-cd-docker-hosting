interface WidgetDataSourceQuery {
    widgetId: string;
    dataSourceId: string;
    queryOption: any;
}
/**
 * Multiple widget may need to manipulate the same datasource with different query, we use this manager to manage it.
 */
export default class WidgetDataSourceQueryManager {
    static instance: WidgetDataSourceQueryManager;
    static getInstance(): WidgetDataSourceQueryManager;
    private queries;
    constructor();
    setWidgetQueryOption(widgetId: string, dataSourceId: string, queryOption: any): void;
    getQueryOption(widgetId: string, dataSourceId: string): WidgetDataSourceQuery;
    getDataSourceQueryOptions(dataSourceId: string): any[];
    getMergedDataSourceQueryOptions(dataSourceId: string): {};
    removeQueryOptionByDataSource(dataSourceId: string): void;
    removeQueryOptionByWidget(widgetId: string): void;
    private onDataSourceChange;
    private onWidgetChange;
}
export {};
