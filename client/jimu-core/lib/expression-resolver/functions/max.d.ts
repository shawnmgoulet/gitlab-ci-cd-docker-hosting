import { ExpressionPart } from '../../types/expression';
export declare function max(parts: ExpressionPart[]): Promise<number>;
