import { ExpressionPart } from '../../types/expression';
export declare function count(parts: ExpressionPart[], dataSourceId: string): Promise<number>;
