import { ExpressionPart } from '../../types/expression';
export declare function min(parts: ExpressionPart[]): Promise<number>;
