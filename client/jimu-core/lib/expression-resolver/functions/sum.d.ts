import { ExpressionPart } from '../../types/expression';
export declare function sum(parts: ExpressionPart[]): Promise<number>;
