import { DataRecord, ImmutableArray, IntlShape } from 'jimu-core';
import { ExpressionPart, ExpressionFunctions, ExpressionPartGoup } from '../types/expression';
import { DataSourceTypes } from '../data-source/ds-base-types';
export declare const localDataSourceTypes: DataSourceTypes[];
export declare enum errorCode {
    ParamType = "INVALID_PARAM_TYPE",
    ParamNumber = "INVALID_NUMBER_OF_PARAMS",
    FuncType = "INVALID_FUNCTION_TYPE"
}
export declare function getFunctionResult(parts: ExpressionPartGoup[], func: ExpressionFunctions): Promise<number>;
export declare function getLocalDsFunctionResult(part: ExpressionPartGoup, func: ExpressionFunctions): Promise<number>;
export declare function getQueryResult(parts: ExpressionPart[], func: ExpressionFunctions): Promise<number>;
export declare function isMultiple(record: DataRecord | {
    [dataSourceId: string]: DataRecord;
}): boolean;
export declare function groupPartsByFunction(parts: ExpressionPart[] | ImmutableArray<ExpressionPart>): ExpressionPartGoup[];
export declare function getWhetherExpUseSelectedRecord(partsGroupByFunction: ExpressionPartGoup[]): boolean;
export declare function resolveFieldAndFunctionParts(partsGroupByFunction: ExpressionPartGoup[], record: DataRecord | {
    [dataSourceId: string]: DataRecord;
}, whetherFormatData: boolean, intl?: IntlShape): Promise<ExpressionPart>[];
export declare function resolveFunctions(p: ExpressionPartGoup, record: DataRecord | {
    [dataSourceId: string]: DataRecord;
}, whetherFormatData: boolean, intl?: IntlShape): Promise<number | string>;
export declare function resolveOneFunction(p: ExpressionPartGoup): Promise<number | string>;
export declare function calculatePostfixExpression(parts: ExpressionPart[], whetherFormatData: boolean): string | number;
export declare function calculate(n1: string | number, n2: string | number, operator: string | number): number | string;
export declare function getPostfixParts(parts: ExpressionPart[]): ExpressionPart[];
export declare function getStringOrNumberPartFromExp(e: any, whetherFormatData: boolean, intl?: IntlShape): ExpressionPart;
export declare function resolveField(jimuFieldName: string, record: DataRecord, whetherFormatData: boolean, intl?: IntlShape): string | boolean | number;
export declare function getWhetherFunctionIncludeFieldParamFromLocalDataSource(parts: ExpressionPartGoup[]): boolean;
export declare function getWhetherIsFromLocalDataSource(part: ExpressionPart): boolean;
