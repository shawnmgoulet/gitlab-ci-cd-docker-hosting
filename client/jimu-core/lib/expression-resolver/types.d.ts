export declare enum StatisticType {
    Count = "count",
    Sum = "sum",
    Min = "min",
    Max = "max",
    Avg = "avg"
}
export interface Cache {
    [id: string]: number;
}
export declare const ESRIOBJECTIDTYPE = "esriFieldTypeOID";
