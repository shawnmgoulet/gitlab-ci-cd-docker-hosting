import { DataRecord, IntlShape } from 'jimu-core';
import { Expression, IMExpression } from '../types/expression';
import { groupPartsByFunction } from './utils';
export { groupPartsByFunction as groupExpressionPartsByFunction };
export declare function resolveExpression(expression: Expression | IMExpression, record?: DataRecord | {
    [dataSourceId: string]: DataRecord;
}, intl?: IntlShape): Promise<string>;
