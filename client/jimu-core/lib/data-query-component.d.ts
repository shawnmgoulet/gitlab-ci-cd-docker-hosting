import * as React from 'react';
import { IMDataSourceInfo } from './types/state';
import { DataSource, QueriableDataSource, DataSourceStatus, QueryResult } from './data-source/ds-base-types';
import DataSourceManager from './data-source-manager';
import { IMDataSourceJson, IMUseDataSource } from './types/app-config';
interface DataRenderFunction {
    (ds: DataSource, queryStatus: DataSourceStatus, queryResult: QueryResult, count: number): React.ReactNode;
}
export interface DataSourceComponentProps {
    useDataSource: IMUseDataSource;
    /**
     * force query even the query param does not change
     */
    refresh?: boolean;
    /**
     * the query params for the queriable data source
     */
    query: any;
    queryCount?: boolean;
    children?: DataRenderFunction | React.ReactNode;
    onQueryStatusChange?: (status: DataSourceStatus) => void;
    onDataSourceCreated?: (ds: DataSource) => void;
    onCreateDataSourceFailed?: (err: any) => void;
    onQueryStart?: () => void;
}
interface DataSourceComponentStateProps {
    dataSource: QueriableDataSource;
    dataSourceInfo: IMDataSourceInfo;
    dataSourceJson: IMDataSourceJson;
    rootDataSourceJson?: IMDataSourceJson;
    dataSourceManager: DataSourceManager;
}
declare type AllProps = DataSourceComponentProps & DataSourceComponentStateProps;
interface State {
    dataSourceId: string;
    currentQueryParam: any;
    queryStatus: DataSourceStatus;
    queryResult: QueryResult;
    count: number;
}
declare class DataQueryComponentInner extends React.PureComponent<AllProps, State> {
    state: {
        dataSourceId: any;
        currentQueryParam: any;
        queryStatus: DataSourceStatus;
        queryResult: any;
        count: any;
    };
    static getDerivedStateFromProps(props: AllProps, state: State): State;
    componentDidMount(): void;
    componentDidUpdate(prevProps: AllProps, prevState: State): void;
    render(): {};
    doQuery(): void;
    createDataSource(): void;
}
declare const _default: import("react-redux").ConnectedComponentClass<typeof DataQueryComponentInner, Pick<AllProps, "children" | "query" | "onDataSourceCreated" | "useDataSource" | "queryCount" | "refresh" | "onCreateDataSourceFailed" | "onQueryStart" | "onQueryStatusChange"> & DataSourceComponentProps>;
export default _default;
