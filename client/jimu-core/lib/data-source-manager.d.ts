import { DataSource, DataSourceConstructorOptions } from './data-source/ds-base-types';
import { IMDataSourceJson, IMDataSourceSchema, IMUseDataSource } from './types/app-config';
import { ImmutableObject } from 'seamless-immutable';
export default class DataSourceManager {
    static instance: DataSourceManager;
    static getInstance(): DataSourceManager;
    constructor();
    private dataSources;
    private dataSourcesCreatePromise;
    private dataSourceFactories;
    getDataSource(dsId: string): DataSource;
    getDataSources(): {
        [dsId: string]: DataSource;
    };
    getDataSourcesAsArray(): DataSource[];
    setDataSource(dsId: string, ds: DataSource): void;
    destroyAllDataSources(): void;
    destroyDataSource(dsId: string): void;
    /**
     * return the data sources that are configured in ds setting
     */
    getConfiguredDataSources(): DataSource[];
    getWidgetGeneratedDataSources(): {
        [widgetId: string]: DataSource[];
    };
    /**
     * return the create successfully datasources only.
     * For the failed data source, return null
     */
    createAllDataSources(): Promise<DataSource[]>;
    /**
     *
     * @param ds if the ds is JSON, the ds may not be in app config, this is the case that widget can use internal data source.
     */
    createDataSource(dsId: string): Promise<DataSource>;
    createDataSource(dsJson: IMDataSourceJson): Promise<DataSource>;
    createDataSource(options: DataSourceConstructorOptions): Promise<DataSource>;
    /**
     * If the use datasource does not exist in app config, try to create it's root datasource instead.
     * @param useDs
     */
    createDataSourceByUseDataSource(useDs: IMUseDataSource): Promise<DataSource>;
    createOriginDataSources(dataSourceJson: IMDataSourceJson): Promise<void>;
    initSchema(dataSource: DataSource, fetchedSchema: IMDataSourceSchema): void;
    mergeSchema(dataSource: DataSource, configedSchema: IMDataSourceSchema, fetchedSchema: IMDataSourceSchema): IMDataSourceSchema;
    private mergeOneSchema;
    private createDsObj;
    protected getAppConfig(): ImmutableObject<import("./types/app-config").AppConfig>;
    private createJimuCoreDataSource;
    private onStoreChange;
}
