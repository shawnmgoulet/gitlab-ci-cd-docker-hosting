import * as React from 'react';
import { IMDataSourceInfo } from './types/state';
import { DataSource } from './data-source/ds-base-types';
import DataSourceManager from './data-source-manager';
import { IMDataSourceJson, IMUseDataSource } from './types/app-config';
interface DataRenderFunction {
    (ds: DataSource, info: IMDataSourceInfo, count: number): React.ReactNode;
}
export interface DataSourceComponentProps {
    useDataSource: IMUseDataSource;
    queryCount?: boolean;
    /**
     * force query even the query param does not change
     */
    refresh?: boolean;
    /**
     * the query params for the queriable data source. Will always use this query to query data for ds
     */
    query?: any;
    /**
     * Will be used only when ds is unloaded.
     */
    defaultQuery?: any;
    /**
     * If the children's render depends on data source, please use render function,
     * or your children will not re-rendered oven the data source is changed,
     * unless you connect the data source info in the children's owner component to make the owner compoent re-render
     */
    children?: DataRenderFunction | React.ReactNode;
    onDataSourceInfoChange?: (info: IMDataSourceInfo) => void;
    onDataSourceCreated?: (ds: DataSource) => void;
    onCreateDataSourceFailed?: (err: any) => void;
    onQueryStart?: () => void;
}
interface DataSourceComponentStateProps {
    dataSource: DataSource;
    dataSourceInfo: IMDataSourceInfo;
    dataSourceJson: IMDataSourceJson;
    rootDataSourceJson?: IMDataSourceJson;
    dataSourceManager: DataSourceManager;
}
interface state {
    count: number;
}
declare type AllProps = DataSourceComponentProps & DataSourceComponentStateProps;
declare class DataSourceComponentInner extends React.PureComponent<AllProps, state> {
    constructor(props: AllProps);
    componentDidMount(): void;
    componentDidUpdate(prevProps: AllProps): void;
    render(): {};
    doQuery(): void;
    createDataSource(): void;
}
declare const _default: import("react-redux").ConnectedComponentClass<typeof DataSourceComponentInner, Pick<AllProps, "children" | "query" | "onDataSourceInfoChange" | "onDataSourceCreated" | "useDataSource" | "queryCount" | "refresh" | "defaultQuery" | "onCreateDataSourceFailed" | "onQueryStart"> & DataSourceComponentProps>;
export default _default;
