import { ImmutableArray, ImmutableObject } from 'seamless-immutable';
export declare enum ClauseLogic {
    And = "AND",
    Or = "OR",
    Not = "NOT"
}
export declare enum ClauseCascade {
    None = "NONE",
    Previous = "PREVIOUS",
    All = "ALL"
}
export declare enum ClauseOperator {
    StringOperatorIs = "STRING_OPERATOR_IS",
    StringOperatorIsNot = "STRING_OPERATOR_IS_NOT",
    StringOperatorContains = "STRING_OPERATOR_CONTAINS",
    StringOperatorDoesNotContain = "STRING_OPERATOR_DOES_NOT_CONTAIN",
    StringOperatorStartsWith = "STRING_OPERATOR_STARTS_WITH",
    StringOperatorEndsWith = "STRING_OPERATOR_ENDS_WITH",
    StringOperatorIsAnyOf = "STRING_OPERATOR_IS_ANY_OF",
    StringOperatorIsNoneOf = "STRING_OPERATOR_IS_NONE_OF",
    NumberOperatorIs = "NUMBER_OPERATOR_IS",
    NumberOperatorIsNot = "NUMBER_OPERATOR_IS_NOT",
    NumberOperatorIsAtLeast = "NUMBER_OPERATOR_IS_AT_LEAST",
    NumberOperatorIsAtMost = "NUMBER_OPERATOR_IS_AT_MOST",
    NumberOperatorIsLessThan = "NUMBER_OPERATOR_IS_LESS_THAN",
    NumberOperatorIsGreaterThan = "NUMBER_OPERATOR_IS_GREATER_THAN",
    NumberOperatorIsBetween = "NUMBER_OPERATOR_IS_BETWEEN",
    NumberOperatorIsNotBetween = "NUMBER_OPERATOR_IS_NOT_BETWEEN",
    NumberOperatorIsAnyOf = "NUMBER_OPERATOR_IS_ANY_OF",
    NumberOperatorIsNoneOf = "NUMBER_OPERATOR_IS_NONE_OF",
    DateOperatorIsOn = "DATE_OPERATOR_IS_ON",
    DateOperatorIsNotOn = "DATE_OPERATOR_IS_NOT_ON"
}
export declare enum ClauseSourceType {
    UserInput = "USER_INPUT",
    Field = "FIELD",
    SingleSelectFromUnique = "UNIQUE",
    MultipleSelectFromUnique = "MULTIPLE"
}
export interface ClauseValuePair {
    value: number | string;
    label: string;
}
export declare enum ClauseType {
    Single = "SINGLE",
    Set = "SET"
}
export interface ClauseValueOptions {
    sourceType: ClauseSourceType;
    inputEditor: string;
    value: ClauseValuePair[];
    isValid?: boolean;
}
export declare type IMClauseValueOptions = ImmutableObject<ClauseValueOptions>;
export interface ClauseAskForValueOptions {
    label: string;
    hint: string;
    cascade?: ClauseCascade;
}
export declare type IMClauseAskForValueOptions = ImmutableObject<ClauseAskForValueOptions>;
export interface SqlClause {
    type: ClauseType;
    caseSensitive?: boolean;
    jimuFieldName: string;
    askForValueOptions: ClauseAskForValueOptions;
    operator: ClauseOperator;
    valueOptions: ClauseValueOptions;
}
export declare type IMSqlClause = ImmutableObject<SqlClause>;
export interface SqlClauseSet {
    type: ClauseType;
    logicalOperator: ClauseLogic;
    parts: ImmutableArray<SqlClause | SqlClauseSet>;
}
export declare type IMSqlClauseSet = ImmutableObject<SqlClauseSet>;
export interface SqlExpression {
    sql: string;
    displaySQL?: string;
    logicalOperator: ClauseLogic;
    parts: (SqlClause | SqlClauseSet)[];
}
export declare type IMSqlExpression = ImmutableObject<SqlExpression>;
export interface SqlResult {
    sql: string;
    displaySQL: string;
}
export declare enum OrderRule {
    Asc = "ASC",
    Desc = "DESC"
}
export interface OrderByOption {
    jimuFieldName: string;
    order: OrderRule;
}
