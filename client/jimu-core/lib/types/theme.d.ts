import { ImmutableObject } from 'seamless-immutable';
declare type NumbericValueWithUnit = string;
declare type Color = string;
declare type Size = NumbericValueWithUnit | 0;
declare type LineHeight = number | NumbericValueWithUnit;
export { Size as ThemeSize, Color as ThemeColor };
export declare enum ThemeCommonColorKeys {
    WHITE = "white",
    BLACK = "black",
    TRANSPARENT = "transparent"
}
export declare enum ThemeThemeColorKeys {
    PRIMARY = "primary",
    SECONDARY = "secondary",
    SUCCESS = "success",
    INFO = "info",
    WARNING = "warning",
    DANGER = "danger",
    LIGHT = "light",
    DARK = "dark"
}
export declare enum ThemeColorationKeys {
    MINIMAL = "minimal",
    DEFAULT = "default"
}
export declare type ThemeColorationType = 'minimal' | 'default';
export declare type ThemeColorationVariants = {
    [index in ThemeColorationKeys]?: {
        header: {
            color?: Color;
            bg?: Color;
        };
        body?: {
            color?: Color;
            bg?: Color;
        };
        footer?: {
            color?: Color;
            bg?: Color;
        };
    };
};
export interface ThemeFontStyleBase {
    fontFamily?: string;
    fontWeight?: string;
    fontSize?: Size;
    fontStyle?: string;
    lineHeight?: LineHeight;
    color?: Color;
}
export interface ThemeFontStyleGroup {
    heading?: ThemeFontStyleBase;
    body?: ThemeFontStyleBase;
}
export interface ThemeHeadings extends ThemeFontStyleBase {
    h1FontSize?: Size;
    h2FontSize?: Size;
    h3FontSize?: Size;
    h4FontSize?: Size;
    h5FontSize?: Size;
    h6FontSize?: Size;
    marginBottom?: Size;
}
export interface ThemeParagraph {
    marginBottom?: Size;
}
export interface ThemeLabel {
    marginBottom?: Size;
}
export interface ThemeHr {
    marginY?: Size;
    borderColor?: Color;
    borderWidth?: Size;
}
export interface ThemeSmall {
    fontSize?: Size;
}
export interface ThemeMark {
    padding?: Size;
    bg?: Color;
}
export interface ThemeTypographyBase {
    fontFamilyBase?: string;
    fontSizeRoot?: Size;
    fontSizeBase?: Size;
    sizes?: {
        default?: Size;
        sm?: Size;
        lg?: Size;
        display1?: Size;
        display2?: Size;
        display3?: Size;
        display4?: Size;
        display5?: Size;
        display6?: Size;
    };
    weights?: {
        medium?: string;
        light?: string;
        extraLight?: string;
        bold?: string;
        extraBold?: string;
    };
    lineHeights?: {
        medium?: LineHeight;
        sm?: LineHeight;
        lg?: LineHeight;
    };
}
export interface ThemeTypography extends ThemeTypographyBase {
    headings?: ThemeHeadings;
    paragraph?: ThemeParagraph;
    label?: ThemeLabel;
    hr?: ThemeHr;
    small?: ThemeSmall;
    mark?: ThemeMark;
}
export declare type ThemeCommonColors = {
    [index in ThemeCommonColorKeys]?: Color;
};
export declare type ThemeThemeColors = {
    [index in ThemeThemeColorKeys]?: Color;
};
export interface ThemeColorPaletteItem {
    100?: Color;
    200?: Color;
    300?: Color;
    400?: Color;
    500?: Color;
    600?: Color;
    700?: Color;
    800?: Color;
    900?: Color;
}
export declare type PaletteLightnessRange = [number, number];
export declare type ThemeColorPalette = {
    [index in ThemeThemeColorKeys]?: ThemeColorPaletteItem;
};
export interface ColorPaletteGenerator {
    shadeCount: number;
    getLightness: (color: Color) => number;
    getShadeLevel: (lightness: number, lightnessRange: PaletteLightnessRange, shadeLevel: number) => number;
    getColorsByShade: (color: Color, lightness: number, shadeLevel: number) => ThemeColorPaletteItem;
    generate: (themeColors: ThemeThemeColors, isDarkTheme?: boolean) => ThemeColorPalette;
}
export interface ThemeAllColors extends ThemeCommonColors, ThemeThemeColors {
    palette?: ThemeColorPalette;
}
export interface ThemeBody extends ThemeFontStyleBase {
    color?: Color;
    bg?: Color;
}
export interface ThemeHeader {
    color?: Color;
    bg?: Color;
}
export interface ThemeFooter {
    color?: Color;
    bg?: Color;
}
export interface ThemeLink {
    color?: Color;
    decoration?: string;
    hover?: {
        color?: Color;
        decoration?: string;
    };
}
export interface ThemeBorder {
    color?: Color;
    width?: Size;
}
export declare type ThemeSizes = {
    0?: Size;
    1?: Size;
    2?: Size;
    3?: Size;
    4?: Size;
    5?: Size;
};
export interface ThemeBorderRadiuses {
    none?: Size;
    sm?: Size;
    default?: Size;
    lg?: Size;
    circle?: Size;
    pill?: Size;
}
export interface ThemeBoxShadows {
    none?: string;
    default?: string;
    sm?: string;
    lg?: string;
}
export interface ThemeButtonSize {
    fontSize?: Size;
    lineHeight?: LineHeight;
    paddingX?: Size;
    paddingY?: Size;
    borderRadius?: Size;
}
export interface ThemeInputSize {
    fontSize?: Size;
    paddingY?: Size;
    paddingX?: Size;
    lineHeight?: LineHeight;
    borderRadius?: Size;
    height?: Size | 'auto';
}
export interface ThemeAlert {
    paddingY?: Size;
    paddingX?: Size;
    marginBottom?: Size;
    borderRadius?: Size;
    linkFontWeight?: string;
    borderWidth?: Size;
    bgLevel?: string;
    borderLevel?: string;
    colorLevel?: string;
}
export interface ThemeBadge {
    fontSize?: Size;
    fontWeight?: string;
    paddingX?: Size;
    paddingY?: Size;
    borderRadius?: Size;
    pill?: {
        paddingX?: Size;
        borderRadius?: Size;
    };
}
export interface ThemeBreadcrumb {
    bg?: Color;
    activeColor?: Color;
    paddingY?: Size;
    paddingX?: Size;
    marginBottom?: Size;
    borderRadius?: Size;
    item?: {
        color?: Color;
        hoverColor?: Color;
        padding?: Size;
    };
    divider?: {
        content?: string;
        color?: Color;
    };
}
export declare enum ThemeButtonTypes {
    'default' = "DEFAULT",
    'primary' = "PRIMARY",
    'secondary' = "SECONDARY",
    'tertiary' = "TERTIARY",
    'danger' = "DANGER",
    'link' = "LINK"
}
export declare type ThemeButtonType = keyof typeof ThemeButtonTypes;
export interface ThemeButtonStyles {
    color?: Color;
    bg?: Color;
    border?: ThemeBorder;
    shadow?: string;
    decoration?: string;
}
export interface ThemeButtonStylesByStates {
    default?: ThemeButtonStyles;
    hover?: ThemeButtonStyles;
    active?: ThemeButtonStyles;
    disabled?: ThemeButtonStyles & {
        opacity?: number;
    };
    focus?: {
        shadow?: string;
        decoration?: string;
    };
}
export interface ThemeButton {
    sizes?: {
        default?: ThemeButtonSize;
        sm?: ThemeButtonSize;
        lg?: ThemeButtonSize;
    };
    variants?: {
        [key in ThemeButtonType]?: ThemeButtonStylesByStates;
    };
    icon?: {
        spacing?: Size;
        size?: Size | 'auto';
    };
}
export interface ThemeCard {
    spacer?: {
        y?: Size;
        x?: Size;
    };
    border?: ThemeBorder;
    borderRadius?: Size;
    innerBorderRadius?: Size;
    capBg?: Color;
    bg?: Color;
    img?: {
        margin?: Size;
        overlayPadding?: Size;
    };
    groupMargin?: Size;
    deckMargin?: Size;
    columns?: {
        count?: number;
        gap?: Size;
        margin?: Size;
    };
}
export interface ThemeClose {
    fontSize?: Size;
    fontWeight?: string;
    color?: Color;
    textShadow?: string;
}
export interface ThemeDropdown {
    minWidth: Size;
    paddingY: Size;
    spacer: Size;
    bg: Color;
    border: ThemeBorder;
    borderRadius: Size;
    divider?: {
        bg?: Color;
    };
    boxShadow?: string;
    link?: {
        color?: Color;
        hoverColor?: Color;
        hoverBg?: Color;
        activeColor?: Color;
        activeBg?: Color;
        checkIconColor?: Color;
        checkIconGutter?: Size;
        disabledColor?: Color;
    };
    item?: {
        paddingX?: Size;
        paddingY?: Size;
    };
    header?: {
        color?: Color;
    };
}
export interface ThemeForm {
    group?: {
        marginBottom?: Size;
    };
    text?: {
        marginTop?: Size;
    };
    check?: {
        inline?: {
            marginX?: string;
            inputMarginX?: string;
        };
    };
}
interface _SwitchBase {
    size?: Size;
    border?: ThemeBorder;
    bg?: Color;
    hover?: {
        borderColor?: Color;
    };
    checked?: {
        color?: Color;
        bg?: Color;
    };
    focusColor?: Color;
    iconSize?: Size;
}
export interface ThemeInputCheckbox extends _SwitchBase {
}
export interface ThemeInputRadio extends _SwitchBase {
}
export interface ThemeInputSwitch {
    width?: Size;
    height?: Size;
    bg?: Color;
    checkedBg?: Color;
    slider?: {
        gap?: Size;
        height?: Size | 'auto';
        width?: Size | 'auto';
        color?: Color;
    };
}
export interface ThemeInputSlider {
    sizes?: {
        default?: {
            height?: Size;
        };
        sm?: {
            height?: Size;
        };
        lg?: {
            height?: Size;
        };
    };
    bg?: Color;
    borderRadius?: string;
    progress?: {
        bg?: Color;
    };
    thumb?: {
        sizes?: {
            defaultSize?: Size;
            smSize?: Size;
            lgSize?: Size;
        };
        bg?: Color;
        borderColor?: Color;
    };
}
export interface ThemeInput {
    bg?: Color;
    disabledBg?: Color;
    color?: Color;
    border?: ThemeBorder;
    boxShadow?: string;
    focus?: {
        bg?: Color;
        borderColor?: Color;
        color?: Color;
        width?: Size;
        boxShadow?: string;
    };
    placeHolderColor?: Color;
    plaintextColor?: Color;
    sizes?: {
        default?: ThemeInputSize;
        sm?: ThemeInputSize;
        lg?: ThemeInputSize;
    };
    transition?: string;
    checkbox?: ThemeInputCheckbox;
    radio?: ThemeInputRadio;
    switch?: ThemeInputSwitch;
    slider?: ThemeInputSlider;
}
export interface ThemeInputGroup {
    addon?: {
        color?: Color;
        bg?: Color;
        borderColor?: Color;
    };
}
export interface ThemeListGroup {
    fontSize?: Size;
    bg?: Color;
    border?: ThemeBorder;
    borderRadius?: Size;
    item?: {
        paddingY?: Size;
        paddingX?: Size;
    };
    hover?: {
        bg?: Color;
    };
    active?: {
        color?: Color;
        bg?: Color;
        borderColor?: Color;
    };
    disabled?: {
        color?: Color;
        bg?: Color;
    };
    action?: {
        color?: Color;
        hover?: {
            color?: Color;
        };
        active?: {
            color?: Color;
            bg?: Color;
        };
    };
}
export interface ThemeModal {
    innerPadding?: Size;
    transition?: string;
    dialog?: {
        margin?: Size;
        marginYSmUp?: Size;
    };
    title?: {
        lineHeight?: LineHeight;
    };
    content?: {
        bg?: Color;
        border?: ThemeBorder;
        borderRadius?: string;
        boxShadow?: string;
        boxShadowSmUp?: string;
    };
    backdrop?: {
        bg?: Color;
        opacity?: number;
    };
    header?: {
        border?: ThemeBorder;
        paddingY?: Size;
        paddingX?: Size;
    };
    footer?: {
        border?: ThemeBorder;
    };
    sizes?: {
        lg?: Size;
        md?: Size;
        sm?: Size;
    };
}
export declare enum ThemeNavTypes {
    'default' = "DEFAULT",
    'underline' = "UNDERLINE",
    'tabs' = "TABS",
    'pills' = "PILLS"
}
export declare type ThemeNavType = keyof typeof ThemeNavTypes;
export interface ThemeNavItem {
    size?: ThemeButtonSize;
    gutter?: Size;
    icon?: {
        spacing?: Size;
    };
}
export interface ThemeNavVariant extends ThemeNavItem {
    bg?: Color;
    border?: ThemeBorder;
    item?: ThemeButtonStylesByStates;
}
export declare type ThemeNavVariants = {
    [key in ThemeNavType]?: ThemeNavVariant;
};
export interface ThemeNav {
    variants?: ThemeNavVariants;
}
export interface ThemeNavBar {
    fontSize?: Size;
    paddingY?: Size;
    paddingX?: Size;
    nav?: {
        link?: {
            paddingX?: Size;
            height?: Size | 'auto';
        };
    };
    brand?: {
        fontSize?: Size;
        height?: Size | 'auto';
        paddingY?: Size | 'auto';
    };
    toggler?: {
        paddingY?: Size;
        paddingX?: Size;
        fontSize?: Size;
        borderRadius?: Size;
    };
    themes?: {
        dark?: {
            color?: Color;
            hoverColor?: Color;
            activeColor?: Color;
            disabledColor?: Color;
            toggler?: {
                iconBg?: string;
                borderColor?: Color;
            };
        };
        light?: {
            color?: Color;
            hoverColor?: Color;
            activeColor?: Color;
            disabledColor?: Color;
            toggler?: {
                iconBg?: string;
                borderColor?: Color;
            };
        };
    };
}
export interface ThemePagination {
    color?: Color;
    bg?: Color;
    border?: ThemeBorder;
    lineHeight?: LineHeight;
    gutter?: Size;
    focus?: {
        boxShadow?: string;
        outline?: Size;
    };
    hover?: {
        color?: Color;
        bg?: Color;
        borderColor?: Color;
    };
    active?: {
        color?: Color;
        bg?: Color;
        borderColor?: Color;
    };
    disabled?: {
        color?: Color;
        bg?: Color;
        borderColor?: Color;
    };
    sizes?: {
        default?: {
            paddingY?: Size;
            paddingX?: Size;
        };
        sm?: {
            paddingY?: Size;
            paddingX?: Size;
        };
        lg?: {
            paddingY?: Size;
            paddingX?: Size;
        };
    };
    item?: {
        minWidth?: Size;
        borderRadius?: Size;
    };
}
export interface ThemePopper {
    fontSize?: Size;
    bg?: Color;
    border?: ThemeBorder;
    borderRadius?: Size;
    boxShadow?: string;
    header?: {
        bg?: Color;
        color?: Color;
        paddingY?: Size;
        paddingX?: Size;
    };
    body?: {
        color?: Color;
        paddingY?: Size;
        paddingX?: Size;
    };
    arrow?: {
        size?: Size;
        color?: Color;
        outerColor?: Color;
    };
}
export interface ThemePaper {
    bg?: Color;
    color?: Color;
    border?: ThemeBorder;
    paddingY?: Size;
    paddingX?: Size;
    boxShadow?: string;
}
export interface ThemeProgress {
    height?: Size;
    fontSize?: Size;
    bg?: Color;
    borderRadius?: Size;
    boxShadow?: string;
    bar?: {
        color?: Color;
        bg?: Color;
        animationTiming?: string;
        transition?: string;
    };
}
export interface ThemeTooltip {
    fontSize?: Size;
    maxWidth?: Size;
    color?: Color;
    bg?: Color;
    borderRadius?: Size;
    boxShadow?: string;
    opacity?: number;
    paddingY?: Size;
    paddingX?: Size;
    margin?: Size;
    arrow?: {
        size?: Size;
        color?: Color;
    };
}
export interface ThemeComponents {
    alert?: ThemeAlert;
    badge?: ThemeBadge;
    breadcrumb?: ThemeBreadcrumb;
    button?: ThemeButton;
    card?: ThemeCard;
    close?: ThemeClose;
    dropdown?: ThemeDropdown;
    form?: ThemeForm;
    input?: ThemeInput;
    inputGroup?: ThemeInputGroup;
    listGroup?: ThemeListGroup;
    modal?: ThemeModal;
    nav?: ThemeNav;
    navbar?: ThemeNavBar;
    pagination?: ThemePagination;
    popper?: ThemePopper;
    paper?: ThemePaper;
    progress?: ThemeProgress;
    tooltip?: ThemeTooltip;
    [component: string]: any;
}
export declare type IMThemeTypography = ImmutableObject<ThemeTypography>;
export declare type IMThemeThemeColors = ImmutableObject<ThemeThemeColors>;
export declare type IMThemeAllColors = ImmutableObject<ThemeAllColors>;
export declare type IMThemeColorationVariants = ImmutableObject<ThemeColorationVariants>;
export declare type IMThemeFontStyleBase = ImmutableObject<ThemeFontStyleBase>;
export declare type IMThemeFontStyleGroup = ImmutableObject<ThemeFontStyleGroup>;
export declare type IMThemeSizes = ImmutableObject<ThemeSizes>;
export declare type IMThemeBorder = ImmutableObject<ThemeBorder>;
export declare type IMThemeBorderRadiuses = ImmutableObject<ThemeBorderRadiuses>;
export declare type IMThemeBoxShadows = ImmutableObject<ThemeBoxShadows>;
export declare type IMThemeHeader = ImmutableObject<ThemeHeader>;
export declare type IMThemeFooter = ImmutableObject<ThemeFooter>;
export declare type IMThemeBody = ImmutableObject<ThemeBody>;
export declare type IMThemeLink = ImmutableObject<ThemeLink>;
export declare type IMThemeComponents = ImmutableObject<ThemeComponents>;
export interface ThemeVariables {
    darkTheme?: boolean;
    coloration?: ThemeColorationType;
    colorationVariants?: IMThemeColorationVariants;
    typography?: IMThemeTypography;
    colors?: IMThemeAllColors;
    spacer?: Size;
    sizes?: IMThemeSizes;
    gutters?: IMThemeSizes;
    borderRadiuses?: IMThemeBorderRadiuses;
    boxShadows?: IMThemeBoxShadows;
    header?: IMThemeHeader;
    footer?: IMThemeFooter;
    body?: IMThemeBody;
    link?: IMThemeLink;
    border?: IMThemeBorder;
    components?: IMThemeComponents;
}
export declare type IMThemeVariables = ImmutableObject<ThemeVariables>;
export interface ThemeVariablesGenerator {
    darkTheme: boolean;
    coloration: ThemeColorationType;
    colorationVariants?: IMThemeColorationVariants;
    colors: ThemeAllColors;
    typography: ThemeTypography;
    spacer: Size;
    sizes: IMThemeSizes;
    gutters: IMThemeSizes;
    border: IMThemeBorder;
    borderRadiuses: IMThemeBorderRadiuses;
    boxShadows: IMThemeBoxShadows;
    body: IMThemeBody;
    link: IMThemeLink;
    generate: (themeVariables: IMThemeVariables) => IMThemeVariables;
}
export interface ThemeInfo {
    name?: string;
    label?: string;
    uri?: string;
    colors?: IMThemeThemeColors;
    font?: IMThemeFontStyleBase;
}
export declare type IMThemeInfo = ImmutableObject<ThemeInfo>;
