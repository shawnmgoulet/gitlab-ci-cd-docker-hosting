import * as React from 'react';
import { ImmutableObject } from 'seamless-immutable';
import { IMAppConfig, WidgetJson, IMWidgetJson } from './app-config';
import { RuntimeInfo, IMRuntimeInfos, IMUser } from './state';
import { UrlParameters } from './url-parameters';
import { IntlShape } from 'react-intl';
import { IMThemeVariables } from './theme';
import { RepeatedDataSource } from '../repeat-data-source-context';
import * as jimuForBuilderModules from 'jimu-for-builder';
import { LayoutEntry } from 'jimu-layouts/layout-builder';
export declare type AllWidgetProps<T> = WidgetProps & WidgetInjectedProps<T>;
export interface WidgetProps extends React.HTMLAttributes<HTMLDivElement> {
    layoutId?: string;
    layoutItemId?: string;
}
/** @ignore */
export interface ControllerWidgetProps {
    widgetsJson: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
    widgetsRuntimeInfo: IMRuntimeInfos;
}
/** @ignore */
export declare type IMControllerWidgetProps = ImmutableObject<ControllerWidgetProps>;
export declare type WidgetInjectedProps<T> = Omit<IMWidgetJson, 'config'> & RuntimeInfo & ControllerWidgetProps & {
    dispatch: any;
    config: T;
    queryObject: ImmutableObject<UrlParameters>;
    portalUrl?: string;
    portalSelf?: any;
    locale: string;
    intl: IntlShape;
    theme: IMThemeVariables;
    appI18nMessages: any;
    /**
     * Widget can have internal state, and widget can put the state in this.state, or put the state
     * in app store by publish `widgetStatePropChange` action, the state props are mapped here.
     *
     * When should widget put state here?
     *  * Widget may have: main widget class, extensions, message actions, data actions, and all of these
     *    components may need to communicate. In this case, widget can put the shared data here to communicate
     *  * Widget can put any data here if it doesn't want to put in this.state.
     *
     */
    stateProps: any;
    repeatedDataSource: RepeatedDataSource | RepeatedDataSource[];
    /**
     * some objects are hard to make them mutable, we can put them here.
     */
    mutableStateProps: any;
    /**
     * this props is used to trigger re-render when the props are changed, because the props may be mutable, which can't trigger re-render.
     */
    mutableStatePropsVersion: number;
    token?: string;
    user?: IMUser;
    builderSupportModules?: {
        jimuForBuilderLib: typeof jimuForBuilderModules;
        LayoutEntry?: typeof LayoutEntry;
        widgetModules?: any;
    };
};
export interface LayoutStateProps {
    dispatch?: any;
    appConfig?: IMAppConfig;
    widgetsRuntimeInfo?: IMRuntimeInfos;
}
