import { ImmutableObject } from 'seamless-immutable';
import { WidgetManifest, ThemeManifest } from './manifest';
import { JimuFieldType, BoundingBox, Size, BrowserSizeMode } from './common';
import { LayoutItemType, LayoutType } from './layout';
import { Analytics } from './tracking-manager';
import { FieldType } from '@esri/arcgis-rest-types';
import { ThemeVariables } from './theme';
import { MessageType } from '../message/message-base-types';
import { SqlExpression, OrderByOption } from './sql-expression';
/**
 * @ignore
 * This is an internal concept. A container has a layout object (size mode layout) to hold widget/widget.
 *
 * A widget has layout/layout widget is not a container, it's a parent widget of the widget in it's layout
 */
export declare enum ContainerType {
    Page = "pages",
    View = "views",
    Dialog = "dialogs",
    Header = "header",
    Footer = "footer"
}
export interface WidgetContext {
    /**
    * absolute URL point to widget folder, like this: http://.../widgets/abc/
    *
    * If you need to use fetch to load something in widget, you can use this *folderUrl*.
    * If you need to use systemjs to load some modules, please use *uri*.
    */
    folderUrl: string;
    isRemote: boolean;
}
export interface IconProps {
    filename: string;
    originalName?: string;
    path?: Array<string> | string;
    size?: number;
    color?: string;
    inlineSvg?: boolean;
}
export declare type IMIconProps = ImmutableObject<IconProps>;
export interface IconResult {
    svg: string;
    properties?: IconProps;
}
export declare type IMIconResult = ImmutableObject<IconResult>;
export interface LayoutItemJson {
    id?: string;
    bbox?: BoundingBox;
    type?: LayoutItemType;
    setting?: any;
    widgetId?: string;
    sectionId?: string;
    isPlaceholder?: boolean;
    isPending?: boolean;
}
export declare type IMLayoutItemJson = ImmutableObject<LayoutItemJson>;
export interface LayoutJson {
    id: string;
    order?: string[];
    type?: LayoutType;
    content?: {
        [layoutItemId: string]: LayoutItemJson;
    };
    setting?: any;
}
export interface AppLayouts {
    [layoutId: string]: LayoutJson;
}
export declare type IMAppLayouts = ImmutableObject<AppLayouts>;
export declare type IMLayoutJson = ImmutableObject<LayoutJson>;
export declare type IMWidgetContext = ImmutableObject<WidgetContext>;
export interface UseDataSource {
    dataSourceId: string;
    /**
     * the data source a widget uses may be a sub data source of another data source,
     * so we save the root data source id here so we can create data source easily.
     */
    rootDataSourceId?: string;
    /**
     * jimu field name array.
     * If no fields, means the widget can work with any field.
     */
    fields?: string[];
    query?: {
        where?: SqlExpression;
        orderBy?: OrderByOption[];
    };
    /**
     * key is used in the expression, such as {$key.fieldName}
     */
    key?: string;
}
export declare type IMUseDataSource = ImmutableObject<UseDataSource>;
export declare const DEFAULT_EMBED_LAYOUT_NAME = "DEFAULT";
export interface WidgetJson {
    id: string;
    icon: IconResult | string;
    label: string;
    visible: boolean;
    index?: number;
    config: any;
    uri: string;
    itemId?: string;
    context: WidgetContext;
    manifest: WidgetManifest;
    _originManifest: WidgetManifest;
    version: string;
    useDataSources?: UseDataSource[];
    useMapWidgetIds?: string[];
    useDataSourcesEnabled?: boolean;
    outputDataSources?: string[];
    widgets?: string[];
    /**
    * for widget that has embedded layout, the name is declared in manifest.json.
    * If there is not layouts declared in manifest, the name is default.
    */
    layouts?: {
        [name: string]: SizeModeLayoutJson;
    };
    style?: {
        border?: any;
        borderRadius?: any;
        boxShadow?: any;
    };
}
export declare type IMWidgetJson = ImmutableObject<WidgetJson>;
export interface SizeModeLayoutJson {
    [sizeMode: string]: string;
}
export declare type IMSizeModeLayoutJson = ImmutableObject<SizeModeLayoutJson>;
/**
 * @ignore
 */
export interface AbstractContainerJson {
    layout: SizeModeLayoutJson;
    backgroundColor?: string;
    backgroundIMage?: string;
    backgroundPosition?: string;
}
/** @ignore */
export declare type IMAbstractContainerJson = ImmutableObject<AbstractContainerJson>;
export interface SectionJson {
    id: string;
    visible?: boolean;
    label: string;
    icon: IconResult | string;
    views: string[];
    style?: {
        border?: any;
        borderRadius?: any;
        boxShadow?: any;
    };
}
export declare type IMSectionJson = ImmutableObject<SectionJson>;
export interface ViewJson extends AbstractContainerJson {
    id: string;
    label: string;
    mobileTheresholdSize?: {
        width?: number;
        height?: number;
    };
    desktopThresholdSize?: {
        width?: number;
        height?: number;
    };
    mobileLayoutCompact?: string;
    desktopLayoutCompact?: string;
}
export declare type IMViewJson = ImmutableObject<ViewJson>;
export interface SizeModePageHeightInBuilder {
    [sizeMode: string]: number;
}
export declare enum PageType {
    Normal = "NORMAL",
    Folder = "FOLDER",
    Link = "LINK"
}
export declare enum PageMode {
    /**
     * width, height = 100%
     */
    FitWindow = "FIT_WINDOW",
    /**
     * width = <>px | 100%,
     * height = auto
     */
    AutoScroll = "AUTO_SCROLL"
}
export interface PageJson extends AbstractContainerJson {
    id: string;
    label: string;
    type: PageType;
    icon: IconResult | string;
    header: boolean;
    footer: boolean;
    mode: PageMode;
    maxWidth?: number;
    heightInBuilder?: SizeModePageHeightInBuilder;
    linkUrl?: string;
    openTarget?: string;
    isVisible: boolean;
    isDefault: boolean;
    bodyBackgroundColor?: string;
    bodyBackgroundIMage?: string;
    bodyBackgroundPosition?: string;
}
export declare type IMPageJson = ImmutableObject<PageJson>;
export interface DialogJson extends AbstractContainerJson {
    id: string;
    label: string;
    icon: IconResult | string;
    width?: number | string;
    height?: number | string;
}
export declare type IMDialogJson = ImmutableObject<DialogJson>;
export interface HeaderJson extends AbstractContainerJson {
    height: {
        [sizeMode: string]: number | string;
    };
}
export declare type IMHeaderJson = ImmutableObject<HeaderJson>;
export interface FooterJson extends AbstractContainerJson {
    height: {
        [sizeMode: string]: number | string;
    };
}
export declare type IMFooterJson = ImmutableObject<FooterJson>;
/**
 * @ignore
 */
export declare type DateFormat = 'shortDate' | 'shortDateLE' | 'longMonthDayYear' | 'dayShortMonthYear' | 'longDate' | 'shortDateShortTime' | 'shortDateLEShortTime' | 'shortDateShortTime24' | 'shortDateLEShortTime24' | 'shortDateLongTime' | 'shortDateLELongTime' | 'shortDateLongTime24' | 'shortDateLELongTime24' | 'longMonthYear' | 'shortMonthYear' | 'year';
export interface FieldFormatSchema {
    dateFormat?: DateFormat;
    digitSeparator?: boolean;
    places?: number;
}
export declare type IMFieldFormatSchema = ImmutableObject<FieldFormatSchema>;
export interface FieldSchema {
    /**
     * widget should use this name to read data.
     * In fact, the jimuName is the field name when the first mapping is configured.
     */
    jimuName: string;
    type: JimuFieldType;
    esriType: FieldType;
    /**
     * this is the actual field name of the current data service.
     */
    name: string;
    alias?: string;
    description?: string;
    format?: FieldFormatSchema;
    /**
     * valid for output ds only. we'll use this info to refer to the origin data source when mapping. The field name in the array is jimuFieldName
     *    * If the datasource has only one origin dataSource, the array contains field names.
     *    * If the datasource has more than one origin dataSources, the array contains "dsId.fieldName".
     */
    originFields?: string[];
}
export declare type IMFieldSchema = ImmutableObject<FieldSchema>;
export interface DataSourceSchema {
    label?: string;
    childId?: string;
    jimuChildId?: string;
    idField?: string;
    fields?: {
        [jimuName: string]: FieldSchema;
    };
    filter?: string;
    /** for ds set
     * In fact, the jimuChildId is the child id when the first mapping is configured.
     */
    childSchemas?: {
        [jimuChildId: string]: DataSourceSchema;
    };
}
export declare type IMDataSourceSchema = ImmutableObject<DataSourceSchema>;
export interface ReversedDataSourceSchema {
    label?: string;
    childId?: string;
    jimuChildId?: string;
    fields?: {
        [fieldName: string]: FieldSchema[];
    };
    childSchemas?: {
        [childId: string]: ReversedDataSourceSchema[];
    };
}
export declare type IMReversedDataSourceSchema = ImmutableObject<ReversedDataSourceSchema>;
export interface DataSourceJson {
    id: string;
    label: string;
    type: string;
    url?: string;
    /**
    * For statistic datasource, we must save the schema in the config.
    */
    isStatistic?: boolean;
    portalUrl?: string;
    itemId?: string;
    layerId?: string;
    layers?: DataSourceLayerJson[];
    /**
     * valid for widget output datasource only. We don't use fields here
     */
    originDataSources?: UseDataSource[];
    /**
     * For configured data source:
     *    * If no mapping is configured, we dont' save DS schema here, the reason is: we can fetch the updated schema when schema is changed in other place, such as mapviewer, server
     *    * After mapping is configured:
     *      * For data source, we save the mapped fields info only, this means, for non-mapped fields, we still can fetch updating.
     *      * for data source set, we save the mapped layers only, this means, for non-mapped layers, we still can fetch updating.
     * For output data source:
     *    * If the schema is the same with the origin data source, we don't need to save schema. The widgets that use this data source will be referenced to the origin data source
     *    * If the schema is not the same, we must save the full schema, and use the schema mapping to find the origin data source's fields.
    */
    schema?: DataSourceSchema;
    data?: object[];
    /**
     * the fields used by the data source self, such as in filter, in sort
     */
    useFields?: string[];
    isOutputFromWidget?: boolean;
    query?: {
        where?: SqlExpression;
        orderBy?: OrderByOption[];
    };
    [key: string]: any;
}
export declare type IMDataSourceJson = ImmutableObject<DataSourceJson>;
export interface DataSourceLayerJson {
    id: string;
    title: string;
    url: string;
}
export declare type IMDataSourceLayerJson = ImmutableObject<DataSourceLayerJson>;
export interface ActionJson {
    actionId: string;
    description: string;
    widgetId: string;
    messageWidgetId: string;
    actionName: string;
    config: any;
}
export declare type IMActionJson = ImmutableObject<ActionJson>;
export interface MessageJson {
    id: string;
    widgetId: string;
    messageType: MessageType;
    actions: ActionJson[];
    /**
     * data sources used in all actions are saved here
     */
    useDataSources?: UseDataSource[];
}
export declare type IMMessageJson = ImmutableObject<MessageJson>;
export interface AttributesJson {
    title?: string;
    description?: string;
    tags?: string[];
    /**
     * These attributes are for arcgis only, so they should be defined in jimu-arcgis.
     * The reason that We put them here is: we can see a full app config in one place.
     */
    portalUrl?: string;
    clientId?: string;
    geometryService?: string;
}
export declare type IMAttributesJson = ImmutableObject<AttributesJson>;
/**
 * @ignore
 */
export interface HubJson {
    community: {
        portalUrl: string;
        orgId?: string;
    };
    initiative?: string;
}
/**
 * @ignore
 */
export declare type IMHubJson = ImmutableObject<HubJson>;
export declare type CustomThemeJson = Partial<ThemeVariables>;
export declare type IMCustomThemeJson = Partial<ImmutableObject<CustomThemeJson>>;
export interface ForBuilderAttributes {
    viewPortSize: {
        [browserSizeMode: string]: Size;
    };
    lockLayout: boolean;
}
export declare type IMForBuilderAttributes = ImmutableObject<ForBuilderAttributes>;
export interface AppConfig {
    template: string;
    mainSizeMode: BrowserSizeMode;
    pages: {
        [pageId: string]: PageJson;
    };
    pageStructure: [{
        [pageId: string]: string[];
    }];
    dialogs: {
        [dialogId: string]: DialogJson;
    };
    layouts: {
        [layoutId: string]: LayoutJson;
    };
    sections?: {
        [sectionId: string]: SectionJson;
    };
    views?: {
        [viewId: string]: ViewJson;
    };
    widgets: {
        [widgetId: string]: WidgetJson;
    };
    header: HeaderJson;
    footer: FooterJson;
    theme: string;
    customTheme?: CustomThemeJson;
    dataSources?: {
        [dsId: string]: DataSourceJson;
    };
    messageConfigs?: {
        [messageConfigId: string]: MessageJson;
    };
    /**
     * @ignore
     */
    hub?: HubJson;
    attributes: AttributesJson;
    analytics: Analytics;
    widgetsManifest: {
        [widgetUri: string]: WidgetManifest;
    };
    themeManifest?: ThemeManifest;
    preloadWidgets?: string[];
    exbVersion: string;
    __not_publish: boolean;
    forBuilderAttributes: ForBuilderAttributes;
}
export declare type IMAppConfig = ImmutableObject<AppConfig>;
export declare enum ConfigNodeType {
    Widget = "widgets",
    Page = "pages",
    Section = "sections",
    View = "views",
    Layout = "layouts",
    Dialog = "dialogs"
}
