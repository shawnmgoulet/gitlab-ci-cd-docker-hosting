import { ImmutableObject } from 'seamless-immutable';
export interface Expression {
    name: string;
    parts: ExpressionPart[];
}
export declare type IMExpression = ImmutableObject<Expression>;
export interface ExpressionMap {
    [expressionId: string]: Expression;
}
export declare type IMExpressionMap = ImmutableObject<ExpressionMap>;
export declare type ExpressionPartGoup = ExpressionPart & {
    parts?: ExpressionPartGoup[];
};
export interface ExpressionPart {
    type: ExpressionPartType;
    exp: string;
    dataSourceId?: string;
    jimuFieldName?: string;
}
export declare enum ExpressionPartType {
    Field = "FIELD",
    String = "STRING",
    Operator = "OPERATOR",
    Function = "FUNCTION",
    Number = "NUMBER",
    Unknown = "UNKNOWN"
}
export declare enum ExpressionFunctions {
    Average = "AVERAGE",
    Count = "COUNT",
    Sum = "SUM",
    Max = "MAX",
    Min = "MIN"
}
