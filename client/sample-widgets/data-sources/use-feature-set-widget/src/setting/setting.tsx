import {React, Immutable, IMFieldSchema, DataSource, DataSourceTypes, DataSourceManager, DataSourceComponent} from 'jimu-core';
import {BaseWidgetSetting} from 'jimu-for-builder';
import {FieldSelector} from 'jimu-ui/data-source-selector';
import {DataSourceSelector, SelectedDataSourceJson} from 'jimu-ui/data-source-selector';

export default class Setting extends BaseWidgetSetting{
  supportedTypes = Immutable([DataSourceTypes.FeatureSet]);
  dsManager = DataSourceManager.getInstance();

  onFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [{
        dataSourceId: this.props.useDataSources[0].dataSourceId,
        rootDataSourceId: this.props.useDataSources[0].rootDataSourceId,
        fields: [field.jimuName]
      }]
    })
  }

  onDataSourceSelected = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs: SelectedDataSourceJson) => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [{
        dataSourceId: currentSelectedDs.dataSourceJson && currentSelectedDs.dataSourceJson.id,
        rootDataSourceId: currentSelectedDs.rootDataSourceId
      }],
    });
  }

  onDataSourceRemoved = () => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [],
    });
  }

  render(){
    return <div className="use-feature-layer-setting p-2">
      <DataSourceSelector
        mustUseDataSource
        types={this.supportedTypes}
        selectedDataSourceIds={this.props.useDataSources && Immutable(this.props.useDataSources.map(ds => ds.dataSourceId))}
        useDataSourcesEnabled={this.props.useDataSourcesEnabled}
        onSelect={this.onDataSourceSelected} onRemove={this.onDataSourceRemoved}
      />

      <DataSourceComponent useDataSource={this.props.useDataSources && this.props.useDataSources[0]}>
        {
          (ds: DataSource) => 
          this.props.useDataSources && this.props.useDataSources.length > 0 &&
          <div className="mt-2">Please choose a Field to query:
            <FieldSelector 
              dataSources={[ds]}
              onSelect={this.onFieldSelected}
              selectedFields={this.props.useDataSources[0].fields || Immutable([])}
            />
          </div>
        }
      </DataSourceComponent>
      
    </div>
  }
}