import { React, Immutable, UseDataSource } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { AllDataSourceTypes, DataSourceSelector, SelectedDataSourceJson } from 'jimu-ui/data-source-selector';

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<any> & {}, {}>{
  supportedTypes = Immutable([AllDataSourceTypes.FeatureLayer, AllDataSourceTypes.FeatureQuery]);

  constructor(props) {
    super(props);
  }

  onToggleUseDataEnabled = (useDataSourcesEnabled: boolean) => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSourcesEnabled
    });
  }

  onDataSourceSelected = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs?: SelectedDataSourceJson) => {
    if(!allSelectedDss){
      return ;
    }
    const useDataSources: UseDataSource[] = allSelectedDss.map(ds => ({
      dataSourceId: ds.dataSourceJson && ds.dataSourceJson.id,
      rootDataSourceId: ds.rootDataSourceId
    }));

    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: useDataSources
    });
  }

  onDataSourceRemoved = () => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: []
    });
  }

  render() {
    let dataSourceId = null;
    if (this.props.useDataSources && this.props.useDataSources[0]) {
      dataSourceId = this.props.useDataSources[0].dataSourceId;
    }

    return <div className="w-100">
      <DataSourceSelector types={this.supportedTypes} selectedDataSourceIds={Immutable([dataSourceId])} useDataSourcesEnabled={this.props.useDataSourcesEnabled}
        onToggleUseDataEnabled={this.onToggleUseDataEnabled} onSelect={this.onDataSourceSelected} onRemove={this.onDataSourceRemoved}
      />
    </div>;
  }
}