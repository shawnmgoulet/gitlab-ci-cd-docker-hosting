/// <reference types="react" />
import { React, IMThemeVariables, IMThemeFontStyleGroup, IntlShape } from 'jimu-core';
interface FontSetSelectorProps {
    fontset?: IMThemeFontStyleGroup;
    theme?: IMThemeVariables;
    intl?: IntlShape;
    className?: string;
    dropdown?: {
        direction?: 'up' | 'down' | 'left' | 'right';
        appendTo?: HTMLElement | 'body';
    };
    onChange?: (fontset: IMThemeFontStyleGroup) => void;
    onCustomizeClick?: (e: any) => void;
}
export declare const FontSetSelector: React.FunctionComponent<FontSetSelectorProps>;
export {};
