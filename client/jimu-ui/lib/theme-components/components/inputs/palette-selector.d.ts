/// <reference types="react" />
import { React, IMThemeVariables, IMThemeThemeColors, IntlShape } from 'jimu-core';
interface PaletteSelectorProps {
    palette?: IMThemeThemeColors;
    theme?: IMThemeVariables;
    intl?: IntlShape;
    className?: string;
    dropdown?: {
        direction?: 'up' | 'down' | 'left' | 'right';
        appendTo?: HTMLElement | 'body';
    };
    onChange?: (palette: IMThemeThemeColors) => void;
    onCustomizeClick?: (e: any) => void;
}
export declare const PaletteSelector: React.FunctionComponent<PaletteSelectorProps>;
export {};
