/** @jsx jsx */
import { IMThemeAllColors, ThemeInfo, IMThemeFontStyleGroup, ThemeColorationType } from 'jimu-core';
export declare function getPalette(colors: Partial<IMThemeAllColors>): JSX.Element;
interface ColorationOption {
    value: ThemeColorationType;
    label: string;
    content: any;
}
export declare function getColorationOptions(colors: Partial<IMThemeAllColors>): ColorationOption[];
export declare function getFontSetCard(fontset: Partial<IMThemeFontStyleGroup>): JSX.Element;
export declare function getThemeTitleLabel(themeInfo: Partial<ThemeInfo>): JSX.Element;
export {};
