/// <reference types="react" />
import { React, IMThemeInfo, CustomThemeJson, IMThemeAllColors, IMThemeThemeColors, IntlShape } from 'jimu-core';
interface ThemeColorConfiguratorProps {
    themeInfo?: IMThemeInfo;
    intl?: IntlShape;
    className?: string;
    onChange?: (customVariables: CustomThemeJson) => void;
}
interface ThemeColorConfiguratorState {
    customColors: Partial<IMThemeThemeColors>;
    themeColors: IMThemeAllColors;
}
export declare class ThemeColorConfigurator extends React.PureComponent<ThemeColorConfiguratorProps, ThemeColorConfiguratorState> {
    private themeManager;
    constructor(props: any);
    onColorChange: (name: string, color: string) => void;
    updateThemeState(): void;
    componentDidMount(): void;
    componentDidUpdate(prevProps: ThemeColorConfiguratorProps, prevState: ThemeColorConfiguratorState): void;
    render(): JSX.Element;
}
export {};
