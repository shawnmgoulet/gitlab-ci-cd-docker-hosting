/// <reference types="react" />
import { React } from 'jimu-core';
export declare const ThemeComponentsConfigurator: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
