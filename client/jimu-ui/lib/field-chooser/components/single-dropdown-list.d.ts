/// <reference types="react" />
import { React, FieldSchema, IMFieldSchema, DataSource, ImmutableArray, ImmutableObject } from 'jimu-core';
interface Props {
    fields: ImmutableObject<{
        [jimuName: string]: FieldSchema;
    }>;
    ds: DataSource;
    selectedFields?: ImmutableArray<string>;
    onFieldClick: (f: IMFieldSchema) => void;
}
interface State {
    isOpen: boolean;
}
export default class SingleDropdownList extends React.PureComponent<Props, State> {
    constructor(props: any);
    toggle: () => void;
    getSelectedFieldAlias: () => string;
    render(): JSX.Element;
}
export {};
