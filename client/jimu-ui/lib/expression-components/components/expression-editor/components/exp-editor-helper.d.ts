/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, Expression, ExpressionPart, ExpressionFunctions, ImmutableArray, DataSource, IntlShape, DataSourceManager } from 'jimu-core';
interface Props {
    expression: Expression;
    dataSourceIds: ImmutableArray<string>;
    externalId: string;
    inEditablePart: boolean;
    container: HTMLElement;
    intl: IntlShape;
    onSelect: (e: Expression, partId: string) => void;
}
interface State {
    active: ExpEditorHelperTabs;
}
declare enum ExpEditorHelperTabs {
    Fields = "FIELDS",
    Functions = "FUNCTIONS"
}
export default class ExpEditorHelper extends React.PureComponent<Props, State> {
    dsManager: DataSourceManager;
    ExpEditorHelperTabs: {
        [ExpEditorHelperTabs.Fields]: string;
        [ExpEditorHelperTabs.Functions]: string;
    };
    constructor(props: any);
    getDataSources: () => DataSource[];
    getTab: (tab: ExpEditorHelperTabs) => JSX.Element;
    onFunctionSelect: (functionName: ExpressionFunctions) => void;
    onFieldSelect: (allSelectedFields: import("seamless-immutable").ImmutableObject<import("jimu-core").FieldSchema>[], field: import("seamless-immutable").ImmutableObject<import("jimu-core").FieldSchema>, ds: DataSource) => void;
    onActiveTabChange: (tab: ExpEditorHelperTabs) => void;
    getSelectionPartId: () => string;
    changeExpression: (parts: ExpressionPart[], expression: Expression) => void;
    render(): JSX.Element;
}
export {};
