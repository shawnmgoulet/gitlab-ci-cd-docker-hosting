export declare enum ExpressionFrom {
    Attribute = "ATTRIBUTE",
    Statistics = "STATISTICS",
    Expression = "EXPRESSION"
}
declare enum FromStatic {
    Static = "STATIC"
}
export declare const ExpressionInputFrom: {
    Attribute: ExpressionFrom.Attribute;
    Statistics: ExpressionFrom.Statistics;
    Expression: ExpressionFrom.Expression;
    Static: FromStatic.Static;
};
export declare type ExpressionInputFrom = FromStatic | ExpressionFrom;
export {};
