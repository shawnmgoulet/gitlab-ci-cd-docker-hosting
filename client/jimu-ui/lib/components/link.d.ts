/// <reference types="react" />
/** @jsx jsx */
import { jimuHistory, IMUrlParameters, React, LinkTo, ThemeVariables } from 'jimu-core';
import { ButtonProps } from './button';
export * from './_link-tip';
export declare type LinkTarget = '_self' | '_blank' | '_parent' | '_top';
export interface LinkProps {
    tag?: React.ElementType;
    onClick?: (evt: React.MouseEvent<HTMLLinkElement>) => void;
    target?: LinkTarget;
    replace?: boolean;
    history?: jimuHistory.History;
    to?: LinkTo;
    queryObject?: IMUrlParameters;
    innerRef?: React.Ref<HTMLElement>;
    title?: string;
    className?: string;
    customStyle?: CustromStyle;
    themeStyle?: ButtonProps;
}
interface CustromStyle {
    style?: React.CSSProperties;
    hoverStyle?: React.CSSProperties;
    visitedStyle?: React.CSSProperties;
}
export declare const isModifiedEvent: (event: any) => boolean;
export declare class _LinkComponent extends React.PureComponent<LinkProps & {
    theme: ThemeVariables;
}, {
    isPopperOpen: boolean;
}> {
    static count: number;
    href: string;
    id: string;
    static defaultProps: Partial<LinkProps>;
    constructor(props: any);
    isToWebAddress: () => boolean;
    isToTopWindow: () => boolean;
    stopGoingToOtherSite: () => boolean;
    handleClick: (event: any) => void;
    render(): JSX.Element;
}
declare const _Link: React.ComponentClass<any, any>;
declare const Link: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export { _Link, Link };
