/// <reference types="react" />
import { React } from 'jimu-core';
import { DraggableData, DraggableBounds, DraggableEventHandler, ControlPosition, PositionOffsetControlPosition } from 'react-draggable';
export declare type DraggableData = DraggableData;
export declare type DragAxis = 'both' | 'x' | 'y' | 'none';
interface Props {
    allowAnyClick?: boolean;
    axis?: DragAxis;
    bounds?: DraggableBounds | string | false;
    cancel?: string;
    enableUserSelectHack: boolean;
    defaultClassName?: string;
    defaultClassNameDragging?: string;
    defaultClassNameDragged?: string;
    defaultPosition?: ControlPosition;
    disabled?: boolean;
    grid?: [number, number];
    handle?: string;
    offsetParent?: HTMLElement;
    onMouseDown?: (e: MouseEvent) => void;
    onStart?: DraggableEventHandler;
    onDrag?: DraggableEventHandler;
    onStop?: DraggableEventHandler;
    position?: ControlPosition;
    positionOffset?: PositionOffsetControlPosition;
    scale?: number;
}
interface State {
    panning: boolean;
}
export declare class Draggable extends React.PureComponent<Props, State> {
    static defaultProps: Partial<Props>;
    constructor(props: any);
    preventTouceMove: (event: any) => void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleDragStart: (event: DragEvent, data: DraggableData) => void;
    handleDragStop: (event: DragEvent, data: DraggableData) => void;
    render(): JSX.Element;
}
export {};
