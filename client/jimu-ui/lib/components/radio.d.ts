import { SwitchBase, SwichBaseProps } from './_switchBase';
export declare class _Radio extends SwitchBase {
    static displayName: string;
    constructor(props: SwichBaseProps);
    render(): JSX.Element;
}
