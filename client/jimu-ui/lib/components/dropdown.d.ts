/// <reference types="react" />
import { React } from 'jimu-core';
import { DropdownProps as BSDropdownProps } from './reactstrap';
interface DropdownProps extends BSDropdownProps {
    toggle?: (evt?: any) => void;
    fluid?: boolean;
    selectable?: boolean;
}
export declare type Direction = 'up' | 'down' | 'left' | 'right';
export declare class _Dropdown extends React.PureComponent<DropdownProps> {
    getContextValue(): {
        toggle: (evt?: any) => void;
        isOpen: boolean;
        direction: "left" | "right" | "up" | "down";
        selectable: boolean;
    };
    render(): JSX.Element;
}
export declare const Dropdown: React.FunctionComponent<DropdownProps>;
export {};
