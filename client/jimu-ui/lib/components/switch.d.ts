import { SwitchBase } from './_switchBase';
export declare class _Switch extends SwitchBase {
    render(): JSX.Element;
}
export declare const Switch: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
