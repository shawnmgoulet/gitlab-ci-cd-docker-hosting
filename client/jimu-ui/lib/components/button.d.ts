/// <reference types="react" />
import { React, ThemeButtonType } from 'jimu-core';
import { ButtonProps as BSButtonProps } from './reactstrap';
declare type OmitBSButtonProps = Omit<BSButtonProps, 'color' | 'type' | 'outline'>;
export interface ButtonProps extends OmitBSButtonProps {
    type?: ThemeButtonType;
    icon?: boolean;
    htmlType?: 'submit' | 'reset' | 'button';
    forwardedRef?: any;
}
declare let _Button: React.ForwardRefExoticComponent<Pick<ButtonProps & BSButtonProps, React.ReactText> & React.RefAttributes<unknown>>;
declare const Button: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export { _Button, Button };
