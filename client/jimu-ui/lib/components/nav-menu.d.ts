/// <reference types="react" />
import { React } from 'jimu-core';
interface NavMenuProps extends React.HTMLAttributes<HTMLElement> {
    appendTo?: HTMLElement;
    appendToBody?: boolean;
    right?: boolean;
    className?: string;
    modifiers?: any;
    flip?: boolean;
    tag?: React.ElementType;
}
export declare class _NavMenu extends React.PureComponent<NavMenuProps> {
    static defaultProps: NavMenuProps;
    static contextType: React.Context<{
        ref?: HTMLElement;
        setRef?: (ref: HTMLElement) => void;
        toggle?: (e: any) => void;
        isOpen?: boolean;
        isDropdow?: boolean;
        direction?: string;
        menuOpenMode?: string;
        menuMode?: string;
        textAlign?: "left" | "right" | "center";
    }>;
    constructor(props: any);
    onDocumentClick(e: any): void;
    onLeave(e: any): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export declare const NavMenu: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
