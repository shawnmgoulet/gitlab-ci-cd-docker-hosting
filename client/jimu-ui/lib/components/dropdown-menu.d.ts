/// <reference types="react" />
import { React } from 'jimu-core';
import { Modifiers } from 'popper.js';
interface DropdownMenuProps extends React.HTMLAttributes<HTMLElement> {
    appendTo?: HTMLElement | 'body';
    alightment?: 'start' | 'end' | 'center';
    className?: string;
    modifiers?: Modifiers;
    showArrow?: boolean;
    flip?: boolean;
    tag?: React.ElementType;
    zIndex?: string;
}
export declare class _DropdownMenu extends React.PureComponent<DropdownMenuProps> {
    static defaultProps: DropdownMenuProps;
    static contextType: React.Context<{}>;
    constructor(props: any);
    onDocumentClick(e: any): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export declare const DropdownMenu: React.FunctionComponent<DropdownMenuProps>;
export {};
