import { SwitchBase } from './_switchBase';
export declare class _Checkbox extends SwitchBase {
    render(): JSX.Element;
}
