/// <reference types="react" />
import { React, ThemeButtonType } from 'jimu-core';
import { DropdownToggleProps as BSDropdownToggleProps } from './reactstrap';
interface DropdownToggleProps extends BSDropdownToggleProps {
    type?: ThemeButtonType;
    icon?: boolean;
    htmlType?: 'submit' | 'reset' | 'button';
}
export declare class _DropdownToggle extends React.PureComponent<DropdownToggleProps> {
    render(): JSX.Element;
}
export declare const DropdownToggle: React.FunctionComponent<DropdownToggleProps>;
export {};
