/// <reference types="react" />
/** @jsx jsx */
import { React, ReactRedux, ThemeVariables } from 'jimu-core';
interface Props {
    imageParam: ImageParam;
    toolTip?: string;
    altText?: string;
    useFadein?: boolean;
    theme?: ThemeVariables;
    onImageLoaded?: (imageWidth: number, imageHeight: number) => void;
}
export declare enum ImgSourceType {
    ByURL = "BY_URL",
    ByUpload = "BY_UPLOAD"
}
export interface ImageParam {
    url?: string;
    originalId?: string;
    originalUrl?: string;
    fileName?: string;
    originalName?: string;
    fileFormat?: string;
    imgSourceType?: ImgSourceType;
    cropParam?: CropParam;
}
declare enum ImgLoadState {
    Loading = "LOADING",
    LoadOk = "LOADOK",
    LoadError = "LOADERROR"
}
interface States {
    picLoadResult: ImgLoadState;
    imageWidth?: number;
    imageHeight?: number;
    widgetWidth?: number;
    widgetHeight?: number;
}
export declare enum CropType {
    Real = "REAL",
    Fake = "FAKE"
}
export interface CropPosition {
    x: number;
    y: number;
}
interface CropPixel {
    x?: number;
    y?: number;
    width?: number;
    height?: number;
}
export interface CropParam {
    cropPosition?: CropPosition;
    cropZoom?: number;
    svgViewBox?: string;
    svgPath?: string;
    cropShape?: string;
    cropPixel?: CropPixel;
    cropType?: CropType;
}
interface ExtraProps {
    appPath: string;
    queryObject: any;
}
export declare class _ImageWithParam extends React.PureComponent<Props & ExtraProps, States> {
    imgObject: any;
    maskId: string;
    __unmount: boolean;
    constructor(props: any);
    getStyle(): import("jimu-core").SerializedStyles;
    static defaultProps: {
        imageParam: {};
    };
    componentDidMount(): void;
    componentWillUnmount(): void;
    preloadImage: (url: string) => void;
    getSnapshotBeforeUpdate(prevProps: Props): boolean;
    componentDidUpdate(prevProps: Props & ExtraProps, prevState: States, snapshot: any): void;
    standardUrl: (url: string) => string;
    onResize: (width: any, height: any) => void;
    render(): JSX.Element;
}
export declare const ImageWithParam: ReactRedux.ConnectedComponentClass<React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & ExtraProps & React.RefAttributes<_ImageWithParam>, "ref" | "theme" | "key" | "appPath" | "queryObject" | "toolTip" | "altText" | "useFadein" | "onImageLoaded"> & Partial<Pick<Props & ExtraProps & React.RefAttributes<_ImageWithParam>, "imageParam">> & Partial<Pick<{
    imageParam: {};
}, never>>, "theme">>, Pick<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & ExtraProps & React.RefAttributes<_ImageWithParam>, "ref" | "theme" | "key" | "appPath" | "queryObject" | "toolTip" | "altText" | "useFadein" | "onImageLoaded"> & Partial<Pick<Props & ExtraProps & React.RefAttributes<_ImageWithParam>, "imageParam">> & Partial<Pick<{
    imageParam: {};
}, never>>, "theme">, "ref" | "theme" | "key" | "imageParam" | "toolTip" | "altText" | "useFadein" | "onImageLoaded"> & Props>;
export {};
