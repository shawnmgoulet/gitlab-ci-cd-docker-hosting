/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { DraggableData } from 'react-draggable';
interface OwnProps {
    className?: string;
    width?: number;
    height?: number;
    /**
     * Minimum size used to limit resize
     */
    minSize?: number[];
    onEnd?: (width: number, height: number) => void;
    onResize?: (width: number, height: number) => void;
    onStart?: (width: number, height: number) => void;
}
declare type DraggableCoreProps = {
    allowAnyClick?: boolean;
    disabled?: boolean;
    enableUserSelectHack?: boolean;
    offsetParent?: HTMLElement;
    grid?: [number, number];
    onMouseDown?: (e: MouseEvent) => void;
    scale?: number;
};
declare type Props = DraggableCoreProps & OwnProps;
interface ExtraProps {
    theme: ThemeVariables;
}
interface State {
    panning: boolean;
}
export declare class _Resizeable extends React.PureComponent<Props & ExtraProps, State> {
    static defaultProps: Partial<Props>;
    constructor(props: any);
    getStyle: () => import("jimu-core").SerializedStyles;
    onResizeStart: () => void;
    onResize: (e: any, data: DraggableData) => void;
    onResizeEnd: () => void;
    preventTouceMove: (event: any) => void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export declare const Resizeable: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<DraggableCoreProps & OwnProps & React.RefAttributes<_Resizeable>, "ref" | "key"> & Partial<Pick<DraggableCoreProps & OwnProps & React.RefAttributes<_Resizeable>, "className" | "height" | "scale" | "width" | "grid" | "disabled" | "onMouseDown" | "allowAnyClick" | "enableUserSelectHack" | "offsetParent" | "onStart" | "minSize" | "onEnd" | "onResize">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
