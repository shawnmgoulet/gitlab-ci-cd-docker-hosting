/// <reference types="react" />
/** @jsx jsx */
import { InputProps } from './reactstrap';
import { React } from 'jimu-core';
interface SliderProps extends InputProps {
}
interface SliderStates {
    value: string | string[] | number;
}
export declare class _Slider extends React.PureComponent<SliderProps, SliderStates> {
    constructor(props: SliderProps);
    onChange(evt: any): void;
    private calcRatio;
    render(): JSX.Element;
}
export declare const Slider: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
