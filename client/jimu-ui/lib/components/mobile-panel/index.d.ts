/// <reference types="react" />
import { React } from 'jimu-core';
import MobilePanelManager from './mobile-panel-manager';
interface Props {
    title?: string;
    open?: boolean;
    children?: React.ReactNode;
    className: string;
    toggle: (evt?: any) => void;
}
export declare enum ExpandStage {
    INITSCREEN = "initscreen",
    HALFSCREEN = "halfscreen",
    FULLSCREEN = "fullscreen"
}
interface States {
    windowHeight: number;
    currentExpandStage: ExpandStage;
}
export declare class _MobilePanel extends React.PureComponent<Props, States> {
    id: string;
    container: HTMLElement;
    currentBottomPanelHeight: number;
    resizeTimeout: any;
    startDrag: boolean;
    moveY: number;
    startY: number;
    sliding: boolean;
    _isMounted: boolean;
    constructor(props: any);
    getExpectedHeightForStage: (currentStage: ExpandStage) => number;
    componentDidMount(): void;
    resize: () => void;
    actualResize: () => void;
    componentDidUpdate(prevProps: Props, prevState: States): void;
    sendToggle: () => void;
    componentWillUnmount(): void;
    start: (event: any, type: any) => void;
    preventTouceMove: (event: any) => void;
    move: (event: any, type: any) => void;
    end: (event: any, type: any) => void;
    handleClickCloseBtn: (evt: any) => void;
    getPanelContent: () => JSX.Element;
    render(): React.ReactPortal;
}
export declare const MobilePanel: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export { MobilePanelManager };
