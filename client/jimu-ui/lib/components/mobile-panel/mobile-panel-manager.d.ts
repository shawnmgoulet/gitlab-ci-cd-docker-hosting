import { _MobilePanel, ExpandStage } from './index';
declare global {
    interface Window {
        _mobilePanelManager: any;
    }
}
export default class MobilePanelManager {
    static instance: MobilePanelManager;
    static getInstance(): MobilePanelManager;
    private mobilePanels;
    private currentExpandStage;
    private currentId;
    setCurrentId(currentId: string): void;
    getCurrentId(): string;
    setCurrentExpandStage(currentExpandStage: ExpandStage): void;
    getCurrentExpandStage(): ExpandStage;
    addMobilePanel(mobilePanel: _MobilePanel): void;
    removeMobilePanel(mobilePanel: _MobilePanel): void;
    getMobilePanelById(id: string): _MobilePanel;
    closePanel(): void;
    checkDomIsContained(dom: HTMLElement): boolean;
}
