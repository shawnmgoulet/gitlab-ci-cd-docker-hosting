/// <reference types="react" />
import { React } from 'jimu-core';
import { Placement, Modifiers, PopperOptions, Data } from 'popper.js';
import { VirtualReference } from './virtual-reference';
export declare type PopperData = Data;
export declare type TargetType = string | HTMLElement | VirtualReference | Function | React.RefObject<any>;
export interface PopperProps {
    /**
     * This is the DOM element, or a function that returns the DOM element,
     * that may be used to set the position of the popover.
     * The return value will passed as the reference object of the Popper
     * instance.
     */
    reference: TargetType;
    /**
     * Popper render function or node.
     */
    /**
     * A node, component instance, or function that returns either.
     * The `container` will passed to the Modal component.
     * By default, it uses the body of the reference's top-level document object,
     * so it's simply `document.body` most of the time.
     */
    container: TargetType;
    /**
     * Always keep the children in the DOM.
     */
    keepMounted?: boolean;
    /**
     * The offset modifier can shift your popper on both its axis.
     * To learn how to use offset, [modifiers.offset documentation](https://github.com/FezVrasta/popper.js/blob/master/docs/_includes/popper-documentation.md#modifiers..offset).
     */
    offset?: number[];
    /**
     * Clicking on the node of toggleClass will not trigger the toggle function
     */
    toggleClass?: string;
    /**
     * When you click on an area outside popper(and toggleClass node), this function is triggered
     */
    toggle?: (evt?: React.MouseEvent<any> | React.TouchEvent<any>) => any;
    /**
     * A modifier is a function that is called each time Popper.js needs to
     * compute the position of the popper.
     * For this reason, modifiers should be very performant to avoid bottlenecks.
     * To learn how to create a modifier, [read the modifiers documentation](https://github.com/FezVrasta/popper.js/blob/master/docs/_includes/popper-documentation.md#modifiers--object).
     */
    modifiers?: Modifiers;
    /**
     * If `true`, the popper is visible.
     */
    open: boolean;
    /**
     * Popper placement.
     */
    placement: Placement;
    /**
     * Options provided to the [`popper.js`](https://github.com/FezVrasta/popper.js) instance.
     */
    popperOptions?: PopperOptions;
    /**
     *  When this value changes, call scheduleUpdate to recalculate the position
     */
    generation?: number;
    zIndex?: number;
    className?: string;
    onClick?: (evt: React.MouseEvent<HTMLDivElement>) => void;
    onMouseDown?: (evt: React.MouseEvent<HTMLDivElement>) => void;
    onCreate?: (data?: PopperData) => void;
}
/**
 * Poppers rely on the 3rd party library [Popper.js](https://github.com/FezVrasta/popper.js) for positioning.
 */
export declare class _Popper extends React.Component<PopperProps> {
    static defaultProps: Partial<PopperProps>;
    popper: any;
    scheduleUpdate: () => void;
    domNode: HTMLDivElement;
    componentDidMount(): void;
    addTargetEvents(): void;
    handleDocumentClick: (evt: React.MouseEvent<any, MouseEvent>) => void;
    _isOut: (target: any, node: any) => boolean;
    componentDidUpdate(prevProps: any): void;
    isSameReference: (ref1: TargetType, ref2: TargetType) => boolean;
    scheduleUpdatePopper: () => void;
    removeTargetEvents(): void;
    componentWillUnmount(): void;
    baseModifiers: () => {
        offset: {
            offset: string;
        };
        preventOverflow: {
            enabled: boolean;
            boundariesElement: HTMLElement;
        };
    };
    handleOpen: () => void;
    handleClose: () => void;
    render(): JSX.Element;
}
export declare const Popper: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
