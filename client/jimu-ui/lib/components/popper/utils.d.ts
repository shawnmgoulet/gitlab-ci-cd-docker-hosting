export declare const canUseDOM: () => boolean;
export declare const isVirtalReference: (target: any) => boolean;
export declare const getTarget: (target: any) => HTMLElement;
export declare const flipPlacement: (placement: any) => any;
