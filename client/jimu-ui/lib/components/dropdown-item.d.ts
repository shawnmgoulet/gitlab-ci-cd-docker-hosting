/// <reference types="react" />
import { React } from 'jimu-core';
import { DropdownItemProps as BSDropdownItemProps } from './reactstrap';
interface DropdownItemProps extends BSDropdownItemProps {
}
export declare class _DropdownItem extends React.PureComponent<DropdownItemProps> {
    static contextType: React.Context<{}>;
    render(): JSX.Element;
}
export declare const DropdownItem: React.FunctionComponent<DropdownItemProps>;
export {};
