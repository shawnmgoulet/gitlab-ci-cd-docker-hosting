/// <reference types="react" />
import { React } from 'jimu-core';
import { LinkProps } from './link';
interface NavLinkProps extends LinkProps {
    tag?: React.ElementType;
    disabled?: boolean;
    active?: boolean;
    className?: string;
    href?: any;
    innerRef?: React.Ref<HTMLElement>;
    icon?: React.ComponentClass<React.SVGProps<SVGElement>> | string;
    iconPosition?: 'start' | 'end' | 'above';
    caret?: boolean;
}
export declare class _NavLink extends React.PureComponent<NavLinkProps> {
    static contextType: React.Context<{
        ref?: HTMLElement;
        setRef?: (ref: HTMLElement) => void;
        toggle?: (e: any) => void;
        isOpen?: boolean;
        isDropdow?: boolean;
        direction?: string;
        menuOpenMode?: string;
        menuMode?: string;
        textAlign?: "left" | "right" | "center";
    }>;
    static defaultProps: NavLinkProps;
    constructor(props: any);
    onClick(e: any): void;
    onCaretClick(e: any): void;
    onEnter(e: any): void;
    onLeave(e: any): void;
    render(): JSX.Element;
}
export declare const NavLink: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
