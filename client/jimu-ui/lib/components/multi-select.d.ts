/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, ImmutableArray } from 'jimu-core';
export interface MultiSelectItem {
    value: string | number;
    label: string;
    render?: (item: MultiSelectItem) => any;
}
interface Props {
    items: ImmutableArray<MultiSelectItem>;
    theme?: ThemeVariables;
    values?: ImmutableArray<string | number>;
    defaultValues?: ImmutableArray<string | number>;
    className?: string;
    placeHolder?: string;
    activeIcon?: React.ReactElement;
    appendTo?: HTMLElement | 'body';
    size?: string;
    fluid?: boolean;
    displayByValues?: (values: Array<string | number>) => string;
    onClickItem?: (evt: React.MouseEvent, value: string | number, selectedValues: Array<string | number>) => void;
}
interface Stats {
    isOpen: boolean;
    values: ImmutableArray<string | number>;
}
export declare class _MultiSelect extends React.PureComponent<Props, Stats> {
    divRef: HTMLDivElement;
    constructor(props: any);
    componentDidUpdate(preProps: any): void;
    onDropDownToggle: (evt: any) => void;
    onItemClick: (evt: any, item: MultiSelectItem) => void;
    getValueLabel: (value: any) => React.ReactText;
    getShowText: () => string;
    render(): JSX.Element;
}
export declare const MultiSelect: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
