/// <reference types="react" />
import { React } from 'jimu-core';
export interface SVGIconProps {
    icon: string;
    size?: number | string;
    width?: number | string;
    height?: number | string;
    className?: string;
    style?: React.CSSProperties;
    color?: string;
    rotate?: number | string;
    flip?: 'horizontal' | 'vertical';
    options?: {
        currentColor?: boolean;
    };
}
interface SVGIconState {
    iconSrc: string;
}
export declare class _Icon extends React.PureComponent<SVGIconProps, SVGIconState> {
    svgUrlTester: RegExp;
    __unmount: boolean;
    static defaultProps: Partial<SVGIconProps>;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: SVGIconProps): void;
    componentWillUnmount(): void;
    fetchSvgByUrl: (url: string) => Promise<string>;
    render(): JSX.Element;
}
export declare const Icon: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
