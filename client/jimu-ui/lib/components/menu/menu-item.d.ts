/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { NavItemProps } from '../reactstrap';
interface MenuItemProps extends NavItemProps {
    indent?: string;
    space?: string;
    disabled?: boolean;
    expand?: boolean;
    mode?: 'static' | 'popable' | 'foldable';
}
interface State {
    opened?: boolean;
}
export declare class MenuItem extends React.PureComponent<MenuItemProps, State> {
    static defaultProps: Partial<MenuItemProps>;
    node: any;
    constructor(props: any);
    static getDerivedStateFromProps(props: any, state: any): {
        opened: boolean;
    };
    subPanelStyle: () => import("jimu-core").SerializedStyles;
    toggle(): void;
    handleSubMenuPanelClick(evt: React.MouseEvent<HTMLDivElement>): void;
    render(): JSX.Element;
}
export {};
