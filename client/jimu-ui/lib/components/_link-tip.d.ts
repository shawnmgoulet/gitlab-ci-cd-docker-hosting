/// <reference types="react" />
/// <reference types="@emotion/core" />
/** @jsx jsx */
import { ThemeVariables, IntlShape } from 'jimu-core';
import { TargetType } from './popper';
import { Placement } from 'popper.js';
interface Props {
    open: boolean;
    reference: TargetType;
    placement?: Placement;
    className?: string;
    theme: ThemeVariables;
    href: string;
    intl: IntlShape;
}
export declare const _LinkTip: ({ open, reference, placement, className, href, theme, intl }: Props) => JSX.Element;
export declare const LinkTip: import("react").FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props, "className" | "theme" | "open" | "placement" | "href" | "reference"> & {
    forwardedRef?: import("react").Ref<any>;
} & import("react").RefAttributes<any>, "theme">>;
export {};
