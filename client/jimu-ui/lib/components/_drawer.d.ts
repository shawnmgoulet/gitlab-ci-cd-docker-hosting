/// <reference types="react" />
import { React } from 'jimu-core';
import { PaperProps } from './paper';
export declare function isHorizontal(anchor: string): boolean;
export declare function getAnchor(anchor: string, isRTL?: boolean): any;
interface DrawerProps {
    /**
   * Side from which the drawer will appear.
   */
    anchor?: 'left' | 'top' | 'right' | 'bottom' | 'full';
    /**
     * The contents of the drawer.
     */
    children: any;
    /**
     * @ignore
     */
    className?: string;
    /**
     * Function that will be run when the modal is requested to be closed, prior to actually closing.
     */
    onRequestClose?: (event?: (React.MouseEvent | React.KeyboardEvent)) => void;
    /**
     * If `true`, the drawer is open.
     */
    open: boolean;
    /**
     * Properties applied to the [`Paper`](/api/paper/) element.
     */
    PaperProps: PaperProps;
    /**
     * The variant to use.
     */
    variant: 'permanent' | 'temporary';
    isRTL?: boolean;
    zIndex?: number;
}
export declare class _Drawer extends React.PureComponent<DrawerProps> {
    domNode: HTMLDivElement;
    static defaultProps: Partial<DrawerProps>;
    render(): JSX.Element;
}
export {};
