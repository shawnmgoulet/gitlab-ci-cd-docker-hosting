/// <reference types="react" />
import { React, ThemeVariables } from 'jimu-core';
import { ToastType } from './types';
interface Props {
    className: string;
    type: ToastType;
    text: string;
    style?: React.CSSProperties;
}
export declare class _Toast extends React.PureComponent<Props & {
    theme: ThemeVariables;
}> {
    static count: number;
    id: string;
    defualtOverlayStyle: {
        backgroundColor: string;
        position: string;
        top: string;
        left: string;
        right: string;
        height: string;
        display: string;
    };
    defaultModalStyle: {
        backgroundColor: string;
        height: string;
        marginLeft: string;
        marginRight: string;
    };
    modalStyle: {
        content: {
            backgroundColor: string;
            height: string;
            marginLeft: string;
            marginRight: string;
        };
        overlay: {
            backgroundColor: string;
            position: string;
            top: string;
            left: string;
            right: string;
            height: string;
            display: string;
        };
    };
    constructor(props: any);
    render(): JSX.Element;
}
export declare const Toast: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
