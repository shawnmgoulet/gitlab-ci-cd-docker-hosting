/// <reference types="react" />
import { React, ThemeVariables } from 'jimu-core';
export declare type SubNavMode = 'static' | 'dropdown' | 'foldable';
interface NavProps {
    tabs?: boolean;
    pills?: boolean;
    underline?: boolean;
    vertical?: boolean | string;
    justified?: boolean;
    fill?: boolean;
    tag?: React.ReactType;
    className?: string;
    style?: any;
    right?: boolean;
    gap?: string;
    textAlign?: 'left' | 'center' | 'right';
    submenuMode?: SubNavMode;
    menuOpenMode?: 'click' | 'hover';
    theme?: ThemeVariables;
}
export declare class _Nav extends React.PureComponent<NavProps> {
    static defaultProps: NavProps;
    constructor(props: any);
    render(): JSX.Element;
}
export declare const Nav: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
