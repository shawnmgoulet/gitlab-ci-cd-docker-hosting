/// <reference types="react" />
import { React } from 'jimu-core';
export interface PaperProps {
    className?: string;
    style?: React.CSSProperties;
    component?: React.ComponentClass<any>;
}
export declare const Paper: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
