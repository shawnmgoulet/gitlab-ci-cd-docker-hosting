/// <reference types="react" />
import { React } from 'jimu-core';
import { PaginationProps as BSPaginationProps } from './reactstrap';
export interface PaginationProps extends BSPaginationProps {
    totalPage: number;
    current: number;
    onChangePage?: (current: number) => void;
}
export declare const Pagination: React.FunctionComponent<PaginationProps>;
