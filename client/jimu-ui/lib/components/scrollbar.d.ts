/// <reference types="react" />
import { React } from 'jimu-core';
export declare type ScrollbarHandles = 'click-rail' | 'drag-thumb' | 'keyboard' | 'wheel' | 'touch';
/**
 * perfect-scrollbar#options [https://github.com/mdbootstrap/perfect-scrollbar#options]
 */
export interface ScrollbarOptions {
    /**
     * It is a list of handlers to scroll the element.
     */
    handlers?: ScrollbarHandles[];
    /**
    * The scroll speed applied to mousewheel event.
    */
    wheelSpeed?: number;
    /**
    * If this option is true, when the scroll reaches the end of the side, mousewheel event will be propagated to parent element.
    */
    wheelPropagation?: boolean;
    /**
    * If this option is true, swipe scrolling will be eased.
    */
    swipeEasing?: Boolean;
    /**
    * When set to an integer value, the thumb part of the scrollbar will not shrink below that number of pixels.
    */
    minScrollbarLength?: number;
    /**
    * When set to an integer value, the thumb part of the scrollbar will not expand over that number of pixels.
    */
    maxScrollbarLength?: number;
    /**
    * This sets threashold for `ps--scrolling-x` and `ps--scrolling-y` classes to remain.
    * In the default CSS, they make scrollbars shown regardless of hover state. The unit is millisecond.
    */
    scrollingThreshold?: number;
    /**
    * When set to true, and only one (vertical or horizontal) scrollbar is visible then both vertical and horizontal scrolling will affect the scrollbar.
    */
    useBothWheelAxes?: boolean;
    /**
    * When set to true, the scroll bar in X axis will not be available, regardless of the content width.
    */
    suppressScrollX?: boolean;
    /**
    *When set to true, the scroll bar in Y axis will not be available, regardless of the content height.
    */
    suppressScrollY?: boolean;
    /**
    * The number of pixels the content width can surpass the container width without enabling the X axis scroll bar.
    * Allows some "wiggle room" or "offset break", so that X axis scroll bar is not enabled just because of a few pixels.
    */
    scrollXMarginOffset?: number;
    /**
    * The number of pixels the content height can surpass the container height without enabling the Y axis scroll bar.
    * Allows some "wiggle room" or "offset break", so that Y axis scroll bar is not enabled just because of a few pixels.
    */
    scrollYMarginOffset?: number;
}
export interface ScrollProps {
    className?: string;
    style?: React.CSSProperties;
    containerRef?: (REF: HTMLDivElement) => void;
    options?: ScrollbarOptions;
    onScrollY?: () => void;
    onScrollX?: () => void;
    onScrollUp?: () => void;
    onScrollDown?: () => void;
    onScrollLeft?: () => void;
    onScrollRight?: () => void;
    onYReachStart?: () => void;
    onYReachEnd?: () => void;
    onXReachStart?: () => void;
    onXReachEnd?: () => void;
}
export declare const Scrollbar: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
