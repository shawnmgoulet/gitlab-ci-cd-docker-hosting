/// <reference types="react" />
import { React } from 'jimu-core';
import { NavItemProps as BSNavItemProps } from './reactstrap';
import { SubNavMode } from './nav';
interface NavItemProps extends BSNavItemProps {
    isOpen?: boolean;
    onToggle?: (isOpen: boolean) => {};
    direction?: 'up' | 'down' | 'left' | 'right';
    disabled?: boolean;
}
interface NavItemState {
    isOpen?: boolean;
    anchor?: HTMLElement;
}
export declare class _NavItem extends React.PureComponent<NavItemProps, NavItemState> {
    static contextType: React.Context<{}>;
    state: {
        isOpen: boolean;
        anchor: any;
    };
    constructor(props: any);
    toggle(e: any): void;
    setRef: (ref: HTMLElement) => void;
    getContextValue(): {
        ref: any;
        setRef: (ref: HTMLElement) => void;
        toggle: (e: any) => void;
        isOpen: boolean;
        isDropdown: any;
        direction: "left" | "right" | "up" | "down";
        textAlign: any;
        menuOpenMode: any;
        menuMode: SubNavMode;
    };
    render(): JSX.Element;
}
export declare const NavItem: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
