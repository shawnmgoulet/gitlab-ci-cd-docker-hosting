/// <reference types="react" />
import { React } from 'jimu-core';
interface TabProps {
    active?: boolean;
    key?: string;
    title: string;
    children: JSX.Element | JSX.Element[];
    className?: string;
}
export declare class Tab extends React.PureComponent<TabProps> {
    render(): any;
}
interface TabsProps {
    underline?: boolean;
    tabs?: boolean;
    pills?: boolean;
    fill?: boolean;
    className?: string;
    onTabSelect?: (title: string) => void;
}
interface TabsState {
    activeTab: string;
}
export declare class _Tabs extends React.PureComponent<TabsProps, TabsState> {
    constructor(props: any);
    selectTab(tab: any): void;
    render(): JSX.Element;
}
export declare const Tabs: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
