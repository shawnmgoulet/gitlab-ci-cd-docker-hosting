/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, IntlShape } from 'jimu-core';
export interface QRcodeProps {
    value: string;
    size?: number;
    bgColor?: string;
    fgColor?: string;
    level?: 'L' | 'M' | 'Q' | 'H';
    includeMargin: boolean;
    className?: string;
    downloadFileName?: string;
    hideDownloadBtn?: boolean;
}
interface ExtraProps {
    intl: IntlShape;
    theme: ThemeVariables;
}
export declare class _QRCode extends React.PureComponent<QRcodeProps & ExtraProps> {
    RENDER_TYPE: string;
    DOWNLOAD_FILE_NAME: string;
    DOWNLOAD_BTN_LABEL: string;
    wapperRef: React.RefObject<any>;
    hiddenLinkRef: React.RefObject<any>;
    constructor(props: any);
    downloadImg(): void;
    _onBtnClick: () => void;
    render(): JSX.Element;
}
export declare const QRCode: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
