/// <reference types="react" />
/** @jsx jsx */
import { React, LayoutItemConstructorProps, IMThemeVariables } from 'jimu-core';
interface State {
    widgetInfoList: LayoutItemConstructorProps[];
    open: boolean;
}
export interface WidgetListPopperProps {
    isPlaceholder?: boolean;
    builderTheme: IMThemeVariables;
    referenceElement: HTMLElement;
    isItemAccepted: (item: LayoutItemConstructorProps, isPlaceholder: boolean) => boolean;
    onItemSelect: (item: LayoutItemConstructorProps) => void;
    onClose: () => void;
}
export declare class WidgetListPopper extends React.PureComponent<WidgetListPopperProps, State> {
    contentRef: HTMLElement;
    static defaultProps: Partial<WidgetListPopperProps>;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    private handleOutsideClick;
    private loadWidgetInfos;
    createWidgetCard(widgetInfo: LayoutItemConstructorProps): JSX.Element;
    getStyle(): import("jimu-core").SerializedStyles;
    togglePopper: () => void;
    render(): JSX.Element;
}
export {};
