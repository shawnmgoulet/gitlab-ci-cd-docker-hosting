/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, PageMode } from 'jimu-core';
interface Props {
    theme: ThemeVariables;
    pageMode: PageMode;
    onItemSelect: (pageJson: any) => void;
}
interface State {
    selectedIndex: number;
}
export declare class PageTemplateContent extends React.PureComponent<Props, State> {
    templates: any;
    constructor(props: any);
    onTemplateSelected: (index: number) => void;
    createTemplateCard(pageTemplate: any, index: number): JSX.Element;
    getStyle(): import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
