import { LayoutTemplateType, SidebarType, CollapseSides, SidebarControllerPositions } from 'jimu-layouts/common';
import { PageMode, PageType, LayoutType, LayoutItemType } from 'jimu-core';
export declare const pageTemplates: ({
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
            };
        };
        layouts: {
            layout_9: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            height: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_11: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            height: number;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_12: {
                id: string;
            };
            layout_13: {
                id: string;
            };
            layout_14: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_15: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_17: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_18: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_19: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_20: {
                id: string;
                order: any[];
                type: LayoutType;
            };
            layout_21: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            height: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_22: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            height: number;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        isPending: boolean;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        isPending: boolean;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_23: {
                id: string;
                type: LayoutType;
                order: string[];
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            left: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_24: {
                id: string;
                type: LayoutType;
                order: string[];
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        bbox: {
                            right: number;
                            left: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_25: {
                id: string;
                order: any[];
                type: LayoutType;
            };
        };
        widgets: {
            widget_24: {
                uri: string;
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    background: {
                        color: string;
                    };
                    boxShadow: {
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
            widget_29: {
                uri: string;
                config: {
                    direction: SidebarType;
                    collapseSide: CollapseSides;
                    resizable: boolean;
                    overlay: boolean;
                    size: string;
                    defaultState: number;
                    toggleBtn: {
                        icon: string;
                        visible: boolean;
                        offsetX: number;
                        offsetY: number;
                        position: SidebarControllerPositions;
                        iconSize: number;
                        height: number;
                        width: number;
                        color: {
                            normal: {
                                icon: {
                                    useTheme: boolean;
                                    color: string;
                                };
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                            hover: {
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                        };
                        expandStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                        collapseStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                    };
                    divider: {
                        visible: boolean;
                    };
                };
                layouts: {
                    FIRST: {
                        LARGE: string;
                    };
                    SECOND: {
                        LARGE: string;
                    };
                };
            };
            widget_25: {
                uri: string;
            };
            widget_26: {
                uri: string;
                config: {
                    placeholder: string;
                    style: {
                        padding: string;
                        verticalAlign: string;
                        wrap: boolean;
                    };
                    text: string;
                };
            };
            widget_27: {
                uri: string;
                config: {
                    placeholder: string;
                    style: {
                        padding: string;
                        verticalAlign: string;
                        wrap: boolean;
                    };
                    text: string;
                };
            };
            widget_28: {
                uri: string;
                layouts: {
                    controller: {
                        LARGE: string;
                    };
                    openwidget: {
                        LARGE: string;
                    };
                };
            };
            widget_30: {
                uri: string;
                config: {};
                layouts: {
                    HOVER: {
                        LARGE: string;
                    };
                    REGULAR: {
                        LARGE: string;
                    };
                    SELECTED: {
                        LARGE: string;
                    };
                };
            };
            widget_34: {
                uri: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_31: {
                uri: string;
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    background: {
                        color: string;
                    };
                    boxShadow: {
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
            widget_32: {
                uri: string;
                config: {
                    direction: SidebarType;
                    collapseSide: CollapseSides;
                    resizable: boolean;
                    overlay: boolean;
                    size: string;
                    defaultState: number;
                    toggleBtn: {
                        icon: string;
                        visible: boolean;
                        offsetX: number;
                        offsetY: number;
                        position: SidebarControllerPositions;
                        iconSize: number;
                        height: number;
                        width: number;
                        color: {
                            normal: {
                                icon: {
                                    useTheme: boolean;
                                    color: string;
                                };
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                            hover: {
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                        };
                        expandStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                        collapseStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                    };
                    divider: {
                        visible: boolean;
                    };
                };
                layouts: {
                    FIRST: {
                        SMALL: string;
                    };
                    SECOND: {
                        SMALL: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    6: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    7: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    8: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    9: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    6: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    7: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    8: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    9: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    10: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    11: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    12: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
})[];
export declare const fullScreenTemplates: ({
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
            };
        };
        layouts: {
            layout_9: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            height: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_11: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            height: number;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_12: {
                id: string;
            };
            layout_13: {
                id: string;
            };
            layout_14: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_15: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_17: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_18: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_19: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            right: number;
                        };
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            height: number;
                            width: number;
                        };
                        setting: {
                            lockParent: boolean;
                        };
                    };
                };
            };
            layout_20: {
                id: string;
                order: any[];
                type: LayoutType;
            };
            layout_21: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            height: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            right: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_22: {
                id: string;
                order: string[];
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            height: number;
                        };
                    };
                    1: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        isPending: boolean;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    2: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        isPending: boolean;
                        bbox: {
                            left: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            width: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_23: {
                id: string;
                type: LayoutType;
                order: string[];
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            right: number;
                            left: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_24: {
                id: string;
                type: LayoutType;
                order: string[];
                content: {
                    0: {
                        id: string;
                        type: LayoutItemType;
                        bbox: {
                            right: number;
                            left: number;
                            top: number;
                            bottom: number;
                        };
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_25: {
                id: string;
                order: any[];
                type: LayoutType;
            };
        };
        widgets: {
            widget_24: {
                uri: string;
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    background: {
                        color: string;
                    };
                    boxShadow: {
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
            widget_29: {
                uri: string;
                config: {
                    direction: SidebarType;
                    collapseSide: CollapseSides;
                    resizable: boolean;
                    overlay: boolean;
                    size: string;
                    defaultState: number;
                    toggleBtn: {
                        icon: string;
                        visible: boolean;
                        offsetX: number;
                        offsetY: number;
                        position: SidebarControllerPositions;
                        iconSize: number;
                        height: number;
                        width: number;
                        color: {
                            normal: {
                                icon: {
                                    useTheme: boolean;
                                    color: string;
                                };
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                            hover: {
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                        };
                        expandStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                        collapseStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                    };
                    divider: {
                        visible: boolean;
                    };
                };
                layouts: {
                    FIRST: {
                        LARGE: string;
                    };
                    SECOND: {
                        LARGE: string;
                    };
                };
            };
            widget_25: {
                uri: string;
            };
            widget_26: {
                uri: string;
                config: {
                    placeholder: string;
                    style: {
                        padding: string;
                        verticalAlign: string;
                        wrap: boolean;
                    };
                    text: string;
                };
            };
            widget_27: {
                uri: string;
                config: {
                    placeholder: string;
                    style: {
                        padding: string;
                        verticalAlign: string;
                        wrap: boolean;
                    };
                    text: string;
                };
            };
            widget_28: {
                uri: string;
                layouts: {
                    controller: {
                        LARGE: string;
                    };
                    openwidget: {
                        LARGE: string;
                    };
                };
            };
            widget_30: {
                uri: string;
                config: {};
                layouts: {
                    HOVER: {
                        LARGE: string;
                    };
                    REGULAR: {
                        LARGE: string;
                    };
                    SELECTED: {
                        LARGE: string;
                    };
                };
            };
            widget_34: {
                uri: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_31: {
                uri: string;
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    background: {
                        color: string;
                    };
                    boxShadow: {
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
            widget_32: {
                uri: string;
                config: {
                    direction: SidebarType;
                    collapseSide: CollapseSides;
                    resizable: boolean;
                    overlay: boolean;
                    size: string;
                    defaultState: number;
                    toggleBtn: {
                        icon: string;
                        visible: boolean;
                        offsetX: number;
                        offsetY: number;
                        position: SidebarControllerPositions;
                        iconSize: number;
                        height: number;
                        width: number;
                        color: {
                            normal: {
                                icon: {
                                    useTheme: boolean;
                                    color: string;
                                };
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                            hover: {
                                bg: {
                                    useTheme: boolean;
                                    color: string;
                                };
                            };
                        };
                        expandStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                        collapseStyle: {
                            style: {
                                borderRadius: string;
                            };
                        };
                    };
                    divider: {
                        visible: boolean;
                    };
                };
                layouts: {
                    FIRST: {
                        SMALL: string;
                    };
                    SECOND: {
                        SMALL: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
})[];
export declare const autoScrollTemplates: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
}[];
