import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const footerTemplate2: {
    type: LayoutTemplateType;
    icon: any;
    config: {
        layouts: {
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                            right: number;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
            };
            layout_2: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                            right: number;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                label: string;
                config: {
                    uiMode: string;
                    popup: {
                        icon: {
                            svg: string;
                            properties: {
                                filename: any;
                                originalName: any;
                                path: any;
                                color: string;
                                size: number;
                                inlineSvg: boolean;
                            };
                        };
                        items: string[];
                        tooltip: string;
                    };
                    inline: {
                        items: string[];
                        design: {
                            direction: string;
                            hideLabel: boolean;
                            btnRad: string;
                            btnColor: string;
                            iconColor: string;
                            size: string;
                        };
                    };
                };
            };
        };
        footer: {
            layout: {
                LARGE: string;
                SMALL: string;
            };
            height: {
                LARGE: string;
                SMALL: string;
            };
            backgroundColor: string;
        };
    };
};
