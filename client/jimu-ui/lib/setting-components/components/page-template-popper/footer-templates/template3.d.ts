import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const footerTemplate3: {
    type: LayoutTemplateType;
    icon: any;
    config: {
        layouts: {
            layout_1: {
                type: string;
                order: string[];
                content: {
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                top: boolean;
                            };
                        };
                    };
                    4: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_3: {
                type: string;
                content: {
                    0: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    1: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        setting: {
                            maintainedByLayout: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                };
            };
            layout_5: {
                order: string[];
                content: {
                    1: {
                        type: string;
                        bbox: {
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    4: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                };
            };
            layout_6: {
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                };
            };
            layout_7: {
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                };
            };
            layout_8: {
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                    };
                };
            };
            layout_9: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                top: boolean;
                            };
                        };
                    };
                    3: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    4: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    5: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    6: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    7: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    8: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    9: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    10: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    11: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    12: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    13: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    14: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    15: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    16: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    17: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    18: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: number;
                            width: string;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_10: {
                type: string;
                content: {
                    0: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    1: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                label: string;
                config: {
                    uiMode: string;
                    popup: {
                        icon: {
                            svg: string;
                            properties: {
                                filename: any;
                                originalName: any;
                                path: any;
                                color: string;
                                size: number;
                                inlineSvg: boolean;
                            };
                        };
                        items: string[];
                        tooltip: string;
                    };
                    inline: {
                        items: string[];
                        design: {
                            direction: string;
                            hideLabel: boolean;
                            btnRad: string;
                            btnColor: string;
                            iconColor: string;
                            size: string;
                        };
                    };
                };
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {};
                    };
                    styleConfig: {};
                };
            };
            widget_4: {
                id: string;
                uri: string;
                label: string;
                config: {};
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    border: {
                        type: string;
                        color: string;
                        width: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
            widget_5: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            number: number[];
                            unit: string;
                        };
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_7: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            space: number[];
                            unit: string;
                        };
                        justifyContent: string;
                        alignItems: string;
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_8: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_9: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_10: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_11: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        toolTipExpression: any;
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_12: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            space: number[];
                            unit: string;
                        };
                        justifyContent: string;
                        alignItems: string;
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_13: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_14: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            space: number[];
                            unit: string;
                        };
                        justifyContent: string;
                        alignItems: string;
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_15: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            space: number[];
                            unit: string;
                        };
                        justifyContent: string;
                        alignItems: string;
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_16: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_17: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_18: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_19: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_20: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_21: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_22: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_23: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_24: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_25: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_26: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_27: {
                id: string;
                uri: string;
                label: string;
                config: {};
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    border: {
                        type: string;
                        color: string;
                        width: {
                            distance: number;
                            unit: string;
                        };
                    };
                };
            };
        };
        footer: {
            layout: {
                LARGE: string;
                SMALL: string;
            };
            height: {
                LARGE: string;
                SMALL: string;
            };
            backgroundColor: string;
        };
    };
};
