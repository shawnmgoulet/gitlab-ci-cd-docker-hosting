import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const footerTemplate1: {
    type: LayoutTemplateType;
    icon: any;
    config: {
        layouts: {
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_2: {
                type: string;
                content: {
                    0: {
                        type: string;
                        setting: {
                            style: {};
                        };
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: number;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                    };
                };
                order: string[];
            };
            layout_3: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: number;
                            right: number;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            autoProps: {
                                left: boolean;
                            };
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    3: {
                        type: string;
                        setting: {
                            style: {};
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    4: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                    5: {
                        type: string;
                        setting: {
                            style: {};
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                label: string;
                config: {
                    uiMode: string;
                    popup: {
                        icon: {
                            svg: string;
                            properties: {
                                filename: any;
                                originalName: any;
                                path: any;
                                color: string;
                                size: number;
                                inlineSvg: boolean;
                            };
                        };
                        items: string[];
                        tooltip: string;
                    };
                    inline: {
                        items: string[];
                        design: {
                            direction: string;
                            hideLabel: boolean;
                            btnRad: string;
                            btnColor: string;
                            iconColor: string;
                            size: string;
                        };
                    };
                };
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_4: {
                id: string;
                uri: string;
                label: string;
                config: {
                    space: number;
                    style: {
                        padding: {
                            number: number[];
                            unit: string;
                        };
                    };
                };
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
            };
            widget_5: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
            widget_6: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        text: string;
                        toolTip: string;
                        linkParam: {};
                        textExpression: any;
                    };
                    styleConfig: {
                        name: string;
                        themeStyle: {
                            className: string;
                            color: string;
                            quickStyleType: string;
                        };
                        customStyle: {
                            regular: {};
                            hover: {};
                            clicked: {};
                        };
                    };
                };
                useDataSources: any[];
            };
        };
        footer: {
            layout: {
                LARGE: string;
                SMALL: string;
            };
            height: {
                LARGE: number;
                SMALL: string;
            };
            backgroundColor: string;
        };
    };
};
