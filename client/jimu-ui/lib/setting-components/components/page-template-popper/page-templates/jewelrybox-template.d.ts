import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const jewelryBox: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "home": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                    "SMALL": string;
                };
                "isVisible": boolean;
                "isDefault": boolean;
                "id": string;
            };
        };
        "layouts": {
            "layout_1": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
            };
            "layout_2": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_3": {};
            "layout_4": {};
            "layout_5": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_6": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_7": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_8": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_9": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_10": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_11": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_12": {
                "type": string;
            };
            "layout_13": {
                "type": string;
            };
            "layout_14": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_15": {
                "type": string;
            };
            "layout_16": {
                "type": string;
            };
            "layout_17": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_18": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_19": {
                "type": string;
                "content": {
                    "1": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fullsize": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_20": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_2": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_3": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_4": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "onlyOpenOne": boolean;
                    "displayType": string;
                    "vertical": boolean;
                    "iconStyle": string;
                    "showLabel": boolean;
                    "space": number;
                    "iconSize": string;
                    "size": {};
                };
                "version": string;
                "layouts": {
                    "controller": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "openwidget": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_6": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "LARGE": string;
                    };
                    "SECOND": {
                        "LARGE": string;
                    };
                };
            };
            "widget_7": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "direction": string;
                    "alignType": string;
                    "isInitialed": boolean;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                    "isItemStyleConfirm": boolean;
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_8": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_9": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_10": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_11": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "toolConifg": {
                        "canZoom": boolean;
                        "canHome": boolean;
                        "canSearch": boolean;
                    };
                };
                "version": string;
                "layouts": {
                    "MapFixedLayout": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_12": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "SMALL": string;
                    };
                };
            };
            "widget_13": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "SMALL": string;
                    };
                    "SECOND": {
                        "SMALL": string;
                    };
                };
            };
        };
    };
};
