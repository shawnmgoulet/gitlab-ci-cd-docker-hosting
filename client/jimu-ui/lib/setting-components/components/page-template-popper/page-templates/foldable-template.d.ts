import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const foldable: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                        };
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_2: {
                type: string;
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
            };
            layout_7: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_8: {};
            layout_9: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {
                            cropParam: {
                                cropShape: string;
                                svgPath: string;
                                svgViewBox: string;
                            };
                        };
                    };
                    styleConfig: {};
                };
                version: string;
            };
            widget_3: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_4: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_5: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_6: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_7: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
};
