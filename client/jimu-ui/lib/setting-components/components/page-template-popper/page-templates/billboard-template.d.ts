import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const billboard: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            layout_0: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_2: {
                order: any[];
                content: {};
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_7: {
                content: {};
                order: any[];
            };
            layout_8: {
                type: string;
            };
            layout_9: {
                content: {
                    0: {
                        type: string;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                        canNavigation: boolean;
                        canBaseMap: boolean;
                        canMeasure: boolean;
                        canFullScreen: boolean;
                        canScaleBar: boolean;
                    };
                    initialMapDataSourceID: string;
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
        };
    };
};
