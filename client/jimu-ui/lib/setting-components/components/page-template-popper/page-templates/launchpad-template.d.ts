import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const launchpad: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_29: {
                type: string;
            };
            layout_30: {
                type: string;
            };
            layout_51: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_52: {};
            layout_53: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_54: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_55: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
                order: string[];
            };
            layout_56: {};
            layout_57: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_23: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_24: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_25: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
                version: string;
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
};
