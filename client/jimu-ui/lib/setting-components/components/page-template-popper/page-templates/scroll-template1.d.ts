import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const scrollTempalte1: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "home": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "id": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
            };
        };
        "layouts": {
            "home-layout-large": {
                "type": string;
                "order": string[];
                "content": {
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "4": {
                        "type": string;
                        "widgetId": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "widgetId": string;
                    };
                    "6": {
                        "type": string;
                        "widgetId": string;
                    };
                    "7": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "9": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "10": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "11": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_6": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_7": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                        };
                    };
                };
            };
            "layout_9": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_10": {
                "type": string;
            };
            "layout_11": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_20": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_21": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_22": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_23": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_24": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_25": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_26": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_27": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_29": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
            };
            "layout_30": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_32": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "top": boolean;
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_33": {
                "type": string;
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_100": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "fitContainer": boolean;
                        };
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_101": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_102": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_103": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_106": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "top": boolean;
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_107": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_108": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_109": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_8": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_9": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_10": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_11": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                                "background": {
                                    "color": string;
                                    "fillType": string;
                                    "image": {
                                        "url": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_12": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_14": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_41": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_42": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_43": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_44": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_45": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_46": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_47": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_48": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_49": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_50": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_51": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_52": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_53": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_54": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_55": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_56": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_57": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_58": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_59": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "useDataSourcesEnabled": boolean;
            };
            "widget_61": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_62": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_63": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_64": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_65": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_212": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_213": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_214": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_215": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_216": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_217": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_225": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_226": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_227": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_228": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_229": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_230": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_231": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_232": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
};
