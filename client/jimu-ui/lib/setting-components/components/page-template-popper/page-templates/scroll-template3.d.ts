import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const scrollTempalte3: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "page_2": {
                "id": string;
                "label": string;
                "mode": string;
                "type": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
                "maxWidth": number;
            };
        };
        "layouts": {
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_78": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "widgetId": string;
                    };
                    "3": {
                        "type": string;
                        "widgetId": string;
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "5": {
                        "type": string;
                        "widgetId": string;
                    };
                    "6": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_79": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_80": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_81": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_82": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_83": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_84": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_85": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_86": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_87": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_88": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_89": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_90": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_91": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_92": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "3": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_93": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_94": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_95": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_96": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_97": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_98": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_99": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_158": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_159": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_160": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_161": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_162": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_163": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_164": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_165": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                    };
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_166": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_167": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_168": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_169": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_170": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_171": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_172": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_173": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_174": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_175": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_176": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_177": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_178": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_179": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_180": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                };
            };
            "widget_181": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_182": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_183": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_184": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_185": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_186": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_187": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_188": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_189": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_190": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_191": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_192": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_193": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_194": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_195": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_196": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_197": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_198": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_199": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_200": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_201": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_202": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_203": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_204": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_205": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_206": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_207": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_208": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_209": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "fillType": string;
                    };
                };
            };
            "widget_210": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_211": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
};
