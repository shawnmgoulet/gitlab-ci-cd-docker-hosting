import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const scrollTempalte2: {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "page_1": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "id": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
            };
        };
        "layouts": {
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_39": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "heightMode": string;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "5": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_40": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_41": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_42": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_43": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_44": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_45": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_46": {
                "type": string;
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_63": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                            "heightMode": string;
                            "offsetX": number;
                        };
                    };
                };
            };
            "layout_64": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_66": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_67": {
                "type": string;
            };
            "layout_71": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_72": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_74": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_75": {
                "type": string;
            };
            "layout_77": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_80": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_81": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_82": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_83": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_84": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_85": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_86": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_87": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                                "background": {
                                    "color": string;
                                    "fillType": string;
                                    "image": {
                                        "url": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_88": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_89": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_90": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_91": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_131": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_132": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_133": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_134": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_145": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_146": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_147": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_148": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_149": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_150": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_154": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_155": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_156": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_157": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
};
