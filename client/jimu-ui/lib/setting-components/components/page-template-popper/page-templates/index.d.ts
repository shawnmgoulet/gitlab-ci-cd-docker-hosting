import { LayoutTemplateType } from 'jimu-layouts/common';
import { PageMode, PageType, LayoutType, LayoutItemType } from 'jimu-core';
export declare const pageTemplates: ({
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                        };
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_2: {
                type: string;
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
            };
            layout_7: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_8: {};
            layout_9: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {
                            cropParam: {
                                cropShape: string;
                                svgPath: string;
                                svgViewBox: string;
                            };
                        };
                    };
                    styleConfig: {};
                };
                version: string;
            };
            widget_3: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_4: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_5: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_6: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_7: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_29: {
                type: string;
            };
            layout_30: {
                type: string;
            };
            layout_51: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_52: {};
            layout_53: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_54: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_55: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
                order: string[];
            };
            layout_56: {};
            layout_57: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_23: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_24: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_25: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
                version: string;
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "home": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                    "SMALL": string;
                };
                "isVisible": boolean;
                "isDefault": boolean;
                "id": string;
            };
        };
        "layouts": {
            "layout_1": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
            };
            "layout_2": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_3": {};
            "layout_4": {};
            "layout_5": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_6": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_7": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_8": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_9": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_10": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_11": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_12": {
                "type": string;
            };
            "layout_13": {
                "type": string;
            };
            "layout_14": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_15": {
                "type": string;
            };
            "layout_16": {
                "type": string;
            };
            "layout_17": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_18": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_19": {
                "type": string;
                "content": {
                    "1": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fullsize": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_20": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_2": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_3": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_4": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "onlyOpenOne": boolean;
                    "displayType": string;
                    "vertical": boolean;
                    "iconStyle": string;
                    "showLabel": boolean;
                    "space": number;
                    "iconSize": string;
                    "size": {};
                };
                "version": string;
                "layouts": {
                    "controller": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "openwidget": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_6": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "LARGE": string;
                    };
                    "SECOND": {
                        "LARGE": string;
                    };
                };
            };
            "widget_7": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "direction": string;
                    "alignType": string;
                    "isInitialed": boolean;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                    "isItemStyleConfirm": boolean;
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_8": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_9": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_10": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_11": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "toolConifg": {
                        "canZoom": boolean;
                        "canHome": boolean;
                        "canSearch": boolean;
                    };
                };
                "version": string;
                "layouts": {
                    "MapFixedLayout": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_12": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "SMALL": string;
                    };
                };
            };
            "widget_13": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "SMALL": string;
                    };
                    "SECOND": {
                        "SMALL": string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            layout_0: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_2: {
                order: any[];
                content: {};
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_7: {
                content: {};
                order: any[];
            };
            layout_8: {
                type: string;
            };
            layout_9: {
                content: {
                    0: {
                        type: string;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                        canNavigation: boolean;
                        canBaseMap: boolean;
                        canMeasure: boolean;
                        canFullScreen: boolean;
                        canScaleBar: boolean;
                    };
                    initialMapDataSourceID: string;
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    6: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    7: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    8: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    9: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            height: string;
                            width: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: string;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {
                    0: {
                        id: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    1: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    2: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    3: {
                        id: string;
                        parentId: string;
                        direction: string;
                        items: string[];
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    4: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    5: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    6: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    7: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    8: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        widgetId: string;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    9: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    10: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    11: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                    12: {
                        id: string;
                        parentId: string;
                        type: LayoutItemType;
                        bbox: {
                            width: string;
                            height: string;
                        };
                    };
                };
                setting: {
                    rootItemId: string;
                };
            };
        };
        widgets: {
            widget_0: {
                uri: string;
            };
        };
    };
})[];
export declare const fullScreenTemplates: ({
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                        };
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_2: {
                type: string;
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                width: boolean;
                            };
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                width: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
            };
            layout_7: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                height: boolean;
                            };
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                            autoProps: {
                                height: boolean;
                            };
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                height: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_8: {};
            layout_9: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        LARGE: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {
                            cropParam: {
                                cropShape: string;
                                svgPath: string;
                                svgViewBox: string;
                            };
                        };
                    };
                    styleConfig: {};
                };
                version: string;
            };
            widget_3: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_4: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                        overflow: string;
                        padding: string;
                    };
                };
                version: string;
            };
            widget_5: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_6: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_7: {
                id: string;
                uri: string;
                label: string;
                config: {};
                version: string;
                layouts: {
                    DEFAULT: {
                        SMALL: string;
                    };
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    boxShadow: {
                        offsetX: {
                            distance: number;
                            unit: string;
                        };
                        offsetY: {
                            distance: number;
                            unit: string;
                        };
                        blur: {
                            distance: number;
                            unit: string;
                        };
                        spread: {
                            distance: number;
                            unit: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            'home-layout-large': {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_29: {
                type: string;
            };
            layout_30: {
                type: string;
            };
            layout_51: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_52: {};
            layout_53: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_54: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_55: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            autoProps: {
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            lockParent: boolean;
                            hCenter: boolean;
                        };
                    };
                };
                order: string[];
            };
            layout_56: {};
            layout_57: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_23: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_24: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
            };
            widget_25: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
                version: string;
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "home": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                    "SMALL": string;
                };
                "isVisible": boolean;
                "isDefault": boolean;
                "id": string;
            };
        };
        "layouts": {
            "layout_1": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
            };
            "layout_2": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_3": {};
            "layout_4": {};
            "layout_5": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_6": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "bottom": number;
                            "right": number;
                        };
                    };
                };
                "order": string[];
            };
            "layout_7": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_8": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_9": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_10": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_11": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "hCenter": boolean;
                            "vCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_12": {
                "type": string;
            };
            "layout_13": {
                "type": string;
            };
            "layout_14": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                    };
                };
            };
            "layout_15": {
                "type": string;
            };
            "layout_16": {
                "type": string;
            };
            "layout_17": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                                "width": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_18": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "setting": {
                            "vCenter": boolean;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "left": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_19": {
                "type": string;
                "content": {
                    "1": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fullsize": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_20": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "autoProps": {
                                "left": boolean;
                                "right": boolean;
                                "width": boolean;
                                "top": boolean;
                                "bottom": boolean;
                                "height": boolean;
                            };
                        };
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": string;
                            "height": string;
                            "right": number;
                            "bottom": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_2": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_3": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_4": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "onlyOpenOne": boolean;
                    "displayType": string;
                    "vertical": boolean;
                    "iconStyle": string;
                    "showLabel": boolean;
                    "space": number;
                    "iconSize": string;
                    "size": {};
                };
                "version": string;
                "layouts": {
                    "controller": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "openwidget": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_6": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "LARGE": string;
                    };
                    "SECOND": {
                        "LARGE": string;
                    };
                };
            };
            "widget_7": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "direction": string;
                    "alignType": string;
                    "isInitialed": boolean;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "MEDIUM": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                    "isItemStyleConfirm": boolean;
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_8": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_9": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_10": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "version": string;
            };
            "widget_11": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "toolConifg": {
                        "canZoom": boolean;
                        "canHome": boolean;
                        "canSearch": boolean;
                    };
                };
                "version": string;
                "layouts": {
                    "MapFixedLayout": {
                        "LARGE": string;
                        "SMALL": string;
                    };
                };
            };
            "widget_12": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {};
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                        "color": string;
                    };
                };
                "layouts": {
                    "DEFAULT": {
                        "SMALL": string;
                    };
                };
            };
            "widget_13": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "direction": string;
                    "collapseSide": string;
                    "overlay": boolean;
                    "size": string;
                    "divider": {
                        "visible": boolean;
                    };
                    "resizable": boolean;
                    "toggleBtn": {
                        "visible": boolean;
                        "icon": string;
                        "offsetX": number;
                        "offsetY": number;
                        "position": string;
                        "iconSize": number;
                        "width": number;
                        "height": number;
                        "color": {
                            "normal": {
                                "icon": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                    "opacity": number;
                                };
                            };
                            "hover": {
                                "bg": {
                                    "useTheme": boolean;
                                    "color": string;
                                };
                            };
                        };
                        "expandStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "collapseStyle": {
                            "style": {
                                "borderRadius": string;
                            };
                        };
                        "template": string;
                    };
                    "defaultState": number;
                };
                "version": string;
                "layouts": {
                    "FIRST": {
                        "SMALL": string;
                    };
                    "SECOND": {
                        "SMALL": string;
                    };
                };
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            home: {
                label: string;
                type: string;
                mode: string;
                layout: {
                    LARGE: string;
                    SMALL: string;
                };
                isVisible: boolean;
                id: string;
            };
        };
        layouts: {
            layout_0: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
            };
            layout_1: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_2: {
                order: any[];
                content: {};
            };
            layout_3: {
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
            layout_4: {
                type: string;
            };
            layout_5: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: number;
                            width: string;
                            height: string;
                            right: number;
                            bottom: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                left: boolean;
                                right: boolean;
                                width: boolean;
                                top: boolean;
                                bottom: boolean;
                                height: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
            layout_6: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            lockParent: boolean;
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                };
                order: string[];
            };
            layout_7: {
                content: {};
                order: any[];
            };
            layout_8: {
                type: string;
            };
            layout_9: {
                content: {
                    0: {
                        type: string;
                        widgetId: string;
                        bbox: {
                            left: number;
                            top: number;
                            bottom: number;
                            right: number;
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                        canNavigation: boolean;
                        canBaseMap: boolean;
                        canMeasure: boolean;
                        canFullScreen: boolean;
                        canScaleBar: boolean;
                    };
                    initialMapDataSourceID: string;
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
            };
            widget_2: {
                id: string;
                uri: string;
                icon: {
                    svg: string;
                    properties: {
                        filename: any;
                        originalName: any;
                        path: any;
                        color: string;
                        size: number;
                        inlineSvg: boolean;
                    };
                };
                label: string;
                config: {
                    onlyOpenOne: boolean;
                    displayType: string;
                    vertical: boolean;
                    iconStyle: string;
                    showLabel: boolean;
                    space: number;
                    iconSize: string;
                    size: {};
                };
                version: string;
                layouts: {
                    controller: {
                        LARGE: string;
                        SMALL: string;
                    };
                    openwidget: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    toolConifg: {
                        canZoom: boolean;
                        canHome: boolean;
                        canSearch: boolean;
                    };
                };
                version: string;
                layouts: {
                    MapFixedLayout: {
                        LARGE: string;
                        SMALL: string;
                    };
                };
                useDataSources: any[];
                useDataSourcesEnabled: boolean;
            };
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
})[];
export declare const autoScrollTemplates: ({
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "home": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "id": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
            };
        };
        "layouts": {
            "home-layout-large": {
                "type": string;
                "order": string[];
                "content": {
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "4": {
                        "type": string;
                        "widgetId": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "widgetId": string;
                    };
                    "6": {
                        "type": string;
                        "widgetId": string;
                    };
                    "7": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "9": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "10": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "11": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_6": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_7": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                        };
                    };
                };
            };
            "layout_9": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_10": {
                "type": string;
            };
            "layout_11": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_20": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_21": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_22": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_23": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_24": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_25": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_26": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_27": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_29": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
            };
            "layout_30": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_32": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "top": boolean;
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_33": {
                "type": string;
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_100": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                            "autoProps": {
                                "height": boolean;
                            };
                        };
                    };
                    "1": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "fitContainer": boolean;
                        };
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_101": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_102": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_103": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_106": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "bottom": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "top": boolean;
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_107": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_108": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_109": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_8": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_9": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_10": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_11": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                                "background": {
                                    "color": string;
                                    "fillType": string;
                                    "image": {
                                        "url": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_12": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_14": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_41": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_42": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_43": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_44": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_45": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_46": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_47": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_48": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_49": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_50": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_51": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_52": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_53": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_54": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_55": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_56": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_57": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_58": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_59": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "useDataSourcesEnabled": boolean;
            };
            "widget_61": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_62": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_63": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_64": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_65": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                                "SMALL": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_212": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_213": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_214": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_215": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_216": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_217": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_225": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_226": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_227": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_228": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_229": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_230": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_231": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_232": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "page_1": {
                "label": string;
                "type": string;
                "mode": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "id": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
            };
        };
        "layouts": {
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_39": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "heightMode": string;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "5": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_40": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_41": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_42": {
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_43": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_44": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_45": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_46": {
                "type": string;
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_63": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "style": {
                                "alignSelf": string;
                            };
                            "heightMode": string;
                            "offsetX": number;
                        };
                    };
                };
            };
            "layout_64": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_66": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_67": {
                "type": string;
            };
            "layout_71": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_72": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_74": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
            "layout_75": {
                "type": string;
            };
            "layout_77": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                            "bottom": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                                "top": boolean;
                            };
                        };
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "lockParent": boolean;
                            "autoProps": {
                                "width": boolean;
                            };
                            "vCenter": boolean;
                        };
                    };
                };
                "order": string[];
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_80": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_81": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_82": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_83": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_84": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_85": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_86": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_87": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                                "background": {
                                    "color": string;
                                    "fillType": string;
                                    "image": {
                                        "url": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_88": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_89": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_90": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "cropShape": string;
                                "svgPath": string;
                                "svgViewBox": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_91": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_131": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_132": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_133": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_134": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_145": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_146": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_147": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_148": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_149": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_150": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "maxItems": number;
                    "space": number;
                    "itemsPerPage": number;
                    "isItemStyleConfirm": boolean;
                    "isInitialed": boolean;
                    "pageStyle": string;
                    "itemStyle": string;
                    "hoverType": string;
                    "selectedStyle": string;
                    "differentOddEven": boolean;
                    "scrollBarOpen": boolean;
                    "direction": string;
                    "alignType": string;
                    "scrollStep": number;
                    "cardConfigs": {
                        "REGULAR": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "HOVER": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "enable": boolean;
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                        };
                        "SELECTED": {
                            "cardSize": {
                                "LARGE": {
                                    "width": number;
                                    "height": number;
                                };
                            };
                            "backgroundStyle": {
                                "border": {
                                    "type": string;
                                    "color": string;
                                    "width": {
                                        "distance": number;
                                        "unit": string;
                                    };
                                };
                            };
                            "selectionMode": string;
                        };
                    };
                    "style": {
                        "id": string;
                    };
                };
                "version": string;
                "layouts": {
                    "REGULAR": {
                        "LARGE": string;
                    };
                    "SELECTED": {
                        "LARGE": string;
                    };
                    "HOVER": {
                        "LARGE": string;
                    };
                };
            };
            "widget_154": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_155": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                        "overflow": string;
                        "padding": string;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_156": {
                "uri": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
            "widget_157": {
                "uri": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "label": string;
                "version": string;
                "id": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        "pages": {
            "page_2": {
                "id": string;
                "label": string;
                "mode": string;
                "type": string;
                "layout": {
                    "LARGE": string;
                };
                "isVisible": boolean;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "heightInBuilder": {
                    "LARGE": number;
                };
                "maxWidth": number;
            };
        };
        "layouts": {
            "layout_3": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                            "autoProps": {
                                "width": boolean;
                                "height": boolean;
                            };
                            "lockParent": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                };
            };
            "layout_4": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "right": boolean;
                                "left": boolean;
                            };
                        };
                    };
                };
                "order": string[];
            };
            "layout_60": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                            "right": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                    };
                };
            };
            "layout_61": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "setting": {
                            "style": {};
                        };
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_62": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": number;
                            "right": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "hCenter": boolean;
                            "autoProps": {
                                "left": boolean;
                            };
                            "vCenter": boolean;
                        };
                        "isPending": boolean;
                    };
                    "3": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "hCenter": boolean;
                            "vCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "4": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                    "5": {
                        "type": string;
                        "setting": {
                            "style": {};
                            "vCenter": boolean;
                            "hCenter": boolean;
                        };
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
            };
            "layout_78": {
                "type": string;
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "2": {
                        "type": string;
                        "widgetId": string;
                    };
                    "3": {
                        "type": string;
                        "widgetId": string;
                    };
                    "4": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                    "5": {
                        "type": string;
                        "widgetId": string;
                    };
                    "6": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
                "order": string[];
            };
            "layout_79": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_80": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_81": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "autoProps": {
                                "height": boolean;
                            };
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_82": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_83": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_84": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_85": {
                "content": {
                    "0": {
                        "bbox": {
                            "width": number;
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_86": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_87": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_88": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_89": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                    "2": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_90": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "maintainedByLayout": boolean;
                        };
                    };
                };
            };
            "layout_91": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_92": {
                "content": {
                    "0": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "1": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "2": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                    "3": {
                        "bbox": {
                            "left": number;
                            "width": number;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "heightMode": string;
                            "aspectRatio": number;
                        };
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_93": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_94": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_95": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_96": {
                "content": {
                    "0": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                        "isPending": boolean;
                    };
                    "1": {
                        "bbox": {
                            "height": string;
                        };
                        "type": string;
                        "widgetId": string;
                        "isPending": boolean;
                    };
                };
                "order": string[];
                "type": string;
            };
            "layout_97": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
            "layout_98": {
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                    "1": {
                        "type": string;
                        "bbox": {
                            "left": string;
                            "top": string;
                            "width": string;
                            "height": string;
                        };
                        "widgetId": string;
                        "setting": {
                            "fitContainer": boolean;
                        };
                    };
                };
            };
            "layout_99": {
                "type": string;
                "order": string[];
                "content": {
                    "0": {
                        "type": string;
                        "bbox": {
                            "left": number;
                            "width": number;
                            "height": number;
                        };
                        "widgetId": string;
                    };
                };
            };
        };
        "widgets": {
            "widget_1": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {
                            "cropParam": {
                                "svgViewBox": string;
                                "svgPath": string;
                                "cropShape": string;
                            };
                        };
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_5": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": any;
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_123": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_124": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "type": string;
                    "subOpenMode": string;
                    "icon": {
                        "svg": string;
                        "properties": {
                            "filename": any;
                            "originalName": any;
                            "path": any;
                            "color": string;
                            "size": number;
                            "inlineSvg": boolean;
                        };
                    };
                    "main": {
                        "alignment": string;
                        "space": {
                            "distance": number;
                            "unit": string;
                        };
                        "showText": boolean;
                        "showIcon": boolean;
                        "iconPosition": string;
                        "variants": {
                            "underline": {
                                "item": {
                                    "default": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                    "active": {
                                        "color": string;
                                        "border": {
                                            "color": string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                    "navType": string;
                    "sub": {
                        "variants": any;
                    };
                };
                "version": string;
            };
            "widget_125": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_126": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "uiMode": string;
                    "popup": {
                        "icon": {
                            "svg": string;
                            "properties": {
                                "filename": any;
                                "originalName": any;
                                "path": any;
                                "color": string;
                                "size": number;
                                "inlineSvg": boolean;
                            };
                        };
                        "items": string[];
                        "tooltip": string;
                    };
                    "inline": {
                        "items": string[];
                        "design": {
                            "direction": string;
                            "hideLabel": boolean;
                            "btnRad": string;
                            "btnColor": string;
                            "iconColor": string;
                            "size": string;
                        };
                    };
                };
                "version": string;
            };
            "widget_127": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_128": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_129": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_130": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                        "textExpression": any;
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                            "quickStyleType": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
            "widget_158": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_159": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_160": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_161": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_162": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_163": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_164": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_165": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                    };
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_166": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_167": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_168": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_169": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "boxShadow": {
                        "offsetX": {
                            "distance": number;
                            "unit": string;
                        };
                        "offsetY": {
                            "distance": number;
                            "unit": string;
                        };
                        "blur": {
                            "distance": number;
                            "unit": string;
                        };
                        "spread": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_170": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_171": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_172": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_173": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_174": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_175": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_176": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_177": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_178": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_179": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_180": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                };
            };
            "widget_181": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_182": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_183": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_184": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_185": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_186": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "style": {
                    "border": {
                        "type": string;
                        "color": string;
                        "width": {
                            "distance": number;
                            "unit": string;
                        };
                    };
                };
            };
            "widget_187": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_188": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_189": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_190": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_191": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_192": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_193": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
            };
            "widget_194": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_195": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
            };
            "widget_196": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_197": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_198": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_199": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_200": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_201": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_202": {
                "id": string;
                "uri": string;
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_203": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_204": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_205": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "altText": string;
                        "toolTip": string;
                        "linkParam": {};
                        "scale": string;
                        "imageParam": {};
                    };
                    "styleConfig": {};
                };
                "version": string;
            };
            "widget_206": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                    };
                };
                "version": string;
            };
            "widget_207": {
                "id": string;
                "uri": string;
                "icon": {};
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "color": string;
                    };
                };
            };
            "widget_208": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "space": number[];
                            "unit": string;
                        };
                        "justifyContent": string;
                        "alignItems": string;
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_209": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "text": string;
                    "placeholder": string;
                    "style": {
                        "verticalAlign": string;
                        "wrap": boolean;
                    };
                };
                "version": string;
                "style": {
                    "background": {
                        "image": {
                            "url": string;
                        };
                        "fillType": string;
                    };
                };
            };
            "widget_210": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "space": number;
                    "style": {
                        "padding": {
                            "number": number[];
                            "unit": string;
                        };
                    };
                };
                "version": string;
                "layouts": {
                    "DEFAULT": {
                        "LARGE": string;
                    };
                };
            };
            "widget_211": {
                "id": string;
                "uri": string;
                "icon": {
                    "svg": string;
                    "properties": {
                        "filename": any;
                        "originalName": any;
                        "path": any;
                        "color": string;
                        "size": number;
                        "inlineSvg": boolean;
                    };
                };
                "label": string;
                "config": {
                    "functionConfig": {
                        "text": string;
                        "toolTip": string;
                        "linkParam": {};
                    };
                    "styleConfig": {
                        "name": string;
                        "themeStyle": {
                            "className": string;
                            "color": string;
                        };
                        "customStyle": {
                            "regular": {};
                            "hover": {};
                            "clicked": {};
                        };
                    };
                };
                "version": string;
            };
        };
        "header": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
            };
            "backgroundColor": string;
        };
        "footer": {
            "layout": {
                "LARGE": string;
                "SMALL": string;
            };
            "height": {
                "LARGE": number;
                "SMALL": string;
            };
            "backgroundColor": string;
        };
    };
} | {
    name: string;
    type: LayoutTemplateType;
    icon: any;
    pageId: string;
    config: {
        pages: {
            page_0: {
                id: string;
                label: string;
                mode: PageMode;
                type: PageType;
                layout: {
                    LARGE: string;
                };
            };
        };
        layouts: {
            layout_0: {
                type: LayoutType;
                content: {};
                order: any[];
            };
        };
    };
})[];
