import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const headerTemplate3: {
    type: LayoutTemplateType;
    icon: any;
    config: {
        layouts: {
            layout_2: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                            bottom: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                right: boolean;
                                left: boolean;
                                width: boolean;
                                top: boolean;
                            };
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                    };
                };
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {};
                    };
                    styleConfig: {};
                };
                style: {
                    background: {
                        image: {
                            url: string;
                        };
                        color: string;
                    };
                    borderRadius: {
                        number: number[];
                        unit: string;
                    };
                };
            };
            widget_4: {
                id: string;
                uri: string;
                label: string;
                config: {
                    type: string;
                    subOpenMode: string;
                    icon: {
                        svg: string;
                        properties: {
                            filename: any;
                            originalName: any;
                            path: any;
                            color: string;
                            size: number;
                            inlineSvg: boolean;
                        };
                    };
                    main: {
                        alignment: string;
                        space: {
                            distance: number;
                            unit: string;
                        };
                        showText: boolean;
                        showIcon: boolean;
                        iconPosition: string;
                        variants: {
                            default: {
                                item: {
                                    default: {
                                        bg: string;
                                        color: string;
                                    };
                                    active: {
                                        color: string;
                                        bg: string;
                                    };
                                };
                                bg: string;
                            };
                        };
                    };
                    navType: string;
                    sub: {
                        variants: any;
                    };
                };
            };
        };
        header: {
            layout: {
                LARGE: string;
            };
            height: {
                LARGE: string;
            };
            backgroundColor: string;
        };
    };
};
