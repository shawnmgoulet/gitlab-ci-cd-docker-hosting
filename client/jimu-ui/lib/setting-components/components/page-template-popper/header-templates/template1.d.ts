import { LayoutTemplateType } from 'jimu-layouts/common';
export declare const headerTemplate1: {
    type: LayoutTemplateType;
    icon: any;
    config: {
        layouts: {
            layout_2: {
                type: string;
                order: string[];
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                right: boolean;
                                left: boolean;
                            };
                            hCenter: boolean;
                        };
                    };
                };
            };
            layout_3: {
                type: string;
                content: {
                    0: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: number;
                            height: number;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    1: {
                        type: string;
                        bbox: {
                            left: number;
                            top: string;
                            width: number;
                            height: string;
                        };
                        widgetId: string;
                        setting: {
                            hCenter: boolean;
                            vCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    2: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                        };
                        widgetId: string;
                        isPending: boolean;
                        setting: {
                            vCenter: boolean;
                            hCenter: boolean;
                        };
                    };
                    3: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: string;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                right: boolean;
                                left: boolean;
                            };
                            hCenter: boolean;
                        };
                        isPending: boolean;
                    };
                    4: {
                        type: string;
                        bbox: {
                            left: string;
                            top: string;
                            width: string;
                            height: string;
                            right: number;
                        };
                        widgetId: string;
                        setting: {
                            autoProps: {
                                right: boolean;
                                left: boolean;
                            };
                        };
                    };
                };
                order: string[];
            };
        };
        widgets: {
            widget_1: {
                id: string;
                uri: string;
                label: string;
                config: {
                    functionConfig: {
                        altText: string;
                        toolTip: string;
                        linkParam: {};
                        scale: string;
                        imageParam: {};
                    };
                    styleConfig: {};
                };
            };
            widget_2: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_3: {
                id: string;
                uri: string;
                label: string;
                config: {
                    text: string;
                    placeholder: string;
                    style: {
                        verticalAlign: string;
                        wrap: boolean;
                    };
                };
            };
            widget_4: {
                id: string;
                uri: string;
                label: string;
                config: {
                    type: string;
                    subOpenMode: string;
                    icon: {
                        svg: string;
                        properties: {
                            color: string;
                            size: number;
                        };
                    };
                    main: {
                        alignment: string;
                        space: {
                            distance: number;
                            unit: string;
                        };
                        showText: boolean;
                        showIcon: boolean;
                        iconPosition: string;
                        variants: any;
                    };
                    navType: string;
                    sub: {
                        variants: any;
                    };
                };
            };
            widget_5: {
                id: string;
                uri: string;
                label: string;
                config: {
                    type: string;
                    subOpenMode: string;
                    icon: {
                        svg: string;
                        properties: {
                            filename: any;
                            originalName: any;
                            path: any;
                            color: string;
                            size: number;
                            inlineSvg: boolean;
                        };
                    };
                    main: {
                        alignment: string;
                        space: {
                            distance: number;
                            unit: string;
                        };
                        showText: boolean;
                        showIcon: boolean;
                        iconPosition: string;
                        variants: any;
                    };
                    navType: string;
                    sub: {
                        variants: any;
                    };
                };
                version: string;
            };
        };
        header: {
            layout: {
                LARGE: string;
                SMALL: string;
            };
            height: {
                LARGE: string;
            };
            backgroundColor: string;
        };
    };
};
