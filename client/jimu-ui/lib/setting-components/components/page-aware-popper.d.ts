/// <reference types="react" />
import { PopperProps } from 'jimu-ui';
import { React, ReactRedux, BrowserSizeMode } from 'jimu-core';
interface StateToPopperProps {
    pageId: string;
    sizemode: BrowserSizeMode;
    dispatch?: any;
}
declare class PageAwarePopper extends React.PureComponent<PopperProps & StateToPopperProps> {
    constructor(props: any);
    componentDidUpdate(prevProps: PopperProps & StateToPopperProps): void;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof PageAwarePopper, Pick<PopperProps & StateToPopperProps, "className" | "toggle" | "zIndex" | "offset" | "open" | "container" | "onClick" | "onMouseDown" | "placement" | "reference" | "keepMounted" | "toggleClass" | "modifiers" | "popperOptions" | "generation" | "onCreate"> & PopperProps>;
export default _default;
