/** @jsx jsx */
import { OrderByOption, ThemeVariables, IMUseDataSource, ImmutableArray } from 'jimu-core';
export interface SortSettingOption {
    ruleOptionName: string;
    rule: Array<OrderByOption>;
    isEditOptionName?: boolean;
}
export interface SortSettingProps {
    onChange: (sortData: Array<SortSettingOption>, index?: number) => void;
    useDataSource: IMUseDataSource;
    value: ImmutableArray<SortSettingOption>;
    theme?: ThemeVariables;
    className?: string;
}
export interface SortSettingState {
    option: SortSettingOption;
}
export declare const SortSetting: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
