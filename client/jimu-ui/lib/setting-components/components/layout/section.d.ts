/// <reference types="react" />
import { React } from 'jimu-core';
interface SettingSectionProps {
    title?: string | JSX.Element;
    className?: string;
}
export declare class _SettingSection extends React.PureComponent<SettingSectionProps> {
    render(): JSX.Element;
}
export declare const SettingSection: React.FunctionComponent<SettingSectionProps>;
export {};
