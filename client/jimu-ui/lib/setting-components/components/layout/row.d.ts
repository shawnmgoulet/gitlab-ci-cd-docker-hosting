/// <reference types="react" />
import { React } from 'jimu-core';
declare type ActionType = 'drilldown';
interface SettingRowProps {
    label?: string | JSX.Element;
    flow?: 'wrap' | 'no-wrap';
    action?: ActionType;
    indented?: boolean;
    className?: string;
    truncateLabel?: boolean;
    onDrillDown?: () => void;
}
export declare class _SettingRow extends React.PureComponent<SettingRowProps> {
    onActionClick: (type: "drilldown") => void;
    render(): JSX.Element;
}
export declare const SettingRow: React.FunctionComponent<SettingRowProps>;
export {};
