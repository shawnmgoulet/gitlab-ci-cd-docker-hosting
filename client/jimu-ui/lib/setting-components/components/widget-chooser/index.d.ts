interface CardImage {
    src: string;
    alt?: string;
}
export interface WidgetCardType {
    title: string;
    uri: string;
    image: CardImage;
}
export declare const WidgetChooser: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
