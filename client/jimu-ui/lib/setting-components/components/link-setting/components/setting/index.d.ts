/// <reference types="react" />
/** @jsx jsx */
import { React, IMAppConfig, ImmutableArray, ThemeVariables, BrowserSizeMode, ReactRedux, IntlShape } from 'jimu-core';
import { LinkParam } from '../../types';
interface Props {
    className?: string;
    linkParam?: LinkParam;
    onSettingConfirm?: any;
    okButtonBottom?: string;
    selectAreaMaxHeight?: string;
    isLinkPageSetting?: boolean;
    dataSourceIds?: ImmutableArray<string>;
}
interface States {
    originLinkParam: LinkParam;
    linkParam: LinkParam;
}
interface StateExtraProps {
    appConfig: IMAppConfig;
    browserSizeMode: BrowserSizeMode;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _LinkSetting extends React.PureComponent<Props & ExtraProps & StateExtraProps, States> {
    constructor(props: any);
    componentWillMount(): void;
    render(): JSX.Element;
    getLinkTypeContent: () => JSX.Element;
    componentDidMount(): void;
    getLinkContent: (linkType: string) => JSX.Element;
    linkTypeChange: (e: any) => void;
    radioOpenTypeChange: (openType: string) => void;
    linkParamChange: (changedParam: LinkParam, isTypeSame?: boolean) => void;
    settingConfirm: () => void;
    isLinkParamOk: (linkParam: LinkParam) => boolean;
}
export declare const LinkSetting: ReactRedux.ConnectedComponentClass<React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
}, Pick<import("react-intl").WithIntlProps<any>, string | number | symbol> & Props>;
export {};
