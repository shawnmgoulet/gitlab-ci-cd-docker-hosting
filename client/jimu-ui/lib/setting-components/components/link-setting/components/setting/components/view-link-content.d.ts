/// <reference types="react" />
import { React, IMAppConfig, BrowserSizeMode } from 'jimu-core';
import { LinkParam } from '../../../types';
interface Props {
    linkParam: LinkParam;
    originLinkParam: LinkParam;
    onLinkParamChange: any;
    appConfig: IMAppConfig;
    browserSizeMode: BrowserSizeMode;
    selectAreaMaxHeight?: string;
}
interface States {
    viewLinkParamArr: string[];
    scrollToViewId: string;
}
export default class Widget extends React.PureComponent<Props, States> {
    noScroll: string;
    constructor(props: any);
    componentDidMount(): void;
    static getDerivedStateFromProps(newProps: Props, prevState: States): {
        viewLinkParamArr: string[];
        scrollToViewId: string;
    };
    getInitLinkParam: () => LinkParam;
    pageChange: (e: any) => void;
    viewChange: (viewId: string, sectionId: string) => void;
    viewRemove: (viewId: string) => void;
    onScrollToViewChange: (viewId: string) => void;
    getDefaultPageId: () => string;
    getSelectedVeiwIds: () => string[];
    render(): JSX.Element;
}
export {};
