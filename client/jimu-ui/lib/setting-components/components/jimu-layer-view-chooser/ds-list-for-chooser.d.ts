/// <reference types="react" />
import { React, ImmutableArray } from 'jimu-core';
export interface SelectedDsJson {
    rootDataSourceId: string;
    dataSourceId: string;
}
interface DsListForChooserProps {
    fromRootDsIds: ImmutableArray<string>;
    selectedDsIds: ImmutableArray<string>;
    onSelect?: (selectedDsJson: SelectedDsJson) => void;
}
interface DsListForChooserStates {
}
export declare class DsListForChooser extends React.PureComponent<DsListForChooserProps, DsListForChooserStates> {
    constructor(props: any);
    getContent(): JSX.Element;
    render(): JSX.Element;
}
export {};
