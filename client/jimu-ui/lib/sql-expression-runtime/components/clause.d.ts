/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, DataSource, CodedValue, SqlClause, ClauseValueOptions, IMFieldSchema } from 'jimu-core';
interface Props {
    id: string;
    config: SqlClause;
    selectedDs: DataSource;
    isHosted?: boolean;
    onChange: (clause: SqlClause, id: string) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
}
export declare class _SqlExprClauseRuntime extends React.PureComponent<Props & ExtraProps, State> {
    codedValues: CodedValue[];
    fieldObj: IMFieldSchema;
    constructor(props: any);
    initDatas: () => void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    i18nMessage: (id: string) => string;
    getLabel: () => string;
    onValueOptsChange: (valueOptions: ClauseValueOptions) => void;
    render(): JSX.Element;
}
declare const SqlExpressionClauseRuntime: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default SqlExpressionClauseRuntime;
