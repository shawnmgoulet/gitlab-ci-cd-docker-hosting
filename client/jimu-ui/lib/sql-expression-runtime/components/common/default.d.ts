import { JimuFieldType, ClauseOperator, ClauseSourceType } from 'jimu-core';
export declare enum SqlExpressionMode {
    All = "ALL",
    Simple = "SIMPLE"
}
export declare enum ClauseInputEditor {
    TextInput = "TEXT_BOX",
    FieldSelector = "FIELD_SELECTOR",
    DoubleNumberSelector = "DOUBLE_NUMBER_SELECTOR",
    SimpleSelect = "SIMPLE_SELECT",
    MultipleSelect = "MULTIPLE_SELECT",
    DatePicker = "DATE_PICKER",
    DateTimePicker = "DATE_TIME_PICKER"
}
export declare const normalInputEditorsForValueAndDate: ClauseInputEditor[];
interface ClauseOtherOptions {
    normalInputEditors: Array<ClauseInputEditor>;
    codedValueInputEditors?: Array<ClauseInputEditor>;
    supportAskForValue?: boolean;
    supportCaseSensitive?: boolean;
}
declare type ClauseRelationshipType = {
    [operator in ClauseOperator]: {
        [sourceType in ClauseSourceType]?: ClauseOtherOptions;
    };
};
export declare const ClauseRelationship: ClauseRelationshipType;
export declare function getOperatorsByShortType(shortType: JimuFieldType, isHosted?: boolean): ClauseOperator[];
export {};
