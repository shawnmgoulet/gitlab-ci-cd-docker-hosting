import { ImmutableArray, SqlClause, SqlClauseSet, DataSource, CodedValue, IntlShape, IMFieldSchema, IMSqlExpression } from 'jimu-core';
export interface SqlResult {
    sql: string;
    displaySQL: string;
}
export declare function getClauseArrayByChange(partArray: ImmutableArray<SqlClause | SqlClauseSet>, clause: SqlClause | SqlClauseSet, id: string): ImmutableArray<SqlClause | SqlClauseSet>;
export declare function getCamelCase(name: string): string;
export declare function queryValueLabelsByField(field: IMFieldSchema, selectedDs: DataSource, intl: IntlShape): Promise<CodedValue[]>;
export declare function getJimuFieldNamesBySqlExpression(sqlExprObj: IMSqlExpression): string[];
export declare function isSqlExpressionValid(sqlExprObj: IMSqlExpression): boolean;
