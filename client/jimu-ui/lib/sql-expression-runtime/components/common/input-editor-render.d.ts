/// <reference types="react" />
import { React, DataSource, CodedValue, ClauseValueOptions, IMFieldSchema } from 'jimu-core';
interface Props {
    runtime: boolean;
    valueOptions: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    selectedDs: DataSource;
    codedValues?: CodedValue[];
    onChange: (valueOptions: ClauseValueOptions) => void;
}
interface State {
    inputEditor: any;
}
export declare class _InputEditorRender extends React.PureComponent<Props, State> {
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    _setStateForInputEditor: () => void;
    render(): JSX.Element;
}
export declare const InputEditorRender: typeof _InputEditorRender;
export {};
