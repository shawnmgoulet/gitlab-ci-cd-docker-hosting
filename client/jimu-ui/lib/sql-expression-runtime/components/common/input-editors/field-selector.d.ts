/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, IMFieldSchema, DataSource, Immutable, ClauseValueOptions, JimuFieldType } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    selectedDs: DataSource;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface State {
    type: JimuFieldType;
}
export declare class _VIFieldSelector extends React.PureComponent<Props, State> {
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    getSelectedFields: () => Immutable.ImmutableArray<string>;
    onChange: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>) => void;
    render(): JSX.Element;
}
declare const VIFieldSelector: typeof _VIFieldSelector;
export default VIFieldSelector;
