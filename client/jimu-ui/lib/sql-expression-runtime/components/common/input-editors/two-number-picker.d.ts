/// <reference types="react" />
import { React, IMFieldSchema, IntlShape, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
}
export declare class _VITwoNumberPicker extends React.PureComponent<Props & ExtraProps, State> {
    ref1: any;
    ref2: any;
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onChange: () => void;
    render(): JSX.Element;
}
declare const VITwoNumberPicker: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default VITwoNumberPicker;
