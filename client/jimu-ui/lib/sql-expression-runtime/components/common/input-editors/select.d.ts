/// <reference types="react" />
import { React, DataSource, IMFieldSchema, IntlShape, CodedValue, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    codedValues?: CodedValue[];
    fieldObj?: IMFieldSchema;
    selectedDs: DataSource;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    list: any[];
}
export declare class _VISimpleSelect extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    componentDidMount(): void;
    componentWillUpdate(prevProps: Props, prevState: State): void;
    updateStates: (codedValues: CodedValue[]) => void;
    onValueChange: (e: any) => void;
    render(): JSX.Element;
}
declare const VISimpleSelect: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default VISimpleSelect;
