/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, DataSource, ImmutableArray, SqlClause, SqlClauseSet, IMSqlExpression } from 'jimu-core';
interface Props {
    id: string;
    config: IMSqlExpression;
    selectedDs: DataSource;
    isHosted?: boolean;
    onChange: (clause: SqlClauseSet, id: string) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    partsArray: ImmutableArray<SqlClause | SqlClauseSet>;
}
export declare class _SqlExprClauseSetRuntime extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    i18nMessage: (id: string) => string;
    onClauseChange: (clause: SqlClause, id: string) => void;
    render(): JSX.Element;
}
declare const SqlExpressionClauseSetRuntime: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default SqlExpressionClauseSetRuntime;
