/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { BoxShadowStyle } from 'jimu-ui';
interface Props {
    className?: string;
    value?: BoxShadowStyle;
    onChange?: (value: BoxShadowStyle) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
export declare class _BoxShadowSetting extends React.PureComponent<Props & ExtraProps> {
    static defaultProps: Partial<Props & ExtraProps>;
    _updateShadow(key: string, newValue: any): void;
    translate: (id: string) => string;
    getShadows: () => {
        name: string;
        label: string;
        min: number;
        max: number;
    }[];
    render(): JSX.Element;
}
export declare const BoxShadowSetting: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
