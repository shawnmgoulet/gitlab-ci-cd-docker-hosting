/// <reference types="react" />
import { React } from 'jimu-core';
export declare enum FontFamilyValue {
    AVENIRNEXT = "Avenir Next W01",
    CALIBRI = "Calibri",
    PMINGLIU = "PmingLiu",
    IMPACT = "Impact",
    GEORGIA = "Georgia",
    ARIAL = "Arial",
    TIMESNEWROMAN = "Times New Roman",
    SIMHEI = "SimHei",
    MICROSOFTYAHEI = "Microsoft YaHei"
}
interface Props {
    className?: string;
    style?: React.CSSProperties;
    font?: FontFamilyValue;
    onChange?: (font: string) => void;
}
export declare const fontValue: FontFamilyValue[];
export declare const FontFamilySelector: ({ font, onChange, style, className }: Props) => JSX.Element;
export {};
