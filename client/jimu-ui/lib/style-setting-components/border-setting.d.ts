/// <reference types="react" />
/** @jsx jsx */
import { React, IMThemeVariables } from 'jimu-core';
import { BorderStyle } from '../types';
interface BorderStyleProps {
    className?: string;
    style?: any;
    value?: BorderStyle;
    onChange?: (param: BorderStyle) => void;
}
interface StateToProps {
    appTheme?: IMThemeVariables;
}
export declare class _BorderSetting extends React.PureComponent<BorderStyleProps & StateToProps> {
    static defaultProps: Partial<BorderStyleProps>;
    _updateBorder(key: string, newValue: any): void;
    render(): JSX.Element;
}
export declare const BorderSetting: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
