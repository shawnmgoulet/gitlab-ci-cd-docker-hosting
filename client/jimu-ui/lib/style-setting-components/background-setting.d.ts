/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, IMThemeVariables } from 'jimu-core';
import { BackgroundStyle, FillType, ImageParam } from 'jimu-ui';
interface Props {
    background?: BackgroundStyle;
    onChange?: (background: BackgroundStyle) => void;
    className?: string;
    style?: any;
}
interface ExtraProps {
    intl: IntlShape;
}
interface StateToProps {
    appTheme?: IMThemeVariables;
}
export declare class _BackgroundSetting extends React.PureComponent<Props & ExtraProps & StateToProps> {
    fileInput: React.RefObject<any>;
    static defaultProps: Partial<Props & ExtraProps & StateToProps>;
    constructor(props: any);
    openBrowseImage: (imageParam: ImageParam) => void;
    _onPositionChange: (e: any) => void;
    _onColorChange: (color: string) => void;
    nls: (id: string) => string;
    getFillType: () => {
        value: FillType;
        label: string;
    }[];
    render(): JSX.Element;
}
export declare const BackgroundSetting: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
