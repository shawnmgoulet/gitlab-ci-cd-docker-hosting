/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { SelectedDataSourceJson } from '../../../types';
interface Props {
    dsJsonWithRootId: SelectedDataSourceJson;
    onRemove?: (dsJson: SelectedDataSourceJson) => void;
    disableRemove?: boolean;
    hideRemove?: boolean;
    showFilter?: boolean;
    showSort?: boolean;
    className?: string;
    onClickFilter?: (dsId: string) => void;
    onClickSort?: (dsId: string) => void;
}
export default class DsItem extends React.PureComponent<Props & {
    theme: ThemeVariables;
}, {}> {
    constructor(props: any);
    onRemove: () => void;
    onFilterClick: () => void;
    onSortClick: () => void;
    getDsLabel: (ds: SelectedDataSourceJson) => string;
    render(): JSX.Element;
}
export {};
