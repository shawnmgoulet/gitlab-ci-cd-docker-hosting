/// <reference types="react" />
import { DataSource, React, ImmutableArray, IntlShape, DataSourceManager } from 'jimu-core';
import { SelectedDataSourceJson } from '../../../types';
interface State {
    isTypeDropdownOpen: boolean;
    selectedErrorDss: string[];
}
interface Props {
    intl: IntlShape;
    showErrorDss?: boolean;
    singleDss?: DataSource[];
    groupedDss?: {
        [groupId: string]: DataSource[];
    };
    selectedDsIds?: ImmutableArray<string>;
    getGroupLabelById: (groupId: string) => string;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    changeHasErrorSelectedDss?: (hasErrorDss: boolean) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class ToUseDsList extends React.PureComponent<Props, State> {
    dsManager: DataSourceManager;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props): void;
    getWhetherShowSingleDss: () => boolean;
    getWhetherShowGroupedDss: () => boolean;
    getSelectedErrorDss: () => string[];
    render(): JSX.Element;
}
export {};
