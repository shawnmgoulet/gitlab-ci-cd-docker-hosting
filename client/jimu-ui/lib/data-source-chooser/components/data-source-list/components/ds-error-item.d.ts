/// <reference types="react" />
import { React, ImmutableArray, IntlShape } from 'jimu-core';
import { SelectedDataSourceJson } from '../../../types';
interface Props {
    dsId: string;
    intl: IntlShape;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    selectedDsIds?: ImmutableArray<string>;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
interface State {
}
export default class DsErrorItem extends React.PureComponent<Props, State> {
    getWhetherSelected: () => boolean;
    getDsJsonWithRootId: () => SelectedDataSourceJson;
    onItemClick: () => void;
    render(): JSX.Element;
}
export {};
