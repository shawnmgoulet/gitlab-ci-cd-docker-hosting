import { ImmutableObject } from 'jimu-core';
import { ImageParam } from 'jimu-ui';
import { BaseTextStyle } from 'jimu-ui/rich-text-editor';
export declare enum LineType {
    SOLID = "solid",
    DASHED = "dashed",
    DOTTED = "dotted",
    DOUBLE = "double"
}
export declare enum ThemeColors {
    PRIMARY = "primary",
    SECONDARY = "secondary",
    SUCCESS = "success",
    INFO = "info",
    WARNING = "warning",
    LIGHT = "light",
    DANGER = "danger",
    DARK = "dark",
    WHITE = "white"
}
export declare enum FillType {
    FIT = "fit",
    FILL = "fill",
    CENTER = "center",
    TILE = "tile",
    STRETCH = "stretch"
}
export declare enum UnitTypes {
    PIXEL = "px",
    REM = "rem",
    EM = "em",
    PERCENTAGE = "%"
}
export declare enum BorderSides {
    TL = "TL",
    TR = "TR",
    BR = "BR",
    BL = "BL"
}
export declare enum Sides {
    T = "T",
    R = "R",
    B = "B",
    L = "L"
}
export interface LinearUnit {
    distance: number;
    unit: UnitTypes;
}
export declare type IMLinearUnit = ImmutableObject<LinearUnit>;
export interface FourSidesUnit {
    unit: UnitTypes;
    number: Array<number>;
}
export interface BorderStyle {
    type: LineType;
    color: string;
    width: LinearUnit;
}
export declare enum BoxShadowValues {
    OffsetX = "OFFSETX",
    OffsetY = "OFFSETY",
    BlurRadius = "BLUERADIUS",
    SpreadRadius = "SPREADRADIUS"
}
export interface BoxShadowStyle {
    offsetX: LinearUnit;
    offsetY: LinearUnit;
    blur: LinearUnit;
    spread: LinearUnit;
    color: string;
}
export interface WidthStyle {
    value?: LinearUnit;
}
export interface BackgroundStyle {
    color?: string;
    fillType: FillType;
    image?: ImageParam;
}
export declare type HeightStyle = WidthStyle;
export interface StyleSettings {
    backgroundColor?: string;
    background?: BackgroundStyle;
    margin?: FourSidesUnit;
    padding?: FourSidesUnit;
    border?: BorderStyle;
    boxShadow?: BoxShadowStyle;
    borderRadius?: FourSidesUnit;
    width?: WidthStyle;
    height?: HeightStyle;
    text?: BaseTextStyle;
}
