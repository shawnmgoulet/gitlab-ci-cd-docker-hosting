/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, DataSourceJson, ThemeVariables, ImmutableObject, ImmutableArray, DataSourceManager, IntlShape, Immutable, IMDataSourceInfo, DataSource, UseDataSource, SqlExpression, OrderByOption } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../../../types';
import { IMSortConfig, IMFilterConfig } from '..';
interface State {
    isDataSourceInited: boolean;
    currentDsId: string;
    isSqlExprShow: boolean;
    modalType: 'none' | 'ds' | 'sort';
}
interface Props {
    types: ImmutableArray<AllDataSourceTypes>;
    selectedDataSourceIds?: ImmutableArray<string>;
    isMultiple?: boolean;
    closeDataSourceListOnSelect?: boolean;
    filterEnabled?: boolean;
    sortEnabled?: boolean;
    sortConfig?: IMSortConfig;
    filterConfig?: IMFilterConfig;
    onSelect?: (selectedDsJsons: SelectedDataSourceJson[], selectedDsJson?: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJsons: SelectedDataSourceJson[], removedDsJson?: SelectedDataSourceJson) => void;
    disableSelection?: (selectedDsJsons: SelectedDataSourceJson[]) => boolean;
    disableRemove?: (selectedDsJsons: SelectedDataSourceJson[]) => boolean;
    onFilterChange?: (sqlExprObj: SqlExpression, dsId: string) => void;
    onSortChange?: (sortData: Array<OrderByOption>, dsId: string) => void;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
interface StateExtraProps {
    dataSources: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    dataSourcesInfo: ImmutableObject<{
        [dsId: string]: IMDataSourceInfo;
    }>;
}
declare class _SetDsBtnDsList extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    panelWidth: string;
    defaultModalStyle: {
        position: string;
        top: string;
        bottom: string;
        right: string;
        left: string;
        width: string;
        height: string;
        zIndex: string;
        borderRight: string;
    };
    modalStyle: object;
    dsManager: DataSourceManager;
    constructor(props: any);
    componentDidUpdate(prevProps: Props & ExtraProps & StateExtraProps): void;
    showSqlExprPopup: () => void;
    toggleSqlExprPopup: () => void;
    onSqlExprBuilderChange: (sqlExprObj: Immutable.ImmutableObject<SqlExpression>) => void;
    onSortChange: (sortData: OrderByOption[], index?: number) => void;
    onClickFilter: (dsId: string) => void;
    onClickSort: (dsId: string) => void;
    getDataSource: (dsId: string) => DataSource;
    changeInitStatus: (isInited: boolean) => void;
    disableSelection: () => boolean;
    disableRemove: () => boolean;
    getDsByDsId: (dsId: string) => DataSource;
    getDssWithRootId: (dataSourceIds: string[]) => SelectedDataSourceJson[];
    getModalStyle: () => {
        position: string;
        top: string;
        bottom: string;
        right: string;
        left: string;
        width: string;
        height: string;
        zIndex: string;
        borderRight: string;
    };
    onModalClose: () => void;
    onDsSelected: (selectedDsJson: SelectedDataSourceJson) => void;
    onDsRemoved: (removedDsJson: SelectedDataSourceJson) => void;
    toggleDsList: () => void;
    getSingleUsedDs: (dsJson: SelectedDataSourceJson) => UseDataSource;
    render(): JSX.Element;
}
declare const _default: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_SetDsBtnDsList>, "theme">>;
export default _default;
