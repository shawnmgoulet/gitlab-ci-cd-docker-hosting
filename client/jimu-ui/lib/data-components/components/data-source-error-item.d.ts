/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, ImmutableObject, IMDataSourceJson, WidgetJson, DataSourceJson, ReactRedux } from 'jimu-core';
interface Props {
    dataSourceJson: IMDataSourceJson;
    dataSources?: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    isMoreIconShown?: boolean;
    isCloseIconShown?: boolean;
    isRelatedWidgetsShown?: boolean;
    isOriginalUrlShown?: boolean;
    isSelected?: boolean;
    onMoreIconClick?: (dsJson: IMDataSourceJson) => void;
    onCloseIconClick?: (dsJson: IMDataSourceJson) => void;
    onDataSourceItemClick?: (dsJson: IMDataSourceJson) => void;
    className?: string;
}
interface StateExtraProps {
    portalUrl: string;
    widgets?: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
}
export declare class _DataSourceErrorItem extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    rootDom: HTMLElement;
    constructor(props: any);
    onMoreIconClick: (e: any) => void;
    onCloseIconClick: (e: any) => void;
    onDataSourceItemClick: (e: any) => void;
    stopPropagation: (e: any) => void;
    render(): JSX.Element;
}
export declare const DataSourceErrorItem: ReactRedux.ConnectedComponentClass<React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
}, Pick<import("react-intl").WithIntlProps<any>, string | number | symbol> & Props>;
export {};
