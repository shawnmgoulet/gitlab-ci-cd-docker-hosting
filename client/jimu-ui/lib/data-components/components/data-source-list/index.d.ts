/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableArray, IntlShape } from 'jimu-core';
import { AllDataSourceTypes, SelectedDataSourceJson } from '../../types';
interface State {
    isExternalDsShown: boolean;
}
interface Props {
    /**
     * whether data sources are inited
     */
    isDataSourceInited: boolean;
    /**
     * supported data source types
     */
    types: ImmutableArray<AllDataSourceTypes>;
    /**
     * only list child data sources generated by these root data sources
     */
    fromRootDsIds?: ImmutableArray<string>;
    /**
     * id of selected data sources
     */
    selectedDsIds?: ImmutableArray<string>;
    /**
     * whether hide header of the component
     */
    hideHeader?: boolean;
    /**
     * whether to support multiple selection
     */
    isMultiple?: boolean;
    /**
     * before selecting, the component will call this method to check if it can continue selecting
     */
    disableSelection?: boolean;
    /**
     * before remove, the component will call this method to check if it can continue removing
     */
    disableRemove?: boolean;
    /**
     * callback when one data source is selected
     */
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    /**
     * callback when one data source is removed
     */
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    /**
     * callback when close icon is clicked
     */
    onCloseClick?: () => void;
    /**
     * change data source init status, it's necessary because that user can add new data source in the componet
     * and data source init status will change whenever new data sources are added
     */
    changeInitStatus: (isInited: boolean) => void;
    /**
     * only used by theme
     */
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
export declare class _DataSourceList extends React.PureComponent<Props & ExtraProps, State> {
    externalDsStyle: {
        width: string;
        height: string;
        maxWidth: string;
        margin: number;
    };
    fontSizeStyle: {
        fontSize: string;
    };
    titleStyle: {
        height: string;
        borderBottom: string;
    };
    constructor(props: any);
    showExternalDs: () => void;
    onSelectDataFinished: (dsJsons: import("seamless-immutable").ImmutableObject<import("jimu-core").DataSourceJson>[]) => void;
    onSelectDataCanceled: () => void;
    onToggleExternalDs: () => void;
    getWhetherOnlyUseAddedData(types: ImmutableArray<AllDataSourceTypes>): boolean;
    getWhetherOnlyUseWidgetOutput(types: ImmutableArray<AllDataSourceTypes>): boolean;
    getWhetherOneTypeIsOutput(type: string): boolean;
    ExternalDs: JSX.Element;
    render(): JSX.Element;
}
declare const DataSourceList: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default DataSourceList;
