/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { DataSource, React, IMWidgetJson, ImmutableArray, IntlShape, Immutable } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../../../types';
interface State {
    selectedType: AllDataSourceTypes;
    selectedWidgetId: string;
    toUseRootDss: {
        [widgetId: string]: DataSource[];
    };
    toUseChildDss: {
        [rootDsId: string]: DataSource[];
    };
    rootDss: {
        [widgetId: string]: DataSource[];
    };
    childDss: {
        [rootDsId: string]: DataSource[];
    };
    toUseWidgets: IMWidgetJson[];
    hasErrorSelectedDss: boolean;
}
interface Props {
    isDataSourceInited: boolean;
    types: ImmutableArray<AllDataSourceTypes>;
    intl: IntlShape;
    fromRootDsIds?: ImmutableArray<string>;
    selectedDsIds?: ImmutableArray<string>;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class DataSourceWidgetOutputs extends React.PureComponent<Props, State> {
    __unmount: boolean;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    componentWillUnmount(): void;
    initData: () => void;
    setToUseDataSources: (usedTypes: Immutable.ImmutableArray<AllDataSourceTypes>, fromRootDsIds: Immutable.ImmutableArray<string>) => void;
    setDataSources: () => void;
    setSelectedType: (types: Immutable.ImmutableArray<AllDataSourceTypes>) => void;
    setSelectedWidgetId: (widgetId: string) => void;
    setHasErrorDss: (hasErrorDss: boolean) => void;
    getRootDss: () => {
        [widgetId: string]: DataSource[];
    };
    getToUseWidgets: (toUseRootDss: {
        [widgetId: string]: DataSource[];
    }, toUseChildDss: {
        [rootDsId: string]: DataSource[];
    }) => Immutable.ImmutableObject<import("jimu-core").WidgetJson>[];
    getToUseRootDss: (usedTypes: Immutable.ImmutableArray<string>, fromRootDsIds: Immutable.ImmutableArray<string>) => {
        [widgetId: string]: DataSource[];
    };
    getToUseChildDss: (usedTypes: Immutable.ImmutableArray<string>, fromRootDsIds: Immutable.ImmutableArray<string>) => Promise<{
        [rootDsId: string]: DataSource[];
    }>;
    getToUseChildDssByWidgetId: (wId: string) => {
        [rootDsId: string]: DataSource[];
    };
    getToUseTypes: () => Immutable.ImmutableArray<AllDataSourceTypes>;
    getWhetherShowList: () => boolean;
    onWidgetClick: (e: any, wId: string) => void;
    onBackClick: () => void;
    render(): JSX.Element;
}
export {};
