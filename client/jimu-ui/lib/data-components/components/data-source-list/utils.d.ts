import { DataSource, IMDataSourceJson, ImmutableArray } from 'jimu-core';
export declare function traverseGetToUseDss(parentDs: DataSource, toUseDss: DataSource[], toUseTypes: ImmutableArray<string>): void;
export declare function getWhetherUseDataSource(dsJson: IMDataSourceJson, toUseTypes: ImmutableArray<string>): boolean;
export declare function getToUseChildDss(rootDss: DataSource[], toUseTypes: ImmutableArray<string>): Promise<{
    [rootDsId: string]: DataSource[];
}>;
export declare function getSortedKeys(obj: object): string[];
export declare function getSortedArrayByLabel<T extends {
    label: string;
}>(arr: T[]): T[];
export declare function getSortedLabels(labels: string[]): string[];
