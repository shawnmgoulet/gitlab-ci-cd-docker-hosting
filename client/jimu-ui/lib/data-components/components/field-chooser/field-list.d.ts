/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, FieldSchema, IMFieldSchema, DataSource, ImmutableArray, ImmutableObject } from 'jimu-core';
interface Props {
    isDropdown?: boolean;
    fields: ImmutableObject<{
        [jimuName: string]: FieldSchema;
    }>;
    ds: DataSource;
    onSelect?: (field: IMFieldSchema, ds: DataSource) => void;
    selectedFields?: ImmutableArray<string>;
}
interface State {
    isOpen: boolean;
}
export default class FieldList extends React.PureComponent<Props, State> {
    constructor(props: any);
    onFieldSelect: (f: import("seamless-immutable").ImmutableObject<FieldSchema>) => void;
    toggle: () => void;
    onSearchChange: (e: any) => void;
    render(): JSX.Element;
}
export {};
