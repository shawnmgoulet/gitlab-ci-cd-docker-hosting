import { IMDataSourceJson } from 'jimu-core';
export declare function isFeatureLayerUrl(url: string): boolean;
export declare const FEATURE_LAYER_TYPE = "Feature Layer";
export declare function getDsJsonUrl(url: any, layer: any, addIdToEnd: any): string;
export declare function getSingleDsJsonFromLayer(url: any, dsJsonId: any, layer: any, addIdToEndOfUrl: any): IMDataSourceJson;
