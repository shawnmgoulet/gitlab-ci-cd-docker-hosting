/// <reference types="react" />
/** @jsx jsx */
import { React, IMDataSourceJson, IntlShape, ImmutableObject, WidgetJson, ReactRedux } from 'jimu-core';
interface Props {
    dataSourceJson: IMDataSourceJson;
    isMoreIconShown?: boolean;
    isCloseIconShown?: boolean;
    isRenameInputShown?: boolean;
    isRelatedWidgetsShown?: boolean;
    isMappingIconShown?: boolean;
    isOriginalUrlShown?: boolean;
    isSelected?: boolean;
    onDataSourceItemClick?: (dsJson: IMDataSourceJson) => void;
    onMappingIconClick?: (dsJson: IMDataSourceJson) => void;
    onCloseIconClick?: (dsJson: IMDataSourceJson) => void;
    onMoreIconClick?: (dsJson: IMDataSourceJson) => void;
    onRename?: (dsLabel: string) => void;
    className?: string;
}
interface StateExtraProps {
    portalUrl: string;
    widgets?: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    newLabel: string;
}
export declare class _DataSourceItem extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    rootDom: HTMLElement;
    renameInput: HTMLInputElement;
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    onLabelChange: (e: any) => void;
    onMoreIconClick: (e: any) => void;
    onCloseIconClick: (e: any) => void;
    onRename: () => void;
    onDataSourceItemClick: (e: any) => void;
    onMappingIconClick: (e: any) => void;
    stopPropagation: (e: any) => void;
    render(): JSX.Element;
}
export declare const DataSourceItem: ReactRedux.ConnectedComponentClass<React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
}, Pick<import("react-intl").WithIntlProps<any>, string | number | symbol> & Props>;
export {};
