import { Editor, QuillSelection, UnprivilegedEditorWithCustom, QuillValue, Sources, FormatType, Formats, Delta } from './type';
import { TextFormatTypes } from './type';
export declare const getFixedPosition: (quill: Editor, index: number, length?: number) => ClientRect;
export declare const setEditorReadOnly: (quill: Editor, value: boolean) => void;
export declare const setEditorContents: (quill: Editor, value: QuillValue, source?: Sources) => void;
export declare const setEditorCursorEnd: (quill: Editor, source?: Sources) => void;
export declare const setEditorContentSelection: (quill: Editor, source?: Sources) => void;
export declare const setEditorSelection: (quill: Editor, range: QuillSelection, source?: Sources) => void;
export declare const setEditorTabIndex: (quill: Editor, tabIndex: number) => void;
export declare const makeUnprivilegedEditor: (quill: Editor) => UnprivilegedEditorWithCustom;
export interface FormatParams {
    type: FormatType;
    key: TextFormatTypes;
    value: any;
    selection?: QuillSelection;
    source?: Sources;
}
export declare const formatText: (quill: Editor, formatParams: FormatParams) => Delta;
export declare const getFormat: (quill: Editor, range?: QuillSelection, focus?: boolean) => Formats;
export declare const handlingexceptionalFormats: (formats: Formats) => Formats;
export declare const convertExceptionalLinkValue: (formats: Formats) => Formats;
export declare const isDelta: (value: any) => boolean;
export declare const isEditorPropsEqual: (value: any, nextValue: any) => boolean;
export declare const isSameContent: (currentContents: any, nextContents: any) => boolean;
