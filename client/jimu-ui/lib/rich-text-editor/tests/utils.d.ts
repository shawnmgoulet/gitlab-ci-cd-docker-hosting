import { Editor } from '../type';
export declare const getQuillInstance: (wrapper: any) => Editor;
export declare const getQuillDOMNode: (wrapper: any) => any;
export declare const getQuillContentsAsHTML: (wrapper: any) => string;
export declare const setQuillContentsFromHTML: (wrapper: any, html: any) => void;
