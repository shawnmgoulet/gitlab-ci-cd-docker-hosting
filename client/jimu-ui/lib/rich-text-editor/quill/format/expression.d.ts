declare const Embed: any;
import { QuillExpressionValue } from '../../type';
export declare const EXPRESSION_ATTRIBUTES: string[];
export declare class Expression extends Embed {
    static blotName: string;
    static tagName: string;
    domNode: any;
    declareClass: 'Expression';
    static create(value?: QuillExpressionValue): any;
    static formats(domNode: any): {};
    static value(domNode: any): any;
    format(name: any, value: any): void;
}
export {};
