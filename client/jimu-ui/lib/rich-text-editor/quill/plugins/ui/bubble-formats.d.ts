/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, ImmutableArray } from 'jimu-core';
import { FormatType, Formats, TextFormatTypes } from '../../../type';
export declare const TEXT_COLOR_POPPER_CLASS_NAME = "text-color-picker-popper";
export interface BubbleFormatsPorps {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormatTypes, value: any, type: FormatType) => void;
    disableLink?: boolean;
}
interface ExtraProps2 {
    intl: IntlShape;
}
export declare class _BubbleFormats extends React.PureComponent<BubbleFormatsPorps & ExtraProps2> {
    static defaultProps: Partial<BubbleFormatsPorps & ExtraProps2>;
    getStyle: () => import("jimu-core").SerializedStyles;
    translate: (id: string) => string;
    render(): JSX.Element;
}
export declare const BubbleFormats: React.ForwardRefExoticComponent<Pick<BubbleFormatsPorps & ExtraProps2, "onChange" | "className" | "style" | "dataSourceIds" | "formats" | "disableLink"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<BubbleFormatsPorps & ExtraProps2>;
};
export {};
