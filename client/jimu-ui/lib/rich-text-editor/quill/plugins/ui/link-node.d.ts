/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableArray } from 'jimu-core';
import { Formats, TextFormatTypes } from '../../../type';
import { FormatType, QuillLinkValue } from '../../../type';
import { LinkParam } from 'jimu-ui/setting-components';
export declare type LinkNodeOption = {
    open: boolean;
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormatTypes, value: QuillLinkValue, type: FormatType, id?: string) => void;
    onClose?: () => void;
};
export declare class LinkNode extends React.PureComponent<LinkNodeOption> {
    getStyle: () => import("jimu-core").SerializedStyles;
    getDataSourceIdString: (link: LinkParam) => string;
    onSettingConfirm: (link: LinkParam) => void;
    render(): JSX.Element;
}
