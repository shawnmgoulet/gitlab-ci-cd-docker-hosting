/// <reference types="react" />
/** @jsx jsx */
import { React, Expression as ExpressionValue, ImmutableArray } from 'jimu-core';
import { ExpressionFrom } from 'jimu-ui/expression-builder';
import { FormatType, Formats, TextFormatTypes } from '../../../type';
export interface ExpressionNodeProps {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormatTypes, value: any, type: FormatType, id?: string) => void;
}
export declare class ExpressionNode extends React.PureComponent<ExpressionNodeProps> {
    from: ExpressionFrom[];
    static defaultProps: Partial<ExpressionNodeProps>;
    private getStyle;
    onExpressionChange: (expression: ExpressionValue) => void;
    render(): JSX.Element;
}
