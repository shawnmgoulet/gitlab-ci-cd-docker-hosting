/// <reference types="react" />
/// <reference types="react-draggable" />
/** @jsx jsx */
import { React, Expression as ExpressionValue, ImmutableArray, ImmutableObject } from 'jimu-core';
import { Editor, FormatType, HeaderProps, Sources, QuillSelection, Formats, TextFormatTypes } from '../../type';
import { Placement } from 'jimu-ui';
export interface Expressions {
    [x: string]: ExpressionValue;
}
export declare type IMExpressions = ImmutableObject<Expressions>;
interface State {
    position: {
        x: number;
        y: number;
    };
    formats: Formats;
}
interface InjectProps {
    quill: Editor;
}
export declare type AroundExpressionOption = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    header?: HeaderProps;
    open?: boolean;
    placement?: Placement;
    zIndex?: number;
    source?: Sources;
};
export declare class _AroundExpression extends React.PureComponent<AroundExpressionOption & InjectProps, State> {
    static defaultProps: Partial<AroundExpressionOption & InjectProps>;
    debounce: any;
    selection: QuillSelection;
    constructor(props: any);
    componentDidMount(): void;
    onEditorChange: () => void;
    onDragStop: (data: import("react-draggable").DraggableData) => void;
    onPopperCreate: (data: import("popper.js").default.Data) => void;
    getSelection: () => QuillSelection;
    getDataSourceIdString: (expression: ExpressionValue) => string;
    handleChange: (key: TextFormatTypes, expression: ExpressionValue, type: FormatType) => void;
    prepareFormats: (formats: Formats) => Formats;
    updateExpression: (uniqueid: string, dsid: string, name: string, expression: ExpressionValue, selection: QuillSelection) => void;
    insertExpression: (uniqueid: string, dsid: string, name: string, expression: ExpressionValue, selection: QuillSelection) => void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export declare const AroundExpression: React.ComponentType<any>;
export {};
