/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableArray } from 'jimu-core';
import { Editor, FormatType, QuillSelection, Sources, Formats, TextFormatTypes } from '../../type';
import { VirtualReference } from 'jimu-ui';
interface State {
    show?: boolean;
    bounds?: ClientRect;
    formats: Formats;
}
interface InjectProps {
    quill: Editor;
}
export declare type BubbleOption = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    /**
     * Some times, the fomrats we got from quill does not contain enough information.
     * For example, for expression format, we only save id in quill object, But its details
     *  are stored in the wdiegt config.
     *  So in this way, you can mix some information into formats by `mixFormats`.
     */
    mixFormats?: (formats: Formats) => Formats;
    zIndex?: number;
    source?: Sources;
};
export declare class _Bubble extends React.PureComponent<BubbleOption & InjectProps, State> {
    static defaultProps: Partial<BubbleOption & InjectProps>;
    debounce: any;
    selection: QuillSelection;
    constructor(props: any);
    componentDidMount(): void;
    prepareFormats: (formats: Formats) => Formats;
    onEditorChange: () => void;
    getSelection: () => QuillSelection;
    updateFormats: (quill: Editor, selection: QuillSelection) => void;
    updateBounds: (quill: Editor, selection: QuillSelection) => void;
    isSameBounds: (bounds1: ClientRect, bounds2: ClientRect, allowedOffset: number) => boolean;
    approximatelyEqualTo: (number1: number, number2: number, allowedOffset: number) => boolean;
    mixinBounds: (bounds: ClientRect) => ClientRect;
    getReference: () => VirtualReference;
    handleChange: (key: TextFormatTypes, value: any, type: FormatType) => void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export declare const Bubble: React.ComponentType<any>;
export {};
