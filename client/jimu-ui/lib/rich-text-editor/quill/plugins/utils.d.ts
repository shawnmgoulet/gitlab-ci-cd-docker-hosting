import { Operation, Delta, QuillLinkValue } from '../..';
export declare const getExpressionOps: (delta: Delta) => Operation[];
export declare const isQuillExpression: (op: Operation) => boolean;
export declare const getQuillExpressionId: (op: Operation) => string;
export declare const getLinkOps: (delta: Delta) => Operation[];
export declare const getUniqueLinkOps: (delta: Delta, contents: Delta) => Operation[];
export declare const getQuillLink: (op: Operation) => QuillLinkValue;
export declare const isQuillLink: (op: Operation) => boolean;
export declare const isUniqueLink: (op: Operation, contents: Delta) => boolean;
export declare const isInCurrentContents: (linkid: string, contents: Delta) => boolean;
export declare const isLinkOp: (contents: Delta, op: Operation) => boolean;
