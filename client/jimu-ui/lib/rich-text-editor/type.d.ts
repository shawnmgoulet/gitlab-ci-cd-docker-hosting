/// <reference types="react" />
import { React, SerializedStyles, Expression } from 'jimu-core';
import { PopperProps, LinkTarget } from 'jimu-ui';
import { ImmutableArray, ImmutableObject } from 'seamless-immutable';
import { Blot } from 'parchment/dist/src/blot/abstract/blot';
import { BubbleOption, AroundExpressionOption } from './quill/plugins';
import { LinkParam } from 'jimu-ui/setting-components';
export declare type IMLinkParamMap = ImmutableObject<{
    [id: string]: LinkParam;
}>;
export declare enum FormatType {
    INLINE = 0,
    BLOCK = 1,
    EMBED = 2
}
export declare type Formats = {
    [x: string]: any;
};
export declare type IMFormats = ImmutableObject<Formats>;
export declare type Operation = {
    insert?: string | object;
    delete?: number;
    retain?: number;
    attributes?: Formats;
};
export interface Delta {
    ops: Operation[];
    length?: () => any;
    diff?: (delta: Delta) => Delta;
    partition?: (func: (op: Operation) => boolean) => [Operation[], Operation[]];
    filter?: (func: (op: Operation) => boolean) => Operation[];
    forEach?: (func: (op: Operation) => void) => void;
}
export declare type QuillValue = Delta | string;
export interface QuillSelection {
    index: number;
    length: number;
}
export interface ClipboardStatic {
    convert(html?: string): Delta;
    addMatcher(selectorOrNodeType: string | number, callback: (node: any, delta: Delta) => Delta): void;
    dangerouslyPasteHTML(html: string, source?: Sources): void;
    dangerouslyPasteHTML(index: number, html: string, source?: Sources): void;
}
export interface Key {
    key: string | number;
    shortKey?: boolean;
}
export interface KeyboardStatic {
    addBinding(key: Key, callback: (range: QuillSelection, context: any) => void): void;
    addBinding(key: Key, context: any, callback: (range: QuillSelection, context: any) => void): void;
    bindings: {
        [x: string]: any;
    };
}
export declare type UnprivilegedEditor = {
    focus: () => void;
    blur: () => void;
    getLength: () => number;
    getText: (index?: number, length?: number) => string;
    getContents: (index?: number, length?: number) => Delta;
    getSelection: (focus?: boolean) => QuillSelection | null;
    getBounds: (index: number, length?: number) => ClientRect;
    getFormat: (range?: QuillSelection) => Formats;
    getIndex: (blot: any) => number;
    getLeaf: (index: number) => [any, number];
};
export interface CustomEditorMethod {
    getHTML: () => string;
}
export declare type UnprivilegedEditorWithCustom = UnprivilegedEditor & CustomEditorMethod;
export declare type TextChangeHandler = (delta: Delta, oldContents: Delta, source: Sources) => any;
export declare type SelectionChangeHandler = (range: QuillSelection, oldRange: QuillSelection, source: Sources) => any;
export declare type EditorChangeHandler = ((name: 'text-change', delta: Delta, oldContents: Delta, source: Sources) => any) | ((name: 'selection-change', range: QuillSelection, oldRange: QuillSelection, source: Sources) => any);
export declare type EventType = 'text-change' | 'selection-change' | 'editor-change' | 'scroll-optimize';
export declare type Hanldes = TextChangeHandler | SelectionChangeHandler | EditorChangeHandler;
export interface EventEmitter {
    on: (eventName: EventType, handler?: Hanldes) => EventEmitter;
    off: (eventName: EventType, handler?: Hanldes) => EventEmitter;
    once: (eventName: EventType, handler?: Hanldes) => EventEmitter;
}
export interface QuillProperties {
    options?: {
        [x: string]: any;
    };
    editor?: any;
}
export interface InsertText {
    insertText(index: number, text: string, source?: Sources): Delta;
    insertText(index: number, text: string, format: string, value: any, source?: Sources): Delta;
    insertText(index: number, text: string, formats: {
        [x: string]: any;
    }, source?: Sources): Delta;
}
export declare type Editor = {
    root: HTMLDivElement;
    scroll: Blot;
    clipboard: ClipboardStatic;
    keyboard: KeyboardStatic;
    hasFocus: () => boolean;
    disable: () => void;
    enable: (enabled?: boolean) => void;
    setContents: (delta: Delta, source?: Sources) => Delta;
    updateContents: (delta: Delta, source?: Sources) => Delta;
    setSelection: (range: QuillSelection | number, source?: Sources) => void;
    getLeaf: (index: number) => any;
    deleteText: (index: number, length: number, source?: Sources) => Delta;
    insertEmbed: (index: Number, type: String, value: any, source?: Sources) => Delta;
    format: (name: string, value: any, source?: Sources) => Delta;
    formatLine: (index: number, length: number, format: string, value: any, source?: Sources) => Delta;
    formatText: (index: number, length: number, format: string, value: any, source?: Sources) => Delta;
    removeFormat: (index: number, length: number, source?: Sources) => Delta;
    getLine: (index: number) => any;
    getLines(index?: number, length?: number): any[];
} & UnprivilegedEditor & EventEmitter & QuillProperties & InsertText;
export declare type Sources = 'api' | 'user' | 'silent';
export declare type RenderPlugin = (quill: Editor, option: PluginOption) => React.ReactNode;
export declare type PluginOption = BubbleOption | AroundExpressionOption;
export declare type Plugins = {
    bubble?: BubbleOption;
    expression?: AroundExpressionOption;
};
export interface QuillOptions {
    debug?: string | boolean;
    bounds?: HTMLElement | string;
    formats?: string[];
    modules?: {
        [x: string]: any;
    };
    plugins?: Plugins;
    placeholder?: string;
    readOnly?: boolean;
    scrollingContainer?: HTMLElement | string;
    tabIndex?: number;
    theme?: string;
}
export declare type _RichTextEditorProps = {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    injectStyle?: SerializedStyles;
    value?: QuillValue;
    defaultValue?: QuillValue;
    enabled?: boolean;
    children?: any;
    preserveWhitespace: boolean;
    onChange?: (value: QuillValue, delta: Delta, source: Sources, editor: UnprivilegedEditor) => any;
    onChangeSelection?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onCreate?: (editor: Editor) => void;
    onUnMount?: () => any;
    onFocus?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onBlur?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onKeyPress?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
    onKeyDown?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
    onClick?: (event: React.MouseEvent<HTMLDivElement>, quill: UnprivilegedEditor) => void;
    onDoubleClick?: (event: React.MouseEvent<HTMLDivElement>, quill: UnprivilegedEditor) => void;
    onKeyUp?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
};
export declare type RichTextEditorProps = _RichTextEditorProps & QuillOptions;
export declare type PluginNodeProps = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormatTypes, value: any, type: FormatType, id?: string) => void;
    onTriggerUserSelect?: () => void;
    effectAll?: boolean;
    selected?: boolean;
};
export interface HeaderProps {
    draggable?: boolean;
    className?: string;
    show?: boolean;
    text?: string;
    onClose?: (e: React.MouseEvent<HTMLDivElement>) => void;
}
export declare type PluginProps = PluginNodeProps & {
    source?: Sources;
    quill: Editor;
    useHooks?: boolean;
    onSelectionChange?: (range: QuillSelection, source: Sources) => any;
    onTextChange?: (html: string, delta: Delta, source: Sources) => any;
    onFormatsChange?: (formats: Formats, source: Sources) => void;
};
export declare type PluginPopperProps = PluginProps & {
    header?: HeaderProps;
} & PopperProps;
export interface QuillLinkValue {
    uniqueid: string;
    dsid: string;
    href: string;
    target: LinkTarget;
    link?: LinkParam;
}
export interface QuillExpressionValue {
    uniqueid: string;
    dsid: string;
    name: string;
    expression?: Expression;
}
export declare enum TextFormatTypes {
    BOLD = "bold",
    ITALIC = "italic",
    UNDERLINE = "underline",
    STRIKE = "strike",
    ALIGN = "align",
    BACKGROUND = "background",
    COLOR = "color",
    FONT = "font",
    BLOCKQUOTE = "blockquote",
    CODE = "code",
    DIRECTION = "direction",
    HEADER = "header",
    INDENT = "indent",
    LINK = "link",
    LIST = "list",
    SCRIPT = "script",
    SIZE = "size",
    LETTERSPACE = "letterspace",
    LINESPACE = "linespace",
    EXPRESSION = "expression",
    Clear = "clear"
}
export interface BaseTextStyle {
    [TextFormatTypes.COLOR]?: string;
    [TextFormatTypes.BACKGROUND]?: string;
    [TextFormatTypes.FONT]?: FontFamilyValue;
    [TextFormatTypes.SIZE]?: string;
    [TextFormatTypes.ALIGN]?: AlignValue;
    [TextFormatTypes.LETTERSPACE]?: string;
    [TextFormatTypes.LINESPACE]?: string;
    [TextFormatTypes.HEADER]: HeaderValue;
}
export interface SeniorTextStyle {
    [TextFormatTypes.BOLD]?: boolean;
    [TextFormatTypes.ITALIC]?: boolean;
    [TextFormatTypes.UNDERLINE]?: boolean;
    [TextFormatTypes.STRIKE]?: boolean;
    [TextFormatTypes.INDENT]?: IndentValue;
    [TextFormatTypes.DIRECTION]?: DirectionValue;
    [TextFormatTypes.CODE]?: boolean;
    [TextFormatTypes.LINK]?: LinkType;
    [TextFormatTypes.LIST]?: ListValue;
    [TextFormatTypes.EXPRESSION]?: string;
}
export declare type TextStyle = BaseTextStyle & SeniorTextStyle;
export declare type IMBaseTextStyle = ImmutableObject<BaseTextStyle>;
export declare type IMTextStyle = ImmutableObject<TextStyle>;
export declare enum ListValue {
    BULLET = "bullet",
    ORDERED = "ordered"
}
export declare enum HeaderValue {
    Normal = 0,
    H1 = 1,
    H2 = 2,
    H3 = 3,
    H4 = 4,
    H5 = 5,
    H6 = 6
}
export declare enum AlignValue {
    LEFT = "left",
    RIGHT = "right",
    CENTER = "center",
    JUSTIFY = "justify"
}
export declare enum IndentValue {
    ZERO = 0,
    ONE = 1,
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    SIX = 6,
    SEVEN = 7,
    EIGHT = 8
}
export declare enum DirectionValue {
    RTL = "rtl"
}
export declare enum FontFamilyValue {
    AVENIRNEXT = "Avenir Next W01",
    CALIBRI = "Calibri",
    PMINGLIU = "PmingLiu",
    IMPACT = "Impact",
    GEORGIA = "Georgia",
    ARIAL = "Arial",
    TIMESNEWROMAN = "Times New Roman",
    SIMHEI = "SimHei",
    MICROSOFTYAHEI = "Microsoft YaHei"
}
export interface LinkType {
    href: string;
    target: string;
}
