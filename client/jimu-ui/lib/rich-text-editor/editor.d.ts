/// <reference types="react" />
import { React } from 'jimu-core';
import { RichTextEditorProps, QuillValue, Editor, Delta, QuillSelection, Sources, Plugins } from './type';
import { BubbleOption } from './quill/plugins';
interface State {
    value?: QuillValue;
    selection?: QuillSelection;
    renderCount?: number;
}
interface Snapshot {
    regenerate: boolean;
}
export declare class _RichTextEditor extends React.Component<RichTextEditorProps, State, Snapshot> {
    quill: Editor;
    lastDeltaChangeSet: Delta;
    quillDelta: Delta;
    quillSelection: QuillSelection;
    editingArea: HTMLElement;
    handleSelectionChange: (range: QuillSelection, oldRange: QuillSelection, source: Sources) => void;
    dirtyProps: string[];
    cleanProps: string[];
    static defaultProps: Partial<RichTextEditorProps>;
    constructor(props: any);
    private isControlled;
    getSnapshotBeforeUpdate(prevProps: any, prevState: any): Snapshot;
    componentDidUpdate(prevProps: any, prevState: any, snapshot: Snapshot): void;
    setEditorContents: (contents: QuillValue) => void;
    componentDidMount(): void;
    aspectForCreateQuill: () => void;
    componentWillUnmount(): void;
    shouldComponentUpdate(nextProps: any, nextState: any): boolean;
    containExpAttributes: (formats: {
        [x: string]: any;
    }) => boolean;
    isExpAttribute: (name: string) => boolean;
    handleEnter(range: any, context: any): void;
    mixinBindings: (modules?: {
        [x: string]: any;
    }) => {
        [x: string]: any;
    };
    private getEditorConfig;
    private getEditor;
    private getEditingArea;
    private getEditorContents;
    private getEditorSelection;
    private renderEditingArea;
    renderPlugin: (quill: Editor, props: BubbleOption, Plugin: React.ComponentClass<any, any>) => JSX.Element;
    renderPlugins: (plugins: Plugins) => JSX.Element[];
    onDoubleClick: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getExpNode: (node: HTMLElement) => Node & ParentNode;
    onClick: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    render(): JSX.Element;
    private onEditorChangeText;
    private onEditorChangeSelection;
    enable: (enabled?: boolean) => void;
    focus: () => void;
    blur: () => void;
    private createEditor;
    private hookEditor;
    private unhookEditor;
}
export declare const RichTextEditor: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
