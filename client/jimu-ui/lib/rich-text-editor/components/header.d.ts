/// <reference types="react" />
/// <reference types="@emotion/core" />
/** @jsx jsx */
import { IntlShape } from 'jimu-core';
import { HeaderValue } from '../type';
interface Props {
    className?: string;
    style?: any;
    value?: HeaderValue;
    onChange?: (value: HeaderValue) => void;
}
interface ExtraProps {
    intl?: IntlShape;
}
export declare const _Header: ({ value, onChange, className, style, intl }: Props & ExtraProps) => JSX.Element;
export declare const Header: import("react").FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps, "theme">>;
export {};
