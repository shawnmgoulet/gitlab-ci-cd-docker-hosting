/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { Placement } from 'jimu-ui';
import { ColorResult } from 'react-color';
interface Props {
    title?: string;
    icon?: React.ComponentClass<React.SVGAttributes<SVGElement>> | string;
    innerClassName?: string;
    className?: string;
    style?: React.CSSProperties;
    color: string;
    presetColors?: string[];
    placement?: Placement;
    onChange?: (color: string) => void;
    onClick?: () => void;
    changeOnClose?: boolean;
    theme?: ThemeVariables;
    outline?: boolean;
}
interface State {
    color?: string;
    showPicker: boolean;
}
export declare class _TextColorPicker extends React.PureComponent<Props, State> {
    static count: number;
    static defaultProps: Partial<Props>;
    domNode: React.RefObject<HTMLDivElement>;
    constructor(props: any);
    componentDidUpdate(prveProps: any, preveState: State): void;
    private getStyle;
    userSelectNone: () => import("jimu-core").SerializedStyles;
    toRgba: (color: ColorResult) => string;
    colorChange: (c: ColorResult) => void;
    onChange: (color: string) => void;
    private handleClick;
    render(): JSX.Element;
}
export declare const TextColorPicker: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_TextColorPicker>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_TextColorPicker>, "className" | "style" | "color" | "onChange" | "theme" | "title" | "outline" | "icon" | "onClick" | "innerClassName" | "presetColors" | "placement" | "changeOnClose">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
