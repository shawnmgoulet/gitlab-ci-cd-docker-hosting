/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, ImmutableArray, ReactRedux, IntlShape } from 'jimu-core';
import { TextFormatTypes, IMTextStyle } from '../type';
interface Props {
    dataSourceIds?: ImmutableArray<string>;
    className?: string;
    style?: any;
    formats?: IMTextStyle;
    onChange?: (key: TextFormatTypes, value: any) => void;
}
interface ExtraProps {
    theme?: ThemeVariables;
}
interface ExtraProps2 {
    intl: IntlShape;
}
export declare class _TextFormats extends React.PureComponent<Props & ExtraProps & ExtraProps2> {
    static defaultProps: Partial<Props & ExtraProps & ExtraProps2>;
    getStyle: () => import("jimu-core").SerializedStyles;
    nls: (id: string) => string;
    render(): JSX.Element;
}
export declare const TextFormats: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<Props & ExtraProps & ExtraProps2, "className" | "style" | "onChange" | "theme" | "dataSourceIds" | "formats"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<Props & ExtraProps & ExtraProps2>;
}, Pick<Pick<Props & ExtraProps & ExtraProps2, "className" | "style" | "onChange" | "theme" | "dataSourceIds" | "formats"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>, "className" | "style" | "onChange" | "ref" | "key" | "forwardedRef" | "dataSourceIds" | "formats"> & Props>;
export {};
