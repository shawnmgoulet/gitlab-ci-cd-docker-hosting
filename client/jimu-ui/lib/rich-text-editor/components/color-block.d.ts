/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
interface Props {
    title?: string;
    id?: string;
    icon?: React.ComponentClass<React.SVGAttributes<SVGElement>> | string;
    className?: string;
    style?: React.CSSProperties;
    color: string;
    onClick?: () => void;
    theme?: ThemeVariables;
    innerRef?: (e: HTMLDivElement) => void;
}
export declare class _ColorBlock extends React.PureComponent<Props> {
    static count: number;
    id: string;
    static defaultProps: Partial<Props>;
    private getStyle;
    render(): JSX.Element;
}
export declare const ColorBlock: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_ColorBlock>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_ColorBlock>, "className" | "style" | "color" | "theme" | "title" | "icon" | "id" | "onClick" | "innerRef">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
