/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
interface Props {
    title?: string;
    id?: string;
    icon?: React.ComponentClass<React.SVGAttributes<SVGElement>> | string;
    className?: string;
    style?: React.CSSProperties;
    color: string;
    onClick?: () => void;
    innerRef?: (e: HTMLDivElement) => void;
    outline?: boolean;
}
interface ExtraProps {
    theme: ThemeVariables;
}
export declare class _ColorIcon extends React.PureComponent<Props & ExtraProps> {
    static count: number;
    id: string;
    static defaultProps: Partial<Props>;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const ColorIcon: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_ColorIcon>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_ColorIcon>, "className" | "style" | "color" | "title" | "outline" | "icon" | "id" | "onClick" | "innerRef">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
