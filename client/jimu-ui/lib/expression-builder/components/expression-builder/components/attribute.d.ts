/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, ImmutableArray, Expression, DataSource, Immutable, ImmutableObject, DataSourceManager } from 'jimu-core';
interface Props {
    dataSourceIds: ImmutableArray<string>;
    expression: Expression | ImmutableObject<Expression>;
    onChange: (expression: Expression) => void;
}
interface State {
    FieldSelector: any;
}
export default class AttributeTab extends React.PureComponent<Props, State> {
    dsManager: DataSourceManager;
    __unmount: boolean;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    getSelectedFields: () => Immutable.ImmutableArray<string> | Immutable.ImmutableObject<{
        [dataSourceId: string]: string[];
    }>;
    getDataSources: () => DataSource[];
    onSelect: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>, ds: DataSource) => void;
    render(): JSX.Element;
}
export {};
