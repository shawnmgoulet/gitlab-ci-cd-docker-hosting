/// <reference types="seamless-immutable" />
export declare const radiusUtilityClasses: (theme: import("seamless-immutable").ImmutableObject<import("jimu-core").ThemeVariables>) => import("jimu-core").SerializedStyles;
