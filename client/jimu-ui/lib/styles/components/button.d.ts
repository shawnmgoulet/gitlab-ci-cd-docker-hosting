import { IMThemeVariables, ThemeButtonType, ThemeButtonStylesByStates, ThemeButtonSize, SerializedStyles } from 'jimu-core';
export declare function buttonSize(sizeVars: ThemeButtonSize): SerializedStyles;
export declare function buttonVariantStyles(buttonVars: ThemeButtonStylesByStates, theme?: IMThemeVariables): SerializedStyles;
export declare function getIconButtonPadding(buttonSizeVars: ThemeButtonSize): string;
export interface IButtonStyle {
    type?: ThemeButtonType;
    icon?: boolean;
    size?: any;
    theme?: IMThemeVariables;
}
export declare const buttonStyles: (props: IButtonStyle) => SerializedStyles;
