import { ThemeVariablesGenerator, IMThemeVariables, IMThemeColorationVariants, ThemeSize, IMThemeAllColors, IMThemeTypography, IMThemeSizes, IMThemeBorder, IMThemeBorderRadiuses, IMThemeBoxShadows, IMThemeBody, IMThemeHeader, IMThemeFooter, IMThemeLink, ThemeColorationType } from 'jimu-core';
export declare class MainThemeVariablesGenerator implements ThemeVariablesGenerator {
    darkTheme: boolean;
    coloration: ThemeColorationType;
    colors: IMThemeAllColors;
    colorationVariants: IMThemeColorationVariants;
    typography: IMThemeTypography;
    spacer: ThemeSize;
    sizes: IMThemeSizes;
    gutters: IMThemeSizes;
    border: IMThemeBorder;
    borderRadiuses: IMThemeBorderRadiuses;
    boxShadows: IMThemeBoxShadows;
    body: IMThemeBody;
    header: IMThemeHeader;
    footer: IMThemeFooter;
    link: IMThemeLink;
    generate(customVariables: IMThemeVariables): IMThemeVariables;
}
