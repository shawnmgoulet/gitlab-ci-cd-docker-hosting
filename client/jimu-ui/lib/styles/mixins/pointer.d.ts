import { ThemeColor, ThemeSize } from 'jimu-core';
declare type Placement = 'top' | 'bottom' | 'left' | 'right';
export declare function popperPointer(placement?: Placement, pointerStyle?: {
    border?: {
        color?: ThemeColor;
        width?: ThemeSize;
    };
    background?: ThemeColor;
    spacer?: ThemeSize;
}): import("jimu-core").SerializedStyles;
export {};
