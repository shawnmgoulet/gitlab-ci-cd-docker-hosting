import { ThemeThemeColors, ColorPaletteGenerator, PaletteLightnessRange, ThemeColorPaletteItem, ThemeColorPalette } from 'jimu-core';
export declare class PaletteGenerator implements ColorPaletteGenerator {
    shadeCount: number;
    getLightness(color: string): any;
    getShadeLevel(lightness: number, lightnessRange: PaletteLightnessRange, shadeStep: number): number;
    getColorsByShade(color: string, shadeLevel: number, shadeStep: number, reverse?: boolean): ThemeColorPaletteItem;
    generate(themeColors: ThemeThemeColors, isDarkTheme?: boolean): ThemeColorPalette;
}
