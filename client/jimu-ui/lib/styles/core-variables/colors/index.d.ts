import commonColors from './common';
import themeColors from './theme';
import { PaletteGenerator } from './palette-generator';
export { commonColors, themeColors, PaletteGenerator };
