import { ImmutableArray, SqlClause, SqlClauseSet, DataSource, CodedValue, IntlShape, IMFieldSchema } from 'jimu-core';
export interface SqlResult {
    sql: string;
    displaySQL: string;
}
export declare function getClauseArrayByChange(partArray: ImmutableArray<SqlClause | SqlClauseSet>, clause: SqlClause | SqlClauseSet, id: string): ImmutableArray<SqlClause | SqlClauseSet>;
export declare function getCamelCase(name: string): string;
export declare function queryValueLabelsByField(field: IMFieldSchema, selectedDs: DataSource, intl: IntlShape): Promise<CodedValue[]>;
