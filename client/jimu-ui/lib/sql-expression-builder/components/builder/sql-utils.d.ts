import { SqlExprObj, IMSqlExprObj } from './default';
import { SqlResult } from './utils';
export declare function getSQL(sqlExprObj: IMSqlExprObj, isHosted?: boolean): SqlResult;
export declare function parserSQL(sql: string): SqlExprObj;
