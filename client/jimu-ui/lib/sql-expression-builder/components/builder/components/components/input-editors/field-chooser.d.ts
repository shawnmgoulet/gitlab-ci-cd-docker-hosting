/// <reference types="react" />
/// <reference types="seamless-immutable" />
import { React, IMFieldSchema, DataSource, Immutable, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    selectedDs: DataSource;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExrtaProps {
}
interface State {
    displayUI: string;
    valueProvider: any;
}
export declare class _VIFieldChooser extends React.PureComponent<Props & ExrtaProps, State> {
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    getSelectedFields: () => Immutable.ImmutableArray<string>;
    onChange: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>) => void;
    render(): JSX.Element;
}
declare const VIFieldChooser: typeof _VIFieldChooser;
export default VIFieldChooser;
