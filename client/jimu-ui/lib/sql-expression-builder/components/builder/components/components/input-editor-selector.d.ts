/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { ClauseInputEditor } from 'jimu-ui/sql-expression-runtime';
interface Props {
    value: ClauseInputEditor;
    list: ClauseInputEditor[];
    onSelect: (type: ClauseInputEditor) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    isOpen: boolean;
}
export declare class _InputEditorSelector extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onTypeSelect: (type: ClauseInputEditor) => void;
    toggle: () => void;
    render(): JSX.Element;
}
declare const InputEditorSelector: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default InputEditorSelector;
