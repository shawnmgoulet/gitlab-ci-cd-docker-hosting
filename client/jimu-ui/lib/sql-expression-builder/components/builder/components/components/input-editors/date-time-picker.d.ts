/// <reference types="react" />
import { React, IMFieldSchema, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExrtaProps {
}
interface State {
    displayUI: string;
}
export declare class _VIDateTimePicker extends React.PureComponent<Props & ExrtaProps, State> {
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    _setStateForInputEditor: () => void;
    getInputEditor: (inputEditorType: string) => string;
    onChange: (e: any) => void;
    render(): JSX.Element;
}
declare const VIDateTimePicker: typeof _VIDateTimePicker;
export default VIDateTimePicker;
