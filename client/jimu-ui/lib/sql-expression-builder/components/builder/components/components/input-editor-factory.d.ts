import { JimuFieldType, DataSource, CodedValue, ClauseOperator, SqlClause, ClauseSourceType, ClauseValueOptions, ClauseValuePair, SqlClauseSet, IMFieldSchema } from 'jimu-core';
import { ClauseInputEditor } from '../../default';
export declare function getInputEditor(valueOptions: ClauseValueOptions, fieldObj: IMFieldSchema, onChange: (valueObj: ClauseValueOptions) => void, selectedDs?: DataSource, codedValues?: CodedValue[], runtime?: boolean): JSX.Element;
export declare function getCodedValuesFromDs(dataSource: DataSource, jimuFieldName: string): CodedValue[];
export declare function getDefaultClauseValObj(sourceType?: ClauseSourceType, inputEditor?: string, value?: ClauseValuePair[]): ClauseValueOptions;
export declare function getDefaultSingleClause(): SqlClause;
export declare function getDefaultClauseSet(): SqlClauseSet;
export declare function isCaseSensitiveSupportedByOperatorAndSourceType(operator: ClauseOperator, sourceType: ClauseSourceType): boolean;
export declare function isAskForValueSupportedByOperatorAndSourceType(operator: ClauseOperator, sourceType: ClauseSourceType): boolean;
export declare function getCodedValueInputEditorsByOperatorAndSourceType(operator: ClauseOperator, sourceType: ClauseSourceType): ClauseInputEditor[];
export declare function getClauseValObjByOperator(operator: ClauseOperator, dataSource?: DataSource, jimuFieldName?: string): ClauseValueOptions;
export declare function getSourceTypesByOperator(operator: ClauseOperator): ClauseSourceType[];
export interface InputEditorTypesAndCodedValues {
    inputEditorTypes: ClauseInputEditor[];
    codedValues: CodedValue[];
}
export declare function getInputEditorListByOperatorAndSourceType(operator: ClauseOperator, sourceType?: ClauseSourceType, dataSource?: DataSource, jimuFieldName?: string): InputEditorTypesAndCodedValues;
export declare function getJimuFieldTypeByEsriType(esriFieldType: any): JimuFieldType;
export declare function getOperatorsByEsriType(esriFieldType: any, isHosted: any): ClauseOperator[];
