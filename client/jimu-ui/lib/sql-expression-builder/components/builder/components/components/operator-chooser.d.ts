/// <reference types="react" />
import { React, IntlShape, ClauseOperator } from 'jimu-core';
interface Props {
    value: string;
    list: ClauseOperator[];
    onChange: (operator: string) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
}
export declare class _OperatorChooser extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onChange: (e: any) => void;
    render(): JSX.Element;
}
export declare const OperatorChooser: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export {};
