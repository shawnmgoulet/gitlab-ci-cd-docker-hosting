/// <reference types="react" />
import { React } from 'jimu-core';
import { OperatorSchema } from '../../default';
interface Props {
    list: Array<OperatorSchema>;
    value: string;
    onChange: (operator: string) => void;
}
interface State {
}
export declare class _OperatorSelection extends React.PureComponent<Props, State> {
    constructor(props: any);
    onChange: (e: any) => void;
    render(): JSX.Element;
}
export declare const OperatorSelection: typeof _OperatorSelection;
export {};
