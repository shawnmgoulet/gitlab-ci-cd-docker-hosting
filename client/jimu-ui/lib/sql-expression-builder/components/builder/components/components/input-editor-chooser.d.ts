/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { ClauseInputEditor } from 'jimu-ui/sql-expression-runtime';
interface Props {
    value: ClauseInputEditor;
    list: ClauseInputEditor[];
    onSelect: (type: ClauseInputEditor) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    isOpen: boolean;
}
export declare class _InputEditorChooser extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onTypeSelect: (type: ClauseInputEditor) => void;
    toggle: () => void;
    render(): JSX.Element;
}
declare const InputEditorChooser: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default InputEditorChooser;
