/// <reference types="react" />
import { React } from 'jimu-core';
import { clauseValueTypes } from '../../default';
interface Props {
    value: clauseValueTypes;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: clauseValueTypes) => void;
}
interface State {
}
export declare class _VItext extends React.PureComponent<Props, State> {
    constructor(props: any);
    onNameChange: (e: any) => void;
    render(): JSX.Element;
}
export declare const VItext: typeof _VItext;
export {};
