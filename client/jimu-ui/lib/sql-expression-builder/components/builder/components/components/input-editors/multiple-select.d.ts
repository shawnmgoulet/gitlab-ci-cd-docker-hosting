/// <reference types="react" />
/// <reference types="seamless-immutable" />
import { React, DataSource, IMFieldSchema, IntlShape, CodedValue, Immutable, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    codedValues?: CodedValue[];
    fieldObj?: IMFieldSchema;
    selectedDs: DataSource;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    list: any[];
    keyLabel: any;
}
export declare class _VIMultipleSelect extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string, values?: any) => string;
    componentDidMount(): void;
    componentWillUpdate(prevProps: Props, prevState: State): void;
    updateStates: (codedValues: CodedValue[]) => void;
    onClickItem: (e: any, value: string | number, selectedValues: any) => void;
    getValues: () => Immutable.ImmutableArray<any>;
    getDisplayLabel: (values: string[]) => string;
    render(): JSX.Element;
}
declare const VIMultipleSelect: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default VIMultipleSelect;
