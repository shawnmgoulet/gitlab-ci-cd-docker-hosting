/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, IntlShape } from 'jimu-core';
interface Props {
    sourceType: string;
    value: string;
    onSelect: (type: string) => void;
    className?: string;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
interface State {
    isOpen: boolean;
}
export declare class _InputTypeEditor extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onTypeSelect: (type: string) => void;
    toggle: () => void;
    render(): JSX.Element;
}
declare const InputTypeEditor: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<any, "theme">>;
export default InputTypeEditor;
