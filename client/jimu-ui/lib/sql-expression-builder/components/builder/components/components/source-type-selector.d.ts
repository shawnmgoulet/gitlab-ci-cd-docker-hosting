/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, ClauseSourceType } from 'jimu-core';
interface Props {
    value: ClauseSourceType;
    list: ClauseSourceType[];
    onSelect: (type: ClauseSourceType) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    isOpen: boolean;
}
export declare class _SourceTypeSelector extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onTypeSelect: (type: ClauseSourceType) => void;
    toggle: () => void;
    render(): JSX.Element;
}
declare const SourceTypeSelector: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default SourceTypeSelector;
