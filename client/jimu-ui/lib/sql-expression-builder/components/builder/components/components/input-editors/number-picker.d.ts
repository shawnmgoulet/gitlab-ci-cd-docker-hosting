/// <reference types="react" />
import { React, IMFieldSchema, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface State {
}
export declare class _VINumberPicker extends React.PureComponent<Props, State> {
    constructor(props: any);
    onChange: () => void;
    render(): JSX.Element;
}
declare const VINumberPicker: typeof _VINumberPicker;
export default VINumberPicker;
