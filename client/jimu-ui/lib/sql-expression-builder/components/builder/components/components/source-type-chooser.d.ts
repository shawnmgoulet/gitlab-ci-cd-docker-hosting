/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, ClauseSourceType } from 'jimu-core';
interface Props {
    value: ClauseSourceType;
    list: ClauseSourceType[];
    onSelect: (type: ClauseSourceType) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    isOpen: boolean;
}
export declare class _SourceTypeChooser extends React.PureComponent<Props & ExtraProps, State> {
    constructor(props: any);
    i18nMessage: (id: string) => string;
    onTypeSelect: (type: ClauseSourceType) => void;
    toggle: () => void;
    render(): JSX.Element;
}
declare const SourceTypeChooser: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default SourceTypeChooser;
