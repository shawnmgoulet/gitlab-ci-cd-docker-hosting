/// <reference types="react" />
import { React, IMFieldSchema, JimuFieldType, ClauseValueOptions } from 'jimu-core';
interface Props {
    value: ClauseValueOptions;
    fieldObj: IMFieldSchema;
    className?: string;
    style?: React.CSSProperties;
    onChange: (valueObj: ClauseValueOptions) => void;
}
interface ExrtaProps {
}
interface State {
    displayUI: string;
}
export declare class _VITextBox extends React.PureComponent<Props & ExrtaProps, State> {
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    _setStateForDisplayUI: () => void;
    getDisplayUI: (fieldType: JimuFieldType) => string;
    onChange: (e: any) => void;
    render(): JSX.Element;
}
declare const VITextBox: typeof _VITextBox;
export default VITextBox;
