/// <reference types="react" />
import { React, IMFieldSchema } from 'jimu-core';
import { ClauseValObjSchema } from '../../default';
interface Props {
    value: ClauseValObjSchema;
    fieldObj: IMFieldSchema;
    onChange: (valueObj: ClauseValObjSchema) => void;
}
interface State {
}
export declare class _ValueInput extends React.PureComponent<Props, State> {
    constructor(props: any);
    onChange: (value: string) => void;
    render(): JSX.Element;
}
export declare const ValueInput: typeof _ValueInput;
export {};
