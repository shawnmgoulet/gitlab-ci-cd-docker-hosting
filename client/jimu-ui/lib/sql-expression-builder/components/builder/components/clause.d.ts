/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, IMFieldSchema, IntlShape, DataSource, Immutable, CodedValue, SqlClause, ClauseOperator, ClauseSourceType, ClauseValueOptions } from 'jimu-core';
import { SqlExpressionMode, ClauseInputEditor } from 'jimu-ui/sql-expression-runtime';
interface Props {
    mode: SqlExpressionMode;
    id: string;
    config: SqlClause;
    selectedDs: DataSource;
    isHosted?: boolean;
    onChange: (clause: SqlClause, id: string) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    rerender: boolean;
    supportCaseSensitive: boolean;
    supportAskForValue: boolean;
    isAskForValuePanelDisplay: boolean;
}
export declare class _SqlExprClause extends React.PureComponent<Props & ExtraProps, State> {
    askForValueCBX: any;
    operatorList: ClauseOperator[];
    sourceTypeList: ClauseSourceType[];
    inputEditorList: ClauseInputEditor[];
    codedValues: CodedValue[];
    fieldObj: IMFieldSchema;
    constructor(props: any);
    getFieldObj: () => void;
    componentDidMount(): void;
    _updateTwoStates: (operator: ClauseOperator, sourceType: ClauseSourceType, rerender?: boolean) => void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    i18nMessage: (id: string) => string;
    isCascadeSupported: () => boolean;
    _updateCaseSensitiveState(operator: ClauseOperator, sourceType: ClauseSourceType): void;
    _updateAskForValueState(operator: ClauseOperator, sourceType: ClauseSourceType): void;
    getOperatorsByField: (field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>) => ClauseOperator[];
    deleteClause: () => void;
    toggleCaseSensitive: () => void;
    toggleAskForValue: () => void;
    clickAskForValueCBX: (e: any) => void;
    getLabel: () => string;
    onAskForValueOptsChange: (options: Object) => void;
    dragtoReorderClause: () => void;
    /*** field ***/
    onFieldChange: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>) => void;
    _updateOptionsByField: (field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>, operator?: ClauseOperator) => ClauseValueOptions;
    /*** operator ***/
    onOperatorChange: (operator: ClauseOperator) => void;
    /*** value ***/
    onValueOptsChange: (valueOptions: ClauseValueOptions) => void;
    /*** value source type ***/
    onSourceTypeSelect: (sourceType: ClauseSourceType) => void;
    _updateInputEditorsBySourceType: (operator: ClauseOperator, sourceType: ClauseSourceType, jimuFieldName?: string) => ClauseValueOptions;
    /*** value source type ***/
    onInputEditorSelect: (inputEditor: ClauseInputEditor) => void;
    onChanged: (options: Object) => void;
    render(): JSX.Element;
}
declare const SqlExpressionClause: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default SqlExpressionClause;
