/// <reference types="react" />
/// <reference types="seamless-immutable" />
/** @jsx jsx */
import { React, IntlShape, DataSource, ImmutableArray, Immutable, SqlClause, ClauseLogic, SqlClauseSet, IMSqlExpression } from 'jimu-core';
import { SqlExpressionMode } from 'jimu-ui/sql-expression-runtime';
interface Props {
    mode: SqlExpressionMode;
    id: string;
    config: IMSqlExpression;
    selectedDs: DataSource;
    isHosted?: boolean;
    onChange: (clause: SqlClauseSet, id: string) => void;
    className?: string;
}
interface ExtraProps {
    intl: IntlShape;
}
interface State {
    logicalOperator: ClauseLogic;
    partsArray: ImmutableArray<SqlClause | SqlClauseSet>;
}
export declare class _SqlExprClauseSet extends React.PureComponent<Props & ExtraProps, State> {
    LogicalOperator: ClauseLogic;
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    i18nMessage: (id: string) => string;
    addClause: (evt: React.MouseEvent<Element, MouseEvent>) => void;
    deleteClauseSet: (evt: React.MouseEvent<Element, MouseEvent>) => void;
    changeAndOR: (logicalOperator: ClauseLogic) => void;
    onClauseChange: (clause: SqlClause, id: string) => void;
    _setStates: (partsArray: Immutable.ImmutableArray<SqlClause | SqlClauseSet>) => void;
    render(): JSX.Element;
}
declare const SqlExpressionClauseSet: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export default SqlExpressionClauseSet;
