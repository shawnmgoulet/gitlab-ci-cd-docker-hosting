/** @jsx jsx */
import { OrderByOption, ThemeVariables, IMUseDataSource, ImmutableArray } from 'jimu-core';
export interface SortProps {
    onChange: (sortData: Array<OrderByOption>, index?: number) => void;
    value: ImmutableArray<OrderByOption>;
    useDataSource: IMUseDataSource;
    onDelete?: (sortData: Array<OrderByOption>, index?: number) => void;
    onSelect?: (sortData: Array<OrderByOption>, index?: number) => void;
    onSortButtonClick?: (sortData: Array<OrderByOption>, index?: number) => void;
    onAddOption?: (sortData: Array<OrderByOption>) => void;
    theme?: ThemeVariables;
    className?: string;
}
export interface SortState {
    option: OrderByOption;
}
export declare const Sort: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
