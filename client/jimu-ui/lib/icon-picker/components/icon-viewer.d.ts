/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, Immutable, IconProps, IMIconProps, IMIconResult, IMThemeVariables, IntlShape } from 'jimu-core';
import { LinearUnit } from 'jimu-ui';
import { PublicIconGroupType } from '../types';
interface IconViewerProps {
    icon?: IMIconResult;
    isOpen?: boolean;
    configurable?: boolean;
    groups?: PublicIconGroupType | PublicIconGroupType[];
    mode?: 'modal' | 'dropdown';
    className?: string;
    onButtonClick?: (e: any) => void;
    onConfirm?: (result: IMIconResult) => void;
    onOpened?: () => void;
    onClosed?: () => void;
}
interface ExtraProps {
    theme?: IMThemeVariables;
    intl?: IntlShape;
}
interface IconPickerState {
    isOpen: boolean;
    selectedIcon: string;
    selectedIconProps: IMIconProps;
}
export declare class _IconViewer extends React.PureComponent<IconViewerProps & ExtraProps, IconPickerState> {
    static defaultProps: Partial<IconViewerProps & ExtraProps>;
    constructor(props: any);
    _getInitialState: () => IconPickerState;
    _getIconGroups: () => Immutable.ImmutableObject<import("../types").IconGroups>;
    toggleModal: () => void;
    onIconClick: (iconComponent: string, iconProps: Partial<IconProps>) => void;
    onIconColorChange: (color: string) => void;
    onIconSizeChange: (value: LinearUnit) => void;
    onConfirm: () => void;
    onOpened: () => void;
    i18nMessage(id: string): string;
    componentDidUpdate(prevProps: IconViewerProps & ExtraProps): void;
    componentWillMount(): void;
    render(): JSX.Element;
}
export declare const IconViewer: React.FunctionComponent<IconViewerProps & ExtraProps>;
export {};
