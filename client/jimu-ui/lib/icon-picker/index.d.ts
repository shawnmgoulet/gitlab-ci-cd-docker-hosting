/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, Immutable, IntlShape, IconResult, IMIconResult } from 'jimu-core';
import { ButtonProps } from 'jimu-ui';
import { PublicIconGroupType } from './types';
interface IconPickerProps {
    className?: string;
    icon?: IconResult;
    showLabel?: boolean;
    hideRemove?: boolean;
    configurable?: boolean;
    buttonOptions?: ButtonProps;
    mode?: 'modal' | 'dropdown';
    groups?: PublicIconGroupType | PublicIconGroupType[];
    onButtonClick?: (e: any) => void;
    onChange?: (result: IMIconResult) => void;
}
interface ExtraProps {
    intl?: IntlShape;
}
interface IconPickerState {
    isIconViewerOpen: boolean;
    icon?: IMIconResult;
}
export declare class _IconPicker extends React.PureComponent<IconPickerProps & ExtraProps, IconPickerState> {
    static defaultProps: IconPickerProps & ExtraProps;
    constructor(props: any);
    onSetBtnClick: (e: any) => void;
    onRemoveClick: (e: any) => void;
    onClosed: () => void;
    onConfirm: (result: Immutable.ImmutableObject<IconResult>) => void;
    clearResult(): void;
    componentDidUpdate(prevProps: IconPickerProps & ExtraProps, preState: IconPickerState): void;
    render(): JSX.Element;
}
export declare const IconPicker: React.FunctionComponent<IconPickerProps & ExtraProps>;
export {};
