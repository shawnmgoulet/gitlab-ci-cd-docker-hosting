/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, Immutable, IconProps, IconResult, IMIconProps, IMIconResult, IMThemeVariables, IntlShape } from 'jimu-core';
import { LinearUnit } from 'jimu-ui';
import { ImageResourceItemInfo } from 'jimu-for-builder';
import { PublicIconGroupType, ConfigurableOption, PreviewOptions } from '../types';
interface IconViewerProps {
    icon?: IconResult;
    isOpen?: boolean;
    configurableOption?: ConfigurableOption;
    previewOptions?: PreviewOptions;
    groups?: PublicIconGroupType | PublicIconGroupType[] | 'none';
    customIcons?: IconResult[];
    className?: string;
    onButtonClick?: (e: any) => void;
    onChange?: (result: IMIconResult) => void;
}
interface ExtraProps {
    theme?: IMThemeVariables;
    intl?: IntlShape;
}
interface IconPickerState {
    selectedIcon: string;
    selectedIconProps: IMIconProps;
    cachedIcons: {
        [id: string]: IconResult;
    };
}
export declare class _IconViewer extends React.PureComponent<IconViewerProps & ExtraProps, IconPickerState> {
    static count: number;
    private _cachedIconsWidgetId;
    static defaultProps: Partial<IconViewerProps & ExtraProps>;
    constructor(props: any);
    _getIconGroups: () => Immutable.ImmutableObject<import("../types").IconGroups>;
    onIconClick: (iconComponent: string, iconProps: Partial<IconProps>) => void;
    onIconColorChange: (color: string) => void;
    onIconSizeChange: (value: LinearUnit) => void;
    onChange: () => void;
    onIconUpload: (result: ImageResourceItemInfo) => void;
    i18nMessage(id: string): string;
    componentDidUpdate(prevProps: IconViewerProps & ExtraProps, prevState: IconPickerState): void;
    componentDidMount(): void;
    render(): JSX.Element;
}
export declare const IconViewer: React.FunctionComponent<IconViewerProps & ExtraProps>;
export {};
