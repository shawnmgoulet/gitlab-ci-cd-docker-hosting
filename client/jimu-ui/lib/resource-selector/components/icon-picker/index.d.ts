/// <reference types="react" />
import { React, IntlShape, IconResult, IMIconResult } from 'jimu-core';
import { ButtonProps } from 'jimu-ui';
import { PublicIconGroupType, ConfigurableOption, PreviewOptions } from './types';
interface IconPickerProps {
    className?: string;
    icon?: IconResult;
    showLabel?: boolean;
    hideRemove?: boolean;
    configurableOption?: ConfigurableOption;
    previewOptions?: PreviewOptions;
    buttonOptions?: ButtonProps;
    groups?: PublicIconGroupType | PublicIconGroupType[] | 'none';
    customIcons?: IconResult[];
    onButtonClick?: (e: any) => void;
    onChange?: (result: IMIconResult) => void;
}
interface ExtraProps {
    intl?: IntlShape;
}
export declare const IconPicker: React.FunctionComponent<IconPickerProps & ExtraProps>;
export {};
