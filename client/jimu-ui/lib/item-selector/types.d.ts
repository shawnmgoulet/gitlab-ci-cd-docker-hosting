export declare enum ItemTypes {
    WebMap = "Web Map",
    WebScene = "Web Scene",
    FeatureService = "Feature Service"
}
export declare enum ItemCategory {
    MyContent = "MY_CONTENT",
    MyOrganization = "MY_ORGANIZATION",
    Public = "PUBLIC"
}
export declare enum SortField {
    Modified = "modified",
    Title = "title",
    Views = "numViews"
}
export declare enum SortOrder {
    Desc = "desc",
    Asc = "asc"
}
