/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { InjectedColorProps } from './react-color';
export declare class Rgba extends React.PureComponent<InjectedColorProps & {
    disableAlpha?: boolean;
}> {
    handleChange: (data: any) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
