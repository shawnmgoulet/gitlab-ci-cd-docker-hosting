/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { InjectedColorProps } from './react-color';
export declare class Hex extends React.PureComponent<InjectedColorProps & {
    className?: string;
}> {
    handleChange: (data: any) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
