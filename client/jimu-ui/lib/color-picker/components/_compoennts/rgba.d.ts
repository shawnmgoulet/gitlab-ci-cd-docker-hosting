/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { InjectedColorProps } from './react-color.core';
export declare class Rgba extends React.PureComponent<InjectedColorProps & {
    className?: string;
    disableAlpha?: boolean;
}> {
    handleChange: (data: any) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
