import { ColorResult } from 'react-color';
import color from 'react-color/lib/helpers/color';
import { InjectedColorProps, ExportedColorProps } from 'react-color/lib/components/common/ColorWrap';
import { CustomPicker } from 'react-color';
import { Checkboard } from 'react-color/lib/components/common';
declare const Saturation: any;
declare const Hue: any;
declare const Alpha: any;
export { ColorResult, color, InjectedColorProps, ExportedColorProps, CustomPicker, Saturation, Hue, Alpha, Checkboard };
