/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
interface Props {
    placeholder?: string;
    label?: string;
    hideLabel?: boolean;
    value: number;
    arrowOffset?: number;
    max?: number;
    onChange?: (value: any) => void;
    theme?: ThemeVariables;
}
interface State {
    value: string;
}
declare class _EditableInput extends React.PureComponent<Props, State> {
    static defaultProps: Partial<Props>;
    input: React.RefObject<HTMLInputElement>;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props): void;
    getOutValue(value: any): any;
    onBlur: () => void;
    onKeyUp: (evt: React.KeyboardEvent<HTMLInputElement>) => void;
    getArrowOffset(): number;
    handleKeyDown: (e: any) => void;
    changeValue(value: any): void;
    handleChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const EditableInput: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_EditableInput>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_EditableInput>, "label" | "theme" | "onChange" | "value" | "placeholder" | "hideLabel" | "arrowOffset" | "max">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
