/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { ExportedColorProps, InjectedColorProps } from './react-color';
interface OwnProps {
    className?: string;
    disableAlpha?: boolean;
    hsv?: any;
}
export declare type ColorPickerProps = OwnProps & ExportedColorProps & InjectedColorProps;
export declare const SketchPicker: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<any, "theme">>;
export {};
