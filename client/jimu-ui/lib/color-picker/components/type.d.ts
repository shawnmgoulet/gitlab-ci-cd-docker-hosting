export interface ColorItem {
    label: string;
    value: string;
    color: string;
}
