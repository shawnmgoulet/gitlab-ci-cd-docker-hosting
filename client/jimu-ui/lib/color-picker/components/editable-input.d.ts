/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
interface Props {
    placeholder?: string;
    label?: string;
    hideLabel?: boolean;
    value: number;
    arrowOffset?: number;
    max?: number;
    onChange?: (value: any) => void;
}
interface State {
    value: string;
}
export declare class EditableInput extends React.PureComponent<Props, State> {
    static defaultProps: Partial<Props>;
    input: React.RefObject<HTMLInputElement>;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props): void;
    getOutValue(value: any): any;
    onBlur: () => void;
    onKeyUp: (evt: React.KeyboardEvent<HTMLInputElement>) => void;
    getArrowOffset(): number;
    handleKeyDown: (e: any) => void;
    changeValue(value: any): void;
    handleChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export default EditableInput;
