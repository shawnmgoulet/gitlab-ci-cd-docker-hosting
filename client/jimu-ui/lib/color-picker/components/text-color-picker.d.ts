/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { Placement } from 'jimu-ui';
import { ColorResult } from './_components/react-color.core';
import { ColorItem } from './type';
interface Props {
    title?: string;
    icon?: React.ComponentClass<React.SVGAttributes<SVGElement>> | string;
    className?: string;
    popperClassName?: string;
    style?: React.CSSProperties;
    color: string;
    placement?: Placement;
    onChange?: (color: string) => void;
    onClick?: () => void;
    theme?: ThemeVariables;
    outline?: boolean;
    presetColors?: ColorItem[];
}
interface State {
    showPicker: boolean;
}
export declare class _TextColorPicker extends React.PureComponent<Props, State> {
    static count: number;
    static defaultProps: Partial<Props>;
    domNode: React.RefObject<HTMLDivElement>;
    constructor(props: any);
    getStyle: () => import("jimu-core").SerializedStyles;
    toRgba: (color: ColorResult) => string;
    handleChange: (c: ColorResult) => void;
    private handleClick;
    render(): JSX.Element;
}
export declare const TextColorPicker: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_TextColorPicker>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_TextColorPicker>, "onChange" | "className" | "style" | "title" | "theme" | "color" | "outline" | "icon" | "onClick" | "popperClassName" | "placement" | "presetColors">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
