/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { InjectedColorProps } from './react-color.core';
declare class _Hex extends React.PureComponent<InjectedColorProps & {
    className?: string;
    theme?: ThemeVariables;
}> {
    handleChange: (data: any) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const Hex: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<InjectedColorProps & {
    className?: string;
    theme?: ThemeVariables;
} & React.RefAttributes<_Hex>, "theme">>;
export {};
