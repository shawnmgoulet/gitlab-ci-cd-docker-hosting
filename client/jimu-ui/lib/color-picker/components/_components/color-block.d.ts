/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
interface Props {
    title?: string;
    className?: string;
    style?: React.CSSProperties;
    color: string;
    active?: boolean;
    onClick?: () => void;
    theme?: ThemeVariables;
}
declare class _ColorBlock extends React.PureComponent<Props> {
    static defaultProps: Partial<Props>;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const ColorBlock: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_ColorBlock>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_ColorBlock>, "className" | "style" | "title" | "theme" | "color" | "active" | "onClick">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
