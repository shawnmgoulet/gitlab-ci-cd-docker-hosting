/// <reference types="react" />
import { React } from 'jimu-core';
import { ColorItem } from './type';
export declare const presetColors: {
    label: string;
    value: string;
    color: string;
}[];
interface Props {
    value: string;
    className?: string;
    onChange?: (value: string) => void;
    colors: ColorItem[];
    space?: string;
}
export declare class _PresetColors extends React.PureComponent<Props> {
    static defaultProps: Partial<Props>;
    active: (value: string, color: string) => boolean;
    render(): JSX.Element;
}
export declare const PresetColor: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
