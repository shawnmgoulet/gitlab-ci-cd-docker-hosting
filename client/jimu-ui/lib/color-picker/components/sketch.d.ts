import { ExportedColorProps, InjectedColorProps } from './_components/react-color.core';
interface OwnProps {
    className?: string;
    disableAlpha?: boolean;
    hsv?: any;
}
export declare type ColorPickerProps = OwnProps & ExportedColorProps & InjectedColorProps;
export declare const Sketch: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
