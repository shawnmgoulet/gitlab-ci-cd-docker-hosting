import { ImmutableObject, WidgetJson, IMWidgetJson, IMDataSourceJson, IntlShape, DataSource, IMAppConfig, IMDataSourceInfo } from 'jimu-core';
export declare enum GeoType {
    Point = "POINT",
    Polyline = "POLYLINE",
    Polygon = "POLYGON"
}
export declare function getDsUsedWidgets(dsId: string, allWidgets: ImmutableObject<{
    [widgetId: string]: WidgetJson;
}>): IMWidgetJson[];
export declare function getOutputDssFromOriginDs(ds: DataSource): DataSource[];
export declare function getAppConfig(): IMAppConfig;
export declare function getDsIcon(dsJson: IMDataSourceJson): any;
export declare function getDsTypeString(dsType: string, intl: IntlShape): string;
export declare function getWhetherDataSourceIsInited(dataSourcesInfo: ImmutableObject<{
    [dsId: string]: IMDataSourceInfo;
}>): boolean;
