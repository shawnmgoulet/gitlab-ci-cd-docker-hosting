/// <reference types="react" />
/** @jsx jsx */
import { React, IMDataSourceJson } from 'jimu-core';
interface Props {
    dataSourceJson: IMDataSourceJson;
    isMoreIconShown?: boolean;
    isCloseIconShown?: boolean;
    isRelatedWidgetsShown?: boolean;
    isOriginalUrlShown?: boolean;
    isSelected?: boolean;
    onMoreIconClick?: (dsJson: IMDataSourceJson) => void;
    onCloseIconClick?: (dsJson: IMDataSourceJson) => void;
    onDataSourceItemClick?: (dsJson: IMDataSourceJson) => void;
    className?: string;
}
interface State {
}
export declare class _DataSourceErrorItem extends React.PureComponent<Props, State> {
    constructor(props: any);
    render(): JSX.Element;
}
export declare const DataSourceErrorItem: import("@emotion/styled-base").StyledComponent<any, Pick<any, string | number | symbol>, object>;
export {};
