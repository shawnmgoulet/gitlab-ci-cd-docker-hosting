/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
declare const ExternalDataSourceSelector: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export default ExternalDataSourceSelector;
