/// <reference types="react" />
import { DataSource, React, ImmutableArray, IntlShape } from 'jimu-core';
import { SelectedDataSourceJson } from '../../../types';
interface State {
    isChildDssOpen: boolean;
}
interface Props {
    label: string;
    childDss: DataSource[];
    selectedDsIds: ImmutableArray<string>;
    intl: IntlShape;
    icon?: any;
    onSelect: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class ToUseDsCollapse extends React.PureComponent<Props, State> {
    constructor(props: any);
    toggleChildDss: () => void;
    render(): JSX.Element;
}
export {};
