import { DataSourceConstructorOptions, DataSource, DataSourceFactory } from "jimu-core";
export declare class HubDataSourceFactory implements DataSourceFactory {
    createDataSource(options: DataSourceConstructorOptions): DataSource;
}
