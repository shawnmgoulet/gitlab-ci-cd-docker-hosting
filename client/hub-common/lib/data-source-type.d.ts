export declare enum DataSourceTypes {
    HubAnnotations = "HUB_ANNOTATIONS",
    HubEvents = "HUB_EVENTS"
}
