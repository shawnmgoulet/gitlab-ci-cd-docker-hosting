/// <reference types="react" />
import { React, IntlShape } from 'jimu-core';
import { ListValue } from 'jimu-ui';
interface Props {
    className?: string;
    list?: ListValue;
    onClick?: (value: ListValue | boolean) => void;
}
interface ExtraProps {
    intl?: IntlShape;
}
export declare const _List: ({ list, onClick, className, intl }: Props & ExtraProps) => JSX.Element;
export declare const List: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps, "theme">>;
export {};
