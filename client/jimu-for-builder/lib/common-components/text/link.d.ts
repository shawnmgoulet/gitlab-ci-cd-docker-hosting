/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableArray } from 'jimu-core';
import { LinkParam } from '../../components/link-setting/link-setting-types';
interface Props {
    disabled?: boolean;
    title?: string;
    dataSourceIds?: ImmutableArray<string>;
    innerClassName?: string;
    className?: string;
    style?: React.CSSProperties;
    link?: LinkParam;
    onChange?: (link: LinkParam) => void;
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}
interface State {
    show: boolean;
}
export declare class Link extends React.PureComponent<Props, State> {
    static count: number;
    id: string;
    static defaultProps: Partial<Props>;
    constructor(props: any);
    private getStyle;
    onSettingConfirm: (link: LinkParam) => void;
    handleClick: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    toggle: () => void;
    render(): JSX.Element;
}
export {};
