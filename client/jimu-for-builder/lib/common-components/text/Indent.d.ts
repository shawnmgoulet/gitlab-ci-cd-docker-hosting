/// <reference types="react" />
import { React, IntlShape } from 'jimu-core';
import { IndentValue } from 'jimu-ui';
interface Props {
    disabled?: boolean;
    className?: string;
    value?: IndentValue;
    onClick?: (value: IndentValue) => any;
}
interface ExtraProps {
    intl?: IntlShape;
}
export declare const _Indent: ({ value, onClick, className, disabled, intl }: Props & ExtraProps) => JSX.Element;
export declare const Indent: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps, "theme">>;
export {};
