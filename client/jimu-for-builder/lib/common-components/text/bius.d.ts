/// <reference types="react" />
import { React, IntlShape } from 'jimu-core';
import { TextFormats } from 'jimu-ui';
import { Formats } from 'jimu-for-builder';
interface Props {
    className?: string;
    formats?: Formats;
    onClick?: (key: TextFormats, value: boolean) => any;
}
interface ExtraProps {
    intl?: IntlShape;
}
export declare const _BIUS: ({ formats, onClick, className, intl }: Props & ExtraProps) => JSX.Element;
export declare const BIUS: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps, "theme">>;
export {};
