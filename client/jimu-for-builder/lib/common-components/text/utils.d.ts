declare const _default: {
    calculateDigits: (n: number) => number;
    getUUID: () => string;
};
export default _default;
