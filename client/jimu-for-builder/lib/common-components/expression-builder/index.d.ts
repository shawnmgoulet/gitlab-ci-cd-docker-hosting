/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { ExpressionFrom } from './types';
export { ExpressionFrom };
declare const ExpressionBuilder: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<React.PropsWithChildren<import("react-intl").WithIntlProps<any>>, "theme">>;
export default ExpressionBuilder;
