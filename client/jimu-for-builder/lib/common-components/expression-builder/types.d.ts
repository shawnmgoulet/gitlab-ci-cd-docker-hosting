export declare enum ExpressionFrom {
    Attribute = "ATTRIBUTE",
    Statistics = "STATISTICS",
    Expression = "EXPRESSION"
}
