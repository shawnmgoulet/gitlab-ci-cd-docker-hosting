/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, ImmutableArray, Expression, DataSource, Immutable, ImmutableObject, DataSourceManager } from 'jimu-core';
interface Props {
    dataSourceIds: ImmutableArray<string>;
    expression: Expression | ImmutableObject<Expression>;
    onChange: (expression: Expression) => void;
    isMultiple?: boolean;
}
export default class AttributeTab extends React.PureComponent<Props, {}> {
    dsManager: DataSourceManager;
    constructor(props: any);
    getSelectedFields: () => Immutable.ImmutableArray<string> | Immutable.ImmutableObject<{
        [dataSourceId: string]: string[];
    }>;
    getDataSources: () => DataSource[];
    onSelect: (field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>, ds: DataSource) => void;
    render(): JSX.Element;
}
export {};
