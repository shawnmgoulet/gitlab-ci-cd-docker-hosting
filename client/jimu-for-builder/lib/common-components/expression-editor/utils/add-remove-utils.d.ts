import { ExpressionPart, ExpressionPartType } from 'jimu-core';
import { EditResult, PopoverItem } from '../types';
export declare function getExps(expressionString: string): string[];
/**
 * return an array which members are string exps and other exps not split
 *
 * e.g. ['"abc"+123+{f1}+AVG({f2})'] -> ['"abc"', '+123+{f1}+AVG({f2})']
 *
 */
export declare function handleStringExps(e: string): string[];
/**
 * return an array which is split by curly brackets
 *
 * e.g. ['"abc"', '+123+{f1}+AVG({f2})'] -> ['"abc"', '+123+', '{f1}', '+AVG(', '{f2}', ')']
 */
export declare function handleFieldExps(e: string[]): string[];
/**
 * return an array which is split by operators and front curly bracket: + - * / , ( ) { space
 *
 * e.g. ['"abc"', '+123+', '{f1}', '+AVG(', '{f2}', ')'] -> ['"abc"', '+', '123', '+', '{f1}', '+', 'AVG', '(', '{f2}', ')']
 *
 */
export declare function handleOperatoExps(e: string[]): string[];
export declare function addCharactersToParts(externalId: string, parts: ExpressionPart[], newCharacters: string, isInEditablePart: boolean, dsIds: string[]): EditResult;
export declare function applyEditResultToParts(editResult: EditResult, parts?: ExpressionPart[]): ExpressionPart[];
export declare function getWhetherPartNeedPopover(type: ExpressionPartType): boolean;
export declare function getExpressionPart(exp: string, dataSourceId?: string, jimuFieldName?: string): ExpressionPart;
export declare function removeParts(externalId: string, parts: ExpressionPart[], startIndex: number, endIndex: number): EditResult;
export declare function removeCharactersFromOnePart(externalId: string, parts: ExpressionPart[], partId: string, startIndex: number, endIndex: number): EditResult;
export declare function getPopoverItems(externalId: string, partId: string, parts: ExpressionPart[], dataSourceIds: string[]): PopoverItem[];
export declare function getPopoverTarget(externalId: string, isInEditablePart: boolean): string;
