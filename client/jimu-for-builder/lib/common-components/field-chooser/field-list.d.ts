/// <reference types="react" />
import { React, FieldSchema, DataSource, ImmutableArray } from 'jimu-core';
interface Props {
    isDropdown?: boolean;
    fields: {
        [jimuName: string]: FieldSchema;
    };
    ds: DataSource;
    onSelect?: (field: FieldSchema, ds: DataSource) => void;
    selectedFields?: ImmutableArray<string>;
}
interface State {
    isOpen: boolean;
}
export default class FieldList extends React.PureComponent<Props, State> {
    constructor(props: any);
    onFieldSelect: (f: FieldSchema) => void;
    toggle: () => void;
    onSearchChange: (e: any) => void;
    render(): JSX.Element;
}
export {};
