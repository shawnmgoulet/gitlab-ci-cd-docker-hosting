import { DataSourceComponentInner, DataSourceComponentProps, DataSourceComponentStateProps, ReactRedux } from 'jimu-core';
declare const _default: ReactRedux.ConnectedComponentClass<typeof DataSourceComponentInner, Pick<DataSourceComponentProps & DataSourceComponentStateProps, "children" | "query" | "useDataSource" | "refresh" | "defaultQuery" | "onDataSourceInfoChange" | "onDataSourceCreated" | "onCreateDataSourceFailed" | "onDataSourceQueryChange"> & DataSourceComponentProps>;
export default _default;
