/// <reference types="react" />
import { IMPageJson } from 'jimu-core';
export declare function getDefaultTocPageIcon(pageJson: IMPageJson): React.ComponentClass<React.SVGAttributes<SVGElement>>;
