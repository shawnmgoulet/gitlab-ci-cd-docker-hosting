export interface ChangeViewInfo {
    sectionId: string;
    viewId: string;
}
export declare enum ToAppMessage {
    AppConfigChanged = "app_config_changed",
    ChangeAppMode = "change_app_mode",
    ChangePage = "change_page",
    ChangeDialog = "change_dialog",
    ChangeView = "change_view",
    ChangeSelection = "change_selection",
    ChangeWidgetStateProp = "change_widget_state_prop",
    ChangeWidgetMutableStateProp = "change_widget_mutable_state_prop",
    BuilderTriggerKeyboard = "builder_trigger_keyboard",
    ChangeBrowserSizeMode = "change_browser_size_mode",
    BuilderSessionChanged = "builder_session_changed"
}
export declare enum ToBuilderMessage {
    AppStateChanged = "app_state_changed",
    AppSessionChanged = "app_session_changed",
    PopupChooseWidget = "popup_choose_widget",
    AppAddResource = "app_add_resource",
    AppClearResources = "app_clear_resources",
    AppTriggerKeyboard = "app_trigger_keyboard",
    SetLayoutTools = "app_set_layout_tools"
}
