/// <reference types="react" />
/** @jsx jsx */
import { React, DataSourceJson, ThemeVariables, SerializedStyles, ImmutableObject, IntlShape, ImmutableArray, WidgetJson, LayoutJson, ReactRedux, BrowserSizeMode } from 'jimu-core';
import { DataSourceJsonWithRootId, AllDataSourceTypes } from '../data-source-list/ds-types';
interface State {
}
interface Props {
    types: ImmutableArray<AllDataSourceTypes>;
    widgetId: string;
    selectedDataSourceIds?: ImmutableArray<string>;
    modalStyle?: object;
    isMultiple?: boolean;
    mustUseDataSource?: boolean;
    onSelect?: (allSelectedDss: DataSourceJsonWithRootId[], currentSelectedDs?: DataSourceJsonWithRootId) => void;
    onRemove?: (allSelectedDss: DataSourceJsonWithRootId[], currentRemovedDs?: DataSourceJsonWithRootId) => void;
    disableSelection?: (allSelectedDss: DataSourceJsonWithRootId[]) => boolean;
    disableRemove?: (allSelectedDss: DataSourceJsonWithRootId[]) => boolean;
}
interface StateExtraProps {
    dataSources: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    layouts: ImmutableObject<{
        [layoutId: string]: LayoutJson;
    }>;
    widgets: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
    browserSizeMode: BrowserSizeMode;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _DataSourceChooser extends React.PureComponent<Props & StateExtraProps & ExtraProps, State> {
    constructor(props: any);
    componentDidMount(): void;
    getStyle: (theme: ThemeVariables) => SerializedStyles;
    toggleDataUse: (isEnabled: boolean) => void;
    onSelect: (allSelectedDss: DataSourceJsonWithRootId[], currentSelectedDs?: DataSourceJsonWithRootId) => void;
    onRemove: (allSelectedDss: DataSourceJsonWithRootId[], currentRemovedDs?: DataSourceJsonWithRootId) => void;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>, "widgetId" | "theme" | "browserSizeMode" | "layouts" | "widgets" | "dataSources" | "forwardedRef" | "key" | "types" | "selectedDataSourceIds" | "modalStyle" | "isMultiple" | "onSelect" | "onRemove" | "disableSelection" | "disableRemove" | "mustUseDataSource"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>, "widgetId" | "theme" | "browserSizeMode" | "layouts" | "widgets" | "dataSources" | "forwardedRef" | "key" | "types" | "selectedDataSourceIds" | "modalStyle" | "isMultiple" | "onSelect" | "onRemove" | "disableSelection" | "disableRemove" | "mustUseDataSource"> & React.RefAttributes<any>, "widgetId" | "theme" | "ref" | "forwardedRef" | "key" | "types" | "selectedDataSourceIds" | "modalStyle" | "isMultiple" | "onSelect" | "onRemove" | "disableSelection" | "disableRemove" | "mustUseDataSource"> & Props>;
export default _default;
