/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { BorderStyle } from 'jimu-ui';
interface BorderStyleProps {
    className?: string;
    style?: any;
    value?: BorderStyle;
    onChange?: (param: BorderStyle) => void;
}
export declare class BorderSetting extends React.PureComponent<BorderStyleProps> {
    static defaultProps: Partial<BorderStyleProps>;
    _updateBorder(key: string, newValue: any): void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export {};
