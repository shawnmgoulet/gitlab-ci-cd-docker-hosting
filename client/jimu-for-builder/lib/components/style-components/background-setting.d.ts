/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { BackgroundStyle, FillType, ImageParam } from 'jimu-ui';
interface Props {
    background?: BackgroundStyle;
    onChange?: (background: BackgroundStyle) => void;
    className?: string;
    style?: any;
}
interface ExtraProps {
    intl: IntlShape;
}
export declare class _BackgroundSetting extends React.PureComponent<Props & ExtraProps> {
    fileInput: React.RefObject<any>;
    static defaultProps: Partial<Props & ExtraProps>;
    constructor(props: any);
    openBrowseImage: (imageParam: ImageParam) => void;
    _onPositionChange: (e: any) => void;
    _onColorChange: (color: string) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    nls: (id: string) => string;
    getFillType: () => {
        value: FillType;
        label: string;
    }[];
    render(): JSX.Element;
}
export declare const BackgroundSetting: React.FunctionComponent<import("react-intl").WithIntlProps<any>> & {
    WrappedComponent: React.ComponentType<any>;
};
export {};
