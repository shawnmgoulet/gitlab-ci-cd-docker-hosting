/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
import { Placement } from 'jimu-ui';
import { ColorResult } from 'react-color';
interface Props {
    title?: string;
    className?: string;
    innerClassName?: string;
    style?: React.CSSProperties;
    color: string;
    presetColors?: string[];
    placement?: Placement;
    onChange?: (color: string) => void;
    onClick?: () => void;
    theme?: ThemeVariables;
}
interface State {
    color?: string;
    showPicker: boolean;
}
declare class _ColorChooser extends React.PureComponent<Props, State> {
    static defaultProps: Partial<Props>;
    blockNode: HTMLDivElement;
    constructor(props: any);
    private getStyle;
    userSelectNone: () => import("jimu-core").SerializedStyles;
    toRgba: (color: ColorResult) => string;
    colorChange: (c: ColorResult) => void;
    private handleClick;
    render(): JSX.Element;
}
export declare const ColorChooser: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<Props & React.RefAttributes<_ColorChooser>, "ref" | "key"> & Partial<Pick<Props & React.RefAttributes<_ColorChooser>, "title" | "className" | "style" | "color" | "onClick" | "theme" | "onChange" | "innerClassName" | "presetColors" | "placement">> & Partial<Pick<Partial<Props>, never>>, "theme">>;
export {};
