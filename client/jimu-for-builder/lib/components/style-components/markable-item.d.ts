/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables } from 'jimu-core';
interface Props {
    label: string;
    value: any;
    marked: boolean;
    className?: string;
    style?: any;
    onChange?: (value: any) => void;
    theme?: ThemeVariables;
}
interface ExtraProps {
    theme?: ThemeVariables;
}
export declare class _MarkableItem extends React.PureComponent<Props & ExtraProps, any> {
    handleClick: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
export declare const MarkableItem: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & React.RefAttributes<_MarkableItem>, "theme">>;
export {};
