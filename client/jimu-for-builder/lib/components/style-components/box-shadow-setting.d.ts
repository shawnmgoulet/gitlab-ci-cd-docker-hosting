/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape } from 'jimu-core';
import { BoxShadowStyle } from 'jimu-ui';
interface Props {
    value?: BoxShadowStyle;
    onChange?: (value: BoxShadowStyle) => void;
}
interface ExtraProps {
    intl: IntlShape;
}
export declare class _BoxShadowSetting extends React.PureComponent<Props & ExtraProps> {
    static defaultProps: Partial<Props & ExtraProps>;
    _updateShadow(key: string, newValue: any): void;
    nls: (id: string) => string;
    getStyle: () => import("jimu-core").SerializedStyles;
    getShadows: () => {
        name: string;
        label: string;
        min: number;
        max: number;
    }[];
    render(): JSX.Element;
}
export declare const BoxShadowSetting: React.ForwardRefExoticComponent<Pick<Props & ExtraProps, "value" | "onChange"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<Props & ExtraProps>;
};
export {};
