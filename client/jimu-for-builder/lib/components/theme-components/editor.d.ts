/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeManager, IMThemeVariables, ThemeInfo, IMThemeInfo, ImmutableArray, IntlShape } from 'jimu-core';
interface ThemeEditorProps {
    themeUri?: string;
    className?: string;
    isCustomizationInprogress?: boolean;
    theme?: IMThemeVariables;
    isBuilder?: boolean;
    themeManager?: ThemeManager;
    intl?: IntlShape;
    onThemeChange?: (name: string, themeInfo?: IMThemeInfo) => void;
    onThemeCustomize?: (themeInfo: IMThemeInfo) => void;
}
interface ThemeEditorState {
    selectedTheme: string;
    status: 'ready' | 'loading' | 'loaded';
    themeInfos: ImmutableArray<ThemeInfo>;
}
export declare class _ThemeEditor extends React.PureComponent<ThemeEditorProps, ThemeEditorState> {
    static getDerivedStateFromProps(props: ThemeEditorProps, state: ThemeEditorState): Partial<ThemeEditorState>;
    private handleThemeChange;
    private handleCustomize;
    private getThemeInfo;
    private getStyle;
    constructor(props: any);
    private loadThemeListInfo;
    render(): JSX.Element;
    componentDidUpdate(prevProps: ThemeEditorProps): void;
    getThemesFolder(): "builder/themes/" | "themes";
}
export declare const ThemeEditor: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<ThemeEditorProps & React.RefAttributes<_ThemeEditor>, "theme">>;
export {};
