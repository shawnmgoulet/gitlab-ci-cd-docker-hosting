/// <reference types="react" />
import { DataSource, React, ImmutableArray, IntlShape, DataSourceManager } from 'jimu-core';
import AppDataSourceManager from '../../app-data-source-manager';
import { DataSourceJsonWithRootId } from './ds-types';
interface State {
    isTypeDropdownOpen: boolean;
    selectedErrorDss: string[];
}
interface Props {
    toUseRootDss: DataSource[];
    toUseChildDss: {
        [rootDsId: string]: DataSource[];
    };
    rootDss: DataSource[];
    selectedDsIds: ImmutableArray<string>;
    showErrorDss: boolean;
    intl: IntlShape;
    onSelect: (selectedDsJson: DataSourceJsonWithRootId) => void;
    changeHasErrorSelectedDss?: (hasErrorDss: boolean) => void;
    onRemove?: (selectedDsJson: DataSourceJsonWithRootId) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class ToUseDsList extends React.PureComponent<Props, State> {
    dsManager: AppDataSourceManager | DataSourceManager;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props): void;
    getWhetherShowRootDss: () => boolean;
    getWhetherShowChildDss: () => boolean;
    getRootDsById: (id: string) => DataSource;
    getSelectedErrorDss: () => string[];
    render(): JSX.Element;
}
export {};
