/// <reference types="react" />
import { React, IntlShape } from 'jimu-core';
import { AllDataSourceTypes } from './ds-types';
interface State {
}
interface Props {
    toUseTypes: AllDataSourceTypes[];
    selectedTypes: AllDataSourceTypes[];
    intl: IntlShape;
    onTypeSelected: (types: AllDataSourceTypes[]) => void;
}
export default class ToUseTypes extends React.PureComponent<Props, State> {
    __unmount: boolean;
    constructor(props: any);
    getWhetherdisableSelectionType: (types: AllDataSourceTypes[]) => boolean;
    onTypeSelected: (e: any) => void;
    render(): JSX.Element;
}
export {};
