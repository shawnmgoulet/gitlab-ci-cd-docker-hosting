/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, DataSource, ImmutableArray, IntlShape } from 'jimu-core';
import { SelectedDataSourceJson } from '../../types';
interface Props {
    dispatch: any;
    intl: IntlShape;
    ds: DataSource;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    selectedDsIds?: ImmutableArray<string>;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
interface State {
}
export default class DsItem extends React.PureComponent<Props, State> {
    getWhetherSelected: () => boolean;
    getDsJsonWithRootId: () => {
        dataSourceJson: import("seamless-immutable").ImmutableObject<import("jimu-core").DataSourceJson>;
        rootDataSourceId: string;
    };
    onItemClick: (e: any) => void;
    render(): JSX.Element;
}
export {};
