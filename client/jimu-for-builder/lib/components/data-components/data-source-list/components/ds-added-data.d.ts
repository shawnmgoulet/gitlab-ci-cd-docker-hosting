/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { DataSource, React, ImmutableArray, IntlShape } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../../types';
interface State {
    selectedTypes: ImmutableArray<AllDataSourceTypes>;
    toUseRootDss: DataSource[];
    toUseChildDss: {
        [rootDsId: string]: DataSource[];
    };
    rootDss: DataSource[];
    childDss: {
        [rootDsId: string]: DataSource[];
    };
    hasErrorSelectedDss: boolean;
}
interface Props {
    isDataSourceInited: boolean;
    types: ImmutableArray<AllDataSourceTypes>;
    intl: IntlShape;
    fromRootDsIds?: ImmutableArray<string>;
    selectedDsIds?: ImmutableArray<string>;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class DataSourceAddedData extends React.PureComponent<Props, State> {
    __unmount: boolean;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: Props, prevState: State): void;
    componentWillUnmount(): void;
    initData: () => void;
    setHasErrorDss: (hasErrorDss: boolean) => void;
    setToUseDataSources: (toUseTypes: import("seamless-immutable").ImmutableArray<import("jimu-core").DataSourceTypes | import("../../../../../../jimu-arcgis").DataSourceTypes | import("../../../../../../hub-common/hub-data-source-type").HubDataSourceTypes>, fromRootDsIds: import("seamless-immutable").ImmutableArray<string>) => void;
    setDataSources: () => void;
    setSelectedType: (types: import("seamless-immutable").ImmutableArray<import("jimu-core").DataSourceTypes | import("../../../../../../jimu-arcgis").DataSourceTypes | import("../../../../../../hub-common/hub-data-source-type").HubDataSourceTypes>) => void;
    getRootDss: () => DataSource[];
    getToUseRootDss: (toUseTypes: import("seamless-immutable").ImmutableArray<string>, fromRootDsIds: import("seamless-immutable").ImmutableArray<string>) => DataSource[];
    getToUseChildDss: (toUseTypes: import("seamless-immutable").ImmutableArray<string>, fromRootDsIds: import("seamless-immutable").ImmutableArray<string>) => Promise<{
        [rootDsId: string]: DataSource[];
    }>;
    getWhetherShowList: () => boolean;
    getWhetherShowRootDss: () => boolean;
    getWhetherShowChildDss: () => boolean;
    render(): JSX.Element;
}
export {};
