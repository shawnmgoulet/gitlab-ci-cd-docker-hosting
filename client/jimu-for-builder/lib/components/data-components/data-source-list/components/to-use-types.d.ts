/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { React, IntlShape, ImmutableArray, Immutable } from 'jimu-core';
import { AllDataSourceTypes } from '../../types';
interface State {
}
interface Props {
    toUseTypes: ImmutableArray<AllDataSourceTypes>;
    selectedTypes: ImmutableArray<AllDataSourceTypes>;
    intl: IntlShape;
    onTypeSelected: (types: ImmutableArray<AllDataSourceTypes>) => void;
}
export default class ToUseTypes extends React.PureComponent<Props, State> {
    __unmount: boolean;
    constructor(props: any);
    getWhetherdisableSelectionType: (types: Immutable.ImmutableArray<import("jimu-core").DataSourceTypes | import("../../../../../../jimu-arcgis").DataSourceTypes | import("../../../../../../hub-common/hub-data-source-type").HubDataSourceTypes>) => boolean;
    onTypeSelected: (e: any) => void;
    render(): JSX.Element;
}
export {};
