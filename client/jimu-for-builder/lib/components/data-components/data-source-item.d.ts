/// <reference types="react" />
/** @jsx jsx */
import { React, IMDataSourceJson, IntlShape, ImmutableObject, WidgetJson, ThemeVariables, SerializedStyles, ReactRedux } from 'jimu-core';
interface Props {
    dataSourceJson: IMDataSourceJson;
    isMoreIconShown?: boolean;
    isCloseIconShown?: boolean;
    isRenameInputShown?: boolean;
    isRelatedWidgetsShown?: boolean;
    isMappingIconShown?: boolean;
    isOriginalUrlShown?: boolean;
    isSelected?: boolean;
    onDataSourceItemClick?: (dsJson: IMDataSourceJson) => void;
    onMappingIconClick?: (dsJson: IMDataSourceJson) => void;
    onCloseIconClick?: (dsJson: IMDataSourceJson) => void;
    onMoreIconClick?: (dsJson: IMDataSourceJson) => void;
    onRename?: (dsLabel: string) => void;
}
interface StateExtraProps {
    portalUrl: string;
    widgets?: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
}
interface ExtraProps {
    intl: IntlShape;
    theme: ThemeVariables;
}
interface State {
    newLabel: string;
}
export declare class _DataSourceItem extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    rootDom: HTMLElement;
    renameInput: HTMLInputElement;
    constructor(props: any);
    componentDidUpdate(prevProps: Props, prevState: State): void;
    onLabelChange: (e: any) => void;
    onMoreIconClick: (e: any) => void;
    onCloseIconClick: (e: any) => void;
    onRename: () => void;
    onDataSourceItemClick: (e: any) => void;
    onMappingIconClick: (e: any) => void;
    stopPropagation: (e: any) => void;
    render(): JSX.Element;
    getStyle: (theme: ThemeVariables) => SerializedStyles;
}
export declare const DataSourceItem: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceItem>, "theme">>, "theme" | "key" | "forwardedRef" | "widgets" | "portalUrl" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRenameInputShown" | "isRelatedWidgetsShown" | "isMappingIconShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onMappingIconClick" | "onCloseIconClick" | "onMoreIconClick" | "onRename"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceItem>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceItem>, "theme">>, "theme" | "key" | "forwardedRef" | "widgets" | "portalUrl" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRenameInputShown" | "isRelatedWidgetsShown" | "isMappingIconShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onMappingIconClick" | "onCloseIconClick" | "onMoreIconClick" | "onRename"> & React.RefAttributes<any>, "theme" | "ref" | "key" | "forwardedRef" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRenameInputShown" | "isRelatedWidgetsShown" | "isMappingIconShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onMappingIconClick" | "onCloseIconClick" | "onMoreIconClick" | "onRename"> & Props>;
export {};
