/// <reference types="react" />
/** @jsx jsx */
import { React, IntlShape, ImmutableObject, IMDataSourceJson, WidgetJson, DataSourceJson, ThemeVariables, SerializedStyles, ReactRedux } from 'jimu-core';
interface Props {
    dataSourceJson: IMDataSourceJson;
    dataSources?: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    isMoreIconShown?: boolean;
    isCloseIconShown?: boolean;
    isRelatedWidgetsShown?: boolean;
    isOriginalUrlShown?: boolean;
    isSelected?: boolean;
    onMoreIconClick?: (dsJson: IMDataSourceJson) => void;
    onCloseIconClick?: (dsJson: IMDataSourceJson) => void;
    onDataSourceItemClick?: (dsJson: IMDataSourceJson) => void;
}
interface StateExtraProps {
    portalUrl: string;
    widgets?: ImmutableObject<{
        [widgetId: string]: WidgetJson;
    }>;
}
interface ExtraProps {
    intl: IntlShape;
    theme: ThemeVariables;
}
interface State {
}
export declare class _DataSourceErrorItem extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    rootDom: HTMLElement;
    constructor(props: any);
    onMoreIconClick: (e: any) => void;
    onCloseIconClick: (e: any) => void;
    onDataSourceItemClick: (e: any) => void;
    stopPropagation: (e: any) => void;
    render(): JSX.Element;
    getStyle: (theme: ThemeVariables) => SerializedStyles;
}
export declare const DataSourceErrorItem: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceErrorItem>, "theme">>, "theme" | "key" | "forwardedRef" | "widgets" | "dataSources" | "portalUrl" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRelatedWidgetsShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onCloseIconClick" | "onMoreIconClick"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceErrorItem>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_DataSourceErrorItem>, "theme">>, "theme" | "key" | "forwardedRef" | "widgets" | "dataSources" | "portalUrl" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRelatedWidgetsShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onCloseIconClick" | "onMoreIconClick"> & React.RefAttributes<any>, "theme" | "ref" | "key" | "forwardedRef" | "dataSources" | "isSelected" | "dataSourceJson" | "isMoreIconShown" | "isCloseIconShown" | "isRelatedWidgetsShown" | "isOriginalUrlShown" | "onDataSourceItemClick" | "onCloseIconClick" | "onMoreIconClick"> & Props>;
export {};
