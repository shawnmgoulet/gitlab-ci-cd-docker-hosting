/// <reference types="react" />
/** @jsx jsx */
import { React, DataSourceJson, ThemeVariables, SerializedStyles, ImmutableObject, IntlShape, ImmutableArray, ReactRedux, IMDataSourceInfo } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../types';
interface State {
}
interface Props {
    /**
     * supported data source types
     */
    types: ImmutableArray<AllDataSourceTypes>;
    /**
     * id of selected data sources
     */
    selectedDataSourceIds?: ImmutableArray<string>;
    /**
     * whether to support multiple selection
     */
    isMultiple?: boolean;
    /**
     * whether to enable data source use
     */
    useDataSourcesEnabled?: boolean;
    /**
     * whether to show toggle data button
     */
    mustUseDataSource?: boolean;
    /**
     * whether to close data source list panel after selecting a data source
     */
    closeDataSourceListOnSelect?: boolean;
    /**
     * callback when toggle data button is clicked
     */
    onToggleUseDataEnabled?: (useDataSourcesEnabled: boolean) => void;
    /**
     * callback when one data source is selected
     */
    onSelect?: (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs: SelectedDataSourceJson) => void;
    /**
     * callback when one data source is removed
     */
    onRemove?: (allSelectedDss: SelectedDataSourceJson[], currentRemovedDs: SelectedDataSourceJson) => void;
    /**
     * before selecting, the component will call this method to check if it can continue selecting
     */
    disableSelection?: (allSelectedDss: SelectedDataSourceJson[]) => boolean;
    /**
     * before remove, the component will call this method to check if it can continue removing
     */
    disableRemove?: (allSelectedDss: SelectedDataSourceJson[]) => boolean;
}
interface StateExtraProps {
    dataSources: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    dataSourcesInfo: ImmutableObject<{
        [dsId: string]: IMDataSourceInfo;
    }>;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _DataSourceChooser extends React.PureComponent<Props & StateExtraProps & ExtraProps, State> {
    constructor(props: any);
    getStyle: (theme: ThemeVariables) => SerializedStyles;
    onToggleUseDataEnabled: () => void;
    onSelect: (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs: SelectedDataSourceJson) => void;
    onRemove: (allSelectedDss: SelectedDataSourceJson[], currentRemovedDs: SelectedDataSourceJson) => void;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>, "theme" | "key" | "forwardedRef" | "dataSources" | "dataSourcesInfo" | "onSelect" | "types" | "isMultiple" | "useDataSourcesEnabled" | "onRemove" | "selectedDataSourceIds" | "mustUseDataSource" | "closeDataSourceListOnSelect" | "onToggleUseDataEnabled" | "disableSelection" | "disableRemove"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_DataSourceChooser>, "theme">>, "theme" | "key" | "forwardedRef" | "dataSources" | "dataSourcesInfo" | "onSelect" | "types" | "isMultiple" | "useDataSourcesEnabled" | "onRemove" | "selectedDataSourceIds" | "mustUseDataSource" | "closeDataSourceListOnSelect" | "onToggleUseDataEnabled" | "disableSelection" | "disableRemove"> & React.RefAttributes<any>, "theme" | "ref" | "key" | "forwardedRef" | "onSelect" | "types" | "isMultiple" | "useDataSourcesEnabled" | "onRemove" | "selectedDataSourceIds" | "mustUseDataSource" | "closeDataSourceListOnSelect" | "onToggleUseDataEnabled" | "disableSelection" | "disableRemove"> & Props>;
export default _default;
