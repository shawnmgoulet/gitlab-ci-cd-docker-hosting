/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, SerializedStyles } from 'jimu-core';
import { SelectedDataSourceJson } from '../../types';
interface Props {
    dsJsonWithRootId: SelectedDataSourceJson;
    onRemove: (dsJson: SelectedDataSourceJson) => void;
    disableRemove?: boolean;
}
export default class DsItem extends React.PureComponent<Props & {
    theme: ThemeVariables;
}, {}> {
    constructor(props: any);
    getStyle: (theme: ThemeVariables) => SerializedStyles;
    onRemove: () => void;
    render(): JSX.Element;
}
export {};
