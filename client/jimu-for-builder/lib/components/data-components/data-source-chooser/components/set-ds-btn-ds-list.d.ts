/// <reference types="react" />
import { React, DataSourceJson, ThemeVariables, ImmutableObject, ImmutableArray, DataSourceManager, IntlShape, IMDataSourceInfo } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../../types';
interface State {
    isDsListShown: boolean;
    isDataSourceInited: boolean;
}
interface Props {
    types: ImmutableArray<AllDataSourceTypes>;
    selectedDataSourceIds?: ImmutableArray<string>;
    isMultiple?: boolean;
    closeDataSourceListOnSelect?: boolean;
    onSelect?: (selectedDsJsons: SelectedDataSourceJson[], selectedDsJson?: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJsons: SelectedDataSourceJson[], removedDsJson?: SelectedDataSourceJson) => void;
    disableSelection?: (selectedDsJsons: SelectedDataSourceJson[]) => boolean;
    disableRemove?: (selectedDsJsons: SelectedDataSourceJson[]) => boolean;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
interface StateExtraProps {
    dataSources: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    dataSourcesInfo: ImmutableObject<{
        [dsId: string]: IMDataSourceInfo;
    }>;
}
export default class SetDsBtnDsList extends React.PureComponent<Props & ExtraProps & StateExtraProps, State> {
    panelWidth: string;
    defaultModalStyle: {
        position: string;
        top: string;
        bottom: string;
        right: string;
        left: string;
        width: string;
        height: string;
        zIndex: string;
        borderRight: string;
    };
    modalStyle: object;
    dsManager: DataSourceManager;
    constructor(props: any);
    componentDidUpdate(prevProps: Props & ExtraProps & StateExtraProps): void;
    changeInitStatus: (isInited: boolean) => void;
    disableSelection: () => boolean;
    disableRemove: () => boolean;
    getDssWithRootId: (dataSourceIds: string[]) => SelectedDataSourceJson[];
    getModalStyle: () => {
        position: string;
        top: string;
        bottom: string;
        right: string;
        left: string;
        width: string;
        height: string;
        zIndex: string;
        borderRight: string;
    };
    getDsLabel: (dsId: string) => string;
    onDsListClose: () => void;
    onDsSelected: (selectedDsJson: SelectedDataSourceJson) => void;
    onDsRemoved: (removedDsJson: SelectedDataSourceJson) => void;
    toggleDsList: () => void;
    render(): JSX.Element;
}
export {};
