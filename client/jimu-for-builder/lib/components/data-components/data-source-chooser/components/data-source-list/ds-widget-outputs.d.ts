/// <reference types="seamless-immutable" />
/// <reference types="react" />
import { DataSource, React, IMWidgetJson, ImmutableArray, IntlShape } from 'jimu-core';
import { SelectedDataSourceJson, AllDataSourceTypes } from '../../../types';
interface State {
    selectedType: AllDataSourceTypes;
    selectedWidgetId: string;
    toUseRootDss: {
        [widgetId: string]: DataSource[];
    };
    toUseChildDss: {
        [rootDsId: string]: DataSource[];
    };
    rootDss: {
        [widgetId: string]: DataSource[];
    };
    childDss: {
        [rootDsId: string]: DataSource[];
    };
    toUseWidgets: IMWidgetJson[];
    hasErrorSelectedDss: boolean;
}
interface Props {
    isDataSourceInited: boolean;
    types: ImmutableArray<AllDataSourceTypes>;
    selectedDsIds: ImmutableArray<string>;
    intl: IntlShape;
    onSelect: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    disableSelection?: boolean;
    disableRemove?: boolean;
}
export default class DataSourceWidgetOutputs extends React.PureComponent<Props, State> {
    __unmount: boolean;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: any, prevState: any): void;
    componentWillUnmount(): void;
    initData: () => void;
    setToUseDataSources: (usedTypes: any) => void;
    setDataSources: () => void;
    setSelectedType: (types: AllDataSourceTypes[]) => void;
    setSelectedWidgetId: (widgetId: string) => void;
    setHasErrorDss: (hasErrorDss: boolean) => void;
    getRootDss: () => {
        [widgetId: string]: DataSource[];
    };
    getToUseWidgets: (toUseRootDss: {
        [widgetId: string]: DataSource[];
    }, toUseChildDss: {
        [rootDsId: string]: DataSource[];
    }) => import("seamless-immutable").ImmutableObject<import("jimu-core").WidgetJson>[];
    geToUseRootDss: (usedTypes: import("seamless-immutable").ImmutableArray<string>) => {
        [widgetId: string]: DataSource[];
    };
    getToUseChildDss: (usedTypes: import("seamless-immutable").ImmutableArray<string>) => Promise<{
        [rootDsId: string]: DataSource[];
    }>;
    getToUseChildDssByWidgetId: (wId: string) => {
        [rootDsId: string]: DataSource[];
    };
    getToUseTypes: () => AllDataSourceTypes[];
    getWhetherShowList: () => boolean;
    onWidgetClick: (e: any, wId: string) => void;
    onBackClick: () => void;
    render(): JSX.Element;
}
export {};
