/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, ImmutableArray, IntlShape } from 'jimu-core';
import { AllDataSourceTypes, SelectedDataSourceJson } from '../../../types';
export { AllDataSourceTypes, SelectedDataSourceJson };
interface State {
    isExternalDsShown: boolean;
}
interface Props {
    isDataSourceInited: boolean;
    types: ImmutableArray<AllDataSourceTypes>;
    selectedDsIds: ImmutableArray<string>;
    isMultiple?: boolean;
    disableSelection?: boolean;
    disableRemove?: boolean;
    onSelect?: (selectedDsJson: SelectedDataSourceJson) => void;
    onRemove?: (selectedDsJson: SelectedDataSourceJson) => void;
    onClose?: () => void;
    changeInitStatus?: (isInited: boolean) => void;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _DataSourceList extends React.PureComponent<Props & ExtraProps, State> {
    externalDsStyle: {
        width: string;
        height: string;
        maxWidth: string;
        margin: number;
    };
    fontSizeStyle: {
        fontSize: string;
    };
    titleStyle: {
        height: string;
        borderBottom: string;
    };
    constructor(props: any);
    showExternalDs: () => void;
    onSelectDataFinished: (dsJsons: import("seamless-immutable").ImmutableObject<import("jimu-core").DataSourceJson>[]) => void;
    onSelectDataCanceled: () => void;
    onToggleExternalDs: () => void;
    ExternalDs: JSX.Element;
    render(): JSX.Element;
}
declare const DataSourceList: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<React.PropsWithChildren<import("react-intl").WithIntlProps<any>>, "theme">>;
export default DataSourceList;
