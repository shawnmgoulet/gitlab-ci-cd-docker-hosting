/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, SerializedStyles, IntlShape, ImmutableArray, IMWidgetJson, ReactRedux, Immutable } from 'jimu-core';
interface State {
}
interface Props {
    useMapWidgetIds?: ImmutableArray<string>;
    onSelect?: (useMapWidgetIds: string[]) => void;
}
interface StateExtraProps {
    mapWidgetJsons: IMWidgetJson[];
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _MapChooser extends React.PureComponent<Props & StateExtraProps & ExtraProps, State> {
    noneMapWidget: IMWidgetJson;
    constructor(props: any);
    getStyle: (theme: ThemeVariables) => SerializedStyles;
    onSelect: (e: any) => void;
    getSelectedMapWidget: () => Immutable.ImmutableObject<import("jimu-core").WidgetJson>;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_MapChooser>, "theme">>, "theme" | "key" | "forwardedRef" | "onSelect" | "useMapWidgetIds" | "mapWidgetJsons"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_MapChooser>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & StateExtraProps & ExtraProps & React.RefAttributes<_MapChooser>, "theme">>, "theme" | "key" | "forwardedRef" | "onSelect" | "useMapWidgetIds" | "mapWidgetJsons"> & React.RefAttributes<any>, "theme" | "ref" | "key" | "forwardedRef" | "onSelect" | "useMapWidgetIds"> & Props>;
export default _default;
