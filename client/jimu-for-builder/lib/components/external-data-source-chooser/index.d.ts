/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
declare const ExternalDataSourceChooser: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<React.PropsWithChildren<import("react-intl").WithIntlProps<any>>, "theme">>;
export default ExternalDataSourceChooser;
