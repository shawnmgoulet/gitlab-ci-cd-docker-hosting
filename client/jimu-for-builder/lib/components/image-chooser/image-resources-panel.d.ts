/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
export declare const ImageResourcesPanel: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<React.PropsWithChildren<import("react-intl").WithIntlProps<any>>, "theme">>;
