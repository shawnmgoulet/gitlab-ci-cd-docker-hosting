/// <reference types="react" />
import { React } from 'jimu-core';
declare const _default: React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<React.PropsWithChildren<import("react-intl").WithIntlProps<any>>, "theme">>;
export default _default;
