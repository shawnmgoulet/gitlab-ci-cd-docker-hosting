/// <reference types="react" />
/** @jsx jsx */
import { React, IMAppConfig, ImmutableArray, ThemeVariables, SerializedStyles, BrowserSizeMode, ReactRedux, IntlShape } from 'jimu-core';
import { LinkParam } from './link-setting-types';
interface Props {
    className?: string;
    linkParam?: LinkParam;
    onSettingConfirm?: any;
    okButtonBottom?: string;
    selectAreaMaxHeight?: string;
    isLinkPageSetting?: boolean;
    dataSourceIds?: ImmutableArray<string>;
}
interface States {
    originLinkParam: LinkParam;
    linkParam: LinkParam;
}
interface StateExtraProps {
    appConfig: IMAppConfig;
    browserSizeMode: BrowserSizeMode;
}
interface ExtraProps {
    theme: ThemeVariables;
    intl: IntlShape;
}
export declare class _LinkSetting extends React.PureComponent<Props & ExtraProps & StateExtraProps, States> {
    constructor(props: any);
    componentWillMount(): void;
    getStyle: (theme: ThemeVariables) => SerializedStyles;
    render(): JSX.Element;
    getLinkTypeContent: () => JSX.Element;
    componentDidMount(): void;
    getLinkContent: (linkType: string) => JSX.Element;
    linkTypeChange: (e: any) => void;
    radioOpenTypeChange: (openType: string) => void;
    linkParamChange: (changedParam: LinkParam, isTypeSame?: boolean) => void;
    settingConfirm: () => void;
    isLinkParamOk: (linkParam: LinkParam) => boolean;
}
declare const _default: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_LinkSetting>, "theme">>, "key" | "forwardedRef" | "browserSizeMode" | "appConfig" | "theme" | "className" | "linkParam" | "onSettingConfirm" | "okButtonBottom" | "selectAreaMaxHeight" | "isLinkPageSetting" | "dataSourceIds"> & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_LinkSetting>, "theme">>;
}, Pick<Pick<import("react-intl").WithIntlProps<import("emotion-theming/types/helper").AddOptionalTo<Props & ExtraProps & StateExtraProps & React.RefAttributes<_LinkSetting>, "theme">>, "key" | "forwardedRef" | "browserSizeMode" | "appConfig" | "theme" | "className" | "linkParam" | "onSettingConfirm" | "okButtonBottom" | "selectAreaMaxHeight" | "isLinkPageSetting" | "dataSourceIds"> & React.RefAttributes<any>, "ref" | "key" | "forwardedRef" | "theme" | "className" | "linkParam" | "onSettingConfirm" | "okButtonBottom" | "selectAreaMaxHeight" | "isLinkPageSetting" | "dataSourceIds"> & Props>;
export default _default;
