/// <reference types="react" />
import { IMDataSourceJson, ImmutableArray, ImmutableObject, UrlParameters, IMUser, IntlShape, IMThemeVariables, IMLayoutItemJson, LayoutInfo, BoundingBox, DataSourceJson, IMWidgetJson, WidgetJson } from 'jimu-core';
export declare type AllWidgetSettingProps<T> = WidgetSettingProps & WidgetSettingInjectedProps<T>;
export interface SettingChangeFunction {
    (widgetJson: Partial<WidgetJson>, outputDataSourcesJson?: DataSourceJson[]): void;
}
export interface WidgetSettingProps extends React.HTMLAttributes<HTMLDivElement> {
    onSettingChange: SettingChangeFunction;
}
export interface WidgetItemSettingProps {
    layout: IMLayoutItemJson;
    onSettingChange: (layoutInfo: LayoutInfo, setting: any) => void;
    onPosChange: (layoutInfo: LayoutInfo, bbox: BoundingBox) => void;
}
export declare type IMUseDataSourcesJson = ImmutableArray<ImmutableObject<{
    dataSourceJson: IMDataSourceJson;
    rootDataSourceId?: string;
    fields?: string[];
}>>;
export declare type WidgetSettingInjectedProps<T> = Omit<IMWidgetJson, 'config'> & {
    dispatch: any;
    queryObject: ImmutableObject<UrlParameters>;
    config: T;
    locale: string;
    appI18nMessages: any;
    intl: IntlShape;
    token?: string;
    user?: IMUser;
    theme: IMThemeVariables;
    portalUrl?: string;
    portalSelf?: any;
};
