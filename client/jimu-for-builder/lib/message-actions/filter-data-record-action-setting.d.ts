/// <reference types="seamless-immutable" />
/// <reference types="react" />
/** @jsx jsx */
import { React, ActionSettingProps, SerializedStyles, ImmutableObject, DataSource, IMDataSourceInfo, ThemeVariables, Immutable, DataSourceJson, UseDataSource, ReactRedux, IMSqlExpression } from 'jimu-core';
import { SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
interface StateExtraProps {
    dataSources: ImmutableObject<{
        [dsId: string]: DataSourceJson;
    }>;
    dataSourcesInfo: ImmutableObject<{
        [dsId: string]: IMDataSourceInfo;
    }>;
}
interface ExtraProps {
    theme?: ThemeVariables;
}
interface States {
    isShowLayerList: boolean;
    currentLayerType: 'trigger' | 'action';
    isSqlExprShow: boolean;
}
interface Config {
    messageUseDataSource: UseDataSource;
    actionUseDataSource: UseDataSource;
    sqlExprObj?: IMSqlExpression;
    enabledDataRelationShip?: boolean;
}
export declare type IMConfig = ImmutableObject<Config>;
declare class _FilterDataRecordActionSetting extends React.PureComponent<ActionSettingProps<IMConfig> & ExtraProps & StateExtraProps, States> {
    modalStyle: any;
    constructor(props: any);
    openChooseLayerList: (currentLayerType: "action" | "trigger") => void;
    closeChooseLayerList: () => void;
    static defaultProps: {
        config: Immutable.ImmutableObject<{
            messageUseDataSource: any;
            actionUseDataSource: any;
            sqlExprObj: any;
            enabledDataRelationShip: boolean;
        }>;
    };
    getInitConfig: () => {
        messageUseDataSource: Immutable.ImmutableObject<UseDataSource>;
        actionUseDataSource: Immutable.ImmutableObject<UseDataSource>;
    };
    componentDidMount(): void;
    getStyle(theme: ThemeVariables): SerializedStyles;
    checkIsShowSetData: (widgetId: string) => boolean;
    getContent: () => JSX.Element;
    handleChooseLayer: (selectedDsJson: SelectedDataSourceJson) => void;
    handleRemoveLayerItemForTriggerLayer: () => void;
    handleRemoveLayerItemForActionLayer: () => void;
    showSqlExprPopup: () => void;
    toggleSqlExprPopup: () => void;
    onSqlExprBuilderChange: (sqlExprObj: Immutable.ImmutableObject<import("jimu-core").SqlExpression>) => void;
    onMessageFieldSelected: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>, ds: DataSource) => void;
    onActionFieldSelected: (allSelectedFields: Immutable.ImmutableObject<import("jimu-core").FieldSchema>[], field: Immutable.ImmutableObject<import("jimu-core").FieldSchema>, ds: DataSource) => void;
    swicthEnabledDataRelationShip: (checked: any) => void;
    checkTrigerLayerIsSameToActionLayer: () => boolean;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<React.FunctionComponent<import("emotion-theming/types/helper").AddOptionalTo<Pick<ActionSettingProps<Immutable.ImmutableObject<Config>> & ExtraProps & StateExtraProps & React.RefAttributes<_FilterDataRecordActionSetting>, "ref" | "theme" | "key" | "intl" | "dataSourcesInfo" | "dataSources" | "widgetId" | "messageType" | "actionId" | "messageWidgetId" | "onSettingChange" | "onDisableDoneBtn"> & Partial<Pick<ActionSettingProps<Immutable.ImmutableObject<Config>> & ExtraProps & StateExtraProps & React.RefAttributes<_FilterDataRecordActionSetting>, "config">> & Partial<Pick<{
    config: Immutable.ImmutableObject<{
        messageUseDataSource: any;
        actionUseDataSource: any;
        sqlExprObj: any;
        enabledDataRelationShip: boolean;
    }>;
}, never>>, "theme">>, Pick<import("emotion-theming/types/helper").AddOptionalTo<Pick<ActionSettingProps<Immutable.ImmutableObject<Config>> & ExtraProps & StateExtraProps & React.RefAttributes<_FilterDataRecordActionSetting>, "ref" | "theme" | "key" | "intl" | "dataSourcesInfo" | "dataSources" | "widgetId" | "messageType" | "actionId" | "messageWidgetId" | "onSettingChange" | "onDisableDoneBtn"> & Partial<Pick<ActionSettingProps<Immutable.ImmutableObject<Config>> & ExtraProps & StateExtraProps & React.RefAttributes<_FilterDataRecordActionSetting>, "config">> & Partial<Pick<{
    config: Immutable.ImmutableObject<{
        messageUseDataSource: any;
        actionUseDataSource: any;
        sqlExprObj: any;
        enabledDataRelationShip: boolean;
    }>;
}, never>>, "theme">, "ref" | "theme" | "key" | "intl" | "config" | "widgetId" | "messageType" | "actionId" | "messageWidgetId" | "onSettingChange" | "onDisableDoneBtn"> & ActionSettingProps<Immutable.ImmutableObject<Config>>>;
export default _default;
