/// <reference types="seamless-immutable" />
import { DataSourceManager } from 'jimu-core';
/**
 * Used to manage app data sources in builder.
 * The data source object is saved in this class, and the data source info is in builder's store state.
 * So, we need to make sure the builder's data source does not have the same data source id with app's.
 */
export default class AppDataSourceManager extends DataSourceManager {
    static _instance: AppDataSourceManager;
    static getInstance(): AppDataSourceManager;
    protected getAppConfig(): import("seamless-immutable").ImmutableObject<import("jimu-core").AppConfig>;
}
