export * from './editor';
export * from './type';
import * as richTextUtils from './utils';
export { richTextUtils };
