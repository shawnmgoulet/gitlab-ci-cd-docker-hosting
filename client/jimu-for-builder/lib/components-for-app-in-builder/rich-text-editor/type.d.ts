/// <reference types="react" />
import { React, SerializedStyles } from 'jimu-core';
import { TextFormats, PopperProps, LinkTarget } from 'jimu-ui';
import { ImmutableArray, ImmutableObject } from 'seamless-immutable';
import { Blot } from 'parchment/dist/src/blot/abstract/blot';
import { BubbleOption, AroundExpressionOption } from './quill/plugins';
export declare enum FormatType {
    INLINE = 0,
    BLOCK = 1,
    EMBED = 2
}
export declare type Formats = {
    [x: string]: any;
};
export declare type IMFormats = ImmutableObject<Formats>;
export declare type Operation = {
    insert?: string | object;
    delete?: number;
    retain?: number;
    attributes?: Formats;
};
export interface Delta {
    ops: Operation[];
    length?: () => any;
    diff?: (delta: Delta) => Delta;
    partition?: (func: (op: Operation) => boolean) => [Operation[], Operation[]];
    filter?: (func: (op: Operation) => boolean) => Operation[];
    forEach?: (func: (op: Operation) => void) => void;
}
export declare type QuillValue = Delta | string;
export interface QuillSelection {
    index: number;
    length: number;
}
export interface ClipboardStatic {
    convert(html?: string): Delta;
    addMatcher(selectorOrNodeType: string | number, callback: (node: any, delta: Delta) => Delta): void;
    dangerouslyPasteHTML(html: string, source?: Sources): void;
    dangerouslyPasteHTML(index: number, html: string, source?: Sources): void;
}
export declare type UnprivilegedEditor = {
    focus: () => void;
    blur: () => void;
    getLength: () => number;
    getText: (index?: number, length?: number) => string;
    getContents: (index?: number, length?: number) => Delta;
    getSelection: (focus?: boolean) => QuillSelection | null;
    getBounds: (index: number, length?: number) => ClientRect;
    getFormat: (range?: QuillSelection) => Formats;
    getIndex: (blot: any) => number;
    getLeaf: (index: number) => [any, number];
};
export interface CustomEditorMethod {
    getHTML: () => string;
}
export declare type UnprivilegedEditorWithCustom = UnprivilegedEditor & CustomEditorMethod;
export declare type TextChangeHandler = (delta: Delta, oldContents: Delta, source: Sources) => any;
export declare type SelectionChangeHandler = (range: QuillSelection, oldRange: QuillSelection, source: Sources) => any;
export declare type EditorChangeHandler = ((name: 'text-change', delta: Delta, oldContents: Delta, source: Sources) => any) | ((name: 'selection-change', range: QuillSelection, oldRange: QuillSelection, source: Sources) => any);
export declare type EventType = 'text-change' | 'selection-change' | 'editor-change';
export declare type Hanldes = TextChangeHandler | SelectionChangeHandler | EditorChangeHandler;
export interface EventEmitter {
    on: (eventName: EventType, handler?: Hanldes) => EventEmitter;
    off: (eventName: EventType, handler?: Hanldes) => EventEmitter;
    once: (eventName: EventType, handler?: Hanldes) => EventEmitter;
}
export interface QuillProperties {
    options?: {
        [x: string]: any;
    };
    editor?: any;
}
export declare type Editor = {
    root: HTMLDivElement;
    scroll: Blot;
    clipboard: ClipboardStatic;
    hasFocus: () => boolean;
    disable: () => void;
    enable: (enabled?: boolean) => void;
    setContents: (delta: Delta, source?: Sources) => Delta;
    updateContents: (delta: Delta, source?: Sources) => Delta;
    setSelection: (range: QuillSelection | number, source?: Sources) => void;
    getLeaf: (index: number) => any;
    deleteText: (index: number, length: number, source?: Sources) => Delta;
    insertText: (index: number, text: string, source?: Sources) => Delta;
    insertEmbed: (index: Number, type: String, value: any, source?: Sources) => Delta;
    format: (name: string, value: any, source?: Sources) => Delta;
    formatLine: (index: number, length: number, format: string, value: any, source?: Sources) => Delta;
    formatText: (index: number, length: number, format: string, value: any, source?: Sources) => Delta;
    removeFormat: (index: number, length: number, source?: Sources) => Delta;
    getLine: (index: number) => any;
    getLines(index?: number, length?: number): any[];
} & UnprivilegedEditor & EventEmitter & QuillProperties;
export declare type Sources = 'api' | 'user' | 'silent';
export declare type RenderPlugin = (quill: Editor, option: PluginOption) => React.ReactNode;
export declare type PluginOption = BubbleOption | AroundExpressionOption;
export declare type PluginOptions = {
    bubble?: BubbleOption;
    expression?: AroundExpressionOption;
};
export interface QuillOptions {
    debug?: string | boolean;
    bounds?: HTMLElement | string;
    formats?: string[];
    modules?: {
        [x: string]: any;
    };
    plugins?: PluginOptions;
    placeholder?: string;
    readOnly?: boolean;
    scrollingContainer?: HTMLElement | string;
    tabIndex?: number;
    theme?: string;
}
export declare type QuillComponentProps = {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    injectStyle?: SerializedStyles;
    value?: QuillValue;
    defaultValue?: QuillValue;
    enabled?: boolean;
    children?: any;
    preserveWhitespace: boolean;
    onChange?: (value: QuillValue, delta: Delta, source: Sources, editor: UnprivilegedEditor) => any;
    onChangeSelection?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onCreate?: (editor: Editor) => void;
    onUnMount?: () => any;
    onFocus?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onBlur?: (nextSelection: QuillSelection, source: Sources, editor: UnprivilegedEditor) => any;
    onKeyPress?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
    onKeyDown?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
    onClick?: (event: React.MouseEvent<HTMLDivElement>, quill: UnprivilegedEditor) => void;
    onDoubleClick?: (event: React.MouseEvent<HTMLDivElement>, quill: UnprivilegedEditor) => void;
    onKeyUp?: (event: React.KeyboardEvent<HTMLDivElement>) => void;
};
export declare type QuillProps = QuillComponentProps & QuillOptions;
export declare type PluginNodeProps = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormats, value: any, type: FormatType, id?: string) => void;
    onTriggerUserSelect?: () => void;
    effectAll?: boolean;
    selected?: boolean;
};
export interface HeaderProps {
    draggable?: boolean;
    className?: string;
    show?: boolean;
    text?: string;
    onClose?: (e: React.MouseEvent<HTMLDivElement>) => void;
}
export declare type PluginProps = PluginNodeProps & {
    source?: Sources;
    quill: Editor;
    useHooks?: boolean;
    onSelectionChange?: (range: QuillSelection, source: Sources) => any;
    onTextChange?: (html: string, delta: Delta, source: Sources) => any;
    onFormatsChange?: (formats: Formats, source: Sources) => void;
};
export declare type PluginPopperProps = PluginProps & {
    header?: HeaderProps;
} & PopperProps;
export interface QuillLinkValue {
    id: string;
    href: string;
    target: LinkTarget;
    class?: string;
}
export interface ExpressionValue {
    expid: string;
    name: string;
    class?: string;
}
