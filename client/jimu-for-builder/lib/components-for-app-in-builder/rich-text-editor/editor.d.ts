/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { QuillProps, QuillValue, Editor, Delta, QuillSelection, Sources, PluginOptions } from './type';
import { BubbleOption, AroundExpressionOption } from './quill/plugins';
interface State {
    value?: QuillValue;
    selection?: QuillSelection;
    renderCount?: number;
}
interface Snapshot {
    regenerate: boolean;
}
export declare class RichTextEditor extends React.Component<QuillProps, State, Snapshot> {
    quill: Editor;
    lastDeltaChangeSet: Delta;
    quillDelta: Delta;
    quillSelection: QuillSelection;
    editingArea: HTMLElement;
    handleSelectionChange: (range: QuillSelection, oldRange: QuillSelection, source: Sources) => void;
    dirtyProps: string[];
    cleanProps: string[];
    static defaultProps: Partial<QuillProps>;
    constructor(props: any);
    private isControlled;
    getSnapshotBeforeUpdate(prevProps: any, prevState: any): Snapshot;
    componentDidUpdate(prevProps: any, prevState: any, snapshot: Snapshot): void;
    setEditorContents: (contents: QuillValue) => void;
    componentDidMount(): void;
    aspectForCreateQuill: () => void;
    componentWillUnmount(): void;
    shouldComponentUpdate(nextProps: any, nextState: any): boolean;
    private getEditorConfig;
    private getEditor;
    private getEditingArea;
    private getEditorContents;
    private getEditorSelection;
    private renderEditingArea;
    bubblePlugin: (quill: Editor, props: BubbleOption) => React.ReactNode;
    expressionPlugin: (quill: Editor, props: AroundExpressionOption) => React.ReactNode;
    renderPlugins: (plugins: PluginOptions) => JSX.Element[];
    onDoubleClick: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getExpNode: (node: HTMLElement) => Node & ParentNode;
    onClick: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
    private onEditorChangeText;
    private onEditorChangeSelection;
    enable: (enabled?: boolean) => void;
    focus: () => void;
    blur: () => void;
    private createEditor;
    private hookEditor;
    private unhookEditor;
}
export {};
