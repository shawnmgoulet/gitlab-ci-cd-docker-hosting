/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, ReactRedux, IntlShape, ImmutableArray } from 'jimu-core';
import { TextFormats } from 'jimu-ui';
import { FormatType, Formats } from '../../../type';
export interface BubbleFormatsPorps {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormats, value: any, type: FormatType) => void;
    onTriggerUserSelect?: () => void;
    disableLink?: boolean;
}
interface ExtraProps {
    theme?: ThemeVariables;
}
interface ExtraProps2 {
    intl: IntlShape;
}
export declare class _BubbleFormats extends React.PureComponent<BubbleFormatsPorps & ExtraProps & ExtraProps2> {
    static defaultProps: Partial<BubbleFormatsPorps & ExtraProps & ExtraProps2>;
    getStyle: () => import("jimu-core").SerializedStyles;
    translate: (id: string) => string;
    render(): JSX.Element;
}
export declare const BubbleFormats: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<BubbleFormatsPorps & ExtraProps & ExtraProps2, "className" | "style" | "theme" | "onChange" | "dataSourceIds" | "formats" | "onTriggerUserSelect" | "disableLink"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<BubbleFormatsPorps & ExtraProps & ExtraProps2>;
}, Pick<Pick<BubbleFormatsPorps & ExtraProps & ExtraProps2, "className" | "style" | "theme" | "onChange" | "dataSourceIds" | "formats" | "onTriggerUserSelect" | "disableLink"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>, "className" | "style" | "ref" | "key" | "onChange" | "forwardedRef" | "dataSourceIds" | "formats" | "onTriggerUserSelect" | "disableLink"> & BubbleFormatsPorps>;
export {};
