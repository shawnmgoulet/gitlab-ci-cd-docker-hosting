/// <reference types="react" />
/** @jsx jsx */
import { React, Expression as ExpressionValue, ImmutableArray } from 'jimu-core';
import { ExpressionFrom } from '../../../../../common-components/expression-builder';
import { FormatType, Formats } from '../../../type';
import { TextFormats } from 'jimu-ui';
export interface ExpressionNodeProps {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    onChange?: (key: TextFormats, value: any, type: FormatType, id?: string) => void;
    onTriggerUserSelect?: () => void;
}
export declare class ExpressionNode extends React.PureComponent<ExpressionNodeProps> {
    from: ExpressionFrom[];
    static defaultProps: Partial<ExpressionNodeProps>;
    private getStyle;
    onExpressionChange: (expression: ExpressionValue) => void;
    render(): JSX.Element;
}
