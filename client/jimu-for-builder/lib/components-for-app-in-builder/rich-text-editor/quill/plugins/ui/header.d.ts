/// <reference types="react" />
/** @jsx jsx */
import { React } from 'jimu-core';
import { HeaderProps } from '../../../type';
export declare class Header extends React.PureComponent<HeaderProps> {
    getStyle: () => import("jimu-core").SerializedStyles;
    render(): JSX.Element;
}
