/// <reference types="react" />
/** @jsx jsx */
import { React, ImmutableArray } from 'jimu-core';
import { Editor, FormatType, QuillSelection, Sources, Formats } from '../../type';
import { VirtualReference, TextFormats } from 'jimu-ui';
import { IMLinkParam } from 'jimu-for-builder';
interface State {
    show?: boolean;
    bounds?: ClientRect;
    range?: QuillSelection;
    formats: Formats;
}
interface InjectProps {
    quill: Editor;
}
export declare type BubbleOption = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    zIndex?: number;
    source?: Sources;
    /**
     * Some times, the fomrats we got from quill does not contain enough information.
     * For example, for expression format, we only save id in quill object, But its details
     *  are stored in the wdiegt config.
     *  So in this way, you can mix some information into formats by `minFormats`.
     */
    mixinFormats?: (formats: Formats) => Formats;
    onChange?: (key: TextFormats, value: any, type: FormatType) => void;
    onInsertLink?: (id: string, value: IMLinkParam) => void;
};
export declare class Bubble extends React.PureComponent<BubbleOption & InjectProps, State> {
    static defaultProps: Partial<BubbleOption & InjectProps>;
    debounce: any;
    constructor(props: any);
    getStyle: () => import("jimu-core").SerializedStyles;
    componentDidMount(): void;
    onEditorChange: () => void;
    updateFormats: (quill: Editor, range: QuillSelection) => void;
    updateBounds: (quill: Editor, range: QuillSelection) => void;
    isSameBounds: (bounds1: ClientRect, bounds2: ClientRect, allowedOffset: number) => boolean;
    approximatelyEqualTo: (number1: number, number2: number, allowedOffset: number) => boolean;
    mixinBounds: (bounds: ClientRect) => ClientRect;
    getReference: () => VirtualReference;
    handleChange: (key: TextFormats, value: any, type: FormatType) => void;
    onTriggerUserSelect: () => void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export {};
