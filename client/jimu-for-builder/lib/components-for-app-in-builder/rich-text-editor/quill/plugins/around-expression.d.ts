/// <reference types="react" />
/// <reference types="react-draggable" />
/** @jsx jsx */
import { React, IMExpression, Expression as ExpressionType, ImmutableArray } from 'jimu-core';
import { Editor, FormatType, HeaderProps, Sources, QuillSelection, Formats } from '../../type';
import { TextFormats, Placement } from 'jimu-ui';
interface State {
    range?: QuillSelection;
    position: {
        x: number;
        y: number;
    };
    formats: Formats;
}
interface InjectProps {
    quill: Editor;
}
export declare type AroundExpressionOption = {
    className?: string;
    style?: any;
    dataSourceIds?: ImmutableArray<string>;
    formats?: Formats;
    header?: HeaderProps;
    open?: boolean;
    placement?: Placement;
    zIndex?: number;
    source?: Sources;
    /**
   * Some times, the fomrats we got from quill does not contain enough information.
   * For example, for expression format, we only save id in quill object, But its details
   *  are stored in the wdiegt config.
   *  So in this way, you can mix some information into formats by `minFormats`.
   */
    mixinFormats?: (formats: Formats) => Formats;
    onInsert?: (expid: string, expression?: IMExpression) => void;
};
export declare class AroundExpression extends React.PureComponent<AroundExpressionOption & InjectProps, State> {
    static defaultProps: Partial<AroundExpressionOption & InjectProps>;
    selectionDebounce: any;
    constructor(props: any);
    componentDidMount(): void;
    onSelectionChange: (selection: QuillSelection, oldSelection: QuillSelection, source: Sources) => void;
    onDragStop: (data: import("react-draggable").DraggableData) => void;
    onPopperCreate: (data: import("popper.js").default.Data) => void;
    onUserSelect: () => void;
    getStyle: () => import("jimu-core").SerializedStyles;
    handleChange: (key: TextFormats, expression: ExpressionType, type: FormatType) => void;
    updateExpression: (expid: string, name: string, selection: QuillSelection) => void;
    insertExpression: (expid: string, name: string, selection: QuillSelection) => void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export {};
