declare const Embed: any;
import { ExpressionValue } from '../../type';
export declare class Expression extends Embed {
    static blotName: string;
    static tagName: string;
    domNode: any;
    declareClass: 'Expression';
    static create(value?: ExpressionValue): any;
    static formats(domNode: any): {};
    static value(domNode: any): {
        expid: any;
        name: any;
        class: any;
    };
    format(name: any, value: any): void;
}
export {};
