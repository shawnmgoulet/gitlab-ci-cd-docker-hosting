/// <reference types="react" />
/** @jsx jsx */
import { React, ThemeVariables, ImmutableArray, ReactRedux, IntlShape } from 'jimu-core';
import { TextFormats } from 'jimu-ui';
import { StringMap, FormatType } from '../type';
interface Props {
    dataSourceIds?: ImmutableArray<string>;
    className?: string;
    style?: any;
    formats?: StringMap;
    onChange?: (key: TextFormats, value: any, type: FormatType) => void;
    onUserSelect?: () => any;
}
interface ExtraProps {
    theme?: ThemeVariables;
}
interface ExtraProps2 {
    intl: IntlShape;
}
export declare class _FormatsPanel extends React.PureComponent<Props & ExtraProps & ExtraProps2> {
    static defaultProps: Partial<Props & ExtraProps & ExtraProps2>;
    getStyle: () => import("jimu-core").SerializedStyles;
    nls: (id: string) => string;
    render(): JSX.Element;
}
export declare const FormatsPanel: ReactRedux.ConnectedComponentClass<React.ForwardRefExoticComponent<Pick<Props & ExtraProps & ExtraProps2, "style" | "theme" | "className" | "onChange" | "dataSourceIds" | "formats" | "onUserSelect"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>> & {
    WrappedComponent: React.ComponentType<Props & ExtraProps & ExtraProps2>;
}, Pick<Pick<Props & ExtraProps & ExtraProps2, "style" | "theme" | "className" | "onChange" | "dataSourceIds" | "formats" | "onUserSelect"> & {
    forwardedRef?: React.Ref<any>;
} & React.RefAttributes<any>, "ref" | "style" | "key" | "className" | "onChange" | "forwardedRef" | "dataSourceIds" | "formats" | "onUserSelect"> & Props>;
export {};
