import { IRequestOptions } from '@esri/arcgis-rest-request';
import { IItem } from '@esri/arcgis-rest-types';
import { ISearchOptions, SearchQueryBuilder, ISearchResult, IUserItemOptions, IUpdateItemOptions, IUpdateItemResponse, ICreateItemOptions, ICreateItemResponse, IItemDataOptions, IItemResourceOptions, IItemResourceResponse } from '@esri/arcgis-rest-portal';
export declare function searchItems(search: string | ISearchOptions | SearchQueryBuilder): Promise<ISearchResult<IItem>>;
export declare function searchTemplate(search: string | ISearchOptions | SearchQueryBuilder): Promise<ISearchResult<IItem>>;
export declare function removeItem(requestOptions: IUserItemOptions): Promise<{
    success: boolean;
    itemId: string;
}>;
export declare function updateItem(requestOptions: IUpdateItemOptions): Promise<IUpdateItemResponse>;
export declare function updateAppThumbnail(requestOptions: IUpdateItemOptions): Promise<IUpdateItemResponse>;
export declare function createItem(requestOptions: ICreateItemOptions): Promise<ICreateItemResponse>;
export declare function getItem(id: string, requestOptions?: IRequestOptions, isFromCreateTemplate?: boolean): Promise<IItem>;
export declare function getItemData(id: string, requestOptions?: IItemDataOptions, isFromCreateTemplate?: boolean): Promise<any>;
export declare function getItemResources(id: string, requestOptions?: IRequestOptions, isFromCreateTemplate?: boolean): Promise<any>;
export declare function addItemResource(requestOptions: IItemResourceOptions): Promise<IItemResourceResponse>;
export declare function updateItemResource(requestOptions: IItemResourceOptions): Promise<IItemResourceResponse>;
export declare function removeItemResource(requestOptions: IItemResourceOptions): Promise<{
    success: boolean;
}>;
export declare function getResourceOrigin(isFromCreateTemplate?: boolean): string;
