import * as appServices from './app-service';
import * as templatesServices from './templates-server';
import * as restServices from './rest-service';
import * as localRestServices from './local-rest-service';
import * as addedPortalRestServices from './added-portal-rest-service';
export { appServices, restServices, localRestServices, templatesServices, addedPortalRestServices };
