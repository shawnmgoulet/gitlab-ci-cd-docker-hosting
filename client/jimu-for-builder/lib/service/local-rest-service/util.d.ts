export declare function getOriginUrl(): string;
export declare function getUserContentUrl(): string;
export declare function getUserName(): string;
