/// <reference types="react" />
import { React, ReactRedux } from 'jimu-core';
import { IMHistoryState } from '../lib/app-state-history-extension';
interface ExtraProps {
    currentPageId: string;
    stateHistory?: IMHistoryState;
}
declare class BuilderKeyboardComponentInner extends React.PureComponent<ExtraProps, {}> {
    isSupportKeyboard: () => boolean;
    switchAppMode: () => void;
    undo: () => void;
    redo: () => void;
    deleteSelectedItem: () => void;
    onKeyboardTrigger: (event: KeyboardEvent) => void;
    isMac: () => boolean;
    render(): JSX.Element;
}
declare const _default: ReactRedux.ConnectedComponentClass<typeof BuilderKeyboardComponentInner, Pick<ExtraProps, never>>;
export default _default;
