declare const _default: {
    chooseDsForFilter: string;
    filterNone: string;
    filterDataRelationShip: string;
    filterMessageField: string;
    chooseDsForSelect: string;
    selectNone: string;
    selectDataRelationShip: string;
    selectMessageField: string;
};
export default _default;
