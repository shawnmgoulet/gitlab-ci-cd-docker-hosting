export declare function crateAppByTemplate(appId: string): Promise<boolean>;
export declare function crateTemplateByApp(appId: string): Promise<boolean>;
export declare function createAppByDefaultTemplate(title: string, templateName: string): Promise<any>;
export declare function duplicateTemplateFromDefault(title: string, templateName: string): Promise<any>;
