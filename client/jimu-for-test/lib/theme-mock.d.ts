declare const _default: {
    darkTheme: boolean;
    coloration: string;
    colors: {
        white: string;
        black: string;
        transparent: string;
        primary: string;
        secondary: string;
        info: string;
        success: string;
        warning: string;
        danger: string;
        light: string;
        dark: string;
        palette: {
            primary: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            secondary: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            info: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            success: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            warning: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            danger: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            light: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
            dark: {
                100: string;
                200: string;
                300: string;
                400: string;
                500: string;
                600: string;
                700: string;
                800: string;
                900: string;
            };
        };
    };
    typography: {
        fontFamilyBase: string;
        fontSizeRoot: string;
        fontSizeBase: string;
        sizes: {
            default: string;
            sm: string;
            lg: string;
            display1: string;
            display2: string;
            display3: string;
            display4: string;
            display5: string;
            display6: string;
        };
        weights: {
            extraLight: string;
            light: string;
            medium: string;
            bold: string;
            extraBold: string;
        };
        lineHeights: {
            medium: number;
            sm: number;
            lg: number;
        };
        headings: {
            h1FontSize: string;
            h2FontSize: string;
            h3FontSize: string;
            h4FontSize: string;
            h5FontSize: string;
            h6FontSize: string;
            fontFamily: string;
            fontWeight: string;
            fontStyle: string;
            lineHeight: number;
            color: string;
            marginBottom: string;
        };
        paragraph: {
            marginBottom: string;
        };
        label: {
            marginBottom: string;
        };
        hr: {
            marginY: string;
            borderColor: string;
            borderWidth: string;
        };
        small: {
            fontSize: string;
        };
        mark: {
            padding: string;
            bg: string;
        };
    };
    spacer: string;
    sizes: {
        0: number;
        1: string;
        2: string;
        3: string;
        4: string;
        5: string;
    };
    gutters: {
        0: number;
        1: string;
        2: string;
        3: string;
        4: string;
        5: string;
    };
    border: {
        color: string;
        width: string;
    };
    borderRadiuses: {
        none: string;
        sm: string;
        default: string;
        lg: string;
        circle: string;
        pill: string;
    };
    boxShadows: {
        none: string;
        sm: string;
        default: string;
        lg: string;
    };
    body: {
        bg: string;
        color: string;
        fontFamily: string;
        fontWeight: string;
        fontSize: string;
        fontStyle: string;
        lineHeight: number;
    };
    header: {
        bg: string;
        color: string;
    };
    footer: {
        bg: string;
        color: string;
    };
    link: {
        color: string;
        decoration: string;
        hover: {
            color: string;
            decoration: string;
        };
    };
    components: {
        alert: {
            paddingY: string;
            paddingX: string;
            marginBottom: string;
            borderRadius: string;
            linkFontWeight: string;
            borderWidth: string;
            bgLevel: string;
            borderLevel: string;
            colorLevel: string;
        };
        badge: {
            fontSize: string;
            fontWeight: string;
            paddingX: string;
            paddingY: string;
            borderRadius: string;
            pill: {
                paddingX: string;
                borderRadius: string;
            };
        };
        breadcrumb: {
            bg: string;
            activeColor: string;
            paddingY: string;
            paddingX: string;
            marginBottom: string;
            borderRadius: string;
            item: {
                color: string;
                hoverColor: string;
                padding: string;
            };
            divider: {
                content: string;
                color: string;
            };
        };
        button: {
            sizes: {
                default: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                };
                sm: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                };
                lg: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                };
            };
            variants: {
                default: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            color: string;
                            width: string;
                        };
                    };
                    hover: {
                        bg: string;
                    };
                    active: {
                        color: string;
                        bg: string;
                        border: {
                            color: string;
                            width: string;
                        };
                    };
                    disabled: {
                        color: string;
                        bg: string;
                        border: {
                            color: string;
                            width: string;
                        };
                        shadow: string;
                        opacity: number;
                    };
                    focus: {
                        shadow: string;
                    };
                };
                primary: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                        };
                    };
                    hover: {
                        bg: string;
                    };
                    active: {
                        bg: string;
                    };
                    disabled: {
                        color: string;
                        bg: string;
                        shadow: string;
                    };
                    focus: {
                        shadow: string;
                    };
                };
                secondary: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                            color: string;
                        };
                    };
                    hover: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                            color: string;
                        };
                    };
                    active: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                            color: string;
                        };
                    };
                    disabled: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                            color: string;
                        };
                        shadow: string;
                    };
                    focus: {
                        shadow: string;
                    };
                };
                tertiary: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            width: number;
                        };
                    };
                    hover: {
                        color: string;
                    };
                    active: {
                        color: string;
                        bg: string;
                    };
                    disabled: {
                        color: string;
                    };
                    focus: {
                        shadow: string;
                    };
                };
                danger: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                        };
                    };
                    hover: {
                        bg: string;
                    };
                    active: {
                        bg: string;
                    };
                    disabled: {
                        color: string;
                        bg: string;
                        shadow: string;
                    };
                    focus: {
                        shadow: string;
                    };
                };
                link: {
                    default: {
                        color: string;
                        bg: string;
                        border: {
                            width: string;
                        };
                        decoration: string;
                    };
                    hover: {
                        color: string;
                        decoration: string;
                    };
                    active: {
                        color: string;
                    };
                    disabled: {
                        color: string;
                    };
                    focus: {
                        shadow: string;
                        decoration: string;
                    };
                };
            };
            icon: {
                spacing: string;
                size: string;
            };
        };
        card: {
            spacer: {
                y: string;
                x: string;
            };
            border: {
                width: string;
                color: string;
            };
            borderRadius: string;
            innerBorderRadius: string;
            capBg: string;
            bg: string;
            img: {
                margin: string;
                overlayPadding: string;
            };
            groupMargin: string;
            deckMargin: string;
            columns: {
                count: number;
                gap: string;
                margin: string;
            };
        };
        close: {
            fontSize: string;
            fontWeight: string;
            color: string;
            textShadow: string;
        };
        dropdown: {
            minWidth: string;
            paddingY: string;
            spacer: string;
            bg: string;
            border: {
                color: string;
                width: string;
            };
            borderRadius: string;
            divider: {
                bg: string;
            };
            boxShadow: string;
            link: {
                color: string;
                hoverColor: string;
                hoverBg: string;
                activeColor: string;
                activeBg: string;
                disabledColor: string;
            };
            item: {
                paddingY: string;
                paddingX: string;
            };
            header: {
                color: string;
            };
        };
        form: {
            group: {
                marginBottom: string;
            };
            text: {
                marginTop: string;
            };
            check: {
                inline: {
                    marginX: string;
                    inputMarginX: string;
                };
            };
        };
        input: {
            bg: string;
            disabledBg: string;
            color: string;
            border: {
                color: string;
                width: string;
            };
            boxShadow: any;
            focus: {
                bg: string;
                borderColor: string;
                color: string;
                width: string;
                boxShadow: any;
            };
            placeHolderColor: string;
            plaintextColor: string;
            sizes: {
                default: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                    height: string;
                };
                sm: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                    height: string;
                };
                lg: {
                    fontSize: string;
                    lineHeight: number;
                    paddingX: string;
                    paddingY: string;
                    borderRadius: string;
                    height: string;
                };
            };
            transition: string;
            checkbox: {
                size: string;
                border: {
                    color: string;
                    width: string;
                };
                bg: string;
                hover: {
                    borderColor: string;
                };
                checked: {
                    color: string;
                    bg: string;
                };
                focusColor: string;
                iconSize: string;
            };
            radio: {
                size: string;
                border: {
                    color: string;
                    width: string;
                };
                hover: {
                    borderColor: string;
                };
                bg: string;
                checked: {
                    color: string;
                    bg: string;
                };
                focusColor: string;
                iconSize: string;
            };
            switch: {
                width: string;
                height: string;
                bg: string;
                checkedBg: string;
                slider: {
                    gap: string;
                    height: string;
                    width: string;
                    color: string;
                };
            };
            slider: {
                sizes: {
                    default: {
                        height: string;
                    };
                    sm: {
                        height: string;
                    };
                    lg: {
                        height: string;
                    };
                };
                bg: string;
                borderRadius: string;
                progress: {
                    bg: string;
                };
                thumb: {
                    sizes: {
                        defaultSize: string;
                        smSize: string;
                        lgSize: string;
                    };
                    bg: string;
                    borderColor: string;
                };
            };
        };
        listGroup: {
            fontSize: string;
            bg: string;
            border: {
                color: string;
                width: string;
            };
            borderRadius: string;
            item: {
                paddingY: string;
                paddingX: string;
            };
            hover: {
                bg: string;
            };
            active: {
                color: string;
                bg: string;
                borderColor: string;
            };
            disabled: {
                color: string;
                bg: string;
            };
            action: {
                color: string;
                hover: {
                    color: string;
                };
                active: {
                    color: string;
                    bg: string;
                };
            };
        };
        inputGroup: {
            addon: {
                color: string;
                bg: string;
                borderColor: string;
            };
        };
        modal: {
            innerPadding: string;
            transition: string;
            dialog: {
                margin: string;
                marginYSmUp: string;
            };
            title: {
                lineHeight: number;
            };
            content: {
                bg: string;
                border: {
                    color: string;
                    width: string;
                };
                borderRadius: string;
                boxShadow: string;
                boxShadowSmUp: string;
            };
            backdrop: {
                bg: string;
                opacity: number;
            };
            header: {
                border: {
                    color: string;
                    width: string;
                };
                paddingY: string;
                paddingX: string;
            };
            footer: {
                border: {
                    color: string;
                    width: string;
                };
            };
            sizes: {
                lg: string;
                md: string;
                sm: string;
            };
        };
        nav: {
            variants: {
                default: {
                    size: {
                        fontSize: string;
                        lineHeight: number;
                        paddingX: string;
                        paddingY: string;
                        borderRadius: string;
                    };
                    gutter: string;
                    icon: {
                        spacing: string;
                    };
                    item: {
                        default: {
                            color: string;
                            bg: string;
                            border: {
                                width: number;
                            };
                        };
                        hover: {
                            color: string;
                        };
                        active: {
                            color: string;
                        };
                        disabled: {
                            color: string;
                            shadow: string;
                        };
                        focus: {
                            shadow: string;
                        };
                    };
                };
                underline: {
                    size: {
                        fontSize: string;
                        lineHeight: number;
                        paddingX: string;
                        paddingY: string;
                        borderRadius: string;
                    };
                    gutter: string;
                    icon: {
                        spacing: string;
                    };
                    border: {
                        color: string;
                        width: string;
                    };
                    item: {
                        default: {
                            color: string;
                            bg: string;
                            border: {
                                width: number;
                            };
                        };
                        hover: {
                            color: string;
                        };
                        active: {
                            color: string;
                            border: {
                                color: string;
                                width: string;
                            };
                        };
                        disabled: {
                            color: string;
                            shadow: string;
                        };
                        focus: {
                            shadow: string;
                        };
                    };
                };
                pills: {
                    size: {
                        fontSize: string;
                        lineHeight: number;
                        paddingX: string;
                        paddingY: string;
                        borderRadius: string;
                    };
                    gutter: string;
                    icon: {
                        spacing: string;
                    };
                    item: {
                        default: {
                            color: string;
                            border: {
                                width: number;
                            };
                        };
                        hover: {
                            color: string;
                        };
                        active: {
                            color: string;
                            bg: string;
                        };
                        disabled: {
                            color: string;
                            shadow: string;
                        };
                        focus: {
                            shadow: string;
                        };
                    };
                };
                tabs: {
                    border: {
                        width: string;
                        color: string;
                    };
                    size: {
                        fontSize: string;
                        lineHeight: number;
                        paddingX: string;
                        paddingY: string;
                        borderRadius: string;
                    };
                    gutter: string;
                    icon: {
                        spacing: string;
                    };
                    item: {
                        default: {
                            color: string;
                            bg: string;
                            border: {
                                width: string;
                                color: string;
                            };
                        };
                        hover: {
                            bg: string;
                            border: {
                                width: string;
                            };
                        };
                        active: {
                            color: string;
                            bg: string;
                            border: {
                                width: string;
                                color: string;
                            };
                        };
                        disabled: {
                            color: string;
                            bg: string;
                        };
                        focus: {
                            shadow: string;
                        };
                    };
                };
            };
        };
        navbar: {
            fontSize: string;
            paddingY: string;
            paddingX: string;
            nav: {
                link: {
                    paddingX: string;
                    height: string;
                };
            };
            brand: {
                fontSize: string;
                height: string;
                paddingY: string;
            };
            toggler: {
                paddingY: string;
                paddingX: string;
                fontSize: string;
                borderRadius: string;
            };
            themes: {
                dark: {
                    color: string;
                    hoverColor: string;
                    activeColor: string;
                    disabledColor: string;
                    toggler: {
                        borderColor: string;
                    };
                };
                light: {
                    color: string;
                    hoverColor: string;
                    activeColor: string;
                    disabledColor: string;
                    toggler: {
                        borderColor: string;
                    };
                };
            };
        };
        pagination: {
            color: string;
            bg: string;
            border: {
                color: string;
                width: string;
            };
            lineHeight: number;
            gutter: string;
            focus: {
                boxShadow: string;
                outline: string;
            };
            hover: {
                color: string;
                bg: string;
                borderColor: string;
            };
            active: {
                color: string;
                bg: string;
                borderColor: string;
            };
            disabled: {
                color: string;
                bg: string;
                borderColor: string;
            };
            sizes: {
                default: {
                    paddingY: string;
                    paddingX: string;
                };
                sm: {
                    paddingY: string;
                    paddingX: string;
                };
                lg: {
                    paddingY: string;
                    paddingX: string;
                };
            };
            item: {
                minWidth: string;
                borderRadius: string;
            };
        };
        popper: {
            fontSize: string;
            bg: string;
            border: {
                color: string;
                width: string;
            };
            borderRadius: string;
            boxShadow: string;
            header: {
                bg: string;
                color: string;
                paddingY: string;
                paddingX: string;
            };
            body: {
                color: string;
                paddingY: string;
                paddingX: string;
            };
            arrow: {
                size: string;
                color: string;
                outerColor: string;
            };
        };
        paper: {
            bg: string;
            color: string;
            paddingX: number;
            paddingY: number;
            border: {
                width: number;
                color: string;
            };
            boxShadow: string;
        };
        progress: {
            height: string;
            fontSize: string;
            bg: string;
            borderRadius: string;
            boxShadow: string;
            bar: {
                color: string;
                bg: string;
                animationTiming: string;
                transition: string;
            };
        };
        tooltip: {
            fontSize: string;
            maxWidth: string;
            color: string;
            bg: string;
            borderRadius: string;
            boxShadow: string;
            opacity: number;
            paddingY: string;
            paddingX: string;
            margin: string;
            arrow: {
                size: string;
                color: string;
            };
        };
    };
};
export default _default;
