export default {
  // message-actions/filter-data-record-action-setting
  chooseDsForFilter: 'Select data to filter',
  filterNone: 'None',
  filterDataRelationShip: 'Data relationship',
  filterMessageField: 'Message field',

  // message-actions/select-data-record-action-setting
  chooseDsForSelect: 'Set data to select',
  selectNone: 'None',
  selectDataRelationShip: 'Data relationship',
  selectMessageField: 'Message field'
}
