import { BaseWidget, React, AllWidgetProps } from 'jimu-core';
import { IMConfig } from '../config';
import { Page1, SettingComponentsPage } from './pages';

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig>> {
  render() {
    return (
      <div className="p-3 shadow h-100 overflow-auto">
        <main className="p-4">
          <SettingComponentsPage />
          <hr/>
          <Page1 theme={this.props.theme}/>
        </main>
      </div>
    )
  }
}
