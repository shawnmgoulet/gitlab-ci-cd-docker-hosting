import { React, ThemeVariables } from 'jimu-core';
import {
  ImageProps, Card, CardProps,
  List, Link, Tabs, Tab, Drawer, Button, Nav, NavLink, NavItem
} from 'jimu-ui';
import { ColorPicker, ThemeColorPicker } from 'jimu-ui/color-picker';
import { ColorResult } from 'react-color';
import { Navbar } from 'reactstrap';

interface State {
  color?: string;
  openDrawer?: boolean;
}
export class Page1 extends React.PureComponent<{ theme?: ThemeVariables }, State> {
  state: State = {};

  onColorChange = (result: ColorResult) => {
    console.log('Color result:', result.hex);
  }

  render() {
    const CARDIMAGE1: ImageProps = {
      src: 'http://via.placeholder.com/320x180',
      alt: 'Card image cap'
    };

    const CARDIMAGE2: ImageProps = {
      src: 'http://via.placeholder.com/120x120',
      alt: 'Card image cap',
      shape: 'circle',
      height: 120
    };

    const CARDIMAGE3: ImageProps = {
      src: 'http://via.placeholder.com/64x64',
      alt: 'Card image cap',
      shape: 'circle',
      height: 64
    };

    const LISTDATA1 = ['aaa', 'bbb', 'ccc'];

    const LISTDATA2: CardProps[] = Array.apply(null, { length: 5 }).map((n, index) => {
      return {
        title: `Card title ${++index}`,
        description: `Some quick example text to build on the card title and make up 
        the bulk of the card's content.`,
        image: CARDIMAGE2,
        horizontal: true
      }
    });

    const LISTDATA3: CardProps[] = Array.apply(null, { length: 5 }).map((n, index) => {
      return {
        title: `Card title ${++index}`,
        description: `Some quick example text to build on the card title and make up the 
        bulk of the card's content.`,
        image: CARDIMAGE2
      }
    });
    
    return (
      <div>
        <h2>Link</h2>
        <Link to="?a=1" className="mb-2">Link</Link>
        <br />
        <Link to="?a=2" themeStyle={{ type: undefined }} className="mb-2">Link as Default Button</Link>
        <br />
        <Link to="?a=3" themeStyle={{ type: 'primary' }} >Link as Primary Button</Link>
        <hr />

        {/* CARD: START */}
        <h2>Cards:</h2>
        <div className="row mb-3">
          <div className="col-sm-4 col-xs-6">
            <Card
              title="Card title"
              description="Some quick example text to build on the card title and make up the bulk of the card's content."
              image={CARDIMAGE1} />
          </div>
          <div className="col-sm-4 col-xs-6">
            <Card
              title="Card title"
              description="Some quick example text to build on the card title and make up the bulk of the card's content."
              image={CARDIMAGE2} />
          </div>
          <div className="col-sm-4 col-xs-6">
            <Card
              title="Card title"
              image={CARDIMAGE2} />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-lg-6 col-sm-12">
            <Card
              title="Card title"
              description="Some quick example text to build on the card title and make up the bulk of the card's content."
              image={CARDIMAGE2}
              className="mb-3"
              horizontal />
          </div>
          <div className="col-lg-6 col-sm-12">
            <Card
              title="Card title"
              image={CARDIMAGE3}
              horizontal
              contentJustify="center" />
          </div>
        </div>
        {/* CARD: END */}
        <hr className="mb-5" />
        {/* List: START */}
        <h2 className="mb-3">List:</h2>
        <h6>List of texts:</h6>
        <List
          dataSource={LISTDATA1}
          className="mb-5" />
        <h6>List of cards:</h6>
        <List
          className="mb-5"
          dataSource={LISTDATA2}
        />
        <h6>List of cards (horizontal):</h6>
        <List horizontal
          cardWidth="240"
          dataSource={LISTDATA3} />
        {/* List: END */}

        <hr />

        {/* TABS: START */}
        <h2>Tabs:</h2>
        <Tabs>
          <Tab title="Data" active={true}>
            <p>Test1</p>
          </Tab>
          <Tab title="Widgets">
            <p>Test2</p>
          </Tab>
        </Tabs>
        {/* TABS: END */}

        {/* Color picker: START */}
        <h2>Color picker:</h2>
        <ColorPicker color={this.state.color} onChange={color => this.setState({ color })} ></ColorPicker>
        <h2>Theme color picker:</h2>
        <ThemeColorPicker palette={this.props.theme.colors.palette} value={this.state.color} onChange={value => this.setState({ color: value })} ></ThemeColorPicker>
        {/* TABS: END */}
        {/* Drawer: START */}
        <h2>Drawer:</h2>
        <Button onClick={() => this.setState({ openDrawer: true })}>Open Drawer</Button>
        <Drawer PaperProps={{ className: 'h-100' }} anchor="left" open={this.state.openDrawer} onRequestClose={() => this.setState({ openDrawer: false })}>
          <Navbar color="light" style={{ width: '200px', height: '100%' }}>
            <Nav vertical navbar tabs>
              <NavItem>
                <NavLink active>Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="w-100" active>Home</NavLink>
                <NavLink className="w-100">About</NavLink>
                <NavLink className="w-100">Page</NavLink>
              </NavItem>
              <NavItem>
                <NavLink>Page</NavLink>
              </NavItem>
            </Nav>
          </Navbar>
        </Drawer>
        {/* Drawer: START */}
      </div>
    )
  }
}