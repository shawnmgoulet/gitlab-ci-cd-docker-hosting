/** @jsx jsx */
import { 
  React, jsx, css, 
  themeUtils, IMThemeVariables
} from 'jimu-core';
import { Button } from 'jimu-ui';
import { IconPicker } from 'jimu-ui/resource-selector';

interface Props {
  theme: IMThemeVariables;
}
interface State {
  iconPickerNoRemoveIcon: any
}

const sampleIcon1 = require('jimu-ui/lib/icons/widget.svg');
const sampleIcon2 = require('jimu-ui/lib/icons/home.svg');
const sampleIcon3 = require('jimu-ui/lib/icons/heart.svg');
const customIconString = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 300" width="300" height="300">` +
`<g><path d="M168.40695 197.13296c2.60284-.18587 5.39177-1.95219 8.3668-5.29897l7.25122-10.31905c1.30133-1.67328 2.78876-1.67328 4.4623 0 13.20075 11.71358 19.70825 18.9648 19.52252 21.75367-.37206 1.48748-1.76652 2.32416-4.1834 2.51004H58.801913c-7.809054.18598-14.595449.83673-20.359206 1.95225-1.673388.55784-2.603032.00005-2.788932-1.67335-.929671-3.34667-2.510065-5.94967-4.741185-7.80902-2.045238-1.85922-2.695989-3.06776-1.952253-3.62561.743694-.74365 1.766301-.74365 3.067826 0 7.065264 1.67342 14.874266 2.5101 23.427031 2.51004H168.40695M66.610924 29.79702H201.59525c1.3013-.1857 2.32391-.65052 3.06782-1.39446l3.90451-5.85676c1.11537-1.67313 2.50983-1.85905 4.1834-.55779 10.78365 8.1811 15.52483 13.20117 14.22355 15.06024-.558.92986-2.04543 1.58061-4.46229 1.95225-3.71879.37207-5.764 1.95247-6.13565 4.74118l-16.4547 96.49706h39.88173c2.04498-.18581 3.43945-.65063 4.1834-1.39446l6.41454-8.92459c1.11533-1.85916 2.5098-1.85916 4.1834 0 6.87911 5.94985 12.36401 11.24881 16.4547 15.89692.74345 1.30161.46456 2.23125-.83668 2.78893-3.90476 1.30161-6.69369 2.60311-8.36679 3.9045-.55804.74382-.9299 2.04532-1.11558 3.90451-5.94997 50.57269-9.85447 80.69313-11.71351 90.36141-4.2766 15.43206-14.03786 26.03006-29.28379 31.79386-1.67357.9296-2.60321.8366-2.78893-.2789-.92986-4.4623-2.78914-7.8091-5.57787-10.0402-3.34692-2.2311-10.96999-4.1834-22.86924-5.8567-2.78911-.3719-4.09061-1.4875-3.90451-3.3468.37168-1.8593 1.67318-2.51 3.90451-1.9522 24.17054 1.8593 37.74333 1.1156 40.71841-2.2312 1.11535-1.3015 3.34649-5.02 6.69344-11.15569.37162-2.41706 4.46205-34.02493 12.2713-94.8237H89.759062c-1.301583 1.30161-2.231226 2.51015-2.788933 3.62561-.371936 1.11568-1.115651 1.11568-2.231145 0-2.603078-3.34661-6.50758-6.41443-11.713516-9.20347-1.487494-.7436-1.580459-1.67325-.278893-2.78893 4.090365-2.60289 6.693366-4.74107 7.80901-6.41455 8.924502-36.25596 13.107896-61.07743 12.550196-74.46449-.000086-1.85909.650664-2.69577 1.952252-2.51004 5.949627 2.41727 12.364167 5.57806 19.243637 9.48237 1.85918.7439 2.78882 1.67355 2.78893 2.78893-.00011 1.11576-1.11568 2.0454-3.34672 2.78893-4.09054 1.30169-7.25132 7.2514-9.48237 17.84917l-8.087904 36.53501c-1.487517 5.02021-2.603089 9.57546-3.346719 13.66577h92.871443l17.57028-101.79603H69.957642c-7.809065.18615-14.59546.8369-20.359206 1.95226-1.673399.558-2.603042.00021-2.788932-1.67336-.929682-3.3465-2.510076-5.9495-4.741185-7.80901-2.045249-1.85907-2.695999-3.0676-1.952253-3.62562.743683-.74348 1.76629-.74348 3.067826 0 7.065253 1.67359 14.874255 2.51027 23.427032 2.51004" style="-inkscape-font-specification:AR PL UKai CN" font-size="285.58666992" font-weight="400" font-family="Arial Black"/>` +
`</g></svg>`;

class _SettingComponentsPage extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      iconPickerNoRemoveIcon: {filename: 'map-pin.svg'}
    }
  }

  render() {
    return (
      <div css={css`
        .widget-setting--iconpicker {
          margin-top: .5rem;
          margin-bottom: 1.5rem;
        }
      `}>
        <h2 className="mb-3">Icon Picker:</h2>
        {/* Icon Picker: START */}
        <IconPicker configurableOption="all"/>
        <div>
          Custom label and button settings:<br></br>
          <IconPicker buttonOptions={{type: 'primary', size: 'default'}}
          > Custom Label and Button </IconPicker>
        </div>
        <div>
          Show a specific group:<br></br>
          <IconPicker 
            groups="sns" //or: groups={['general', 'arrows']}
            customIcons={[{svg: sampleIcon3, properties: {filename: 'heart.svg', color: '#E91E63', inlineSvg: true}}]}
          > SNS icons </IconPicker>
        </div>
        <div>
          Predefined icon:<br></br>
          <IconPicker icon={{svg: sampleIcon1, properties: {filename: 'widget.svg'}}} />
        </div>
        <div>
          Show icon name in the result:<br></br>
          <IconPicker showLabel icon={{svg: sampleIcon3, properties: {filename: 'heart.svg', color: '#E91E63'}}} />
        </div>
        <div>
          Predefined icon with custom settings:<br></br>
          <IconPicker icon={{ 
            svg: customIconString, 
            properties: {
              filename: 'test',
              size: 16,
              color: 'red'
            }
          }}/>
        </div>
        <div>
          Hide 'remove' icon:<br></br>
          <IconPicker icon={{svg: sampleIcon2, properties: {filename: 'home.svg'}}}/>
          <Button type="link" onClick={() => {this.setState({
            iconPickerNoRemoveIcon: null
          })}}
          disabled={!this.state.iconPickerNoRemoveIcon}>reset</Button>
        </div>
        {/* Icon Picker: END */}
      </div>
    )
  }
}

export const SettingComponentsPage = themeUtils.withTheme(_SettingComponentsPage);
