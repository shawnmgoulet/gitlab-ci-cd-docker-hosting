/** @jsx jsx */
import { React, css, jsx } from 'jimu-core';
import {
  Nav, NavItem, NavLink, NavMenu,
  Breadcrumb, BreadcrumbItem,
  Navbar,
  Collapse,
  NavbarBrand,
  Dropdown,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem ,
  InputGroup, Input, ButtonGroup, Button, InputGroupAddon,
  Icon, Switch
} from 'jimu-ui';

type navbarColors = '' | 'dark' | 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'danger' | 'white' | 'light';

interface pageStates {
  navbarColor: navbarColors;
  navLinksVertical: boolean;
  navPillsVertical: boolean;
  navTabsVertical: boolean;
  navTabsIsRight: boolean;
  navGap: number;
  navbarMenu1Open: boolean;
  navbarMenu2Open: boolean;
  navbarSubMenu1Open: boolean;
  navIsFill: boolean;
  navShowIcon: boolean;
  navIconPosition: 'start' | 'end' | 'above';
}

const sampleIcon1 = require('jimu-ui/lib/icons/home.svg');
const sampleIcon2 = require('jimu-ui/lib/icons/map-pin.svg');
const sampleIcon3 = require('jimu-ui/lib/icons/toc-link.svg');
const sampleIcon4 = require('jimu-ui/lib/icons/app-access.svg');

export class Page4 extends React.PureComponent<{}, pageStates> {

  constructor(props) {
    super(props);

    this.state = {
      navbarColor: '',
      navLinksVertical: false,
      navPillsVertical: false,
      navTabsVertical: false,
      navTabsIsRight: false,
      navGap: 5,
      navbarMenu1Open: false,
      navbarMenu2Open: false,
      navbarSubMenu1Open: false,
      navIsFill: false,
      navShowIcon: false,
      navIconPosition: undefined
    };
  }

  render() {

    function NavComponentSample(props) {
      const {
        showIcon,
        iconPosition,
        ...otherProps
      } = props;
      return <Nav {...otherProps} submenuMode="foldable" textAlign="left">
      <NavItem>
        <NavLink caret icon={showIcon ? sampleIcon1 : undefined} iconPosition={iconPosition} active>Home</NavLink>
        <NavMenu>
          <NavItem>
            <NavLink>Option 1</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Option 2</NavLink>
          </NavItem>
        </NavMenu>
      </NavItem>
      <NavItem>
        <NavLink icon={showIcon ? sampleIcon2 : undefined} iconPosition={iconPosition} to={{linkType: 'VIEW', value: 'default,jimu-ui'}}>Jimu UI</NavLink>
      </NavItem>
      <NavItem>
        <NavLink icon={showIcon ? sampleIcon3 : undefined} iconPosition={iconPosition} href="http://www.esri.com">Esri.com</NavLink>
      </NavItem>
      <NavItem>
        <NavLink icon={showIcon ? sampleIcon4 : undefined} iconPosition={iconPosition} disabled href="#">Disabled Link</NavLink>
      </NavItem>
    </Nav>
    }

    return (
      <div css={css`
          .jimu-nav.vertical {
            min-height: 240px;
          }

      `}>
        <h2>Navs 
          <span className="float-right d-flex align-items-center">
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Show Icons:</small>
            <Switch checked={this.state.navShowIcon} onChange={e => {
              this.setState({ navShowIcon: e.target.checked })
            }}></Switch>
            {this.state.navShowIcon && <span className="mx-3">
              <Input type="select" onChange={evt => {
                this.setState({ navIconPosition: evt.target.value })
              }}>
                <option value="start">start</option>
                <option value="end">end</option>
                <option value="above">above</option>
              </Input>
            </span>}
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Item Full Width:</small>
            <Switch checked={this.state.navIsFill} onChange={e => {
              this.setState({ navIsFill: e.target.checked })
            }}></Switch> 
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Gap:</small>
            <Input type="range" style={{width: 100}} min={0} max={30} value={this.state.navGap} onChange={e => {
              this.setState({
                navGap: e.target.value
              })
            }}></Input> <small className="h5 ml-2 text-muted">{this.state.navGap}px</small>
          </span>
        </h2>
        {/* Navs: START */}
        <h4 className="mt-5 mb-3">Default:
          <span className="float-right d-flex align-items-center">
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Vertical:</small>
            <Switch checked={this.state.navLinksVertical} onChange={evt => {
              this.setState({ navLinksVertical: evt.target.checked })
            }}/>
          </span>
        </h4>

        <NavComponentSample 
          fill={this.state.navIsFill}
          gap={`${this.state.navGap}px`} 
          className="mb-3" 
          vertical={this.state.navLinksVertical}
          showIcon={this.state.navShowIcon}
          iconPosition={this.state.navIconPosition}
          submenuMode="static"
        />

        <h4 className="mt-5 mb-3">Underline:
          <span className="float-right d-flex align-items-center">
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Vertical:</small>
            <Switch checked={this.state.navTabsVertical} onChange={evt => {
              this.setState({ navTabsVertical: evt.target.checked })
            }} />
            <span style={{display: this.state.navTabsVertical ? '' : 'none'}}>
              <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Right:</small>
              <Switch checked={this.state.navTabsIsRight} onChange={evt => {
                this.setState({ navTabsIsRight: evt.target.checked })
              }}/>
            </span>
          </span>
        </h4>

        <NavComponentSample
          underline
          fill={this.state.navIsFill}
          gap={`${this.state.navGap}px`} 
          className="mb-3" 
          vertical={this.state.navTabsVertical}
          showIcon={this.state.navShowIcon}
          iconPosition={this.state.navIconPosition}
          right={this.state.navTabsIsRight}
        />

        <h4 className="mt-5 mb-3">Tabs:</h4>
        
        <NavComponentSample
          tabs
          fill={this.state.navIsFill}
          gap={`${this.state.navGap}px`} 
          className="mb-3" 
          vertical={this.state.navTabsVertical}
          showIcon={this.state.navShowIcon}
          iconPosition={this.state.navIconPosition}
          right={this.state.navTabsIsRight}
        />
     
        <h4 className="mt-5 mb-3">Pills:
          <span className="float-right d-flex align-items-center">
            <small className="h5 font-weight-normal text-muted m-0 mr-1 ml-3">Vertical:</small>
            <Switch checked={this.state.navPillsVertical} onChange={evt => {
              this.setState({ navPillsVertical: evt.target.checked })
            }}/>
          </span>
        </h4>

        <NavComponentSample 
          pills
          fill={this.state.navIsFill}
          gap={`${this.state.navGap}px`} 
          className="mb-3" 
          vertical={this.state.navPillsVertical}
          showIcon={this.state.navShowIcon}
          iconPosition={this.state.navIconPosition}
        />

        <h4 className="mt-5 mb-3">Breadcrumb:</h4>
        <Breadcrumb>
          <BreadcrumbItem><a href="#">Home</a></BreadcrumbItem>
          <BreadcrumbItem><a href="#">Library</a></BreadcrumbItem>
          <BreadcrumbItem active>Data</BreadcrumbItem>
        </Breadcrumb>
        {/* NAVS: END */}
        <hr/>
        {/* NAVBAR: START */}
        <h2>Navbar
          <span className="float-right d-flex align-items-center">
            <small className="h5 font-weight-normal text-muted m-0 mr-3 ml-5">color: {this.state.navbarColor === '' ? '(default)' : this.state.navbarColor}</small>
            <ButtonGroup 
              css={css`
                .btn {
                  width: 1.5rem;
                  height: 1rem;
                  &.active {
                    outline: 4px solid #333;
                  }
                }
              `} 
              size="sm" onClick={e => {
                let targetButton = e.target as HTMLButtonElement;
                this.setState({ navbarColor: targetButton.value as navbarColors })
              }}>
              <Button value="dark" className="bg-dark" active={this.state.navbarColor === 'dark'}></Button>
              <Button value="primary" className="bg-primary" active={this.state.navbarColor === 'primary'}></Button>
              <Button value="secondary" className="bg-secondary" active={this.state.navbarColor === 'secondary'}></Button>
              <Button value="success" className="bg-success" active={this.state.navbarColor === 'success'}></Button>
              <Button value="info" className="bg-info" active={this.state.navbarColor === 'info'}></Button>
              <Button value="warning" className="bg-warning" active={this.state.navbarColor === 'warning'}></Button>
              <Button value="danger" className="bg-danger" active={this.state.navbarColor === 'danger'}></Button>
              <Button value="light" className="bg-light" outline active={this.state.navbarColor === 'light'}></Button>
              <Button value="white" className="bg-white" active={this.state.navbarColor === 'white'}></Button>
            </ButtonGroup>
          </span>
        </h2>
        <div className="mt-5 mb-3">
          <Navbar color={this.state.navbarColor} borderBottom>
            <NavbarBrand>
            <img src="https://via.placeholder.com/64x32/CFDBE7/131314?text=LOGO" />
            </NavbarBrand>
            <Collapse navbar>
              <Nav navbar className="flex-grow-1">
                <NavItem>
                  <NavLink>Components</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink>GitHub</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Options
                  </DropdownToggle>
                  <DropdownMenu alightment="center">
                    <DropdownItem>
                      Option 1
                    </DropdownItem>
                    <DropdownItem>
                      Option 2
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Reset
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
              <InputGroup style={{width: 'auto'}} className="mr-4">
                <Input style={{maxWidth: 200}} placeholder="search"/>
                <InputGroupAddon addonType="append">
                  <Button icon><Icon icon={require('jimu-ui/lib/icons/search.svg')} /></Button>
                </InputGroupAddon>
              </InputGroup>
              <Nav navbar>
                <NavItem>
                  <NavLink><Icon size="24" icon={require('jimu-ui/lib/icons/app-access.svg')} />User</NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </div>

        <h2 className="mt-5 mb-3">Navbar (with Drawer)</h2>
        <div>
          <Navbar className="justify-content-between" color={this.state.navbarColor} borderBottom>
            <NavbarBrand>
            <img src="https://via.placeholder.com/64x32/CFDBE7/131314?text=LOGO" />
            </NavbarBrand>
            <Nav navbar>
              <NavItem>
                <NavLink>Components</NavLink>
              </NavItem>
              <NavItem>
                <NavLink>GitHub</NavLink>
              </NavItem>
              <Dropdown nav isOpen={this.state.navbarMenu1Open} toggle={() => {this.setState({navbarMenu1Open: !this.state.navbarMenu1Open})}} >
                <DropdownToggle nav caret>
                  Dropdown
                </DropdownToggle>
                <DropdownMenu appendTo="body">
                  <DropdownItem header>Header</DropdownItem>
                  <DropdownItem disabled>Action</DropdownItem>
                  <DropdownItem>Another Action</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
              </Dropdown>
              <NavItem dropdown menuOpenMode="hover" isOpen={this.state.navbarMenu2Open} toggle={e => {this.setState({navbarMenu2Open: !this.state.navbarMenu2Open})}} >
                <NavLink icon={sampleIcon1} caret>Custom Nav Link</NavLink>
                <NavMenu>
                  <NavItem>
                    <NavLink>Option 1</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink>Option 2</NavLink>
                  </NavItem>
                  <NavItem dropdown direction="right" menuOpenMode="hover" isOpen={this.state.navbarSubMenu1Open} toggle={e => {this.setState({navbarSubMenu1Open: !this.state.navbarSubMenu1Open})}} >
                    <NavLink icon={sampleIcon2} caret>Option 3</NavLink>
                    <NavMenu>
                      <NavItem>
                        <NavLink>Option 1</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink>Option 2</NavLink>
                      </NavItem>
                    </NavMenu>
                  </NavItem>
                </NavMenu>
              </NavItem>
            </Nav>
          </Navbar>
        </div>
        {/* NAVBAR: END */}
        </div>
    )
  }
}