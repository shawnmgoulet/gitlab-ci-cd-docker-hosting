/** @jsx jsx */
import { React, jsx, classNames, ThemeButtonType } from 'jimu-core';
import {
  Button, ButtonGroup, Dropdown,
  _DropdownToggle, DropdownToggle, DropdownMenu, DropdownItem,
  Pagination, 
  Icon
} from 'jimu-ui';

type buttonShadows = '' | 'default' | 'sm' | 'lg';
type buttonSize = 'default' | 'sm' | 'lg';
type buttonIconPosition = 'none' | 'left' | 'right';

interface pageStates {
  dropdownOpen: boolean;
  dropdownButtonOpenStates: {
    [index: string]: boolean;
  };
  splitDropdownOpen: boolean;
  fullDropdownOpen: boolean;
  IconInButtonPoistion: buttonIconPosition;
  buttonShadow: buttonShadows;
  buttonSize: buttonSize;
}

const THEMETYPES: ThemeButtonType[] = [
  'default',
  'primary',
  'secondary',
  'tertiary',
  'danger',
  'link'
];

const sampleIcon = require('jimu-ui/lib/icons/heart.svg');

export class Page2 extends React.PureComponent<{}, pageStates> {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      dropdownButtonOpenStates: {},
      splitDropdownOpen: false,
      fullDropdownOpen: false,
      IconInButtonPoistion: 'none',
      buttonShadow: '',
      buttonSize: 'default'
    };

    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.toggleDropdownButton = this.toggleDropdownButton.bind(this);
    this.splitToggleDropdown = this.splitToggleDropdown.bind(this);
  }

  toggleDropdown() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  toggleDropdownButton(componentIndex) {
    this.setState((prevState) => {
      let newStates = Object.assign({}, prevState.dropdownButtonOpenStates);
      newStates[componentIndex] = !(prevState.dropdownButtonOpenStates
      && prevState.dropdownButtonOpenStates[componentIndex]);
      return {
        dropdownButtonOpenStates: newStates
      }
    });
  }

  splitToggleDropdown() {
    this.setState(prevState => ({
      splitDropdownOpen: !prevState.splitDropdownOpen
    }));
  }
  

  fullToggleDropdown() {
    this.setState(prevState => ({
      fullDropdownOpen: !prevState.fullDropdownOpen
    }));
  }
  
  createAllButtons(props) {
    let buttonNodes = [];
    let size = this.state.buttonSize;
    let isIconButon = props && props.isIcon;
    let iconPosition = this.state.IconInButtonPoistion;
    let icon = <Icon className={iconPosition === 'right' && 'right-icon'} icon={sampleIcon} />;
    
    THEMETYPES && THEMETYPES.forEach((type, index) => {
      buttonNodes.push(<Button 
        key={index} 
        className={props && props.classNames}
        size={size}
        icon={isIconButon}
        type={type}>
        {iconPosition === 'left' && !isIconButon && icon}
        {isIconButon ? icon : type}
        {iconPosition === 'right' && !isIconButon && icon}
      </Button>);
    })

    return buttonNodes;
  }

  createAllButtonGroups(props) {
    let buttonGroupNodes = [];
    let size = this.state.buttonSize;
    let isIconButon = props && props.isIcon;
    let iconPosition = this.state.IconInButtonPoistion;
    let icon = <Icon className={iconPosition === 'right' && 'right-icon'} icon={sampleIcon} />;
    
    THEMETYPES && THEMETYPES.forEach((type, index) => {
      buttonGroupNodes.push(
        <ButtonGroup 
          key={index} 
          className={props && props.classNames}
          size={size}
        >
          {['Left', 'Middle', 'Right'].map((buttonText, buttonIndex) => <Button 
            key={buttonIndex}
            active={buttonIndex === 0}
            icon={isIconButon}
            type={type}>
            {iconPosition === 'left' && !isIconButon && icon}
            {isIconButon ? icon : buttonText}
            {iconPosition === 'right' && !isIconButon && icon}
          </Button>)}
        </ButtonGroup>
      );
    })

    return buttonGroupNodes;
  }

  createAllDropdownButtons(props) {
    let buttonGroupNodes = [];
    let size = this.state.buttonSize;
    let dMenu = <DropdownMenu appendTo="body">
      <DropdownItem active onClick={e => {console.log('Dropdown item clicked')}}>Another Action</DropdownItem>
      <DropdownItem>Another Action</DropdownItem>
      <DropdownItem>Another Action</DropdownItem>
      <DropdownItem divider />
      <DropdownItem>Another Action</DropdownItem>
      <DropdownItem>Another Action</DropdownItem>
    </DropdownMenu>;
    
    THEMETYPES && THEMETYPES.forEach((type, index) => {
      buttonGroupNodes.push(
        <Dropdown
        selectable
          key={index} 
          className={props && props.classNames}
          isOpen={this.state.dropdownButtonOpenStates && this.state.dropdownButtonOpenStates[index]} 
          toggle={() => this.toggleDropdownButton(index)}
        >
          <DropdownToggle size={size} type={type as ThemeButtonType} caret>
            Dropdown
          </DropdownToggle>
          {dMenu}
        </Dropdown>
      );
    })

    return buttonGroupNodes;
  }

  render() {
    let buttonClasses = classNames(
      this.state.buttonShadow ? 
      this.state.buttonShadow === 'default' ? 'shadow' : 'shadow-' + this.state.buttonShadow : '',
      'mb-2 mr-4'
    );

    return (
      <div>
        {
          /* BUTTONS: */
        }

        <h2 className="mb-3">Button</h2>

        {
          /* CONFIGURABLE OPTIONS */
        }

        <div className="d-flex align-items-center">
          <small className="h5 font-weight-normal text-muted m-0 mr-3">Icon:</small>
          <ButtonGroup size="sm" onClick={e => {
            let targetButton = e.target as HTMLButtonElement;
            this.setState({ IconInButtonPoistion: targetButton.value as buttonIconPosition })
          }}>
            <Button value="none" active={this.state.IconInButtonPoistion === 'none'}>none</Button>
            <Button value="left" active={this.state.IconInButtonPoistion === 'left'}>left</Button>
            <Button value="right" active={this.state.IconInButtonPoistion === 'right'}>right</Button>
          </ButtonGroup>
          <small className="h5 font-weight-normal text-muted m-0 mr-3 ml-5">Size:</small>
            <ButtonGroup size="sm" onClick={e => {
              let targetButton = e.target as HTMLButtonElement;
              this.setState({ buttonSize: targetButton.value as buttonSize })
            }}>
              <Button value="sm" outline active={this.state.buttonSize === 'sm'}>small</Button>
              <Button value="default" outline active={this.state.buttonSize === 'default'}>default</Button>
              <Button value="lg" outline active={this.state.buttonSize === 'lg'}>large</Button>
            </ButtonGroup>
          <small className="h5 font-weight-normal text-muted m-0 mr-3 ml-5">shadow(CSS class):</small>
          <ButtonGroup size="sm" onClick={e => {
            let targetButton = e.target as HTMLButtonElement;
            this.setState({ buttonShadow: targetButton.value as buttonShadows })
          }}>
            <Button value="" active={this.state.buttonShadow === ''}>none</Button>
            <Button value="sm" active={this.state.buttonShadow === 'sm'}>small</Button>
            <Button value="default" active={this.state.buttonShadow === 'default'}>default</Button>
            <Button value="lg" active={this.state.buttonShadow === 'lg'}>large</Button>
          </ButtonGroup>
        </div>
       
        <hr/>

        <h6>Button Types:</h6>
        <div className="mb-3">
          {this.createAllButtons({
            classNames: buttonClasses
          })}
        </div>
        <h6>Icon (Only):</h6>
        <div className="mb-3">
          {this.createAllButtons({
            classNames: buttonClasses,
            isIcon: true
          })}
        </div>

        <hr />
        
        {
          /* BUTTON GROUPS: */
        }

        <h2 className="mb-3">Button Group: </h2>
        <h6>By Type:</h6>
        <div className="mb-3">
          {this.createAllButtonGroups({
            classNames: buttonClasses
          })}
        </div>
        <h6>Icon Only:</h6>
        <div className="mb-3">
          {this.createAllButtonGroups({
            classNames: buttonClasses,
            isIcon: true
          })}
        </div>

        <hr />

        {
          /* DROPDOWNS: */
        }

        <h2 className="mb-3">Dropdown:</h2>
        <h6>By Types:</h6>
        <div className="mb-3">
          {this.createAllDropdownButtons({
            classNames: buttonClasses
          })}
        </div>
        <h6>Split Dropdown:</h6>
        <div className="mb-3">
          <Dropdown fluid group size={this.state.buttonSize}  isOpen={this.state.splitDropdownOpen} toggle={this.splitToggleDropdown} className={buttonClasses}>
            <Button id="caret">Split Dropdown</Button>
            <DropdownToggle caret/>
            <DropdownMenu zIndex="55">
              <DropdownItem header>Header</DropdownItem>
              <DropdownItem disabled>Action</DropdownItem>
              <DropdownItem>Another Action</DropdownItem>
              <DropdownItem divider/>
              <DropdownItem>Another Action</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
        <h6>Full Width Dropdown:</h6>
        <div className="mb-3">
          <Dropdown fluid size={this.state.buttonSize}  isOpen={this.state.fullDropdownOpen} toggle={this.fullToggleDropdown} className={buttonClasses}>
            <DropdownToggle caret>Full Width Dropdown</DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Action</DropdownItem>
              <DropdownItem>Another Action</DropdownItem>
              <DropdownItem>Another Action</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
        {/* <h2 className="mb-3">Dropdown:</h2>
        <div className="mb-3">
          <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
            <DropdownToggle caret  size={this.state.buttonSize} className={buttonClasses} >
              Dropdown
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem header>Header</DropdownItem>
              <DropdownItem>Some Action</DropdownItem>
              <DropdownItem disabled>Action (disabled)</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Foo Action</DropdownItem>
              <DropdownItem>Bar Action</DropdownItem>
              <DropdownItem>Quo Action</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div> */}

        <hr />

        {
          /* PAGINATION: */
        }

        <h2 className="mb-3">Pagination:</h2>
        <div>
          <Pagination totalPage={10} current={3} >
          </Pagination>
        </div>
        {/* BUTTONS: END */}
      </div>
    )
  }
}