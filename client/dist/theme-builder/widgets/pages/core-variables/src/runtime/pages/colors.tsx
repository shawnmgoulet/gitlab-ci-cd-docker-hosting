/** @jsx jsx */
import { 
  React, polished, jsx, css, 
  themeUtils, ThemeColorPalette, 
  IMThemeVariables, ThemeAllColors,
  ThemeCommonColorKeys, ThemeThemeColorKeys, 
} from 'jimu-core';
import { Table } from 'jimu-ui';

interface Props {
  theme: IMThemeVariables;
}

function ColorBlock(props) {
  const isTransparent = props && props.color === 'transparent';
  const isActive = !!props.active;
  return <React.Fragment>
    <div className={isActive ? 'shadow-lg' : 'shadow-sm'} css={css`
      padding: .5rem;
      min-width: 60px;
      /* border: 1px solid ${polished.rgba('black', .25)}; */
      background: ${isTransparent ? 
        'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAH0lEQVQoU2NkYGDwZUAFm5G5jPRRgOYEVDeB3EBjBQBuSwU9/8GksAAAAABJRU5ErkJggg==) repeat' : 
        props.color
      };
      color: ${!isTransparent && polished.readableColor(props.color)};
      ${isActive && css`
        transform: scale(1.075);
      `}
    `}
    ><span className={isTransparent ? 'bg-white px-2' : undefined}>{props.color}</span></div>
  </React.Fragment>;
}

class _ColorsPage extends React.PureComponent<Props, {}> {
  constructor(props) {
    super(props);
  }

  getColorTableContent(colors: ThemeAllColors) {
    let palette: ThemeColorPalette = colors && colors.palette || {};
    // let THEMECOLOR_to_SHADE_MAPPING: ThemeColorToShadeMapping = colors && colors._themeColorToShadeMapping || {};
    let rows = [];
    for (let i = 1; i <= 9; i++) {
      let shadeIndex = i * 100;
      rows.push(
        <tr key={shadeIndex}>
          <th>{shadeIndex}</th>
          {
            Object.keys(palette).map(colorName =>
              <td key={`${colorName}${shadeIndex}`}>
                <ColorBlock 
                  color={palette[colorName][shadeIndex]}
                  active={colors[colorName] === palette[colorName][shadeIndex]}
                />
              </td>
            )
          }
        </tr>
      )
    }
    return rows;
  }

  render() {
    const theme = this.props.theme;
    const {
      colors
    } = theme;

    return (
      <div css={css`
        table {
          table-layout: fixed;
        }
        th, td {
          padding: 0 .5rem;
          border: 0;
          text-align: center;
        }
        thead th {
          padding-bottom: 1rem;
        }
      `}>
        <h2 className="mb-3">Colors:</h2>
        {/* COLORS: START */}
        <h6 className="mt-5 mb-3">Common Colors:</h6>
        <Table className="w-100" >
          <thead>
            <tr>
              <th></th>
              {Object.keys(ThemeCommonColorKeys).map(colorKey => {
                return <th key={colorKey}>{ThemeCommonColorKeys[colorKey]}</th>
              })}
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              {colors && Object.keys(ThemeCommonColorKeys).map(colorKey => {
                return <td key={colorKey}>
                  <ColorBlock color={colors[ThemeCommonColorKeys[colorKey]]} />
                </td>
              })}
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </Table>

        <h6 className="mt-5 mb-3">Theme Colors:</h6>
        <Table className="w-100" >
          <thead>
            <tr>
              <th></th>
              {Object.keys(ThemeThemeColorKeys).map(colorKey => <th key={colorKey}>{ThemeThemeColorKeys[colorKey]}</th>)}
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              {Object.keys(colors.palette).map(colorName => <td key={colorName}>
                <ColorBlock color={colors[colorName]} />
              </td>)}
            </tr>
          </tbody>
        </Table>

        <h6 className="mt-5 mb-3">Theme Palette:</h6>

        <Table className="w-100" >
          <thead>
            <tr>
              <th></th>
              {Object.keys(ThemeThemeColorKeys).map(colorKey => <th key={colorKey}>{ThemeThemeColorKeys[colorKey]}</th>)}
            </tr>
          </thead>
          <tbody>
            {this.getColorTableContent(colors)}
          </tbody>
        </Table>
        {/* COLORS: END */}
      </div>
    )
  }
}

export const ColorsPage = themeUtils.withTheme(_ColorsPage);
