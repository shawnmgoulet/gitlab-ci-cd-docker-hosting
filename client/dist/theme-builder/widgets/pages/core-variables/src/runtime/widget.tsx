import { BaseWidget, React, AllWidgetProps } from 'jimu-core';
import { IMConfig } from '../config';
import { ColorsPage, TypographyPage, SizesPage, RadiusesPage, ShadowsPage } from './pages';

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig>> {
  render() {
    return (
      <div className="shadow p-3 h-100 overflow-auto">
        <main className="p-4">
          <ColorsPage />
          <hr />
          <SizesPage />
          <hr />
          <RadiusesPage />
          <hr />
          <ShadowsPage />
          <hr />
          <TypographyPage />
        </main>
      </div>
    )
  }
}
