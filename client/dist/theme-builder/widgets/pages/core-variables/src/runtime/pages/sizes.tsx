/** @jsx jsx */
import { 
  React, jsx, css, 
  themeUtils, IMThemeVariables
} from 'jimu-core';

interface Props {
  theme: IMThemeVariables;
}

function SizeBlock(props) {
  const {size} = props;
  return size && <React.Fragment>
    <div css={css`
        padding: 5px;
        position: relative;
      `}
      className={props.className}
    >
      <b css={css`
        display:inline-block;
        width: 2rem;
      `}>
      {size[0]}: 
      </b>
      <span css={css`
        display:inline-block;
        width: ${size[1]};
        height: 0;
        border-bottom: 2px solid red;
      `}
      className="ml-4"></span>
      <small css={css`
        position: absolute; 
        left: 200px;
        `}
        className="text-muted" 
      >{size[1]}</small>
    </div>
  </React.Fragment>;
}

class _SizesPage extends React.PureComponent<Props, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const theme = this.props.theme;
    const {
      sizes,
      gutters
    } = theme;

    return (
      <div>
        <h2 className="mb-3">Sizes <small>(Padding, Margin)</small>:</h2>
        {/* Sizing: START */}
        {sizes && Object.keys(sizes).map(sizeKey => {
          return <SizeBlock key={sizeKey} className="mr-3" size={[sizeKey, sizes[sizeKey]]} />
        })}

        <hr/>
        
        <h2 className="mb-3">Gutters <small>(Relatively small sizes)</small>:</h2>
        {gutters && Object.keys(gutters).map(gutterKey => {
          return <SizeBlock key={gutterKey} className="mr-3" size={[gutterKey, gutters[gutterKey]]} />
        })}
        {/* Sizing: END */}
      </div>
    )
  }
}

export const SizesPage = themeUtils.withTheme(_SizesPage);
