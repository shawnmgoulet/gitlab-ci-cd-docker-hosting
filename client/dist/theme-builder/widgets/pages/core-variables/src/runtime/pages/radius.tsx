/** @jsx jsx */
import { 
  React, jsx, css, 
  themeUtils, IMThemeVariables,
} from 'jimu-core';

interface Props {
  theme: IMThemeVariables;
}

function RadiusBlock(props) {
  const theme = props.theme;
  return <React.Fragment>
    <div css={css`
      padding: 5px;
      width: ${props.type === 'pill' ? '160px' : '80px'};
      height: 80px;
      text-align: center;
      flex-direction: column;
      padding-top: 24px;
      background: ${theme.colors.palette.primary[100]};
      border-radius: ${props.radius};
      small {
        display: block;
        opacity: .65;
      }
      `}
      className={props.className}
    >{props.children}</div>
  </React.Fragment>;
}

class _RadiusesPage extends React.PureComponent<Props, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const theme = this.props.theme;
    const {
      borderRadiuses
    } = theme;

    return (
      <div>
        <h2 className="mb-3">Radius:</h2>
        {/* RADIUS: START */}
        {borderRadiuses && 
          <div className="d-flex">
            {borderRadiuses && Object.keys(borderRadiuses).map(key => 
            <RadiusBlock key={key} className="mr-3" radius={borderRadiuses[key]} theme={theme} type={key}>
              {key}<small>{borderRadiuses[key]}</small>
            </RadiusBlock>
            )}
          </div>
        }
        {/* RADIUS: END */}
      </div>
    )
  }
}

export const RadiusesPage = themeUtils.withTheme(_RadiusesPage);
