/** @jsx jsx */
import { 
  React, jsx, css, 
  themeUtils, IMThemeVariables,
} from 'jimu-core';

interface Props {
  theme: IMThemeVariables;
}

function ShadowBlock(props) {
  const theme = props.theme;
  return <React.Fragment>
    <div css={css`
      padding: 5px;
      width: 80px;
      height: 80px;
      line-height:80px;
      text-align: center;
      background: ${theme && (theme.darkTheme ? 
        theme.colors.palette.light[800] : theme.colors.white) || 'white'};
      box-shadow: ${props.shadow};
      `}
      className={props.className}
    >{props.children}</div>
  </React.Fragment>;
}

class _ShadowsPage extends React.PureComponent<Props, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const theme = this.props.theme;
    const {
      boxShadows
    } = theme;

    return (
      <div>
        <h2 className="mb-3">Shadows:</h2>
        {/* SHADOWS: START */}
        {boxShadows && 
          <div className="d-flex">
            {boxShadows && Object.keys(boxShadows).map(key => 
            <ShadowBlock key={key} className="mr-3" shadow={boxShadows[key]} theme={theme}>
              {key}
            </ShadowBlock>
            )}
          </div>
        }
        {/* SHADOWS: END */}
      </div>
    )
  }
}

export const ShadowsPage = themeUtils.withTheme(_ShadowsPage);
