/** @jsx jsx */
import {
  React, BaseWidget, IMState, AllWidgetProps, IMAppConfig, ThemeManifest,
  Immutable, CustomThemeJson, IMCustomThemeJson, ThemeManager,
  css, jsx, IMThemeVariables, ThemeInfo, IMThemeInfo, SerializedStyles,
  ImmutableArray, ImmutableObject, UrlParameters, urlUtils
} from 'jimu-core';
import { getAppConfigAction } from 'jimu-for-builder';
import { Button, Icon } from 'jimu-ui';
import { ThemeSelector, ThemeQuickStyler, ThemeColorConfigurator, FontsetConfigurator, ThemeComponentsConfigurator } from 'jimu-ui/theme-components';
import { SettingSection } from 'jimu-ui/setting-components';
import defaultMessages from './translations/default';
import { Stepper, StepperPane } from './components/pane-wrapper';

interface ExtraProps {
  appConfig: IMAppConfig;
  sharedTheme: object;
  queryObject?: ImmutableObject<UrlParameters>;
}

interface WState {
  themeInfo: IMThemeInfo;
  selectedTheme: string;
  themeInfos: ImmutableArray<ThemeInfo>
  currentStep: number;
  loaded: boolean;
  cutomizationStep: 'overall' | 'palette' | 'fontset' | 'app-elements'
}

export default class Widget extends BaseWidget<AllWidgetProps<{}> & ExtraProps, WState>{
  loadingStr: string;
  titleStr: string;

  _customTheme: {
    name: string;
    themeInfo: IMThemeInfo;
  } = {
    name: '',
    themeInfo: Immutable({})
  };

  private themeManager: ThemeManager;

  constructor(props) {
    super(props);
    this.state = {
      themeInfo: null,
      selectedTheme: null,
      themeInfos: Immutable([]),
      currentStep: 1,
      loaded: false,
      cutomizationStep: 'overall'
    }

    this.loadingStr = this.props.intl.formatMessage({ id: 'loading', defaultMessage: defaultMessages.loading });
    this.titleStr = this.props.intl.formatMessage({ id: 'chooseTemplate', defaultMessage: defaultMessages.chooseTemplate });

    this.themeManager = ThemeManager.getInstance();

    if (this.props.appConfig && this.props.appConfig.customTheme) {
      this.themeManager.updateThemeCustomVariables(
        this.props.appConfig.theme,
        this.props.appConfig.customTheme
      )
    }
  }

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      appConfig: state.appStateInBuilder && state.appStateInBuilder.appConfig || null,
      sharedTheme: state.portalSelf && state.portalSelf.portalProperties && state.portalSelf.portalProperties.sharedTheme,
      queryObject: state.queryObject
    }
  }

  private loadThemeListInfo = async (): Promise<Immutable.ImmutableArray<ThemeInfo>> => {
    const result = await fetch(`${urlUtils.getAbsoluteRootUrl()}themes/themes-info.json`);
    return Promise.resolve(Immutable(await result.json()));
  }

  private getThemeInfo = (themeUri: string): ThemeInfo => {
    return this.state.themeInfos.find(themeInfo => themeInfo.uri === themeUri);
  }

  handleOnChange = (themeUri: string) => {
    console.log(`SELECTED THEME IS: ${themeUri}`);

    this.themeManager.loadThemeManifest(themeUri).then((result: ThemeManifest) => {
      getAppConfigAction(this.props.appConfig).editTheme(themeUri, Immutable(result)).exec();
    });

    const themeInfo = this.getThemeInfo(themeUri)

    if (themeInfo) {
      this.setState({
        selectedTheme: themeUri,
        themeInfo: Immutable(themeInfo)
      })
    }
  }

  handleOnUpdate = (customVariables: CustomThemeJson) => {
    if (!customVariables) return;

    // reset(remove) some custom variables if coloration has changed
    if(customVariables.hasOwnProperty('coloration')) {
      this.themeManager.removeThemeCustomVariables(this._customTheme.name, [
        'header', 'footer', 'body.bg', 'body.color'
      ]);
    }
    const updatedCustomVariables: IMCustomThemeJson = this.themeManager.addThemeCustomVariables(this._customTheme.name, customVariables);
    getAppConfigAction(this.props.appConfig).editCustomTheme(updatedCustomVariables).exec();
  }

  handleOnCustomize = () => {
    this.setState({
      currentStep: 2,
      cutomizationStep: 'overall'
    })
  }

  handleOnPaletteCustomize = () => {
    this.setState({
      currentStep: 3,
      cutomizationStep: 'palette'
    })
  }

  handleOnFontsetCustomize = () => {
    this.setState({
      currentStep: 3,
      cutomizationStep: 'fontset'
    })
  }

  handleOnAppElementsCustomize = () => {
    this.setState({
      currentStep: 3,
      cutomizationStep: 'app-elements'
    })
  }

  handleOnReset = () => {
    this.themeManager.updateThemeCustomVariables(this._customTheme.name, Immutable({}));
    getAppConfigAction(this.props.appConfig).editCustomTheme(Immutable({})).exec();
  }

  i18n = (id: string) => {
    const intl = this.props.intl;
    return intl ? intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  componentDidUpdate() {
    if (this.props.appConfig && !this.state.loaded) {
      if (this.props.appConfig.theme) {
        const themeInfo = Immutable(this.getThemeInfo(this.props.appConfig.theme));
        this.setState({
          selectedTheme: this.props.appConfig.theme,
          themeInfo: themeInfo
        })
        if (this.props.appConfig.customTheme) {
          this.themeManager.updateThemeCustomVariables(
            this.props.appConfig.theme,
            this.props.appConfig.customTheme
          )
        }
        this._customTheme = {
          themeInfo: themeInfo,
          name: this.props.appConfig.theme
        }
        this.setState({
          loaded: true
        });
      }
    }
  }

  componentDidMount() {
    this.loadThemeListInfo().then((result: ImmutableArray<ThemeInfo>) => {
      this.setState({
        themeInfos: result
      });
    });
  }

  render() {
    const {
      appConfig,
      theme
    } = this.props;

    const configuratorTitle = () => {
      let titleString: string;
      switch(this.state.cutomizationStep) {
        case 'palette':  
          titleString = this.i18n('customPaletteTitle');
          break;
        case 'fontset':
          titleString = this.i18n('customFontsetTitle');
          break;
        case 'app-elements': 
          titleString = this.i18n('customAppElementsTitle');
          break;
        default:
          titleString = '---';
      }
      return titleString;
    }

    return <React.Fragment>
      <Stepper className="jimu-widget widget-builder-themes d-flex flex-column bg-light-300 w-100 h-100"
        css={this.getStyle(theme)}>
        <StepperPane active={this.state.currentStep === 1}>
          <SettingSection title={this.titleStr} className="widget-builder-themes--pane">
            <ThemeSelector
              theme={theme}
              themeListInfo={this.state.themeInfos}
              selectedTheme={appConfig ? appConfig.theme : undefined}
              customThemeVariables={appConfig ? appConfig.customTheme : undefined}
              onChange={this.handleOnChange}
              intl={this.props.intl}
            />
          </SettingSection>
          <SettingSection>
            <Button type="primary" className="w-100" onClick={this.handleOnCustomize} disabled={!this.state.themeInfo}>
              {this.i18n('customTheme')}
            </Button>
          </SettingSection>
        </StepperPane>
        <StepperPane active={this.state.currentStep === 2}>
          <div className="mt-2">
            <Button type="tertiary" onClick={() => this.setState({ currentStep: 1 })}>
              <Icon icon={require('jimu-ui/lib/icons/arrow-left.svg')} />
              {this.i18n('back')}
            </Button>
          </div>
          <ThemeQuickStyler
            className="widget-builder-themes--pane"
            customVariables={this.state.selectedTheme === this._customTheme.name ? appConfig ? appConfig.customTheme : undefined : undefined}
            themeInfo={this.state.themeInfo}
            onChange={this.handleOnUpdate}
            onPaletteCustomize={this.handleOnPaletteCustomize}
            onFontsetCustomize={this.handleOnFontsetCustomize}
            onAppElementsCustomize={this.handleOnAppElementsCustomize}
          />
          <SettingSection className="d-flex">
            <Button css={css`flex: 1;`} onClick={this.handleOnReset}>
              {this.i18n('reset')}
            </Button>
          </SettingSection>
        </StepperPane>
        <StepperPane active={this.state.currentStep === 3}>
          <div className="p-3 d-flex align-items-center">
            <h4 className="text-dark-600 flex-grow-1 m-0">
              {configuratorTitle()}
            </h4>
            <Button size="sm" type="tertiary" icon onClick={() => this.setState({ currentStep: 2 })} title={this.i18n('close')}>
              <Icon icon={require('jimu-ui/lib/icons/close-12.svg')} />
            </Button>
          </div>
          {this.state.cutomizationStep === 'palette' && <ThemeColorConfigurator 
            themeInfo={this.state.themeInfo}
            onChange={this.handleOnUpdate}
          />}
          {this.state.cutomizationStep === 'fontset' && <FontsetConfigurator 
            themeInfo={this.state.themeInfo}
            onChange={this.handleOnUpdate}
          />}
          {this.state.cutomizationStep === 'app-elements' && <ThemeComponentsConfigurator 
            themeInfo={this.state.themeInfo}
            onChange={this.handleOnUpdate}
          />}
        </StepperPane>
      </Stepper>;
    </React.Fragment>
  }

  getStyle = (theme: IMThemeVariables): SerializedStyles => {
    return css`
      .jimu-builder-stepper-pane {
        display: none;
        height: 100%;
        &.active {
          display: flex;
          flex-direction: column;
        }
      }
      .widget-builder-themes--pane {
        height: 100%;
        flex: 1;
        overflow: auto;
        border-bottom: 1px solid ${theme.colors.palette.light[800]};
        .jimu-widget-setting--section-header h6 {
          font-size: 1rem;
          margin-bottom: 1rem;
          font-weight: ${theme.typography.weights.extraBold};
          color: ${theme.colors.palette.dark[600]};
        }
        .split-line {
          display: none;
        }
      }
    `
  }
}
