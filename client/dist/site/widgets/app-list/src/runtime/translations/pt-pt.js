define({
  search: 'ã_Search_____________Ç',
  newApp: 'ã_Create New_____________________Ç',
  labelByModified: 'ã_Recent experiences___________________Ç',
  labelByTitle: 'ã_Experiences by title_____________________Ç',
  labelByView: 'ã_Experiences by views_____________________Ç',
  ownByMe: 'ã_Owned by me____________Ç',
  ownByAnyone: 'ã_Owned by anyone________________Ç',
  sharedWithMe: 'ã_Shared with me_______________Ç',
  orderByModified: 'ã_Last modified______________Ç',
  orderByTitle: 'ã_Title___________Ç',
  orderByView: 'ã_Most views_____________________Ç',
  views: 'ã_views___________Ç',
  duplicate: 'ã_Duplicate___________________Ç',
  editInfo: 'ã_Edit info___________________Ç',
  viewDetail: 'ã_View details_____________Ç',
  delete: 'ã_Delete_____________Ç',
  name: 'ã_Name_________Ç',
  description: 'ã_Description____________Ç',
  download: 'ã_Download_________________Ç',
  editExperienceInfo: 'ã_Edit experience info_____________________Ç',
  launchApp: 'ã_Launch_____________Ç',
  more: 'ã_More_________Ç',
  popUpOk: 'ã_OK_____Ç',
  popUpCancel: 'ã_Cancel_____________Ç',
  popUpTitle: 'ã_Warning_______________Ç',
  /* tslint:disable-next-line */
  popUpDiscription1: "ã_You don't have the permission to edit this web experience. Currently, you can only edit your own items_____________________________________________________Ç.",
  popUpDiscription2: 'ã_You can view or duplicate it______________________________Ç.',
  descriptionRemind: 'ã_Here is the description of the item____________________Ç. ',
  editThumbnail: 'ã_Edit thumbnail_______________Ç',
  itemDeleteRemind: 'ã_Are you sure you want to delete this item______________________Ç?',
  untitledExperience: 'ã_Untitled experience____________________Ç',
  webExperience: 'ã_Web Experience_______________Ç',
  webExperienceTemplate: 'ã_Web Experience Template________________________Ç',
  createExperience: 'ã_Create your first experience_____________________________Ç',
  toCreate: 'ã_to start a new experience__________________________Ç',
  click: 'ã_Click___________Ç'
});