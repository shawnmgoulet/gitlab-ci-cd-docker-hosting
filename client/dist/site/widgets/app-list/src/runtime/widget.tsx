/** @jsx jsx */
import {BaseWidget, IMState, AllWidgetProps, Immutable, SessionManager, urlUtils, jsx} from 'jimu-core';
import {Button, Input, Icon} from 'jimu-ui';
import {IItem} from '@esri/arcgis-rest-types';
import DetailView from './components/app-list-detailview';
import ListView from './components/app-list-listview';
import {AppListContext} from './lib/app-list-context'
import {ISearchOptions} from '@esri/arcgis-rest-portal';
import {getStyle} from './lib/app-list-style';
import defaultMessages from './translations/default';
import {appServices, templatesServices} from 'jimu-for-builder/service';
import { isTemplateList } from '../../config';
import {AppListEmpty} from './components/app-list-empty';
interface WState {
  apps: IItem[],
  isDetailContent: boolean,
  accessType: 'me' | 'anyone' | 'notme',
  filterField?: string,
  filterTitle: string,
  searchText: string,
  loading: boolean;
  experienceType: ExperienceType;
  defaultTemplates: Array<any>
}

interface ExtraProps {
  // refreshAppList: boolean;
  title: string,
  config: isTemplateList,
  isTest?: boolean,
}

enum AccessType {
  ME = 'me',
  ANYONE = 'anyone',
  NOTME = 'notme'
}

enum ExperienceType {
  WE = '"Web Experience"',
  WET = '"Web Experience Template"'
}

enum FilterField {
  Modified = 'modified',
  Title = 'title',
  NumViews = 'numViews'
}

let IconSearch   = require('jimu-ui/lib/icons/search-16.svg');
let IconAdd      = require('jimu-ui/lib/icons/add-16.svg');
let IconViewList = require('jimu-ui/lib/icons/app-view-list.svg');
let IconViewCard = require('jimu-ui/lib/icons/app-view-card.svg');

export default class Widget extends BaseWidget<AllWidgetProps<{}> & ExtraProps , WState>{

  contentNode: HTMLDivElement;
  appListContainer: HTMLDivElement;
  static scrollTop: number = 0;
  pageStart: number = 1;
  searchPromise: Promise<any> = null;
  onSearchTextInputed: any;
  static mapExtraStateProps = (state: IMState) => {
    return {
      // refreshAppList: state.builder.refreshAppList
    }
  };

  constructor(props){
    super(props);
    this.state = {
      apps: [],
      isDetailContent: true,
      accessType: AccessType.ME,
      // filterTitle: this.props.title,
      filterTitle: this.nls('labelByModified'),
      filterField: FilterField.Modified,
      searchText: '',
      loading: false,
      experienceType: ExperienceType.WE,
      defaultTemplates: [],
    };

  }

  componentDidMount() {
    if(!this.props.isTest){
      this.getDefaultTemplate()
    }
    let requestOption = this.getRequestOption(this.state.accessType, this.state.filterField);
    this.setState({loading: true});
    this.refresh(requestOption as any);
    if (this.contentNode) {
      this.contentNode.addEventListener('scroll', this.onScrollHandle.bind(this));
      this.contentNode.scrollTop = Widget.scrollTop;
    }
  }

  onScrollHandle(event) {
    const clientHeight = event.target.clientHeight
    const scrollHeight = event.target.scrollHeight
    const scrollTop = event.target.scrollTop
    const isBottom = (clientHeight + scrollTop === scrollHeight)
    if(!this.state.loading && isBottom){
      let requestOption = this.getRequestOption(this.state.accessType, this.state.filterField, true);
      this.setState({loading: true});
      this.refresh(requestOption as any, true);
    }
  }

  componentWillUnmount() {
    if (this.contentNode) {
      this.contentNode.removeEventListener('scroll', this.onScrollHandle.bind(this));
      Widget.scrollTop = this.contentNode.scrollTop;
    }
  }

  getExperienceType(){
    const views = this.getQueryString('views');
    let experienceType = ExperienceType.WE;
    if(views == 'templatelist'){
      experienceType = ExperienceType.WET;
    }
    this.setState({
      experienceType: experienceType
    });
    return experienceType;
  }

  getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null){
      return unescape(r[2]);
    }  
    return null;
  }

  nls = (id: string) => {
    return this.props.intl ? this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  getRequestOption = (accessType: string, filterField: string, isMore: boolean = false) => {
    if(this.props.isTest) return null;
    let experienceType = this.getExperienceType();
    let session        = SessionManager.getInstance().getMainSession();
    let requestOption  = Immutable({});
    let searchText = ''
    if(this.state.searchText){
      searchText = ` AND title:"${this.state.searchText}"`
    }

    if (session) {
      switch (accessType) {
        case AccessType.ME:
          requestOption = requestOption.merge({
            q: `type: ${experienceType} AND owner:${session.username} ${searchText}`
          });
          break;
        case AccessType.ANYONE:
          requestOption = requestOption.merge({
            q: `type: ${experienceType}${searchText}`
          });
          break;
        case AccessType.NOTME:
          requestOption = requestOption.merge({
            q: `type: ${experienceType} NOT owner:${session.username}${searchText}`
          });
          break;
      }
    }else {
      requestOption = requestOption.merge({
        q: `type: ${experienceType}`
      });
    }

    if (filterField) {
      if (filterField === 'title') {
        requestOption = requestOption.merge({
          sortField: filterField,
          sortOrder: 'asc'
        });
      } else {
        requestOption = requestOption.merge({
          sortField: filterField,
          sortOrder: 'desc'
        });
      }
    }



    if(isMore){
      this.pageStart = this.state.apps.length + 1;
      requestOption = requestOption.merge({
        num: 30,
        start: this.pageStart
      });
    }else{
      this.pageStart = 1;
      // let containerWidth = this.appListContainer.clientWidth;
      let containerHeight = document.body.clientHeight - 215;
      // let pageNum = Math.ceil(containerHeight / 330) * Math.ceil((containerWidth / 240));
      let pageNum = Math.ceil(containerHeight / 60);

      requestOption = requestOption.merge({
        num: pageNum,
        start: this.pageStart
      });
    }
    return requestOption;      
    
  }

  refresh(requestOption?: ISearchOptions, isMore: boolean = false){
    if (!requestOption) {
      return;
    }

    this.searchPromise = null;
    this.searchPromise = this.appServiceSearchApp(requestOption);
    this.searchPromise.then(apps => {
      if (!this.searchPromise) {
        this.setState({loading: false});
        return;
      }

      let newApps = this.state.apps;
      if(isMore){
        newApps = newApps.concat(apps);
      }else{
        newApps = apps;
      }

      const {
        accessType,
        experienceType,
        defaultTemplates
      } = this.state;

      if(accessType == AccessType.ME && experienceType == ExperienceType.WET && !isMore){
        newApps = newApps.concat(defaultTemplates);
      }

      this.setState({
        apps: newApps,
        loading: false
      });
    }, () => {
      this.setState({loading: false});
    })
  }

  getDefaultTemplate = () => {
    fetch(`${urlUtils.getAbsoluteRootUrl()}templates/templates-info.json`)
    .then(res => res.json()).then((templates: any[]) => {
      templates = templates.map((t, index) => {
        let listItem = {
          isExperiencesTemplate : false,
          name : t.name,
          title : t.label,
          thumbnail : t.thumbnail,
          description : t.description,
          isDefaultTemplate: true,
          id: `default${index}`
        };
        return listItem;
      })
      this.setState({
        defaultTemplates: templates
      })
      return { templates };
    })
  }

  appServiceSearchApp = (requestOption): Promise<any> => {
    return appServices.searchApp(requestOption);
  }

  newApp = () => {
    window.location.assign(`${window.jimuConfig.mountPath}builder/page/template`);
  }

  deleteApp = (appId: string) => {
    this.setState({ loading: true });
    appServices.deleteApp(appId).then(() => {
      let self = this;

      // since deleteApp api can not update resultdata timely, so add timeout function to fresh
      setTimeout(() => {
        let requestOption = self.getRequestOption(self.state.accessType, self.state.filterField);
        self.refresh(requestOption as any);
      }, 1000);
    }, () => {
      this.setState({loading: false});
    });
  }

  refreshList = () => {
    let self = this;
    this.setState({
      filterField: FilterField.Modified,
      accessType: AccessType.ME,
      filterTitle: this.nls('labelByModified'),
      searchText: ''
    });

    // since updateApp api can not update resultdata timely, so add timeout function to fresh
    setTimeout(() => {
      let requestOption = self.getRequestOption(self.state.accessType, self.state.filterField);
      self.refresh(requestOption as any);
    }, 1000);
  }

  duplicateAppItem = (appId: string, isDefaultTemplate, name: string ) => {
    if(isDefaultTemplate){
      this.duplicateDefaultTemplate(name);
    }else{
      this.duplicateApp(appId);
    }
  }

  duplicateApp = (appId: string) => {
    this.setState({ loading: true });

    appServices.duplicateApp(appId).then(result => {
      if (result) {
        this.refreshList();
      } else {
        this.setState({ loading: false });
      }
    }, () => {
      this.setState({ loading: false });
    })
  }

  duplicateDefaultTemplate = (name: string) => {
    this.setState({ loading: true });
    let title = this.nls('untitledExperience');
    templatesServices.duplicateTemplateFromDefault(title, name).then(result => {
      if (result) {
        this.refreshList();
      } else {
        this.setState({ loading: false });
      }
    }, err => {
      this.setState({ loading: false });
    })
  }
  
  createApp = (appId: string, isDefaultTemplate, name: string ) => {
    if(isDefaultTemplate){
      this.crateAppByDefaultTemplate(name);
    }else{
      this.crateAppByTemplate(appId);
    }
  }

  crateAppByTemplate = (appId: string) => {
    this.setState({ loading: true });

    templatesServices.crateAppByTemplate(appId).then(newAppId => {
      if (newAppId) {
        let to = window.jimuConfig.mountPath + 'builder' + '?id=' + newAppId;
        window.location.href = to;
      }
      this.setState({ loading: false });
    }, () => {
      this.setState({ loading: false });
    })
  }

  crateAppByDefaultTemplate = (name: string) => {
    this.setState({ loading: true });
    let title = this.nls('untitledExperience');
    templatesServices.createAppByDefaultTemplate(title, name).then(newAppId => {
      if (newAppId) {
        let to = window.jimuConfig.mountPath + 'builder' + '?id=' + newAppId;
        window.location.href = to;
      }
      this.setState({ loading: false });
    }, err => {
      this.setState({ loading: false });
    })
  }

  switchListView = (viewState: boolean) => {
    this.setState({
      isDetailContent: viewState
    });
  }

  accessChange = (e: any) => {
    let accessType = e.target.value;
    let requestOption = this.getRequestOption(accessType, this.state.filterField);
    this.setState({loading: true});
    this.refresh(requestOption as any);

    this.setState({
      accessType: accessType
    });
  }

  filterFieldChange = (e: any) => {
    let filterField = e.target.value;
    let requestOption = this.getRequestOption(this.state.accessType, filterField);
    this.setState({loading: true});
    this.refresh(requestOption as any);

    let filterLabel = '';
    if (filterField == FilterField.Title) {
      filterLabel = this.nls('labelByTitle');
    } else if (filterField == FilterField.NumViews) {
      filterLabel = this.nls('labelByView');
    } else if (filterField == FilterField.Modified) {
      filterLabel = this.nls('labelByModified');
    }

    this.setState({
      filterField: filterField,
      filterTitle: filterLabel
    });
  }

  handleKeydown = (e: any) => {
    if (e.keyCode === 13) {
      this.searchExperiences(e.target.value);
    } else {
      return;
    }
  }

  searchExperiences = (content: string) => {
    let requestOption = this.getRequestOption(this.state.accessType, this.state.filterField) as any;
    let q = `${requestOption.q} AND title:"${content}"` ;
    requestOption = requestOption.set('q', q);
    this.setState({loading: true});
    this.refresh(requestOption as any);
  }

  searchTextChange = (e: any) => {
    let searchText = e.target.value;
    this.setState({
      searchText: searchText
    });
    clearTimeout(this.onSearchTextInputed);
    this.onSearchTextInputed = setTimeout(() => {
      this.searchExperiences(searchText);
    }, 500);
  }

  render() {
    const isDevEdition   = window.isDevEdition;
    const isLogged       = SessionManager.getInstance().getMainSession() ? true : false;
    const isTemplateList = this.props.config.isTemplateList || false;
    const {
      isDetailContent,
      apps
    } = this.state;
    return <AppListContext.Provider value = {{deleteApp: this.deleteApp, refreshList: this.refreshList, duplicateAppItem: this.duplicateAppItem, createApp: this.createApp}} >
      <div css={getStyle(this.props.theme)} className="h-100">
        <div className="widget-builder-app-list bg-light-300 h-100" ref={ node => this.contentNode = node }>
          {/* banner */}
          <div className="banner-con fixed-top bg-light-300">
            <div className="widget-builder-app-list-screen">
              <div style={{overflow: 'hidden'}} className="app-list-search-container">

                <div className="app-list-banner d-flex justify-content-between" style={{position: 'relative'}}>
                  <h2 className="flex-sm-fill list-title">{isTemplateList ? this.nls('webExperienceTemplate') : this.state.filterTitle}</h2>
                  {this.state.experienceType == ExperienceType.WE && <Button className="float-left btn-lg pt-0 pb-0 app-list-newapp" size="lg" type="primary" onClick={this.newApp}>
                    <Icon className="app-list-newapp-icon" width={16} height={16} icon={IconAdd}/>{this.nls('newApp')}
                  </Button>}
                </div>

              </div>
            </div>

            <div className="app-list-filterbar ">
              <div className="d-flex justify-content-between align-items-center app-list-filterbar-title widget-builder-app-list-screen">

                <div className="search-con d-flex">
                  <div style={{position: 'absolute'}}
                    onClick={() => {this.searchExperiences(this.state.searchText)}}>
                    <Icon width={24} height={24} icon={IconSearch} className="app-list-searchIconFill"/>
                  </div>

                  <Input className="float-left pt-2 pb-2 app-list-searchbox app-list-h1 flex-sm-fill" 
                    placeholder={this.nls('search')} 
                    onChange={this.searchTextChange} value={this.state.searchText} onKeyDown ={ (e) => {this.handleKeydown(e)}}>
                  </Input>
                </div>
                
                <div className="flex-sm-grow-1">
                  <Button icon  type={!isDetailContent ? 'primary' : ''} className="float-right app-list-switchview app-list-switchview-btton app-list-switchview-right" 
                    onClick={() => this.switchListView(false)}>
                    <Icon icon={IconViewList} width={14} height={14} className="app-list-iconfill app-list-iconmargin"/>
                  </Button>

                  <Button icon  type={isDetailContent ? 'primary' : ''} className="float-right app-list-switchview " 
                    onClick={() => this.switchListView(true)}>
                    <Icon icon={IconViewCard} width={14} height={14} className="app-list-iconfill app-list-iconmargin"/>
                  </Button>
                  
                  <Input type="select" onChange={this.filterFieldChange} value={this.state.accessType} 
                    className="float-right filterbar-input">
                    <option value="modified">{this.nls('orderByModified')}</option>
                    <option value="title">{this.nls('orderByTitle')}</option>
                    {!isDevEdition && <option value="numViews">{this.nls('orderByView')}</option>}
                  </Input>

                  {(!isDevEdition && isLogged) && <Input type="select" onChange={this.accessChange} value={this.state.accessType} 
                    className="float-right filterbar-input">
                    <option value={AccessType.ME}>{this.nls('ownByMe')}</option>
                    <option value={AccessType.NOTME}>{this.nls('sharedWithMe')}</option>
                    <option value={AccessType.ANYONE}>{this.nls('ownByAnyone')}</option>
                  </Input>}
                </div>

              </div>
            </div>
          </div>

          <div className="widget-builder-app-list-screen" ref={ref => {this.appListContainer = ref; }}>
            {/* appList */}
            <div>
              <div className="app-list-content">
                <div className="top-space"></div>
                {this.state.isDetailContent && <DetailView portalUrl={this.props.portalUrl} intl={this.props.intl}
                  folderUrl={this.props.context.folderUrl} apps={this.state.apps} switchListView={this.switchListView} isTemplateList={isTemplateList}></DetailView>}
                {!this.state.isDetailContent && <ListView portalUrl={this.props.portalUrl} intl={this.props.intl}
                  folderUrl={this.props.context.folderUrl} apps={this.state.apps} switchListView={this.switchListView} isTemplateList={isTemplateList}></ListView>}
              </div>
            </div>
            {(apps.length < 1 && !this.state.loading) && <AppListEmpty intl={this.props.intl} portalUrl={this.props.portalUrl} folderUrl={this.props.context.folderUrl}></AppListEmpty>}
          </div>

        </div>
        {this.state.loading && <div style={{ position: 'absolute', left: '50%', top: '50%' }} className="jimu-primary-loading"></div>}

      </div>
    </AppListContext.Provider>;
  }
}