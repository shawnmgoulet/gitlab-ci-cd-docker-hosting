import { ThemeVariables, css, SerializedStyles, polished } from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles {
  let max_width = 1200;
  // with font_size_root theme variable can't be get, so define font_size_root temporarily
  let font_size_root = 16;
  return css`
    .widget-builder-app-list {
      overflow-y: auto;

      .widget-builder-app-list-screen {
        width:810px;
        margin: auto;
      }
     

      button.app-list-newapp:hover{
        color: ${theme.colors.black} !important;
      }

      .modal-content {
        background:none;
      }

      .app-list-newapp {
        padding-left: ${polished.rem(16)};
        padding-right: ${polished.rem(16)};
        font-size: ${polished.rem(16)};
        margin-left: ${polished.rem(20)};
        height:${polished.rem(40)};
        .app-list-newapp-icon {
          margin-right: ${polished.rem(8)};
        }
      }

      .app-list-search-container {
        padding-left: ${polished.rem(16)};
        padding-right: ${polished.rem(16)};
      }
    
      .app-list-h1 {
        font-size: ${polished.rem(24)};
      }
    
      .app-list-h2 {
        font-size: ${polished.rem(16)};
      }

      .app-list-h3 {
        font-size: ${polished.rem(13)};
      }
    
      .app-list-maxwidth {
        max-width: ${max_width / font_size_root}rem;
        top:40px;
      }
      
      .banner-con {
        top:50px;
      }

      .app-list-banner {
        overflow: hidden;
        width: 100%;
        height: ${80 / font_size_root}rem;
        align-items: center;
        .list-title {
          font-size:${20 / font_size_root}rem;
          color: ${theme.colors.palette.dark[600]};
        }
      }
    
      .app-list-filterbar {
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        padding-top: ${30 / font_size_root}rem;
        padding-bottom: ${27 / font_size_root}rem;
        border-top: 1px solid ${theme.colors.palette.secondary[800]};
        .search-con {
          position:relative;
          align-items:center;
          border-radius:2px;
          background:${theme.colors.palette.light[200]};
          width:${polished.rem(257)};
        }
        .app-list-switchview {
          border-radius: 2px 0 0 2px;
        }
        .app-list-switchview-right {
          border-radius: 0 2px 2px 0;
        }
        .app-list-searchIconFill {
          padding-left: ${polished.rem(6)};
        }
        .app-list-searchbox {
          color: ${theme.colors.black};
          padding-left: ${28 / font_size_root}rem !important;
          width: 100%;
          height: ${32 / font_size_root}rem;
          font-size:${14 / font_size_root}rem;
        }
    
        .filterbar-input {
          width: ${180 / font_size_root}rem;
          margin-top: 3px;
          margin-right:${polished.rem(10)};
          height:${polished.rem(32)};
          line-height:${polished.rem(32)};
          padding-top:0;
          padding-bottom:0;
        }

        .app-list-filterbar-title {
          padding-left: ${polished.rem(16)};
          padding-right: ${polished.rem(16)};
        }
      }
    
      .app-list-content {
        width: 100%;
        padding-bottom: ${100 / font_size_root}rem;
      }
      .top-space {
        height: ${186 / font_size_root}rem;
      }
      .app-list-detailview {
        width: 100%;

        .app-list-detailview-name {
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          max-width: ${polished.rem(120)};
        }
    
        .app-list-detailview-content {
          width: 100%;
         
          display: flex;
          flex-wrap: wrap;
    
          .app-list-detailview-pic {
            background-size: cover;
            background-position: top center;
          }
        }

        .dropdown-toggle::after {
          display: none !important;
        }
      }
    
      .app-list-listview {

        .app-list-listview-name {
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        }

        .app-list-listview-view {
          min-width: ${polished.rem(90)};
        }

        .app-list-listview-container {
          position: relative;
          padding-left: ${polished.rem(16)};
          padding-right: ${polished.rem(16)};
        }
    
        .app-list-listview-pic {
          width:${polished.rem(60)};
          height:${polished.rem(40)};
          left:0;
          top:0;
          background-size: cover;
          background-position: center center;
          background-color: ${theme.colors.palette.dark[600]};
          position: inherit;
        }
      }

      .detailview-item {
        width: ${polished.rem(240)};
        height: ${polished.rem(330)};
        margin: 0 ${polished.rem(15)} ${polished.rem(30)} ${polished.rem(15)};
      }

      .app-item-more {
        margin-right: -0.625rem;
      }

      .app-item-backcolor {
        background-color: ${theme.colors.palette.light[500]};
      }

      .app-item-launch:focus {
        outline: none;
        box-shadow: none !important;
      }
    
      .app-list-icon-margin {
        fill: #BCC4CD !important;
        margin-right: 0;
        margin-left: 0;
      }

      .app-list-dropdown {
        padding: 0;
        margin: 0;
        min-width: 0;
      }

      .dropdown-toggle::after {
        display: none !important;
      }

      .app-list-iconmargin {
        margin-top: 0.2rem;
      }
    
      .app-list-searchIconFill {
        fill: ${theme.colors.palette.dark[200]} !important;
        color: ${theme.colors.palette.dark[200]} !important;
      }

      .item-message-con {
        padding:${polished.rem(12)};
        padding-bottom:0;
      }
      .item-card-title {
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
        height:${polished.rem(40)};
        color:${theme.colors.palette.dark[800]};
        font-size:${16 / font_size_root}rem;
        margin-bottom:${polished.rem(12)};
      }
      .app-list-detailview-name, .app-list-detailview-time {
        color:${theme.colors.palette.dark[800]};
        font-size:${13 / font_size_root}rem;
        line-height:${16 / font_size_root}rem;
        margin-bottom:${polished.rem(9)};
      }
      .edit-item-con {
        & {
          border-top:1px solid ${theme.colors.palette.light[800]};
          padding-top:${polished.rem(4)};
        }
        .item-views-number {
          color:${theme.colors.palette.dark[400]};
        }
      }
      .description-con {
        & {
          background-color: ${polished.rgba(theme.colors.white, 0.8)};
          left:0;
          top:0;
          right:0;
          bottom:0;
          box-sizing: border-box;
          padding:${polished.rem(16)};
          color:${theme.colors.palette.dark[800]};
          font-size:${12 / font_size_root}rem;
          opacity: 0;
          transition: all ease .2s;
          overflow-y:scroll;
        }
        div {

        }
      }
      .dropdown-toggle svg.jimu-icon {
        transform: rotate(0deg);
        margin-right: 3px;
      }
      .app-list-detailview-pic:hover .description-con {
        opacity: 1;
      }
      .item-list-con {
        & {
          height:${polished.rem(44)};
        }
        .item-list-name-con {
          padding-left:${polished.rem(76)};
          font-size:${16 / font_size_root}rem;
          color:${theme.colors.palette.dark[800]};
        }
        .app-list-h3 {
          color:${theme.colors.palette.dark[600]};
        }
        .item-list-border-l {
          border-left 1px solid ${theme.colors.palette.secondary[800]};
          box-sizing:border-box;
          padding-left:${polished.rem(16)};
          height:${polished.rem(24)};
          margin-top:${polished.rem(8)};
        }
        .item-list-icon-con {
          padding-right:0;
        }
      }
      @media only screen and (min-width: 1280px) {
        .widget-builder-app-list-screen {
          width:1086px;
          .app-list-filterbar .search-con {
            width:${polished.rem(400)};
          }
        }
        .app-list-filterbar .filterbar-input {
          width: ${200 / font_size_root}rem;
          margin-right:${polished.rem(20)};
        }
      }
      @media only screen and (min-width: 1400px) {
        .widget-builder-app-list-screen {
            width:1350px;
        }
      }

      @media only screen and (min-width: 1680px) {
        .widget-builder-app-list-screen {
            width:1620px;
        }
      }
    }
  `;
}