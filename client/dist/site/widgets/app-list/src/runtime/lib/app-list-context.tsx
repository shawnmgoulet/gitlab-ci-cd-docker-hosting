import {React} from 'jimu-core';

export const AppListContext = React.createContext({
  deleteApp: (appId: string) => {},
  refreshList: () => {},
  duplicateAppItem: (appId: string, isDefaultTemplate, name: string) => {},
  createApp: (appId: string, isDefaultTemplate, name: string ) => {},
});