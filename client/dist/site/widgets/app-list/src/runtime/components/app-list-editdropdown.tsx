/** @jsx jsx */
import {React, IntlShape, css, jsx} from 'jimu-core';
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Icon, Modal} from 'jimu-ui';
import {AppListContext} from '../lib/app-list-context';
import {AppEditInfo} from '../components/app-list-editinfo';
import {IItem} from '@esri/arcgis-rest-types';
import defaultMessages from '../translations/default';
import DeletePopup from './app-list-delete-info';

interface State{
  isOpen: boolean;
  isShownEditInfo: boolean;
  isShowDeleteAlertPopup: boolean;
}

interface Props{
  isOwner: boolean;
  portalUrl: string;
  appItem: IItem;
  folderUrl: string;
  isTemplateList: boolean;
  isShownEditInfo: boolean;
  className?: string;
  intl?: IntlShape;
  handleToggleEditInfo?: any;
}

let IconMore = require('jimu-ui/lib/icons/more-16.svg');

export default class Widget extends React.PureComponent<Props, State>{
  eidtInfoStyle = {width: '100%', height: '100%', maxWidth: '5000px', margin: 0};

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isShownEditInfo: false,
      isShowDeleteAlertPopup: false
    }
  }

  getStyle = () => {
    return css`
      .modal-content {
        background:none;
      }
    `
  }
  appLaunch = () => {
    let appUrl = window.jimuConfig.useStructuralUrl ? `../stemapp/${this.props.appItem.id}/` : `../stemapp/?id=${this.props.appItem.id}`;
    window.open(appUrl);
  }

  viewDetails = () => {
    let detailUrl = `${this.props.portalUrl}/home/item.html?id=${this.props.appItem.id}`;
    window.open(detailUrl);
  }

  showEditInfo = () => {
    this.setState({isShownEditInfo: true});
  }

  onEditInfoCancel = () => {
    if(this.props.handleToggleEditInfo){
      this.props.handleToggleEditInfo(null, true)
    }
    this.setState({isShownEditInfo: false});
  }
  onEditInfoOk = () => {
    if(this.props.handleToggleEditInfo){
      this.props.handleToggleEditInfo(null, true)
    }
    this.setState({isShownEditInfo: false}); 
  }

  
  deleteToggle = () => {
    this.setState({
      isShowDeleteAlertPopup: !this.state.isShowDeleteAlertPopup
    });
  }

  render(){
    const isDevEdition = window.isDevEdition;
    const isShowEdit = this.state.isShownEditInfo || this.props.isShownEditInfo;
    const {
      id,
      isDefaultTemplate,
      name
    } = this.props.appItem;
    const {
      isShowDeleteAlertPopup
    } = this.state
    return <AppListContext.Consumer>
      {({deleteApp, refreshList, duplicateAppItem}) => (
        <div css={this.getStyle()} className={this.props.className} title={this.props.intl ? 
          this.props.intl.formatMessage({id: 'more', defaultMessage: defaultMessages.more}) : defaultMessages.more}>
          <Dropdown  direction="right" size="sm" isOpen={this.state.isOpen} className="app-list-dropdown"
            onClick={evt => evt.stopPropagation()} toggle={() => this.setState({isOpen: !this.state.isOpen})}>
            <DropdownToggle size="sm" type="tertiary">
              <Icon style={{transform: 'rotate(0deg)', marginRight: '3px'}} icon={IconMore} className="app-list-iconfill app-list-icon-margin dropdown-icon"></Icon>
            </DropdownToggle>
            <DropdownMenu appendTo="body" className="app-list-dropdown">
              {this.props.isOwner && <DropdownItem className="pb-1" onClick={() => duplicateAppItem(id, isDefaultTemplate, name)} 
                title={this.props.intl.formatMessage({id: 'duplicate', defaultMessage: defaultMessages.duplicate})}>
                {this.props.intl.formatMessage({id: 'duplicate', defaultMessage: defaultMessages.duplicate})}
              </DropdownItem>}

              {(this.props.isOwner && !this.props.isTemplateList) && <DropdownItem className="pb-1" onClick={this.showEditInfo}
                title={this.props.intl.formatMessage({id: 'editInfo', defaultMessage: defaultMessages.editInfo})}>
                {this.props.intl.formatMessage({id: 'editInfo', defaultMessage: defaultMessages.editInfo})}</DropdownItem>}

              {!isDevEdition && <DropdownItem className="pb-1" onClick={this.viewDetails}
                title={this.props.intl.formatMessage({id: 'viewDetail', defaultMessage: defaultMessages.viewDetail})}>
                {this.props.intl.formatMessage({id: 'viewDetail', defaultMessage: defaultMessages.viewDetail})}</DropdownItem>}

              {this.props.isOwner && <DropdownItem className="pb-1" onClick={() => {this.deleteToggle()}}
                title={this.props.intl.formatMessage({id: 'delete', defaultMessage: defaultMessages.delete})}>
                {this.props.intl.formatMessage({id: 'delete', defaultMessage: defaultMessages.delete})}</DropdownItem>}

              {/* <DropdownItem href={`/api/download/${this.props.appItem.id}?portalUrl=${this.props.portalUrl}&token=${SessionManager.getInstance().getSession() ? 
                SessionManager.getInstance().getSession().token : ''}`}>
                {this.props.intl.formatMessage({id: 'download', defaultMessage: defaultMessages.download})}
              </DropdownItem> */}
            </DropdownMenu>
          </Dropdown>
          {isShowEdit && <Modal  css={this.getStyle()}  isOpen={isShowEdit} style={this.eidtInfoStyle} contentClassName="h-100 w-100">
              <AppEditInfo appItem={this.props.appItem} portalUrl={this.props.portalUrl} folderUrl={this.props.folderUrl} 
              onEditInfoCancel={this.onEditInfoCancel} intl={this.props.intl} 
              onEditInfoOk={() => {this.onEditInfoOk(); refreshList()}}></AppEditInfo>
            </Modal>}
          <DeletePopup isOpen={isShowDeleteAlertPopup} deleteToggle={this.deleteToggle} itemId={this.props.appItem.id} intl={this.props.intl}></DeletePopup>
        </div>
      )}
    </AppListContext.Consumer>;
  }
}