import {React, classNames, FormattedDate} from 'jimu-core';
import {Icon, Row, Col, Button, AlertPopup} from 'jimu-ui';
import EditDropDown from './app-list-editdropdown';
import defaultMessages from '../translations/default';
import {AppListContext} from '../lib/app-list-context';
import ViewActionComponent from './app-list-view-action';

export default class Widget extends ViewActionComponent{
 
  render(){
    const {
      IconPlay,
      IconAppPreview,
      IconToolEdit,
      IconAdd,
      isDevEdition,
    } = this.state

    const {
      id,
      isDefaultTemplate,
      name,
      modified
    } = this.props.appItem
    const limitJson = this.getShareState(this.props.appItem.access);
    return <AppListContext.Consumer>
      {({createApp}) => ( 
        <Row onClick={this.handleItemClick} style={{cursor: 'pointer'}} 
          className={classNames('mt-3 mb-3 ml-0 mr-0 position-relative app-item-backcolor border border-2 listview-item item-list-con', 
          this.state.itemSelected ? 'border-primary' : 'border-transparent')} 
          onMouseEnter={() => {this.setState({itemSelected: true})}} 
          onMouseLeave={() => {this.setState({itemSelected: false})}}>

          <Col className="p-0 position-absolute">
            <div className="app-list-listview-pic" style={{backgroundImage: 'url(' + this.getThumbnail() + ')'}}>
            </div>
          </Col>

          <Col sm="5" className="d-flex align-items-center item-list-name-con">
            <div className="app-list-h2" style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} 
            title={this.props.appItem.title}>{this.props.appItem.title}</div>
          </Col>

          <Col sm={isDevEdition ? '3' : '2'} className="d-flex align-items-center">
            <div className="app-list-h3 app-list-listview-name item-list-border-l" title={this.props.appItem.owner}>{this.props.appItem.owner}</div>
          </Col>

          <Col sm="2" className="d-flex align-items-center item-list-border-l">
            <div className="app-list-h3">{modified && <FormattedDate value={new Date(this.props.appItem.modified)}/>}</div>
          </Col>

          {!isDevEdition && <Col sm="1" className="d-flex align-items-center item-list-border-l">
           <div className="app-list-h3 app-list-listview-view">{this.props.appItem.numViews + ' ' + (this.props.intl ? 
              this.props.intl.formatMessage({id: 'views', defaultMessage: defaultMessages.views}) : defaultMessages.views)}</div>
          </Col>}

          <Col sm="2" className="d-flex align-items-center justify-content-end item-list-border-l item-list-icon-con">

            {/* experiences */}
            {!this.props.isTemplateList && <div className="d-flex align-items-center">
              {!isDevEdition && <Button rel="noreferrer"  type="tertiary" title={limitJson.tips}  className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" onClick={this.viewDetails}>
                <Icon  className="app-list-iconfill mr-0" icon={limitJson.icon} />
              </Button>}
              <a href={window.jimuConfig.useStructuralUrl ?
                `../stemapp/${this.props.appItem.id}/` : `../stemapp/?id=${this.props.appItem.id}`}  rel="noreferrer" target="_blank">
                <Button title={this.launchApp} 
                  className="float-right ml-1 mr-1 btn app-item-backcolor app-item-launch border-0" type="tertiary" onClick={this.appLaunch}>
                  <Icon className="app-list-iconfill mr-0" icon={IconPlay}/>
                </Button>
              </a>
              <EditDropDown className="float-right" portalUrl={this.props.portalUrl} intl={this.props.intl}
                appItem={this.props.appItem} folderUrl={this.props.folderUrl} isOwner={this.checkIsOwner()} isTemplateList={this.props.isTemplateList} isShownEditInfo={this.state.isShownEditInfo}>
              </EditDropDown>
            </div>}
            
            {/* template */}
            {this.props.isTemplateList && <div className="d-flex align-items-center">
              <Button 
                type="tertiary" 
                tag="a" 
                href={window.jimuConfig.useStructuralUrl ? `../stemapp/${this.props.appItem.id}/` : `../stemapp/?id=${this.props.appItem.id}`}
                rel="noreferrer" 
                target="_blank"
                title={this.launchApp} 
                className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" 
                onClick={this.appLaunch}
              >
                <Icon  className="app-list-iconfill mr-0" icon={IconAppPreview} />
              </Button>
              {this.checkIsOwner() && <Button rel="noreferrer" type="tertiary" className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" onClick={this.handleToggleEditInfo}>
                <Icon  className="app-list-iconfill mr-0" icon={IconToolEdit} />
              </Button>}
              <Button rel="noreferrer" type="tertiary" className="float-right pl-1 pr-1 app-item-launch border-0" onClick={(e) => {this.appLaunch(e); createApp(id, isDefaultTemplate, name)}}>
                <Icon  className="app-list-iconfill mr-0" icon={IconAdd} />
              </Button>
        
              {!(!this.state.isDevEdition && this.props.appItem.isDefaultTemplate) && <EditDropDown className="float-right" portalUrl={this.props.portalUrl} intl={this.props.intl}
                appItem={this.props.appItem} 
                folderUrl={this.props.folderUrl} 
                isOwner={this.checkIsOwner()} 
                isTemplateList={this.props.isTemplateList} 
                isShownEditInfo={this.state.isShownEditInfo}
                handleToggleEditInfo={this.handleToggleEditInfo}>
              </EditDropDown>}
            </div>}

          </Col>

          <AlertPopup isOpen={this.state.isShowAlertPopup} 
              title={this.popUpTitle} 
              hideCancel toggle={this.handleToggle}>
              <div style={{ fontSize: '1rem' }}>
                {this.popUpDiscription1}
              </div>
              <div style={{ fontSize: '1rem' }} className="mt-4">
                {this.popUpDiscription2}
              </div>
            </AlertPopup>
        </Row>
      )}
    </AppListContext.Consumer>
  }
}