import {React, classNames, FormattedDate} from 'jimu-core';
import {Icon, Button, BSCard as Card, CardBody, AlertPopup} from 'jimu-ui';
import EditDropDown from './app-list-editdropdown';
import defaultMessages from '../translations/default';
import {AppListContext} from '../lib/app-list-context';
import ViewActionComponent from './app-list-view-action';

export default class Widget extends ViewActionComponent{
  
  render() {
    // this.initExtraState();
    const {
      IconPlay,
      IconAppPreview,
      IconToolEdit,
      IconAdd,
      isDevEdition,
      isShowAlertPopup,
    } = this.state
    const {
      id,
      isDefaultTemplate,
      name
    } = this.props.appItem;
    const limitJson = this.getShareState(this.props.appItem.access)
    return <AppListContext.Consumer>
      {({createApp}) => (
        <Card className={classNames('detailview-item', {'border-primary': this.state.itemSelected})} onClick = {this.handleItemClick}
          style={{position: 'relative', cursor: 'pointer'}} 
          onMouseEnter={() => {this.setState({itemSelected: true})}} onMouseLeave={() => {this.setState({itemSelected: false})}}>
            <div className="h-100 w-100" style={{position: 'absolute'}}>
              {/* item image */}
              <div className="w-100 app-list-detailview-pic position-relative" style={{height: '53%', backgroundImage: 'url(' + this.getThumbnail() + ')'}}>
                {/* <div className="description-con position-absolute">
                  <div>
                    {this.props.appItem.description || this.descriptionRemind}
                  </div>
                </div> */}
              </div>
              
              <CardBody className="w-100  justify-content-between flex-column item-message-con" style={{height: '48%'}}>
                <h2 className="app-list-h2 item-card-title" title={this.props.appItem.title}>{this.props.appItem.title}</h2>
                <div className="app-list-h3 app-list-detailview-name" title={this.props.appItem.owner}>{this.props.appItem.owner}</div>
                <div className="app-list-h3 app-list-detailview-time">
                  {this.props.appItem.modified && <FormattedDate value={new Date(this.props.appItem.modified)}/>}
                </div>

                {/* experiences */}
                {!this.props.isTemplateList && <div className="d-flex justify-content-between align-items-center edit-item-con">
                  {/* number of view */}
                  {!isDevEdition ? <div className="app-list-h3 item-views-number">{this.props.appItem.numViews + ' ' + (this.props.intl ? 
                    this.props.intl.formatMessage({id: 'views', defaultMessage: defaultMessages.views}) : defaultMessages.views)}</div> : <div className="app-list-h3"></div>}

                  <div className="d-flex align-items-center item-btn-box">
                    {!isDevEdition && <Button  rel="noreferrer" title={limitJson.tips}  type="tertiary"  className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" onClick={this.viewDetails}>
                      <Icon  className="app-list-iconfill mr-0" icon={limitJson.icon} />
                    </Button>}

                    <Button 
                      type="tertiary" 
                      tag="a" 
                      href={window.jimuConfig.useStructuralUrl ? `../stemapp/${this.props.appItem.id}/` : `../stemapp/?id=${this.props.appItem.id}`}
                      rel="noreferrer" 
                      target="_blank"
                      title={this.launchApp} 
                      className="float-right pl-1 pr-1 app-item-launch border-0" 
                      onClick={this.appLaunch}
                    >
                      <Icon className="app-list-iconfill mr-0" icon={IconPlay}/>
                    </Button>

                    <EditDropDown 
                      className="float-right app-item-more" 
                      portalUrl={this.props.portalUrl} 
                      intl={this.props.intl}
                      appItem={this.props.appItem} 
                      folderUrl={this.props.folderUrl} 
                      isOwner={this.checkIsOwner()} 
                      isTemplateList={this.props.isTemplateList} 
                      isShownEditInfo={this.state.isShownEditInfo}>
                    </EditDropDown>
                  </div>
                </div>}
                
                {/* templates */}
                {this.props.isTemplateList && <div className="d-flex justify-content-between align-items-center edit-item-con">
                  
                  <div className="item-btn-box">
                    <Button rel="noreferrer" type="tertiary" className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" 
                      onClick={(e) => {this.appLaunch(e); createApp(id, isDefaultTemplate, name)}}>
                        <Icon  className="app-list-iconfill mr-0" icon={IconAdd} />
                    </Button>
                    {this.checkIsOwner() && <Button rel="noreferrer"  type="tertiary" className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" onClick={this.handleToggleEditInfo}>
                        <Icon  className="app-list-iconfill mr-0" icon={IconToolEdit} />
                    </Button>}
                    <Button 
                      type="tertiary" 
                      tag="a" 
                      href={this.getPreviewUrl()}
                      rel="noreferrer" 
                      target="_blank"
                      title={this.launchApp} 
                      className="float-right pl-1 pr-1 app-item-launch border-0 mr-2" 
                      onClick={this.appLaunch}
                    >
                        <Icon  className="app-list-iconfill mr-0" icon={IconAppPreview} />
                    </Button>
                  </div>

                  {!(!this.state.isDevEdition && this.props.appItem.isDefaultTemplate) && <div className="d-flex align-items-center item-btn-box">
                    <EditDropDown className="float-right app-item-more" 
                      portalUrl={this.props.portalUrl} 
                      intl={this.props.intl}
                      appItem={this.props.appItem} 
                      folderUrl={this.props.folderUrl} 
                      isOwner={this.checkIsOwner()} 
                      isTemplateList={this.props.isTemplateList} 
                      isShownEditInfo={this.state.isShownEditInfo}
                      handleToggleEditInfo={this.handleToggleEditInfo}>
                      
                    </EditDropDown>
                  </div>}
                </div>}
              </CardBody>

            </div>
            <AlertPopup isOpen={isShowAlertPopup} title={this.popUpTitle} hideCancel toggle={this.handleToggle}>
              <div style={{ fontSize: '1rem' }}>
                {this.popUpDiscription1}
              </div>
              {/* <div style={{ fontSize: '1rem' }} className="mt-4">
                {this.popUpDiscription2}
              </div> */}
            </AlertPopup>
          </Card>
        )}
      </AppListContext.Consumer>
  }
}