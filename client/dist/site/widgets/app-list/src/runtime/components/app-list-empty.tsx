/** @jsx jsx */
import { React, themeUtils, ThemeVariables , css, jsx, polished, IntlShape } from 'jimu-core';
import defaultMessages from '../translations/default';
interface Props{
  portalUrl: string;
  folderUrl: string;
  intl?: IntlShape;
}

interface States{
}

export class _AppListEmpty extends React.PureComponent<Props & {theme: ThemeVariables}, States>{
  
  constructor(props) {
    super(props);
  }

  
  nls = (id: string) => {
    return this.props.intl ? this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  getStyle() {
    let theme = this.props.theme;
    let dark400 = theme.colors && theme.colors.palette ? theme.colors.palette.dark[400] : '#a8a8a8';
    let dark600 = theme.colors && theme.colors.palette ? theme.colors.palette.dark[600] : '#c5c5c5';
    let backgroundUrl = this.props.folderUrl + './dist/runtime/assets/empty.png'
    return css`
        {
         & {
          width:100%;
          margin-top:${polished.rem(120)};
         }
         div {
           text-align: center;
         }
         .empty-con {
           width:100%;
           top:50%;
         }
         .icon-con {
           width:${polished.rem(458)};
           height:${polished.rem(303)};
           background:url(${backgroundUrl}) no-repeat left top;
           background-size: contain;
           margin: 0 auto;
           opacity:0.4;
         }
         .create-experience {
          color:${dark600};
          font-size:${polished.rem(16)};
          line-height:${polished.rem(16)};
          margin-top:${polished.rem(30)};
          font-weight:500;
         }
         .remind-to-click {
          color:${dark400};
          font-size:${polished.rem(14)};
          line-height:${polished.rem(14)};
          margin-top:${polished.rem(20)};
         }
         .remind-to-click span {
          color:${dark600};
          font-weight:500;
         }
        }
        @media only screen and (max-width: 1600px) {
          & {
            margin-top:${polished.rem(80)};
          }
        }
        @media only screen and (max-width: 1360px) {
          & {
            width:100%;
            margin-top:${polished.rem(0)};
          }
          .icon-con {
            width:${polished.rem(200)};
            height:${polished.rem(132)};
          }
          .create-experience {
            margin-top:${polished.rem(20)};
           }
           .remind-to-click {
              margin-top:${polished.rem(10)};
           }
        }
        @media only screen and (max-width: 1176PX) {
          & {
            margin-top:${polished.rem(20)};
          }
        }
      `;
  }

  render() {
    return <div css={this.getStyle()} className="app-list-empty-con">
      <div className="empty-con">
        <div className="icon-con"></div>

        <div className="create-experience">{this.nls('createExperience')}</div>
        <div className="remind-to-click">
          {this.nls('click')} <span>+ {this.nls('newApp')}</span> {this.nls('toCreate')}
        </div>
      </div>


    </div>;
  }
}

export const AppListEmpty = themeUtils.withTheme(_AppListEmpty);