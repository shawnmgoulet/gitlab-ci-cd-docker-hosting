import {React, IntlShape, getAppStore, SessionManager} from 'jimu-core';
import {IItem} from '@esri/arcgis-rest-types';
import defaultMessages from '../translations/default';
interface State{
  tooltipOpen: boolean;
  itemSelected: boolean;
  isShowAlertPopup: boolean;
  isShownEditInfo: boolean;
}
interface IconState{
  IconPlay: any;
  IconPeople: any;
  IconAppPreview: any;
  IconToolEdit: any;
  IconAdd: any;
  IconSharedEveryone: any;
  IconSharedGroup: any;
  IconSharedNoOne: any;
  IconSharedOrganization: any;
}
interface ExtraState{
  isDevEdition: boolean;
  thumbnail: string;
  detailUrl: string;
  limitsTips: string;
}
interface Props{
  itemIdx: number;
  appItem: IItem;
  folderUrl: string;
  portalUrl: string;
  intl?: IntlShape;
  isTemplateList: boolean;
}

interface LimitJson {
  icon: any;
  tips: string;
}
export default class ViewActionComponent extends React.PureComponent<Props, State & IconState & ExtraState>{
  popUpTitle: string;
  popUpDiscription1: string;
  popUpDiscription2: string;
  launchApp: string;
  descriptionRemind: string;

  constructor(props) {
    super(props);
    this.state = {
      tooltipOpen      : false,
      itemSelected     : false,
      isShowAlertPopup : false,
      isShownEditInfo  : false,

      IconPlay       : require('jimu-ui/lib/icons/app-launch.svg'),
      IconPeople     : require('jimu-ui/lib/icons/app-access.svg'),
      IconAppPreview : require('jimu-ui/lib/icons/app-preview.svg'),
      IconToolEdit   : require('jimu-ui/lib/icons/tool-edit.svg'),
      IconAdd        : require('jimu-ui/lib/icons/add-16.svg'),

      IconSharedEveryone     : require('jimu-ui/lib/icons/shared-everyone.svg'),
      IconSharedGroup        : require('jimu-ui/lib/icons/shared-group.svg'),
      IconSharedNoOne        : require('jimu-ui/lib/icons/shared-no-one.svg'),
      IconSharedOrganization : require('jimu-ui/lib/icons/shared-organization.svg'),

      isDevEdition : window.isDevEdition,
      thumbnail    : '',
      detailUrl    : '',
      limitsTips   : '',
    }

    this.popUpTitle        = this.props.intl ? this.props.intl.formatMessage({id: 'popUpTitle', defaultMessage: defaultMessages.popUpTitle}) : defaultMessages.popUpTitle;
    this.popUpDiscription1 = this.props.intl ? this.props.intl.formatMessage({id: 'popUpDiscription1', defaultMessage: defaultMessages.popUpDiscription1}) : defaultMessages.popUpDiscription1;
    this.popUpDiscription2 = this.props.intl ? this.props.intl.formatMessage({id: 'popUpDiscription2', defaultMessage: defaultMessages.popUpDiscription2}) : defaultMessages.popUpDiscription2;
    this.launchApp         = this.props.intl ? this.props.intl.formatMessage({id: 'launchApp', defaultMessage: defaultMessages.launchApp}) : defaultMessages.launchApp;
    this.descriptionRemind = this.props.intl ? this.props.intl.formatMessage({id: 'descriptionRemind', defaultMessage: defaultMessages.descriptionRemind}) : defaultMessages.descriptionRemind;
  }

  componentDidMount(){
    this.initExtraState();
  }

  toggle = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  handleItemClick = () => {
    if(this.props.isTemplateList) return false;
    if (this.checkIsOwner()) {
      let to = window.jimuConfig.mountPath + 'builder' + '?id=' + this.props.appItem.id;
      // jimuHistory.browserHistory.push(to);
      window.location.href = to;
    } else {
      this.setState({
        isShowAlertPopup: true
      })
    }
  }

  appLaunch = (e) => {
    e.stopPropagation();
  }

  checkIsOwner = () => {
    let user = getAppStore().getState().user;
    if (user && this.props.appItem.owner && user.username === this.props.appItem.owner) {
      return true;
    } else {
      return false;
    }
  }

  viewDetails = (e) => {
    this.appLaunch(e)
    let detailUrl = `${this.props.portalUrl}/home/item.html?id=${this.props.appItem.id}`;
    window.open(detailUrl);
  }

  handleToggle = () => {
    this.setState({
      isShowAlertPopup: !this.state.isShowAlertPopup
    });
  }


  handleToggleEditInfo = (e, isFromChild?: boolean) => {
    if(!isFromChild){
      e.stopPropagation();
    }
    this.setState({
      isShownEditInfo: !this.state.isShownEditInfo
    });
  }

  initExtraState = () => {
    const detailUrl   = `${this.props.portalUrl}/home/item.html?id=${this.props.appItem.id}`;
    this.setState({
      detailUrl: detailUrl,
    });
  }

  getThumbnail = () => {
    let itemThumbnail     = this.props.appItem.thumbnail;
    let isDefaultTemplate = this.props.appItem.isDefaultTemplate
    let thumbnail         = itemThumbnail;

    const session           = SessionManager.getInstance().getSessionByUrl(this.props.portalUrl);
    const queryToken      = session && session.token ? '?token=' + session.token : '';
    if (thumbnail) {
      thumbnail = this.props.portalUrl + '/sharing/rest/content/items/' + this.props.appItem.id + '/info/' + thumbnail + queryToken;
    } else {
      thumbnail = this.props.folderUrl + './dist/runtime/assets/defaultthumb.png';
    }

    if(itemThumbnail && this.state.isDevEdition){
      thumbnail = window.location.origin + '/apps/' + this.props.appItem.id + '/' + itemThumbnail;
    }

    if(isDefaultTemplate){
      thumbnail = window.location.origin + '/' + itemThumbnail;
    }

    return thumbnail;
  }

  getPreviewUrl = () => {
    let isDefaultTemplate = this.props.appItem.isDefaultTemplate
    let previewUrl;
    if(isDefaultTemplate){
      previewUrl =  `${window.location.origin}/stemapp/?config=/templates/${name}/config.json`;
    }else{
      previewUrl = window.jimuConfig.useStructuralUrl ? `../stemapp/${this.props.appItem.id}/` : `../stemapp/?id=${this.props.appItem.id}`
    }
    return previewUrl;
  }

  getShareState = (access: string): LimitJson => {
    let limitJson = {
      icon: this.state.IconSharedEveryone,
      tips: 'Shared: Everyone'
    }
    if(!access) return null;
    switch (access){
      case 'public':
        limitJson.tips = 'Shared: Everyone';
        limitJson.icon = this.state.IconSharedEveryone;
        break;
      case 'private':
        limitJson.tips = 'Shared: No One';
        limitJson.icon = this.state.IconSharedNoOne;
        break;
      case 'org':
        limitJson.tips = 'Shared: Organization';
        limitJson.icon = this.state.IconSharedOrganization;
        break;
      case 'shared':
        limitJson.tips = 'Shared: Group';
        limitJson.icon = this.state.IconSharedGroup;
        break;
    }
    return limitJson;
  }

  isHideEditDropBtn = () => {
    
    if(this.state.isDevEdition){
      return true;
    }else {
      if(this.props.appItem.isDefaultTemplate){
        return false;
      }
    }
  }



}