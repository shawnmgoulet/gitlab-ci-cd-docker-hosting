/** @jsx jsx */
import {React, classNames, css, jsx, IntlShape} from 'jimu-core';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'jimu-ui';
// import { Modal, ModalHeader, ModalBody, ModalFooter } from 'jimu-ui/reactstrap';
import defaultMessages from '../translations/default';
import {AppListContext} from '../lib/app-list-context';
interface props {
  itemId: string;
  isOpen: boolean;
  deleteToggle: () => void;
  intl?: IntlShape;
}

export default class DeletePopup extends React.PureComponent<props, {isOpen: boolean}>{

  constructor(props){
    super(props);
  }
  getStyle = () => {
    return css`
      svg {
        color: #fff;
      }
    `
  }
  render(){
    let {isOpen} = this.props;
    const deleteText = this.props.intl ? this.props.intl.formatMessage({id: 'delete', defaultMessage: defaultMessages.delete}) : defaultMessages.delete;
    const popUpCancel = this.props.intl ? this.props.intl.formatMessage({id: 'popUpCancel', defaultMessage: defaultMessages.popUpCancel}) : defaultMessages.popUpCancel;
    const deleteRemind = this.props.intl ? this.props.intl.formatMessage({id: 'itemDeleteRemind', defaultMessage: defaultMessages.itemDeleteRemind}) : defaultMessages.itemDeleteRemind;
    return <AppListContext.Consumer>
      {({deleteApp}) => (
          <Modal className={classNames('d-flex justify-content-center')} isOpen={isOpen} centered={true} toggle={this.props.deleteToggle}>
            <ModalHeader tag="h4" toggle={this.props.deleteToggle} className="item-delete-header" css={this.getStyle()}>
              {deleteText}
            </ModalHeader>
            <ModalBody>
              {deleteRemind}
            </ModalBody>
            <ModalFooter>
              <Button type="danger" onClick={() => { deleteApp(this.props.itemId); this.props.deleteToggle()}}>
                {deleteText}
              </Button>
              <Button type="secondary" onClick={this.props.deleteToggle}>
                {popUpCancel}
              </Button>
            </ModalFooter>
          </Modal>
      )}
    </AppListContext.Consumer>
  }
}

