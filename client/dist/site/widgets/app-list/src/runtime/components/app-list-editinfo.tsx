/** @jsx jsx */
import { React, themeUtils, ThemeVariables , css, jsx, SessionManager, 
  Immutable, defaultMessages as commonMessages, IntlShape, polished, ImmutableObject } from 'jimu-core';
import { Icon, Input, Button } from 'jimu-ui';
import {IItem} from '@esri/arcgis-rest-types';
import defaultMessages from '../translations/default';
import {appServices} from 'jimu-for-builder/service';
// import {getAppStore} from 'jimu-ui';
interface Props{
  appItem: IItem;
  portalUrl: string;
  folderUrl: string;
  onEditInfoCancel?: () => void;
  onEditInfoOk?: () => void;
  intl?: IntlShape;
}

interface States{
  localAppItem: ImmutableObject<IItem>;
  currentTitleInput: string;
  currentDescriptInput: string;
  loading: boolean;
  files: any;
}

let IconClose = require('jimu-ui/lib/icons/close-16.svg');

export class _AppEditInfo extends React.PureComponent<Props & {theme: ThemeVariables}, States>{

  fileInput: any;
  supportedResourceSuffix = {
    IMAGE: ['.png', '.jpeg', '.jpg', '.gif', '.bmp', '.svg', '.svgz']
  }

  constructor(props) {
    super(props);

    this.state = {
      localAppItem: Immutable(this.props.appItem),
      currentTitleInput: this.props.appItem.title,
      currentDescriptInput: this.props.appItem.description ? this.props.appItem.description : '',
      loading: false,
      files: ''
    }
    this.fileInput = React.createRef();
  }

  getStyle() {
    let theme = this.props.theme;
    let gray600 = theme.colors && theme.colors.palette ? theme.colors.palette.dark[300] : '#9fa4a7';
    let light300 = theme.colors && theme.colors.palette ? theme.colors.palette.light[300] : '#282828';
    let light800 = theme.colors && theme.colors.palette ? theme.colors.palette.light[800] : '#6a6a6a';
    let dark400 = theme.colors && theme.colors.palette ? theme.colors.palette.dark[400] : '#a8a8a8';
    return css`
      .app-editinfo {
        width: ${polished.rem(700)};
        height: ${polished.rem(420)};
        left:50%;
        top:50%;
        transform:translate3d(-50%,-60%,0);
        background: ${light300};
        border: 1px solid ${light800};
        .info-title {
          height: ${polished.rem(54)};
          border-bottom: 1px solid ${light800};
          font-weight:500;
        }

        .info-title-text {
          padding-left: ${polished.rem(30)};
          font-size: ${polished.rem(16)};
          color:${dark400};
        }

        .info-title-close {
          padding-right: ${polished.rem(30)};
          cursor: pointer;
        }

        .info-content {
          padding: ${polished.rem(40)} ${polished.rem(30)} 0;
          height: calc(100% - ${polished.rem(55)});

          .info-content-pic {
            width: ${polished.rem(240)};
            height: ${polished.rem(160)};
            background-size: cover;
            background-position: top center;
            margin-right: ${polished.rem(20)};
          }
          .edit-thumbnail-btn {
            width: ${polished.rem(240)};
            margin-top:${polished.rem(12)};
          }
          .edit-thumbnail-inp {
            bottom:0;
            left:0;
            width: ${polished.rem(240)};
            height: ${polished.rem(34)};
            opacity:0;
          }
          .info-content-btn-group {
            padding-top: ${polished.rem(36)};
          }

          .info-content-otherinfo {
            width: ${polished.rem(450)};

            .info-content-label {
              font-size: ${polished.rem(14)};
              color: ${gray600};
              font-weight: 600;
              margin-bottom: ${polished.rem(12)} !important;
            }

            .info-content-text {
              
            }

            .info-content-textarea {
              width: 100%;
              height: 100px;
              resize: none;
            }

            .info-content-btn {
              width: ${polished.rem(80)};
            }
          }
        }
      }`;
  }
  
  getFileFromBrowse = () => {

    let files = this.fileInput.current.files;
    if (!files || !files[0]) {
      return;
    }

    let fileName = files[0].name as string;
    let fileSuffix = '.' + fileName.split('.')[fileName.split('.').length - 1];
    if (this.supportedResourceSuffix.IMAGE.indexOf(fileSuffix.toLocaleLowerCase()) === -1) {
      return false;
    }

    let cacheBlob = window.URL.createObjectURL(files[0]);
    this.setState({
      files: cacheBlob
    });
    let localAppItem = {
      id: this.props.appItem.id,
      f: 'json',
      token: SessionManager.getInstance().getSessionByUrl(this.props.portalUrl).token,
      thumbnail: files[0],
    };

    this.setState({ loading: true });
    appServices.updateAppThumbnails(localAppItem).then(() => {
      this.setState({ 
        loading: false,
        files: cacheBlob 
      });
    }, err => {
      this.setState({ loading: false });
      console.error(err);
    })

    this.fileInput.current.value = '';
  }

  titleInputChange = () => {
    let localAppItem = Immutable(this.state.localAppItem);
    localAppItem = localAppItem.set('title', this.state.currentTitleInput);
    this.setState({
      localAppItem: localAppItem
    });
  }

  descriptInputChange = () => {
    let localAppItem = Immutable(this.state.localAppItem);
    localAppItem = localAppItem.set('description', this.state.currentDescriptInput);
    this.setState({
      localAppItem: localAppItem
    });
  }

  handleDescriptInputKeydown = (e: any) => {
    if (e.keyCode === 13) {
      this.descriptInputChange();
    } else {
      return;
    }
  }

  updateAppInfo = () => {
    this.setState({ loading: true });
    appServices.updateAppItem(this.state.localAppItem).then(() => {
      this.setState({ loading: false });
      // getAppStore().dispatch(builderActions.refreshAppListAction(true));
      this.props.onEditInfoOk();
    }, err => {
      this.setState({ loading: false });
      console.error(err);
    })
  }

  getThumbnailUrl = () => {
    let itemThumbnail = this.props.appItem.thumbnail;
    let thumbnail     = itemThumbnail;

    if (thumbnail) {
      thumbnail = this.props.portalUrl + '/sharing/rest/content/items/' + this.props.appItem.id + '/info/' 
        + thumbnail + '?token=' + SessionManager.getInstance().getSessionByUrl(this.props.portalUrl).token;
    } else {
      thumbnail = this.props.folderUrl + './dist/runtime/assets/defaultthumb.png';
    }

    if(itemThumbnail && window.isDevEdition){
      thumbnail = window.location.origin + '/apps/' + this.props.appItem.id + '/' + itemThumbnail;
    }
    return thumbnail;
  }

  render() {
    const thumbnail = this.getThumbnailUrl();
    const accept    = '.png, .jpeg, .jpg, .gif, .bmp, .svg, .svgz';
    let dark400 = this.props.theme.colors && this.props.theme.colors.palette ? this.props.theme.colors.palette.dark[400] : '#a8a8a8';

    return <div css={this.getStyle()} className="edit-info-con" onClick={(e) => {e.stopPropagation()}}>

      <div className="app-editinfo position-fixed">

        <div className="bg-light-300 info-title align-items-center d-flex justify-content-between">
          <div className="info-title-text">{this.props.intl ? 
            this.props.intl.formatMessage({id: 'editExperienceInfo', defaultMessage: defaultMessages.editExperienceInfo}) : defaultMessages.editExperienceInfo}
          </div>
          <div className="info-title-close" onClick={() => {this.props.onEditInfoCancel()}}>
            <Icon width={16} height={16} color={dark400} icon={IconClose}/>
          </div>
        </div>

        <div className="bg-light-300 info-content">
          <div className="d-flex justify-content-center">

            <div>
              <div className="info-content-pic" style={{backgroundImage: 'url(' + (this.state.files || thumbnail) + ')'}}></div>
              <div className="position-relative">
                <Button className="edit-thumbnail-btn" onClick={this.updateAppInfo}>
                  {this.props.intl ? this.props.intl.formatMessage({id: 'editThumbnail', defaultMessage: defaultMessages.editThumbnail}) : defaultMessages.editThumbnail}
                </Button>
                <input ref={this.fileInput} className="position-absolute edit-thumbnail-inp" type="file" accept={accept} onChange={this.getFileFromBrowse}></input>
              </div>
            </div>

            <div className="info-content-otherinfo">
              <div className="info-content-label">{this.props.intl ? 
                this.props.intl.formatMessage({id: 'name', defaultMessage: defaultMessages.name}) : defaultMessages.name}</div>
              <Input value={this.state.currentTitleInput} className="mb-3 info-title-input" 
                onChange={(event) => {this.setState({ currentTitleInput: event.target.value}); }}
                onBlur={() => {this.titleInputChange()}} onKeyUp={() => {this.titleInputChange()}}></Input>
              
              <div className="info-content-label">{this.props.intl ? 
                this.props.intl.formatMessage({id: 'description', defaultMessage: defaultMessages.description}) : defaultMessages.description}</div>
              <Input type="textarea" value={this.state.currentDescriptInput} className="info-content-textarea form-control mb-3"
                onChange={(event) => {this.setState({ currentDescriptInput: event.target.value}); }}
                onBlur={() => {this.descriptInputChange()}} onKeyDown ={ (e) => {this.handleDescriptInputKeydown(e)}}>
              </Input>

              <div className="d-flex justify-content-end info-content-btn-group">
                <Button type="primary" className="info-content-btn mr-2" onClick={this.updateAppInfo}>{this.props.intl ? 
                  this.props.intl.formatMessage({id: 'ok', defaultMessage: commonMessages.ok}) : commonMessages.ok}</Button>
                <Button className="info-content-btn" 
                  onClick={() => {this.props.onEditInfoCancel()}}>{this.props.intl ? 
                    this.props.intl.formatMessage({id: 'cancel', defaultMessage: commonMessages.cancel}) : commonMessages.cancel}
                </Button>
              </div>

            </div>

          </div>
        </div>
      </div>
      {this.state.loading && <div style={{ position: 'absolute', left: '50%', top: '50%' }} className="jimu-primary-loading"></div>}
    </div>;
  }
}

export const AppEditInfo = themeUtils.withTheme(_AppEditInfo);