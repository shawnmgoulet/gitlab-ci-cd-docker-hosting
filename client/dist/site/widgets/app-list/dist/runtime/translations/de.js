define({
  search: 'ä_Search_____________Ü',
  newApp: 'ä_Create New_____________________Ü',
  labelByModified: 'ä_Recent experiences___________________Ü',
  labelByTitle: 'ä_Experiences by title_____________________Ü',
  labelByView: 'ä_Experiences by views_____________________Ü',
  ownByMe: 'ä_Owned by me____________Ü',
  ownByAnyone: 'ä_Owned by anyone________________Ü',
  sharedWithMe: 'ä_Shared with me_______________Ü',
  orderByModified: 'ä_Last modified______________Ü',
  orderByTitle: 'ä_Title___________Ü',
  orderByView: 'ä_Most views_____________________Ü',
  views: 'ä_views___________Ü',
  duplicate: 'ä_Duplicate___________________Ü',
  editInfo: 'ä_Edit info___________________Ü',
  viewDetail: 'ä_View details_____________Ü',
  delete: 'ä_Delete_____________Ü',
  name: 'ä_Name_________Ü',
  description: 'ä_Description____________Ü',
  download: 'ä_Download_________________Ü',
  editExperienceInfo: 'ä_Edit experience info_____________________Ü',
  launchApp: 'ä_Launch_____________Ü',
  more: 'ä_More_________Ü',
  popUpOk: 'ä_OK_____Ü',
  popUpCancel: 'ä_Cancel_____________Ü',
  popUpTitle: 'ä_Warning_______________Ü',
  /* tslint:disable-next-line */
  popUpDiscription1: "ä_You don't have the permission to edit this web experience. Currently, you can only edit your own items_____________________________________________________Ü.",
  popUpDiscription2: 'ä_You can view or duplicate it______________________________Ü.',
  descriptionRemind: 'ä_Here is the description of the item____________________Ü. ',
  editThumbnail: 'ä_Edit thumbnail_______________Ü',
  itemDeleteRemind: 'ä_Are you sure you want to delete this item______________________Ü?',
  untitledExperience: 'ä_Untitled experience____________________Ü',
  webExperience: 'ä_Web Experience_______________Ü',
  webExperienceTemplate: 'ä_Web Experience Template________________________Ü',
  createExperience: 'ä_Create your first experience_____________________________Ü',
  toCreate: 'ä_to start a new experience__________________________Ü',
  click: 'ä_Click___________Ü'
});