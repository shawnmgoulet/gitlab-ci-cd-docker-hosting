define({
  search: 'æ_Search_____________Â',
  newApp: 'æ_Create New_____________________Â',
  labelByModified: 'æ_Recent experiences___________________Â',
  labelByTitle: 'æ_Experiences by title_____________________Â',
  labelByView: 'æ_Experiences by views_____________________Â',
  ownByMe: 'æ_Owned by me____________Â',
  ownByAnyone: 'æ_Owned by anyone________________Â',
  sharedWithMe: 'æ_Shared with me_______________Â',
  orderByModified: 'æ_Last modified______________Â',
  orderByTitle: 'æ_Title___________Â',
  orderByView: 'æ_Most views_____________________Â',
  views: 'æ_views___________Â',
  duplicate: 'æ_Duplicate___________________Â',
  editInfo: 'æ_Edit info___________________Â',
  viewDetail: 'æ_View details_____________Â',
  delete: 'æ_Delete_____________Â',
  name: 'æ_Name_________Â',
  description: 'æ_Description____________Â',
  download: 'æ_Download_________________Â',
  editExperienceInfo: 'æ_Edit experience info_____________________Â',
  launchApp: 'æ_Launch_____________Â',
  more: 'æ_More_________Â',
  popUpOk: 'æ_OK_____Â',
  popUpCancel: 'æ_Cancel_____________Â',
  popUpTitle: 'æ_Warning_______________Â',
  /* tslint:disable-next-line */
  popUpDiscription1: "æ_You don't have the permission to edit this web experience. Currently, you can only edit your own items_____________________________________________________Â.",
  popUpDiscription2: 'æ_You can view or duplicate it______________________________Â.',
  descriptionRemind: 'æ_Here is the description of the item____________________Â. ',
  editThumbnail: 'æ_Edit thumbnail_______________Â',
  itemDeleteRemind: 'æ_Are you sure you want to delete this item______________________Â?',
  untitledExperience: 'æ_Untitled experience____________________Â',
  webExperience: 'æ_Web Experience_______________Â',
  webExperienceTemplate: 'æ_Web Experience Template________________________Â',
  createExperience: 'æ_Create your first experience_____________________________Â',
  toCreate: 'æ_to start a new experience__________________________Â',
  click: 'æ_Click___________Â'
});