define({
  search: '須_Search_____________鷗',
  newApp: '須_Create New_____________________鷗',
  labelByModified: '須_Recent experiences___________________鷗',
  labelByTitle: '須_Experiences by title_____________________鷗',
  labelByView: '須_Experiences by views_____________________鷗',
  ownByMe: '須_Owned by me____________鷗',
  ownByAnyone: '須_Owned by anyone________________鷗',
  sharedWithMe: '須_Shared with me_______________鷗',
  orderByModified: '須_Last modified______________鷗',
  orderByTitle: '須_Title___________鷗',
  orderByView: '須_Most views_____________________鷗',
  views: '須_views___________鷗',
  duplicate: '須_Duplicate___________________鷗',
  editInfo: '須_Edit info___________________鷗',
  viewDetail: '須_View details_____________鷗',
  delete: '須_Delete_____________鷗',
  name: '須_Name_________鷗',
  description: '須_Description____________鷗',
  download: '須_Download_________________鷗',
  editExperienceInfo: '須_Edit experience info_____________________鷗',
  launchApp: '須_Launch_____________鷗',
  more: '須_More_________鷗',
  popUpOk: '須_OK_____鷗',
  popUpCancel: '須_Cancel_____________鷗',
  popUpTitle: '須_Warning_______________鷗',
  /* tslint:disable-next-line */
  popUpDiscription1: "須_You don't have the permission to edit this web experience. Currently, you can only edit your own items_____________________________________________________鷗.",
  popUpDiscription2: '須_You can view or duplicate it______________________________鷗.',
  descriptionRemind: '須_Here is the description of the item____________________鷗. ',
  editThumbnail: '須_Edit thumbnail_______________鷗',
  itemDeleteRemind: '須_Are you sure you want to delete this item______________________鷗?',
  untitledExperience: '須_Untitled experience____________________鷗',
  webExperience: '須_Web Experience_______________鷗',
  webExperienceTemplate: '須_Web Experience Template________________________鷗',
  createExperience: '須_Create your first experience_____________________________鷗',
  toCreate: '須_to start a new experience__________________________鷗',
  click: '須_Click___________鷗'
});