export default {
  createNew: 'Create New',
  newExperience: 'New experience',
  signOut:  'Sign out',
  signIn:  'Sign in',
  experiences: 'Experiences',
  templates: 'Templates'
}
