define({
  signIn: 'ķ_Sign In_______________ū',
  portalUrl: 'ķ_Portal URL_____________________ū',
  clientId: 'ķ_Client ID___________________ū',
  example: 'ķ_Example___________________ū: ',
  portalUrlDescription: 'ķ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ū',
  clientIdDescription: 'ķ_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ū',
  sampleOrgUrl: 'ķ_http://myorg.maps.arcgis.com_____________________________ū',
  samplePortalUrl: 'ķ_http://myportal.company.com/arcgis__________________ū',
  invalidPortalUrlMessage: 'ķ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ū,',
  unableToAccessMessage: 'ķ_Unable to access_________________ū',
  invalidHostNameMessage: 'ķ_A server with the specified hostname could not be found_____________________________ū.'
});