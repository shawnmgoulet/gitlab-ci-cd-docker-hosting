define({
  signIn: 'á_Sign In_______________Ó',
  portalUrl: 'á_Portal URL_____________________Ó',
  clientId: 'á_Client ID___________________Ó',
  example: 'á_Example___________________Ó: ',
  portalUrlDescription: 'á_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Ó',
  clientIdDescription: 'á_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Ó',
  sampleOrgUrl: 'á_http://myorg.maps.arcgis.com_____________________________Ó',
  samplePortalUrl: 'á_http://myportal.company.com/arcgis__________________Ó',
  invalidPortalUrlMessage: 'á_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Ó,',
  unableToAccessMessage: 'á_Unable to access_________________Ó',
  invalidHostNameMessage: 'á_A server with the specified hostname could not be found_____________________________Ó.'
});