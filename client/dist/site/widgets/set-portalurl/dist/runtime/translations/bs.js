define({
  signIn: 'Č_Sign In_______________ž',
  portalUrl: 'Č_Portal URL_____________________ž',
  clientId: 'Č_Client ID___________________ž',
  example: 'Č_Example___________________ž: ',
  portalUrlDescription: 'Č_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ž',
  clientIdDescription: 'Č_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ž',
  sampleOrgUrl: 'Č_http://myorg.maps.arcgis.com_____________________________ž',
  samplePortalUrl: 'Č_http://myportal.company.com/arcgis__________________ž',
  invalidPortalUrlMessage: 'Č_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ž,',
  unableToAccessMessage: 'Č_Unable to access_________________ž',
  invalidHostNameMessage: 'Č_A server with the specified hostname could not be found_____________________________ž.'
});