define({
  signIn: 'ã_Sign In_______________Ç',
  portalUrl: 'ã_Portal URL_____________________Ç',
  clientId: 'ã_Client ID___________________Ç',
  example: 'ã_Example___________________Ç: ',
  portalUrlDescription: 'ã_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Ç',
  clientIdDescription: 'ã_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Ç',
  sampleOrgUrl: 'ã_http://myorg.maps.arcgis.com_____________________________Ç',
  samplePortalUrl: 'ã_http://myportal.company.com/arcgis__________________Ç',
  invalidPortalUrlMessage: 'ã_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Ç,',
  unableToAccessMessage: 'ã_Unable to access_________________Ç',
  invalidHostNameMessage: 'ã_A server with the specified hostname could not be found_____________________________Ç.'
});