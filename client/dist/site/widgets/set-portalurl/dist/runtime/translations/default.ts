export default {
  signIn: 'Sign In',
  portalUrl: 'Portal URL',
  clientId: 'Client ID',
  example: 'Example: ',
  portalUrlDescription: 'Specify the URL to your ArcGIS Online organization or Portal for ArcGIS',
  clientIdDescription: 'Provide the Client ID for Experience Builder registered in the portal you specify above',
  sampleOrgUrl: 'http://myorg.maps.arcgis.com',
  samplePortalUrl: 'http://myportal.company.com/arcgis',
  invalidPortalUrlMessage: 'Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example,',
  unableToAccessMessage: 'Unable to access',
  invalidHostNameMessage: 'A server with the specified hostname could not be found.'
}
