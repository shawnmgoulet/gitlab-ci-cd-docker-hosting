define({
  signIn: 'Ĳ_Sign In_______________ä',
  portalUrl: 'Ĳ_Portal URL_____________________ä',
  clientId: 'Ĳ_Client ID___________________ä',
  example: 'Ĳ_Example___________________ä: ',
  portalUrlDescription: 'Ĳ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ä',
  clientIdDescription: 'Ĳ_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ä',
  sampleOrgUrl: 'Ĳ_http://myorg.maps.arcgis.com_____________________________ä',
  samplePortalUrl: 'Ĳ_http://myportal.company.com/arcgis__________________ä',
  invalidPortalUrlMessage: 'Ĳ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ä,',
  unableToAccessMessage: 'Ĳ_Unable to access_________________ä',
  invalidHostNameMessage: 'Ĳ_A server with the specified hostname could not be found_____________________________ä.'
});