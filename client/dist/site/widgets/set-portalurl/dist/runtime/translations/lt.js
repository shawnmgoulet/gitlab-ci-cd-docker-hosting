define({
  signIn: 'Į_Sign In_______________š',
  portalUrl: 'Į_Portal URL_____________________š',
  clientId: 'Į_Client ID___________________š',
  example: 'Į_Example___________________š: ',
  portalUrlDescription: 'Į_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________š',
  clientIdDescription: 'Į_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________š',
  sampleOrgUrl: 'Į_http://myorg.maps.arcgis.com_____________________________š',
  samplePortalUrl: 'Į_http://myportal.company.com/arcgis__________________š',
  invalidPortalUrlMessage: 'Į_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________š,',
  unableToAccessMessage: 'Į_Unable to access_________________š',
  invalidHostNameMessage: 'Į_A server with the specified hostname could not be found_____________________________š.'
});