define({
  signIn: 'כן_Sign In_______________ש',
  portalUrl: 'כן_Portal URL_____________________ש',
  clientId: 'כן_Client ID___________________ש',
  example: 'כן_Example___________________ש: ',
  portalUrlDescription: 'כן_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ש',
  clientIdDescription: 'כן_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ש',
  sampleOrgUrl: 'כן_http://myorg.maps.arcgis.com_____________________________ש',
  samplePortalUrl: 'כן_http://myportal.company.com/arcgis__________________ש',
  invalidPortalUrlMessage: 'כן_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ש,',
  unableToAccessMessage: 'כן_Unable to access_________________ש',
  invalidHostNameMessage: 'כן_A server with the specified hostname could not be found_____________________________ש.'
});