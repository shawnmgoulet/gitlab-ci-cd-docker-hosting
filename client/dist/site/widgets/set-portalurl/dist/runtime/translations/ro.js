define({
  signIn: 'Ă_Sign In_______________ș',
  portalUrl: 'Ă_Portal URL_____________________ș',
  clientId: 'Ă_Client ID___________________ș',
  example: 'Ă_Example___________________ș: ',
  portalUrlDescription: 'Ă_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ș',
  clientIdDescription: 'Ă_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ș',
  sampleOrgUrl: 'Ă_http://myorg.maps.arcgis.com_____________________________ș',
  samplePortalUrl: 'Ă_http://myportal.company.com/arcgis__________________ș',
  invalidPortalUrlMessage: 'Ă_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ș,',
  unableToAccessMessage: 'Ă_Unable to access_________________ș',
  invalidHostNameMessage: 'Ă_A server with the specified hostname could not be found_____________________________ș.'
});