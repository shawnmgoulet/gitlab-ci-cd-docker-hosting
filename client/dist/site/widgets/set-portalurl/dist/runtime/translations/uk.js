define({
  signIn: 'ґ_Sign In_______________Ї',
  portalUrl: 'ґ_Portal URL_____________________Ї',
  clientId: 'ґ_Client ID___________________Ї',
  example: 'ґ_Example___________________Ї: ',
  portalUrlDescription: 'ґ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Ї',
  clientIdDescription: 'ґ_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Ї',
  sampleOrgUrl: 'ґ_http://myorg.maps.arcgis.com_____________________________Ї',
  samplePortalUrl: 'ґ_http://myportal.company.com/arcgis__________________Ї',
  invalidPortalUrlMessage: 'ґ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Ї,',
  unableToAccessMessage: 'ґ_Unable to access_________________Ї',
  invalidHostNameMessage: 'ґ_A server with the specified hostname could not be found_____________________________Ї.'
});