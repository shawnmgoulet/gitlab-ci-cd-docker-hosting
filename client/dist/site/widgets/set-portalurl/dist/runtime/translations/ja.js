define({
  signIn: '須_Sign In_______________鷗',
  portalUrl: '須_Portal URL_____________________鷗',
  clientId: '須_Client ID___________________鷗',
  example: '須_Example___________________鷗: ',
  portalUrlDescription: '須_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________鷗',
  clientIdDescription: '須_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________鷗',
  sampleOrgUrl: '須_http://myorg.maps.arcgis.com_____________________________鷗',
  samplePortalUrl: '須_http://myportal.company.com/arcgis__________________鷗',
  invalidPortalUrlMessage: '須_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________鷗,',
  unableToAccessMessage: '須_Unable to access_________________鷗',
  invalidHostNameMessage: '須_A server with the specified hostname could not be found_____________________________鷗.'
});