define({
  signIn: 'Š_Sign In_______________ä',
  portalUrl: 'Š_Portal URL_____________________ä',
  clientId: 'Š_Client ID___________________ä',
  example: 'Š_Example___________________ä: ',
  portalUrlDescription: 'Š_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ä',
  clientIdDescription: 'Š_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ä',
  sampleOrgUrl: 'Š_http://myorg.maps.arcgis.com_____________________________ä',
  samplePortalUrl: 'Š_http://myportal.company.com/arcgis__________________ä',
  invalidPortalUrlMessage: 'Š_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ä,',
  unableToAccessMessage: 'Š_Unable to access_________________ä',
  invalidHostNameMessage: 'Š_A server with the specified hostname could not be found_____________________________ä.'
});