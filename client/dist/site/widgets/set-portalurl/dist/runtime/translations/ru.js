define({
  signIn: 'Ж_Sign In_______________Я',
  portalUrl: 'Ж_Portal URL_____________________Я',
  clientId: 'Ж_Client ID___________________Я',
  example: 'Ж_Example___________________Я: ',
  portalUrlDescription: 'Ж_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Я',
  clientIdDescription: 'Ж_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Я',
  sampleOrgUrl: 'Ж_http://myorg.maps.arcgis.com_____________________________Я',
  samplePortalUrl: 'Ж_http://myportal.company.com/arcgis__________________Я',
  invalidPortalUrlMessage: 'Ж_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Я,',
  unableToAccessMessage: 'Ж_Unable to access_________________Я',
  invalidHostNameMessage: 'Ж_A server with the specified hostname could not be found_____________________________Я.'
});