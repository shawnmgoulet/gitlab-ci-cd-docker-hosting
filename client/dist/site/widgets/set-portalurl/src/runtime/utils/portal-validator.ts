//import { request } from '@esri/arcgis-rest-request';
import { ICredential } from '@esri/arcgis-rest-auth';
import { portalUrlUtils, urlUtils, UrlProtocol } from 'jimu-core';
import fetchJsonp from 'fetch-jsonp-es6';

export enum InvalidMessage {
  InvalidPortalUrl = 'InvalidPortalUrl',
  UnableToAccess = 'UnableToAccess'
}

/*
export interface TokenInfo {
  token: string,
  ssl: boolean,
  expires: number,
  server: string,
  userId: string;
}
 */

export interface PortalInfo {
  isValid: boolean,
  invalidMessage: string,
  isWebTier: boolean,
  supportsOAuth: boolean,
  tokenInfo: ICredential,
  url: string;
}

export class PortalValidator {
  private validPortalInfos: PortalInfo[];

  constructor(){
    this.validPortalInfos = [];
  }

  public getDefaultProtalInfo(portalUrl?: string) {
    return {
      isValid: false,
      invalidMessage: '',  
      isWebTier: false,
      supportsOAuth: true,
      tokenInfo: {
        token: null,
        expires: null,
        ssl: false,
        server: '',
        userId: ''
      },
      url: this.getStandardPortalUrl(portalUrl)
    };
  }

  private getValidPortalInfo(portalUrl: string) {
    let resultPortalInfo = null;
    this.validPortalInfos.some( (portalInfo) => {
      if(portalUrlUtils.isSamePortalUrl(portalUrl, portalInfo.url)) {
        resultPortalInfo = portalInfo;
        //// udpate portalUrl protocol with new portalUrl.
        //resultPortalInfo.url = this.getStandardPortalUrl(portalUrl);
        return true;
      } else {
        return false;
      }
    });
    return resultPortalInfo;
  }

  private getStandardPortalUrl(portalUrl: string): string {
    if(!portalUrl) {
      return '';
    }
    //if user doesn't input protocol for portalUrl, we should use http protocol as default
    //rather than browser's protocol
    let isHttpProtocol = portalUrl.toLowerCase().indexOf('http://') === 0;
    let isHttpsProtocol = portalUrl.toLowerCase().indexOf('https://') === 0;
    if(!isHttpProtocol && !isHttpsProtocol) {
      portalUrl = urlUtils.setProtocol(portalUrl, UrlProtocol.Https);
    }

    // portalUrlUtils will change portalUrl to location's protocol.
    portalUrl = portalUrlUtils.getStandardPortalUrl(portalUrl);
    //if(!isHttpsProtocol) {
    //  portalUrl = urlUtils.setProtocol(portalUrl, UrlProtocol.Http);
    //}
    portalUrl = urlUtils.setProtocol(portalUrl, UrlProtocol.Https);
    return portalUrl;
  } 

  private validatePortalUrl(portalUrl: string): PortalInfo {
    let portalInfo = this.getDefaultProtalInfo(portalUrl);

    /*
    // Doesn't allow a named host for local networks or localhost
    // must be organization account
    if ((/www.arcgis.com/gi).test(portalUrl) || !this._allowNamedorLocal(portalUrl)) {
      return;
    }
    */
    if ((/www.arcgis.com/gi).test(portalUrl)) {
      portalInfo.isValid = false;
      portalInfo.invalidMessage = InvalidMessage.InvalidPortalUrl;
    } else {
      portalInfo.isValid = true;
    }
    return portalInfo;
  }

  private generateToken(portalUrl: string): Promise<any> {
    // before portal 10.3 doesn't support GET method with 'rest'
    var generateTokenUrl = urlUtils.setProtocol(`${portalUrlUtils.getPortalRestUrl(portalUrl)}generateToken?f=json`, UrlProtocol.Https);
    //@ts-ignore
    return fetchJsonp(generateTokenUrl, {
      jsonpCallback: 'callback',
      timeout: 5000
    }).then( (response)  => {
      return response.json();
    }).then( (tokenInfo) => {
      //if web-tier(iwa/pki), the response is {expires,ssl,token}
      //else the response is :
      // {
      //   "error": {
      //     "code": 400,
      //     "message": "Unable to generate token.",
      //     "details": ["'username' must be specified.",
      //                 "'password' must be specified.",
      //                 "'referer' must be specified."]
      //   }
      // }
      return tokenInfo;
    }, (error) => {
      return null;
    });
  }

  private getPortalSelfInfo(portalUrl: string): Promise<any> {
    // Only JSONP request can caulse browser to popup signIn dialog for webTier portal. 
    var portalSelefUrl = urlUtils.setProtocol(`${portalUrlUtils.getPortalRestUrl(portalUrl)}portals/self?f=json`, UrlProtocol.Https);
    //@ts-ignore
    return fetchJsonp(portalSelefUrl, {
      jsonpCallback: 'callback',
      timeout: 5000
    }).then( (response) => {
      return response.json();
    }).then( (portalSelfInfo) => {
      return portalSelfInfo;
    }).catch( (error) => {
      return null;
    });
  }

  private getPortalInfo(portalUrl: string): Promise<PortalInfo>{
    let portalInfo = this.getDefaultProtalInfo(portalUrl);
    return this.getPortalSelfInfo(portalUrl).then( (portalSelfInfo) => {
      if(!portalSelfInfo || (!portalSelfInfo.isPortal && !portalSelfInfo.urlKey)) {
        portalInfo.isValid = false;
        portalInfo.invalidMessage = InvalidMessage.UnableToAccess;
        return;
      } else {
        portalInfo.isValid = true;
        portalInfo.supportsOAuth = portalSelfInfo.supportsOAuth;
        // Only webTier portal be able to get user info at this time.
        portalInfo.tokenInfo.userId = portalSelfInfo.user ? portalSelfInfo.user.username : '';
        this.validPortalInfos.push(portalInfo);
        return this.generateToken(portalUrl);
      }
    }).then( (tokenInfo) => {
      if(tokenInfo && tokenInfo.token) {
        // Is webTier portal.
        portalInfo.tokenInfo.token = tokenInfo.token;
        portalInfo.tokenInfo.expires = tokenInfo.expires;
        portalInfo.tokenInfo.ssl = tokenInfo.ssl;
        portalInfo.tokenInfo.server = portalUrlUtils.getPortalRestUrl(portalUrl);

        portalInfo.isWebTier = true;
        portalInfo.supportsOAuth = false;
      }
      return portalInfo;
    });
  }

  validatePortal(portalUrlParam: string): Promise<PortalInfo> {
    if(!portalUrlParam) {
      return Promise.reject('Portal URL is empty.');
    }
    let resultPromise;
    let portalUrl = this.getStandardPortalUrl(portalUrlParam);
    let portalInfo = this.getValidPortalInfo(portalUrl);
    if(portalInfo) {
      resultPromise = Promise.resolve(portalInfo);
    } else {
      portalInfo = this.validatePortalUrl(portalUrl);
      if(!portalInfo.isValid) {
        resultPromise = Promise.resolve(portalInfo);
      } else {
        resultPromise = this.getPortalInfo(portalUrl); 
      }
    }
    return resultPromise;
  }
}

