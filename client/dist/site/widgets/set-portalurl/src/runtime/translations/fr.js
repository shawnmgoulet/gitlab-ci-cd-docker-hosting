define({
  signIn: 'æ_Sign In_______________Â',
  portalUrl: 'æ_Portal URL_____________________Â',
  clientId: 'æ_Client ID___________________Â',
  example: 'æ_Example___________________Â: ',
  portalUrlDescription: 'æ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Â',
  clientIdDescription: 'æ_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Â',
  sampleOrgUrl: 'æ_http://myorg.maps.arcgis.com_____________________________Â',
  samplePortalUrl: 'æ_http://myportal.company.com/arcgis__________________Â',
  invalidPortalUrlMessage: 'æ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Â,',
  unableToAccessMessage: 'æ_Unable to access_________________Â',
  invalidHostNameMessage: 'æ_A server with the specified hostname could not be found_____________________________Â.'
});