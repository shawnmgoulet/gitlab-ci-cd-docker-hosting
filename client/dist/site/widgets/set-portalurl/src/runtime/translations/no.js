export default {
  signIn: 'å_Sign In_______________ø',
  portalUrl: 'å_Portal URL_____________________ø',
  clientId: 'å_Client ID___________________ø',
  example: 'å_Example___________________ø: ',
  portalUrlDescription: 'å_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ø',
  clientIdDescription: 'å_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ø',
  sampleOrgUrl: 'å_http://myorg.maps.arcgis.com_____________________________ø',
  samplePortalUrl: 'å_http://myportal.company.com/arcgis__________________ø',
  invalidPortalUrlMessage: 'å_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ø,',
  unableToAccessMessage: 'å_Unable to access_________________ø',
  invalidHostNameMessage: 'å_A server with the specified hostname could not be found_____________________________ø.'
}
