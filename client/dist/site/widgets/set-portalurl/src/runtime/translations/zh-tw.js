define({
  signIn: '試_Sign In_______________驗',
  portalUrl: '試_Portal URL_____________________驗',
  clientId: '試_Client ID___________________驗',
  example: '試_Example___________________驗: ',
  portalUrlDescription: '試_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________驗',
  clientIdDescription: '試_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________驗',
  sampleOrgUrl: '試_http://myorg.maps.arcgis.com_____________________________驗',
  samplePortalUrl: '試_http://myportal.company.com/arcgis__________________驗',
  invalidPortalUrlMessage: '試_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________驗,',
  unableToAccessMessage: '試_Unable to access_________________驗',
  invalidHostNameMessage: '試_A server with the specified hostname could not be found_____________________________驗.'
});