define({
  signIn: 'ó_Sign In_______________à',
  portalUrl: 'ó_Portal URL_____________________à',
  clientId: 'ó_Client ID___________________à',
  example: 'ó_Example___________________à: ',
  portalUrlDescription: 'ó_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________à',
  clientIdDescription: 'ó_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________à',
  sampleOrgUrl: 'ó_http://myorg.maps.arcgis.com_____________________________à',
  samplePortalUrl: 'ó_http://myportal.company.com/arcgis__________________à',
  invalidPortalUrlMessage: 'ó_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________à,',
  unableToAccessMessage: 'ó_Unable to access_________________à',
  invalidHostNameMessage: 'ó_A server with the specified hostname could not be found_____________________________à.'
});