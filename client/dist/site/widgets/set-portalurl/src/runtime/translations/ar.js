define({
  signIn: 'بيت_Sign In_______________لاحقة',
  portalUrl: 'بيت_Portal URL_____________________لاحقة',
  clientId: 'بيت_Client ID___________________لاحقة',
  example: 'بيت_Example___________________لاحقة: ',
  portalUrlDescription: 'بيت_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________لاحقة',
  clientIdDescription: 'بيت_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________لاحقة',
  sampleOrgUrl: 'بيت_http://myorg.maps.arcgis.com_____________________________لاحقة',
  samplePortalUrl: 'بيت_http://myportal.company.com/arcgis__________________لاحقة',
  invalidPortalUrlMessage: 'بيت_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________لاحقة,',
  unableToAccessMessage: 'بيت_Unable to access_________________لاحقة',
  invalidHostNameMessage: 'بيت_A server with the specified hostname could not be found_____________________________لاحقة.'
});