define({
  signIn: 'आ_Sign In_______________ज',
  portalUrl: 'आ_Portal URL_____________________ज',
  clientId: 'आ_Client ID___________________ज',
  example: 'आ_Example___________________ज: ',
  portalUrlDescription: 'आ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ज',
  clientIdDescription: 'आ_Provide the App ID for Experience Builder registered in the portal you specify above___________________________________________ज',
  sampleOrgUrl: 'आ_http://myorg.maps.arcgis.com_____________________________ज',
  samplePortalUrl: 'आ_http://myportal.company.com/arcgis__________________ज',
  invalidPortalUrlMessage: 'आ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ज,',
  unableToAccessMessage: 'आ_Unable to access_________________ज',
  invalidHostNameMessage: 'आ_A server with the specified hostname could not be found_____________________________ज.'
});