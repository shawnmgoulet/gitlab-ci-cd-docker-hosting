define({
  signIn: 'ng_Sign In_______________ny',
  portalUrl: 'ng_Portal URL_____________________ny',
  clientId: 'ng_Client ID___________________ny',
  example: 'ng_Example___________________ny: ',
  portalUrlDescription: 'ng_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ny',
  clientIdDescription: 'ng_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ny',
  sampleOrgUrl: 'ng_http://myorg.maps.arcgis.com_____________________________ny',
  samplePortalUrl: 'ng_http://myportal.company.com/arcgis__________________ny',
  invalidPortalUrlMessage: 'ng_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ny,',
  unableToAccessMessage: 'ng_Unable to access_________________ny',
  invalidHostNameMessage: 'ng_A server with the specified hostname could not be found_____________________________ny.'
});