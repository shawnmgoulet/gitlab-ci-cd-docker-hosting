define({
  signIn: 'Å_Sign In_______________ö',
  portalUrl: 'Å_Portal URL_____________________ö',
  clientId: 'Å_Client ID___________________ö',
  example: 'Å_Example___________________ö: ',
  portalUrlDescription: 'Å_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ö',
  clientIdDescription: 'Å_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ö',
  sampleOrgUrl: 'Å_http://myorg.maps.arcgis.com_____________________________ö',
  samplePortalUrl: 'Å_http://myportal.company.com/arcgis__________________ö',
  invalidPortalUrlMessage: 'Å_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ö,',
  unableToAccessMessage: 'Å_Unable to access_________________ö',
  invalidHostNameMessage: 'Å_A server with the specified hostname could not be found_____________________________ö.'
});