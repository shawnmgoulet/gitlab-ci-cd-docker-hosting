define({
  signIn: 'é_Sign In_______________È',
  portalUrl: 'é_Portal URL_____________________È',
  clientId: 'é_Client ID___________________È',
  example: 'é_Example___________________È: ',
  portalUrlDescription: 'é_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________È',
  clientIdDescription: 'é_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________È',
  sampleOrgUrl: 'é_http://myorg.maps.arcgis.com_____________________________È',
  samplePortalUrl: 'é_http://myportal.company.com/arcgis__________________È',
  invalidPortalUrlMessage: 'é_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________È,',
  unableToAccessMessage: 'é_Unable to access_________________È',
  invalidHostNameMessage: 'é_A server with the specified hostname could not be found_____________________________È.'
});