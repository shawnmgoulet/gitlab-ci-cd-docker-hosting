define({
  signIn: 'ı_Sign In_______________İ',
  portalUrl: 'ı_Portal URL_____________________İ',
  clientId: 'ı_Client ID___________________İ',
  example: 'ı_Example___________________İ: ',
  portalUrlDescription: 'ı_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________İ',
  clientIdDescription: 'ı_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________İ',
  sampleOrgUrl: 'ı_http://myorg.maps.arcgis.com_____________________________İ',
  samplePortalUrl: 'ı_http://myportal.company.com/arcgis__________________İ',
  invalidPortalUrlMessage: 'ı_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________İ,',
  unableToAccessMessage: 'ı_Unable to access_________________İ',
  invalidHostNameMessage: 'ı_A server with the specified hostname could not be found_____________________________İ.'
});