define({
  signIn: 'Ř_Sign In_______________ů',
  portalUrl: 'Ř_Portal URL_____________________ů',
  clientId: 'Ř_Client ID___________________ů',
  example: 'Ř_Example___________________ů: ',
  portalUrlDescription: 'Ř_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ů',
  clientIdDescription: 'Ř_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ů',
  sampleOrgUrl: 'Ř_http://myorg.maps.arcgis.com_____________________________ů',
  samplePortalUrl: 'Ř_http://myportal.company.com/arcgis__________________ů',
  invalidPortalUrlMessage: 'Ř_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ů,',
  unableToAccessMessage: 'Ř_Unable to access_________________ů',
  invalidHostNameMessage: 'Ř_A server with the specified hostname could not be found_____________________________ů.'
});