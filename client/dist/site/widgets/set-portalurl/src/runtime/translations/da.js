define({
  signIn: 'ø_Sign In_______________å',
  portalUrl: 'ø_Portal URL_____________________å',
  clientId: 'ø_Client ID___________________å',
  example: 'ø_Example___________________å: ',
  portalUrlDescription: 'ø_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________å',
  clientIdDescription: 'ø_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________å',
  sampleOrgUrl: 'ø_http://myorg.maps.arcgis.com_____________________________å',
  samplePortalUrl: 'ø_http://myportal.company.com/arcgis__________________å',
  invalidPortalUrlMessage: 'ø_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________å,',
  unableToAccessMessage: 'ø_Unable to access_________________å',
  invalidHostNameMessage: 'ø_A server with the specified hostname could not be found_____________________________å.'
});