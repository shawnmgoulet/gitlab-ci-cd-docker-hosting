define({
  signIn: 'ł_Sign In_______________ą',
  portalUrl: 'ł_Portal URL_____________________ą',
  clientId: 'ł_Client ID___________________ą',
  example: 'ł_Example___________________ą: ',
  portalUrlDescription: 'ł_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ą',
  clientIdDescription: 'ł_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ą',
  sampleOrgUrl: 'ł_http://myorg.maps.arcgis.com_____________________________ą',
  samplePortalUrl: 'ł_http://myportal.company.com/arcgis__________________ą',
  invalidPortalUrlMessage: 'ł_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ą,',
  unableToAccessMessage: 'ł_Unable to access_________________ą',
  invalidHostNameMessage: 'ł_A server with the specified hostname could not be found_____________________________ą.'
});