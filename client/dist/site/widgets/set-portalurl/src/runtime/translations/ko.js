define({
  signIn: '한_Sign In_______________빠',
  portalUrl: '한_Portal URL_____________________빠',
  clientId: '한_Client ID___________________빠',
  example: '한_Example___________________빠: ',
  portalUrlDescription: '한_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________빠',
  clientIdDescription: '한_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________빠',
  sampleOrgUrl: '한_http://myorg.maps.arcgis.com_____________________________빠',
  samplePortalUrl: '한_http://myportal.company.com/arcgis__________________빠',
  invalidPortalUrlMessage: '한_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________빠,',
  unableToAccessMessage: '한_Unable to access_________________빠',
  invalidHostNameMessage: '한_A server with the specified hostname could not be found_____________________________빠.'
});