define({
  signIn: 'ä_Sign In_______________Ü',
  portalUrl: 'ä_Portal URL_____________________Ü',
  clientId: 'ä_Client ID___________________Ü',
  example: 'ä_Example___________________Ü: ',
  portalUrlDescription: 'ä_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________Ü',
  clientIdDescription: 'ä_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________Ü',
  sampleOrgUrl: 'ä_http://myorg.maps.arcgis.com_____________________________Ü',
  samplePortalUrl: 'ä_http://myportal.company.com/arcgis__________________Ü',
  invalidPortalUrlMessage: 'ä_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________Ü,',
  unableToAccessMessage: 'ä_Unable to access_________________Ü',
  invalidHostNameMessage: 'ä_A server with the specified hostname could not be found_____________________________Ü.'
});