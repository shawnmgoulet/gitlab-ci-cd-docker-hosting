define({
  signIn: 'í_Sign In_______________ő',
  portalUrl: 'í_Portal URL_____________________ő',
  clientId: 'í_Client ID___________________ő',
  example: 'í_Example___________________ő: ',
  portalUrlDescription: 'í_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ő',
  clientIdDescription: 'í_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ő',
  sampleOrgUrl: 'í_http://myorg.maps.arcgis.com_____________________________ő',
  samplePortalUrl: 'í_http://myportal.company.com/arcgis__________________ő',
  invalidPortalUrlMessage: 'í_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ő,',
  unableToAccessMessage: 'í_Unable to access_________________ő',
  invalidHostNameMessage: 'í_A server with the specified hostname could not be found_____________________________ő.'
});