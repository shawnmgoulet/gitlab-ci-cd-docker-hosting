define({
  signIn: 'ก้_Sign In_______________ษฺ',
  portalUrl: 'ก้_Portal URL_____________________ษฺ',
  clientId: 'ก้_Client ID___________________ษฺ',
  example: 'ก้_Example___________________ษฺ: ',
  portalUrlDescription: 'ก้_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ษฺ',
  clientIdDescription: 'ก้_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ษฺ',
  sampleOrgUrl: 'ก้_http://myorg.maps.arcgis.com_____________________________ษฺ',
  samplePortalUrl: 'ก้_http://myportal.company.com/arcgis__________________ษฺ',
  invalidPortalUrlMessage: 'ก้_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ษฺ,',
  unableToAccessMessage: 'ก้_Unable to access_________________ษฺ',
  invalidHostNameMessage: 'ก้_A server with the specified hostname could not be found_____________________________ษฺ.'
});