define({
  signIn: 'Š_Sign In_______________č',
  portalUrl: 'Š_Portal URL_____________________č',
  clientId: 'Š_Client ID___________________č',
  example: 'Š_Example___________________č: ',
  portalUrlDescription: 'Š_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________č',
  clientIdDescription: 'Š_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________č',
  sampleOrgUrl: 'Š_http://myorg.maps.arcgis.com_____________________________č',
  samplePortalUrl: 'Š_http://myportal.company.com/arcgis__________________č',
  invalidPortalUrlMessage: 'Š_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________č,',
  unableToAccessMessage: 'Š_Unable to access_________________č',
  invalidHostNameMessage: 'Š_A server with the specified hostname could not be found_____________________________č.'
});