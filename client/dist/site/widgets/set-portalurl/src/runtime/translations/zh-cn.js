define({
  signIn: '试_Sign In_______________验',
  portalUrl: '试_Portal URL_____________________验',
  clientId: '试_Client ID___________________验',
  example: '试_Example___________________验: ',
  portalUrlDescription: '试_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________验',
  clientIdDescription: '试_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________验',
  sampleOrgUrl: '试_http://myorg.maps.arcgis.com_____________________________验',
  samplePortalUrl: '试_http://myportal.company.com/arcgis__________________验',
  invalidPortalUrlMessage: '试_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________验,',
  unableToAccessMessage: '试_Unable to access_________________验',
  invalidHostNameMessage: '试_A server with the specified hostname could not be found_____________________________验.'
});