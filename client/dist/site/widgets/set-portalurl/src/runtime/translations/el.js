define({
  signIn: 'Đ_Sign In_______________ớ',
  portalUrl: 'Đ_Portal URL_____________________ớ',
  clientId: 'Đ_Client ID___________________ớ',
  example: 'Đ_Example___________________ớ: ',
  portalUrlDescription: 'Đ_Specify the URL to your ArcGIS Online organization or Portal for ArcGIS_____________________________________ớ',
  clientIdDescription: 'Đ_Provide the Client ID for Experience Builder registered in the portal you specify above_____________________________________________ớ',
  sampleOrgUrl: 'Đ_http://myorg.maps.arcgis.com_____________________________ớ',
  samplePortalUrl: 'Đ_http://myportal.company.com/arcgis__________________ớ',
  invalidPortalUrlMessage: 'Đ_Please enter the full URL of your ArcGIS Online organization or Portal for ArcGIS, for example_________________________________________________ớ,',
  unableToAccessMessage: 'Đ_Unable to access_________________ớ',
  invalidHostNameMessage: 'Đ_A server with the specified hostname could not be found_____________________________ớ.'
});