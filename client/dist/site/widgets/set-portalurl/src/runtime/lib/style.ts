import { ThemeVariables, css, SerializedStyles } from 'jimu-core';
export function getStyle(theme: ThemeVariables): SerializedStyles {
  return css`
    width: 700px;
    margin: 0 auto;
    .portal-clientId.hidden{
      display: none;
    }
    .loading-panel {
      position: absolute;
      width: 15px;
      background-color: ${theme.colors.light};

      .jimu-small-loading {
        left: 2px;
        top: 12px;
      }
      
      .jimu-small-loading, .jimu-small-loading:before, .jimu-small-loading:after {
        width: 2px;
        height: 5px;
        left: -27px;
      }
      .jimu-small-loading:before {
        left: -5px;
      }
      .jimu-small-loading:after {
        left: 5px;
      }
    }

    .loading-panel.hidden {
      display: none;
    }

    .dropdown-menu {
      width: 702px;
      left: -603px !important;
    }

    .description, .message {
      color: ${theme.colors.palette.dark[600]};
    }

    .invalid-message {
      color: ${theme.colors.palette.danger[700]};
    }
  `;
}
