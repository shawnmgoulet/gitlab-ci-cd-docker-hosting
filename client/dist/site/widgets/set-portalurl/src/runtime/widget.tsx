/** @jsx jsx */
import {BaseWidget, AllWidgetProps, jsx, styled, esri, SessionManager, portalUrlUtils} from 'jimu-core';
import {IOAuth2Options, ICredential} from '@esri/arcgis-rest-auth';
import * as Cookies from 'js-cookie';
//import {IMConfig} from '../config';
import {Button, Label, Input, FormGroup, InputGroup, /*InputGroupText,*/ InputGroupAddon, FormFeedback, Dropdown,
DropdownToggle, DropdownMenu, DropdownItem} from 'jimu-ui';
import defaultMessages from './translations/default';
import {PortalInfo, InvalidMessage, PortalValidator} from './utils/portal-validator';
import {getStyle} from './lib/style';

export enum LoadStatus {
  Pending = 'Pending',
  Fulfilled = 'Fulfilled',
  Rejected = 'Rejected'
}

interface SigninInfo {
  portalUrl: string,
  clientId: string,
  supportsOAuth: boolean,
  isWebTier: boolean; 
}

interface WState {
  signinInfos: SigninInfo[], 
  showUrlList: boolean,
  loadStatus: LoadStatus,
  selectedPortalInfo: PortalInfo;
}

export default class Widget extends BaseWidget<AllWidgetProps<{}>, WState>{

  private portalValidator: PortalValidator;
  //private selectedPortalInfo: PortalInfo;


  public refs: {
    urlInput: HTMLInputElement;
    clientIdInput: HTMLInputElement;
  }

  constructor(props){
    super(props);

    this.portalValidator = new PortalValidator();

    this.state = {
      signinInfos: [],
      showUrlList: false,
      loadStatus: LoadStatus.Pending,
      selectedPortalInfo: this.portalValidator.getDefaultProtalInfo()
    };
  }

  componentDidMount() {
    this.fetchSigninInfo();
  }

  fetchSigninInfo() {
    let signinInfoUri = `${window.location.protocol}//${window.location.host}/signininfo`;
    return window.fetch(signinInfoUri, {cache: 'no-cache'}).then(res => res.json()).then(signinInfos => {
      let portalUrl = Cookies.get('exb_portalurl') ||
                              (signinInfos[0] && signinInfos[0].portalUrl) ||
                              '';
      let state = {
        signinInfos: signinInfos
      }
      this.selectPortalUrl(portalUrl, state)
      return signinInfos;
    }).catch(error => {
      console.error(error);
      return error;
    });
  }

  addSigninInfo(signinInfo: SigninInfo) {
    let addSigninInfoUri = `${window.location.protocol}//${window.location.host}/signinInfo`;
    return window.fetch(addSigninInfoUri, {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify(signinInfo)
    }).then(res => res.json())
    .catch(error => {
      console.error(error);
    });
  }

  getSigninInfo(portalUrl: string): SigninInfo {
    var signinInfo = null;
    this.state.signinInfos.some((_signinInfo) => {
      if(portalUrlUtils.isSamePortalUrl(_signinInfo.portalUrl, portalUrl)) {
        signinInfo = _signinInfo; 
        return true;
      } else {
        return false;
      }
    });
    return signinInfo;
  }

  OAuthSignIn(signinInfo: SigninInfo) {
    const portal = signinInfo.portalUrl + '/sharing/rest/';
    let oauth2Info: IOAuth2Options = {
      portal: portal,
      clientId: signinInfo.clientId,
      redirectUri: `${window.location.origin}/jimu-core/oauth-callback.html?clientID=${signinInfo.clientId}&portal=${portal}&isDevEdition=${window.isDevEdition}&fromUrl=${encodeURIComponent('/')}`,
      popup: false,
      params: {
        showSignupOption: true,
        signupType: 'esri'
      }
    }; 
    esri.restAuth.UserSession.beginOAuth2(oauth2Info);
  }

  webTierSignIn() {
    // add session
    var credential: ICredential = this.state.selectedPortalInfo.tokenInfo;
    const sessionManager = SessionManager.getInstance();
    sessionManager.addFromArcGisJSCredential(credential);
    // set cookie
    const session = esri.restAuth.UserSession.fromCredential(credential);
    this.writeCookie(session);
    // redirect
    window.location.href = encodeURIComponent('/');
  }

  writeCookie(session) {
    let i = session.portal.indexOf('/sharing/rest');
    let domain = session.portal.substring('https://'.length, i);
    let parts = domain.split('.');
    let urlKey = parts[0];
    parts.splice(0, 1);
    let customBaseUrl = parts.join('.');
    let esriAuthJson = {
      portalApp: false,
      email: session.username,
      token: session.token,
      // culture: "en-US",
      // region: "WO",
      expires: session.tokenExpires,
      // accountId: "oC086ufSSQ6Avnw2",
      // role: "account_admin",
      // created: 1559636894575,
      // persistent: false,
      // id: "GaSIxAT21wz1Cet2",
      urlKey: urlKey,
      customBaseUrl: customBaseUrl
    }
    Cookies.set('esri_auth', JSON.stringify(esriAuthJson));

    if(window.isDevEdition) {
      let portalUrl = session.portal.substring(0, i);
      Cookies.set('exb_portalurl', portalUrl, {expires: 365});
    }
  }

  signIn(signinInfo: SigninInfo): void {
    if(signinInfo.supportsOAuth) {
      this.OAuthSignIn(signinInfo); 
    } else if(this.state.selectedPortalInfo.isWebTier) {
      this.webTierSignIn();
    }
  }

  validatePortal(portalUrl: string): Promise<PortalInfo>{
    return this.portalValidator.validatePortal(portalUrl);
  }

  selectPortalUrl(portalUrl: string, state?: any) {
    if(portalUrl) {
      this.setState({
        loadStatus: LoadStatus.Pending,
        showUrlList: false
      });
      this.validatePortal(portalUrl).then( (portalInfo) => {
        this.setState({selectedPortalInfo: portalInfo, loadStatus: LoadStatus.Fulfilled, ...state});
      }).catch(() => this.setState({loadStatus: LoadStatus.Rejected}));
    } else {
      this.setState({selectedPortalInfo: this.portalValidator.getDefaultProtalInfo(), loadStatus: LoadStatus.Fulfilled});
    }
  }

  onSignInBtnClick = () => {
    if(!this.state.selectedPortalInfo.isValid) {
      return;
    }

    let signinInfo = {
      portalUrl: this.state.selectedPortalInfo.url,
      clientId: this.refs.clientIdInput.value,
      supportsOAuth: this.state.selectedPortalInfo.supportsOAuth,
      isWebTier: this.state.selectedPortalInfo.isWebTier
    };

    this.addSigninInfo(signinInfo).then(() => {
      this.signIn(signinInfo);
    });
  }

  onPortalUrlInputBlur = () => {
    let portalUrl = this.refs.urlInput.value;
    this.selectPortalUrl(portalUrl);
  }

  onUrlListBtnClick = () => {
    this.setState({
      showUrlList: !this.state.showUrlList
    });
  }

  onUrlListItemClick = (portalUrl: string) => {
    this.selectPortalUrl(portalUrl);
  }

  render() {
    const SignInButton = styled(Button)`
      display: block;
      margin: 0 auto;
    `;

    let portalUrlListContent;
    portalUrlListContent = this.state.signinInfos.map((signinInfo, i) => {
      return (
        <DropdownItem
         key={i}
         active = {portalUrlUtils.isSamePortalUrl(signinInfo.portalUrl, this.state.selectedPortalInfo.url) ? true : false}
         onClick={() => this.onUrlListItemClick(signinInfo.portalUrl)}>
           {signinInfo.portalUrl}
        </DropdownItem>);
    });
    const selectedSigninInfo = this.getSigninInfo(this.state.selectedPortalInfo.url);
    const clientId = selectedSigninInfo && selectedSigninInfo.clientId || '';

    let message;
    if(!this.state.selectedPortalInfo.url && this.state.loadStatus !== LoadStatus.Pending) {
      message = (
        <div>
          <div>{defaultMessages.sampleOrgUrl}</div>
          <div>{defaultMessages.samplePortalUrl}</div>
        </div>
      );
    }

    let invalidMessage;
    if(this.state.selectedPortalInfo &&
      this.state.selectedPortalInfo.invalidMessage &&
      this.state.loadStatus !== LoadStatus.Pending) {
      if(this.state.selectedPortalInfo.invalidMessage === InvalidMessage.InvalidPortalUrl) {
        invalidMessage = (
          <div>
            <div>{defaultMessages.invalidPortalUrlMessage}</div>
            <div>{defaultMessages.sampleOrgUrl}</div>
            <div>{defaultMessages.samplePortalUrl}</div>
          </div>
        );
      } else {
        invalidMessage = (
          <div>
            <div>{`${defaultMessages.unableToAccessMessage} ${this.state.selectedPortalInfo.url}`}</div>
            <div>{defaultMessages.invalidHostNameMessage}</div>
          </div>
        );
      }
    }

    let hideClientIdInput = this.state.selectedPortalInfo && this.state.selectedPortalInfo.supportsOAuth ? 'displayed' : 'hidden';
    let hideLoading = 'hidden'; 
    let disabled = false;
    if(this.state.loadStatus === LoadStatus.Pending) {
      disabled = true;
      hideLoading = 'displayed';
    }
    let signInBtnDisabled = !this.state.selectedPortalInfo.isValid;

    return (
      <div css={ getStyle(this.props.theme)} className="widget-set-portalurl">
        <div className="mt-5 mb-2">
          <Label className="description"> {defaultMessages.portalUrlDescription} </Label>
          <Dropdown fluid group selectable size="default" className="dropdown" isOpen={this.state.showUrlList} toggle={this.onUrlListBtnClick}>
            <InputGroup>
              <Input id="caret" disabled={disabled} bsSize="lg" placeholder={defaultMessages.portalUrl} min={0} max={100} type="text"
                value={this.state.selectedPortalInfo.url || ''} onBlur={this.onPortalUrlInputBlur} ref="urlInput"/>

              <InputGroupAddon addonType="append" >
                <div className={`loading-panel ${hideLoading}`}>
                  <div className="jimu-small-loading"/>
                </div>
              </InputGroupAddon>
            </InputGroup>
            <DropdownToggle className="dropdown-toggle" caret/>
            <DropdownMenu className="dropdown-menu" zIndex="55">
              {portalUrlListContent}
            </DropdownMenu>
          </Dropdown>
        </div>

        <FormFeedback className="message mx-2" valid> {message} </FormFeedback>
        <FormFeedback className="message invalid-message mx-2" valid> {invalidMessage} </FormFeedback>
        <FormGroup className={`portal-clientId mt-5 ${hideClientIdInput}`}>
          <Label className="description" for="clientIdText"> {defaultMessages.clientIdDescription} </Label>
          <Input disabled={disabled} bsSize="lg" type="text" name="text" id="clientIdText" placeholder={defaultMessages.clientId} value={clientId} ref="clientIdInput"/>
        </FormGroup>
        <SignInButton disabled={disabled || signInBtnDisabled} size="lg" onClick={() => this.onSignInBtnClick()}> {defaultMessages.signIn} </SignInButton>
      </div>
    )
  }

}
