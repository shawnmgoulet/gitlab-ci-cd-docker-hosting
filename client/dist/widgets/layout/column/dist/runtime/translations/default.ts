export default {
  _widgetLabel: 'column',
  widgetProperties: 'Widget properties',
  widgetFunctions: 'Widget functions',
  widgetName: 'widget name:',
  widgetProps: 'widget properties:',
  tips: 'Column'
}
