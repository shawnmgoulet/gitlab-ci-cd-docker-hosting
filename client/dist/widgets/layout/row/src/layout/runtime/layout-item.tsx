/** @jsx jsx */
import { React, jsx, classNames, lodash, BoundingBox } from 'jimu-core';
import {
  LayoutItemProps,
  StateToLayoutItemProps,
} from 'jimu-layouts/common';
import { LayoutItem } from 'jimu-layouts/layout-runtime';
import { RowLayoutItemSetting } from '../../config';
import { DEFAULT_ROW_ITEM_SETTING } from '../../default-config';

interface OwnProps {
  offset: number;
  span: number;
  alignItems?: string;
  children?: any;
  style?: any;
}

export default class RowItem extends React.PureComponent<LayoutItemProps &
StateToLayoutItemProps & OwnProps> {
  calHeight(itemSetting: RowLayoutItemSetting, bbox: BoundingBox) {
    if (itemSetting.heightMode === 'fit') {
      return { height: 'auto', alignSelf: 'stretch' };
    }
    if (itemSetting.heightMode === 'ratio') {
      return { alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
    }
    if (itemSetting.heightMode === 'fixed') {
      return { height: bbox.height, alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
    }
    return { height: 'auto', alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
  }

  render() {
    const {
      span,
      offset,
      layoutId,
      layoutItem,
      style,
    } = this.props;
    if (!layoutItem) {
      return null;
    }
    const bbox = layoutItem.bbox;
    const itemSetting: RowLayoutItemSetting = lodash.assign({}, DEFAULT_ROW_ITEM_SETTING, layoutItem.setting);
    const mergedClass = classNames(
      'row-layout-item',
      `col-${span}`,
      `offset-${offset}`,
    );

    const mergedStyle: any = {
      ...style,
      ...this.calHeight(itemSetting, bbox),
    };

    if (itemSetting.offsetX || itemSetting.offsetY) {
      mergedStyle.transform = `translate(${itemSetting.offsetX || 0}px, ${itemSetting.offsetY || 0}px)`;
    }

    return (
      <LayoutItem
        style={mergedStyle}
        className={mergedClass}
        layoutId={layoutId}
        layoutItemId={layoutItem.id}
        forceAspectRatio={itemSetting.heightMode === 'ratio'}
        aspectRatio={itemSetting.aspectRatio}
        onClick={this.props.onClick}
      />
    );
  }
}
