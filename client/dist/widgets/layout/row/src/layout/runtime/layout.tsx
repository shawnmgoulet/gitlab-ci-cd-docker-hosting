/** @jsx jsx */
import {
  React,
  ReactRedux,
  classNames,
  jsx,
  css,
  ExtensionManager,
  extensionSpec,
  LayoutTransformFunc,
  lodash,
  IMLayoutJson,
  BrowserSizeMode,
  IMThemeVariables,
} from 'jimu-core';
import { styleUtils } from 'jimu-ui';

import { IMRowConfig } from '../../config';
import { Row } from './row';
import {
  LayoutProps,
  StateToLayoutProps,
  mapStateToLayoutProps,
  PageContext,
  PageContextProps,
} from 'jimu-layouts/common';
import { ChildRect, TOTAL_COLS } from '../types';

type FlexboxLayoutProps = LayoutProps & {
  config: IMRowConfig;
};

class RowLayout extends React.PureComponent<FlexboxLayoutProps & StateToLayoutProps> {
  childRects: ChildRect[];
  rows: string[][];
  finalLayout: IMLayoutJson;
  layoutTransform: LayoutTransformFunc;

  browserSizeMode: BrowserSizeMode;
  mainSizeMode: BrowserSizeMode;
  theme: IMThemeVariables;

  findExtension() {
    const exts = ExtensionManager.getInstance().getExtensions(
      `${extensionSpec.ExtensionPoints.LayoutTransformer}`,
    );
    if (exts && exts.length > 0) {
      const ext = exts.find(item => (item as extensionSpec.LayoutTransformer).layoutType === 'ROW');
      this.layoutTransform = lodash.getValue(ext, 'transformLayout');
    }
  }

  collectRowItems() {
    const { layout, layouts } = this.props;

    if (!this.layoutTransform) {
      this.findExtension();
    }

    let targetLayout = layout;
    if (layouts[this.browserSizeMode] !== layout.id && this.layoutTransform) {
      targetLayout = this.layoutTransform(layout, this.mainSizeMode, this.browserSizeMode);
    }
    this.finalLayout = targetLayout;

    const content = targetLayout.order || [];
    const rows = [];
    let row = [];
    let rowIndex = 0;
    rows.push(row);
    content.forEach((itemId) => {
      const bbox = targetLayout.content[itemId].bbox;
      const left = parseInt(bbox.left, 10);
      const rowNum = Math.floor(left / TOTAL_COLS);
      if (rowNum > rowIndex) {
        row = [];
        rowIndex = rowNum;
        rows.push(row);
      }
      row.push(itemId);
    });
    return rows;
  }

  createRow() {
    const { layout, config, layouts } = this.props;
    return <Row
      layouts={layouts}
      layout={layout}
      transformedLayout={this.finalLayout}
      config={config}
      theme={this.theme}
    ></Row>;
  }

  render() {
    const { layout, className, config } = this.props;

    const layoutStyle: any = config.style || {};

    const mergedStyle: any = {
      ...styleUtils.toCSSStyle(layoutStyle as any),
    };

    return (<PageContext.Consumer>
      {(props: PageContextProps) => {
        this.browserSizeMode = props.browserSizeMode;
        this.mainSizeMode = props.mainSizeMode;
        this.theme = props.theme;

        this.rows = this.collectRowItems();
        return <div className={classNames('layout d-flex', className)} css={css`
          width: 100%;
          overflow: hidden;
        `} style={mergedStyle} data-layoutid={layout.id}>
          {this.createRow()}
        </div>;
      }}
    </PageContext.Consumer>);
  }
}

export default ReactRedux.connect<StateToLayoutProps, {}, FlexboxLayoutProps>(mapStateToLayoutProps)(RowLayout);
