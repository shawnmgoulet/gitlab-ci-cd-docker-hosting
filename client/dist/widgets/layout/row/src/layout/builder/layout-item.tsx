/** @jsx jsx */
import {
  React,
  jsx,
  css,
  classNames,
  LayoutItemType,
  LayoutItemJson,
  IMLayoutItemJson,
  lodash,
  getAppStore,
  WidgetType,
  IMThemeVariables,
  LayoutItemConstructorProps,
  IMSizeModeLayoutJson,
  BrowserSizeMode,
  Immutable,
  BoundingBox,
} from 'jimu-core';
import {
  LayoutItemProps,
} from 'jimu-layouts/common';
import { getAppConfigAction } from 'jimu-for-builder';
import {
  LayoutItemInBuilder,
  DropArea,
  mergeWidgetsIntoColumn,
} from 'jimu-layouts/layout-builder';
import { RowLayoutItemSetting } from '../../config';
import { DEFAULT_ROW_ITEM_SETTING } from '../../default-config';

interface OwnProps {
  layoutItem: IMLayoutItemJson;
  offset: number;
  span: number;
  order: number;
  alignItems?: string;
  builderTheme: IMThemeVariables;
  isDesignMode?: boolean;
  children?: any;
  style?: any;
  onResizeStart: (id: string) => void;
  onResizing: (id: string, x: number, y: number, dw: number, dh: number) => void;
  onResizeEnd: (id: string, x: number, y: number, dw: number, dh: number, layoutItem: LayoutItemJson) => void;
  onDragStart: (id: string) => void;
  onDragging: (id: string, dx: number, dy: number, outOfBoundary: boolean) => void;
  onDragEnd: (id: string, dx: number, dy: number, outOfBoundary: boolean) => void;
}

const dropareaStyle = css`
  position: absolute;
  left: 0;
  max-height: 40px;
  height: 20%;
  right: 0;
  z-index: 10;
  display: flex;

  &.drop-active {
    background: transparent !important;
  }
`;

const topDropareaStyle = css`
  ${dropareaStyle};
  top: 0;
`;

const bottomDropareaStyle = css`
  ${dropareaStyle};
  bottom: 0;
`;

interface State {
  isResizing: boolean;
  dh: number;
}

export default class RowItem extends React.PureComponent<LayoutItemProps & OwnProps, State> {
  initHeight: number;
  initWidth: number;
  fakeTopLayouts: IMSizeModeLayoutJson;
  fakeBottomLayouts: IMSizeModeLayoutJson;

  state: State = {
    isResizing: false,
    dh: 0,
  };

  constructor(props) {
    super(props);
    this.fakeTopLayouts = Immutable({
      [BrowserSizeMode.Large]: `${this.props.layoutId}_${this.props.layoutItemId}_tlarge`,
      [BrowserSizeMode.Medium]: `${this.props.layoutId}_${this.props.layoutItemId}_tmedium`,
      [BrowserSizeMode.Small]: `${this.props.layoutId}_${this.props.layoutItemId}_tsmall`
    });

    this.fakeBottomLayouts = Immutable({
      [BrowserSizeMode.Large]: `${this.props.layoutId}_${this.props.layoutItemId}_blarge`,
      [BrowserSizeMode.Medium]: `${this.props.layoutId}_${this.props.layoutItemId}_bmedium`,
      [BrowserSizeMode.Small]: `${this.props.layoutId}_${this.props.layoutItemId}_bsmall`
    });

  }

  onResizeStart = (id: string, initW: number, initH: number) => {
    this.initWidth = initW;
    this.initHeight = initH;
    this.props.onResizeStart(id);
    this.setState({
      isResizing: true,
    });
  }

  onResizing = (id: string, x: number, y: number, dw: number, dh: number) => {
    this.props.onResizing(id, x, y, dw, dh);
    this.setState({
      dh,
      isResizing: true,
    });
  }

  onResizeEnd = (id: string, x: number, y: number, dw: number, dh: number, shiftKey?: boolean) => {
    const { layoutItem } = this.props;
    this.props.onResizeEnd(id, x, y, dw, dh, layoutItem);
    this.setState({
      isResizing: false,
      dh: 0,
    });
  }

  isFunctionalWidget() {
    const { layoutItem } = this.props;
    if (layoutItem.type === LayoutItemType.Widget) {
      let isLayoutWidget = false;
      const widget = lodash.getValue(getAppStore().getState(), `appConfig.widgets.${layoutItem.widgetId}`);
      if (widget && widget.manifest && widget.manifest.properties) {
        isLayoutWidget =
          widget.manifest.properties.type === WidgetType.Layout ||
          widget.manifest.properties.type === WidgetType.Controller ||
          widget.manifest.properties.hasEmbeddedLayout;
      }
      return !isLayoutWidget;
    }
    return false; // layoutItem.type is LayoutItemType.Section
  }

  dropAtTop = (
    draggingItem: LayoutItemConstructorProps,
    containerRect: ClientRect,
    itemRect: ClientRect,
  ) => {
    this.dropAtBoundary(draggingItem, containerRect, itemRect, 'top');
  }

  dropAtBottom = (
    draggingItem: LayoutItemConstructorProps,
    containerRect: ClientRect,
    itemRect: ClientRect,
  ) => {
    this.dropAtBoundary(draggingItem, containerRect, itemRect, 'bottom');
  }

  dropAtBoundary = (
    draggingItem: LayoutItemConstructorProps,
    containerRect: ClientRect,
    itemRect: ClientRect,
    side: 'top' | 'bottom',
  ) => {
    let appConfigAction = getAppConfigAction();
    mergeWidgetsIntoColumn(
      appConfigAction.appConfig,
      draggingItem,
      containerRect,
      itemRect,
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItemId,
      },
      side,
    ).then(({ updatedAppConfig }) => {
      appConfigAction = getAppConfigAction(updatedAppConfig);
      appConfigAction.exec();
    });
  }

  calHeight(itemSetting: RowLayoutItemSetting, bbox: BoundingBox) {
    if (itemSetting.heightMode === 'fit') {
      return { height: 'auto', alignSelf: 'stretch' };
    }
    if (itemSetting.heightMode === 'ratio') {
      return { alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
    }
    if (itemSetting.heightMode === 'fixed') {
      return { height: bbox.height, alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
    }
    return { height: 'auto', alignSelf: lodash.getValue(itemSetting, 'style.alignSelf', 'flex-start') };
  }

  render() {
    const {
      order,
      span,
      offset,
      layoutId,
      layoutItem,
      draggable,
      resizable,
      selectable,
      style,
      isDesignMode,
    } = this.props;
    if (!layoutItem || layoutItem.isPending) {
      return null;
    }
    const { dh, isResizing } = this.state;
    const bbox = layoutItem.bbox;
    const itemSetting: RowLayoutItemSetting = lodash.assign({}, DEFAULT_ROW_ITEM_SETTING, layoutItem.setting);
    const mergedClass = classNames('row-layout-item', `col-${span}`, `offset-${offset}`, `order-${order}`);

    const mergedStyle: any = {
      ...style,
      ...this.calHeight(itemSetting, bbox),
    };

    if (itemSetting.offsetX || itemSetting.offsetY) {
      mergedStyle.transform = `translate(${itemSetting.offsetX || 0}px, ${itemSetting.offsetY || 0}px)`;
    }

    if (isResizing && dh) {
      mergedStyle.height = this.initHeight + dh;
      // style.width = this.domRect.width + dw;
    }

    const canDropAtBoundary = this.isFunctionalWidget();

    const highlightStyle = css`
      width: 100%;
      height: 10px;
      background: ${this.props.builderTheme.colors.palette.primary[700]};
    `;

    return (
      <LayoutItemInBuilder
        style={JSON.stringify(mergedStyle)}
        layoutId={layoutId}
        layoutItemId={layoutItem.id}
        onResizeStart={this.onResizeStart}
        onResizing={this.onResizing}
        onResizeEnd={this.onResizeEnd}
        onDragStart={this.props.onDragStart}
        onDragging={this.props.onDragging}
        onDragEnd={this.props.onDragEnd}
        left={true}
        right={true}
        top={false}
        bottom={itemSetting.heightMode === 'fixed'}
        draggable={draggable}
        resizable={resizable}
        selectable={selectable}
        onClick={this.props.onClick}
        className={mergedClass}
        forceAspectRatio={itemSetting.heightMode === 'ratio'}
        aspectRatio={isNaN(itemSetting.aspectRatio) ? 1 : itemSetting.aspectRatio}>
        {canDropAtBoundary && <DropArea
          css={css`
            ${topDropareaStyle};
            display: ${!isDesignMode ? 'none' : 'flex'} !important;
          `}
          layouts={this.fakeTopLayouts}
          highlightDragover={true}
          onDrop={this.dropAtTop}>
          <div css={highlightStyle}></div>
        </DropArea>}
        {canDropAtBoundary && <DropArea
          css={css`
            ${bottomDropareaStyle};
            display: ${!isDesignMode ? 'none' : 'flex'} !important;
          `}
          layouts={this.fakeBottomLayouts}
          highlightDragover={true}
          onDrop={this.dropAtBottom}>
          <div css={css`${highlightStyle};position: absolute; bottom:0;`}></div>
        </DropArea>}
      </LayoutItemInBuilder>
    );
  }
}
