/** @jsx jsx */
import {
  React,
  ReactRedux,
  classNames,
  jsx,
  css,
  BrowserSizeMode,
  lodash,
  ExtensionManager,
  extensionSpec,
  LayoutTransformFunc,
  IMLayoutJson,
  // polished,
  IMThemeVariables,
} from 'jimu-core';
import { styleUtils, Loading } from 'jimu-ui';
import { getAppConfigAction } from 'jimu-for-builder';
import { IMRowConfig } from '../../config';
import {
  LayoutProps,
  StateToLayoutProps,
  mapStateToLayoutProps,
  PageContext,
  PageContextProps,
} from 'jimu-layouts/common';
import { TOTAL_COLS } from '../types';
import { Row } from './row';
import { transformToOneColumn } from './utils';

type RowLayoutProps = LayoutProps & {
  widgetId: string;
  config: IMRowConfig;
  parentLayoutId?: string;
  parentLayoutItemId?: string;
};

class RowLayout extends React.PureComponent<RowLayoutProps & StateToLayoutProps> {
  layoutTransform: LayoutTransformFunc;
  finalLayout: IMLayoutJson;
  ref: HTMLElement;
  numOfRows: number;
  // page env
  inSelectMode: boolean;
  theme: IMThemeVariables;
  builderTheme: IMThemeVariables;
  mainSizeMode: BrowserSizeMode;
  browserSizeMode: BrowserSizeMode;

  findExtension() {
    const exts = ExtensionManager.getInstance().getExtensions(
      `${extensionSpec.ExtensionPoints.LayoutTransformer}`,
    );
    if (exts && exts.length > 0) {
      const ext = exts.find(item => (item as extensionSpec.LayoutTransformer).layoutType === 'ROW');
      this.layoutTransform = lodash.getValue(ext, 'transformLayout');
    }
  }

  splitIntoMultipleRows() {
    transformToOneColumn(getAppConfigAction().appConfig, this.props.widgetId).then((appConfig) => {
      getAppConfigAction(appConfig).exec();
    });
  }

  collectRowItems() {
    const { layout, layouts } = this.props;
    if (!this.layoutTransform) {
      this.findExtension();
    }
    let targetLayout = layout;
    const viewOnly = layouts[this.browserSizeMode] !== layout.id;
    if (viewOnly && this.layoutTransform) {
      targetLayout = this.layoutTransform(layout, this.mainSizeMode, this.browserSizeMode);
    }
    this.finalLayout = targetLayout;

    const content = targetLayout.order || [];

    const rows = [];
    let row = [];
    let rowIndex = 0;
    rows.push(row);
    content.forEach((itemId) => {
      if (targetLayout.content[itemId].isPending) {
        return;
      }
      const bbox = targetLayout.content[itemId].bbox;
      const left = parseInt(bbox.left, 10);
      const rowNum = Math.floor(left / TOTAL_COLS);
      if (rowNum > rowIndex) {
        row = [];
        rowIndex = rowNum;
        rows.push(row);
      }
      row.push(itemId);
    });
    return rows;
  }

  createRow() {
    const { layout, config, layouts } = this.props;
    return <Row
      layouts={layouts}
      layout={layout}
      transformedLayout={this.finalLayout}
      config={config}
      isMultiRow={this.numOfRows > 1}
    >{this.props.children}</Row>;
  }

  render() {
    const { layout, className, config } = this.props;
    const layoutStyle: any = config.style || {};

    return (
      <PageContext.Consumer>
        {(pageContext: PageContextProps) => {
          this.theme = pageContext.theme;
          this.browserSizeMode = pageContext.browserSizeMode;
          this.mainSizeMode = pageContext.mainSizeMode;
          this.builderTheme = pageContext.builderTheme;
          this.numOfRows = this.collectRowItems().length;

          if (this.numOfRows > 1 && pageContext.isDesignMode && !pageContext.viewOnly) {
            this.splitIntoMultipleRows();
            return <div className={classNames('d-flex justify-content-center align-items-center', className)}
            css={css`width: 100%`}
            >
              <Loading/>
            </div>;
          }

          const mergedStyle: any = {
            ...styleUtils.toCSSStyle(layoutStyle as any),
          };
          // if (!mergedStyle.border && pageContext.isDesignMode) {
          //   mergedStyle.border = `1px dashed ${polished.rgba(this.builderTheme.colors.palette.dark[300], 0.3)}`;
          // }

          return (<div className={classNames('layout d-flex', classNames)} css={css`
            width: 100%;
            overflow: hidden;
          `} style={mergedStyle} data-layoutid={layout.id}
          ref={el => this.ref = el}>
            {this.createRow()}
          </div>);
        }}
      </PageContext.Consumer>
    );
  }
}

export default ReactRedux.connect<StateToLayoutProps, {}, RowLayoutProps>(mapStateToLayoutProps)(
  RowLayout as any,
);
