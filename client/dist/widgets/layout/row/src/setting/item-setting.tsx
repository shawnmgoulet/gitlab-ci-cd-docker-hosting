/** @jsx jsx */
import {
  React,
  Immutable,
  jsx,
  lodash,
} from 'jimu-core';
import {
  SettingSection,
  SettingRow,
} from 'jimu-ui/setting-components';
import { LayoutItemSettingProps } from 'jimu-layouts/common';

import { UnitTypes, LinearUnit, Input } from 'jimu-ui';
import { CommonLayoutItemSetting } from 'jimu-layouts/layout-builder';
import { RowLayoutItemSetting } from '../config';
import { DEFAULT_ROW_ITEM_SETTING } from '../default-config';
import { InputUnit } from 'jimu-ui/style-setting-components';

const inputStyle = { width: '7.5rem' };

const availableUnits = [UnitTypes.PIXEL];

export default class RowItemSetting extends React.PureComponent<LayoutItemSettingProps> {
  updateStyle(key, value) {
    const { layoutItem } = this.props;
    let style = Immutable(lodash.getValue(layoutItem, 'setting.style', {}));
    this.props.onSettingChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      {
        style: style.set(key, value),
      }
    );
  }

  updateAlign = (e) => {
    this.updateStyle('alignSelf', e.target.value);
  }

  updateHeight = (value: LinearUnit) => {
    const { bbox } = this.props.layoutItem;
    const update = {
      height: `${value.distance}${value.unit}`,
    };
    this.props.onPosChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      lodash.assign({}, bbox, update) as any,
    );
  }

  updateHeightMode = (e) => {
    let setting = this.props.layoutItem.setting || Immutable({});
    setting = setting.set('heightMode', e.target.value);
    this.props.onSettingChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      setting,
    );
  }

  updateAspectRatio = (e) => {
    let setting = this.props.layoutItem.setting || Immutable({});
    setting = setting.set('aspectRatio', +e.target.value);
    this.props.onSettingChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      setting,
    );
  }

  updateOffsetX = (e) => {
    const setting = this.props.layoutItem.setting || Immutable({});
    this.props.onSettingChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      setting.set('offsetX', +e.target.value),
    );
  }

  updateOffsetY = (e) => {
    const setting = this.props.layoutItem.setting || Immutable({});
    this.props.onSettingChange(
      {
        layoutId: this.props.layoutId,
        layoutItemId: this.props.layoutItem.id,
      },
      setting.set('offsetY', +e.target.value),
    );
  }

  formatMessage = (id: string) => {
    return this.props.formatMessage(id);
  }

  render() {
    const { layoutId, layoutItem, isLockLayout } = this.props;
    if (!layoutItem) {
      return null;
    }
    const itemSetting: RowLayoutItemSetting = lodash.assign({}, DEFAULT_ROW_ITEM_SETTING, layoutItem.setting);
    const bbox = layoutItem.bbox;
    const style = itemSetting.style || {} as any;

    return (
      <div className="fixed-item-setting">
        {!isLockLayout && <React.Fragment>
          <SettingSection title={this.formatMessage('size')}>
            <SettingRow label={this.formatMessage('height')}>
              <Input type="select" value={itemSetting.heightMode} onChange={this.updateHeightMode}>
                <option value="fit">{this.formatMessage('fitToContainer')}</option>
                <option value="auto">{this.formatMessage('auto')}</option>
                <option value="fixed">{this.formatMessage('fixed')}</option>
                <option value="ratio">{this.formatMessage('aspectRatio')}</option>
              </Input>
            </SettingRow>
            {itemSetting.heightMode === 'ratio' &&
            <SettingRow label={this.formatMessage('aspectRatioFull')}>
              <Input type="number" style={inputStyle} value={isNaN(itemSetting.aspectRatio) ? 1 : itemSetting.aspectRatio}
              onChange={this.updateAspectRatio}/>
            </SettingRow>}
            {itemSetting.heightMode === 'fixed' &&
            <SettingRow label={this.formatMessage('height')}>
              <InputUnit
                style={inputStyle}
                units={availableUnits}
                value={{
                  distance: parseFloat(bbox.height),
                  unit: UnitTypes.PIXEL,
                }}
                onChange={this.updateHeight}
              />
            </SettingRow>}
          </SettingSection>
          {itemSetting.heightMode !== 'fit' && <SettingSection>
            <SettingRow label={this.formatMessage('align')}>
              <Input style={inputStyle} type="select" value={style.alignSelf || 'flex-start'} onChange={this.updateAlign}>
                <option value="flex-start">{this.formatMessage('T')}</option>
                <option value="flex-end">{this.formatMessage('B')}</option>
                <option value="center">{this.formatMessage('center')}</option>
              </Input>
            </SettingRow>
          </SettingSection>}
          <SettingSection title={this.formatMessage('position')}>
            <SettingRow label={this.formatMessage('offsetX')}>
              <Input
                type="number"
                style={inputStyle}
                value={itemSetting.offsetX}
                onChange={this.updateOffsetX}
              />
            </SettingRow>
            <SettingRow label={this.formatMessage('offsetY')}>
              <Input
                type="number"
                style={inputStyle}
                value={itemSetting.offsetY}
                onChange={this.updateOffsetY}
              />
            </SettingRow>
          </SettingSection>
        </React.Fragment>}
        <CommonLayoutItemSetting
          layoutId={layoutId}
          layoutItemId={layoutItem.id}
          style={this.props.style}
          onStyleChange={this.props.onStyleChange}
          formatMessage={this.props.formatMessage}/>
      </div>
    );
  }
}
