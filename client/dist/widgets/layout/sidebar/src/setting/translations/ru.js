define({
  expanded: 'Ж_Expanded_________________Я',
  collapsed: 'Ж_Collapsed___________________Я',
  panel: 'Ж_Side Panel_____________________Я',
  overlay: 'Ж_Overlay_______________Я',
  resizable: 'Ж_Resizable___________________Я',
  defaultState: 'Ж_Default state______________Я',
  toggleBtn: 'Ж_Toggle button______________Я',
  style: 'Ж_Style___________Я',
  iconSize: 'Ж_Icon size___________________Я',
  divider: 'Ж_Divider_______________Я',
  leftPanel: 'Ж_Left panel_____________________Я',
  topPanel: 'Ж_Top panel___________________Я',
  rightPanel: 'Ж_Right panel____________Я',
  bottomPanel: 'Ж_Bottom panel_____________Я',
  width: 'Ж_Width___________Я',
  height: 'Ж_Height_____________Я',
  size: 'Ж_Size_________Я',
  direction: 'Ж_Direction___________________Я',
  horizontal: 'Ж_Horizontal_____________________Я',
  vertical: 'Ж_Vertical_________________Я',
  dockSide: 'Ж_Dock side___________________Я',
  visible: 'Ж_Visible_______________Я',
});