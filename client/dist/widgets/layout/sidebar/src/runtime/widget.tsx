/** @jsx jsx */
import { BaseWidget, AllWidgetProps, jsx, IMThemeVariables, IMState, lodash } from 'jimu-core';
// import defaultMessages from './translations/default';
import { SidebarLayout } from 'jimu-layouts/layout-runtime';
import { IMSidebarConfig } from 'jimu-layouts/common';

interface ExtraProps {
  sidebarVisible: boolean;
}

export default class Widget extends BaseWidget<AllWidgetProps<IMSidebarConfig> & ExtraProps> {

  static mapExtraStateProps = (state: IMState, props: AllWidgetProps<IMSidebarConfig>): ExtraProps => {
    return {
      sidebarVisible: lodash.getValue(state, `widgetsState.${props.id}.collapse`, true),
    };
  }

  render() {
    const { layouts, theme, builderSupportModules } = this.props;
    const LayoutComponent = !window.jimuConfig.isInBuilder
      ? SidebarLayout
      : builderSupportModules.widgetModules.SidebarLayoutBuilder;

    if (!LayoutComponent) {
      return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>No layout component!</div>
      );
    }

    return (
      <div className="widget-sidebar-layout d-flex w-100 h-100" >
        <LayoutComponent
          theme={theme as IMThemeVariables}
          widgetId={this.props.id}
          direction={this.props.config.direction}
          // layouts={layouts[DEFAULT_EMBED_LAYOUT_NAME]}
          firstLayouts={layouts.FIRST}
          secondLayouts={layouts.SECOND}
          config={this.props.config}
          sidebarVisible={this.props.sidebarVisible}
        />
      </div>
    );
  }
}
