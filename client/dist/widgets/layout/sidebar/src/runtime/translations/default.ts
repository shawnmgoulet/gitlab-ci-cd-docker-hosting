export default {
  _widgetLabel: 'Sidebar',
  loading: 'Loading',
  widgetProperties: 'Widget properties',
  widgetFunctions: 'Widget functions',
  widgetName: 'widget name:',
  widgetProps: 'widget properties:'
}
