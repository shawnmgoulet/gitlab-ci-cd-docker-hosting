import {
  IMSidebarConfig,
  CollapseSides,
  SidebarControllerPositions,
  SidebarType,
  ICON_TYPE,
} from 'jimu-layouts/common';
import { Immutable } from 'jimu-core';

export const defaultConfig: IMSidebarConfig = Immutable({
  direction: SidebarType.Horizontal,
  collapseSide: CollapseSides.First,
  overlay: false,
  size: '300px',
  divider: {
    visible: true,
  },
  resizable: false,
  toggleBtn: {
    visible: true,
    icon: ICON_TYPE.Left,
    offsetX: 15,
    offsetY: 0,
    position: SidebarControllerPositions.Center,
    iconSize: 14,
    width: 15,
    height: 60,
    color: {
      normal: {
        icon: {
          useTheme: false,
          color: '#FFFFFF',
        },
        bg: {
          useTheme: true,
          color: 'colors.primary',
        },
      },
      hover: {
        bg: {
          useTheme: true,
          color: 'colors.palette.primary[600]',
        },
      },
    },
    expandStyle: {
      style: {
        borderRadius: '0 92px 92px 0',
      },
    },
    collapseStyle: {
      style: {
        borderRadius: '0 92px 92px 0',
      },
    },
  },
  defaultState: 1,
});
