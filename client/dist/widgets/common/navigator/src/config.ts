
import { LinkResult, ImmutableObject, ImmutableArray, IMIconResult, ThemeNav, ThemeNavType } from 'jimu-core';
import { LinearUnit } from 'jimu-ui'

export enum Position {
  Start = 'start',
  End = 'end'
}

export interface NavLinkType {
  name?: string;
  value: LinkResult
}

export enum Alignment {
  Left = 'left',
  Center = 'center',
  Right = 'right'
}

export enum ViewType {
  Auto = 'AUTO',
  Custom = 'CUSTOM'
}

export type NavSetting = {
  alignment?: Alignment;
  space?: LinearUnit,
  showText?: boolean;
  showIcon?: boolean;
  iconPosition?: Position;
} & ThemeNav;

export interface Arrow {
  arrow?: boolean;
  reverse?: boolean;
  backwordIcon?: IMIconResult;
  forwordIcon?: IMIconResult;
}

export interface Slide {
  slide?: boolean;
  type?: any;
  style?: any;
  text?: boolean;
  icon?: boolean;
  iconPosition?: Position;
}

export type NavDisplay = {
  vertical?: boolean;
  navType: ThemeNavType,
  arrow?: Arrow;
} & NavSetting;

export type IMNavDisplay = ImmutableObject<NavDisplay>;

export interface Config {
  data: ImmutableObject<{
    section: string;
    type: ViewType,
    views?: ImmutableArray<string>;
  }>,
  display: IMNavDisplay;
}

export type IMConfig = ImmutableObject<Config>;