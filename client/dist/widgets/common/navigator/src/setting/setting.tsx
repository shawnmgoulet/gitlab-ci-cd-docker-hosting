
/** @jsx jsx */
import {
  IMState, IMAppConfig, css, jsx, ThemeVariables, LayoutItemType, BrowserSizeMode, ImmutableArray, Immutable, lodash,
  ThemeNavVariants, ThemeNav, ImmutableObject, ThemeButtonStylesByStates, ThemeButtonStyles, polished
} from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps, appConfigUtils, defaultMessages } from 'jimu-for-builder';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { IMConfig, ViewType, IMNavDisplay } from '../config';
import { Input, Label, ButtonGroup, Icon, Button, MultiSelect, MultiSelectItem, Tabs, Tab } from 'jimu-ui';
import { toMultiSelectItems, getSectionLabel, getSectionViews } from '../utils'
import { ThemeColorPicker } from 'jimu-ui/color-picker';

const rightArrowIcon = require('jimu-ui/lib/icons/direction-right.svg');
const downArrowIcon = require('jimu-ui/lib/icons/direction-down.svg');

interface ExtraProps {
  appConfig: IMAppConfig;
  appTheme: ThemeVariables;
  sizeMode: BrowserSizeMode;
}

interface State {
  sections: string[];
  views?: string[];
}

type Props = AllWidgetSettingProps<IMConfig> & ExtraProps;

export default class Setting extends BaseWidgetSetting<Props, State>{

  static mapExtraStateProps = (state: IMState) => {
    return {
      appConfig: state && state.appStateInBuilder && state.appStateInBuilder.appConfig,
      appTheme: state && state.appStateInBuilder && state.appStateInBuilder.theme,
      sizeMode: state && state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      sections: [],
      views: []
    }
  }

  componentDidMount() {
    const { appConfig, id, sizeMode } = this.props;
    const sections = this.getSections(appConfig, id, sizeMode);
    this.setState({ sections });
    let section = this.props.config.data.section;
    if (sections.indexOf(section) < 0) {
      section = sections[0];
      this.onDataSectionChange(section);
      this.setState({ views: [] });
    } else {
      const views = this.getViews(section);
      this.setState({ views });
    }
  }

  componentDidUpdate(preProps: Props, prveState: State) {
    const { appConfig, id, sizeMode } = this.props;
    const sections = this.getSections(appConfig, id, sizeMode);
    const preSections = prveState.sections;
    let section = this.props.config.data.section;

    if (!lodash.isDeepEqual(sections, preSections)) {
      this.setState({ sections });
      if (sections.indexOf(section) < 0) {
        section = sections[0];
        this.onDataSectionChange(section);
        this.setState({ views: [] });
      } else {
        const preSection = preProps.config.data.section;

        if (section !== preSection) {
          const views = this.getViews(section);
          this.setState({ views });
          this.onSettingChange(['data', 'views'], []);
        }
      }
    } else {
      const preSection = preProps.config.data.section;

      if (section !== preSection) {
        const views = this.getViews(section);
        this.setState({ views });
        this.onSettingChange(['data', 'views'], []);
      }
    }

  }

  getStyle = () => {
    return css`
      .jimu-multi-select {
        width: 100%;
        > .jimu-menu-item {
          width: 100%;
          height: ${polished.rem(26)};
        }
      }
      .tab-title-item{
        width: 33%;
      }
      .radio-container {
        display: flex;
        align-items: center;
        width: 100%;
        margin-top: 0.5rem;
        label {
          margin-bottom: 0;
        }
      }
    `;
  }

  translate = (id: string, values?: any) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }, values);
  }

  onSettingChange = (key: string | string[], value: any) => {
    let config = this.props.config;
    if (Array.isArray(key)) {
      config = config.setIn(key, value);
    } else {
      config = config.set(key, value);
    }
    this.props.onSettingChange({
      id: this.props.id,
      config
    });
  }

  onNavTypeRadioChange = (e: React.ChangeEvent<HTMLInputElement>, value) => {
    const checked = e.currentTarget.checked;
    if (!checked) {
      return;
    }
    let config = this.props.config;
    config = config.setIn(['display', 'navType'], value);
    config = config.setIn(['display', 'variants'], null);
    this.props.onSettingChange({
      id: this.props.id,
      config
    });
  }

  onSectionChange = (section: string) => {
    section && this.onSettingChange(['data', 'section'], section);
  }

  onViewsChange = (e, v, views) => {
    this.onSettingChange(['data', 'views'], views);
  }

  getSections = (appConfig: IMAppConfig, id: string, sizeMode: BrowserSizeMode): string[] => {
    return appConfigUtils.getWidgetsOrSectionsInTheSameContainer(appConfig, id, LayoutItemType.Widget, LayoutItemType.Section, sizeMode) || [];
  }

  getViews = (section: string): string[] => {
    if (!section) {
      return [];
    }
    const { appConfig } = this.props;
    return appConfig.sections[section] && appConfig.sections[section].views.asMutable() || [];
    const views = appConfig.sections[section] && appConfig.sections[section].views;
    return views ? views.asMutable() : [];
  }

  getViewSelectItems = (): ImmutableArray<MultiSelectItem> => {
    const { appConfig } = this.props;
    const { views } = this.state;
    const selectItems = toMultiSelectItems(views, appConfig);
    return Immutable(selectItems);
  }

  onDataSectionChange = (section: string) => {
    let { config: { data } } = this.props;
    data = data.set('section', section).set('views', Immutable([]));
    this.onSettingChange('data', data);
  }

  onViewTypeChange = (type: ViewType) => {
    if (type === ViewType.Auto) {
      this.onSettingChange(['data', 'type'], type);
    } else {
      let { config: { data } } = this.props;
      data = data.set('type', type).set('views', Immutable([]));
      this.onSettingChange('data', data);
    }
  }

  generateNavTypes = () => {
    return [{ label: this.translate('default'), value: 'default' },
    { label: this.translate('underline'), value: 'underline' },
    { label: this.translate('pills'), value: 'pills' }];
  }

  getSelectedViews = (): ImmutableArray<string> => {
    let { config: { data: { section, views = Immutable([]), type } }, appConfig } = this.props;

    if (type === ViewType.Auto || !views) {
      return Immutable([]);
    } else {
      const sectionViews = getSectionViews(section, appConfig);
      const fileredViews = views.filter(view => !!(sectionViews.indexOf(view) > -1));
      return Immutable(fileredViews);
    }
  }

  renderSelectText = (values: string[]) => {
    const viewNumber = values ? values.length : 0;
    return this.translate('viewsSelected', { viewNumber });
  }

  getNavVariantsFromTheme(theme: ThemeVariables): ImmutableObject<ThemeNavVariants> {
    let navVars: ThemeNav = lodash.getValue(theme, 'components.nav');
    return navVars && navVars.variants as ImmutableObject<ThemeNavVariants>;
  }

  render() {
    let { config: { data: { section, type }, display: { vertical, navType, variants } = {} as IMNavDisplay }, appTheme, appConfig } = this.props;
    variants = variants || this.getNavVariantsFromTheme(appTheme);
    const background = lodash.getValue(variants, `${navType}.bg`) as string;
    const itemSetting = lodash.getValue(variants, `${navType}.item`) as ThemeButtonStylesByStates;
    const regular = lodash.getValue(itemSetting, 'default') as ThemeButtonStyles;
    const active = lodash.getValue(itemSetting, 'active') as ThemeButtonStyles;
    const hover = lodash.getValue(itemSetting, 'hover') as ThemeButtonStyles;

    const { sections } = this.state;

    return <div className="widget-setting-navigator jimu-widget-setting" css={this.getStyle()}>
      <SettingSection>
        <SettingRow flow="wrap" label={this.translate('linkTo')}>
          <Input type="select" value={section} onChange={e => this.onSectionChange(e.target.value)}>
            {sections.map((sid, index) => <option key={index} value={sid}>{getSectionLabel(sid, appConfig)}</option>)}
          </Input>
        </SettingRow>

      </SettingSection>

      <SettingSection>
        <SettingRow label={this.translate('views')}></SettingRow>
        <SettingRow>
          <Input id="view-type-auto" style={{ cursor: 'pointer' }}
            name="view-type" onChange={e => this.onViewTypeChange(ViewType.Auto)} type="radio" checked={type === ViewType.Auto} />
          <Label style={{ cursor: 'pointer' }} for="view-type-auto" className="ml-1">{this.translate('auto')}</Label>
        </SettingRow>
        <SettingRow>
          <Input id="view-type-custom" style={{ cursor: 'pointer' }}
            name="view-type" onChange={e => this.onViewTypeChange(ViewType.Custom)} type="radio" checked={type === ViewType.Custom} />
          <Label style={{ cursor: 'pointer' }} for="view-type-custom" className="ml-1">{this.translate('custom')}</Label>
        </SettingRow>
        {type === ViewType.Custom && <SettingRow flow="wrap">
          <MultiSelect values={this.getSelectedViews()} items={this.getViewSelectItems()} onClickItem={this.onViewsChange} displayByValues={this.renderSelectText} ></MultiSelect>
        </SettingRow>}
      </SettingSection>

      <SettingSection title={this.translate('appearance')}>

        <SettingRow label={this.translate('style')} flow="wrap">
          {this.generateNavTypes().map((item, index) =>
            <div className="radio-container" key={index}>
              <Input id={'nav-style-type' + index} style={{ cursor: 'pointer' }}
                name="style-type" onChange={e => this.onNavTypeRadioChange(e, item.value)} type="radio" checked={navType === item.value} />
              <Label style={{ cursor: 'pointer' }} for={'nav-style-type' + index} className="ml-1">{item.label}</Label>
            </div>)
          }
        </SettingRow>

        <SettingRow flow="no-wrap" label={this.translate('direction')}>
          <ButtonGroup>
            <Button title={this.translate('right')} type="secondary" icon size="sm" active={!vertical} onClick={() => this.onSettingChange(['display', 'vertical'], false)}>
              <Icon icon={rightArrowIcon}></Icon>
            </Button>
            <Button title={this.translate('down')} type="secondary" icon size="sm" active={vertical} onClick={() => this.onSettingChange(['display', 'vertical'], true)}>
              <Icon icon={downArrowIcon}></Icon>
            </Button>
          </ButtonGroup>
        </SettingRow>
        <SettingRow label={this.translate('background')} flow="no-wrap">
          <ThemeColorPicker colors={appTheme && appTheme.colors} value={background} onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'bg'], value)} />
        </SettingRow>


        <SettingRow label={this.translate('states')} flow="wrap">
          <Tabs pills className="flex-grow-1 w-100 h-100" fill>
            <Tab className="tab-title-item" active={true} title={this.translate('regular')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={regular && regular.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'default', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={regular && regular.bg}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'default', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={regular && regular.border && regular.border.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'default', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
            <Tab className="tab-title-item" title={this.translate('select')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={active && active.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'active', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={active && active.bg}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'active', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={active && active.border && active.border.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'active', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
            <Tab className="tab-title-item" title={this.translate('hover')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={hover && hover.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'hover', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={hover && hover.bg}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'hover', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={hover && hover.border && hover.border.color}
                  onChange={(value) => this.onSettingChange(['display', 'variants', navType, 'item', 'hover', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
          </Tabs>
        </SettingRow>

      </SettingSection>
    </div >
  }
}