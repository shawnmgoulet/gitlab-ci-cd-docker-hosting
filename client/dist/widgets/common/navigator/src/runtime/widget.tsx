/** @jsx jsx */
import { IMState, IMAppConfig, jsx, css, lodash, ThemeButtonStylesByStates, ThemeButtonStyles } from 'jimu-core';
import { BaseWidget } from 'jimu-core';
import { AllWidgetProps } from 'jimu-core';
import { Nav, NavLink, NavItem, Icon } from 'jimu-ui';
import { IMConfig, NavLinkType, ViewType, IMNavDisplay } from '../config';
import { toNavViewLinks, isNavItemActive, getSectionViews, updateViewsBySection } from '../utils';
import defaultMessages from './translations/default';

const navigatorIcon = require('jimu-ui/lib/icons/navigator.svg');

interface State {
  links: NavLinkType[];
}

interface ExtraProps {
  appConfig: IMAppConfig;
  viewIds: string[];
}

type Props = AllWidgetProps<IMConfig> & ExtraProps;

export default class Widget extends BaseWidget<Props, State>{
  constructor(props) {
    super(props);
    this.state = {
      links: []
    }
  }

  static mapExtraStateProps = (state: IMState) => {
    return {
      appConfig: state && state.appConfig,
      viewIds: state && state.appRuntimeInfo && state.appRuntimeInfo.currentViewIds
    }
  }

  componentDidMount() {
    this.updateLinks();
  }

  componentDidUpdate(preProps: Props) {
    const { appConfig: { sections: preSections, views: preAllViews } } = preProps;
    const { appConfig: { sections, views: allViews } } = this.props;
    const { config: { data: { type: preType, section: preSection, views: preViews } } } = preProps;
    const { config: { data: { type, section, views } } } = this.props;

    const shouldUpdate = allViews !== preAllViews || sections !== preSections ||
      type !== preType || section !== preSection || (type === ViewType.Custom && views !== preViews);

    if (shouldUpdate) {
      this.updateLinks();
    }
  }

  updateLinks = () => {
    const views = this.getViews();
    const links = this.getLinks(views);
    this.setState({ links });
  }

  getViews = (): string[] => {
    let { config: { data: { type, section, views } }, appConfig } = this.props;
    if (type === ViewType.Auto) {
      return getSectionViews(section, appConfig);
    } else {
      let vs = views ? views.asMutable() : [];
      return updateViewsBySection(vs, section, appConfig);
    }
  }

  getLinks = (views: string[]) => {
    const { appConfig } = this.props;
    return toNavViewLinks(views, appConfig);
  }

  getStyle = () => {
    return css`
      overflow: hidden;
      .nav-ul {
        flex-wrap: nowrap;
        &.nav-tabs .nav-link.active {
          background-color: unset;
        }
      }
      .placeholder {
        position: absolute;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    `;
  }

  navStyle = () => {
    const { config: { display: { vertical, navType, variants } = {} as IMNavDisplay } } = this.props;
    const background = lodash.getValue(variants, `${navType}.bg`) as string;
    const itemSetting = lodash.getValue(variants, `${navType}.item`) as ThemeButtonStylesByStates;
    const regular = lodash.getValue(itemSetting, 'default') as ThemeButtonStyles;
    const active = lodash.getValue(itemSetting, 'active') as ThemeButtonStyles;
    const hover = lodash.getValue(itemSetting, 'hover') as ThemeButtonStyles;

    const width = vertical ? '100%' : undefined;

    return css`
        *:focus{
          box-shadow: none !important;
        }
        width: ${width};
        background: ${background};
        flex-wrap: nowrap;
        .nav-link {
          white-space: nowrap;
          overflow: hidden;
          width: 100%;
          ${regular && css`
            color: ${regular && regular.color};
            background: ${regular.bg && regular.bg};
            border-color: ${regular && regular.border && regular.border.color};
            border-width: ${regular && regular.border && regular.border.width};
          `}
          ${hover && css`
            &:hover {
              color: ${hover && hover.color};
              background: ${hover.bg && hover.bg};
              border-color: ${hover && hover.border && hover.border.color};
              border-width: ${hover && hover.border && hover.border.width};
            }
          `}
          ${active && css`
            &:not(:disabled):not(.disabled):active, 
            &:not(:disabled):not(.disabled).active, 
            &[aria-expanded="true"] {
              color: ${active && active.color};
              background: ${active.bg && active.bg};
              border-color: ${active && active.border && active.border.color};
              border-width: ${active && active.border && active.border.width};
            }
          `}
        }
    `;
  }


  translate = (id: string) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] });
  }

  placeholer = () => {
    const { links } = this.state;
    if (links.length || !window.jimuConfig.isInBuilder) {
      return null;
    }
    return <div className="placeholder">
      <Icon icon={navigatorIcon} size="16"></Icon>
      <span className="ml-2">{this.translate('placeholder')}</span>
    </div>;
  }

  isActive = (link: NavLinkType, index: number) => {
    const { viewIds } = this.props;
    const active = isNavItemActive(viewIds, link, index);
    return active;
  }

  navbar = () => {
    const { links } = this.state;
    const { config: { display: { vertical, navType, alignment } = {} as IMNavDisplay }, queryObject } = this.props;
    if (!links.length) {
      return false;
    }
    return <Nav
      navbar
      css={this.navStyle()}
      className="w-100 h-100 nav-ul"
      underline={navType === 'underline'}
      pills={navType === 'pills'}
      vertical={vertical}
      justified={!vertical}
      textAlign={alignment}>
      {links.map((link, index) =>
        <NavItem key={index}>
          <NavLink to={link.value} active={this.isActive(link, index)} queryObject={queryObject}>{link.name}</NavLink>
        </NavItem>
      )}
    </Nav>
  }


  render() {
    return <div className="widget-view-navigation jimu-widget" css={this.getStyle()}>
      {this.placeholer()}
      {this.navbar()}
    </div>
  }
}