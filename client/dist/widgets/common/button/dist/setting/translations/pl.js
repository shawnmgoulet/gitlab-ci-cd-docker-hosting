define({
  default: 'ł_Default_______________ą',
  outline: 'ł_Outline_______________ą',
  link: 'ł_Link_________ą',
  large: 'ł_Large___________ą',
  small: 'ł_Small___________ą',
  primary: 'ł_Primary_______________ą',
  secondary: 'ł_Secondary___________________ą',
  success: 'ł_Success_______________ą',
  info: 'ł_Info_________ą',
  warning: 'ł_Warning_______________ą',
  danger: 'ł_Danger_____________ą',
  light: 'ł_Light___________ą',
  dark: 'ł_Dark_________ą',
  connectToData: 'ł_Connect to data________________ą',
  setLink: 'ł_Set link_________________ą',
  style: 'ł_Style___________ą',
  size: 'ł_Size_________ą',
  color: 'ł_Color___________ą',
  text: 'ł_Text_________ą',
  tooltip: 'ł_Tooltip_______________ą',
  defaultRound: 'ł_Default round______________ą',
  outlineRound: 'ł_Outline round______________ą'
});