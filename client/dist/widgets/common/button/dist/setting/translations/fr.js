define({
  default: 'æ_Default_______________Â',
  outline: 'æ_Outline_______________Â',
  link: 'æ_Link_________Â',
  large: 'æ_Large___________Â',
  small: 'æ_Small___________Â',
  primary: 'æ_Primary_______________Â',
  secondary: 'æ_Secondary___________________Â',
  success: 'æ_Success_______________Â',
  info: 'æ_Info_________Â',
  warning: 'æ_Warning_______________Â',
  danger: 'æ_Danger_____________Â',
  light: 'æ_Light___________Â',
  dark: 'æ_Dark_________Â',
  connectToData: 'æ_Connect to data________________Â',
  setLink: 'æ_Set link_________________Â',
  style: 'æ_Style___________Â',
  size: 'æ_Size_________Â',
  color: 'æ_Color___________Â',
  text: 'æ_Text_________Â',
  tooltip: 'æ_Tooltip_______________Â',
  defaultRound: 'æ_Default round______________Â',
  outlineRound: 'æ_Outline round______________Â'
});