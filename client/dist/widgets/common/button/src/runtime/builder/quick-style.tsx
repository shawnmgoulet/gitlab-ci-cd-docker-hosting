/** @jsx jsx */
import { React, jsx, ThemeButtonType, IntlShape, injectIntl, css, ThemeVariables, themeUtils, SerializedStyles, ImmutableObject } from 'jimu-core';
import { Link, Popper } from 'jimu-ui';
import defaultMessages from '../translations/default';

interface OwnProps {
  reference: React.RefObject<any>;
  onChange: (t: ThemeButtonType) => void;
}

type ExtraProps = {
  intl: IntlShape;
}

interface State {

}

export class _QuickStyle extends React.PureComponent<OwnProps & ExtraProps, State> {
  THEMETYPES: ThemeButtonType[] = [
    'default',
    'primary',
    'secondary',
    'tertiary',
    'danger',
    'link'
  ];

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  translate = (id: string) => {
    const { intl } = this.props;
    return intl ? intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : '';
  }

  getStyle = (theme: ImmutableObject<ThemeVariables>): SerializedStyles => {
    return css`
      width: 295px;
      height: 160px;
      background-color: ${theme.colors.light};
      color: ${theme.colors.dark};
    `;
  }

  render() {
    return (
      <Popper reference={this.props.reference} open={true} zIndex={3} placement="right-start" container={document && document.body} offset={[20, 10]}
        css={this.getStyle(themeUtils.getBuilderThemeVariables())}
      >
        <h4 className="p-2">{this.translate('quickStyle')}</h4>
        {
          this.THEMETYPES.map((t, i) => <Link className="m-2 d-inline-block" onClick={() => this.props.onChange(t)} themeStyle={{type: t}} key={i}>Button</Link>)
        }
      </Popper>
    )
  }
}

export const QuickStyle = injectIntl(_QuickStyle);
