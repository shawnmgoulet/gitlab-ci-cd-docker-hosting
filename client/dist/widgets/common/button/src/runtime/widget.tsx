/** @jsx jsx */
import {React, LinkType, BaseWidget, AllWidgetProps, DataRecord, ExpressionPartType, ExpressionResolverErrorCode, appActions,
  RepeatedDataSource, IMExpression, IMDataSourceInfo, jsx, ExpressionResolverComponent, Immutable, IMState, AppMode, ThemeButtonType} from 'jimu-core';
import {styleUtils, Link, ButtonProps} from 'jimu-ui';
import {IMConfig} from '../config';
import {getStyle} from './style';

enum RepeatType { None, Main, Sub };

interface State{
  info: IMDataSourceInfo;
  text: string;
  toolTip: string;
  url: string;
  repeat: RepeatType;
  textExpression: IMExpression;
  tipExpression: IMExpression;
  urlExpression: IMExpression;
}

interface ExtraProps {
  active: boolean;
  showQuickStyle: boolean;
  appMode: AppMode;
}

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{
  domNode: React.RefObject<HTMLDivElement>;

  constructor(props) {
    super(props);
    this.state = {
      repeat: 0,
      info: null,
      text: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.text || 'button',
      toolTip: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.toolTip || '',
      url: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.linkParam && this.props.config.functionConfig.linkParam.value || '',
      textExpression: this.props.useDataSourcesEnabled && this.getTextExpression(),
      tipExpression: this.props.useDataSourcesEnabled && this.getTipExpression(),
      urlExpression: this.props.useDataSourcesEnabled && this.getUrlExpression()
    };
    this.domNode = React.createRef();
  }

  static mapExtraStateProps = (state: IMState, ownProps: AllWidgetProps<IMConfig>): ExtraProps => {
    let selected = false;
    const selection = state.appRuntimeInfo.selection;
    if (selection && state.appConfig.layouts[selection.layoutId]) {
      const layoutItem = state.appConfig.layouts[selection.layoutId].content[selection.layoutItemId];
      selected = layoutItem && layoutItem.widgetId === ownProps.id
    }
    const isInBuilder = state.appContext.isInBuilder;
    const active = isInBuilder && selected;

    const widgetState = state.widgetsState[ownProps.id] || Immutable({});
    const showQuickStyle = !!widgetState.showQuickStyle;
    return {
      active,
      showQuickStyle,
      appMode: state.appRuntimeInfo.appMode
    }
  };

  componentDidMount(){
    this.setRepeatType();
    this.props.dispatch(appActions.widgetStatePropChange(this.props.id, 'showQuickStyle', true));
  }

  componentDidUpdate(prevProps: AllWidgetProps<IMConfig> & ExtraProps, prevState: State){
    if(!this.props.useDataSourcesEnabled &&
      (
        this.props.config !== prevProps.config || prevProps.useDataSourcesEnabled
      )
    ){
      this.setState({
        text: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.text,
        toolTip: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.toolTip,
        url: this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.linkParam && this.props.config.functionConfig.linkParam.value
      });
    }

    if(this.props.useDataSourcesEnabled &&
      (
        this.props.config !== prevProps.config || !prevProps.useDataSourcesEnabled
      )
    ){
      this.setState({
        textExpression: this.getTextExpression(),
        tipExpression: this.getTipExpression(),
        urlExpression: this.getUrlExpression()
      });
    }

    if(
      (this.props.appMode !== prevProps.appMode && this.props.appMode === AppMode.Run) ||
      (this.props.active !== prevProps.active)
    ){
      this.props.dispatch(appActions.widgetStatePropChange(this.props.id, 'showQuickStyle', false));
    }
  }

  setRepeatType = () => {
    const repeatedDataSource = this.props.repeatedDataSource as RepeatedDataSource;
    let repeat;
    if (!repeatedDataSource) {
      repeat = RepeatType.None;
    } else {
      if (repeatedDataSource.recordIndex === 0) {
        repeat = RepeatType.Main;
      } else {
        repeat = RepeatType.Sub;
      }
    }
    this.setState({ repeat });
  }

  getTipExpression = (): IMExpression => {
    return this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.toolTipExpression &&
      this.props.config.functionConfig.toolTipExpression ||
      Immutable({
        name: 'default expression',
        parts: [{type: ExpressionPartType.String, exp: `"${this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.toolTip}"`}]
      });
  }

  getTextExpression = (): IMExpression => {
    return this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.textExpression &&
      this.props.config.functionConfig.textExpression ||
      Immutable({
        name: 'default expression', parts: [{type: ExpressionPartType.String, exp: `"${this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.text}"`}]
      });
  }

  getUrlExpression = (): IMExpression => {
    const expression = this.props.config && this.props.config.functionConfig && this.props.config.functionConfig.linkParam &&
      this.props.config.functionConfig.linkParam && this.props.config.functionConfig.linkParam.expression;

    return expression || null;
  }

  onTextExpResolveSucess = (result: string) => {
    this.setState({text: result});
  }

  onTipExpResolveSucess = (result: string) => {
    this.setState({toolTip: result});
  }

  onUrlExpResolveSucess = (result: string) => {
    this.setState({url: result});
  }

  onTextExpResolveError = error => {
    let result: string = '';
    let errorCode = error && error.errorCode;
    if(errorCode === ExpressionResolverErrorCode.Failed){
      result = this.state.textExpression && this.state.textExpression.name;
    }

    this.setState({text: result});
  }

  onTipExpResolveError = error => {
    let result: string = '';
    let errorCode = error && error.errorCode;
    if(errorCode === ExpressionResolverErrorCode.Failed){
      result = this.state.tipExpression && this.state.tipExpression.name;
    }

    this.setState({toolTip: result});
  }

  onUrlExpResolveError = error => {
    let result: string = '';
    let errorCode = error && error.errorCode;
    if(errorCode === ExpressionResolverErrorCode.Failed){
      result = this.state.urlExpression && this.state.urlExpression.name;
    }

    this.setState({url: result});
  }

  getRecordsFromRepeatedDataSource = (): { [dataSourceId: string]: DataRecord } => {
    let dataSourceId = this.props.useDataSources && this.props.useDataSources[0] && this.props.useDataSources[0].dataSourceId;

    if(dataSourceId && this.props.repeatedDataSource){
      let record = (this.props.repeatedDataSource as RepeatedDataSource).record as DataRecord;

      return {
        [dataSourceId]: record
      }
    }

    return null;
  }

  showQuickStylePanel = (): boolean => {
    const { repeat } = this.state;
    const { active } = this.props;
    return this.props.showQuickStyle && active && repeat !== RepeatType.Sub;
  }

  getWhetherUseQuickStyle = (config: IMConfig): boolean => {
    return !!(config && config.styleConfig && config.styleConfig.themeStyle && config.styleConfig.themeStyle.quickStyleType);
  }

  getCustomButtonType = (config: IMConfig): ThemeButtonType => {
    return config.styleConfig.themeStyle.color === 'link' ?  'link' : (config.styleConfig.themeStyle.outline ? 'secondary' : 'primary');
  }

  getThemeClassName = (config: IMConfig): string => {
    let themeClassName = '';

    const custtomButtonType = this.getCustomButtonType(config);
    if(config.styleConfig.themeStyle.rounded){
      themeClassName += ' widget-button-rounded';
    }
    if(config.styleConfig.themeStyle.color !== 'link'){
      themeClassName += ` widget-button-color-${config.styleConfig.themeStyle.color}`;
    }
    if(custtomButtonType === 'primary'){
      themeClassName += ' widget-button-default';
    }else if(custtomButtonType === 'secondary'){
      themeClassName += ' widget-button-outline';
    }

    return themeClassName;
  }

  getLinkComponent = () => {
    const config = this.props.config;
    const linkParam = config.functionConfig.linkParam;
    const text = this.state.text;
    const toolTip = this.state.toolTip;

    let linkType = linkParam.linkType as any;
    let style = styleUtils.toCSSStyle(config.styleConfig.customStyle.regular) as React.CSSProperties;
    let hoverStyle = styleUtils.toCSSStyle(config.styleConfig.customStyle.hover) as React.CSSProperties;
    let visitedStyle = styleUtils.toCSSStyle(config.styleConfig.customStyle.clicked) as React.CSSProperties;
    let tempTarget = linkParam.openType;

    let customStyle = {
      style,
      hoverStyle,
      visitedStyle
    };
    let useQuickStyle = this.getWhetherUseQuickStyle(config);
    let themeStyle: ButtonProps = {
      type: useQuickStyle ? config.styleConfig.themeStyle.quickStyleType : this.getCustomButtonType(config)
    };
    let baseThemeClassName = config.styleConfig.themeStyle.className + ' widget-button-link text-truncate w-100 h-100';
    let themeClassName = useQuickStyle ? baseThemeClassName : baseThemeClassName + ' ' + this.getThemeClassName(config);

    let LinkComponent;

    if (linkParam.linkType == LinkType.Page || linkParam.linkType == LinkType.Dialog || linkParam.linkType == LinkType.View) {
      LinkComponent = <Link to={{linkType: linkType, value: linkParam.value}}
        className={themeClassName} queryObject={this.props.queryObject}
        target={tempTarget} title={toolTip} customStyle={customStyle} themeStyle={themeStyle}
      >
        {text}
      </Link>;
    } else if (linkParam.linkType == LinkType.WebAddress) {
      let linkTo = this.state.url;

      LinkComponent = <Link to={linkTo} className={themeClassName} themeStyle={themeStyle}
        target={tempTarget} customStyle={customStyle} title={toolTip}
      >
        {text}
      </Link>
    } else {
      LinkComponent = <Link target={tempTarget} className={themeClassName} themeStyle={themeStyle}
        customStyle={customStyle} title={toolTip}
      >
        {text}
      </Link>
    }

    return LinkComponent;
  }

  onQuickStyleChange = (t: ThemeButtonType) => {
    let { builderSupportModules, config, id } = this.props
    const getAppConfigAction = builderSupportModules && builderSupportModules.jimuForBuilderLib.getAppConfigAction;
    if (getAppConfigAction) {
      getAppConfigAction().editWidgetProperty(id, 'config', config.setIn(['styleConfig', 'themeStyle', 'quickStyleType'], t)).exec();
    }
  }

  getQuickStyleComponent = () => {
    const QuickStyle = this.props.builderSupportModules && this.props.builderSupportModules.widgetModules && this.props.builderSupportModules.widgetModules.QuickStyle;
    return !QuickStyle ? null : <QuickStyle onChange={this.onQuickStyleChange} reference={this.domNode && this.domNode.current} />;
  }

  render() {
    const isDataSourceUsed = this.props.useDataSourcesEnabled;
    const showQuickStylePanel = this.showQuickStylePanel();

    const LinkComponent = this.getLinkComponent();
    const QuickStyleComponent = this.getQuickStyleComponent();

    return (
      <div className="jimu-widget widget-button w-100 h-100" css={getStyle(this.props.theme)} ref={this.domNode}>
        {LinkComponent}

        {
          showQuickStylePanel && QuickStyleComponent
        }

        <div style={{ display: 'none' }}>
          {
            isDataSourceUsed &&
            <div>
              <ExpressionResolverComponent useDataSources={this.props.useDataSources} expression={this.state.textExpression}
                records={this.getRecordsFromRepeatedDataSource()} onSuccess={this.onTextExpResolveSucess} onError={this.onTextExpResolveError}
              />
              <ExpressionResolverComponent useDataSources={this.props.useDataSources} expression={this.state.tipExpression}
                records={this.getRecordsFromRepeatedDataSource()} onSuccess={this.onTipExpResolveSucess} onError={this.onTipExpResolveError}
              />
              <ExpressionResolverComponent useDataSources={this.props.useDataSources} expression={this.state.urlExpression}
                records={this.getRecordsFromRepeatedDataSource()} onSuccess={this.onUrlExpResolveSucess} onError={this.onUrlExpResolveError}
              />
            </div>
          }
        </div>
      </div>
    )
  }
}
