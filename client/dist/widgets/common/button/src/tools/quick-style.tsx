import { extensionSpec, React, appActions, getAppStore, LayoutContextToolProps, Immutable } from 'jimu-core';
import { IMWidgetState } from '../config';
export default class QuickStyle implements extensionSpec.ContextTool {
  index = 2;
  id = 'button-quick-style';
  widgetId: string;

  visible(props: LayoutContextToolProps) {
    return true;
  }

  getGroupId() {
    return null;
  }

  getTitle() {
    return 'Quick style';
  }

  checked(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState: IMWidgetState = getAppStore().getState().widgetsState[widgetId] || Immutable({});
    return !!widgetState.showQuickStyle;
  }

  getIcon() {
    return require('jimu-ui/lib/icons/design.svg');
  }

  onClick(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState: IMWidgetState = getAppStore().getState().widgetsState[widgetId] || Immutable({});
    const showQuickStyle = !widgetState.showQuickStyle;

    getAppStore().dispatch(appActions.widgetStatePropChange(widgetId, 'showQuickStyle', showQuickStyle));
  }

  getSettingPanel(): React.ComponentClass<{}> {
    return null;
  }
}




