define({
  default: 'ã_Default_______________Ç',
  outline: 'ã_Outline_______________Ç',
  link: 'ã_Link_________Ç',
  large: 'ã_Large___________Ç',
  small: 'ã_Small___________Ç',
  primary: 'ã_Primary_______________Ç',
  secondary: 'ã_Secondary___________________Ç',
  success: 'ã_Success_______________Ç',
  info: 'ã_Info_________Ç',
  warning: 'ã_Warning_______________Ç',
  danger: 'ã_Danger_____________Ç',
  light: 'ã_Light___________Ç',
  dark: 'ã_Dark_________Ç',
  connectToData: 'ã_Connect to data________________Ç',
  setLink: 'ã_Set link_________________Ç',
  style: 'ã_Style___________Ç',
  size: 'ã_Size_________Ç',
  color: 'ã_Color___________Ç',
  text: 'ã_Text_________Ç',
  tooltip: 'ã_Tooltip_______________Ç',
  defaultRound: 'ã_Default round______________Ç',
  outlineRound: 'ã_Outline round______________Ç'
});