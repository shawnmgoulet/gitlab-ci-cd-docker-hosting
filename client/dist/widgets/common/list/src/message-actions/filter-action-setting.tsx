/** @jsx jsx */
import {React, css, jsx, ActionSettingProps, SerializedStyles, ImmutableObject, DataSource, IMDataSourceInfo,
  ReactDOM, themeUtils, ThemeVariables, getAppStore, Immutable, MultipleDataSourceComponent, 
  UseDataSource, DataSourceComponent, IMUseDataSource, IMFieldSchema, IMSqlExpression, 
  dataSourceUtils, DataSourceManager, MessageType, polished} from 'jimu-core';
import {Button, Icon, Switch} from 'jimu-ui';
import {ArcGISDataSourceTypes} from 'jimu-arcgis';
import { SqlExpressionBuilderPopup } from 'jimu-ui/sql-expression-builder';
import defaultMessages from '../setting/translations/default';
import { Fragment } from 'react';
import { DataSourceList, FieldSelector, DataSourceItem, SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
import { SqlExpressionMode } from 'jimu-ui/sql-expression-runtime';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';

interface ExtraProps {
  theme?: ThemeVariables;
}

interface States {
  isShowLayerList: boolean;
  currentLayerType: 'trigger' | 'action';
  isSqlExprShow: boolean;
}

interface Config {
  messageUseDataSource: UseDataSource;
  actionUseDataSource: UseDataSource;
  sqlExprObj?: IMSqlExpression;

  enabledDataRelationShip?: boolean;
  enableQueryWithCurrentExtent?: boolean;
}

export type IMConfig = ImmutableObject<Config>;

let IconClose = require('jimu-ui/lib/icons/close.svg');
let IconDeleteX = require('jimu-ui/lib/icons/close-12.svg');

class _FilterActionSetting extends React.PureComponent<ActionSettingProps<IMConfig> & ExtraProps, States>{

  modalStyle: any = {position: 'absolute', top: '0', bottom: '0', right: '260px', left: 'auto', width: '259px',
    height: 'auto', zIndex: '3', borderRight: '', borderBottom: '', paddingBottom: '1px'};

  constructor(props){
    super(props);

    this.modalStyle.borderRight = `1px solid black`;
    this.modalStyle.borderBottom = `1px solid black`;

    this.state = {
      isShowLayerList: false,
      currentLayerType: null,
      isSqlExprShow: false
    }
  }

  formatMessage = (id: string, values?: {[key: string]: any}) => {
    return this.props.intl.formatMessage({id: id, defaultMessage: defaultMessages[id]}, values)
  }

  openChooseLayerList = (currentLayerType: 'trigger' | 'action') => {
    this.setState({
      isShowLayerList: true,
      currentLayerType: currentLayerType
    });
  }

  closeChooseLayerList = () => {
    this.setState({
      isShowLayerList: false,
      currentLayerType: null
    });
  }

  static defaultProps = {
    config: Immutable({
      messageUseDataSource: null,
      actionUseDataSource: null,
      sqlExprObj: null,
      enabledDataRelationShip: true
    })
  }

  getInitConfig = () => {
    let messageWidgetId = this.props.messageWidgetId;
    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let messageWidgetJson = config.widgets[messageWidgetId];

    let messageUseDataSource = null;
    let actionUseDataSource = null;
    let enableQueryWithCurrentExtent = true;

    if (!this.props.config.messageUseDataSource) {
      if (messageWidgetJson && messageWidgetJson.useDataSources && messageWidgetJson.useDataSources[0] && messageWidgetJson.useDataSources.length === 1 ) {
        let dsJson = config.dataSources[messageWidgetJson.useDataSources[0].dataSourceId];
        if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
          messageUseDataSource = null;
        } else {
          messageUseDataSource = Immutable({
            dataSourceId: messageWidgetJson.useDataSources[0].dataSourceId,
            rootDataSourceId: messageWidgetJson.useDataSources[0].rootDataSourceId
          });
        }
      }
    } else {
      messageUseDataSource = this.props.config.messageUseDataSource;
    }

    if(this.props.config.enableQueryWithCurrentExtent !== undefined){
      enableQueryWithCurrentExtent = this.props.config.enableQueryWithCurrentExtent;
    }

    let actionWidgetId = this.props.widgetId;
    let actionWidgetJson = config.widgets[actionWidgetId];
    if (actionWidgetJson && actionWidgetJson.useDataSources && actionWidgetJson.useDataSources[0] && actionWidgetJson.useDataSources.length === 1 ) {
      let dsJson = config.dataSources[actionWidgetJson.useDataSources[0].dataSourceId];
      if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
        actionUseDataSource = null;
      } else {
        actionUseDataSource = {
          dataSourceId: actionWidgetJson.useDataSources[0].dataSourceId,
          rootDataSourceId: actionWidgetJson.useDataSources[0].rootDataSourceId
        };
      }
    }

    const oldActionUseDataSource = this.props.config.actionUseDataSource;
    if (oldActionUseDataSource) {
      if(actionUseDataSource){
        if(oldActionUseDataSource.dataSourceId === actionUseDataSource.dataSourceId &&
          oldActionUseDataSource.rootDataSourceId === actionUseDataSource.rootDataSourceId){
          actionUseDataSource = oldActionUseDataSource.asMutable({deep: true});
        }
      }
      if(!actionUseDataSource){
        actionUseDataSource = {fields: oldActionUseDataSource.fields}
      }else{
        actionUseDataSource.fields = oldActionUseDataSource.fields;
      }
    }

    return {
      messageUseDataSource: messageUseDataSource,
      actionUseDataSource: actionUseDataSource,
      enableQueryWithCurrentExtent,
    }
  }

  componentDidMount() {
    let initConfig = this.getInitConfig();

    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', initConfig.messageUseDataSource)
        .set('actionUseDataSource', initConfig.actionUseDataSource)
        .set('enableQueryWithCurrentExtent', initConfig.enableQueryWithCurrentExtent)
    });
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .equal-text {
        padding: ${polished.rem(4)} ${polished.rem(8)};
        background-color: ${theme.colors.palette.light[400]};
        color: ${theme.colors.palette.dark[600]};
        width: fit-content;
        height: auto;
        margin-top: ${polished.rem(10)};
        margin-bottom: ${polished.rem(10)};
      }

      .relate-panel {
        padding: ${polished.rem(10)} ${polished.rem(8)};
        background-color: ${theme.colors.palette.light[500]};
      }

      .relate-text {
        color: ${theme.colors.palette.dark[800]};
      }

      .setting-header {
        padding: ${polished.rem(10)} ${polished.rem(16)} ${polished.rem(0)} ${polished.rem(16)};
      }

      .deleteIcon {
        cursor: pointer;
        opacity: .8;
      }

      .deleteIcon:hover {
        opacity: 1;
      }

      .sql-expr-display {
        width: 100%;
        height: auto;
        min-height: ${polished.rem(80)};
        line-height: 25px;
        color: ${theme.colors.palette.dark[400]};
        border: 1px solid ${theme.colors.palette.dark[500]};
        padding: ${polished.rem(4)} ${polished.rem(6)};
      }
    `;
  }

  checkIsShowSetData = (widgetId: string) => {
    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let widgetJson = config.widgets[widgetId];
    if (widgetJson && widgetJson.useDataSources && widgetJson.useDataSources[0] && widgetJson.useDataSources.length === 1 ) {
      let dsJson = config.dataSources[widgetJson.useDataSources[0].dataSourceId];
      if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  getContent = () => {
    let widgetId = null;
    let currentActiveUseDataSource: IMUseDataSource = null;
    if (this.state.currentLayerType === 'trigger') {
      widgetId = this.props.messageWidgetId;
      currentActiveUseDataSource = this.props.config.messageUseDataSource;
    } else if (this.state.currentLayerType === 'action') {
      widgetId = this.props.widgetId;
      currentActiveUseDataSource = this.props.config.actionUseDataSource;
    } else {
      return null;
    }

    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let widgetJson = config.widgets[widgetId];
    if (widgetJson && widgetJson.useDataSources && widgetJson.useDataSources[0]) {
      return <MultipleDataSourceComponent useDataSources={widgetJson.useDataSources}>
        {(dss: { [dataSourceId: string]: DataSource }, infos: { [dataSourceId: string]: IMDataSourceInfo}) => {
          let fromRootDsIds = Immutable(Object.keys(dss));
          let types = [ArcGISDataSourceTypes.FeatureLayer];

          let selectedDsIds = [];
          if (currentActiveUseDataSource && currentActiveUseDataSource.dataSourceId) {
            selectedDsIds.push(currentActiveUseDataSource.dataSourceId);
          }

          return <DataSourceList isDataSourceInited={true} types={Immutable(types)} fromRootDsIds={fromRootDsIds} hideHeader={true} hideTypeDropdown={true} hideAddData={true}
            onSelect={(selectedDsJson: SelectedDataSourceJson) => {this.handleChooseLayer(selectedDsJson)}} selectedDsIds={Immutable(selectedDsIds)}></DataSourceList>
        }}
      </MultipleDataSourceComponent>
    } else {
      return null;
    }
  }

  handleChooseLayer = (selectedDsJson: SelectedDataSourceJson) => {
    let useDataSource: UseDataSource = {
      dataSourceId: selectedDsJson.dataSourceJson.id,
      rootDataSourceId: selectedDsJson.rootDataSourceId
    }

    if (this.state.currentLayerType === 'trigger') {
      this.props.onSettingChange({
        actionId: this.props.actionId,
        config: this.props.config.set('messageUseDataSource', useDataSource)
      })
    }

    if (this.state.currentLayerType === 'action') {
      this.props.onSettingChange({
        actionId: this.props.actionId,
        config: this.props.config.set('actionUseDataSource', useDataSource)
      })
    }

    this.setState({
      isShowLayerList: false,
      currentLayerType: null
    });
  }

  handleRemoveLayerItemForTriggerLayer = () => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', null)
    })
  }

  handleRemoveLayerItemForActionLayer = () => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('actionUseDataSource', null)
    })
  }

  showSqlExprPopup = () => {
    this.setState({isSqlExprShow: true});
  }

  toggleSqlExprPopup = () => {
    this.setState({isSqlExprShow: !this.state.isSqlExprShow});
  }

  onSqlExprBuilderChange = (sqlExprObj: IMSqlExpression) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('sqlExprObj', sqlExprObj)
    })
  }

  onMessageFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', {
        dataSourceId: this.props.config.messageUseDataSource.dataSourceId,
        rootDataSourceId: this.props.config.messageUseDataSource.rootDataSourceId,
        fields: [field.jimuName]
      })
    })
  }

  onActionFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('actionUseDataSource', {
        dataSourceId: this.props.config.actionUseDataSource.dataSourceId,
        rootDataSourceId: this.props.config.actionUseDataSource.rootDataSourceId,
        fields: [field.jimuName]
      })
    })
  }

  swicthEnabledDataRelationShip = (checked) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('enabledDataRelationShip', checked)
    })
  }

  swicthEnabledQueryWithCurrentExtent = (checked) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('enableQueryWithCurrentExtent', checked)
    })
  }

  checkTrigerLayerIsSameToActionLayer = () => {
    if (this.props.config.messageUseDataSource && this.props.config.actionUseDataSource) {
      if (this.props.config.messageUseDataSource.dataSourceId === this.props.config.actionUseDataSource.dataSourceId &&
        this.props.config.messageUseDataSource.rootDataSourceId === this.props.config.actionUseDataSource.rootDataSourceId) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  render(){
    let isShowSetDataForTriggerLayer = this.checkIsShowSetData(this.props.messageWidgetId);
    let actionUseDataSourceInstance = this.props.config.actionUseDataSource 
      && DataSourceManager.getInstance().getDataSource(this.props.config.actionUseDataSource.dataSourceId);
    const {messageType, config} = this.props;
    const isExtendChange = messageType === MessageType.ExtentChange;
    return <div css={this.getStyle(this.props.theme)}>
      {!isExtendChange &&
        <Fragment>
          <SettingSection title={this.formatMessage('triggerLayer')}>
            <SettingRow flow="wrap">
              <Button type="primary" className="w-100" onClick={() => {this.openChooseLayerList('trigger')}}>{this.formatMessage('setData')}</Button>
            </SettingRow>
            <SettingRow>
              {this.props.config.messageUseDataSource && <LayerItem onRemovedLayerItem={() => {this.handleRemoveLayerItemForTriggerLayer()}} 
                useDataSource={this.props.config.messageUseDataSource} isHiddenCloseIcon={!isShowSetDataForTriggerLayer}></LayerItem>}
            </SettingRow>
          </SettingSection>
          {
            this.state.isShowLayerList && <div>{ReactDOM.createPortal(
              <div className="bg-light-300 border-color-gray-400" style={this.modalStyle}>
                <div className="w-100">
                  <div className="w-100 d-flex align-items-center justify-content-between setting-header border-0">
                    <div className="setting-title mt-1" style={{maxWidth: '150px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} title={'Choose Layer'}>
                      {'Choose Layer'}
                    </div>
                    <div style={{cursor: 'pointer'}} onClick={this.closeChooseLayerList}><Icon icon={IconClose} size="12" className="text-dark"/></div>
                  </div>
                  <div>
                    {this.state.currentLayerType ? this.getContent() : null }
                  </div>
                </div>
              </div>
            , document.getElementById('default'))}</div>
          }
        </Fragment>
      }
      
      {(!!config.messageUseDataSource || isExtendChange) &&
        <Fragment>
          <SettingSection title={this.formatMessage('conditions')}>
            {isExtendChange &&
              <SettingRow label={this.formatMessage('queryWithCurrentExtent')}>
                <Switch checked={config.enableQueryWithCurrentExtent} onChange={evt => {this.swicthEnabledQueryWithCurrentExtent(evt.target.checked)}}/> 
              </SettingRow>
            }
            {!isExtendChange &&
              <SettingRow label={this.formatMessage('relateMessage')}>
                <Switch checked={config.enabledDataRelationShip} onChange={evt => {this.swicthEnabledDataRelationShip(evt.target.checked)}}/> 
              </SettingRow>
            }
            {!isExtendChange && this.props.config.enabledDataRelationShip &&
                <SettingRow flow="wrap">
                  {this.checkTrigerLayerIsSameToActionLayer() && 
                  <div className="w-100 border p-1 mr-2">{this.formatMessage('twoLayersAutoBind')}</div>}
                  {!this.checkTrigerLayerIsSameToActionLayer() && <div className="w-100 relate-panel">
                    <div className="w-100">
                      <div className="h6 relate-text">{this.formatMessage('triggerLayerField')}</div>
                      {(!this.props.config.messageUseDataSource) && <div className="mt-2">none</div>}
                      <div className="w-100">
                        <DataSourceComponent useDataSource={this.props.config.messageUseDataSource}>
                            {
                              (ds: DataSource) => <div>
                                <FieldSelector className="w-100 mt-2"
                                dataSources={[ds]} isDataSourceDropDownHidden={true}
                                onSelect={this.onMessageFieldSelected} useDropdown={true} isSearchInputHidden={true}
                                selectedFields={this.props.config.messageUseDataSource && this.props.config.messageUseDataSource.fields
                                  ? this.props.config.messageUseDataSource.fields : Immutable([])}/>
                              </div>
                            }
                          </DataSourceComponent>
                      </div>
                    </div>
                    <div 
                      className="equal-text" 
                      >
                      {this.formatMessage('equals')}
                    </div>
                    <div className="w-100">
                      <div className="h6 relate-text" >{this.formatMessage('actionLayerField')}</div>
                      {(!this.props.config.actionUseDataSource) && <div className="mt-2">{this.formatMessage('none')}</div>}
                      <div className="w-100">
                        <DataSourceComponent useDataSource={this.props.config.actionUseDataSource}>
                            {
                              (ds: DataSource) => <div>
                                <FieldSelector className="w-100 mt-2"
                                dataSources={[ds]} isDataSourceDropDownHidden={true}
                                onSelect={this.onActionFieldSelected} useDropdown={true} isSearchInputHidden={true}
                                selectedFields={this.props.config.actionUseDataSource && this.props.config.actionUseDataSource.fields
                                  ? this.props.config.actionUseDataSource.fields : Immutable([])}/>
                              </div>
                            }
                          </DataSourceComponent>
                      </div>
                    </div>
                  </div>}
                </SettingRow>
            }
            <SettingRow label={
            <Button className="p-0" type="link" disabled={!this.props.config.actionUseDataSource} 
            onClick={this.showSqlExprPopup}>{this.formatMessage('moreConditions')}</Button>
            }>
            </SettingRow>
            <SettingRow>
              {this.props.config.actionUseDataSource && <DataSourceComponent useDataSource={this.props.config.actionUseDataSource}>{(ds) => {
                return <SqlExpressionBuilderPopup selectedDs={ds} mode={SqlExpressionMode.Simple}
                  isOpen={this.state.isSqlExprShow} toggle={this.toggleSqlExprPopup}
                  config = {this.props.config.sqlExprObj} onChange={(sqlExprObj) => {this.onSqlExprBuilderChange(sqlExprObj)}}
                  id="filter-widget-sql-expression-builder-popup" />
              }}</DataSourceComponent>}
              <div className="sql-expr-display body-1">
                {this.props.config.sqlExprObj && actionUseDataSourceInstance
                  ? dataSourceUtils.getArcGISSQL(this.props.config.sqlExprObj, actionUseDataSourceInstance).displaySQL 
                    : this.formatMessage('setExprTip')}
              </div>
            </SettingRow>
          </SettingSection>
          
        </Fragment>
      }
      
    </div>
  }
}

interface LayerItemProps {
  useDataSource?: IMUseDataSource;
  onRemovedLayerItem?: (useDataSource?: IMUseDataSource) => void;
  isHiddenCloseIcon?: boolean;
}

interface LayerItemStates {
  LoadError: boolean;
}

class LayerItem extends React.PureComponent<LayerItemProps, LayerItemStates>{
  
  constructor(props){
    super(props);

    this.state = {
      LoadError: false
    }
  }

  getMapLabel = () => {
    if (this.props.useDataSource && this.props.useDataSource.rootDataSourceId) {
      let datasourceinfos = getAppStore().getState().appStateInBuilder.appConfig.dataSources;
      if (datasourceinfos[this.props.useDataSource.rootDataSourceId]) {
        return datasourceinfos[this.props.useDataSource.rootDataSourceId].label;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  handleRemoveLayerItem = () => {
    this.props.onRemovedLayerItem && this.props.onRemovedLayerItem(this.props.useDataSource);
  }

  handleCreateDataSourceFailed = () => {
    this.setState({
      LoadError: true
    });
  }

  render() {
    if (this.state.LoadError) {
      return null;
    } else {
      return <DataSourceComponent useDataSource={this.props.useDataSource} onCreateDataSourceFailed={() => {this.handleCreateDataSourceFailed}}>
        {(dataSource: DataSource) => {
          return  <div className="w-100 border pr-1 pl-1 pb-1 mb-2">
            <div className="mt-2 d-flex justify-content-between align-items-center">
              <div>{this.getMapLabel()}</div>
              {!this.props.isHiddenCloseIcon && <div onClick={() => this.handleRemoveLayerItem()}>
                <Icon className="p-0 deleteIcon" width={14} height={14} icon={IconDeleteX}/>
              </div>}
              {this.props.isHiddenCloseIcon && <div></div>}
          </div>
          <DataSourceItem className="mt-2" dataSourceJson={dataSource.dataSourceJson}></DataSourceItem></div>
        }}
      </DataSourceComponent> 
    }
  }
}

// export default themeUtils.withTheme(ReactRedux.connect<StateProps, {}, ActionSettingProps<IMConfig>>(
//   (state: IMState, props: ActionSettingProps<IMConfig>) => {
//     const {widgetId} = props
//     const appConfig = state && state.appStateInBuilder && state.appStateInBuilder.appConfig;
//     if(!appConfig)return null;
//     let actionWidgetId = widgetId;
//     let actionWidgetJson = appConfig.widgets[actionWidgetId];
//     const useDS = actionWidgetJson && actionWidgetJson.useDataSources && actionWidgetJson.useDataSources[0]
//     return{
//       dataSourceId: useDS && useDS.dataSourceId,
//       rootDataSourceId: useDS && useDS.rootDataSourceId
//     }
//   }
// )(_FilterActionSetting));
export default themeUtils.withTheme(_FilterActionSetting);