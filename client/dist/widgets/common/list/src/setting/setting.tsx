/** @jsx jsx */
import { classNames, Immutable, DataSource, FieldSchema, IMState,
  IMAppConfig, ThemeVariables, SerializedStyles, css, jsx, IMThemeVariables, utils,
  DataSourceSchema, polished, AppMode, BrowserSizeMode, WidgetManager, UseDataSource, DataSourceComponent,
  appConfigUtils, getAppStore, ImmutableObject, LayoutItemType, ImmutableArray, 
  DataSourceJson, JimuFieldType, OrderByOption, IMSqlExpression, SqlExpression } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps,
  getAppConfigAction, builderAppSync,
} from 'jimu-for-builder';
import { SettingSection, SettingRow, SortSetting, SortSettingOption } from 'jimu-ui/setting-components';
import { BackgroundSetting, BorderSetting, FourSides } from 'jimu-ui/style-setting-components';
import { Input, Switch, Icon, BorderSides, Button, ButtonGroup, MultiSelect, MultiSelectItem, Collapse } from 'jimu-ui';
import { DataSourceSelector, AllDataSourceTypes, SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
import { IMConfig, PageStyle, ItemStyle, DirectionType, SelectionModeType, Status, CardSize } from '../config';
import defaultMessages from './translations/default';
import { Fragment } from 'react';
import {SqlExpressionBuilderPopup } from 'jimu-ui/sql-expression-builder';
import { handleResizeCard } from '../common-builder-support';
import { getJimuFieldNamesBySqlExpression } from 'jimu-ui/sql-expression-runtime';

const prefix = 'jimu-widget-';

const arrowDown = require('jimu-ui/lib/icons/arrow-down-12.svg');
const arrowUp = require('jimu-ui/lib/icons/arrow-up-12.svg');

const style0 = Immutable(require('./template/card-style0.json'));
const style1 = Immutable(require('./template/card-style1.json'));
const style2 = Immutable(require('./template/card-style2.json'));
const style3 = Immutable(require('./template/card-style3.json'));
const style4 = Immutable(require('./template/card-style4.json'));
const style5 = Immutable(require('./template/card-style5.json'));
const style6 = Immutable(require('./template/card-style6.json'));

const AllStyles = {
  STYLE0: style0,
  STYLE1: style1,
  STYLE2: style2,
  STYLE3: style3,
  STYLE4: style4,
  STYLE5: style5,
  STYLE6: style6,
}

const directions = [
  {icon: require('jimu-ui/lib/icons/direction-right.svg'), value: DirectionType.Horizon},
  {icon: require('jimu-ui/lib/icons/direction-down.svg'), value: DirectionType.Vertical},
]

// const getAlignTypes = (direction: DirectionType) => {
//   return direction === DirectionType.Horizon ? [
//     {icon: require('jimu-ui/lib/icons/align-v-top-10.svg'), value: AlignType.Start},
//     {icon: require('jimu-ui/lib/icons/align-v-center-10.svg'), value: AlignType.Center},
//     {icon: require('jimu-ui/lib/icons/align-v-bottom-10.svg'), value: AlignType.End}
//   ] :
//   [
//     {icon: require('jimu-ui/lib/icons/align-h-left-10.svg'), value: AlignType.Start},
//     {icon: require('jimu-ui/lib/icons/align-h-center-10.svg'), value: AlignType.Center},
//     {icon: require('jimu-ui/lib/icons/align-h-right-10.svg'), value: AlignType.End}
//   ]
// }

const defaultConfig = Immutable(require('../../config.json'));

// const pageTransitions = [
//   {label: 'Glide', icon: require('./assets/img-glide.svg'), value: PageTransitonType.Glide},
//   {label: 'Fade', icon: require('./assets/img-fade.svg'), value: PageTransitonType.Fade},
//   {label: 'Float', icon: require('./assets/img-float.svg'), value: PageTransitonType.Float},
// ]

interface State {
  datasource: DataSource;
  fields: { [jimuName: string]: FieldSchema };
  isTextExpPopupOpen: boolean;
  currentTextInput: string;
  isTipExpPopupOpen: boolean;
  isLinkSettingShown: boolean;
  isSqlExprShow: boolean;
  showItemStyle: boolean;
  showTools: boolean;
}

interface ExtraProps{
  appConfig: IMAppConfig;
  appTheme: ThemeVariables;
  appMode: AppMode;
  browserSizeMode: BrowserSizeMode;
  showCardSetting: Status;
}

interface CustomeProps{
  theme: IMThemeVariables
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig> & ExtraProps & CustomeProps, State> {
  dsJsonWidthRootId: SelectedDataSourceJson;
  supportedTypes = Immutable([AllDataSourceTypes.FeatureLayer, AllDataSourceTypes.FeatureQuery]);
  needUpdateFields: boolean;

  lastHoverLayout = {
    layout: [],
    widgets: {}
  };
  lastSelectedLayout = {
    layout: [],
    widgets: {}
  };
  static mapExtraStateProps = (state: IMState, props: AllWidgetSettingProps<IMConfig>) => {
    const {id} = props;
    return {
      appConfig: state && state.appStateInBuilder && state.appStateInBuilder.appConfig,
      appTheme: state && state.appStateInBuilder && state.appStateInBuilder.theme,
      appMode: state && state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo && state.appStateInBuilder.appRuntimeInfo.appMode,
      browserSizeMode: state && state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
      showCardSetting: state && state.appStateInBuilder && state.appStateInBuilder.widgetsState
         && state.appStateInBuilder.widgetsState[id] && state.appStateInBuilder.widgetsState[id]['showCardSetting'] || Status.Regular,
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      datasource: null,
      fields: {},
      isTextExpPopupOpen: false,
      currentTextInput: '',
      isTipExpPopupOpen: false,
      isLinkSettingShown: false,
      isSqlExprShow: false,
      showItemStyle: false,
      showTools: true
    };

    let {config, id, useDataSourcesEnabled, appConfig} = props;
    if(!config.isInitialed){
      if(!useDataSourcesEnabled){
        appConfig = getAppConfigAction().editWidget(appConfig.widgets[id].set('useDataSourcesEnabled', true)).appConfig;
      }
      this.onItemStyleChanged(config.itemStyle, appConfig);
    }
  }

  onPropertyChange = (name, value) => {
    const {config} = this.props;

    if(value === config[name]){
      return;
    }
    if(name === 'sorts' || name === 'filter' || name === 'searchFields'){
      this.needUpdateFields = true;
    }
    const newConfig = config.set(name, value);
    this.onConfigChange(newConfig);
  };

  changeUsedFields = () => {
    const {useDataSources} = this.props;
    if(useDataSources && useDataSources[0]){
      const useDS = useDataSources[0].asMutable({deep: true});
      useDS.fields = this.getAllFields();
      const alterProps = {
        id: this.props.id,
        useDataSources: [useDS]
      }
      this.props.onSettingChange(alterProps);
    }
  }

  onConfigChange = (newConfig) => {
    
    const alterProps = {
      id: this.props.id,
      config: newConfig
    };
    this.props.onSettingChange(alterProps);

  };

  onBackgroundStyleChange = (status: Status, key, value) => {
    let {config} = this.props;
    config = config.setIn(['cardConfigs', status, 'backgroundStyle', key], value);
    this.onConfigChange(config);
  }

  onSelectionModeChange = (value) => {
    let {config, id, layouts, browserSizeMode, appConfig} = this.props;
    if(config.cardConfigs[Status.Selected].selectionMode === value){
      return;
    }
    let action = getAppConfigAction();
    if(config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None && value === SelectionModeType.None){//remove selected layout
      const desLayoutId = utils.findLayoutId(layouts[Status.Selected], browserSizeMode, appConfig.mainSizeMode);
      action = action.resetLayout(desLayoutId, true);
      this.changeBuilderStatus(Status.Regular);
    }else if(config.cardConfigs[Status.Selected].selectionMode === SelectionModeType.None && value !== SelectionModeType.None){
      const oriLayoutId = utils.findLayoutId(layouts[Status.Regular], browserSizeMode, appConfig.mainSizeMode);
      const desLayoutId = utils.findLayoutId(layouts[Status.Selected], browserSizeMode, appConfig.mainSizeMode);
      action = action.duplicateLayoutItems(oriLayoutId, desLayoutId, false);
      this.changeBuilderStatus(Status.Selected);
    }
    config = config.setIn(['cardConfigs', Status.Selected, 'selectionMode'], value);
    action.editWidgetConfig(id, config).exec();
  }

  onSelectionSwitch = (evt) => {
    const selected = evt.target.checked;
    if(selected){
      this.onSelectionModeChange(SelectionModeType.Single);
      
    }else {
      this.onSelectionModeChange(SelectionModeType.None);
    }
  }

  onHoverLayoutOpenChange = (evt) => {
    let {config, id, layouts, browserSizeMode, appConfig} = this.props;
    const value = evt.target.checked;
    if(config.cardConfigs[Status.Hover].enable === value)return;
    let action = getAppConfigAction();
    if(config.cardConfigs[Status.Hover].enable && !value){//remove hover layout
      const desLayoutId = utils.findLayoutId(layouts[Status.Hover], browserSizeMode, appConfig.mainSizeMode);
      action = action.resetLayout(desLayoutId, true);
      this.changeBuilderStatus(Status.Regular);
    }else if(!config.cardConfigs[Status.Hover].enable && value){
      const oriLayoutId = utils.findLayoutId(layouts[Status.Regular], browserSizeMode, appConfig.mainSizeMode);
      const desLayoutId = utils.findLayoutId(layouts[Status.Hover], browserSizeMode, appConfig.mainSizeMode);
      action = action.duplicateLayoutItems(oriLayoutId, desLayoutId, false);
      this.changeBuilderStatus(Status.Hover);
    }
    config = config.setIn(['cardConfigs', Status.Hover, 'enable'], value);
    action.editWidgetConfig(id, config).exec();
  }

  onOpenCardSetting = (evt, status) => {
    this.changeCardSettingAndBuilderStatus(status)
  }

  onExportClick = (evt) => {
    let {appConfig, layouts, config, id, browserSizeMode} = this.props;
    const currentPageId = getAppStore().getState().appStateInBuilder.appRuntimeInfo.currentPageId;
    const pageJson = appConfig.pages[currentPageId === 'default' ? 'home' : currentPageId];

    const pageTemplates = [
      {
        config: {
          ...config,
          isInitialed: false,
          isItemStyleConfirm: false
        },
        layouts: layouts.without(Status.Selected, Status.Hover),
        allLayouts: appConfig.layouts.without(pageJson.layout[browserSizeMode], layouts[Status.Selected][browserSizeMode], layouts[Status.Hover][browserSizeMode]),
        widgets: appConfig.widgets.without(id),
        views: appConfig.views,
        sections: appConfig.sections
      }
    ]

    const template0 = pageTemplates[0];
    template0.allLayouts && Object.keys(template0.allLayouts).forEach(layoutId => {
      let layoutJson = template0.allLayouts[layoutId].without('id');
      layoutJson.content && Object.keys(layoutJson.content).forEach(lEId => {
        const lEJson = (layoutJson.content[lEId] as any).without('id', 'parentId', 'layoutId').setIn(['setting', 'lockParent'], true);
        layoutJson = layoutJson.setIn(['content', lEId], lEJson);
      })
      template0.allLayouts = template0.allLayouts.set(layoutId, layoutJson);
    })

    template0.widgets && Object.keys(template0.widgets).forEach((wId, index) => {
      const wJson = template0.widgets[wId];
      template0.widgets = template0.widgets.set(wId, wJson.without('context', 'icon', 'label', 'manifest', '_originManifest',
       'version', 'id', 'useDataSourcesEnabled', 'useDataSources'))
    })

    template0.sections && Object.keys(template0.sections).forEach((sId, index) => {
      const sJson = template0.sections[sId];
      template0.sections = template0.sections.set(sId, sJson.without('id', 'label'));
    })

    template0.views && Object.keys(template0.views).forEach((vId, index) => {
      const vJson = template0.views[vId];
      template0.views = template0.views.set(vId, vJson.without('id', 'label'));
    })
    console.log(JSON.stringify(pageTemplates[0]));
    

    // const wJson = appConfig.widgets[this.props.id];
    // let embedLayoutJson = appConfig.layouts[wJson.layouts[Status.Regular].LARGE]

    // const template = {
    //   cardSize: config.cardSize,
    //   cardSpace: config.space,
    //   layout: [],
    //   widgets: {}
    // }
    // Object.keys(embedLayoutJson).forEach((key) => {
    //   if(key === 'id' || key === 'ROOT_ID')return;
    //   let layoutEle = embedLayoutJson[key];
    //   if (layoutEle.type === LayoutItemType.Widget && layoutEle.widgetId){
    //     template.widgets[layoutEle.widgetId] = appConfig.widgets[layoutEle.widgetId].without('context', 'icon', 'label', 'manifest', '_originManifest', 'version', 'useDataSources');
    //     template.layout.push(layoutEle);
    //   }
    // });
    // console.log(JSON.stringify(template));
  }

  onCardSettingReturnBackClick = (evt) => {
    this.changeCardSettingAndBuilderStatus(Status.Regular);
  }

  showSqlExprPopup = () => {
    this.setState({isSqlExprShow: true});
  }

  toggleSqlExprPopup = () => {
    this.setState({isSqlExprShow: !this.state.isSqlExprShow});
  }

  onSqlExprBuilderChange = (sqlExprObj: IMSqlExpression) => {
    this.onPropertyChange('filter', sqlExprObj);
  }

  private changeCardSettingAndBuilderStatus = (status: Status) => {
    let {id, config} = this.props;
    builderAppSync.publishChangeWidgetStatePropToApp({
      widgetId: id,
      propKey: 'showCardSetting',
      value: status
    });
    if(status === Status.Regular ||
        (status === Status.Hover && config.cardConfigs[Status.Hover].enable) ||
        (status === Status.Selected && config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None)){
      this.changeBuilderStatus(status)
    }
  }

  private changeBuilderStatus = (status: Status) => {
    let {id} = this.props;
    builderAppSync.publishChangeWidgetStatePropToApp({
      widgetId: id,
      propKey: 'builderStatus',
      value: status
    });
  }

  onItemStyleChanged = (style: ItemStyle, updatedAppConfig = undefined) => {
    // if(this.props.appMode === AppMode.Run) return;
    let {appConfig, id, browserSizeMode} = this.props;
    if(updatedAppConfig){
      appConfig = updatedAppConfig;
    }
    const wJson = appConfig.widgets[id];

    // remove all widgets
    const willRemoveLayoutInfos = [];
    Object.keys(wJson.layouts).forEach(key => {
      const devices = wJson.layouts[key];
      Object.keys(devices).forEach(device => {
        const embedLayoutJson = appConfig.layouts[devices[device]]
        if(embedLayoutJson.content){
          Object.keys(embedLayoutJson.content).forEach(key => {
            let layoutEle = embedLayoutJson.content[key];
            willRemoveLayoutInfos.push({layoutId: embedLayoutJson.id, layoutItemId: layoutEle.id});
          })
        }
      })
      
    })
    
    // add new widgets in item style
    let embedLayoutJson = appConfig.layouts[utils.findLayoutId(wJson.layouts[Status.Regular], browserSizeMode, appConfig.mainSizeMode)];
    if(!embedLayoutJson.content){
      embedLayoutJson = embedLayoutJson.set('content', {});
    }
    let styleTemp = AllStyles[style];
    if(styleTemp && styleTemp.layouts && styleTemp.layouts[Status.Regular]){
      styleTemp = styleTemp.set('style', style);
      styleTemp = styleTemp.setIn(['config', 'isItemStyleConfirm'], false);
      if(styleTemp.widgets && Object.keys(styleTemp.widgets).length > 0){
        this.loadTemplateWidgets(styleTemp, appConfig, willRemoveLayoutInfos);
      }else {
        this.parseTemplate(styleTemp, appConfig, willRemoveLayoutInfos);
      }
    }else {
      willRemoveLayoutInfos.forEach((layoutInfo) => {
        appConfig = getAppConfigAction(appConfig).removeLayoutItem(layoutInfo, true).appConfig;
      })
      this._onItemStyleChange(appConfig, style, styleTemp);
    }
  }

  private getCardSize = (props): CardSize => {
    props = props || this.props;
    const {config, builderStatus, browserSizeMode} = props;
    let cardConfigs = config.cardConfigs[builderStatus];
    if(!cardConfigs || !cardConfigs.cardSize){
      cardConfigs = config.cardConfigs[Status.Regular];
    }
    let cardSize = cardConfigs.cardSize[browserSizeMode];
    if(!cardSize){
      cardSize = cardConfigs.cardSize[Object.keys(cardConfigs.cardSize)[0]]
    }
    return cardSize.asMutable({deep: true});
  }

  handleItemSizeChange = (value: string) => {
    if(!value || value === '') return;
    const valueInt = parseInt(value);
    const {config} = this.props;
    const oldCardSize = this.getCardSize(this.props);
    const ratio = oldCardSize.width / oldCardSize.height;
    if(config.direction === DirectionType.Horizon){
      oldCardSize.width = valueInt;
      if(config.lockItemRatio){
        oldCardSize.height = valueInt / ratio;
      }
    }else {
      oldCardSize.height = valueInt;
      if(config.lockItemRatio){
        oldCardSize.width = valueInt * ratio;
      }
    }
    handleResizeCard(this.props, oldCardSize).exec();
    
  }
  
  componentDidUpdate(preProps){
    const {useDataSources, config} = this.props;
    let fieldsWillChange = false;
    if(useDataSources !== preProps.useDataSources){
      if(!useDataSources || useDataSources.length < 1 || !preProps.useDataSources || preProps.useDataSources.length < 1 || 
          preProps.useDataSources[0].dataSourceId !== useDataSources[0].dataSourceId){
        this.onConfigChange(config.set('sorts', undefined).set('searchFields', undefined).set('filter', undefined));
        fieldsWillChange = true;
      }
    }

    if(this.needUpdateFields && !fieldsWillChange){
      this.needUpdateFields = false;
      this.changeUsedFields();
    }
    
  }

  loadTemplateWidgets = (templateJson: ImmutableObject<any>, appConfig: IMAppConfig, willRemoveLayoutInfos) => {
    let widgetLength = Object.keys(templateJson.widgets).length;
    const widgetIds = this.getUniqueIds(appConfig, 'widget', widgetLength);
    const appConfigAction = getAppConfigAction();
    templateJson.widgets && Object.keys(templateJson.widgets).forEach(wId => {
      WidgetManager.getInstance().handleNewWidgetJson(templateJson.widgets[wId].asMutable()).then((widgetJson) => {
        widgetJson.id = widgetIds[widgetIds.length - widgetLength];
        const widgetJsonLayouts = widgetJson.layouts;
        appConfigAction.createWidget(Immutable(widgetJson));
        let imWJson = appConfigAction.appConfig.widgets[widgetJson.id];
        imWJson.layouts && widgetJsonLayouts && Object.keys(imWJson.layouts).forEach(name => {
          const sizeLayouts = widgetJsonLayouts[name];
          if(sizeLayouts && Object.keys(sizeLayouts).length > 0){
            //delete created layout
            Object.keys(imWJson.layouts[name]).forEach(device => {
              const lId = imWJson.layouts[name][device];
              appConfigAction.appConfig = appConfigAction.appConfig.set('layouts', appConfigAction.appConfig.layouts.without(lId));
            })
            imWJson = imWJson.setIn(['layouts', name], sizeLayouts);
          }
        })
        templateJson = templateJson.setIn(['widgets', wId], imWJson);

        widgetLength --;
        if(widgetLength === 0){
          this.parseTemplate(templateJson, appConfigAction.appConfig, willRemoveLayoutInfos);
        }
      })
    })
  }

  parseTemplate = (templateJson: ImmutableObject<any>, appConfig: IMAppConfig, willRemoveLayoutInfos) => {

    const length = Object.keys(templateJson.allLayouts).length;
    const ids = this.getUniqueIds(appConfig, 'layout', length);
    Object.keys(templateJson.allLayouts).forEach((lId, index) => {
      const lJson = templateJson.allLayouts[lId];
      templateJson = templateJson.setIn(['allLayouts', lId], lJson.set('id', ids[index]));
    })
    if(templateJson.widgets && Object.keys(templateJson.widgets).length > 0){
      Object.keys(templateJson.widgets).forEach((wId) => {
        const wJson = templateJson.widgets[wId];
        wJson.layouts && Object.keys(wJson.layouts).forEach(name => {
          wJson.layouts[name] && Object.keys(wJson.layouts[name]).forEach(device => {
            const lId = wJson.layouts[name][device];
            templateJson = templateJson.setIn(['widgets', wId, 'layouts', name, device], templateJson.allLayouts[lId].id);
          })
        })
      })
    }

    if(templateJson.views && Object.keys(templateJson.views).length > 0){
      const length = Object.keys(templateJson.views).length;
      const ids = this.getUniqueIds(appConfig, 'view', length);
      const labels = this.getUniqueLabels(appConfig, 'view', length);
      Object.keys(templateJson.views).forEach((vId, index) => {
        let vJson = templateJson.views[vId];
        templateJson = templateJson.setIn(['views', vId], vJson.set('id', ids[index]).set('label', labels[index]));
        vJson.layout && Object.keys(vJson.layout).forEach(device => {
          const lId = vJson.layout[device];
          templateJson = templateJson.setIn(['views', vId, 'layout', device], templateJson.allLayouts[lId].id);
        })
      })
    }

    if(templateJson.sections && Object.keys(templateJson.sections).length > 0){
      const length = Object.keys(templateJson.sections).length;
      const ids = this.getUniqueIds(appConfig, 'section', length);
      const labels = this.getUniqueLabels(appConfig, 'section', length);
      Object.keys(templateJson.sections).forEach((sId, index) => {
        const sJson = templateJson.sections[sId];
        templateJson = templateJson.setIn(['sections', sId], sJson.set('id', ids[index]).set('label', labels[index]));
        sJson.views && sJson.views.forEach((vId, index) => {
          templateJson = templateJson.setIn(['sections', sId, 'views', index], templateJson.views[vId].id);
        })
      })
    }

    Object.keys(templateJson.allLayouts).forEach((lId) => {
      const lJson = templateJson.allLayouts[lId];
      lJson.content && Object.keys(lJson.content).forEach(lEId => {
        let lEle = lJson.content[lEId].set('id', lEId);
        if(lEle.layoutId){
          lEle = lEle.set('layoutId', lJson.id);
        }
        if(lEle.type === LayoutItemType.Section){
          lEle = lEle.set('sectionId', templateJson.sections[lEle.sectionId].id);
        }else if(lEle.type === LayoutItemType.Widget){
          lEle = lEle.set('widgetId', templateJson.widgets[lEle.widgetId].id);
        }else {
          if(lEle.setting && lEle.setting.layouts){
            Object.keys(lEle.setting.layouts).forEach(device => {
              const lId = lEle.setting.layouts[device];
              lEle = lEle.setIn(['setting', 'layouts', device], templateJson.allLayouts[lId].id);
            })
          }
        }
        templateJson = templateJson.setIn(['allLayouts', lId, 'content', lEId], lEle);
      })

    })

    //remove old contents
    willRemoveLayoutInfos.forEach((layoutInfo) => {
      appConfig = getAppConfigAction(appConfig).removeLayoutItem(layoutInfo, true).appConfig;
    })

    templateJson.widgets && Object.keys(templateJson.widgets).forEach(id => 
      appConfig = appConfig.setIn(['widgets', templateJson.widgets[id].id], templateJson.widgets[id]));
    templateJson.allLayouts && Object.keys(templateJson.allLayouts).forEach(id => 
      appConfig = appConfig.setIn(['layouts', templateJson.allLayouts[id].id], templateJson.allLayouts[id]));
    templateJson.sections && Object.keys(templateJson.sections).forEach(id => 
      appConfig = appConfig.setIn(['sections', templateJson.sections[id].id], templateJson.sections[id]));
    templateJson.views && Object.keys(templateJson.views).forEach(id => 
      appConfig = appConfig.setIn(['views', templateJson.views[id].id], templateJson.views[id]));

    const {id} = this.props;
    const wJson = appConfig.widgets[id];

    Object.keys(templateJson.layouts).forEach(name => {
      if(Object.keys(templateJson.layouts[name]).length < 1){
        templateJson = templateJson.set('layouts', templateJson.layous.without(name));
        return;
      }
      Object.keys(templateJson.layouts[name]).forEach(device => {
        const lId = templateJson.layouts[name][device];
        templateJson = templateJson.setIn(['layouts', name, device], templateJson.allLayouts[lId].id);
        appConfig = appConfig.setIn(['widgets', id, 'layouts', name, device], templateJson.layouts[name][device]);
        const oldLId = wJson.layouts[name][device];
        if (oldLId) {
          appConfig = appConfig.set('layouts', appConfig.layouts.without(oldLId));
        }
      })
    });

    Object.keys(wJson.layouts).forEach(name => {
      wJson.layouts[name] && Object.keys(wJson.layouts[name]).forEach(device => {
        if(templateJson.layouts[name] && templateJson.layouts[name][device])return;
        const config = defaultConfig.merge(templateJson.config);
        let embedLayoutJson = undefined;
        let sizeLayouts = templateJson.layouts[name];
        if(!sizeLayouts){
          const layoutKeys = Object.keys(templateJson.layouts);
          sizeLayouts = templateJson.layouts[layoutKeys[layoutKeys.length - 1]];
        }
        const length = Object.keys(sizeLayouts).length;
        embedLayoutJson = appConfig.layouts[sizeLayouts[Object.keys(sizeLayouts)[length - 1]]]

        if(!embedLayoutJson){
          return;
        }
        if(!embedLayoutJson.content){
          embedLayoutJson = embedLayoutJson.set('content', {});
        }
        const desLayoutId = wJson.layouts[name][device]
        appConfig = appConfig.setIn(['layouts', desLayoutId, 'type'], embedLayoutJson.type);
        if(name === Status.Selected ){
          if(config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None){
            //reference to 
            appConfig = getAppConfigAction(appConfig).duplicateLayoutItems(embedLayoutJson.id, desLayoutId, false).appConfig;
          }
        }else if(name === Status.Hover){
          if(config.cardConfigs[Status.Hover].enable){
            appConfig = getAppConfigAction(appConfig).duplicateLayoutItems(embedLayoutJson.id, desLayoutId, false).appConfig;
          }
        }else {
          appConfig = getAppConfigAction(appConfig).duplicateLayoutItems(embedLayoutJson.id, desLayoutId, false).appConfig;
        }
      })
    })

    this._onItemStyleChange(appConfig, templateJson.style, templateJson);
  }

  onSettingSortChange = (sortData: Array<SortSettingOption>, index?: number) => {
    this.onPropertyChange('sorts', sortData);
  };

  getUniqueIds = (appConfig: IMAppConfig, type: 'page' | 'layout' | 'widget' | 'section' | 'view', size: Number) : string[] => {
    const ids: string[] = [];
    for(let i = 0; i < size; i ++){
      const id = appConfigUtils.getUniqueId(appConfig, type);
      ids.push(id);
      appConfig = appConfig.setIn([type + 's', id], {id: id} as any);
    }
    return ids;
  }

  getUniqueLabels = (appConfig: IMAppConfig, type: 'page' | 'layout' | 'widget' | 'section' | 'view', size: Number) : string[] => {
    const labels: string[] = [];
    for(let i = 0; i < size; i ++){
      const id = appConfigUtils.getUniqueId(appConfig, type);
      const label = appConfigUtils.getUniqueLabel(appConfig, type, type);
      labels.push(label);
      appConfig = appConfig.setIn([type + 's', id], {id: id, label: label} as any);
    }
    return labels;
  }

  private _onItemStyleChange = (appConfig, style, templateJson) => {
    let {id} = this.props;
    let config = defaultConfig;
    const appConfigAction = getAppConfigAction(appConfig);
    if(templateJson){
      config = config.merge(templateJson.config || {});
    }
    
    let wJson = appConfig.widgets[id];
    //process inherit properties
    if(wJson.useDataSources && wJson.useDataSources.length > 0){
      appConfigAction.copyUseDataSourceToAllChildWidgets(wJson.set('useDataSources', null), wJson);
    }

    config = config.set('itemStyle', style);
    if(!config.isInitialed){
      config = config.set('isInitialed', true);
    }
    appConfigAction.editWidgetProperty(wJson.id, 'config', config).exec();
    // selectSelf(this.props);
  }


  setDatasource = (ds: DataSource) => {
    let schema = ds && ds.getSchema();
    this.setState({
      datasource: ds,
      fields: (schema as DataSourceSchema).fields as { [jimuName: string]: FieldSchema }
    });
  }

  getIndexForPickerData(value, data){
    let index = -1;
    data.some((d, i) => {
      if(value === d.value){
        index = i;
        return true;
      }
    })
    return index;
  }

  getSelectModeOptions = (): JSX.Element[] => {
    return [
      <option key={SelectionModeType.Single} value={SelectionModeType.Single}>{this.formatMessage('single')}</option>,
      <option key={SelectionModeType.Multiple} value={SelectionModeType.Multiple}>{this.formatMessage('multiple')}</option>,
    ]
  }

  getSearchingFields = (isSearch: boolean): MultiSelectItem[] => {
    const {datasource} = this.state;
    if(datasource){
      const scheme = datasource.getSchema();
      if(scheme && scheme.fields){
        const res = [];
        Object.keys(scheme.fields).forEach(fieldKey => {
          const field = scheme.fields[fieldKey];
          if(isSearch){
            if(field.type == JimuFieldType.String){
              res.push({
                value: fieldKey,
                label: scheme.fields[fieldKey].alias || scheme.fields[fieldKey].name
              });
            }
          }else {
            res.push({
              value: fieldKey,
              label: scheme.fields[fieldKey].alias || scheme.fields[fieldKey].name
            });
          }
        })
        return res;
      }
    }
    return []
  }

  getPageStyleOptions = (): JSX.Element[] => {
    return [
      <option key={PageStyle.Scroll} value={PageStyle.Scroll}>{this.formatMessage('scroll')}</option>,
      <option key={PageStyle.MultiPage} value={PageStyle.MultiPage}>{this.formatMessage('multiPage')}</option>
    ]
  }

  onDsCreate = ds => {
    this.setDatasource(ds)
  };

  getStyle = (theme: ThemeVariables): SerializedStyles => {
    return css`
      &.jimu-widget-list-setting{
        .sort-container {
          margin-top: 12px;
          .sort-multi-select {
            width: 100%;
          }
        }

        .search-container {
          margin-top: 12px;
          .search-multi-select {
            width: 100%;
          }
        }

        .resetting-template {
          cursor: pointer;
          color: ${theme.colors.palette.primary[700]};
        }

        .resetting-template:hover {
          cursor: pointer;
          color: ${theme.colors.palette.primary[800]};
        }
        
        .setting-next {
          width: 38px;
          justify-content: space-between;
          align-items: center;
          cursor: pointer;
          text-align: end;
          font-size: ${polished.rem(13)};
        }

        .card-setting-return {
          cursor: pointer;
        }

        .style-group {
          .style-img {
            cursor: pointer;
            width: 100%;
            height: 70px;
            border: 1px solid ${theme.colors.palette.light[500]};
            background-color: ${theme.colors.white};
            &.active {
              border: 2px solid ${theme.colors.primary};
            }
            &.style-img-h {
              width: 109px;
              height: 109px;
            }
            &.low {
              height: 48px;
            }
            &.empty {
              height: 40px;
              color: ${theme.colors.palette.dark[200]};
            }
          }
          .vertical-space {
            height: 10px;
          }

        }
      }
    `
  }

  getAllFields = (): string[] => {
    const {config, useDataSources} = this.props;
    const useDS = useDataSources && useDataSources[0];
    if(!useDS)return [];

    const usedFields = {};
    if(config.sortOpen && config.sorts){
      config.sorts.forEach(sort => {
        sort.rule.forEach(sortData => {
          sortData.jimuFieldName && (usedFields[sortData.jimuFieldName] = 0);
        })
      })
    }
    if(useDS.query && useDS.query.orderBy && useDS.query.orderBy.length > 0){
      useDS.query.orderBy.forEach(sortData => {
        sortData.jimuFieldName && (usedFields[sortData.jimuFieldName] = 0);
      })
    }

    if(useDS.query && useDS.query.where){
      (getJimuFieldNamesBySqlExpression(useDS.query.where) || []).forEach(field => usedFields[field] = 0)
    }

    if(config.filter){
      (getJimuFieldNamesBySqlExpression(config.filter) || []).forEach(field => usedFields[field] = 0)
    }

    if(config.searchOpen && config.searchFields){
      (config.searchFields.split(',') || []).forEach(field => usedFields[field] = 0)
    }
    return usedFields && Object.keys(usedFields) || [];
  }

  formatMessage = (id: string, values?: {[key: string]: any}) => {
    return this.props.intl.formatMessage({id: id, defaultMessage: defaultMessages[id]}, values)
  }

  getSingleUsedDs = (dsJson: SelectedDataSourceJson): UseDataSource => {
    if(!dsJson || !dsJson.dataSourceJson){
      return null;
    }

    let singleUsedDs: UseDataSource = {
      dataSourceId: dsJson.dataSourceJson.id,
    };

    if(dsJson.rootDataSourceId){
      singleUsedDs.rootDataSourceId = dsJson.rootDataSourceId;
    }

    return singleUsedDs;
  }

  getWhetherDsInUseDataSources = (dsJson: SelectedDataSourceJson, useDataSources: ImmutableArray<UseDataSource>): boolean => {
    if(!dsJson || !dsJson.dataSourceJson || !useDataSources){
      return false;
    }
    return useDataSources.some(ds => ds.dataSourceId === dsJson.dataSourceJson.id);
  }

  getOutputDataSourceJson = (useDataSources: UseDataSource[]): DataSourceJson => {
    // outputDataSourceJson should bind widget instance, such as MapviewDataSource
    const dsJson: DataSourceJson = {
      id: `${this.props.id}-output`,
      label: this.formatMessage('outputDsLabel', {label: this.props.label}),
      type: AllDataSourceTypes.FeatureSet,
      originDataSources: useDataSources
    };
    
    return dsJson;
  }

  onFilterChange = (sqlExprObj: SqlExpression, dsId: string) => {
    const {useDataSources} = this.props;
    if(!useDataSources || !useDataSources[0] || useDataSources[0].dataSourceId !== dsId)return;
    this.needUpdateFields = true;
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [useDataSources[0].setIn(['query', 'where'], sqlExprObj).asMutable({deep: true})]
    });
  }
  
  onDSSelectorSortChange = (sortData: Array<OrderByOption>, dsId: string) => {
    const {useDataSources} = this.props;
    if(!useDataSources || !useDataSources[0] || useDataSources[0].dataSourceId !== dsId)return;
    this.needUpdateFields = true;
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [useDataSources[0].setIn(['query', 'orderBy'], sortData).asMutable({deep: true})]
    });
  }

  onDataSelect = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs: SelectedDataSourceJson) => {
    let widgets = this.props.appConfig && this.props.appConfig.widgets;

    const widgetJson = widgets[this.props.id];
    let udpateWidgetJson = {id: this.props.id} as any;
    const appConfigAction = getAppConfigAction();
    let useDataSources: UseDataSource[];
    let singleUsedDs: UseDataSource;
    if(this.getWhetherDsInUseDataSources(currentSelectedDs, widgetJson.useDataSources)){
      useDataSources = widgetJson.useDataSources.asMutable({deep: true});
    }else{
      singleUsedDs = this.getSingleUsedDs(currentSelectedDs);
      useDataSources = [singleUsedDs];
      udpateWidgetJson['config'] = widgetJson.config
      .set('searchFields', null)
      .set('filters', null)
      .set('sorts', null);
    }
    // Instead of function onSettingChange, use action to change widget json, which can avoid conflict.
    // Because editing widget json in builder needs pub-sub and pub-sub is async.
    udpateWidgetJson['useDataSources'] = useDataSources;
    //outputdatasource
    let outputDataSourceJson = this.getOutputDataSourceJson(useDataSources)
    if (outputDataSourceJson) {
      const outputDataSources = [outputDataSourceJson]
      // appConfigAction.editSubWidgetsInheritProperty(widgetJson.id, 'outputDataSources', outputDataSources, 'dataSourceId', widgetJson.outputDataSources);
      // widgetJson = widgetJson.set('outputDataSources', outputDataSources);
      appConfigAction.editWidget(udpateWidgetJson, outputDataSources).exec();
    }else {
      appConfigAction.editWidget(udpateWidgetJson).exec();
    }
    this.needUpdateFields = true;
  }

  onDataRemove = (allSelectedDss: SelectedDataSourceJson[], currentRemovedDs: SelectedDataSourceJson) => {
    const widgets = this.props.appConfig && this.props.appConfig.widgets;
    const widgetJson = widgets[this.props.id];
    let updateWidgetJson = {id: this.props.id} as any;
    let appConfigAction = getAppConfigAction();
    let useDataSources: ImmutableArray<UseDataSource> = widgetJson.useDataSources.filter(usedDs => usedDs.dataSourceId !== currentRemovedDs.dataSourceJson.id);
    //outputdatasource
    // if(widgetJson.outputDataSources && widgetJson.outputDataSources.length > 0){
    //   // appConfigAction.editSubWidgetsInheritProperty(widgetJson.id, 'outputDataSourcesJson', [], 'dataSourceId', widgetJson.outputDataSources);
    //   widgetJson = widgetJson.set('outputDataSourcesJson', []);
    // }
    
    // Instead of function onSettingChange, use action to change widget json, which can avoid conflict.
    // Because editing widget json in builder needs pub-sub and pub-sub is async.
    updateWidgetJson['config'] = widgetJson.config
      .set('sqlExprObj', null)
      .set('searchFields', null)
      .set('filters', null)
      .set('sortFields', null);
    updateWidgetJson['useDataSources'] = useDataSources;
    appConfigAction.editWidget(updateWidgetJson, []).exec();
    this.needUpdateFields = true;
    this.setState({
      datasource: undefined,
      fields: undefined
    })
  }

  rednerBgSetting() {
    let {config, showCardSetting} = this.props;
    const borderSides = [BorderSides.TL, BorderSides.TR, BorderSides.BR, BorderSides.BL];
    
    return (
      <Fragment>
        <SettingRow label="Background" flow="wrap">
          <BackgroundSetting
            background={config.cardConfigs[showCardSetting].backgroundStyle.background}
            onChange={value => this.onBackgroundStyleChange(showCardSetting, 'background', value)} />
        </SettingRow>
        <SettingRow label="Border" flow="wrap">
          <BorderSetting value={config.cardConfigs[showCardSetting].backgroundStyle.border}
            onChange={value => this.onBackgroundStyleChange(showCardSetting, 'border', value)} />
        </SettingRow>
        <SettingRow label="Border radius" flow="wrap">
          <FourSides sides={borderSides}
            value={config.cardConfigs[showCardSetting].backgroundStyle.borderRadius}
            onChange={value => this.onBackgroundStyleChange(showCardSetting, 'borderRadius', value)}
          />
        </SettingRow>
        {/* <SettingRow label="Box shadow" flow="wrap">
          <BoxShadowSetting
            value={config.cardConfigs[showCardSetting].backgroundStyle.boxShadow}
            onChange={value => this.onBackgroundStyleChange(showCardSetting, 'boxShadow', value)} />
        </SettingRow> */}
      </Fragment>
    )
  }

  render() {
    const {
      config,
      theme,
      showCardSetting,
      useDataSources
    } = this.props;
    const {showItemStyle, showTools} = this.state;
    const statusIntl = {}
    statusIntl[Status.Hover] = this.formatMessage('hover');
    statusIntl[Status.Selected] = this.formatMessage('listSelected');
    statusIntl[Status.Regular] = this.formatMessage('regular');

    const filterConfig = {};
    const sortConfig = {};
    const useDsIds = useDataSources && useDataSources.map(useDs => {
      filterConfig[useDs.dataSourceId] = useDs.query && useDs.query.where;
      sortConfig[useDs.dataSourceId] = useDs.query && useDs.query.orderBy;
      return useDs.dataSourceId;
    })

    const cardSize = this.getCardSize(this.props);
    return (
    <div className={classNames(`${prefix}list-setting`, `${prefix}setting`)} css={this.getStyle(this.props.theme)} >
      {
        !config.isItemStyleConfirm ? 
        <SettingSection title={this.formatMessage('chooseTemplateTip')}>
          <SettingRow>
            <div className="style-group w-100">
              <div className="d-flex justify-content-between w-100">
                <img className={`style-img style-img-h ${config.itemStyle === ItemStyle.Style0 && 'active'}`}
                  src={require('./assets/style1.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style0)} />
                <img className={`style-img style-img-h ${config.itemStyle === ItemStyle.Style1 && 'active'}`}
                  src={require('./assets/style2.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style1)} />
              </div>
              <div className="vertical-space"/>
              <div className="d-flex justify-content-between w-100">
                <img className={`style-img style-img-h ${config.itemStyle === ItemStyle.Style2 && 'active'}`}
                  src={require('./assets/style3.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style2)} />
                <img className={`style-img style-img-h ${config.itemStyle === ItemStyle.Style3 && 'active'}`}
                  src={require('./assets/style4.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style3)} />
              </div>
              <div className="vertical-space"/>
              <img className={`style-img ${config.itemStyle === ItemStyle.Style4 && 'active'}`}
                  src={require('./assets/style5.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style4)} />
              <div className="vertical-space"/>
              <img className={`style-img ${config.itemStyle === ItemStyle.Style5 && 'active'}`}
                  src={require('./assets/style6.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style5)} />
              <div className="vertical-space"/>
              <img className={`style-img low ${config.itemStyle === ItemStyle.Style6 && 'active'}`}
                  src={require('./assets/style7.png')} onClick={evt => this.onItemStyleChanged(ItemStyle.Style6)} />
              <div className="vertical-space"/>
              <div className={`style-img empty d-flex justify-content-center align-items-center ${config.itemStyle === ItemStyle.Style7 && 'active'}`}
                   onClick={evt => this.onItemStyleChanged(ItemStyle.Style7)} >
                {this.formatMessage('emptyTemplate')}
              </div>
            </div>
          </SettingRow>
          <SettingRow >
            <Button type="primary" className="w-100" onClick={evt => this.onPropertyChange('isItemStyleConfirm', true)} >{this.formatMessage('start')}</Button>
          </SettingRow>
        </SettingSection> :
        <Fragment>
          { showCardSetting === Status.Regular &&
            <div className="list-list-setting">
              <SettingSection >
                {/* <SettingRow label={'export style'}>
                  <Button type="primary" onClick={this.onExportClick} >Test</Button>
                </SettingRow> */}
                <SettingRow flow="wrap">
                  <span>{this.formatMessage('listUseGuide')} 
                    <a className="resetting-template" onClick={evt => this.onPropertyChange('isItemStyleConfirm', false)} >{this.formatMessage('resettingTheTemplate')}</a>
                  </span>
                  
                </SettingRow>
                <SettingRow flow="wrap">
                  <DataSourceSelector
                    types={Immutable([AllDataSourceTypes.FeatureLayer, AllDataSourceTypes.FeatureQuery])}
                    selectedDataSourceIds={Immutable(useDsIds || [])}
                    mustUseDataSource={true} onSelect={this.onDataSelect} onRemove={this.onDataRemove}
                    filterEnabled={true} sortEnabled={true} 
                    sortConfig={Immutable(sortConfig)} 
                    filterConfig={Immutable(filterConfig)}
                    onFilterChange={this.onFilterChange}
                    onSortChange={this.onDSSelectorSortChange}
                  />
                </SettingRow>
                <SettingRow label={this.formatMessage('maxItems')}>
                  <div className="d-flex">
                    <Switch 
                      checked={config.maxItemsOpen}
                      onChange={evt => this.onPropertyChange('maxItemsOpen', !config.maxItemsOpen)}
                    />
                  </div>
                </SettingRow>
                {config.maxItemsOpen &&
                  <SettingRow >
                    <div className="d-flex w-100">
                      <Input 
                        className="w-100" 
                        value={config.maxItems + ''}
                        type="number"
                        onAcceptValue={value => 
                        {
                          if(!value || value === ''){
                            value = '0';
                          }
                          let valueInt = parseInt(value);
                          if(valueInt < 0)valueInt = 0;
                          this.onPropertyChange('maxItems', valueInt) 
                        }} />
                    </div>
                  </SettingRow>
                }
    
              </SettingSection>

              <SettingSection>
              <SettingRow label={this.formatMessage('direction')}>
                  <ButtonGroup size="sm" >
                    {
                      directions.map((data, i) => {
                        return <Button key={i} icon active={config.direction === data.value}
                          onClick={evt => this.onPropertyChange('direction', data.value)} ><Icon size={12} icon={data.icon} /></Button>
                      })
                    }
                  </ButtonGroup>
                </SettingRow>
                <SettingRow label={`${config.direction === DirectionType.Vertical ? this.formatMessage('itemHeight') : this.formatMessage('itemWidth')}(px)`}>
                    <Input 
                      style={{width: '40%'}}
                      value={(config.direction === DirectionType.Vertical ? cardSize.height : cardSize.width).toFixed(2) + ''}
                      type="number"
                      min="0"
                      disabled={config.lockItemRatio}
                      onAcceptValue={this.handleItemSizeChange}
                    />
                    {/* <Button 
                      size="sm"
                      onClick={e => this.onPropertyChange('lockItemRatio', !config.lockItemRatio)}
                      icon type="tertiary">
                      <Icon 
                        icon={
                          config.lockItemRatio ? require('jimu-ui/lib/icons/lock.svg') : require('jimu-ui/lib/icons/unlock.svg')
                        } 
                        size={12}>
                      </Icon>
                    </Button> */}
                </SettingRow>
                <SettingRow label="">
                  <div className="d-flex">
                    <Input type="checkbox" theme={theme} 
                    onClick={(e) => { this.onPropertyChange('lockItemRatio', !config.lockItemRatio) }}
                    checked={config.lockItemRatio} />
                    <div style={{marginLeft: polished.rem(8)}} >{this.formatMessage('lockItemRatio')}</div>
                  </div>
                </SettingRow>
                <SettingRow flow="wrap" label={(config.direction === DirectionType.Vertical ? this.formatMessage('verticalSpacing') : this.formatMessage('horizontalSpacing')) + ' (px)'}>
                  <div className="d-flex justify-content-between w-100">
                    <Input 
                      type="range"
                      style={{width: '60%'}}
                      onChange={e => this.onPropertyChange('space',  parseInt(e.target.value))}
                      value={config.space}
                      title="0-50"
                      bsSize="sm"
                      min="0"
                      max="50"
                    />
                    <Input
                      style={{width: '25%'}}
                      value={config.space + ''}
                      type="number"
                      min="0"
                      max="50"
                      title="0-50"
                      onChange={evt => {
                        let value = evt.target.value;
                        if(!value || value === '') return;
                        value = parseInt(evt.target.value);
                        if(value < 0){value = 0}
                        if(value > 50){value = 50}
                        this.onPropertyChange('space', value)
                      } 
                      }
                    />
                  </div>
                </SettingRow>
                <SettingRow label={this.formatMessage('pagingStyle')}>
                  <Input type="select" value={config.pageStyle}
                    onChange={(e) => { this.onPropertyChange('pageStyle', e.target.value) }}>
                    {
                      this.getPageStyleOptions()
                    }
                  </Input>
                </SettingRow>
                {config.pageStyle === PageStyle.Scroll &&
                  <SettingRow label={this.formatMessage('scrollBar')}>
                    <div className="d-flex">
                      <Switch 
                        checked={config.scrollBarOpen}
                        onChange={evt => this.onPropertyChange('scrollBarOpen', !config.scrollBarOpen)}
                      />
                    </div>
                  </SettingRow>
                }
                {config.pageStyle === PageStyle.Scroll &&
                <SettingRow label={this.formatMessage('navigator')}>
                  <div className="d-flex">
                    <Switch 
                      checked={config.navigatorOpen}
                      onChange={evt => this.onPropertyChange('navigatorOpen', !config.navigatorOpen)}
                    />
                  </div>
                </SettingRow>
                }
                {config.pageStyle === PageStyle.Scroll && config.navigatorOpen &&
                <SettingRow label={this.formatMessage('step')}>
                    <Input 
                      value={config.scrollStep} 
                      style={{width: '25%'}}
                      type="number" 
                      min="1"
                      onChange={(event) => { this.onPropertyChange('scrollStep', parseInt(event.target.value)); }}
                    />
                </SettingRow>
                }
                {config.pageStyle === PageStyle.MultiPage &&
                  <SettingRow label={this.formatMessage('itemPerPage')}>
                    <Input 
                      value={config.itemsPerPage} 
                      style={{width: '25%'}}
                      type="number" 
                      min="1"
                      onAcceptValue={(value) => { this.onPropertyChange('itemsPerPage', parseInt(value)); }}
                    />
                  </SettingRow>
                }
                
              </SettingSection>
              
              {/* itemStyle */}
              <SettingSection title={
                <div className="w-100 d-flex justify-content-between">
                  {this.formatMessage('states')}
                  <Button size={'sm'} type={'tertiary'} icon onClick={evt => this.setState({showItemStyle: !this.state.showItemStyle})}>
                    <Icon size={12} color={theme.colors.black} icon={showItemStyle ? arrowUp : arrowDown} />
                  </Button>
                </div>
              }>
                <Collapse isOpen={showItemStyle} >
                  {this.rednerBgSetting()}
                  <SettingRow label={this.formatMessage('hover')}>
                    <div className="setting-next d-flex" onClick={evt => this.onOpenCardSetting(evt, Status.Hover)}>
                      <div>{config.cardConfigs[Status.Hover].enable ? this.formatMessage('on') : this.formatMessage('off')}</div>
                      <Icon className="sm" size={12} icon={require('jimu-ui/lib/icons/arrow-right-12.svg')}/>
                    </div>
                  </SettingRow>
                  <SettingRow label={this.formatMessage('listSelected')}>
                    <div className="setting-next d-flex" onClick={evt => this.onOpenCardSetting(evt, Status.Selected)}>
                      <div>{config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None ? this.formatMessage('on') : this.formatMessage('off')}</div>
                      <Icon className="sm" size={12} icon={require('jimu-ui/lib/icons/arrow-right-12.svg')}/>
                    </div>
                  </SettingRow>
                </Collapse>
              </SettingSection>

              {/* tool setting */}
              {this.state.datasource &&
                <SettingSection title={
                  <div className="w-100 d-flex justify-content-between">
                    {this.formatMessage('tools')}
                    <Button size={'sm'} type={'tertiary'} icon onClick={evt => this.setState({showTools: !this.state.showTools})}>
                      <Icon size={12} color={theme.colors.black} icon={showTools ? arrowUp : arrowDown} />
                    </Button>
                  </div>
                }>
                  <Collapse isOpen={showTools}>
                    <SettingRow label={this.formatMessage('search')}>
                      <div className="d-flex">
                        <Switch 
                          checked={config.searchOpen}
                          onChange={evt => this.onPropertyChange('searchOpen', !config.searchOpen)}
                          />
                      </div>
                    </SettingRow>
                    {config.searchOpen &&
                      <SettingRow  flow="wrap" label={this.formatMessage('chooseSearchingFields')}>
                        <div className="d-flex w-100 search-container">
                          <MultiSelect 
                            items={Immutable(this.getSearchingFields(true))}
                            values={config.searchFields && Immutable(config.searchFields.split(','))}
                            className="search-multi-select"
                            fluid={true}
                            placeHolder={this.formatMessage('selectSearchFields')}
                            onClickItem={(e, value, values) => this.onPropertyChange('searchFields', values.join(','))}
                            displayByValues={(values) => {
                              return this.formatMessage('searchSelected', {selectedCount: values.length})
                            }} />
                        </div>
                        <div className="d-flex" style={{marginTop: '10px'}}>
                          <Input type="checkbox"
                          onClick={(e) => { this.onPropertyChange('searchExact', !config.searchExact) }}
                          checked={config.searchExact} />
                          <div style={{marginLeft: polished.rem(8)}}>{this.formatMessage('exactMatch')}</div>
                        </div>
                      </SettingRow>
                    }
                    <SettingRow label={this.formatMessage('sort')}>
                      <div className="d-flex">
                          <Switch 
                            checked={config.sortOpen}
                            onChange={evt => this.onPropertyChange('sortOpen', !config.sortOpen)}
                          />
                      </div>
                    </SettingRow>
                    {config.sortOpen &&
                      <SettingRow flow="wrap" label={this.formatMessage('chooseSortingFields')}>
                        <SortSetting 
                          onChange={this.onSettingSortChange}
                          useDataSource={useDataSources && useDataSources[0]}
                          value={config.sorts || []}
                        />
                      </SettingRow>
                    }
                    <SettingRow label={this.formatMessage('filter')}>
                      <div className="d-flex">
                        <Switch 
                          checked={config.filterOpen}
                          onChange={evt => this.onPropertyChange('filterOpen', !config.filterOpen)}
                          />
                      </div>
                    </SettingRow>
                    {
                      config.filterOpen && 
                      <Fragment>
                        <SettingRow>
                          <div className="d-flex justify-content-between w-100 align-items-center">
                            {
                            <Button className="w-100 text-dark set-link-btn" color={!this.state.datasource ? 'secondary' : 'primary'} disabled={!this.state.datasource}
                              onClick={this.showSqlExprPopup} title={this.formatMessage('setFilters')}>
                              {this.formatMessage('setFilters')}
                            </Button>
                            }
                          </div>
                        </SettingRow>
                        <SettingRow flow="wrap">
                          <SqlExpressionBuilderPopup
                            selectedDs={this.state.datasource}
                            isOpen={this.state.isSqlExprShow} toggle={this.toggleSqlExprPopup}
                            config = {config.filter} onChange={this.onSqlExprBuilderChange}
                            theme={this.props.theme} id="list-widget-sql-expression-builder-popup" />
                        </SettingRow>
                      </Fragment>
                    }
                    <SettingRow label={this.formatMessage('showSelectedOnly')}>
                      <Switch 
                        checked={config.showSelectedOnlyOpen}
                        onChange={evt => this.onPropertyChange('showSelectedOnlyOpen', !config.showSelectedOnlyOpen)}
                        />
                    </SettingRow>
                  </Collapse>
                  
                </SettingSection>
              }
            </div>
          }
          {showCardSetting !== Status.Regular &&
            <div className="list-card-setting">
              <SettingSection >
                <SettingRow>
                  <Button type="tertiary" onClick={this.onCardSettingReturnBackClick} >
                    <Icon className="sm" size={14} icon={require('jimu-ui/lib/icons/arrow-left-14.svg')}/>
                    <div>{statusIntl[showCardSetting]}</div>
                  </Button>
                </SettingRow>
              </SettingSection>
    
              <SettingSection >
                <SettingRow label={this.formatMessage('enableStatus', {status: statusIntl[showCardSetting]})}>
                  <Switch
                    checked={showCardSetting === Status.Hover ? config.cardConfigs[Status.Hover].enable : config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None}
                    onChange={showCardSetting === Status.Hover ? this.onHoverLayoutOpenChange : this.onSelectionSwitch}/>
                </SettingRow>
                {((showCardSetting === Status.Selected && config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None) ||
                  (showCardSetting === Status.Hover && config.cardConfigs[Status.Hover].enable)) &&
                  <Fragment>
                    {showCardSetting === Status.Selected &&
                      <SettingRow label={this.formatMessage('selectMode')}>
                        <Input type="select" value={config.cardConfigs[Status.Selected].selectionMode}
                          onChange={(e) => { this.onSelectionModeChange(e.target.value) }}>
                          {
                            this.getSelectModeOptions()
                          }
                        </Input>
                        
                      </SettingRow>
                    }
                    {this.rednerBgSetting()}
                  </Fragment>
                }
                
              </SettingSection>
            </div>
          }
        </Fragment>
        
      }
      
      {this.props.useDataSources && this.props.useDataSources[0] && this.props.useDataSources[0] &&
        <div className="waiting-for-database">
          <DataSourceComponent useDataSource={this.props.useDataSources[0]} onDataSourceCreated={this.onDsCreate}/>
        </div>
      }
    </div>);
  }
}