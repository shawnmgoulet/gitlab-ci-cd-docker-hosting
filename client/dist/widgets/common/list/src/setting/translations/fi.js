define({
  default: 'Å_default_______________ö',
  _action_query_label: 'Å_Query by extent________________ö',
  listSource: 'Å_List Source____________ö',
  maxItems: 'Å_Max items___________________ö',
  itemPerPage: 'Å_Item per page______________ö',
  selectMode: 'Å_Select mode____________ö',
  style: 'Å_Style___________ö',
  quickStyle: 'Å_quickStyle_____________________ö',
  more: 'Å_More_________ö',
  direction: 'Å_Direction___________________ö',
  verticalAlignment: 'Å_Vertical alignment___________________ö',
  horizontalAlignment: 'Å_Horizontal alignment_____________________ö',
  verticalSpacing: 'Å_Vertical spacing_________________ö',
  horizontalSpacing: 'Å_Horizontal spacing___________________ö',
  differentOddEvenItems: 'Å_Different odd & even items___________________________ö',
  mouseOverStyle: 'Å_Mouse-over style_________________ö',
  selectedStyle: 'Å_Selected style_______________ö',
  pagingStyle: 'Å_Paging style_____________ö',
  scroll: 'Å_Scroll_____________ö',
  multiPage: 'Å_Multi-page_____________________ö',
  pageTransition: 'Å_Page transition________________ö',
  tools: 'Å_Tools___________ö',
  filter: 'Å_Filter_____________ö',
  sort: 'Å_Sort_________ö',
  single: 'Å_Single_____________ö',
  multiple: 'Å_Multiple_________________ö',
  regular: 'Å_Regular_______________ö',
  hover: 'Å_Hover___________ö',
  listSelected: 'Å_Selected_________________ö',
  off: 'Å_Off_______ö',
  on: 'Å_On_____ö',
  enable: 'Å_Enable_____________ö',
  start: 'Å_Start___________ö',
  chooseTemplateTip: 'Å_Choose a template to start with_________________ö',
  listUseGuide: 'Å_Drag and drop widgets from Element panel to customize your list or you can_______________________________________ö ',
  resettingTheTemplate: 'Å_reset the template___________________ö',
  emptyTemplate: 'Å_Empty template_______________ö',
  chooseSortingFields: 'Å_Choose sorting fields______________________ö',
  waitingForDatasource: 'Å_Waiting for datasource_______________________ö',
  zeroHint: 'Å_Zero is infinite_________________ö',
  selectSortFields: 'Å_Select sort fields___________________ö', 
  sortSelected: 'Å_{selectedCount} items selected_______________________________ö',
  openFilterBuilder: 'Å_Open Filter Builder____________________ö',
  chooseSearchingFields: 'Å_Choose searching fields________________________ö',
  selectSearchFields: 'Å_Select search fields_____________________ö', 
  searchSelected: 'Å_{selectedCount} items selected_______________________________ö',
  exactMatch: 'Å_Exact match____________ö',
  scrollBar: 'Å_Scroll bar_____________________ö',
  navigator: 'Å_Navigator___________________ö',
  states: 'Å_States_____________ö',
  itemHeight: 'Å_Item height____________ö',
  itemWidth: 'Å_Item width_____________________ö',
  lockItemRatio: 'Å_Lock width / height ratio__________________________ö'
});