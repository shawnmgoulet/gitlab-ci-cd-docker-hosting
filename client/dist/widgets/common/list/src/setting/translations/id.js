define({
  default: 'ng_default_______________ny',
  _action_query_label: 'ng_Query by extent________________ny',
  listSource: 'ng_List Source____________ny',
  maxItems: 'ng_Max items___________________ny',
  itemPerPage: 'ng_Item per page______________ny',
  selectMode: 'ng_Select mode____________ny',
  style: 'ng_Style___________ny',
  quickStyle: 'ng_quickStyle_____________________ny',
  more: 'ng_More_________ny',
  direction: 'ng_Direction___________________ny',
  verticalAlignment: 'ng_Vertical alignment___________________ny',
  horizontalAlignment: 'ng_Horizontal alignment_____________________ny',
  verticalSpacing: 'ng_Vertical spacing_________________ny',
  horizontalSpacing: 'ng_Horizontal spacing___________________ny',
  differentOddEvenItems: 'ng_Different odd & even items___________________________ny',
  mouseOverStyle: 'ng_Mouse-over style_________________ny',
  selectedStyle: 'ng_Selected style_______________ny',
  pagingStyle: 'ng_Paging style_____________ny',
  scroll: 'ng_Scroll_____________ny',
  multiPage: 'ng_Multi-page_____________________ny',
  pageTransition: 'ng_Page transition________________ny',
  tools: 'ng_Tools___________ny',
  filter: 'ng_Filter_____________ny',
  sort: 'ng_Sort_________ny',
  single: 'ng_Single_____________ny',
  multiple: 'ng_Multiple_________________ny',
  regular: 'ng_Regular_______________ny',
  hover: 'ng_Hover___________ny',
  listSelected: 'ng_Selected_________________ny',
  off: 'ng_Off_______ny',
  on: 'ng_On_____ny',
  enable: 'ng_Enable_____________ny',
  start: 'ng_Start___________ny',
  chooseTemplateTip: 'ng_Choose a template to start with_________________ny',
  listUseGuide: 'ng_Drag and drop widgets from Element panel to customize your list or you can_______________________________________ny ',
  resettingTheTemplate: 'ng_reset the template___________________ny',
  emptyTemplate: 'ng_Empty template_______________ny',
  chooseSortingFields: 'ng_Choose sorting fields______________________ny',
  waitingForDatasource: 'ng_Waiting for datasource_______________________ny',
  zeroHint: 'ng_Zero is infinite_________________ny',
  selectSortFields: 'ng_Select sort fields___________________ny', 
  sortSelected: 'ng_{selectedCount} items selected_______________________________ny',
  openFilterBuilder: 'ng_Open Filter Builder____________________ny',
  chooseSearchingFields: 'ng_Choose searching fields________________________ny',
  selectSearchFields: 'ng_Select search fields_____________________ny', 
  searchSelected: 'ng_{selectedCount} items selected_______________________________ny',
  exactMatch: 'ng_Exact match____________ny',
  scrollBar: 'ng_Scroll bar_____________________ny',
  navigator: 'ng_Navigator___________________ny',
  states: 'ng_States_____________ny',
  itemHeight: 'ng_Item height____________ny',
  itemWidth: 'ng_Item width_____________________ny',
  lockItemRatio: 'ng_Lock width / height ratio__________________________ny'
});