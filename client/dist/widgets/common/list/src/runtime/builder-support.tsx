import {LayoutInfo, getAppStore, React} from 'jimu-core';
import {interact} from 'jimu-core/dnd';
import { ButtonGroup, Button} from 'jimu-ui'
import {handleResizeCard as commonHandlResizeCard, selectSelf as commonSelectSelf} from '../common-builder-support'
import { layoutUtils } from 'jimu-layouts/common';
import { appConfigUtils, getAppConfigAction} from 'jimu-for-builder';
import { GLOBAL_DRAGGING_CLASS_NAME } from 'jimu-layouts/layout-builder'
import { withBuilderStyle, withBuilderTheme } from 'jimu-core/lib/utils/theme-utils';
import MyDropDown, {MyDropdownProps} from './components/my-dropdown';

const widgetModules = {
  ButtonGroup: ButtonGroup,
  interact: interact,
  appConfigUtils: appConfigUtils,
  getAppConfigAction: getAppConfigAction,
  GLOBAL_DRAGGING_CLASS_NAME: GLOBAL_DRAGGING_CLASS_NAME,
  withBuilderStyle: withBuilderStyle,
  withBuilderTheme: withBuilderTheme,
  BuilderDropDown: withBuilderTheme((props: MyDropdownProps) => { return <MyDropDown {...props} withBuilderStyle={withBuilderStyle} /> }),
  BuilderButton: withBuilderStyle(Button),

  handleResizeCard: (props, newCardSize, isTop: boolean = false, isLeft: boolean = false, isEnd: boolean = false) => {
    const action = commonHandlResizeCard(props, newCardSize, isTop, isLeft, isEnd);
    if(action){
      action.exec();
    }
  },

  selectSelf: (props) => {

    commonSelectSelf(props, true);
  },

  selectionIsSelf: (layoutInfo: LayoutInfo, id: string, appConfig: any) => {
    if(!layoutInfo || !layoutInfo.layoutItemId || !layoutInfo.layoutId){
      return false;
    }
    const layoutItem = layoutUtils.findLayoutItem(appConfig, layoutInfo);
    if(layoutItem && layoutItem.widgetId && layoutItem.widgetId === id){
      return true;
    }
    return false;
  },
  
  selectionInList: (layoutInfo: LayoutInfo, id: string, appConfig: any, notSearchEmbed?: boolean) => {
    if(!layoutInfo || !layoutInfo.layoutItemId || !layoutInfo.layoutId){
      return false;
    }
    return appConfigUtils.getLayoutItemsInWidgetByLayoutInfo(appConfig, layoutInfo, id, getAppStore().getState().browserSizeMode).length > 0
  }
}

export default widgetModules;