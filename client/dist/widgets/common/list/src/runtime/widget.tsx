/** @jsx jsx */
import {  IMState, classNames, React,
  FeatureQueryDataSource, css, jsx, polished, BaseWidget, AllWidgetProps,
  ThemeVariables, SerializedStyles, RepeatedDataSource,
   DataSource, DataRecord, DataSourceTypes, DataSourceStatus, LayoutInfo, AppMode, utils,
   BrowserSizeMode, appActions, DataQueryComponent, MessageManager,
   DataRecordsSelectionChangeMessage, IMAppConfig, getAppStore, ReactResizeDetector,
   DataSourceManager, IMDataSourceJson, QueryResult, ImmutableArray, 
   IMSqlExpression, MessageType, LayoutType, Immutable, lodash } from 'jimu-core';

import {  WidgetPlaceholder, Button, Icon, Pagination} from 'jimu-ui';
import {  FeatureLayerDataSource, DataSourceTypes as  ArcGISDataSourceTypes} from 'jimu-arcgis';
import { IMConfig, DirectionType, SelectionModeType, Status,
   LIST_CARD_PADDING, PageTransitonType, PageStyle, CardSize } from '../config';
import ListCard from './components/list-card';
import { LayoutViewer } from 'jimu-layouts/layout-runtime';
import defaultMessages from './translations/default';
const defaultConfig = require('../../config.json');
import SearchBox from './components/search-box';
import { VariableSizeList as List, areEqual} from 'react-window';
import { memo, Fragment } from 'react';
import { SortSettingOption } from 'jimu-ui/setting-components';
import MyDropDown, { MyDropDownItem } from './components/my-dropdown';
import FilterPicker from './components/filter-picker';


const arrowDown12 = require('jimu-ui/lib/icons/arrow-down-12.svg');
const arrowUp12 = require('jimu-ui/lib/icons/arrow-up-12.svg');
const arrowLeft12 = require('jimu-ui/lib/icons/arrow-left-12.svg');
const arrowRight12 = require('jimu-ui/lib/icons/arrow-right-12.svg');
const showSelectedOnly = require('jimu-ui/lib/icons/show-selected-only.svg');
const CSSClasses = {
  list: 'widget-list'
};


interface Props{
  selection: LayoutInfo,
  appMode: AppMode,
  draggingWidget: any,
  browserSizeMode: BrowserSizeMode,
  builderStatus: Status,
  currentPageId: string,
  isRTL: boolean
  outputDataSourceJson: IMDataSourceJson;
  subLayoutType: LayoutType,
  left: number | string,
  top: number | string
}

interface States {
  LayoutEntry: any;
  queryStart: number;
  sortOptionName?: string,
  searchText: string;
  currentFilter: IMSqlExpression;
  currentCardSize: CardSize;
  forceShowMask: boolean;
  showList: boolean;
  showSelectionOnly: boolean;
  widgetRect: {
    width: number,
    height: number
  },
  hideCardTool: boolean,
  scrollStatus: 'start' | 'end' | 'mid',
  listVisibleStartIndex: number;
  listVisibleStopIndex: number;
}

interface QueryOptions {
  geometry?: any,
  sortField?: string,
  sortOrder?: string,
  orderByFields?: string | string[],
  resultOffset?: number, 
  resultRecordCount?: number,
  num?: number,
  start?: number,
  where?: string,
}

const DS_TOOL_H = 42;
const BOTTOM_TOOL_H = 40;
const COMMON_PADDING = 0;
const DS_TOOL_BOTTOM_PADDING = 16;
const BOTTOM_TOOL_TOP_PADDING = 14;
function createQuery(props: AllWidgetProps<IMConfig>, options?: QueryOptions): any{
  
  let q = {
    where: '1=1',
    outFields: ['*'],
    returnGeometry: true,
    ...(options || {})
  } as any;

  return q;
}

function loadRecords(props: AllWidgetProps<IMConfig>, ds: DataSource): Promise<DataRecord[]>{
  if(!ds)return Promise.resolve([]);

  let q = createQuery(props);

  if(ds.type === ArcGISDataSourceTypes.FeatureLayer){
    return (ds as FeatureLayerDataSource).load(q).then(() => ds.getRecords());
  }else if(ds.type === DataSourceTypes.FeatureQuery){
    return (ds as FeatureQueryDataSource).load(q).then(() => ds.getRecords());
  }
}

function isDsConfigured(props: AllWidgetProps<IMConfig>): boolean{
  return props.useDataSources && !!props.useDataSources[0];
}

let lastWidgetSize = {
  height: 1,
  width: 1
}

export class Widget extends BaseWidget<AllWidgetProps<IMConfig> & Props, States>{
  lastSelectedIndex: number;
  isMySelected: boolean;
  lastSelectedRecordIds: string[];
  belongToPageId: string;
  listRef: any;
  listOutDivRef: React.RefObject<HTMLDivElement> 
  paginatorDiv: React.RefObject<HTMLDivElement> 
  datasource: DataSource;
  records: Array<any>;
  queryStatus: DataSourceStatus;
  totalCount: number;
  lastQueryStart: number;
  isSwitchPage: boolean;
  scrollSelf: boolean;
  needScroll: boolean;
  lastPageSize: number;

  static mapExtraStateProps = (state: IMState, props: AllWidgetProps<IMConfig>): Props => {
    const appConfig = state && state.appConfig;
    const { layouts, layoutId, layoutItemId } = props;
    const browserSizeMode = state && state.browserSizeMode;
    const builderStatus = state && state.widgetsState && state.widgetsState[props.id] && state.widgetsState[props.id]['builderStatus'] || Status.Regular;
    let subLayoutType;
    if(appConfig){
      const subLayout = state && state.appConfig && state.appConfig.layouts 
        && state.appConfig.layouts[utils.findLayoutId(layouts[builderStatus], browserSizeMode, appConfig.mainSizeMode)];
      subLayoutType = subLayout && subLayout.type;
    }

    let widgetPosition = undefined;
    if(window.jimuConfig.isInBuilder){
      const bbox = lodash.getValue(appConfig, `layouts.${layoutId}.content.${layoutItemId}.bbox`)
      widgetPosition = bbox && {
        left: bbox.left,
        top: bbox.top
      };
    }
    return {
      selection: state && state.appRuntimeInfo && state.appRuntimeInfo.selection,
      appMode: state && state.appRuntimeInfo && state.appRuntimeInfo.appMode,
      draggingWidget: state && state.appRuntimeInfo && state.appRuntimeInfo.draggingWidget,
      browserSizeMode: state && state.browserSizeMode,
      builderStatus: state && state.widgetsState && state.widgetsState[props.id] && state.widgetsState[props.id]['builderStatus'] || Status.Regular,
      currentPageId: state && state.appRuntimeInfo && state.appRuntimeInfo.currentPageId,
      isRTL: state && state.appContext && state.appContext.isRTL,
      outputDataSourceJson: state.appConfig && state.appConfig.dataSources[props.outputDataSources && props.outputDataSources[0]],
      subLayoutType,
      left: widgetPosition && widgetPosition.left,
      top: widgetPosition && widgetPosition.top
    };
  };

  static preloadData = (state: IMState, allProps: AllWidgetProps<IMConfig> & Props, dataSources: {[dsId: string]: DataSource}): Promise<any> => {
    if(!isDsConfigured(allProps)){
      return Promise.resolve([]);
    }
    return loadRecords(allProps, dataSources[allProps.useDataSources[0].dataSourceId]).then(records => {
      return []
    });
  };

  constructor(props) {
    super(props);
    this.listOutDivRef = React.createRef<HTMLDivElement>();
    this.paginatorDiv = React.createRef<HTMLDivElement>();
    this.belongToPageId = props.currentPageId;
    const {config} = props;
    let stateObj: States = {
      LayoutEntry: null,
      queryStart: 0,
      sortOptionName: config.sorts && config.sorts[0] && config.sorts[0].ruleOptionName,
      currentCardSize: this.getCardSize(props),
      forceShowMask: false,
      showList: true,
      widgetRect: undefined,
      searchText: undefined,
      currentFilter: undefined,
      showSelectionOnly: false,
      hideCardTool: false,
      scrollStatus: 'start',
      listVisibleStartIndex: 0,
      listVisibleStopIndex: 0
    };
    this.selectSelf = this.selectSelf.bind(this);
    this.handleResizeCard = this.handleResizeCard.bind(this);
    this.listRef = React.createRef();

    if (window.jimuConfig.isInBuilder) {
      stateObj.LayoutEntry = this.props.builderSupportModules.LayoutEntry;
      const {config} = props;
      if(!config.isInitialed){
        const newConfig = config.merge(defaultConfig);
        this.editWidgetConfig(newConfig);
      }
    }else {
      stateObj.LayoutEntry = LayoutViewer;
    }
    this.state = stateObj;
    // this.editWidgetConfig('builderStatus', Status.Regular);

    this.onResize = this.onResize.bind(this);
  }

  updateWidgetRectTimeout: NodeJS.Timer = undefined;
  componentDidUpdate(preProps, preState){
    const {config, appMode, id, selection, builderSupportModules, layouts, top, left, 
      browserSizeMode, builderStatus, currentPageId, subLayoutType} = this.props
    const {currentCardSize} = this.state;
    const appConfig = this.getAppConfig();
    if(currentPageId !== preProps.currentPageId && (currentPageId === this.belongToPageId || preProps.currentPageId === this.belongToPageId)){
      this.setState({
        showList: false
      }, () => {
        this.setState({
          showList: true
        })
      })
    }
    if (!window.jimuConfig.isInBuilder || appMode === AppMode.Run) {
      //Listen selected records change from outside
      if (this.datasource && config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None) {
        const { datasource } = this;
        const selectedRecordIds = datasource.getSelectedRecordIds();
        if (this.isMySelected) {
          this.isMySelected = false;
          this.lastSelectedRecordIds = selectedRecordIds || [];
          return;
        }
        if (selectedRecordIds && selectedRecordIds.length > 0) {
          let isSame = true;
          if(!this.lastSelectedRecordIds || selectedRecordIds.length !== this.lastSelectedRecordIds.length){
            isSame = false;
          }else {
            selectedRecordIds.some(recordId => {
              const rId = this.lastSelectedRecordIds.find(lastRecordId => lastRecordId === recordId);
              if(!rId){
                isSame = false;
                return true;
              }
            })
          }
          if (!isSame) {
            const indexes = datasource.getSelectedRecordIndexes();
            if (indexes && indexes.length > 0) {
              const index0 = indexes[0];
              if (config.maxItems === 0 || index0 < config.maxItems) {
                this.scrollToIndex(indexes[0])
              }
            }
          }
        }
        this.lastSelectedRecordIds = selectedRecordIds || [];
      }
    }

    if(window.jimuConfig.isInBuilder){
      //listen the type change of layout
      const newLayoutType = subLayoutType
      const oldLayoutType = preProps.subLayoutType

      if (oldLayoutType !== newLayoutType) {
        const appConfigAction = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
        Object.keys(layouts).forEach(name => {
          layouts[name] && Object.keys(layouts[name]).forEach(device => {
            appConfigAction.editLayoutType({ layoutId: layouts[name][device] }, newLayoutType);
          })
        })
        appConfigAction.exec();
      }

      // listen space change and then update grid
      const space = config.space;
      const oldSpace = preProps.config.space;
      const maxItem = config.maxItems;
      const oldMaxItem = preProps.config.maxItems;
      const oldCardSize = preState.currentCardSize;
      if (space !== oldSpace || maxItem !== oldMaxItem ||
          config.direction !== preProps.config.direction ||
          oldCardSize.width !== currentCardSize.width || 
          oldCardSize.height !== currentCardSize.height ||
          (top !== preProps.top || left !== preProps.left)) {
        if (this.listRef.current){
          this.listRef.current.resetAfterIndex(0, true);
        }
        this.setState({
          hideCardTool: true
        })
      }else {
        this.setState({
          hideCardTool: false
        })
      }

      //listen paging type change
      const {pageStyle} = config;
      const oldPageStyle = preProps.config.pageStyle;
      if(pageStyle !== oldPageStyle){
        this.setState({
          queryStart: 0
        })
      }

      //listen useDatasources change
      const {useDataSources} = this.props;
      const oldUseDataSources = preProps.useDataSources;
      if(useDataSources && useDataSources[0]){
        const oldUseDataSource = oldUseDataSources && oldUseDataSources[0];
        if(!oldUseDataSource || oldUseDataSource.dataSourceId !== useDataSources[0].dataSourceId){
          //reset querysStart
          this.setState({
            queryStart: 0
          })
        }
      }

      //listen sort change
      if(config.sortOpen){
        const sorts = config.sorts;
        const oldSorts = preProps.config.sorts;
        if(sorts !== oldSorts){
          this.setState({
            sortOptionName: undefined
          })
        }
      }
    }

    if(config.cardConfigs[Status.Selected].selectionMode !== preProps.config.cardConfigs[Status.Selected].selectionMode){
      this.selectRecords([])
    }
    if(appMode !== AppMode.Run){
      const {selectionInList, selectionIsSelf} = builderSupportModules.widgetModules;
      if(selection !== preProps.selection){
        //change status by toc
        if(selectionInList(selection, id, appConfig, true)){
          const status = Object.keys(layouts).find(status => utils.findLayoutId(layouts[status], browserSizeMode, appConfig.mainSizeMode) === selection.layoutId);
          if(status !== builderStatus){
            this.editBuilderAndSettingStatus(status as any);
          }
        }else if(!selectionIsSelf(selection, id, appConfig)){
          this.editBuilderAndSettingStatus(Status.Regular);
        }
      }
    }

    const newCardSize = this.getCardSize(this.props);
    const oldCardSize = this.getCardSize(preProps);
    if(oldCardSize.width !== newCardSize.width || oldCardSize.height !== newCardSize.height){
      this.setState({
        currentCardSize: newCardSize
      }, () => {
        if(this.listRef.current){
          this.listRef.current.resetAfterIndex(0, true);
          // this.gridRef.recomputeGridSize();
        }
      })
    }

    if(preProps.appMode != appMode){
      if(appMode === AppMode.Run){
        this.editBuilderAndSettingStatus(Status.Regular);
      }else {
        this.scrollToIndex(0)
      }
    }
    // this.initCanvas();
  }

  resizeTimeout: NodeJS.Timer;
  onResize = (width, height) => {
    const newWidgetRect = {
      width,
      height
    }
    const {config} = this.props;
    const listH = this._getListHeight(newWidgetRect);
    const oldCardSize = this.getCardSize(this.props);
    const cardSize = {
      width: oldCardSize.width,
      height: oldCardSize.height
    }
    if(config.lockItemRatio){
      const ratio = cardSize.width / cardSize.height;
      if(config.direction === DirectionType.Horizon){
        cardSize.height = listH;
        cardSize.width = listH * ratio;
      }else {
        cardSize.height = width / ratio;
        cardSize.width = width;
      }
    }else {
      if(config.direction === DirectionType.Horizon){
        cardSize.height = listH;
      }else {
        cardSize.width = width;
      }
    }
    this.setState({
      widgetRect: newWidgetRect,
      currentCardSize: cardSize
    })
    if(this.resizeTimeout){
      clearTimeout(this.resizeTimeout);
      this.resizeTimeout = undefined;
    }
    this.resizeTimeout = setTimeout(() => {
      this.handleResizeCard(cardSize, true);
    }, 500);
    
  }

  private selectRecords = (records: DataRecord[]) => {
    const {datasource} = this;
    
    if(datasource){
      MessageManager.getInstance().publishMessage(new DataRecordsSelectionChangeMessage(this.props.id, records));

      if(records){
        this.isMySelected = true;
        datasource.selectRecordsByIds(
          records.map(record => record.getId()), records
        )
        const outputDs = this.getOutputDs();
        outputDs && outputDs.selectRecordsByIds(records.map(record => record.getId()), records);
      }
    }

  }

  formatMessage = (id: string, values?: {[key: string]: any}) => {
    return this.props.intl.formatMessage({id: id, defaultMessage: defaultMessages[id]}, values)
  }

  // call exec manuly
  editStatus = (name, value) => {
    const {dispatch, id} = this.props;
    dispatch(appActions.widgetStatePropChange(id, name, value));
  }

  editWidgetConfig = (newConfig) => {
    if(!window.jimuConfig.isInBuilder)return;

    const appConfigAction = this.props.builderSupportModules.jimuForBuilderLib.getAppConfigAction();
    appConfigAction.editWidgetConfig(this.props.id, newConfig).exec();
  }

  scrollToIndex = (index: number, type: string = 'smart') => {
    if (this.listRef.current) {
      this.scrollSelf = true;
      this.listRef.current.scrollToItem(index, type)
    }
  }

  private getCardSize = (props?: AllWidgetProps<IMConfig> & Props): CardSize => {
    props = props || this.props;
    const {config, builderStatus, browserSizeMode} = props;
    let cardConfigs = config.cardConfigs[builderStatus];
    if(!cardConfigs || !cardConfigs.cardSize){
      cardConfigs = config.cardConfigs[Status.Regular];
    }
    let cardSize = cardConfigs.cardSize[browserSizeMode];
    if(!cardSize){
      cardSize = cardConfigs.cardSize[Object.keys(cardConfigs.cardSize)[0]]
    }
    return cardSize;
  }

  // private moveFeatureCenter = (objectId: number) => {
  //   if(!this.dataSource)return;
  //   if(this.dataSource.type === ArcGISDataSourceTypes.FeatureLayerView ){
  //     (this.dataSource as FeatureLayerViewDataSource).moveFeatureToCenter(objectId + '');
  //   }else if(this.dataSource.type === ArcGISDataSourceTypes.FeatureLayer){
  //     let view = (this.dataSource as FeatureLayerDataSource).getLayerViewDataSource()
  //     if(view){
  //       (view as FeatureLayerViewDataSource).moveFeatureToCenter(objectId + '');
  //     }
  //   }

  // }

  isEditing = (): boolean => {
    const {builderSupportModules, selection, id, appMode, config} = this.props;
    const appConfig = this.getAppConfig()
    if(!window.jimuConfig.isInBuilder) return false;
    const {selectionIsSelf, selectionInList} = builderSupportModules.widgetModules;
    return (selectionIsSelf(selection, id, appConfig) || selectionInList(selection, id, appConfig))
          && window.jimuConfig.isInBuilder && appMode !== AppMode.Run && config.isItemStyleConfirm
  }

  private handleItemChange = (listItem: ListCard) => {
    const {config} = this.props;
    if(!this.datasource)return;
    const {providerData} = listItem.props;

    let itemRecord = undefined;
    if(Array.isArray(providerData)){
      itemRecord = providerData[0].record;
    }else{
      itemRecord = (providerData as RepeatedDataSource).record;
    }

    const {datasource} = this;
    let selectedRecordIds = datasource.getSelectedRecordIds();
    if(config.cardConfigs[Status.Selected].selectionMode && config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None){
      const recordId = itemRecord.getId();
      if(config.cardConfigs[Status.Selected].selectionMode === SelectionModeType.Single){
        let index = selectedRecordIds.indexOf(recordId);
        if(index === 0) {
          this.selectRecords([])
        }else {
          this.selectRecords([itemRecord])
        }
      }else{
        let index = selectedRecordIds.indexOf(recordId);
        if(index > -1){
          selectedRecordIds.splice(index, 1);
        }else{
          selectedRecordIds.push(itemRecord.getId());
        }
        const selectedRecords = selectedRecordIds.length > 0 && this.records.filter(record => selectedRecordIds.indexOf(record.getId()) > -1) || [];
        this.selectRecords(selectedRecords);
      }
    }
  }

  mouseClickTimeout: NodeJS.Timer;

  handleListPointerDown = (evt) => {
    this.setState({
      forceShowMask: true
    })
    if(this.mouseClickTimeout){
      clearTimeout(this.mouseClickTimeout);
      this.mouseClickTimeout = undefined;
    }
    this.mouseClickTimeout = setTimeout(() => {
      this.setState({
        forceShowMask: false
      })
    }, 200);
  }

  handleScrollUp = (e) => {
    const {scrollStep} = this.props.config;
    const {listVisibleStartIndex} = this.state;
    let toIndex = listVisibleStartIndex - scrollStep;
    if(toIndex < 0){
      toIndex = 0;
    }
    this.scrollToIndex(toIndex, 'start');
  }

  handleScrollDown = (e) => {
    const {scrollStep} = this.props.config;
    const {listVisibleStopIndex, listVisibleStartIndex} = this.state;
    if(listVisibleStopIndex + scrollStep >= this.records.length - 1 && this.records.length < this.getTotalCount()){
      this.isSwitchPage = true;
      this.lastQueryStart = this.state.queryStart;
      this.setState({
        queryStart: this.records.length
      })
    }else {
      this.scrollToIndex(listVisibleStartIndex + scrollStep, 'start');
    }
  }

  handleSwitchPage = (pageNum: number) => {
    const totalPages = this.getTotalPage();
    if(pageNum < 1 || pageNum > totalPages) return;
    const {config} = this.props;
    const start = (pageNum - 1) * config.itemsPerPage;
    this.selectRecords([]);
    if(start !== this.state.queryStart){
      this.isSwitchPage = true;
      this.lastQueryStart = this.state.queryStart;
      this.setState({
        queryStart: start
      })
    }
  }

  handleListScroll = ({scrollDirection, scrollOffset, scrollUpdateWasRequested}) => {
    const {appMode, config} = this.props;
    const listDiv = this.listOutDivRef.current;
    const datasource = this.getDataSource();
    if(!listDiv)return;
    if(config.pageStyle === PageStyle.Scroll && 
      this.queryStatus !== DataSourceStatus.Loading &&
      datasource && 
      (!window.jimuConfig.isInBuilder || appMode === AppMode.Run)){
      if(this.isScrollEnd()){
        
        if(this.scrollSelf){
          this.setState({
            scrollStatus: 'end'
          })
        }else {
          if(this.records.length !== this.getTotalCount()){
            let queryStart = this.records.length;
            const pageSize = this.getPageSize();
            if(this.lastQueryStart === this.records.length){//request failed last
              this.lastQueryStart = queryStart - pageSize;
              this.setState({
                queryStart : queryStart - pageSize < 0 ? 0 : queryStart - pageSize,
                scrollStatus: 'end'
              }, () => {
                this.setState({
                  queryStart: queryStart,
                  scrollStatus: 'end'
                })
                
              })
            }else{
              this.lastQueryStart = this.state.queryStart;
              this.setState({
                queryStart : this.records.length,
                scrollStatus: 'end'
              })
            }
            this.isSwitchPage = true;
          }
        }
        
      }else if(this.isScrollStart()){
        this.setState({
          scrollStatus: 'start'
        })
      }else {
        this.setState({
          scrollStatus: 'mid'
        })
      }
    }
    if(this.scrollSelf){
      this.scrollSelf = false;
    }
  }

  getTotalPage = () => {
    const {totalCount} = this;
    const {config} = this.props;
    let total = totalCount;
    if(config.maxItemsOpen){
      if(total > config.maxItems){
        total = config.maxItems;
      }
    }
    let totalPage = Math.floor(total / config.itemsPerPage);
    const mode = total % config.itemsPerPage;
    return mode === 0 ? totalPage : totalPage + 1;
  }

  isScrollEnd = (): boolean => {
    const {config} = this.props;
    const listDiv = this.listOutDivRef.current;
    if(!listDiv)return false;
    const clientHOrW = config.direction === DirectionType.Vertical ? listDiv.clientHeight : listDiv.clientWidth;
    const scrollHOrW = config.direction === DirectionType.Vertical ? listDiv.scrollHeight : listDiv.scrollWidth;
    const scrollTOrL = config.direction === DirectionType.Vertical ? listDiv.scrollTop : listDiv.scrollLeft;
    const isEnd = (clientHOrW + scrollTOrL === scrollHOrW)
    return isEnd;
  }

  isScrollStart = (): boolean => {
    const {config} = this.props;
    const listDiv = this.listOutDivRef.current;
    if(!listDiv)return true;
    const scrollTOrL = config.direction === DirectionType.Vertical ? listDiv.scrollTop : listDiv.scrollLeft;
    const isStart = scrollTOrL === 0
    return isStart;
  }

  handleSearchTextChange = (searchText) => {
    // this.setState({
    //   searchText
    // });
    if(searchText === '' || !searchText){
      this.handleSearchSubmit(searchText);
    }
  }

  handleSearchSubmit = (searchText) => {
    this.setState({
      searchText: searchText,
      queryStart: 0
    })
  }

  handleToolsMouseDown = evt => {
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  handleResizeCard(newCardSize, resizeEnd: boolean = false, isTop?: boolean, isLeft?: boolean, isEnd?: boolean){

    if(resizeEnd){
      window.jimuConfig.isInBuilder && this.props.builderSupportModules.widgetModules.handleResizeCard(this.props, newCardSize, isTop, isLeft, isEnd);
    }else {
      this.setState({
        currentCardSize: newCardSize
      })
    }
  }

  handleFilterChange = (sqlExprObj: IMSqlExpression) => {
    this.setState({
      currentFilter: sqlExprObj,
      queryStart: 0
    })
  }

  _getCurrentPage = () => {
    return this.state.queryStart / this.props.config.itemsPerPage + 1;
  }

  getTotalCount = () => {
    const {config} = this.props;
    let total = this.totalCount;
    if(config.maxItemsOpen){
      total = Math.min(config.maxItems, total);
    }
    return total
  }

  getPageSize = (): number => {
    const {widgetRect} = this.state;
    const cardSize = this.getCardSize();
    const {config} = this.props;
    let pageSize;
    if(config.pageStyle === PageStyle.Scroll){
      if(!widgetRect){
        return this.lastPageSize || 0;
      }
      if(config.direction === DirectionType.Vertical){
        if(widgetRect.height === 0) return this.lastPageSize || 0;
        pageSize = Math.ceil(this._getListHeight(widgetRect) / (cardSize.height + config.space)) + 1;
      }else {
        if(widgetRect.width === 0) return this.lastPageSize || 0;
        pageSize = Math.ceil(widgetRect.width / (cardSize.width + config.space)) + 1;
      }
      if(config.navigatorOpen){
        pageSize = Math.max(pageSize, config.scrollStep)
      }
    }else {
      pageSize = config.itemsPerPage;
    }
    this.lastPageSize = pageSize;
    return pageSize;
  }

  getQueryOptions = (): QueryOptions => {
    const options: QueryOptions = {}
    const ds = this.getDataSource();
    const {config, stateProps, useDataSources} = this.props
    const {sortOptionName, searchText, currentFilter} = this.state
    if(!ds)return null;
    //sort
    const useDS = useDataSources[0];

    let sortOption: SortSettingOption;
    if(config.sortOpen && config.sorts){
      sortOption = config.sorts.find((sort: SortSettingOption) => sort.ruleOptionName === sortOptionName);
      sortOption = sortOption || (config.sorts[0] as any);
      if(sortOption){
        const orderBys = [];
        sortOption.rule.forEach(sortData => {
          if(!!sortData.jimuFieldName){
            orderBys.push(`${sortData.jimuFieldName} ${sortData.order}`);
          }
        })
        if(ds.type === ArcGISDataSourceTypes.FeatureLayer){
          options.orderByFields = orderBys.join(',')
        }else{
          options.orderByFields = orderBys;
        }
      }
    }

    if(!sortOption){
      if(useDS.query && useDS.query.orderBy && useDS.query.orderBy.length > 0){
        const orderBys = [];
        useDS.query.orderBy.forEach(sortData => {
          if(!!sortData.jimuFieldName){
            orderBys.push(`${sortData.jimuFieldName} ${sortData.order}`);
          }
        })
        if(ds.type === ArcGISDataSourceTypes.FeatureLayer){
          options.orderByFields = orderBys.join(',')
        }else{
          options.orderByFields = orderBys;
        }
      }
    }

    if(useDS.query && useDS.query.where && useDS.query.where.sql){
      options.where = useDS.query.where.sql;
    }

    if(config.filter && currentFilter && currentFilter.sql){
      options.where = (options.where || '1=1') + ' AND ';
      options.where += `(${currentFilter.sql})`;
    }

    //action
    if(stateProps){
      const extentMsg = stateProps[MessageType.ExtentChange];
      //action-filter
      if(extentMsg){
        options.geometry = extentMsg.queryExtent
        if(extentMsg.querySQL){
          options.where = (options.where || '1=1') + ' AND ';
          options.where += `(${extentMsg.querySQL})`;
        }
      }

      const selectionMsg = stateProps[MessageType.DataRecordsSelectionChange];
      if(selectionMsg && selectionMsg.querySQL){
        options.where = (options.where || '1=1') + ' AND ';
        options.where += `(${selectionMsg.querySQL})`;
      }
    }

    if(config.searchOpen && config.searchFields && searchText){
      options.where = (options.where || '1=1') + ' AND ';
      options.where += `(${config.searchFields.split(',').map(field => {
        if(config.searchExact){
          return `${field} = '${searchText}'`
        }else {
          return `${field} LIKE '%${searchText}%'`
        }
      }).join(' OR ')})`;
    }

    //paging
    let queryStart = this.state.queryStart || 0;
    let num = this.getPageSize();
    if(num === 0)return null;
  
    num = config.pageStyle === PageStyle.Scroll ? num + queryStart : num;
    queryStart = config.pageStyle === PageStyle.Scroll ? 0 : queryStart;
    if(config.maxItems > -1 && config.maxItemsOpen){
      if(config.maxItems < num + queryStart){
        num = config.pageStyle === PageStyle.Scroll ? config.maxItems : (num + queryStart - config.maxItems);
      }
    }
    if(ds.type == ArcGISDataSourceTypes.FeatureLayer){
      options.start = config.pageStyle === PageStyle.Scroll ? 0 : queryStart;
      options.num = config.pageStyle === PageStyle.Scroll ? num + queryStart : num;
      
    }else {
      options.resultOffset = config.pageStyle === PageStyle.Scroll ? 0 : queryStart;
      options.resultRecordCount = config.pageStyle === PageStyle.Scroll ? num + queryStart : num;
    }
    return Object.keys(options).length > 0 ? options : null

  }

  selectCard = () => {
    const {id, selection, builderSupportModules} = this.props;
    const appConfig = this.getAppConfig()
    const {selectionInList} = builderSupportModules.widgetModules;
    if(selectionInList(selection, id, appConfig)){
      this.selectSelf();
    }
  }

  selectSelf(){
    window.jimuConfig.isInBuilder && this.props.builderSupportModules.widgetModules.selectSelf(this.props);
  }

  editBuilderAndSettingStatus = (status: Status) => {
    this.editStatus('showCardSetting', status);
    this.editStatus('builderStatus', status);
  }

  showBottomTools = (): boolean => {
    const {config} = this.props;
    return !(config.pageStyle === PageStyle.Scroll && !config.navigatorOpen) 
  }

  showDSTools = (): boolean => {
    return this.showSort() || this.showDisplaySelectedOnly() || this.showFilter() || this.showSearch()
  }

  showSort = (): boolean => {
    const {config} = this.props;
    if(!config.sortOpen || !config.sorts || config.sorts.length < 1)return false;
    const sorts = config.sorts;
    let isValid = false;
    sorts.some((sort: SortSettingOption) => {
      sort.rule && sort.rule.some(sortData => {
        if(sortData && !!sortData.jimuFieldName){
          isValid = true;
        }
        return isValid
      })
      return isValid
    })
    return isValid;
  }

  showSearch = (): boolean => {
    const {config} = this.props;
    return config.searchOpen && config.searchFields && config.searchFields !== '';
  }

  showFilter = (): boolean => {
    const {config} = this.props;
    return config.filterOpen && !!config.filter;
  }

  showDisplaySelectedOnly = (): boolean => {
    const {config} = this.props;
    return config.showSelectedOnlyOpen;
  }

  getAppConfig = (): IMAppConfig => {

    return getAppStore().getState().appConfig;
  }

  getSortItems = () : ImmutableArray<MyDropDownItem> => {
    const {config} = this.props;
    let options = [] as any;
    if(config.sorts){
      config.sorts.forEach((sort) => {
        sort.rule && sort.rule.forEach(sortData => {
          if(sortData && !!sortData.jimuFieldName){
            options.push({
              label: sort.ruleOptionName,
              event: (evt) => { this.setState({
                sortOptionName: sort.ruleOptionName,
                queryStart: 0
              }) },
            })
          }
        })
      })
    }
    return Immutable(options);
  }

  getStyle = (theme: ThemeVariables): SerializedStyles => {
    const {config, id, appMode} = this.props;
    return css`
      ${'&.list-widget-' + id} {
        overflow: visible;
        background-color: transparent;
        border: ${polished.rem(COMMON_PADDING)} solid ${polished.rgba(theme.colors.black, window.jimuConfig.isInBuilder && this.isEditing() ? 0.2 : 0)};
        .list-with-mask {
          position: absolute;
          left: 0;
          right: 0;
          bottom: 0;
          top: 0;
          background-color: ${polished.rgba(theme.colors.black, 0)};
          z-index: 1;
        }

        .widget-list {
          overflow: ${(window.jimuConfig.isInBuilder && appMode !== AppMode.Run) ? 'hidden' : 'auto'};
          height: 100%;
          width: 100%;
          align-items: ${config.alignType};
          justify-content: ${config.alignType};
          ${
            config.direction === DirectionType.Horizon ?
            `
            overflow-y: hidden;
            ` :
            `
            overflow-x: hidden;
            `
          }
        }
      }

    `
  }

  listStyle = (): SerializedStyles => {
    const {currentCardSize} = this.state;
    const {config, appMode, theme} = this.props;
    const dsToolH = this.showDSTools() ? DS_TOOL_H : 0;
    const bottomToolH = this._getBottomToolH();
    return css`
      &.list-container {
        position: relative;
        z-index: 0;
        overflow: hidden;
        ${
          config.direction === DirectionType.Horizon ?
          `
          margin-left: ${LIST_CARD_PADDING + 'px'};
          height: 100%;
          width: calc(100% - ${LIST_CARD_PADDING + 'px'});
          ` :
          `
          margin-top: ${LIST_CARD_PADDING + 'px'};
          width: 100%;
          height: calc(100% - ${LIST_CARD_PADDING + 'px'});
          `
        }

        .editing-mask-list {
          position: absolute;
          top: ${(config.direction === DirectionType.Vertical ? currentCardSize.height + dsToolH : dsToolH)}px;
          left: ${config.direction === DirectionType.Horizon ? currentCardSize.width : 0}px;
          bottom: ${polished.rem(bottomToolH)};
          right: 0;
          z-index: 10;
          background-color: ${polished.rgba(theme.colors.black, 0.2)};
        }

        .editing-mask-ds-tool {
          position: absolute;
          z-index: 10;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          background-color: ${polished.rgba(theme.colors.black, 0.2)};
        }

        .editing-mask-bottom-tool {
          position: absolute;
          z-index: 10;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          background-color: ${polished.rgba(theme.colors.black, 0.2)};
        }

        .tip-mask-list {
          position: absolute;
          top: ${dsToolH}px;
          left: 0;
          bottom: 0;
          right: 0;
          z-index: 10;
          padding-top: 20%;
          background-color: ${theme.colors.white};
        }

        .datasource-tools {
          position: relative;
          height: ${polished.rem(dsToolH)};
          padding-bottom: ${polished.rem(DS_TOOL_BOTTOM_PADDING)};
          .sort-fields-input {
            width: 200px;
            margin-left: 8px;
            margin-right: 16px;
          }
          svg {
            color: ${theme.colors.palette.dark[300]};
          }
          .tool-row {
            height: ${DS_TOOL_H - DS_TOOL_BOTTOM_PADDING}px;
          }
          .ds-tools-line {
            width: 100%;
            height: 1px;
            margin-top: ${polished.rem(6)};
            background-color: ${theme.colors.palette.light[500]};
          }
          .list-search {
            margin-bottom: ${polished.rem(-4)};
          }
        }

        .bottom-tools {
          position: relative;
          padding-top: ${polished.rem(BOTTOM_TOOL_TOP_PADDING)};
          min-height: ${polished.rem(BOTTOM_TOOL_H)};
          .scroll-navigator {
            .btn {
              border-radius: ${theme.borderRadiuses.circle};
            }
          }
        }
        
        .widget-list-list:focus {
          outline: none;
        }
        .widget-list-list {
          padding: 0;
          position: relative;
          ${(!window.jimuConfig.isInBuilder || appMode === AppMode.Run) ?
            '' :
            'overflow: hidden !important;'
          }
          height: ${`calc(100% - ${dsToolH}px - ${ bottomToolH })px`} !important;
          width: 100% !important;
          .ReactVirtualized__Grid__innerScrollContainer {
            ${
              config.direction === DirectionType.Horizon ?
              `
                height: 100% !important;
                max-height: 100% !important;
              ` :
              `
                width: 100% !important;
                max-width: 100% !important;
              `
            }
          }
        }
        ${
          config.pageStyle === PageStyle.Scroll && !config.scrollBarOpen ?
          `
          .widget-list-list::-webkit-scrollbar {
            display: none; //Safari and Chrome
          }
          .widget-list-list {
              -ms-overflow-style: none; //IE 10+
              overflow: -moz-scrollbars-none; //Firefox
          }
          ` :
          ''
        }
        
        &.animation {
          animation: ${config.pageTransition} 1s;
        }
        @keyframes ${PageTransitonType.Glide} {
          0% {
            ${
              config.direction === DirectionType.Horizon ?
              `
                margin-left: -150px;
              ` :
              `
                margin-top: -150px;
              `
            }

          }
          100% {
            ${
              config.direction === DirectionType.Horizon ?
              `
                margin-left: ${LIST_CARD_PADDING + 'px'};
              ` :
              `
                margin-top: ${LIST_CARD_PADDING + 'px'};
              `
            }

          }
        }
        @keyframes ${PageTransitonType.Fade} {
          0% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }
        @keyframes ${PageTransitonType.Float} {
          0% {
            opacity: 0;
            ${
              config.direction === DirectionType.Horizon ?
              `
                margin-right: -150px;
              ` :
              `
                margin-bottom: -150px;
              `
            }
          }
          100% {
            opacity: 1;
            ${
              config.direction === DirectionType.Horizon ?
              `
                margin-right: 0px;
              ` :
              `
                margin-bottom: 0px;
              `
            }
          }
        }
      }
    `
  }

  getDataSource = (): DataSource => {
    const useDs = this.props.useDataSources && this.props.useDataSources[0];
    if(!useDs) return null;
    const dsManager = DataSourceManager.getInstance();
    return dsManager.getDataSource(useDs.dataSourceId);
  }

  getOutputDs(): DataSource{
    let outputDsId = this.props.outputDataSources && this.props.outputDataSources[0];
    const useDs = this.props.useDataSources && this.props.useDataSources[0]
    if(!outputDsId || !useDs) return;
    let dsManager = DataSourceManager.getInstance();
    return dsManager.getDataSource(outputDsId);
  }

  createOutputDs(records: DataRecord[]){
    let outputDsId = this.props.outputDataSources && this.props.outputDataSources[0];
    const useDs = this.props.useDataSources && this.props.useDataSources[0]
    if(!outputDsId || !useDs) return;
    let dsManager = DataSourceManager.getInstance();
    if(dsManager.getDataSource(outputDsId)){
      if(dsManager.getDataSource(outputDsId).dataSourceJson.originDataSources[0].dataSourceId !== useDs.dataSourceId){
        dsManager.destroyDataSource(outputDsId);

        dsManager.createDataSource(outputDsId).then(ods => {
          ods.setRecords(records);
        });
      }else{
        const ods = dsManager.getDataSource(outputDsId);
        ods.setRecords(records);
      }
    }else {
      dsManager.createDataSource(outputDsId).then(ods => {
        ods.setRecords(records);
      });
    }
  }

  renderListTools = (ds: DataSource, queryStatus?: DataSourceStatus) => {
    const toolsDisabled = this.isEditing() || !ds || queryStatus !== DataSourceStatus.Loaded;
    const {searchText, sortOptionName, showSelectionOnly} = this.state;
    const {config, theme, id} = this.props;
    const sortItems = this.getSortItems();
    return (
      <div className="datasource-tools w-100">
        <div className="tool-row row1 d-flex justify-content-between align-items-end w-100">
        <div>
          {
            this.showSearch() &&
            <SearchBox
              theme={this.props.theme}
              placeholder={this.formatMessage('search')}
              searchText={searchText}
              onSearchTextChange={this.handleSearchTextChange}
              onSubmit={this.handleSearchSubmit}
              disabled={toolsDisabled}
              className="list-search"
            />
          }
        </div>
        <div className="d-flex align-items-center mr-1">
          {
            this.showSort() &&
            <Fragment>
              <MyDropDown 
                theme={theme}
                items={sortItems}
                toggleType="tertiary"
                toggleContent={theme => sortOptionName || sortItems && sortItems[0].label}
                appendTo="body"
                size="sm"
                caret={true}
                showActive={true}
                activeLabel={sortOptionName || sortItems && sortItems[0].label}
              />
            </Fragment>
          }
          {
            this.showFilter() &&
            <FilterPicker 
              filter={config.filter}
              selectedDs={this.getDataSource()}
              handleFilterChange={this.handleFilterChange}
              formatMessage={this.formatMessage}
              theme={theme}
              widgetId={id}
            />
          }
          {this.showDisplaySelectedOnly() &&
          <Button type="tertiary" icon size="sm" active={showSelectionOnly}
            onClick={evt => this.setState({showSelectionOnly: !this.state.showSelectionOnly})}
          ><Icon icon={showSelectedOnly} 
            color={showSelectionOnly ? theme.colors.white : theme.colors.palette.light[800]} 
            size={12}></Icon>
          </Button>
          }
          
        </div>
        
        </div>

        {/* <div className="tool-row row2 d-flex align-items-center w-100">
          {this.renderCapsulePicker()}
        </div> */}

        {/* <div className="tool-row row3 d-flex align-items-center w-100">
          <Button outline>{this.formatMessage('clearFilter')}</Button>
          <Button outline
            style={{marginLeft: '10px'}}
            onClick={evt => this.selectRecords([])}
          >{this.formatMessage('clearSelection')}</Button>
        </div> */}

        <div className="ds-tools-line"/>
        {window.jimuConfig.isInBuilder && this.isEditing() &&
          <div className="editing-mask-ds-tool" ></div>}
      </div>
    )
  }
  // ds: DataSource, info: IMDataSourceInfo, fields?: string[]
  renderList = (ds?: DataSource, queryStatus?: DataSourceStatus, queryResult?: QueryResult, count?: number) => {
    let records = queryResult && queryResult.records || [];
    const {LayoutEntry, showSelectionOnly, widgetRect, scrollStatus, listVisibleStopIndex} = this.state;
    const {config, isRTL, appMode} = this.props;
    if(queryStatus === DataSourceStatus.Unloaded){
      ds = undefined;
    }
    this.datasource = ds;
    this.queryStatus = queryStatus;
    let showLoading = false;
    if(ds){
      // if(queryStatus !== DataSourceStatus.Loading && queryStatus !== DataSourceStatus.Loaded
      //     && this.isSwitchPage){//request failed or is switch page
      //   if(config.pageStyle === PageStyle.MultiPage){
      //     this.setState({
      //       queryStart: this.lastQueryStart || 0
      //     })
      //   }
      // }
      
      if(queryStatus === DataSourceStatus.Loaded && count !== null){
        this.totalCount = count;
      }
    }

    if((window.jimuConfig.isInBuilder && !LayoutEntry) || (ds && queryStatus === DataSourceStatus.Loading)){
      showLoading = true;
    }

    let selectedRecordIds = (!ds || !config.isItemStyleConfirm ?  [] : ds.getSelectedRecordIds()).map(v => v + '') || [];
    if(!ds || !config.isItemStyleConfirm){
      records = [
        {fake: true} as any, {fake: true} as any, {fake: true} as any
      ]
    }else {
      if(!this.lastSelectedRecordIds){
        this.lastSelectedRecordIds = selectedRecordIds;
      }
      if(showSelectionOnly){
        records = records.filter(record => selectedRecordIds.indexOf(record.getId()) > -1)
      }
      this.createOutputDs(records);
    }
    if(window.jimuConfig.isInBuilder && appMode !== AppMode.Run){
      if(!showLoading && ds && config.isItemStyleConfirm){
        if(records.length < 1){
          records = [
            {fake: true} as any, {fake: true} as any, {fake: true} as any
          ]
        }
      }
    }
    
    records = records || [];
    this.records = records;

    const lastListHeight = this._getListHeight(lastWidgetSize);
    const listHeight = this._getListHeight(widgetRect) || lastListHeight || 1;
    const listWidth = widgetRect && widgetRect.width || lastWidgetSize.width;
    const recordSizePerPage = this.getPageSize();
    const itemData = this.createItemData(this.getItemsByRecords(this.records), this.getOtherProps());
    const overscanCount = (window.jimuConfig.isInBuilder && appMode !== AppMode.Run) ? 0 : recordSizePerPage;
    return (
      <div className={`list-container animation`}
        css={this.listStyle()} >
        {this.showDSTools() && 
          this.renderListTools(ds, queryStatus)}
        <List
          className="widget-list-list"
          ref={this.listRef}
          outerRef={this.listOutDivRef}
          direction={isRTL ? 'rtl' : 'ltr'}
          layout={config.direction === DirectionType.Horizon ? 'horizontal' : 'vertical'}
          itemCount={this.records.length}
          overscanCount={overscanCount}
          itemKey={this.itemKey}
          itemData={itemData}
          width={listWidth}
          height={listHeight}
          onItemsRendered={this.onItemsRendered}
          // estimatedItemSize={config.direction === DirectionType.Horizon ? (currentCardSize.width + config.space) : (currentCardSize.height + config.space)}
          itemSize={this.itemSize}
          onScroll={this.handleListScroll}
        >
          {this.itemRender}
        </List>
        
        {this.showBottomTools() &&
          <div ref={this.paginatorDiv} className="bottom-tools w-100 d-flex align-items-center justify-content-center pl-2 pr-2">
            {config.pageStyle === PageStyle.MultiPage ?
              <Pagination 
                size="sm"
                totalPage={this.getTotalPage()}
                current={this._getCurrentPage()}
                onChangePage={this.handleSwitchPage}
                >
              </Pagination> :
              <div className="d-flex scroll-navigator">
                <Button disabled={scrollStatus === 'start'} type="primary" size="sm" outline icon onClick={this.handleScrollUp} >
                  <Icon icon={config.direction === DirectionType.Horizon ? arrowLeft12 : arrowUp12} size={12}></Icon>
                </Button>
                <Button disabled={scrollStatus === 'end' && listVisibleStopIndex === this.getTotalCount() - 1} type="primary" size="sm" style={{marginLeft: polished.rem(10)}} 
                outline icon onClick={this.handleScrollDown}>
                  <Icon icon={config.direction === DirectionType.Horizon ? arrowRight12 : arrowDown12} size={12}></Icon></Button>
              </div>
            }
            {window.jimuConfig.isInBuilder && this.isEditing() && 
              <div className="editing-mask-bottom-tool" />}
          </div>
        }
        {window.jimuConfig.isInBuilder && this.isEditing() && 
              <div className="editing-mask-list" ></div>}
        {(!showLoading && (!records || records.length < 1)) && <div className="tip-mask-list text-center">{this.formatMessage(showLoading ? 'listLoading' : 'listNoData')}...</div>}
        {showLoading && <div className="jimu-small-loading"></div>}
      </div>
    )

  }

  onItemsRendered = ({
    overscanStartIndex,
    overscanStopIndex,
    visibleStartIndex,
    visibleStopIndex
  }) => {
    // All index params are numbers.
    
    this.setState({
      listVisibleStartIndex: visibleStartIndex,
      listVisibleStopIndex: visibleStopIndex
    }, () => {
      const {config} = this.props;
      if(this.isSwitchPage) {
        if(config.pageStyle === PageStyle.Scroll){
          if(this.records.length > visibleStopIndex + 1){
            this.isSwitchPage = false;
            setTimeout(() => {
              this.scrollToIndex(visibleStartIndex + config.scrollStep, 'start');
            }, 200);
            
          }
        }else{
          this.isSwitchPage = false;
        }
      }
    })
    
  }

  _getBottomToolH = () => {
    let bottomToolH = BOTTOM_TOOL_H;
    if(this.paginatorDiv.current){
      bottomToolH = this.paginatorDiv.current.clientHeight;
    }
    bottomToolH = this.showBottomTools() ? bottomToolH : 0;
    return bottomToolH;
  }

  _getListHeight = (widgetRect) => {
    const dsToolH = this.showDSTools() ? DS_TOOL_H : 0;
    const bottomToolH = this._getBottomToolH();
    if(!widgetRect)return 0;
    const height = widgetRect.height - dsToolH - bottomToolH;
    return height < 0 ? 0 : height;
  }

  createItemData = (items, otherProps) => ({
    items,
    otherProps,
  });

  getItemsByRecords = (records) => {
    const {config} = this.props;
    const {datasource} = this;
    const selectedRecordIds = (!datasource || !config.isItemStyleConfirm ?  [] : datasource.getSelectedRecordIds()).map(v => v + '');
    return records && records.map((record, index) => ({
      record,
      providerData: {
        widgetId: this.props.id,
        dataSourceId: config.isItemStyleConfirm ? (datasource && datasource.id) : undefined,
        recordIndex: index,
        record: config.isItemStyleConfirm ? record : undefined
      },
      active: !record.fake && config.isItemStyleConfirm && datasource && selectedRecordIds.indexOf(record.getId()) > -1
    }))
  }

  getOtherProps = () => {
    const {config, theme, selection, id, appMode, builderSupportModules, layouts,
      builderStatus, browserSizeMode, draggingWidget, dispatch, isRTL} = this.props;
    let selectionInList, selectionIsSelf;
    const appConfig = this.getAppConfig();
    if(window.jimuConfig.isInBuilder){
      selectionInList = builderSupportModules.widgetModules.selectionInList;
      selectionIsSelf = builderSupportModules.widgetModules.selectionIsSelf;
    }
    const selectionIsSelfB = selectionIsSelf && selectionIsSelf(selection, id, appConfig);
    const selectionIsInListB = selectionInList && selectionInList(selection, id, appConfig)
    return {
      browserSizeMode: browserSizeMode,
      space: config.space,
      isRTL: isRTL,
      builderSupportModules: builderSupportModules,
      formatMessage: this.formatMessage,
      selection: selection,
      dispatch: dispatch,
      draggingWidget: draggingWidget,
      widgetId: id,
      interact: window.jimuConfig.isInBuilder && builderSupportModules.widgetModules.interact,
      selectCard: this.selectCard,
      handleResizeCard: this.handleResizeCard,
      appMode: appMode,
      selectionIsList: selectionIsSelfB,
      selectionIsInList: selectionIsInListB,
      onChange: this.handleItemChange,
      hoverLayoutOpen: config.cardConfigs[Status.Hover].enable,
      selectable: config.cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None,
      direction: config.direction,
      theme: theme,
      isEditing: this.isEditing(),
      builderStatus: builderStatus,
      LayoutEntry: this.state.LayoutEntry,
      layouts: layouts,
      cardConfigs: config.cardConfigs,
      lockItemRatio: config.lockItemRatio,
      cardSize: this.state.currentCardSize,
      hideCardTool: this.state.hideCardTool
    }
  }

  itemSize = (index) => {
    const {config} = this.props;
    const {currentCardSize} = this.state;

    const size = config.direction === DirectionType.Horizon ?
    ((index === (this.records.length - 1) && index !== 0) ?
      currentCardSize.width : (currentCardSize.width + config.space)) : 
      ((index === (this.records.length - 1) && index !== 0) ?
      currentCardSize.height : (currentCardSize.height + config.space))
    return size;
  }

  itemRender = memo((props) => {
    const data = props['data'];
    const style = props['style'];
    const index = props['index'];
    const {items, otherProps} = data;
    const {config} = this.props;
    if(config.direction === DirectionType.Horizon){
      if(style.width !== this.itemSize(index)){
        this.listRef.current.resetAfterIndex(index);
      }
    }else {
      if(style.height !== this.itemSize(index)){
        this.listRef.current.resetAfterIndex(index);
      }
    }
    return (
        <ListCard
          gridStyle={style}
          providerData={items[index].providerData}
          active={items[index].active}
          index={index}
          {...otherProps}
        />
    )
  }, areEqual);

  itemKey = (index) => {
    const item = this.records[index];
    return `${(item.getId && item.getId()) || index}-${this.itemSize(index)}`;
  }

  render() {

    const {config, selection, id, appMode, builderSupportModules, browserSizeMode,
      draggingWidget, useDataSources, builderStatus, layouts} = this.props;
    const appConfig = this.getAppConfig()
    const {forceShowMask} = this.state;
    const isInBuilder = window.jimuConfig.isInBuilder;
    const classes = classNames(
      'jimu-widget',
      'widget-list',
      'list-widget-' + id
    );

    if(!config.itemStyle){
      return <WidgetPlaceholder widgetId={this.props.id} icon={require('./assets/icon.svg')} message="Please choose list template"/>;
    }

    let selectionInList, selectionIsSelf;
    if(isInBuilder){
      selectionInList = builderSupportModules.widgetModules.selectionInList;
      selectionIsSelf = builderSupportModules.widgetModules.selectionIsSelf;
    }

    const isInList = selectionInList && selectionInList(selection, id, appConfig);
    const isSelf = selectionIsSelf && selectionIsSelf(selection, id, appConfig);

    const queryOptions: QueryOptions = this.getQueryOptions();

    const currentLayout = appConfig.layouts[utils.findLayoutId(layouts[builderStatus], browserSizeMode, appConfig.mainSizeMode)];
    const currentLayoutType = currentLayout && currentLayout.type;

    const query = createQuery(this.props, queryOptions)
    
    const isQueryCount = !this.isSwitchPage;
    return <div className={classes} css={this.getStyle(this.props.theme)} 
      onPointerDown={evt => isInBuilder && appMode !== AppMode.Run && !isSelf && !isInList && this.handleListPointerDown(evt)}  >

      <div className={`${CSSClasses.list} d-flex`}>
        {
          isDsConfigured(this.props) ?
          <DataQueryComponent
            query={query}
            queryCount={isQueryCount}
            useDataSource={useDataSources && useDataSources[0]}
          >
            {this.renderList}
          </DataQueryComponent>
          :
          this.renderList()
        }
      </div>
      
      {((isInBuilder && appMode !== AppMode.Run) && (forceShowMask || (!isInList && !isSelf && !draggingWidget) || (!config.isItemStyleConfirm && currentLayoutType))) && 
      <div
        className="list-with-mask"
      />}
      <ReactResizeDetector handleWidth handleHeight onResize={this.onResize} />

    </div>
  }
}

export default Widget;

