/** @jsx jsx */
import { React, ThemeVariables, SerializedStyles, css, jsx, polished } from 'jimu-core';
import { Input, Button, Icon, InputProps } from 'jimu-ui';

interface Props {
  theme: ThemeVariables;
  placeholder?: string;
  searchText?: string;
  onSearchTextChange?: (searchText: string) => void;
  onSubmit?: (searchText: string) => void;
  showClear?: boolean;
  hideSearchIcon?: boolean;
  inputRef?: (ref: HTMLInputElement) => void,
}

interface Stats {
  searchText: string
}

export default class SearchBox extends React.PureComponent<Props & InputProps, Stats> {

  constructor(props){
    super(props);
    this.state = {
      searchText: props.searchText || ''
    }
  }

  componentDidUpdate(preProps){
    if(this.props.searchText !== preProps.searchText && this.props.searchText !== this.state.searchText){
      this.setState({
        searchText: this.props.searchText
      })
    }
    
  }

  handleChange = searchText => {
    this.setState({
      searchText: searchText
    }, () => {
      const {onSearchTextChange} = this.props;
      if(onSearchTextChange){
        onSearchTextChange(searchText)
      }
    })
    
  }

  handleSubmit = value => {
    const {onSubmit} = this.props;
    if(onSubmit){
      onSubmit(value)
    }
  }

  onKeyUp = (evt) => {
    if(!evt || !evt.target) return;
    if (evt.keyCode === 13) {
      
      this.handleSubmit(evt.target.value);
    }
  }

  handleClear = evt => {
    this.setState({
      searchText: ''
    })
    evt.stopPropagation();
  }

  getStyle = (): SerializedStyles => {
    const {theme} = this.props;
    return css`
      position: relative;
      .search-input {
        cursor: pointer;
        padding-left: 3px;
        border: 0;
        min-width: ${polished.rem(50)};
        max-width: ${polished.rem(200)};
        width: 45%;
        // border-bottom-width: 1px;
        // border-bottom-style: solid;
        // border-color: ${theme.colors.primary};
        background: transparent;
      }
      .search-input:focus {
        background: transparent;
      }
    `
  }

  render() {
    const { placeholder, className, showClear, hideSearchIcon, 
      inputRef, onFocus, onBlur, theme } = this.props;
    const {searchText} = this.state;
    
    return (
        
      <div css={this.getStyle()} className={`w-100 d-flex align-items-center ${className}`}>
        {!hideSearchIcon &&
          <Button type="tertiary" icon size="sm"
            onClick={evt => this.handleSubmit(this.state.searchText)} >
            <Icon size={16} icon={require('jimu-ui/lib/icons/search-16.svg')} color={theme.colors.palette.light[800]} />
          </Button>
        }
        <Input className="search-input" 
          ref={inputRef}
          placeholder={placeholder}
          onChange={e => this.handleChange(e.target.value)} 
          onBlur={onBlur}
          onFocus={onFocus}
          value={searchText} 
          onKeyDown ={ (e) => this.onKeyUp(e)}>
        </Input>
        
        {showClear &&
          <Button color="tertiary"  icon size="sm"
            onClick={this.handleSubmit} >
            <Icon size={12} icon={require('jimu-ui/lib/icons/close-12.svg')}/>
          </Button>
        }
      </div>
      
    )
  }
}
