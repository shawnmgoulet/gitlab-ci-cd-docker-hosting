/** @jsx jsx */
import {React, RepeatedDataSourceProvider, jsx, css, IMThemeVariables,
        RepeatedDataSource, AppMode, BrowserSizeMode, LayoutInfo, utils,
         LayoutItemType, appActions, Immutable, ImmutableArray, LayoutItemConstructorProps} from 'jimu-core';
import { ListGroupItem, styleUtils, Icon } from 'jimu-ui';
import { LayoutViewer } from 'jimu-layouts/layout-runtime';
import { DirectionType, CardSize, Status, SelectionModeType } from '../../config';
import { layoutUtils } from 'jimu-layouts/common';
import { MyDropDownItem } from './my-dropdown';
import { Fragment } from 'react';

const cornerSize = 12;
const cornerPosition = -10;
const sideSize = 16;
const sidePosition = -10;
const zindexHandle = 20;

const toolIsolate = require('jimu-ui/lib/icons/unlink-16.svg');
const toolSync = require('jimu-ui/lib/icons/link-16.svg');

interface Props{
  theme: IMThemeVariables,
  index: number,
  builderSupportModules: any,
  selection: LayoutInfo,
  providerData: RepeatedDataSource | RepeatedDataSource[],
  widgetId: string,
  gridStyle: any,
  isEditing: boolean,
  LayoutEntry: any,
  layouts: any,
  direction: DirectionType,
  space: number,
  active: boolean,
  selectionIsList: boolean,
  selectionIsInList: boolean,
  builderStatus: Status,
  interact: any,
  selectable: boolean,
  hoverLayoutOpen: boolean,
  draggingWidget: any,
  browserSizeMode: BrowserSizeMode,
  dispatch: any,
  isRTL: boolean,
  lockItemRatio: boolean,
  hideCardTool: boolean,
  formatMessage: (id: string, values?: {[key: string]: any}) => string,
  appMode: AppMode,
  handleResizeCard?: (newCardSize: CardSize, resizeEnd?: boolean, isLeft?: boolean, isTop?: boolean, isEnd?: boolean) => void;
  onChange: (item: ListCard) => void,
  cardConfigs?: any,
  selectCard?: () => void
}

interface State {
  isHover: boolean,
}

export default class ListCard extends React.PureComponent<Props, State>{
  isViewer: boolean = true;
  interactable: Interact.Interactable;
  resizeRef: React.RefObject<HTMLDivElement> ;
  lastResizeCall = null;

  constructor(props){
    super(props);

    this.state = {
      isHover: false,

    }

    const {providerData} = props;
    const {recordIndex} = providerData as RepeatedDataSource;
    this.resizeRef = React.createRef();
    if(recordIndex === 0){
      this.isViewer = false;
    }
  }

  componentDidUpdate(preProps){
    const {selectionIsInList, isEditing, direction, isRTL, lockItemRatio} = this.props;
    if(window.jimuConfig.isInBuilder && !this.isViewer){
      if(this.interactable){
        if(isEditing && !selectionIsInList && !lockItemRatio){
          this.interactable.resizable({
            edges: {
              top: false,
              left: direction === DirectionType.Horizon && isRTL,
              bottom: direction === DirectionType.Vertical,
              right: direction === DirectionType.Horizon && !isRTL
            }
          })
          this.interactable.resizable(true);
        }else {
          this.interactable.resizable(false);
        }
      }
      
    }
  }

  componentDidMount(){
    if(!this.isViewer && window.jimuConfig.isInBuilder && this.resizeRef.current){
      const {interact, handleResizeCard, direction, isRTL} = this.props;
      this.interactable = interact(this.resizeRef.current)
        .resizable({
          // resize from all edges and corners
          edges: {
            top: false,
            left: direction === DirectionType.Horizon && isRTL,
            bottom: direction === DirectionType.Vertical,
            right: direction === DirectionType.Horizon && !isRTL
          },
          modifiers: [
            // keep the edges inside the parent
            interact.modifiers.restrictEdges({
              outer: 'parent',
              endOnly: true,
            }),

            // minimum size
            interact.modifiers.restrictSize({
              min: { width: 20, height: 20 },
            }),
          ],
          inertia: false,
          onstart: (event: Interact.InteractEvent) => {
            event.stopPropagation();
          },
          onmove: (event: Interact.ResizeEvent) => {
            event.stopPropagation();

            if (this.lastResizeCall) {
              cancelAnimationFrame(this.lastResizeCall);
            }
            const rect = event.rect;
            const newCardSize = {
              width: rect.right - rect.left,
              height: rect.bottom - rect.top
            };

            this.lastResizeCall = requestAnimationFrame(() => {
              const edges = {} as any; //event.interaction.edges;
              handleResizeCard(newCardSize, false, edges.top, edges.left, false);
            });
          },
          onend: (event: Interact.ResizeEvent) => {
            event.stopPropagation();
            if (this.lastResizeCall) {
              cancelAnimationFrame(this.lastResizeCall);
            }
            this.lastResizeCall = requestAnimationFrame(() => {
              const rect = event.rect;
              const newCardSize = {
                width: rect.right - rect.left,
                height: rect.bottom - rect.top
              };
              handleResizeCard(newCardSize, true);
            });
          }
        })
    }
  }

  componentWillUnMount() {
    if (this.lastResizeCall) {
      cancelAnimationFrame(this.lastResizeCall);
    }
    if (this.interactable) {
      this.interactable.unset();
      this.interactable = null;
    }
  }

  formatMessage = (id: string, values?: {[key: string]: any}) => {
    return this.props.formatMessage(id, values);
  }

  handleHoverChange = (isHover) => {
    if(window.jimuConfig.isInBuilder && this.props.appMode !== AppMode.Run)return;
    this.setState({
      isHover
    })
  }

  handleItemClick = (evt) => {
    if(!window.jimuConfig.isInBuilder || this.props.appMode === AppMode.Run){
      const {onChange, active} = this.props;
      if(active){
        const tagName = (evt.target && evt.target.tagName) || '';
        if(!(tagName.toLowerCase() === 'a' || tagName.toLowerCase() === 'button' || evt.exbEventType === 'linkClick')){
          onChange(this);
        }
      }else {
        onChange(this);
      }
      if(evt.exbEventType === 'linkClick'){
        delete evt.exbEventType
      }
    }
    this._handleItemChange(evt);
  }

  handleCopyTo = (evt, status: Status, selectedLayoutItem, linked: boolean) => {
    if(!selectedLayoutItem) return;
    const {layouts, builderSupportModules, browserSizeMode, builderStatus} = this.props;
    const action = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
    const appConfig = action.appConfig;
    const originLayoutId = utils.findLayoutId(layouts[builderStatus], browserSizeMode, appConfig.mainSizeMode)
    layouts[status] && Object.keys(layouts[status]).forEach(device => {
      const desLayoutId = layouts[status][device];
      if(linked){
        const appConfigUtils = builderSupportModules.widgetModules.appConfigUtils;
        const layoutItem = appConfigUtils.getWidgetOrSectionLayoutItem(appConfig.layouts[desLayoutId], selectedLayoutItem.widgetId, LayoutItemType.Widget);
        !!layoutItem && action.removeLayoutItem({layoutId: desLayoutId, layoutItemId: layoutItem.id}, false);
      }
      action.duplicateLayoutItem(originLayoutId, desLayoutId, selectedLayoutItem.id, false, linked);
    })
    action.exec();
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  editStatus = (name, value) => {
    const {dispatch, widgetId} = this.props;
    dispatch(appActions.widgetStatePropChange(widgetId, name, value));
  }

  handleBuilderStatusChange(evt, status) {

    this.editStatus('showCardSetting', status);
    this.editStatus('builderStatus', status);

    const {selectCard} = this.props;
    selectCard()

    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  handleBreakLink = (evt) => {

    const {layouts, builderSupportModules, browserSizeMode, selection, builderStatus, dispatch} = this.props;
    const action = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
    const appConfig = action.appConfig;
    const selectedLayoutItem = layoutUtils.findLayoutItem(appConfig, selection);
    if(!selectedLayoutItem)return;
    let currentLayoutId;
    layouts[builderStatus] && Object.keys(layouts[builderStatus]).forEach(device => {
      const layoutId = layouts[builderStatus][device];
      if(device === browserSizeMode){
        currentLayoutId = layoutId;
      }
      action.duplicateLayoutItem(layoutId, layoutId, selectedLayoutItem.id, true);
      action.removeLayoutItem({layoutId: layoutId, layoutItemId: selectedLayoutItem.id}, false)
    })
    action.exec();
    
    if (selection.layoutId === currentLayoutId && selection.layoutItemId === selectedLayoutItem.id) {
      dispatch(appActions.selectionChanged(null));
    }
    const content = action.appConfig.layouts[currentLayoutId].content;
    const newItemKey = Object.keys(content)[Object.keys(content).length - 1]
    if(newItemKey){
      const newItem = content[newItemKey];
      dispatch(appActions.selectionChanged({
        layoutId: currentLayoutId,
        layoutItemId: newItem.id
      }))
    }

    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  private _handleItemChange = (evt = undefined) => {
    const {selectCard, appMode} = this.props;
    if(!window.jimuConfig.isInBuilder || this.isViewer || appMode === AppMode.Run) return;
    if(evt){
      evt.stopPropagation();
    }
    selectCard();
  }

  private getCopyDropdownItems = (showBreak: boolean): {items: ImmutableArray<MyDropDownItem>, title: string} => {
    const {cardConfigs, layouts, browserSizeMode, selection, builderStatus, builderSupportModules} = this.props;
    const action = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
    const appConfig = action.appConfig;
    const selectedLayoutItem = layoutUtils.findLayoutItem(appConfig, selection);
    if(!selection || !selectedLayoutItem || !window.jimuConfig.isInBuilder) return {
      items: Immutable([]),
      title: ''
    }
    const items = [] as any;
    let title = '';
    let linkedToRegular = true;
    let linkedToSelect = true;
    let linkedToHover = true;
    const isWidgetInLayout = (layoutId: string, widgetId: string): boolean => {
      const appConfigUtils = builderSupportModules.widgetModules.appConfigUtils;
      let widgets = appConfigUtils.getWidgetsOrSectionsInLayout(appConfig.layouts[layoutId], LayoutItemType.Widget);
      return widgets.indexOf(widgetId) > -1;
    }

    const syncToHover = () => {
      if(cardConfigs[Status.Hover].enable){
        const layoutId = utils.findLayoutId(layouts[Status.Hover], browserSizeMode, appConfig.mainSizeMode);
        if(!isWidgetInLayout(layoutId, appConfig.layouts[selection.layoutId].content[selection.layoutItemId].widgetId)){
          linkedToHover = false;
        }
        items.push({
          label: this.formatMessage('applyTo', {status: this.formatMessage('hover').toLocaleLowerCase()}),
          event: (evt) => {this.handleCopyTo(evt, Status.Hover, selectedLayoutItem, linkedToHover)},
        })
      }
    }

    const syncToSelected = () => {
      if(cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None){
        const layoutId = utils.findLayoutId(layouts[Status.Selected], browserSizeMode, appConfig.mainSizeMode);
        if(!isWidgetInLayout(layoutId, appConfig.layouts[selection.layoutId].content[selection.layoutItemId].widgetId)){
          linkedToSelect = false;
        }
        items.push({
          label: this.formatMessage('applyTo', {status: this.formatMessage('listSelected').toLocaleLowerCase()}),
          event: (evt) => {this.handleCopyTo(evt, Status.Selected, selectedLayoutItem, linkedToSelect)},
        })
      }
    }

    const syncToRegular = () => {
      const layoutId = utils.findLayoutId(layouts[Status.Regular], browserSizeMode, appConfig.mainSizeMode);
      if(!isWidgetInLayout(layoutId, appConfig.layouts[selection.layoutId].content[selection.layoutItemId].widgetId)){
        linkedToRegular = false;
      }
      items.push({
        label: this.formatMessage('applyTo', {status: this.formatMessage('regular').toLocaleLowerCase()}),
        event: (evt) => {this.handleCopyTo(evt, Status.Regular, selectedLayoutItem, linkedToRegular)},
      })
    }

    if(builderStatus === Status.Regular){
      syncToHover();
      syncToSelected();
      if(linkedToHover && linkedToSelect){
        title = this.formatMessage('linkedToAnd', {where1: this.formatMessage('listSelected').toLocaleLowerCase(), where2: this.formatMessage('hover').toLocaleLowerCase()})
      }else if(linkedToHover){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('hover').toLocaleLowerCase()})
      }else if(linkedToSelect){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('listSelected').toLocaleLowerCase()})
      }
    }else if(builderStatus === Status.Hover){
      syncToRegular();
      syncToSelected();
      if(linkedToRegular && linkedToSelect){
        title = this.formatMessage('linkedToAnd', {where1: this.formatMessage('regular').toLocaleLowerCase(), where2: this.formatMessage('listSelected').toLocaleLowerCase()})
      }else if(linkedToRegular){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('regular').toLocaleLowerCase()})
      }else if(linkedToSelect){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('listSelected').toLocaleLowerCase()})
      }
    }else {
      syncToRegular();
      syncToHover();
      if(linkedToRegular && linkedToHover){
        title = this.formatMessage('linkedToAnd', {where1: this.formatMessage('regular').toLocaleLowerCase(), where2: this.formatMessage('hover').toLocaleLowerCase()})
      }else if(linkedToRegular){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('regular').toLocaleLowerCase()})
      }else if(linkedToHover){
        title = this.formatMessage('linkedTo', {where: this.formatMessage('hover').toLocaleLowerCase()})
      }
    }
    if(showBreak){
      items.push({
        label: this.formatMessage('isolate'),
        event: this.handleBreakLink
      })
    }else{
      title = this.formatMessage('isolate');
    }

    return {
      items: Immutable(items),
      title: title
    }
  }

  getStyle = () => {
    const {theme, widgetId, selectable, appMode, direction, space, index} = this.props;
    return css`
    ${'&.list-card-' + widgetId} {
      padding: 0;
      border: 0;
      background-color: transparent;
      ${
        direction === DirectionType.Horizon ?
        `
          padding-right: ${space}px;
          height: 100% !important;
        ` :
        `
          padding-bottom: ${space}px;
          width: 100% !important;
        `
      }
      .list-card-content {
        width: 100%;
        height: 100%;
        overflow: hidden;
        background: ${theme.colors.white};
      }
      .my-dropdown {
        position: absolute;
        left: 0;
        right: ${direction === DirectionType.Vertical ? 0 : space}px;
        visibility: hidden;
        .dropdown-toggle {
          width: 100%;
        }
    }
    ${'&.list-card-' + widgetId}:last-of-type {
      ${index !== 0 && (
        direction === DirectionType.Horizon ?
        `
          padding-right: 0;
        ` :
        `
          padding-bottom: 0;
        `
      )
      }
    }
    ${'&.list-card-' + widgetId}:hover {
      ${
        (!window.jimuConfig.isInBuilder || appMode === AppMode.Run) && selectable ?
        'cursor: pointer;' :
        ''
      }
    }
    `;
  }

  _renderAction = () => {
    let handlers = [];
    const { theme, direction, space } = this.props;
    const sideHandle = css`
      box-shadow: none;

      &:after {
        position: absolute;
        content: "";
        width: ${cornerSize}px;
        height: ${cornerSize}px;
        background-color: ${theme.colors.palette.primary[500]};
        border: 2px solid ${theme.colors.white};
        border-radius: 50%;
        z-index: ${zindexHandle};
      }
    `;

    const bottomSideLine = css`
      box-shadow: none;
      height: ${sideSize}px;
      left: 0px;
      right: 0px;
      bottom: ${-1 * (sideSize / 2) + space}px;
      &:after {
        position: absolute;
        content: "";
        bottom: 50%;
        left: 0;
        right: 0;
        height: 2px;
        background-color: ${theme.colors.palette.primary[300]};
        z-index: ${zindexHandle};
      }
    `;

    const rightSideLine = css`
      box-shadow: none;
      width: ${sideSize}px;
      top: 0px;
      bottom: 0px;
      right: ${-1 * (sideSize / 2) + space}px;
      &:after {
        position: absolute;
        content: "";
        right: 50%;
        top: 0;
        bottom: 0;
        width: 2px;
        background-color: ${theme.colors.palette.primary[300]};
        z-index: ${zindexHandle};
      }
    `;

    const handle = css`
      position: absolute;
    `;

    const rightSideCursor = css`
      width: ${sideSize}px;
      top: ${-1 * sidePosition}px;
      bottom: ${-1 * sidePosition}px;
      right: ${-1 * (sideSize / 2) + space}px;

      &:after {
        top: 50%;
        right: 50%;
        margin-top: ${cornerPosition / 2}px;
        margin-right: ${cornerPosition / 2}px;
      }
    `;
    const bottomSideCursor = css`
      height: ${sideSize}px;
      left: ${-1 * sidePosition}px;
      right: ${-1 * sidePosition}px;
      bottom: ${-1 * (sideSize / 2) + space}px;

      &:after {
        left: 50%;
        bottom: 50%;
        margin-left: ${cornerPosition / 2}px;
        margin-bottom: ${cornerPosition / 2}px;
      }
    `;

    if (direction === DirectionType.Horizon) {
      handlers.push(
        <span key="10" className="list-card-rnd-resize-left-line" css={[handle, rightSideLine]}></span>
      );
      handlers.push(
        <span key="4" className="list-card-rnd-resize-right" css={[handle, sideHandle, rightSideCursor]}></span>
      );

      // handlers.push(
      //   <span key="8" className="list-card-rnd-resize-left" css={[handle, sideHandle, leftSideCursor]}></span>
      // );
    } else {
      // handlers.push(
      //   <span key="2" className="list-card-rnd-resize-top" css={[handle, sideHandle, topSideCursor]}></span>
      // );
      handlers.push(
        <span key="9" className="list-card-rnd-resize-bottom-line" css={[handle, bottomSideLine]}></span>
      );
      handlers.push(
        <span key="6" className="list-card-rnd-resize-bottom" css={[handle, sideHandle, bottomSideCursor]}></span>
      );

    }

    return handlers;
  }

  getCardToolsStyle = (theme: IMThemeVariables) => {
    return css`
        width: 100%;
        .btn {
          width: 100%;
        }
        .dropdown-toggle {
          justify-content: center;
        }
        `
  }

  renderCardTools = () => {
    const isInBuilder = window.jimuConfig.isInBuilder;
    if(!isInBuilder || this.isViewer)return;
    const {cardConfigs, selection, widgetId, appMode, builderSupportModules,
      draggingWidget, browserSizeMode, builderStatus, selectionIsInList, selectionIsList, hideCardTool} = this.props;

    const action = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
    const appConfig = action.appConfig;

    let showTools = true;
    const {appConfigUtils, BuilderDropDown, GLOBAL_DRAGGING_CLASS_NAME, BuilderButton} = builderSupportModules.widgetModules;

    const isInList = selectionIsInList;
    const isSelf = selectionIsList;
    if((!isInList && !isSelf) || (isSelf && draggingWidget) || appMode === AppMode.Run
     || hideCardTool
     ){
      showTools = false;
    }

    const showBreak = !isSelf && selection && appConfigUtils && appConfigUtils.getLayoutItemsInWidgetByLayoutInfo(appConfig as any, selection, widgetId, browserSizeMode).length > 1;
    const {items: syncItems, title: syncTitle} = this.getCopyDropdownItems(showBreak);
    const showSync = syncItems && syncItems.length > 0;

    const CardMenu = (theme) => {
      return (
      <div className="status-group d-flex flex-column align-items-center p-2" css={this.getCardToolsStyle(theme)} >
        <BuilderButton active={builderStatus === Status.Regular}
          onClick={evt => this.handleBuilderStatusChange(evt, Status.Regular)}>{this.formatMessage('regular')}</BuilderButton>
        {cardConfigs[Status.Hover].enable && 
        <BuilderButton active={builderStatus === Status.Hover} className="mt-1"
          onClick={evt => this.handleBuilderStatusChange(evt, Status.Hover)}>{this.formatMessage('hover')}</BuilderButton>}
        {cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None &&
        <BuilderButton active={builderStatus === Status.Selected} className="mt-1"
        onClick={evt => this.handleBuilderStatusChange(evt, Status.Selected)}>{this.formatMessage('listSelected')}</BuilderButton>}
        {!isSelf && (showSync || showBreak) &&
        <BuilderDropDown 
          className="mt-1 w-100" 
          toggleIsIcon={true} 
          toggleTitle={syncTitle} 
          appendTo="body"
          direction="left" 
          toggleContent={(theme) => <Icon icon={showBreak ? toolSync : toolIsolate} size={16}/>} 
          modifiers={{
            offset: {
              offset: '0, 10px'
            },
            arrow: {
              enabled: true
            }
          }} items={syncItems} />
        }
      </div>)
    };
    return (
      showTools && this.props.isEditing
      && (cardConfigs[Status.Hover].enable ||  cardConfigs[Status.Selected].selectionMode !== SelectionModeType.None)
      && this.resizeRef.current &&
      <BuilderDropDown 
        menuCss={(theme) =>
          css`
            .${GLOBAL_DRAGGING_CLASS_NAME} & {
              &.dropdown-menu.show{
                display: none;
              }
            }
          `
        }
        menuContent={CardMenu}
        modifiers={{
          offset: {
            offset: '0, 5px'
          }
        }}
        appendTo="body"
        toggleContent={theme => <div></div>}
        direction="left"
        isDropDownOpen={showTools}
      />
    )
  }

  render(){
    const {providerData, selectable, active, cardConfigs, LayoutEntry,
      selectionIsInList, isEditing, widgetId, gridStyle, lockItemRatio,
      builderStatus, layouts, hoverLayoutOpen, appMode} = this.props;
    const isInBuilder = window.jimuConfig.isInBuilder;
    const {isHover} = this.state;
    let layout = undefined;
    let bgStyle = undefined;
    if(isInBuilder && appMode !== AppMode.Run){
      layout = layouts[builderStatus]
      bgStyle = cardConfigs[builderStatus].backgroundStyle;
      if(this.isViewer){
        bgStyle = cardConfigs[Status.Regular].backgroundStyle;
        layout = layouts[Status.Regular];
      }
    }else {
      layout = layouts[Status.Regular]
      bgStyle = cardConfigs[Status.Regular].backgroundStyle;
      if(hoverLayoutOpen && isHover  && layouts[Status.Hover]){
        layout = layouts[Status.Hover]
        bgStyle = cardConfigs[Status.Hover].backgroundStyle;
      }
      if(selectable && active && layouts[Status.Selected]){
        layout = layouts[Status.Selected]
        bgStyle = cardConfigs[Status.Selected].backgroundStyle;
      }
    }

    const mergedStyle: any = {
      ...styleUtils.toCSSStyle(bgStyle || {} as any),
    };
    return (
      <RepeatedDataSourceProvider
        data={providerData}
      >

        <ListGroupItem
          active={selectable && active}
          css={this.getStyle()}
          style={gridStyle}
          className={'list-card-' + widgetId}
          onMouseEnter={evt => this.handleHoverChange(true)}
          onMouseLeave={evt => this.handleHoverChange(false)}
          onClick={this.handleItemClick}
        >

          {
            this.isViewer ?
            <div className="list-card-content d-flex list-card-content-view"
              style={mergedStyle}>
              <LayoutViewer layouts={layout}></LayoutViewer>
            </div>
            :
            (
              <Fragment>
                {this.renderCardTools()}
                <div
                  ref={this.resizeRef}
                  className={`list-card-content d-flex `}
                  style={mergedStyle}
                >
                  {
                    appMode === AppMode.Run ?
                    <LayoutViewer layouts={layout}></LayoutViewer>
                    :
                    <LayoutEntry 
                      isItemAccepted={(item: LayoutItemConstructorProps, isPlaceholder: boolean) => {
                        const supportRepeat = item.manifest.properties.supportRepeat;
                        const {builderSupportModules, widgetId, } = this.props;
                        const action = builderSupportModules.jimuForBuilderLib.getAppConfigAction();
                        const appConfig = action.appConfig;
                        const selectionInList = builderSupportModules.widgetModules.selectionInList;
                        return isEditing && supportRepeat && (!item.layoutInfo || (item.layoutInfo && selectionInList(item.layoutInfo, widgetId, appConfig)));
                      }}
                      isRepeat={true} layouts={layout} isInWidget={true} >
                    </LayoutEntry>
                  }
                  {isEditing && !selectionIsInList && !lockItemRatio && this._renderAction()}
                </div>
              </Fragment>

            )
          }
        </ListGroupItem>
      </RepeatedDataSourceProvider>
    )
  }
}