/** @jsx jsx */
import {React, jsx, ImmutableArray, SerializedStyles, IMThemeVariables} from 'jimu-core';
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Direction, Modifiers } from 'jimu-ui';

export interface MyDropDownItem{
  label: string;
  event: (evt) => void;
  hide?: boolean;
}

export interface MyDropdownProps{
  toggleContent?: (theme: IMThemeVariables) => any,
  theme?: IMThemeVariables,
  toggleTitle?: string,
  toggleIsIcon?: boolean,
  toggleType?: string,
  items?: ImmutableArray<MyDropDownItem>,
  size?: string,
  appendTo?: HTMLElement | 'body';
  menuContent?: (theme: IMThemeVariables) => any,
  caret?: boolean;
  modifiers?: Modifiers;
  isDropDownOpen?: boolean;
  direction?: Direction;
  menuCss?: (theme: IMThemeVariables) => SerializedStyles;
  className?: string;
  withBuilderStyle?: any;
  showActive?: boolean;
  activeLabel?: string;
  onDropDownOpenChange?: (isOpen: boolean) => void;
}
export default class MyDropDown extends React.PureComponent<MyDropdownProps, {isOpen: boolean}>{

  constructor(props){
    super(props);
    this.state = {
      isOpen: false
    }
  }

  onDropDownToggle = (evt) => {
    evt.stopPropagation();

    const {isDropDownOpen, onDropDownOpenChange} = this.props;
    if(isDropDownOpen !== undefined){
      onDropDownOpenChange && onDropDownOpenChange(!isDropDownOpen)
    }else {
      this.setState({isOpen: !this.state.isOpen});
    }
  }

  onItemClick = (evt, item) => {
    const {isDropDownOpen, onDropDownOpenChange} = this.props;
    if(isDropDownOpen !== undefined){
      onDropDownOpenChange && onDropDownOpenChange(false)
    }else {
      this.setState({isOpen: false});
    }
    item.event(evt);
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  onDropDownMouseClick = (evt) => {
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  render(){
    const {items, toggleContent, caret, toggleType, menuContent, modifiers, toggleIsIcon, theme, withBuilderStyle,
      isDropDownOpen, size, appendTo, toggleTitle, direction, menuCss, className, showActive, activeLabel} = this.props;
    let {isOpen} = this.state;
    isOpen = isDropDownOpen === undefined ? isOpen : isDropDownOpen;
    const MyDropdown = !!withBuilderStyle ? withBuilderStyle(Dropdown) : Dropdown;
    const MyDropdownToggle = !!withBuilderStyle ? withBuilderStyle(DropdownToggle) : DropdownToggle;
    const MyDropdownMenu = !!withBuilderStyle ? withBuilderStyle(DropdownMenu) : DropdownMenu;
    const MyDropdownItem = !!withBuilderStyle ? withBuilderStyle(DropdownItem) : DropdownItem;
    return (
      <MyDropdown size={size} toggle={this.onDropDownToggle} direction={direction}
        isOpen={isOpen} className={`my-dropdown ${className}`}>
        {toggleContent &&
          <MyDropdownToggle icon={toggleIsIcon} title={toggleTitle} size={size} type={toggleType} caret={caret}>
            {toggleContent(theme)}
          </MyDropdownToggle>
        }
        <MyDropdownMenu appendTo={appendTo} modifiers={modifiers} css={menuCss && menuCss(theme)} >
          {menuContent ? 
            menuContent(theme) :
            items && items.asMutable().map((item: MyDropDownItem, i: number) => {
              return !item.hide && 
              <MyDropdownItem key={i} className="no-user-select" active={showActive && item.label === activeLabel}
                onClick={(evt) => this.onItemClick(evt, item)}>
                {item.label}
              </MyDropdownItem>;
            })
          }
        </MyDropdownMenu>
      </MyDropdown>
    )
      
  }
}
