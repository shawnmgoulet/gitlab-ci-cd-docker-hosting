/** @jsx jsx */
import {React, jsx, IMSqlExpression, DataSource, IMThemeVariables, polished} from 'jimu-core';
import {Icon, Button } from 'jimu-ui';
import { SqlExpressionRuntime } from 'jimu-ui/sql-expression-runtime';
import MyDropDown from './my-dropdown';

interface Props{
  filter: IMSqlExpression,
  selectedDs: DataSource,
  widgetId: string,
  handleFilterChange: (sqlExprObj: IMSqlExpression) => void,
  formatMessage: (id: string, values?: {[key: string]: any}) => string,
  theme: IMThemeVariables
}

interface Stats{
  isOpen: boolean,
  finalFilter: IMSqlExpression,
}

const filterIcon = require('jimu-ui/lib/icons/filter.svg');
const filterOnIcon = require('../assets/filter-on.svg');

export default class FilterPicker extends React.PureComponent<Props, Stats>{

  currentFilter: IMSqlExpression;
  constructor(props){
    super(props);
    this.currentFilter = props.filter
    this.state = {
      isOpen: false,
      finalFilter: undefined
    }
  }

  formatMessage = (id: string, values?: {[key: string]: any}) => {
    return this.props.formatMessage(id, values);
  }

  componentDidUpdate(preProps, preState){
    if(preProps.filter !== this.props.filter){
      this.props.handleFilterChange(undefined);
      this.currentFilter = this.props.filter;
      this.setState({
        finalFilter: undefined
      })
    }
  }

  onDropDownToggle = (evt) => {
    evt.stopPropagation();
    this.setState({isOpen: !this.state.isOpen});
  }

  onItemClick = (evt, item) => {
    this.setState({isOpen: false});
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  onDropDownMouseClick = (evt) => {
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  handleFilterChange = (sqlExprObj: IMSqlExpression) => {
    this.currentFilter = sqlExprObj;
  }

  handleApplyClick = (evt) => {
    const {handleFilterChange} = this.props;
    handleFilterChange(this.currentFilter);
    this.setState({
      isOpen: false,
      finalFilter: this.currentFilter
    })
  }

  handleClearClick = (evt) => {
    const {handleFilterChange} = this.props;
    handleFilterChange(undefined);
    this.setState({
      isOpen: false,
      finalFilter: undefined
    })
  }

  render(){
    const {filter, selectedDs, theme, widgetId} = this.props;
    const {isOpen, finalFilter} = this.state;
    return (
    <MyDropDown 
      size="sm"
      appendTo="body"
      theme={theme}
      isDropDownOpen={isOpen}
      onDropDownOpenChange={isOpen => {this.setState({isOpen: isOpen})}}
      toggleIsIcon={true}
      toggleType="tertiary"
      modifiers={{
        offset: {
          offset: '0, 5px'
        }
      }}
      toggleContent={theme => <Icon icon={!!finalFilter ? filterOnIcon : filterIcon} color={theme.colors.palette.light[800]} size={12}/>}
      menuContent={
        theme => {
          return (
            <div style={{paddingLeft: polished.rem(20), paddingRight: polished.rem(20)}}>
              <div>
                <SqlExpressionRuntime 
                  id={`list-${widgetId}-filter-picker-sql-expression-runtime`}
                  selectedDs={selectedDs}
                  config={this.currentFilter || filter}
                  onChange={this.handleFilterChange}
                />
              </div>
              <div className="d-flex justify-content-between w-100 mt-3" >
                <Button diabled={finalFilter && this.currentFilter && 
                  finalFilter.sql === this.currentFilter.sql} onClick={this.handleApplyClick} outline type="primary">{this.formatMessage('apply')}</Button>
                <Button onClick={this.handleClearClick} outline >{this.formatMessage('clear')}</Button>
              </div>
            </div>
          )
        }
      }
    />)
  }
}