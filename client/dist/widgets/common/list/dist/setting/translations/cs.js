define({
  default: 'Ř_default_______________ů',
  _action_query_label: 'Ř_Query by extent________________ů',
  listSource: 'Ř_List Source____________ů',
  maxItems: 'Ř_Max items___________________ů',
  itemPerPage: 'Ř_Item per page______________ů',
  selectMode: 'Ř_Select mode____________ů',
  style: 'Ř_Style___________ů',
  quickStyle: 'Ř_quickStyle_____________________ů',
  more: 'Ř_More_________ů',
  direction: 'Ř_Direction___________________ů',
  verticalAlignment: 'Ř_Vertical alignment___________________ů',
  horizontalAlignment: 'Ř_Horizontal alignment_____________________ů',
  verticalSpacing: 'Ř_Vertical spacing_________________ů',
  horizontalSpacing: 'Ř_Horizontal spacing___________________ů',
  differentOddEvenItems: 'Ř_Different odd & even items___________________________ů',
  mouseOverStyle: 'Ř_Mouse-over style_________________ů',
  selectedStyle: 'Ř_Selected style_______________ů',
  pagingStyle: 'Ř_Paging style_____________ů',
  scroll: 'Ř_Scroll_____________ů',
  multiPage: 'Ř_Multi-page_____________________ů',
  pageTransition: 'Ř_Page transition________________ů',
  tools: 'Ř_Tools___________ů',
  filter: 'Ř_Filter_____________ů',
  sort: 'Ř_Sort_________ů',
  single: 'Ř_Single_____________ů',
  multiple: 'Ř_Multiple_________________ů',
  regular: 'Ř_Regular_______________ů',
  hover: 'Ř_Hover___________ů',
  listSelected: 'Ř_Selected_________________ů',
  off: 'Ř_Off_______ů',
  on: 'Ř_On_____ů',
  enable: 'Ř_Enable_____________ů',
  start: 'Ř_Start___________ů',
  chooseTemplateTip: 'Ř_Choose a template to start with_________________ů',
  listUseGuide: 'Ř_Drag and drop widgets from Element panel to customize your list or you can_______________________________________ů ',
  resettingTheTemplate: 'Ř_reset the template___________________ů',
  emptyTemplate: 'Ř_Empty template_______________ů',
  chooseSortingFields: 'Ř_Choose sorting fields______________________ů',
  waitingForDatasource: 'Ř_Waiting for datasource_______________________ů',
  zeroHint: 'Ř_Zero is infinite_________________ů',
  selectSortFields: 'Ř_Select sort fields___________________ů', 
  sortSelected: 'Ř_{selectedCount} items selected_______________________________ů',
  openFilterBuilder: 'Ř_Open Filter Builder____________________ů',
  chooseSearchingFields: 'Ř_Choose searching fields________________________ů',
  selectSearchFields: 'Ř_Select search fields_____________________ů', 
  searchSelected: 'Ř_{selectedCount} items selected_______________________________ů',
  exactMatch: 'Ř_Exact match____________ů',
  scrollBar: 'Ř_Scroll bar_____________________ů',
  navigator: 'Ř_Navigator___________________ů',
  states: 'Ř_States_____________ů',
  itemHeight: 'Ř_Item height____________ů',
  itemWidth: 'Ř_Item width_____________________ů',
  lockItemRatio: 'Ř_Lock width / height ratio__________________________ů'
});