export default {
  _widgetLabel: 'å_List_________ø',
  _action_query_label: 'å_Query by extent________________ø',
  syncTo: 'å_Sync to {status}_________________ø',
  regular: 'å_Regular_______________ø',
  hover: 'å_Hover___________ø',
  listSelected: 'å_Selected_________________ø',
  listLoading: 'å_Loading_______________ø',
  pleaseSelect: 'å_Please select______________ø',
  listNoData: 'å_No data_______________ø',
  selectSortFields: 'å_Select sort fields___________________ø',
  clearFilter: 'å_Clear filter_____________ø',
  clearSelection: 'å_Clear Selection________________ø',
  
}