export default {
  _widgetLabel: 'List',
  _action_query_label: 'Query by extent',
  applyTo: 'Apply to {status}',
  regular: 'Regular',
  hover: 'Hover',
  listSelected: 'Selected',
  listLoading: 'Loading',
  pleaseSelect: 'Please select',
  listNoData: 'No data',
  selectSortFields: 'Select sorting fields',
  clearFilter: 'Clear filter',
  clearSelection: 'Clear selection',
  isolate: 'Isolate',
  linkedToAnd: 'Linked to {where1} and {where2}',
  linkedTo: 'Linked to {where}',
  apply: 'Apply',
  clear: 'Clear',

  
}