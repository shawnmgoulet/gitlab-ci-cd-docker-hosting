import { React } from 'jimu-core'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { IMUser } from 'jimu-core';
import { HubSignInModal } from './hub-sign-in-modal';
import { AuthenticationProvider } from '@esri/arcgis-rest-auth';

// TODO: extend or mixin dropdown props?
interface UserMenuProps {
  user?: IMUser;
  onSignIn: (provider?: AuthenticationProvider) => void;
  onSignOut: () => void;
  useHub?: boolean
}

interface UserMenuState {
  dropdownOpen: boolean;
  modalOpen: boolean;
}

export class UserMenu extends React.PureComponent<UserMenuProps, UserMenuState>{

  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      modalOpen: false
    };
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onSignIn = this.onSignIn.bind(this);
  }


  toggleDropdown() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  toggleModal() {
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  }

  onSignIn () {
    this.props.onSignIn();
  }

  render(){
    const {
      user
    } = this.props;

    let content;
    if (user) {
      // TODO: show thumbnail image too
      content = (<Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
        <DropdownToggle type="primary" caret>
          {user.fullName ? user.fullName : user.username}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={this.props.onSignOut}>Sign out</DropdownItem>
        </DropdownMenu>
      </Dropdown>);
    } else {
      if (this.props.useHub) {
        content = (<div>
          <button className="btn btn-outline-primary" onClick={this.toggleModal}>Sign in</button>
          <HubSignInModal title="Sign in" isOpen={this.state.modalOpen} toggle={this.toggleModal} onSignIn={this.props.onSignIn}></HubSignInModal>
        </div>);
      } else {
        content = <button className="btn btn-outline-primary" onClick={this.onSignIn}>Sign in</button>;
      }
    }

    return <div className="jimu-user-menu">{content}</div>;
  }
}
