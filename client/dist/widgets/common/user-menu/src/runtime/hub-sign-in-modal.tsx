import { React } from 'jimu-core';
import { Icon } from 'jimu-ui';

import { 
  Collapse,
  Modal, ModalBody, ModalHeader
} from 'reactstrap';

// TODO: extend or mixin dropdown props?
interface HubSignInModalProps {
  isOpen: boolean;
  toggle: () => void;
  title: string;
  className?: string;
  onSignIn: (provider?: string) => void;
}

export class HubSignInModal extends React.PureComponent<HubSignInModalProps, {collapseOpen: boolean}>{

  constructor(props) {
    super(props);

    this.state = {
      collapseOpen: false
    };
    this.toggleCollapse = this.toggleCollapse.bind(this);
    this.onAgoClick = this.onAgoClick.bind(this);
    this.onFacebookClick = this.onFacebookClick.bind(this);
    this.onGoogleClick = this.onGoogleClick.bind(this);
  }

  toggleCollapse() {
    this.setState(prevState => ({
      collapseOpen: !prevState.collapseOpen
    }));
  }

  onAgoClick() {
    this.props.toggle();
    this.props.onSignIn();
  } 

  onFacebookClick() {
    this.props.toggle();
    this.props.onSignIn('facebook');
  } 

  onGoogleClick() {
    this.props.toggle();
    this.props.onSignIn('google');
  } 

  render(){
    const {
      isOpen,
      toggle,
      className,
      title
    } = this.props;

    return (
      <Modal isOpen={isOpen} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>{title}</ModalHeader>
        <ModalBody>
          <h3>A Hub community account allows you to:</h3>
        <ul>
          <li>Follow Hub initiatives to stay up to date and informed</li>
          <li>Register for Hub events to engage with the Hub</li>
          <li>Create analyses and narratives to tell your own stories with Hub data</li>
        </ul>
        <hr />
        <h3>Sign In/Sign Up</h3>
        <div>
          <button onClick={this.onFacebookClick} className="btn btn-block btn-social btn-facebook">
            <img src="//d1iq7pbacwn5rb.cloudfront.net/opendata-ui/assets/assets/images/facebook_btn-cece4e56a57496bc2da4c62d8fbd691d.png" alt="" /> Using Facebook
          </button>
          <button onClick={this.onGoogleClick} className="btn btn-block btn-social btn-google">
            <img src="//d1iq7pbacwn5rb.cloudfront.net/opendata-ui/assets/assets/images/google_btn-8b7e21aaa900e1cb388760f964bb0b5b.png" alt="" /> Sign in with Google
          </button>
          <button onClick={this.onAgoClick} className="btn btn-block btn-social btn-ago">
            <img src="//d1iq7pbacwn5rb.cloudfront.net/opendata-ui/assets/assets/images/bank-34bcd4a0cb3cf2f66d6161f95a3be3d0.png" alt="" /> Sign in with Organization
          </button>
        </div>
        <hr />
        <h4><a role="button" onClick={this.toggleCollapse} className="signin-faq"><Icon icon={this.state.collapseOpen ? require('jimu-ui/lib/icons/arrow-down.svg')
        : require('jimu-ui/lib/icons/arrow-right.svg')} /> FAQ</a></h4>
        <Collapse tag="dl" isOpen={this.state.collapseOpen}>
          <dt>"Why can't I login with my public account?"</dt>
          <dd>Your public account does not give you access to Hub features. You need a community account.</dd>
          <dt>"Why can't I sign up with an email other than Google or Facebook?"</dt>
          <dd>Email signups need manual admin approval and by using these alternatives, we can get you signed in immediately.</dd>
        </Collapse>
        </ModalBody>
      </Modal>
    );
  }
}
