import { ImmutableObject } from 'seamless-immutable';
import { IconTextSize } from './runtime/common';

export enum DisplayType {
  Stack = 'STACK',
  SideBySide = 'SIDEBYSIDE'
}

export interface Config {
  onlyOpenOne: boolean;
  displayType: DisplayType;
  vertical: boolean;
  iconStyle: 'circle' | 'rectangle';
  showLabel: boolean;
  iconSize: IconTextSize;
  space: number;
  size: ImmutableObject<{
    [widgetId: string]: {
      width: number,
      height: number
    }
  }>
}

export type IMConfig = ImmutableObject<Config>;