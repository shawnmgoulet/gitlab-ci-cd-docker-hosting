import { extensionSpec, React, getAppStore, LayoutContextToolProps, appActions } from 'jimu-core';


export default class Next implements extensionSpec.ContextTool {
  index = 3;
  id = 'controller-roll-list-next';
  widgetId: string;

  classes: { [widgetId: string]: React.ComponentClass<{}> } = {};

  visible(props: LayoutContextToolProps) {
    return true;
  }
  
  checked(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState = getAppStore().getState().widgetsState[widgetId] || {};
    const checked = widgetState && widgetState.layoutAbility;
    return !!checked;
  }

  getGroupId() {
    return null;
  }

  getTitle() {
    return 'Layout editing';
  }

  getIcon() {
    return require('jimu-ui/lib/icons/tool-edit.svg');
  }

  onClick(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState = getAppStore().getState().widgetsState[widgetId] || {};
    getAppStore().dispatch(appActions.widgetStatePropChange(widgetId, 'layoutAbility', !widgetState.layoutAbility));
  }
  
  getSettingPanel(props: LayoutContextToolProps): React.ComponentClass<{}> {
    return null;
  }
}




