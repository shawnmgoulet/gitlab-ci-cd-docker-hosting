import { Popper, PopperProps } from 'jimu-ui';
import { React, ReactRedux, BrowserSizeMode, IMState } from 'jimu-core';

interface ExtraProps {
  pageId: string;
  sizemode: BrowserSizeMode;
}

class _SensitivePopper extends React.PureComponent<PopperProps & { onRequestClose?: () => void } & ExtraProps> {
  componentDidUpdate(prevProps: PopperProps & ExtraProps) {
    if (this.props.onRequestClose && this.props.pageId !== prevProps.pageId || this.props.sizemode !== prevProps.sizemode) {
      this.props.onRequestClose();
    }
  }

  render() {
    const { pageId, sizemode, onRequestClose, ...others } = this.props;
    return <Popper {...others} />;
  }
}

function mapStateToProps(state: IMState): ExtraProps {
  return {
    pageId: state.appRuntimeInfo.currentPageId,
    sizemode: state.browserSizeMode,
  };
}

export const SensitivePopper = ReactRedux.connect<ExtraProps, {}, PopperProps>(mapStateToProps)(_SensitivePopper);
