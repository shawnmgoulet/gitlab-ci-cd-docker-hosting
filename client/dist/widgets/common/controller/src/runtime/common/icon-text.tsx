/** @jsx jsx */
import { React, css, jsx, polished, IMIconResult, ThemeButtonType } from 'jimu-core';
import { WIDGET_ITEM_SIZES, LABEL_HEIGHT } from '../consts';
import { Button, Icon } from 'jimu-ui';

export const getItemLength = (iconSize: IconTextSize, showLabel: boolean, shape: 'circle' | 'rectangle') => {
  let size = WIDGET_ITEM_SIZES[iconSize];
  if (showLabel) {
    size = size + LABEL_HEIGHT;
  }

  const padding = calcPadding(iconSize, shape);
  size = size + padding * 2;
  return size;
};

const calcPadding = (size: IconTextSize, shape: 'circle' | 'rectangle'): number => {
  const circle = shape === 'circle';
  if (!circle) return 6;
  if (size === IconTextSize.Small) return 4;
  if (size === IconTextSize.Medium) return 2;
  if (size === IconTextSize.Large) return 0;
}

export enum IconTextSize {
  Small = 'SMALL',
  Medium = 'MEDIUM',
  Large = 'LARGE',
}

export interface IconTextProps {
  active?: boolean,
  label?: string,
  showLabel?: boolean,
  icon?: IMIconResult | string,
  shape: 'circle' | 'rectangle';
  size: IconTextSize,
  marker?: string, //icon
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void,
  onMarkerClick?: (e: React.MouseEvent<HTMLButtonElement>) => void,
  innerRef?: (ref) => void;
  buttonType?: ThemeButtonType;
  style?: React.CSSProperties;
  className?: string;
}

export class IconText extends React.PureComponent<IconTextProps> {
  static defaultProps: Partial<IconTextProps> = {
    size: IconTextSize.Medium,
    icon: {} as IMIconResult,
    buttonType: 'primary'
  }

  getStyle = () => {
    const { showLabel, size, shape } = this.props;
    const length = getItemLength(size, showLabel, shape);
    const padding = calcPadding(size, shape);
    return css`
      display: flex;
      align-items:center;
      flex-direction: column;
      justify-content: ${showLabel ? 'space-around' : 'center'};
      width: ${polished.rem(length)} !important;
      height: ${polished.rem(length)};
      .button-container {
        padding: ${padding}px;
        position: relative;
        text-align: center;
        .icon-btn.marker-container {
          position: absolute;
          right: 0;
          top: 0;
          padding: 0;
          border-radius: 50%;
          display: flex;
          align-items: center;
          justify-content: center;
        }
      }
      .text-capitalize {
        text-align: center;
        width: 100%;
        min-height: ${polished.rem(21)};
      }

      
    `;
  }

  getButtonSize = (size: IconTextSize) => {
    if (size === IconTextSize.Small) {
      return 'sm'
    } else if (size === IconTextSize.Large) {
      return 'lg'
    }
  }

  render() {
    let { style, className, label, showLabel, size, shape, icon, marker, onClick, active, onMarkerClick, innerRef, buttonType } = this.props;
    icon = icon as IMIconResult;
    return <div css={this.getStyle()} onClick={onClick} ref={innerRef} style={style} className={className} title={label}>
      <div className="button-container">
        {!!marker && <Button size="xs" icon onClick={onMarkerClick} className="marker-container">
          <Icon size={10} icon={marker}></Icon>
        </Button>}
        <Button className="widget-icon" icon type={buttonType} size={this.getButtonSize(size)} active={active} style={{ borderRadius: shape === 'circle' ? '50%' : undefined }}>
          <Icon
            color={icon.properties && icon.properties.color}
            icon={icon.svg || icon}></Icon>
        </Button>
      </div>
      {showLabel && <div className="text-capitalize text-truncate">{label}</div>}
    </div>
  }
}