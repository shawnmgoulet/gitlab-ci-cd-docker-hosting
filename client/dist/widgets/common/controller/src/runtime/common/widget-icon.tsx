import { React, IMWidgetJson, IMState, ReactRedux, IMIconResult } from 'jimu-core';
import { IconText, IconTextSize } from './icon-text';
import { Loading } from 'jimu-ui';
const MARK_ICON = require('jimu-ui/lib/icons/close-12.svg');

export interface WidgetIconProps {
  size: IconTextSize;
  showLabel?: boolean;
  shape: 'circle' | 'rectangle';
  active?: boolean;
  onWidgetClick?: (widgetNode: HTMLDivElement, widgetId: string) => void;
  widgetId?: string;
  showMarker?: boolean;
  onRemoveWidget?: () => void;
  style?: React.CSSProperties;
  className?: string;
}

interface ExtraProps {
  widgetJson: IMWidgetJson;
}

type Props = WidgetIconProps & ExtraProps;

class _WidgetIcon extends React.PureComponent<Props> {

  handleClick = e => {
    e.stopPropagation();
    if (this.props.onWidgetClick) {
      this.props.onWidgetClick(e.currentTarget, this.props.widgetId);
    }
  };

  render() {
    const { style, className, widgetJson, size, showLabel, shape, active, showMarker, onRemoveWidget } = this.props;

    if (widgetJson) {
      return <IconText
        className={className}
        style={style}
        active={active}
        size={size}
        shape={shape}
        showLabel={showLabel}
        label={widgetJson.label}
        icon={widgetJson.icon as IMIconResult}
        marker={showMarker ? MARK_ICON : ''}
        onMarkerClick={onRemoveWidget}
        onClick={this.handleClick}
      />;
    }
    return <Loading />;

  }
}

const mapStateToProps = (state: IMState, ownProps: WidgetIconProps) => {
  const { widgetId } = ownProps;
  return {
    widgetJson: state.appConfig.widgets[widgetId],
  };
};

export const WidgetIcon = ReactRedux.connect<ExtraProps, {}, WidgetIconProps>(mapStateToProps)(_WidgetIcon);