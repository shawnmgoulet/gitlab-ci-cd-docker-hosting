export const MOCK_LAYOUT_ORDER_NAME = 'ADD';
export const DROP_ZONE_PLACEHOLDER_WIDTH = 5;
export const BASE_LAYOUT_NAME = 'controller';
export const CONTAINER_LAYOUT_NAME = 'openwidget';
export const MIN_WIDGET_WIDTH = 150;
export const MIN_WIDGET_HEIGHT = 50;
export const BASE_POPPER_INDEX = 21;
export const ARROW_ICON_SIZE = 20;
export const ADD_WIDGET_SIZE = 30;

export const LABEL_HEIGHT = 21;

export const WIDGET_ITEM_SIZES = {
  SMALL: 24,
  MEDIUM: 32,
  LARGE: 46
};