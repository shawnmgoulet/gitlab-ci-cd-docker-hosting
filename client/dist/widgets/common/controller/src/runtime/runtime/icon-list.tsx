/** @jsx jsx */
import { React, css, jsx, classNames } from 'jimu-core';
import { IconTextSize, WidgetIcon } from '../common';


interface Props {
  vertical?: boolean;
  lists: string[];
  className?: string;
  style?: React.CSSProperties;
  start?: number; // start index of the content, include
  end?: number; // end index of the content, exclude
  onClick?: (widgetNode: HTMLDivElement, widgetId: string) => void;
  activeIds?: string[];
  space?: number;
  item: {
    size: IconTextSize;
    showLabel?: boolean;
    shape: 'circle' | 'rectangle';
  }
}

export class IconList extends React.PureComponent<Props> {

  getStyle = () => {
    return css`
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
    `;
  }

  filterList = (lists: string[]) => {
    const { start, end } = this.props;
    lists = lists || []
    return lists.slice(start, end);
  }

  createItem = (widgetId: string, index: number) => {
    const { item, space, onClick, vertical, activeIds } = this.props;

    const style = {
      marginLeft: !vertical && index > 0 ? space : 'unset',
      marginTop: vertical && index > 0 ? space : 'unset'
    }

    return <WidgetIcon
      key={index}
      className="widget-icon"
      style={style}
      active={activeIds.indexOf(widgetId) > -1}
      widgetId={widgetId}
      showMarker={false}
      {...item}
      onWidgetClick={(e) => onClick(e, widgetId)} ></WidgetIcon>
  }

  render() {
    const { className, style, lists, vertical } = this.props;
    const list = this.filterList(lists);
    return <div
      css={this.getStyle()}
      style={style}
      className={classNames('icon-list', { 'flex-column': vertical }, className)}>
      {list.map((item, index) => this.createItem(item, index))}
    </div>
  }
}