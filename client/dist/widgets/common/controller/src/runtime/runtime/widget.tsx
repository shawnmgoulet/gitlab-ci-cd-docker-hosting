import {
  React, IMState, IMLayoutJson, utils as jimuUtils, ReactRedux, ThemeVariables, ImmutableObject, LayoutItemType, ContainerType,
  appConfigUtils, Immutable, lodash, ReactResizeDetector, IMRuntimeInfos, appActions, getAppStore, WidgetState, BrowserSizeMode, WidgetManager, WidgetJson
} from 'jimu-core';
import { IMConfig } from '../../config';
import { RollList, getItemLength } from '../common'
import * as utils from '../utils';
import { IconList } from './icon-list'
import { MultiplePopper, WidgetInfo } from './multiple-popper';
import { MobilePanel } from 'jimu-ui';

const LayoutName = 'controller';

const position = Immutable({
  left: 70,
  top: 70,
  offset: 30,
  space: 30
});

const widgetRenderDefSize = {
  width: 300,
  height: 300
}

interface OwnProps {
  id: string;
  layoutId?: string;
  config: IMConfig;
  translate: (id: string) => string;
  generation?: number;
};

interface ExtraProps {
  mobile: boolean;
  layout: IMLayoutJson;
  theme: ThemeVariables;
  currentPageId: string;
  showInView: boolean;
  widgetsRuntimeInfo: IMRuntimeInfos;
  widgetJsons: ImmutableObject<{ [widgetId: string]: WidgetJson }>,
}

export type IMWidgetInfo = ImmutableObject<WidgetInfo>;

interface State {
  openedWidgets: string[];
  activeWidgetNode: HTMLDivElement;
  start: number,
  end: number,
  openMobilePanel?: boolean;
}

export class _Widget extends React.PureComponent<OwnProps & ExtraProps, State>{
  domNode: HTMLElement;
  forbiddenZone: ClientRect | DOMRect;
  resizeDebounce: any;
  wm: WidgetManager;
  constructor(props) {
    super(props);
    this.state = {
      openedWidgets: [],
      activeWidgetNode: null,
      start: 0,
      end: 1,
      openMobilePanel: false
    }
    this.toggleMobilePanel = this.toggleMobilePanel.bind(this);
    this.mobilePanel = this.mobilePanel.bind(this);
    this.resizeDebounce = lodash.debounce(this.onRollLayoutResize.bind(this), 100);
    this.wm = WidgetManager.getInstance();
  }

  componentDidMount() {
    this.forbiddenZone = this.domNode.getBoundingClientRect();
  }

  getOpeningWidgets = (): string[] => {
    const { layout, widgetsRuntimeInfo } = this.props;
    const widgetIds = utils.getWidgetIdsFromLayout(layout) || [];

    const openingWidgets = Object.keys(widgetsRuntimeInfo).filter(widgetId => {
      const runtimeInfo = widgetsRuntimeInfo[widgetId];
      return !!(runtimeInfo && runtimeInfo.state && runtimeInfo.state === WidgetState.Opened);
    });

    return widgetIds.filter(widgetId => {
      return openingWidgets.indexOf(widgetId) > -1;
    });
  }

  closeWidgets = (widgetIds: string[]) => {
    widgetIds.forEach(widgetId => {
      this.closeWidget(widgetId);
    })
  }

  closeWidget = (widgetId: string) => {
    const action = appActions.closeWidget(widgetId);
    getAppStore().dispatch(action);
  }

  closeOpeningWidgets = () => {
    const openedWidgets = this.getOpeningWidgets();
    this.closeWidgets(openedWidgets);
  }

  openWidget = (widgetId: string) => {
    const action = appActions.openWidget(widgetId);
    getAppStore().dispatch(action);
  }

  componentDidUpdate(preProps: OwnProps & ExtraProps) {
    const { generation: preGeneration } = preProps;
    const { generation } = this.props;

    if (generation !== preGeneration) {
      this.closeOpeningWidgets();
      this.setState({ openedWidgets: [] })
    }

    const { currentPageId: prePageId, showInView: preShowInView } = preProps;
    const { currentPageId, showInView } = this.props;

    if (currentPageId !== prePageId || (showInView !== preShowInView && !showInView)) {
      this.closeOpeningWidgets();
    }
  }

  private handleClick = (widgetNode: HTMLDivElement, widgetId: string, ) => {
    let { openedWidgets } = this.state;

    if (openedWidgets.indexOf(widgetId) < 0) {
      openedWidgets = [...openedWidgets, widgetId];
      this.setState({ openedWidgets });
    }
    this.setState({ activeWidgetNode: widgetNode });

    const { config: { onlyOpenOne }, mobile } = this.props;
    const keepOneOpened = mobile ? true : onlyOpenOne;

    const openingWidgtes = this.getOpeningWidgets();
    if (keepOneOpened) {
      this.closeOpeningWidgets();
      if (openingWidgtes.indexOf(widgetId) < 0) {
        this.openWidget(widgetId);
      }
    } else {
      if (openingWidgtes.indexOf(widgetId) < 0) {
        this.openWidget(widgetId);
      } else {
        this.closeWidget(widgetId);
      }
    }
  }

  private getIconUnitLength = (): number => {
    const { iconSize, space, showLabel, iconStyle } = this.props.config;
    const baseLength = getItemLength(iconSize, showLabel, iconStyle);
    return baseLength + space;
  }

  private calculateStartEnd = (width: number, height: number): [number, number] => {
    const { config: { vertical, space } } = this.props;
    const length = utils.getListContentLength(width, height, vertical, space);
    const unitLength = this.getIconUnitLength();
    const number = utils.getOneScreenNumber(length, unitLength);
    return utils.calculateStartEnd(0, number);
  }

  private onRollLayoutResize = (width: number, height: number) => {
    const counts = utils.getLayoutItemCounts(this.props.layout);
    if (!counts) {
      return;
    }
    const [start, end] = this.calculateStartEnd(width, height);
    this.setState({ start, end });
  }

  private onListArrowClick = (previous: boolean, rollOne: boolean = true) => {
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const { disablePrevious, disableNext } = this.calculateRollListState(this.state.start, this.state.end, counts);
    if ((disablePrevious && previous) || (disableNext && !previous)) {
      return;
    }

    const [start, end] = utils.onListArrowClick(previous, counts, this.state.start, this.state.end, rollOne);
    this.setState({ start, end });
  }

  private calculateRollListState = (start: number, end: number, counts: number) => {
    const { showArrow, disablePrevious, disableNext } = utils.calculateRollListState(start, end, counts);
    return { showArrow, disablePrevious, disableNext };
  }

  private genarateWidgetInfos = (): WidgetInfo[] => {
    const { openedWidgets } = this.state;
    const openingWidgtes = this.getOpeningWidgets();
    const widgetInfos = openedWidgets.map(openedWidget => {
      const show = openingWidgtes.indexOf(openedWidget) > -1;
      return {
        id: openedWidget,
        show
      }
    });
    return widgetInfos;
  }

  private onWidgetPopperClose = (id: string) => {
    this.closeWidget(id);
  }

  private getItemSetting = (config: IMConfig) => {
    const { iconSize, iconStyle, showLabel } = config;
    return {
      size: iconSize,
      shape: iconStyle,
      showLabel,
    }
  }

  private setRef = (ref: HTMLDivElement) => {
    this.domNode = ref && ref.parentElement;
  }

  widgetIconList = () => {
    const { layout, config, config: { vertical: vertical, space } } = this.props;
    const { start, end } = this.state;
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const { showArrow, disableNext, disablePrevious } = this.calculateRollListState(start, end, counts);
    const widgetIds = utils.getWidgetIdsFromLayout(layout);
    const openedWidgets = this.getOpeningWidgets();
    return <RollList
      innerRef={this.setRef}
      vertical={vertical}
      showArrow={showArrow}
      disableNext={disableNext}
      disablePrevious={disablePrevious}
      onArrowClick={this.onListArrowClick}>
      <IconList
        start={start}
        end={end}
        lists={widgetIds}
        space={space}
        vertical={vertical}
        activeIds={openedWidgets}
        onClick={this.handleClick}
        item={this.getItemSetting(config)} >
      </IconList>
    </RollList>
  }


  getWidgetTitle = (widgetId: string) => {
    const { widgetJsons } = this.props;
    return widgetJsons && widgetJsons[widgetId] && widgetJsons[widgetId].label;
  }

  getWidgetComponent = (widgetId: string) => {
    let widgetContent;
    const { widgetsRuntimeInfo: wris } = this.props;
    const isClassLoaded = wris[widgetId] && wris[widgetId].isClassLoaded
    if (!isClassLoaded) {
      this.wm.loadWidgetClass(widgetId);
    }
    if (isClassLoaded) {
      let Widget = this.wm.getWidgetClass(widgetId);
      widgetContent = <Widget />;
    } else {
      widgetContent = <div>Loading...</div>;
    }
    return widgetContent;
  }

  openWidgetsRenderer = () => {
    const { config: { vertical, onlyOpenOne }, id, layout } = this.props;
    const { activeWidgetNode } = this.state;
    const placement = onlyOpenOne ? !vertical ? 'bottom' : 'right-start' : 'bottom-start';
    const offset = onlyOpenOne ? vertical ? [0, 0] : [0, 0] : [0, 0];
    const widgetIds = utils.getWidgetIdsFromLayout(layout);
    return <MultiplePopper
      widgetIds={widgetIds}
      forbiddenZone={this.forbiddenZone}
      widgets={this.genarateWidgetInfos()}
      referenceNode={activeWidgetNode}
      widgetId={id}
      placement={placement}
      offset={offset}
      container="body"
      position={position}
      defaultSize={widgetRenderDefSize}
      onClose={this.onWidgetPopperClose} />;
  }

  toggleMobilePanel() {
    this.closeOpeningWidgets();
  }

  mobilePanel() {
    const openedWidgets = this.getOpeningWidgets();
    const openedWidget = openedWidgets && openedWidgets[0];
    const widget = this.getWidgetComponent(openedWidget);
    const title = this.getWidgetTitle(openedWidget);
    return <MobilePanel title={title} open={!!openedWidget} toggle={this.toggleMobilePanel}>{widget}</MobilePanel>;
  }

  render() {
    const { mobile } = this.props;
    return <React.Fragment>
      {mobile && this.mobilePanel()}
      {!mobile && this.openWidgetsRenderer()}
      {this.widgetIconList()}
      <ReactResizeDetector handleWidth handleHeight onResize={this.resizeDebounce}></ReactResizeDetector>
    </React.Fragment>
  }
}

const mapStateToProps = (state: IMState, ownProps: OwnProps) => {
  const layouts = state.appConfig.widgets[ownProps.id].layouts;
  const info = appConfigUtils.getWidgetOrSectionContainerInfo(state.appConfig, ownProps.id, LayoutItemType.Widget, state.browserSizeMode);
  const viewIds = state.appRuntimeInfo.currentViewIds || [];

  let showInView = true;
  if (info && info.type === ContainerType.View) {
    showInView = viewIds.indexOf(info.id) > -1;
  }

  const widgetsRuntimeInfo = state.widgetsRuntimeInfo || Immutable({}) as IMRuntimeInfos;

  return {
    widgetJsons: state.appConfig.widgets,
    mobile: state.browserSizeMode === BrowserSizeMode.Small,
    widgetsRuntimeInfo,
    theme: state.theme,
    currentPageId: state.appRuntimeInfo.currentPageId,
    showInView,
    layout: state.appConfig.layouts[jimuUtils.findLayoutId(layouts[LayoutName], state.browserSizeMode, state.appConfig.mainSizeMode)],
    pageId: (info && info.type === ContainerType.Page) ? info.id : ''
  }
}

export const WidgetRuntime = ReactRedux.connect<ExtraProps, {}, OwnProps>(mapStateToProps)(_Widget);