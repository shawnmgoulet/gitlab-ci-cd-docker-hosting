/** @jsx jsx */
import { BaseWidget, jsx, css, AllWidgetProps, IMState, AppMode, polished, BrowserSizeMode, IMRuntimeInfos, Immutable, utils, IMLayoutJson, lodash } from 'jimu-core';
import { IMConfig } from '../config';
import { WidgetRuntime } from './runtime';
import defaultMessages from './translations/default';
import { MIN_WIDGET_WIDTH, BASE_LAYOUT_NAME, MIN_WIDGET_HEIGHT } from './consts';

interface ExtraProps {
  isInBuilder: boolean;
  appMode: AppMode;
  layout: IMLayoutJson;
  browserSizeMode: BrowserSizeMode;
  widgetsRuntimeInfo: IMRuntimeInfos;
  bbox: {
    left: number,
    top: number,
    width: number,
    height: number,
  }
}

interface State {
  generation?: number;
  moves: number;
}

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{
  static mapExtraStateProps = (state: IMState, ownProps: AllWidgetProps<IMConfig>) => {
    const widgetsRuntimeInfo = state.widgetsRuntimeInfo || Immutable({}) as IMRuntimeInfos;
    const layouts = state.appConfig.widgets[ownProps.id].layouts;
    const layout = state.appConfig.layouts[utils.findLayoutId(layouts[BASE_LAYOUT_NAME], state.browserSizeMode, state.appConfig.mainSizeMode)];

    const { layoutId, layoutItemId } = ownProps;
    const appConfig = state && state.appConfig;
    let bbox = null;
    if (window.jimuConfig.isInBuilder) {
      const box = lodash.getValue(appConfig, `layouts.${layoutId}.content.${layoutItemId}.bbox`)
      bbox = {
        left: box.left,
        top: box.top,
        width: box.width,
        height: box.height
      };
    }
    return {
      bbox,
      layout,
      widgetsRuntimeInfo,
      browserSizeMode: state.browserSizeMode,
      isInBuilder: state.appContext.isInBuilder,
      appMode: state.appRuntimeInfo.appMode
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      generation: 0,
      moves: 0
    }
  }

  componentDidUpdate(prveProps: AllWidgetProps<IMConfig> & ExtraProps) {
    const { config: { onlyOpenOne: preOnlyOpenOne, displayType: preDisplayType }, appMode: preAppMode, browserSizeMode: preBrowserSizeMode, bbox: preBbox } = prveProps;
    const { config: { onlyOpenOne, displayType }, appMode, browserSizeMode, bbox } = this.props;

    if (preOnlyOpenOne !== onlyOpenOne || displayType !== preDisplayType || appMode !== preAppMode || browserSizeMode !== preBrowserSizeMode) {
      this.setState({ generation: this.state.generation + 1 });
    }

    if (!lodash.isDeepEqual(bbox, preBbox)) {
      this.setState({ moves: this.state.moves + 1 })
    }
  }

  getStyle = () => {
    const { config: { vertical } } = this.props;
    return css`
      overflow: hidden;
      white-space: nowrap;
      .controller-container {
        width: 100%;
        height: 100%;
        padding: 10px;
        min-width: ${!vertical ? polished.rem(MIN_WIDGET_WIDTH) : polished.rem(MIN_WIDGET_HEIGHT)};
        min-height: ${vertical ? polished.rem(MIN_WIDGET_WIDTH) : polished.rem(MIN_WIDGET_HEIGHT)};
      }

    `;
  }

  isBuilder = () => {
    const { isInBuilder, appMode } = this.props;
    return isInBuilder && appMode !== AppMode.Run;
  }


  translate = (id: string) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] })
  }

  render() {
    const { builderSupportModules: bsm, id, config } = this.props;
    const { moves, generation } = this.state;
    const isBuilder = this.isBuilder();
    const WidgetInBuilder = isBuilder && bsm && bsm.widgetModules.WidgetInBuilder;
    return <div className="widget-controller jimu-widget shadow" css={this.getStyle()}>
      <div className="controller-container">
        {!isBuilder && <WidgetRuntime generation={this.state.generation} id={id} config={config} translate={this.translate}></WidgetRuntime>}
        {isBuilder && WidgetInBuilder && <WidgetInBuilder id={id} moves={moves} generation={generation} config={config} nls={this.translate}></WidgetInBuilder>}
      </div>
    </div>
  }
}