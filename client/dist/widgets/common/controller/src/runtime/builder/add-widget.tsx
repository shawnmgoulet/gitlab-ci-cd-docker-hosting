/** @jsx jsx */
import { React, jsx, css, IMThemeVariables, themeUtils, LayoutItemConstructorProps, WidgetType } from 'jimu-core';
import { WidgetListPopper } from 'jimu-ui/setting-components';
import { IconText, IconTextSize } from '../common';
const addIcon = require('jimu-ui/lib/icons/add-16.svg');

interface Props {
  className?: string;
  onAddWidget?: (item: LayoutItemConstructorProps) => void;
  showLabel?: boolean;
  shape: 'circle' | 'rectangle';
  size: IconTextSize;
  style?: React.CSSProperties;
}


interface State {
  open?: boolean;
}

export class AddWidget extends React.PureComponent<Props, State> {
  button: any;
  buildTheme: IMThemeVariables;
  constructor(props) {
    super(props);
    this.state = {
      open: false
    }
    this.toggle = this.toggle.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  toggle() {
    this.setState({ open: !this.state.open });
  }

  handleClick(evt: React.MouseEvent<HTMLDivElement>) {
    evt.stopPropagation();
    this.toggle();
  }

  componentDidMount() {
    this.buildTheme = themeUtils.getBuilderThemeVariables();
  }

  popperStyle = () => {
    return css`
      width: 300px;
      height: 300px;
      overflow-y: auto;
    `;
  }

  isItemAccepted(item: LayoutItemConstructorProps): boolean {
    return item.manifest.properties.type !== WidgetType.Layout &&
      item.manifest.properties.type !== WidgetType.Controller;
  }

  render() {
    const { onAddWidget, showLabel, shape, size, style } = this.props;
    const { open } = this.state;
    return <React.Fragment>
      <IconText
        style={style}
        buttonType="default"
        showLabel={showLabel}
        shape={shape}
        size={size}
        icon={{ svg: addIcon } as any}
        innerRef={ref => this.button = ref}
        onClick={this.handleClick}>
      </IconText>
      {open && <WidgetListPopper
        builderTheme={this.buildTheme}
        referenceElement={this.button}
        isItemAccepted={this.isItemAccepted}
        onItemSelect={onAddWidget}
        onClose={this.toggle}
      >
      </WidgetListPopper>}
    </React.Fragment>
  }
}