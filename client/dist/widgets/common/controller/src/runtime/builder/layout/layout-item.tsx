import { React, IMLayoutItemJson, classNames } from 'jimu-core';
import { withRnd } from 'jimu-layouts/layout-builder';
import { getAppConfigAction, AppConfigAction } from 'jimu-for-builder'
import { WidgetIconProps, WidgetIcon as _WidgetIcon } from '../../common';
const WidgetIcon = withRnd(false)(_WidgetIcon as any);

type Props = WidgetIconProps & {
  layoutItem: IMLayoutItemJson;
  layoutId: string;
  layoutItemId: string;
  syncWidgetsToOtherSizeMode: (appConfigAction: AppConfigAction) => void;
  draggable?: boolean;
}

export default class LayoutItem extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.remove = this.remove.bind(this);
  }

  remove() {
    const { layoutId, layoutItemId } = this.props;
    let appConfigAction = getAppConfigAction();
    appConfigAction.removeLayoutItem({ layoutId, layoutItemId }, true);
    this.props.syncWidgetsToOtherSizeMode(appConfigAction);
    appConfigAction.exec();
  }

  render() {
    const { layoutItem, layoutId, layoutItemId, widgetId, onRemoveWidget, onWidgetClick, draggable, className, ...others } = this.props;

    return <WidgetIcon
      {...others}
      className={classNames({ 'no-drag-action': !draggable }, className)}
      layoutId={layoutId}
      layoutItem={layoutItem}
      widgetId={layoutItem.widgetId}
      layoutItemId={layoutItem.id}
      onRemoveWidget={this.remove}
      onWidgetClick={onWidgetClick}
    />
  }
}