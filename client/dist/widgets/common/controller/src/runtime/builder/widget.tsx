/** @jsx jsx */
import {
  React, jsx, IMState, IMLayoutJson, appActions, Immutable, utils as jimuUtils, IMSizeModeLayoutJson, ReactRedux, ThemeVariables,
  getAppStore, ImmutableObject, LayoutItemConstructorProps, lodash, ReactResizeDetector, IMRuntimeInfos, WidgetState, BrowserSizeMode
} from 'jimu-core';
import { IMConfig } from '../../config';
import { RollList, getItemLength } from '../common'
import * as utils from '../utils';
import { getAppConfigAction, AppConfigAction } from 'jimu-for-builder';
import LayoutIconList from './layout/layout';
import { LayoutContainer } from './layout-container';
import { addItemToLayout } from 'jimu-layouts/layout-builder';
import { BASE_LAYOUT_NAME, CONTAINER_LAYOUT_NAME } from '../consts';
import { AddWidget } from './add-widget';
import { Placement } from 'jimu-ui';

interface OwnProps {
  id: string;
  config: IMConfig;
  translate: (id: string) => string;
  generation?: number;
  moves: number;
};

interface ExtraProps {
  layout: IMLayoutJson;
  layouts: IMSizeModeLayoutJson;
  popperLayouts: IMSizeModeLayoutJson;
  theme: ThemeVariables;
  widgetsRuntimeInfo: IMRuntimeInfos;
  sizeMode: BrowserSizeMode;
  widgetState?: {
    layoutAbility: boolean
  }
}

type Size = ImmutableObject<{
  width: number,
  height: number
}>

interface State {
  activeIconNode: HTMLDivElement;
  start: number,
  end: number,
  size?: Size
}

export class _Widget extends React.PureComponent<OwnProps & ExtraProps, State>{
  rollContent: HTMLDivElement;
  resizeDebounce: any;
  dropzoneNode: HTMLDivElement;
  rolllistNode: HTMLDivElement;
  constructor(props) {
    super(props);
    this.state = {
      activeIconNode: null,
      start: 0,
      end: 4,
      size: Immutable({ width: 0, height: 0 })
    }

    this.addWidgetToLayout = this.addWidgetToLayout.bind(this);
    this.addWidgetFromList = this.addWidgetFromList.bind(this);
    this.resizeDebounce = lodash.debounce(this.onContentSizeChange.bind(this), 200);
    this.syncWidgetsToOtherSizeMode = this.syncWidgetsToOtherSizeMode.bind(this);
    this.setDropzoneRef = this.setDropzoneRef.bind(this);
    this.setRollListRef = this.setRollListRef.bind(this);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
  }

  componentDidMount() {
    getAppStore().dispatch(appActions.widgetStatePropChange(this.props.id, 'onArrowClick', this.onListArrowClick));
    this.addTargetEvents();
  }

  addTargetEvents() {
    ['click', 'touchstart'].forEach(event =>
      document.addEventListener(event, this.handleDocumentClick as any, true)
    );
  }

  removeTargetEvents() {
    ['click', 'touchstart'].forEach(event =>
      document.removeEventListener(event, this.handleDocumentClick as any, true)
    );
  }

  handleDocumentClick(evt: React.MouseEvent<any>) {
    let { widgetState, id } = this.props;
    if (!widgetState.layoutAbility) return;
    const target = evt.target as HTMLElement
    const isToolItem = !!(target.classList && target.classList.contains('tool-item-icon'));
    if (isToolItem) return;
    const isdropzone = target === this.dropzoneNode;
    const outBoundary = !this.rolllistNode.contains(target);
    if (isdropzone || outBoundary) {
      getAppStore().dispatch(appActions.widgetStatePropChange(id, 'layoutAbility', false));
    }
  }

  setDropzoneRef(ref: HTMLDivElement) {
    this.dropzoneNode = ref;
  }

  setRollListRef(ref: HTMLDivElement) {
    this.rolllistNode = ref;
  }


  componentWillUnmount() {
    this.removeTargetEvents();
  }

  getOpeningWidgets = (): string[] => {
    const { layout, widgetsRuntimeInfo } = this.props;
    const widgetIds = utils.getWidgetIdsFromLayout(layout) || [];

    const openedWidgets = Object.keys(widgetsRuntimeInfo).filter(widgetId => {
      const runtimeInfo = widgetsRuntimeInfo[widgetId];
      return !!(runtimeInfo && runtimeInfo.state && runtimeInfo.state === WidgetState.Opened);
    });

    return widgetIds.filter(widgetId => {
      return openedWidgets.indexOf(widgetId) > -1;
    });
  }

  closeWidgets = (widgetIds: string[]) => {
    widgetIds.forEach(widgetId => {
      this.closeWidget(widgetId);
    })
  }

  closeWidget = (widgetId: string) => {
    const action = appActions.closeWidget(widgetId);
    getAppStore().dispatch(action);
  }

  closeOpeningWidgets = () => {
    const openingWidgets = this.getOpeningWidgets();
    this.closeWidgets(openingWidgets);
  }

  openWidget = (widgetId: string) => {
    const action = appActions.openWidget(widgetId);
    getAppStore().dispatch(action);
  }

  componentDidUpdate(preProps: OwnProps & ExtraProps, prveState: State) {
    const { config: { space: preSpace, iconSize: preIconSize, showLabel: preShowLabel }, layout, layout: { order: preOrder = [] },
      generation: preGeneration } = preProps;
    const { config: { space, iconSize, showLabel }, layout: { order = [] }, generation } = this.props;

    if (generation !== preGeneration) {
      this.closeOpeningWidgets();
    }

    if (order.length !== preOrder.length) { //When the number of widgets changes
      const widgetIds = utils.getWidgetIdsFromLayout(layout);
      this.updateWidgetJsonWidgets(widgetIds);
      if (order[order.length - 1] !== preOrder[preOrder.length - 1]) {
        this.reflowWhenCountsChange(true);
      } else {
        this.reflowWhenCountsChange(false);
      }
    }
    /**
     * When the overall size changes, or the item size changes
     */
    if (space !== preSpace || iconSize !== preIconSize || this.state.size !== prveState.size || showLabel !== preShowLabel) {
      this.reflowWhenSizeChange();
    }
  }

  private getIconUnitLength = (): number => {
    const { iconSize, space, showLabel, iconStyle } = this.props.config;
    const baseLength = getItemLength(iconSize, showLabel, iconStyle);
    return baseLength + space;
  }

  private onPopperClose = () => {
    this.closeOpeningWidgets();
  }

  private updateWidgetJsonWidgets = (widgetIds: string[]) => {
    getAppConfigAction().editWidget({ widgets: widgetIds }).exec();
  }

  private handleClick = (widgetNode: HTMLDivElement, widgetId: string) => {
    this.setState({ activeIconNode: widgetNode });
    this.closeOpeningWidgets();
    this.openWidget(widgetId)
  }

  private onWidgetSizeChanged = (widgetId: string, width: number, height: number) => {
    if (!widgetId) {
      return
    }
    let { config, config: { size = Immutable({}) } } = this.props;
    const oneSize = Immutable({ [widgetId]: { width, height } });
    size = size.merge(oneSize);
    config = config.set('size', size);
    this.updateWidgetConfig(config);
  }

  private updateWidgetConfig = (config: IMConfig) => {
    getAppConfigAction().editWidgetConfig(this.props.id, config).exec();
  }

  private publishRollListStateToWidgetState = (disablePrevious: boolean, disableNext: boolean, showArrow: boolean) => {
    getAppStore().dispatch(appActions.widgetStatePropChange(this.props.id, 'disablePrevious', disablePrevious));
    getAppStore().dispatch(appActions.widgetStatePropChange(this.props.id, 'disableNext', disableNext));
    getAppStore().dispatch(appActions.widgetStatePropChange(this.props.id, 'showArrow', showArrow));
  }

  private addWidgetToLayout(itemProps: LayoutItemConstructorProps, containerRect: ClientRect, itemRect: ClientRect, insertIndex: number) {
    const { layout } = this.props;
    const layoutInfo = {
      layoutId: layout.id,
    };
    let appConfigAction = getAppConfigAction();
    addItemToLayout(appConfigAction.appConfig, itemProps, layoutInfo, containerRect, itemRect, insertIndex)
      .then(((result) => {
        const { updatedAppConfig } = result;
        appConfigAction = getAppConfigAction(updatedAppConfig);
        this.syncWidgetsToOtherSizeMode(appConfigAction);
        appConfigAction.exec();
      }));
  }

  private syncWidgetsToOtherSizeMode(appConfigAction: AppConfigAction) {
    const { layout, sizeMode, layouts } = this.props;
    Object.keys(layouts).forEach(sm => {
      if (sizeMode !== sm) {
        appConfigAction.copyLayoutContent(layout.id, layouts[sm]);
      }
    })
  }


  private addWidgetFromList(item: LayoutItemConstructorProps) {
    const { layout } = this.props;
    const insertIndex = (layout && layout.order && layout.order.length) || 0;
    const containerRect = {} as ClientRect;
    const itemRect = {} as ClientRect;
    this.addWidgetToLayout(item, containerRect, itemRect, insertIndex);
  }

  private reflowWhenCountsChange(reverse?: boolean) {
    let counts = utils.getLayoutItemCounts(this.props.layout);
    const { size: { width, height } } = this.state;
    const [start, end] = this.calculateStartEnd(width, height, reverse);
    const { showArrow, disablePrevious, disableNext } = utils.calculateRollListState(start, end, counts);
    this.publishRollListStateToWidgetState(disablePrevious, disableNext, showArrow);
    this.setState({ start, end });
  }

  private reflowWhenSizeChange() {
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const { size: { width, height } } = this.state;
    const [start, end] = this.calculateStartEnd(width, height, false);
    const { showArrow, disablePrevious, disableNext } = utils.calculateRollListState(start, end, counts);
    this.publishRollListStateToWidgetState(disablePrevious, disableNext, showArrow);
    this.setState({ start, end });
  }

  private onContentSizeChange = (width: number, height: number) => {
    let { size } = this.state;
    size = size.set('width', width).set('height', height);
    this.setState({ size });
  }

  private calculateStartEnd = (width: number, height: number, reverse?: boolean): [number, number] => {
    const { config: { vertical, space } } = this.props;
    const unitLength = this.getIconUnitLength();
    const offset = unitLength; //The width/height of add widget and space
    const length = utils.getListContentLength(width, height, vertical, space, offset);
    const number = utils.getOneScreenNumber(length, unitLength);
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const origin = reverse ? counts : 0;
    return utils.calculateStartEnd(origin, number, reverse);
  }

  private onListArrowClick = (previous: boolean, rollOne: boolean = true) => {
    let counts = utils.getLayoutItemCounts(this.props.layout);
    const [start, end] = utils.onListArrowClick(previous, counts, this.state.start, this.state.end, rollOne);
    const { showArrow, disablePrevious, disableNext } = utils.calculateRollListState(start, end, counts);
    this.publishRollListStateToWidgetState(disablePrevious, disableNext, showArrow);
    this.setState({ start, end });
  }

  private getItemSetting = (config: IMConfig) => {
    const { iconSize, iconStyle, showLabel } = config;
    return {
      size: iconSize,
      shape: iconStyle,
      showLabel,
    }
  }

  addWidget = () => {
    const { vertical, space, showLabel, iconSize, iconStyle } = this.props.config;
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const empty = !counts;
    const spacing = !empty ? space : 'unset';
    const style = {
      marginLeft: !vertical ? spacing : 'unset',
      marginTop: vertical ? spacing : 'unset',
      zIndex: 1
    }
    return <AddWidget showLabel={!empty ? showLabel : false} size={iconSize} shape={iconStyle} style={style} onAddWidget={this.addWidgetFromList}></AddWidget>
  }

  widgetIconList = () => {
    const { layouts, config, config: { vertical, space }, widgetState } = this.props;
    const { start, end } = this.state;
    const counts = utils.getLayoutItemCounts(this.props.layout);
    const { showArrow, disablePrevious, disableNext } = utils.calculateRollListState(start, end, counts);
    const openingWidgets = this.getOpeningWidgets();
    return <RollList
      vertical={vertical}
      showArrow={showArrow}
      disableNext={disableNext}
      disablePrevious={disablePrevious}
      onArrowClick={this.onListArrowClick}
      innerRef={this.setRollListRef}
      placeholder={this.addWidget()}>
      <LayoutIconList
        start={start}
        end={end}
        space={space}
        layouts={layouts}
        vertical={vertical}
        dropZoneRef={this.setDropzoneRef}
        draggable={widgetState && widgetState.layoutAbility}
        activeIds={openingWidgets}
        onWidgetClick={this.handleClick}
        addWidgetToLayout={this.addWidgetToLayout}
        syncWidgetsToOtherSizeMode={this.syncWidgetsToOtherSizeMode}
        item={this.getItemSetting(config)}
      />
    </RollList>
  }

  layoutContailer = () => {
    const { id, popperLayouts, config: { vertical, size = {} }, theme, layout, moves } = this.props;
    const { activeIconNode } = this.state;
    const openingWidgets = this.getOpeningWidgets() || [];
    const placement: Placement = !vertical ? 'bottom' : 'right-start'
    return <LayoutContainer
      parentId={id}
      generation={moves}
      placement={placement}
      onClose={this.onPopperClose}
      theme={theme}
      size={size[openingWidgets[0]]}
      layouts={popperLayouts}
      widgetIds={utils.getWidgetIdsFromLayout(layout)}
      onWidgetSizeChange={this.onWidgetSizeChanged}
      widgetId={openingWidgets[0]}
      reference={activeIconNode}></LayoutContainer>
  }

  render() {
    return <React.Fragment>
      {this.layoutContailer()}
      {this.widgetIconList()}
      <ReactResizeDetector handleWidth handleHeight onResize={this.resizeDebounce}></ReactResizeDetector>
    </React.Fragment>
  }
}

const mapStateToProps = (state: IMState, ownProps: OwnProps) => {
  const layouts = state.appConfig.widgets[ownProps.id].layouts;
  const widgetsRuntimeInfo = state.widgetsRuntimeInfo || Immutable({}) as IMRuntimeInfos;

  return {
    widgetState: state.widgetsState[ownProps.id],
    widgetsRuntimeInfo,
    theme: state.theme,
    layout: state.appConfig.layouts[jimuUtils.findLayoutId(layouts[BASE_LAYOUT_NAME], state.browserSizeMode, state.appConfig.mainSizeMode)],
    layouts: layouts[BASE_LAYOUT_NAME],
    popperLayouts: layouts[CONTAINER_LAYOUT_NAME],
    sizeMode: state.browserSizeMode
  }
}

export const WidgetInBuilder = ReactRedux.connect<ExtraProps, {}, OwnProps>(mapStateToProps)(_Widget) as any;