
/** @jsx jsx */
import { css, jsx, polished, classNames, ThemeVariables } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { IMConfig, DisplayType } from '../config';
import { Switch, Input, Label, ButtonGroup, Button, Icon, defaultMessages as jimuDefaultMessages } from 'jimu-ui';
import defaultMessages from './translations/default';
import { IconTextSize } from '../runtime/common';

const rightArrowIcon = require('jimu-ui/lib/icons/direction-right.svg');
const downArrowIcon = require('jimu-ui/lib/icons/direction-down.svg');

enum ShapeType { Circle, Rectangle };


interface ChooseShapeProps {
  title?: string;
  className?: string,
  type: ShapeType,
  active: boolean,
  theme: ThemeVariables,
  onClick: () => any
}

export const ChooseShape = ({ className, title, type, active, theme, onClick }: ChooseShapeProps) => {
  const white = theme ? theme.colors.white : '';
  const cyan500 = theme ? theme.colors.palette.primary[700] : '';
  const gray900 = theme ? theme.colors.palette.dark[900] : '';
  const style = css`
    background-color: ${white};
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${polished.rem(28)};
    height: ${polished.rem(28)};
    margin: 2px;
    &.active{
      outline: 2px ${cyan500} solid;
    }
    .inner {
      width: 66%;
      height: 66%;
      border: 1px ${gray900} solid;
      border-radius: 2px;
      &.circle {
        border-radius: 50%;
      }
    }
  
  `;


  return <div css={style} onClick={onClick} title={title}
    className={classNames('choose-shape', { active: active }, className)}>
    <div className={classNames('inner', { rectangle: type === ShapeType.Rectangle }, { circle: type === ShapeType.Circle })}></div>
  </div>;
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig>>{
  getStyle = () => {
    const { theme } = this.props;
    const white = theme && theme.colors ? theme.colors.white : '';
    const black = theme && theme.colors ? theme.colors.black : '';
    const gray300 = theme && theme.colors ? theme.colors.palette.light[500] : '';
    const gray700 = theme && theme.colors ? theme.colors.palette.dark[600] : '';

    return css`
      font-size: 13px;
      font-weight: lighter;
      .setting-row-item {
        width: 100%;
        height: ${polished.rem(30)};
        display: flex;
        align-items: center;
        label {
          margin-bottom: unset;
        }
      }
      .icon-button {
        width:${polished.rem(30)};
        height:${polished.rem(26)};
        border-color: #4d545b;
        padding: 0 !important;
        color: ${gray700} !important;
        background-color: ${white} !important;
        &.active {
            color: ${black} !important;
            background-color: ${gray300} !important;
        }
        > .jimu-icon {
            margin-right: 0 !important;
        }
      }
    `;
  }

  onConfigChange = (key: string, value: any) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(key, value)
    });
  }

  onRadioChange = (e: React.ChangeEvent<HTMLInputElement>, key, value) => {
    const checked = e.currentTarget.checked;
    if (!checked) {
      return;
    }
    if (key === 'displayType') {
      value = this.getDisplayType(value);
    }
    this.onConfigChange(key, value);
  }

  getDisplayType = (isStack: boolean) => {
    return isStack ? DisplayType.Stack : DisplayType.SideBySide;
  }

  translate = (id: string, jimu?: boolean) => {
    const message = jimu ? jimuDefaultMessages : defaultMessages;
    return this.props.intl.formatMessage({ id: id, defaultMessage: message[id] })
  }

  render() {
    const { config: { onlyOpenOne, displayType, showLabel, iconSize, space, iconStyle, vertical }, theme } = this.props;
    return <div className="widget-setting-controller jimu-widget-setting" css={this.getStyle()}>
      <SettingSection>
        <SettingRow flow="no-wrap" label={this.translate('direction')}>
          <ButtonGroup>
            <Button title={this.translate('right')} type="secondary" icon size="sm" active={!vertical} onClick={() => this.onConfigChange('vertical', false)}>
              <Icon icon={rightArrowIcon}></Icon>
            </Button>
            <Button title={this.translate('down')} type="secondary" icon size="sm" active={vertical} onClick={() => this.onConfigChange('vertical', true)}>
              <Icon icon={downArrowIcon}></Icon>
            </Button>
          </ButtonGroup>
        </SettingRow>
      </SettingSection>
      <SettingSection title={this.translate('behavior')}>
        <SettingRow flow="wrap" label={this.translate('openWidget')}>
          <div className="setting-row-item">
            <Input id="only-open-one" style={{ cursor: 'pointer' }}
              name="only-open-one" onChange={e => this.onRadioChange(e, 'onlyOpenOne', true)} type="radio" checked={onlyOpenOne} />
            <Label style={{ cursor: 'pointer' }} for="only-open-one" className="ml-1">{this.translate('onlyOne')}</Label>
          </div>
          <div className="setting-row-item">
            <Input id="open-multiple" style={{ cursor: 'pointer' }}
              name="only-open-one" onChange={e => this.onRadioChange(e, 'onlyOpenOne', false)} type="radio" checked={!onlyOpenOne} />
            <Label style={{ cursor: 'pointer' }} for="open-multiple" className="ml-1">{this.translate('multiple')}</Label>
          </div>
        </SettingRow>

        {!onlyOpenOne && <SettingRow flow="wrap" label={this.translate('displayType')}>
          <div className="setting-row-item">
            <Input id="display-stack" style={{ cursor: 'pointer' }}
              name="display-type" onChange={e => this.onRadioChange(e, 'displayType', true)}
              type="radio" checked={displayType === DisplayType.Stack} />
            <Label style={{ cursor: 'pointer' }} for="display-stack" className="ml-1">{this.translate('stack')}</Label>
          </div>
          <div className="setting-row-item">
            <Input id="display-side-by-side" style={{ cursor: 'pointer' }}
              name="display-type" onChange={e => this.onRadioChange(e, 'displayType', false)}
              type="radio" checked={displayType === DisplayType.SideBySide} />
            <Label style={{ cursor: 'pointer' }} for="display-side-by-side" className="ml-1">{this.translate('sideBySide')}</Label>
          </div>
        </SettingRow>}

      </SettingSection>

      <SettingSection title={this.translate('appearance', true)}>
        <SettingRow flow="wrap" label={this.translate('iconStyle')}>
          <ChooseShape type={ShapeType.Circle} title={this.translate('circle')} className="mr-2" active={iconStyle === 'circle'} theme={theme}
            onClick={() => this.onConfigChange('iconStyle', 'circle')}></ChooseShape>
          <ChooseShape type={ShapeType.Rectangle} title={this.translate('rectangle')} active={iconStyle === 'rectangle'} theme={theme}
            onClick={() => this.onConfigChange('iconStyle', 'rectangle')}></ChooseShape>
        </SettingRow>
        <SettingRow label={this.translate('showIconLabel')}>
          <Switch checked={showLabel} onChange={(evt) => this.onConfigChange('showLabel', evt.target.checked)}></Switch>
        </SettingRow>
        <SettingRow flow="no-wrap" label={this.translate('iconSize')}>
          <Input type="select" value={iconSize} onChange={(e) => this.onConfigChange('iconSize', e.target.value)} className="w-50">
            <option value={IconTextSize.Small}>{this.translate('small')}</option>
            <option value={IconTextSize.Medium}>{this.translate('medium')}</option>
            <option value={IconTextSize.Large}>{this.translate('large')}</option>
          </Input>
        </SettingRow>
        <SettingRow flow="no-wrap" label={this.translate('iconInterval')}>
          <Input className="w-50" type="number" value={space} onAcceptValue={(value) => this.onConfigChange('space', +value)}></Input>
        </SettingRow>
      </SettingSection>
    </div>
  }
}