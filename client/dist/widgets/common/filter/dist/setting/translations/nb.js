export default {
  matchMsgSetAll: 'å_Display features that match all of the filters________________________ø',
  matchMsgSetAny: 'å_Display features that match any of the filters________________________ø',

  filtersName: 'å_Filter items_____________ø',
  filtersDesc: 'å_Add new filters and custom options___________________ø.',
  newFilter: 'å_New filter_____________________ø',
  options: 'å_Options_______________ø',
  setFilterItem: 'å_Set Filter item________________ø',

  // noTasksTip: 'å_No filters configured. Click \'${newFilter}\' to add a new one_________________________________ø.',
  zoomto: 'å_Zoom to the remaining features after the filter is applied______________________________ø',
  groupByLayer: 'å_Group filters by layer_______________________ø',
  // filterActions: 'å_Custom filter actions______________________ø',
  custom: 'å_Enable custom filters______________________ø',
  resetAll: 'å_Enable reset all_________________ø',
  turnOffAll: 'å_Enable turn off all____________________ø',


  filterName: 'å_Filter item____________ø',
  data: 'å_Data_________ø',
  icon: 'å_Icon_________ø',
  setIcon: 'å_Set icon_________________ø',
  defaultIcon: 'å_defaultIcon.svg________________ø',
  sqlExpr: 'å_SQL Expression_______________ø',
  sqlExprDesc: 'å_add SQL expressions to your filter___________________ø.',
  openFilterBuilder: 'å_Open SQL Expression Builder____________________________ø',
  setExprTips: 'å_Please set your expressions first__________________ø',

  // enableMapFilter: 'å_Remove the preset layer filter from the map_______________________ø.', //remove this option for exb
  autoApplyWhenWidgetOpen: 'å_Apply this filter when the widget is opened_______________________ø',
  collapseFilterExprs: 'å_Collapse the filter expressions (if any) when the widget is opened__________________________________ø'
}