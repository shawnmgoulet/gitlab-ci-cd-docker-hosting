import {ThemeVariables, css, SerializedStyles, polished} from 'jimu-core';

export function getStyleForFI(theme: ThemeVariables): SerializedStyles{
  return css`
    .filter-item-panel{
      .setting-header {
        padding: ${polished.rem(10)} ${polished.rem(16)} ${polished.rem(0)} ${polished.rem(16)}
      }

      .setting-title {
        font-size: ${polished.rem(16)};
      }

      .setting-container {
        height: calc(100% - ${polished.rem(50)});

        .title-desc{
          color: ${theme.colors.palette.dark[200]};
        }
      }
    }
  `
}

export function getStyleForWidget(theme: ThemeVariables): SerializedStyles{
  return css`
    .widget-setting-filter{
      .filter-items-desc{
        color: #c8cbcd;
      }
      .filter-item {
        cursor: pointer;
        line-height: 23px;
        background-color: ${theme.colors.secondary};
        .filter-item-name{
          word-break: break-all;
        }
      }

      .filter-item-active {
        border-left: 2px solid ${theme.colors.palette.primary[600]};
      }

      .options-container {
        border-top: 2px solid ${theme.colors.secondary};
        height: calc(100% - ${polished.rem(50)});
        label {
            /* temp width, not work????*/
            /* width: 200px;  */
        }
      }
    }
  `
}