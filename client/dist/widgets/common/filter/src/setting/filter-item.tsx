/** @jsx jsx */
import { React, jsx, ThemeVariables, Immutable, IntlShape, DataSourceManager, DataSourceComponent, IMUseDataSource, IMSqlExpression, IMIconResult, dataSourceUtils } from 'jimu-core';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { Icon, Input, Button, Switch } from 'jimu-ui';
import { DataSourceSelector, AllDataSourceTypes, SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
import { SqlExpressionBuilderPopup } from 'jimu-ui/sql-expression-builder'
import { filterItemConfig } from '../config';
import defaultMessages from './translations/default'
import { getStyleForFI } from './style';
import { IconPicker } from 'jimu-ui/resource-selector';

let IconClose = require('jimu-ui/lib/icons/close.svg');

interface Props {
  selectedDataSource: IMUseDataSource;
  intl: IntlShape;
  theme: ThemeVariables;
  onClose?: () => void;
  optionChange: (prop: string, value: string | boolean | IMIconResult) => void;
  dataSourceChange: (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs?: SelectedDataSourceJson) => void;
  onSqlExprBuilderChange: (sqlExprObj: IMSqlExpression) => void;
}

interface State {
  isSqlExprShow: boolean;
}

export default class FilterItem extends React.PureComponent<Props & filterItemConfig, State>{
  dsManager: DataSourceManager = window && window.jimuConfig && window.jimuConfig.isBuilder ? DataSourceManager.getInstance()
  : DataSourceManager.getInstance();
  supportedDsTypes = Immutable([AllDataSourceTypes.FeatureLayer, AllDataSourceTypes.FeatureQuery]);

  constructor(props) {
    super(props);

    this.state = {
      isSqlExprShow: false
    }
  }

  // get a ref to the dom node where we'll render the chart
  filterItem: HTMLDivElement;
  setFilterItemRef = el => {
    this.filterItem = el;
  }

  showSqlExprPopup = () => {
    this.setState({isSqlExprShow: true});
  }

  toggleSqlExprPopup = () => {
    this.setState({isSqlExprShow: !this.state.isSqlExprShow});
  }

  nameChange = (event) => {
    if(event && event.target && event.target.value){
      const value = event.target.value.trim();
      this.props.optionChange('name', value);
    }
  }

  i18nMessage = (id: string) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] });
  }

  render(){
    const isDisabled = (this.props.selectedDataSource && this.props.selectedDataSource.dataSourceId) ? false : true;
    return(
      <div className="w-100 h-100" css={getStyleForFI(this.props.theme)}>
        <div className="w-100 h-100 filter-item-panel">
          <div className="w-100 d-flex align-items-center justify-content-between setting-header setting-title pb-2">
            <div>{this.i18nMessage('setFilterItem')}</div>
            <div style={{cursor: 'pointer'}} onClick={this.props.onClose}><Icon icon={IconClose} size="12" className="text-dark"/></div>
          </div>
          <div className="setting-container">
            <SettingSection>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-start">
                  <label>{this.i18nMessage('data')}</label>
                </div>
              </SettingRow>
              <SettingRow>
                <DataSourceSelector types={this.supportedDsTypes}
                  selectedDataSourceIds={this.props.selectedDataSource ? Immutable([this.props.selectedDataSource.dataSourceId]) : Immutable([])}
                  mustUseDataSource={true} onSelect={this.props.dataSourceChange} closeDataSourceListOnSelect={true} />
              </SettingRow>
            </SettingSection>

            <SettingSection>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-start">
                  <label>{this.i18nMessage('name')}</label>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-center">
                  <Input type="text" className="w-100" value={this.props.name ? this.props.name : ''}
                  onChange={this.nameChange}
                  />
                </div>
              </SettingRow>
            </SettingSection>

            <SettingSection>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-start">
                  <label>{this.i18nMessage('icon')}</label>
                  <IconPicker buttonOptions={{ type: 'default', size: 'sm' }} hideRemove icon={this.props.icon as any}
                    onChange={(icon) => this.props.optionChange('icon', icon)}></IconPicker>
                  {this.props.icon ? <label>{this.props.icon.properties.filename}</label> : null}
                </div>
              </SettingRow>
            </SettingSection>

            <SettingSection>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-start">
                  <label>{this.i18nMessage('sqlExpr')}</label>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="title-desc">
                  {this.i18nMessage('sqlExprDesc')}
                </div>
              </SettingRow>
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-center">
                  {
                  <Button className="w-100 text-dark set-link-btn" type={isDisabled ? 'secondary' : 'primary'} disabled={isDisabled}
                    onClick={this.showSqlExprPopup} title={this.i18nMessage('openFilterBuilder')}>
                    {this.i18nMessage('openFilterBuilder')}
                  </Button>
                  }
                </div>
              </SettingRow>
              <SettingRow>
                <Input type="textarea" style={{ height: '80px' }} className="w-100" spellCheck={false} placeholder={this.i18nMessage('setExprTips')}
                  value={(this.props.sqlExprObj && this.props.sqlExprObj.displaySQL) ? this.props.sqlExprObj.displaySQL : ''}
                  onClick={e => e.target.select()} />
              </SettingRow>
            </SettingSection>

            <SettingSection className="border-0">
              <SettingRow>
                <div className="d-flex justify-content-between w-100 align-items-start">
                  <label>{this.i18nMessage('options')}</label>
                </div>
              </SettingRow>
              <SettingRow label={this.i18nMessage('autoApplyWhenWidgetOpen')}>
                <Switch checked={this.props.autoApplyWhenWidgetOpen} onChange={() => this.props.optionChange('autoApplyWhenWidgetOpen', !this.props.autoApplyWhenWidgetOpen)}/>
              </SettingRow>
              {/* <SettingRow>
                <div className="d-flex justify-content-between w-100">
                  <label>{this.i18nMessage('collapseFilterExprs')}</label>
                  <Switch checked={this.props.collapseFilterExprs} onChange={() => this.props.optionChange('collapseFilterExprs', !this.props.collapseFilterExprs)}/>
                </div>
              </SettingRow> */}
            </SettingSection>

            {!isDisabled &&
              <DataSourceComponent useDataSource={this.props.selectedDataSource}>{(ds) => {
                //check if timezone is changed
                if(this.props.sqlExprObj){
                  let sqlResult = dataSourceUtils.getArcGISSQL(this.props.sqlExprObj, ds);
                  if(sqlResult.displaySQL !== this.props.sqlExprObj.displaySQL){
                    let sqlExprObj = Object.assign({}, this.props.sqlExprObj, sqlResult);
                    this.props.onSqlExprBuilderChange(sqlExprObj);
                  }
                }
                return <SqlExpressionBuilderPopup selectedDs={ds}
                  isOpen={this.state.isSqlExprShow} toggle={this.toggleSqlExprPopup}
                  config = {this.props.sqlExprObj} onChange={this.props.onSqlExprBuilderChange} />
              }}</DataSourceComponent>
            }
          </div>
        </div>
      </div>
    )
  }
}
