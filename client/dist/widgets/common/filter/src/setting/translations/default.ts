export default {
  matchMsgSetAll: 'Display features that match all of the filters',
  matchMsgSetAny: 'Display features that match any of the filters',

  filtersName: 'Filter items',
  filtersDesc: 'Add new filters and customize options.',
  newFilter: 'New filter',
  options: 'Options',
  setFilterItem: 'Set filter item',

  // noTasksTip: 'No filters configured. Click \'${newFilter}\' to add a new one.',
  // zoomto: 'Zoom to the remaining features after the filter is applied',
  groupByLayer: 'Group filters by layer',
  // filterActions: 'Custom filter actions',
  custom: 'Enable custom filters',
  resetAll: 'Enable reset all',
  turnOffAll: 'Enable turn off all',

  data: 'Data',
  icon: 'Icon',
  setIcon: 'Set icon',
  defaultIcon: 'defaultIcon.svg',
  sqlExpr: 'SQL Expression',
  sqlExprDesc: 'Add SQL expressions to your filter.',
  openFilterBuilder: 'Open SQL Expression Builder',
  setExprTips: 'Please set your expressions first.',

  // enableMapFilter: 'Remove the preset layer filter from the map.', //remove this option for exb
  autoApplyWhenWidgetOpen: 'Apply this filter when the widget is opened',
  collapseFilterExprs: 'Collapse the filter expressions (if any) when the widget is opened'
}