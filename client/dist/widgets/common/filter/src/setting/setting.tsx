/** @jsx jsx */
import { jsx, classNames, Immutable, UseDataSource, ImmutableObject, IMUseDataSource, IMSqlExpression, IMIconResult, ClauseLogic } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { SettingSection, SettingRow, SidePopper } from 'jimu-ui/setting-components';
import FilterItem from './filter-item';
import { Button, Icon, ButtonGroup } from 'jimu-ui';
import { SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
import { WidgetConfig, filterItemConfig } from '../config';
import defaultMessages from './translations/default'
import { getStyleForWidget } from './style';
import { getJimuFieldNamesBySqlExpression } from 'jimu-ui/sql-expression-runtime';

let IconClose = require('jimu-ui/lib/icons/close.svg');
let IconAdd = require('jimu-ui/lib/icons/add-16.svg');
let FilterIcon = require('jimu-ui/lib/icons/filter-16.svg');

let DefaultIconResult: IMIconResult = Immutable({
  svg: FilterIcon,
  properties: {
    color: '#fff',
    filename: 'filter-16.svg',
    originalName: 'filter-16.svg',
    inlineSvg: true,
    path: (2) ['general', 'filter'],
    size: 16
  }
});

interface State {
  showFilterItemPanel: boolean;
  refreshFilterItemPanel: boolean;
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<WidgetConfig> & State, any>{
  index: number;
  dsHash: {[index: number]: ImmutableObject<UseDataSource>};

  modalStyle: any = {position: 'absolute', top: '0', bottom: '0', right: '260px', left: 'auto', width: '259px',
    height: 'auto', zIndex: '3', paddingBottom: '1px'};

  constructor(props){
    super(props);
    this.index = 0;
    this.updateDsHash(this.props.config.filterItems ? this.props.config.filterItems as unknown as filterItemConfig[] : []);

    this.modalStyle.borderRight = `1px solid ${this.props.theme.colors.white}`;
    this.modalStyle.borderBottom = `1px solid ${this.props.theme.colors.white}`;

    this.state = {
      showFilterItemPanel: false,
      refreshFilterItemPanel: false
    };
  }
  // optionsContainerStyle : any = {position: 'absolute', bottom: '0', height: 'auto'};


  /************ For widget ***********/
  i18nMessage = (id: string) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] });
  }

  updateDsHash = (filterItems: filterItemConfig[]) => {
    this.dsHash = {};
    let index = 0;
    filterItems.map(item => {
      this.dsHash[index] = item.dataSource;
      index ++;
    });
  }

  toggleTimeout: NodeJS.Timer;
  onShowFilterItemPanel = (index?: number) => {
    if(index === this.index){
      this.setState({
        showFilterItemPanel: !this.state.showFilterItemPanel
      });
    }else{
      this.setState({
        showFilterItemPanel: true
      });
      this.setState({
        refreshFilterItemPanel: !this.state.refreshFilterItemPanel
      });
      this.index = index;
    }
  }

  onCloseFilterItemPanel = () => {
    this.setState({
      showFilterItemPanel: false
    });
    this.index = 0;
  }

  updateConfigForOptions = (prop: string, value: boolean | ClauseLogic) => {
    let config = {
      id: this.props.id,
      config: this.props.config.set(prop, value)
    }
    this.props.onSettingChange(config);
  }


  /************ For Filter Item config ***********/
  removeFilterItem = (index: number) => {
    if(this.index === index){
      this.onCloseFilterItemPanel();
    }
    //del current filter item
    let _fis = this.props.config.filterItems.asMutable({deep: true});
    _fis.splice(index, 1);
    let fis = this.props.config.set('filterItems', _fis);

    //update dsHash
    delete this.dsHash[index];
    this.updateDsHash(_fis);

    let config = {
      id: this.props.id,
      config: fis,
      useDataSources: this.getUseDataSourcesByDsHash()
    }
    this.props.onSettingChange(config);

    if(this.index > index){
      this.index--;
    }
  }

  optionChangeForFI = (prop: string, value: string | boolean | IMIconResult) => {
    let currentFI = this.props.config.filterItems[this.index];
    if(currentFI){
      let fItems = this.props.config.filterItems.asMutable({deep: true});
      let fItem = Immutable(fItems[this.index]).set(prop, value);
      fItems.splice(this.index, 1, fItem.asMutable({deep: true}));

      let config = {
        id: this.props.id,
        config: this.props.config.set('filterItems', fItems)
      }
      this.props.onSettingChange(config);
    }
  }

  //save currentSelectedDs to array;
  dataSourceChangeForFI = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs?: SelectedDataSourceJson) => {
    let _currentDs: UseDataSource = {
      dataSourceId: currentSelectedDs.dataSourceJson.id,
      rootDataSourceId: currentSelectedDs.rootDataSourceId
    }

    let filterItem: filterItemConfig = {
      icon: DefaultIconResult,
      name: currentSelectedDs.dataSourceJson.label,
      dataSource: Immutable(_currentDs),
      sqlExprObj: null,
      autoApplyWhenWidgetOpen: false,
      collapseFilterExprs: false
    };

    // let config;
    let currentFI = this.props.config.filterItems[this.index];
    let filterItems;
    if(currentFI){ //update FI, reset other opts for current FI
      let _fis = this.props.config.filterItems.asMutable({deep: true});
      _fis.splice(this.index, 1, filterItem);
      filterItems = Immutable(_fis);
    }else{ //add new FI to FIs
      filterItems = this.props.config.filterItems.concat(Immutable([Immutable(filterItem)]));
    }
    //update dsHash
    this.dsHash[this.index] = Immutable(_currentDs);
    this.updateDsHash(filterItems);

    let config = {
      id: this.props.id,
      config: this.props.config.set('filterItems', filterItems),
      useDataSources: this.getUseDataSourcesByDsHash()
    };
    this.props.onSettingChange(config);
  }

  sqlExprBuilderChange = (sqlExprObj: IMSqlExpression) => {
    //get fields
    let fields = getJimuFieldNamesBySqlExpression(sqlExprObj);
    let updatedDs: UseDataSource = {
      dataSourceId: this.dsHash[this.index].dataSourceId,
      rootDataSourceId: this.dsHash[this.index].rootDataSourceId,
      fields: fields
    };

    //update sqlExprObj, sqlExprObj and ds
    let fItems = this.props.config.filterItems.asMutable({deep: true});
    let fItem = Object.assign({}, fItems[this.index], {sqlExprObj: sqlExprObj, dataSource: updatedDs});
    fItems.splice(this.index, 1, fItem);

    //update dsHash
    this.dsHash[this.index] = Immutable(updatedDs);
    this.updateDsHash(fItems);

    let config = {
      id: this.props.id,
      config: this.props.config.set('filterItems', Immutable(fItems)),
      useDataSources: this.getUseDataSourcesByDsHash()
    };
    this.props.onSettingChange(config);
  }

  getUniqueValues = (array1: any[] = [], array2: any[] = []): any[] => {
    let array = array1.concat(array2);
    var res = array.filter(function(item, index, array){
      return array.indexOf(item) === index;
    })
    return res;
  }

  getUseDataSourcesByDsHash = (): UseDataSource[] => {
    let dsHash: any = {};
    Object.keys(this.dsHash).map((index) => {
      let dsId = this.dsHash[index].dataSourceId;
      let ds: IMUseDataSource;
      if(!dsHash[dsId]){
        ds = this.dsHash[index];
      }else{
        ds = Immutable({
          dataSourceId: this.dsHash[index].dataSourceId,
          rootDataSourceId: this.dsHash[index].rootDataSourceId,
          fields: this.getUniqueValues(dsHash[dsId].fields, this.dsHash[index].fields as unknown as any[])
        });
      }
      dsHash[dsId] = ds;
    });

    // get new array from hash
    let dsArray = [];
    Object.keys(dsHash).map(dsId => {
      dsArray.push(dsHash[dsId]);
    });
    return dsArray;
  }

  getDataSourceById = (useDataSources: UseDataSource[], dataSourceId: string): ImmutableObject<UseDataSource> => {
    let dsList = useDataSources.filter(ds => ds.dataSourceId === dataSourceId);
    return Immutable(dsList[0]);
  }

  changeAndOR = (logicalOperator: ClauseLogic) => {
    this.updateConfigForOptions('logicalOperator', logicalOperator);
  }

  render(){
    const config = this.props.config;
    return <div css={getStyleForWidget(this.props.theme)}>
      <div className="jimu-widget-setting widget-setting-filter">
        <SettingSection className="border-0">
          <SettingRow>
            <div className="d-flex justify-content-between w-100 align-items-start">
              <label>{this.i18nMessage('filtersName')}</label>
            </div>
          </SettingRow>
          <SettingRow>
            <div className="d-flex justify-content-between w-100 align-items-start filter-items-desc">
              <label>{this.i18nMessage('filtersDesc')}</label>
            </div>
          </SettingRow>
          <SettingRow>
            <Button className="w-100 text-dark set-link-btn" type="primary" onClick={() => {this.onShowFilterItemPanel(config.filterItems.length)}}>
              <Icon icon={IconAdd} className="mr-1" size={14}/>
              {this.i18nMessage('newFilter')}
            </Button>
          </SettingRow>
        </SettingSection>

        <SettingSection className="border-0 pt-0">
          {
            config.filterItems.length > 1 ?
            <SettingRow>
              <ButtonGroup>
                <Button onClick={() => {this.changeAndOR(ClauseLogic.And)}} className="btn-secondary" size="sm"
                  type={this.props.config.logicalOperator === ClauseLogic.And ? 'primary' : 'secondary'} >
                  {this.i18nMessage('and')}
                </Button>
                <Button onClick={() => {this.changeAndOR(ClauseLogic.Or)}} className="ml-0 btn-secondary" size="sm"
                  type={this.props.config.logicalOperator === ClauseLogic.Or ? 'primary' : 'secondary'} >
                  {this.i18nMessage('or')}
                </Button>
              </ButtonGroup>
            </SettingRow> : null
          }
          <div className="filter-items-container mt-2">
            {config.filterItems.asMutable().map((item: ImmutableObject<filterItemConfig>, index: number) => {
              return (
                <div key={index} className={classNames('d-flex pt-2 pb-2 mb-1 filter-item',
                  (this.state.showFilterItemPanel && this.index === index) ? 'filter-item-active' : '')} >
                  {
                    item.icon ?
                    <Button size="sm" icon type="tertiary" disabled={true}>
                      <Icon icon={item.icon.svg} color={item.icon.properties.color} size={item.icon.properties.size} />
                    </Button> : null
                  }
                  <div className="filter-item-name flex-grow-1" onClick={() => {this.onShowFilterItemPanel(index)}} >{item.name}</div>
                  <Button size="sm" icon onClick={() => this.removeFilterItem(index)}>
                    <Icon icon={IconClose} size={12}></Icon>
                  </Button>
                </div>
              )
            })}
            {(config.filterItems.length === this.index && this.state.showFilterItemPanel) ?
              <div className="d-flex pt-2 pb-2 mb-1 filter-item filter-item-active" >
                <Button size="sm" icon type="tertiary" disabled={true}>
                  <Icon icon={DefaultIconResult.svg} color={DefaultIconResult.properties.color} size={DefaultIconResult.properties.size} />
                </Button>
                <div className="filter-item-name flex-grow-1" >......</div>
              </div> : null
            }
          </div>
        </SettingSection>
        {
          <SidePopper isOpen={this.state.showFilterItemPanel} position="right">
            <FilterItem {...config.filterItems[this.index]} intl={this.props.intl} theme={this.props.theme}
              selectedDataSource={this.dsHash[this.index] ? this.dsHash[this.index] : null}
              dataSourceChange={this.dataSourceChangeForFI} optionChange={this.optionChangeForFI}
              onSqlExprBuilderChange={this.sqlExprBuilderChange} onClose={this.onCloseFilterItemPanel} />
          </SidePopper>
        }
        {/*
        <div style={this.optionsContainerStyle}>
          <SettingSection className="options-container">
            <SettingRow>
              <div className="d-flex w-100 align-items-start">
                <label>{this.i18nMessage('options')}</label>
              </div>
            </SettingRow>
            <SettingRow>
              <div className="d-flex justify-content-between w-100">
                <label>{this.i18nMessage('groupByLayer')}</label>
                <Switch checked={config.groupByLayer} onChange={() => this.updateConfigForOptions('groupByLayer', !config.groupByLayer)}/>
              </div>
            </SettingRow>
            <SettingRow>
              <div className="d-flex justify-content-between w-100">
                <label>{this.i18nMessage('custom')}</label>
                <Switch checked={config.custom} onChange={() => this.updateConfigForOptions('custom', !config.custom)}/>
              </div>
            </SettingRow>

            <SettingRow>
              <div className="d-flex justify-content-between w-100">
                <label>{this.i18nMessage('resetAll')}</label>
                <Switch checked={config.resetAll} onChange={() => this.updateConfigForOptions('resetAll', !config.resetAll)}/>
              </div>
            </SettingRow>

            <SettingRow>
              <div className="d-flex justify-content-between w-100">
                <label>{this.i18nMessage('turnOffAll')}</label>
                <Switch checked={config.turnOffAll} onChange={() => this.updateConfigForOptions('turnOffAll', !config.turnOffAll)}/>
              </div>
            </SettingRow>
          </SettingSection>
        </div>*/}
      </div>
    </div>
  }
}