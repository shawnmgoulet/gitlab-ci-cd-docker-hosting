import { ImmutableObject, ImmutableArray, IMSqlExpression, IMUseDataSource, IMIconResult, ClauseLogic } from 'jimu-core';

export interface filterItemConfig {
  icon?: IMIconResult;
  name: string;
  dataSource: IMUseDataSource,
  sqlExprObj?: IMSqlExpression;
  exprInvert?: boolean;
  autoApplyWhenWidgetOpen?: boolean;
  collapseFilterExprs?: boolean;
}

export interface filterConfig {
  id: string;
  filterItems?: ImmutableArray<filterItemConfig>
  logicalOperator: ClauseLogic;
  groupByLayer: boolean;
  custom: boolean;
  resetAll: boolean;
  turnOffAll: boolean;
}

export type WidgetConfig = ImmutableObject<filterConfig>;