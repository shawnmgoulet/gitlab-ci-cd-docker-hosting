/** @jsx jsx */
import { React, jsx, DataSource, IMSqlExpression } from 'jimu-core';
import { filterItemConfig } from '../config';
import { Switch, Icon } from 'jimu-ui';
import { SqlExpressionRuntime } from 'jimu-ui/sql-expression-runtime';

interface Props {
  id: number;
  config: filterItemConfig;
  selectedDs: DataSource;
  onChange: (id: number, dataSource: DataSource, sqlExprObj: IMSqlExpression, applied: boolean) => void;
}

interface State {
  rerender: boolean;
}

export default class FilterItem extends React.PureComponent<Props, State>{
  sqlExprObj: IMSqlExpression;
  applied: boolean;

  constructor(props){
    super(props);
    this.applied = this.props.config.autoApplyWhenWidgetOpen;
    this.state = {
      rerender: false
    }
    this.sqlExprObj = this.props.config.sqlExprObj;
  }

  componentDidUpdate(prevProps: Props, prevState: State){
    if(prevProps.config !== this.props.config){
      this.sqlExprObj = this.props.config.sqlExprObj;
      this.applied = this.props.config.autoApplyWhenWidgetOpen;
      this.setState({
        rerender: !this.state.rerender // trigger to re-render
      });
    }
  }

  onApplyChange = () => {
    this.applied = !this.applied;
    this.props.onChange(this.props.id, this.props.selectedDs, this.sqlExprObj, this.applied);
  }

  onSqlExpressionChange = (sqlExprObj: IMSqlExpression) => {
    this.sqlExprObj = sqlExprObj;
    this.props.onChange(this.props.id, this.props.selectedDs, sqlExprObj, this.applied);
  }

  render(){
    const { icon, name } = this.props.config;
    return (
      <div className="filter-item pt-2 pl-3 pr-3">
        {
          this.sqlExprObj &&
          <div className="filter-item-inline pt-2 pb-2">
            <div className="d-flex justify-content-between w-100 p-2">
              {
                icon && <div className="d-flex align-items-center">
                  <Icon icon={icon.svg} color={icon.properties.color} size={icon.properties.size} />
                </div>
              }
              <div className="filter-item-name flex-grow-1 m-1">{name}</div>
              <div className="d-flex align-items-center">
                <Switch checked={this.applied} onChange={this.onApplyChange}/>
              </div>
            </div>
            <div className="w-100 pl-3 pr-3">
              <SqlExpressionRuntime selectedDs={this.props.selectedDs} config={this.sqlExprObj} onChange={this.onSqlExpressionChange} />
            </div>
          </div>
        }
      </div>
    );
  }
}
