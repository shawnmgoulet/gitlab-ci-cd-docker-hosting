import {ThemeVariables, css, SerializedStyles, polished} from 'jimu-core';

export function getStyles(theme: ThemeVariables): SerializedStyles{
  return css`
    .filter-item {
      .filter-item-inline {
        background-color: ${theme.colors.palette.light[300]};
        .filter-item-name{
          font-size: ${polished.rem(13)};
          color: ${theme.colors.black};
          word-break: break-all;
        }
      }
    }
    .filter-item:first-child{
      padding-top: ${polished.rem(16)}!important;
    }
    .filter-item:last-child{
      padding-bottom: ${polished.rem(16)}!important;
    }
  `
}