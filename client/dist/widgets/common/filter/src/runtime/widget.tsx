/** @jsx jsx */
import {BaseWidget, AllWidgetProps, jsx, DataSourceComponent, DataSource, QueriableDataSource, WidgetDataSourceQueryManager,
  dataSourceUtils, ImmutableArray, UseDataSource, ImmutableObject, Immutable, IMSqlExpression } from 'jimu-core';
import {WidgetConfig, filterItemConfig, filterConfig} from '../config';
import FilterItem from './filter-item';
import { WidgetPlaceholder } from 'jimu-ui';
import defaultMessages from './translations/default';
import { getStyles } from './style';

let FilterIcon = require('jimu-ui/lib/icons/filter-2.svg');

interface State{
  rerender: boolean;
}

export default class Widget extends BaseWidget<AllWidgetProps<WidgetConfig>, any>{
  index: number;
  dataSources: {[dsId: string]: DataSource};
  filterItems: ImmutableArray<filterItemConfig>;

  dsQueryManager: WidgetDataSourceQueryManager = WidgetDataSourceQueryManager.getInstance();

  constructor(props){
    super(props);
    this.index = 0;
    this.state = {
      rerender: false
    }
    this.filterItems = this.props.config.filterItems;
  }

  componentDidUpdate(prevProps: AllWidgetProps<filterConfig>, prevState: State){
    if(prevProps.config !== this.props.config){ //update by setting
      this.filterItems = this.props.config.filterItems;
      this.setState({
        rerender: !this.state.rerender
      });
    }else if(prevState.rerender !== this.state.rerender){ //refresh all sqls for different dataSources when setting's changed
      Object.keys(this.dataSources).map(dsId => {
        let sql = this.getQuerySqlFromDs(this.dataSources[dsId]);
        if(sql !== ''){
          this.setSqlToDs(this.dataSources[dsId], sql);
        }
      });
    }
  }

  onFilterItemChange = (index: number, dataSource: DataSource, sqlExprObj: IMSqlExpression, applied: boolean) => {
    //update fitlerItem
    let fItems = this.filterItems.asMutable({deep: true});
    let needQuery = (applied === false && fItems[index].autoApplyWhenWidgetOpen === false) ? false : true;
    let fItem = Object.assign({}, fItems[index], {sqlExprObj: sqlExprObj, autoApplyWhenWidgetOpen: applied});
    fItems.splice(index, 1, fItem);
    this.filterItems = Immutable(fItems);

    if(needQuery){
      let sql = this.getQuerySqlFromDs(dataSource);
      this.setSqlToDs(dataSource, sql);
    }
  }

  setSqlToDs = (dataSource: DataSource, sql: string) => {
    this.dsQueryManager.setWidgetQueryOption(this.props.id, dataSource.id, {where: sql});
    let mergedDsOpts = this.dsQueryManager.getMergedDataSourceQueryOptions(dataSource.id);
    (dataSource as QueriableDataSource).load(mergedDsOpts);
  }

  getQuerySqlFromDs = (dataSource: DataSource) => {
    let sqls = []; //get sqls for current ds
    this.filterItems.map(item => {
      if(item.dataSource.dataSourceId === dataSource.id && item.autoApplyWhenWidgetOpen){
        sqls.push(dataSourceUtils.getArcGISSQL(item.sqlExprObj, dataSource).sql);
      }
    });
    let sqlString = '(1=1)';
    if(sqls.length){
      sqlString = sqls.length === 1 ? sqls[0] : '(' + sqls.join(') ' + this.props.config.logicalOperator + ' (') + ')';
    }
    return sqlString;
  }

  getDataSourceById = (dataSourceId: string): ImmutableObject<UseDataSource> => {
    let dsList = this.props.useDataSources.asMutable({deep: true}).filter(ds => ds.dataSourceId === dataSourceId);
    return Immutable(dsList[0]);
  }

  render(){
    this.dataSources = {};
    if(this.filterItems.length === 0){
      return <WidgetPlaceholder icon={FilterIcon} widgetId={this.props.id}
      message={this.props.intl.formatMessage({ id: '_widgetLabel', defaultMessage: defaultMessages._widgetLabel })} />
    }

    return (
      <div className="jimu-widget widget-filter overflow-auto" css={getStyles(this.props.theme)}>
        {(this.filterItems as unknown as filterItemConfig[]).map((item, index) => {
          return <DataSourceComponent key={index} useDataSource={this.getDataSourceById(item.dataSource.dataSourceId)}>{(ds) => {
            this.dataSources[ds.id] = ds;
            return <FilterItem id={index} selectedDs={ds} config={item} onChange={this.onFilterItemChange} />
          }}</DataSourceComponent>
        })}
      </div>
    );
  }
}
