/** @jsx jsx */
import { React, jsx, ImmutableArray, IMUseDataSource, appActions, getAppStore, IMState, Immutable, ReactRedux, IntlShape, injectIntl, css, ThemeVariables } from 'jimu-core';
import defaultMessages from '../translations/default';
import { Editor as Quill, Delta, Plugins, RichTextEditor, Formats, Sources } from 'jimu-ui/rich-text-editor';
import * as utils from '../utils';
import { mixinFormats } from '../../utils';
import { IMConfig } from '../../config';

const imageMatcher = (node, delta: Delta) => {
  return { ops: [], length: 0 };
}

const modules = {
  toolbar: false,
  autoformat: {
    link: {
      trigger: /[\s]/,
      find: /https?:\/\/[\S]+|(www\.[\S]+)/gi,
      transform: function (value, noProtocol) {
        return noProtocol ? 'http://' + value : value;
      },
      format: 'link'
    }
  },
  clipboard: {
    matchers: [
      ['img', imageMatcher]
    ]
  }
};

interface OwnProps {
  widgetId: string;
  enabled?: boolean;
  useDataSources: ImmutableArray<IMUseDataSource>;
  onQuillCreated?: (quill: Quill) => void;
  onQuillUnMount?: () => void;
  text: string;
  placeholder: string;
  persistPartialConfig: (config: Partial<IMConfig>) => void;
}

interface ExtraProps2 {
  intl: IntlShape;
}

interface ExtraProps {
  theme: ThemeVariables;
  showExpression?: boolean;
}

interface State {
  html?: string,
  placeholder?: string,
}

export class _Editor extends React.PureComponent<OwnProps & ExtraProps & ExtraProps2, State> {
  quill: Quill;
  constructor(props) {
    super(props);
    this.state = {
      html: ''
    }
    this.onChange = this.onChange.bind(this);
    this.handleQuillCreate = this.handleQuillCreate.bind(this);
    this.mixinFormats = this.mixinFormats.bind(this);
    this.isEditingPlaceholder = this.isEditingPlaceholder.bind(this);
  }

  componentDidMount() {
    const { text, placeholder } = this.props;
    this.setState({ html: text, placeholder });
  }

  translate = (id: string) => {
    const { intl } = this.props;
    return intl ? intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : '';
  }

  mixinFormats(formats: Formats = {}): Formats {
    const { theme } = this.props;
    formats = mixinFormats(theme, formats);
    return formats
  }

  getPligins = (): Plugins => {
    const { useDataSources, showExpression } = this.props;
    return {
      bubble: {
        zIndex: 51,
        dataSourceIds: utils.getDataSourceIds(useDataSources),
        mixFormats: this.mixinFormats,
        source: 'user'
      },
      expression: {
        open: showExpression,
        header: {
          show: true,
          text: this.translate('dynamicContent'),
          onClose: () => this.onExpressionStateChange(false)
        },
        zIndex: 51,
        dataSourceIds: utils.getDataSourceIds(useDataSources),
        source: 'user'
      }
    }
  }

  getStyle = () => {
    const { useDataSources } = this.props;
    const dsids = utils.getInvalidDataSourceIds(this.state.html, useDataSources);
    let expressionStyles;
    if (dsids) {
      expressionStyles = dsids.map(dsid => {
        return css`
          exp[data-dsid*="${dsid}"] {
            opacity: 0.5;
            background: red;
            outline: 1px solid white;
          }
        `;
      });
    }
    return css`${expressionStyles}`;
  }

  componentDidUpdate(prevProps: OwnProps & ExtraProps) {
    let { enabled } = this.props;
    if ((!enabled && (enabled != prevProps.enabled))) {
      const text = this.persistPartialConfig();
      if (this.isPlaceholderInEditor(text, this.state.placeholder)) {
        const quill = this.quill;
        quill.setContents(quill.clipboard.convert(this.state.placeholder), 'silent');
      }
    }
    /**
     * When inline editing is `first activated`, `text` is empty and `placeholder` are set,
     * We clear the text and add a zero width space `\uFEFF` to inherit the style of placeholder
     */
    if (enabled && !prevProps.enabled) {
      if (this.isPlaceholderInEditor(this.props.text, this.state.placeholder)) {
        const quill = this.quill;
        let plaintext = quill.getText() || '';
        plaintext = plaintext.trim();
        const placeholder = this.state.placeholder.replace(plaintext, '\uFEFF');
        quill.setContents(quill.clipboard.convert(placeholder), 'silent');
        quill.focus();
      }
    }
  }

  componentWillUnmount() {
    if (this.props.onQuillUnMount) {
      this.props.onQuillUnMount();
    }
    this.persistPartialConfig();
    this.quill = null;
  }

  persistPartialConfig = (): string => {
    if (this.state.html !== this.props.text || this.state.placeholder !== this.props.placeholder) {
      let config = Immutable({}) as IMConfig;
      config = config.set('text', this.state.html).set('placeholder', this.state.placeholder);
      this.props.persistPartialConfig(config);
      return this.state.html;
    }
    return this.props.text;
  }

  onExpressionStateChange = (showExpression: boolean) => {
    const { widgetId } = this.props;
    getAppStore().dispatch(appActions.widgetStatePropChange(widgetId, 'showExpression', showExpression));
  }

  isEditingPlaceholder() {
    const { enabled, text } = this.props;
    return !enabled && this.isBlank(text);
  }

  onChange(html: string, _, source: Sources) {
    if (source === 'silent') return;
    if (this.isEditingPlaceholder()) {
      this.setState({ placeholder: html });
    } else {
      this.setState({ html });
    }
  }

  isBlank(text: string) {
    /**
     * In quill, we treat both '<p></p>', '<p>\uFEFF</p>' and '<p><br></p>' as blanks
     */
    return !text || text === '<p></p>' || text === '<p>\uFEFF</p>' || text === '<p><br></p>';
  }

  isPlaceholderInEditor(text: string, placeholder: string) {
    return this.isBlank(text) && placeholder;
  }

  handleQuillCreate(quill: Quill) {
    let { text } = this.props;
    const placeholder = this.state.placeholder || this.props.placeholder;
    /**
     * When we first initialized quill, `text` is empty and `placeholder` are set
     * We paste the placeholder into the editor
     */
    if (this.isPlaceholderInEditor(text, placeholder)) {
      quill.setContents(quill.clipboard.convert(placeholder), 'silent');
    }
    this.quill = quill;
    this.props.onQuillCreated(quill);
  }

  render() {
    const { text, enabled } = this.props;
    return <RichTextEditor
      css={this.getStyle()}
      preserveWhitespace={true}
      onCreate={this.handleQuillCreate}
      modules={modules}
      enabled={enabled}
      plugins={this.getPligins()}
      onChange={this.onChange}
      defaultValue={text}
    />;
  }
}

const mapStateToProps = (state: IMState, ownProps: OwnProps) => {
  const widgetState = state.widgetsState[ownProps.widgetId] || Immutable({});
  const showExpression = !!widgetState.showExpression;
  return {
    theme: state.theme,
    showExpression
  }
}

export const Editor = ReactRedux.connect<ExtraProps, {}, OwnProps>(mapStateToProps)(injectIntl(_Editor));