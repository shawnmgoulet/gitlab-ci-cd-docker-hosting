import {
  React, RepeatedDataSource, DataRecord, Immutable, IMDataSourceInfo, ImmutableArray, IMUseDataSource,
  ImmutableObject, DataSourceManager, ReactRedux, IMUrlParameters, IMState, ExpressionResolverComponent, IMExpressionMap
} from 'jimu-core';
import * as utils from '../utils';

export type DataSourcesInfo = ImmutableObject<{ [dsId: string]: IMDataSourceInfo }>;

export type Records = { [dataSourceId: string]: DataRecord };

interface RenderFunction {
  (text: string): React.ReactNode
}

interface Props {
  repeatedDataSource: RepeatedDataSource,
  useDataSources: ImmutableArray<IMUseDataSource>;
  text: string;
  children?: RenderFunction | React.ReactNode;
}

interface ExtraProps {
  queryObject: IMUrlParameters;
}


interface State {
  expression?: IMExpressionMap;
  records?: Records;
  resolvedValues: { [expressionId: string]: string };
}

export class _TextResolver extends React.PureComponent<Props & ExtraProps, State>{
  static displayName = '_TextResolver';
  dsm: DataSourceManager;

  constructor(props) {
    super(props);
    this.state = {
      expression: Immutable({}),
      records: {},
      resolvedValues: {}
    };
    this.dsm = DataSourceManager.getInstance();
    this.onResolved = this.onResolved.bind(this);
  }

  componentDidMount() {
    const expression = utils.getAllExpressions(this.props.text);
    const repeatedDataSource = this.props.repeatedDataSource as RepeatedDataSource;
    const records = this.getRecords(repeatedDataSource);
    this.setState({ expression, records });
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.text !== prevProps.text) {
      const expression = utils.getAllExpressions(this.props.text);
      this.setState({ expression });
    }
    if (this.props.repeatedDataSource !== prevProps.repeatedDataSource) {
      const repeatedDataSource = this.props.repeatedDataSource as RepeatedDataSource;
      const records = this.getRecords(repeatedDataSource);
      this.setState({ records });
    }
  }

  getRecords = (repeatedDataSource: RepeatedDataSource) => {
    const record = repeatedDataSource && repeatedDataSource.record;
    const dsid = repeatedDataSource && repeatedDataSource.dataSourceId;
    return { [dsid]: record };
  }

  resolveTextVariables = (): string => {
    let { text, queryObject } = this.props;
    const { resolvedValues } = this.state;

    if (!text) {
      return '';
    }

    text = utils.replaceHtmlExpression(text, resolvedValues);
    text = utils.replaceHtmlLinkHref(text, queryObject, resolvedValues);
    return text;
  }

  onResolved(resolvedValues: { [expressionId: string]: string }) {
    this.setState({ resolvedValues });
  }

  render() {
    const { useDataSources = Immutable([]), children } = this.props;
    const text = this.resolveTextVariables()
    return <React.Fragment>
      <ExpressionResolverComponent
        useDataSources={useDataSources}
        expression={this.state.expression}
        records={this.state.records}
        onSuccess={this.onResolved}
      ></ExpressionResolverComponent>
      {typeof children === 'function' ? (children as RenderFunction)(text) : children}
    </React.Fragment>
  }
}

const mapStateToProps = (state: IMState) => {
  return {
    queryObject: state.queryObject
  }
}

export default ReactRedux.connect<ExtraProps, {}, Props>(mapStateToProps)(_TextResolver);