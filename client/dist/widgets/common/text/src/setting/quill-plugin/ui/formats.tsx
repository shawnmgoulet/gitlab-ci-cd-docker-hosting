/** @jsx jsx */
import { React, jsx, css, classNames, injectIntl, IntlShape, ImmutableArray } from 'jimu-core';
import { FontFamily, Indent, Size, LinkNode, AlignValue, ListValue, FormatType, Formats, TextFormatTypes, QuillLinkValue } from 'jimu-ui/rich-text-editor';
import { Icon, Button, Input, ButtonGroup, defaultMessages } from 'jimu-ui';
import { SettingRow } from 'jimu-ui/setting-components';
import { TextColorPicker } from 'jimu-ui/color-picker';

const textIcon = require('jimu-ui/lib/icons/uppercase.svg');
const fillIcon = require('jimu-ui/lib/icons/fill.svg');
const boldIcon = require('jimu-ui/lib/icons/bold.svg');
const italicIcon = require('jimu-ui/lib/icons/italic.svg');
const underlineIcon = require('jimu-ui/lib/icons/underscore.svg');
const strikeIcon = require('jimu-ui/lib/icons/strike-through.svg');
const leftIcon = require('jimu-ui/lib/icons/align-left.svg');
const centerIcon = require('jimu-ui/lib/icons/align-middle.svg');
const rightIcon = require('jimu-ui/lib/icons/align-right.svg');
const justifyIcon = require('jimu-ui/lib/icons/align-justify.svg');
const bulletIcon = require('jimu-ui/lib/icons/text-dots.svg');
const orderedIcon = require('jimu-ui/lib/icons/text-123.svg');
const linkIcon = require('jimu-ui/lib/icons/link.svg');

const DEFAULTLETTERSIZE = '0px';
const DEFAULLINESTACE = 1.5;

export interface FormatsNodePorps {
  className?: string;
  style?: any;
  dataSourceIds?: ImmutableArray<string>;
  formats?: Formats;
  onChange?: (key: TextFormatTypes, value: any, type: FormatType, id?: string) => void;
  disableLink?: boolean;
  disableIndent?: boolean;
}

interface ExtraProps {
  intl: IntlShape
}

interface State {
  openLink: boolean;
}

export class _FormatsNode extends React.PureComponent<FormatsNodePorps & ExtraProps, State> {
  static defaultProps: Partial<FormatsNodePorps & ExtraProps> = {
    formats: {},
    onChange: () => { }
  }

  constructor(props) {
    super(props);
    this.state = {
      openLink: false
    }
  }

  getStyle = () => {
    return css`
      > * {
        user-select: none;
      }
    `;
  }

  translate = (id: string) => {
    return this.props.intl ? this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  handleListChange = (value: ListValue) => {
    const { formats } = this.props;
    const list = formats.list === value ? false : value;
    this.props.onChange(TextFormatTypes.LIST, list, FormatType.BLOCK)
  }

  handleLinkChange = (key: TextFormatTypes, value: QuillLinkValue, type: FormatType) => {
    this.props.onChange(key, value, type);
    this.toggleLinkOpen();
  }

  toggleLinkOpen = () => {
    this.setState({ openLink: !this.state.openLink })
  }

  render() {
    const { className, style, formats, dataSourceIds, onChange, disableLink, disableIndent } = this.props;
    return <div css={this.getStyle()} style={style} className={classNames(className, 'format-panel')}>
      <SettingRow>
        <div className="d-flex align-items-center justify-content-between w-100">
          <FontFamily style={{ width: '60%' }} font={formats.font} onChange={v => onChange(TextFormatTypes.FONT, v, FormatType.INLINE)}></FontFamily>
          <Size style={{ width: '35%' }} value={formats.size} onChange={v => onChange(TextFormatTypes.SIZE, v, FormatType.INLINE)}></Size>
        </div>
      </SettingRow>

      <SettingRow>
        <div className="d-flex align-items-center justify-content-between w-100">
          <ButtonGroup style={{ width: '45%' }}>
            <Button title={this.translate('bold')} active={!!formats[TextFormatTypes.BOLD]} type="secondary" icon size="sm"
              onClick={() => onChange(TextFormatTypes.BOLD, !formats[TextFormatTypes.BOLD], FormatType.INLINE)}>
              <Icon size={14} icon={boldIcon}></Icon>
            </Button>
            <Button title={this.translate('italic')} active={!!formats[TextFormatTypes.ITALIC]} type="secondary" icon size="sm"
              onClick={() => onChange(TextFormatTypes.ITALIC, !formats[TextFormatTypes.ITALIC], FormatType.INLINE)}>
              <Icon size={14} icon={italicIcon}></Icon>
            </Button>
            <Button title={this.translate('strike')} active={!!formats[TextFormatTypes.STRIKE]} type="secondary" icon size="sm"
              onClick={() => onChange(TextFormatTypes.STRIKE, !formats[TextFormatTypes.STRIKE], FormatType.INLINE)}>
              <Icon size={14} icon={strikeIcon}></Icon>
            </Button>
            <Button title={this.translate('underline')} active={!!formats[TextFormatTypes.UNDERLINE]} type="secondary" icon size="sm"
              onClick={() => onChange(TextFormatTypes.UNDERLINE, !formats[TextFormatTypes.UNDERLINE], FormatType.INLINE)}>
              <Icon size={14} icon={underlineIcon}></Icon>
            </Button>
          </ButtonGroup>

          <div className="d-flex align-items-center justify-content-between" style={{ width: '50%' }}>
            <TextColorPicker title={this.translate('highlight')}
              color={formats.background} icon={fillIcon} onChange={v => onChange(TextFormatTypes.BACKGROUND, v, FormatType.INLINE)}></TextColorPicker>
            <TextColorPicker title={this.translate('color')}
              color={formats.color} icon={textIcon} onChange={v => onChange(TextFormatTypes.COLOR, v, FormatType.INLINE)}></TextColorPicker>
            <Button title={this.translate('link')} disabled={disableLink} active={!!formats[TextFormatTypes.LINK]} type="secondary" icon size="sm"
              onClick={this.toggleLinkOpen}>
              <Icon size={14} icon={linkIcon}></Icon>
            </Button>
            <LinkNode
              dataSourceIds={dataSourceIds}
              open={this.state.openLink}
              onClose={this.toggleLinkOpen}
              formats={formats}
              onChange={this.handleLinkChange}
              className="mr-2_5"></LinkNode>
          </div>

        </div>
      </SettingRow>

      <SettingRow>
        <div className="d-flex align-items-center justify-content-between w-100">

          <ButtonGroup>
            <Button title={this.translate('left')} active={formats.align === AlignValue.LEFT} type="secondary" icon size="sm"
              onClick={() => onChange(TextFormatTypes.ALIGN, AlignValue.LEFT, FormatType.BLOCK)}>
              <Icon size={14} icon={leftIcon}></Icon>
            </Button>
            <Button title={this.translate('center')} active={formats.align === AlignValue.CENTER} type="secondary" icon size="sm"
            onClick={() => onChange(TextFormatTypes.ALIGN, AlignValue.CENTER, FormatType.BLOCK)}>
              <Icon size={14} icon={centerIcon}></Icon>
            </Button>
            <Button title={this.translate('right')} active={formats.align === AlignValue.RIGHT} type="secondary" icon size="sm"
            onClick={() => onChange(TextFormatTypes.ALIGN, AlignValue.RIGHT, FormatType.BLOCK)}>
              <Icon size={14} icon={rightIcon}></Icon>
            </Button>
            <Button title={this.translate('justify')} active={formats.align === AlignValue.JUSTIFY} type="secondary" icon size="sm"
            onClick={() => onChange(TextFormatTypes.ALIGN, AlignValue.JUSTIFY, FormatType.BLOCK)}>
              <Icon size={14} icon={justifyIcon}></Icon>
            </Button>
          </ButtonGroup>

          <ButtonGroup>
            <Button title={this.translate('bullet')} active={formats.list === ListValue.BULLET} type="secondary" icon size="sm" onClick={() => this.handleListChange(ListValue.BULLET)}>
              <Icon size={14} icon={bulletIcon}></Icon>
            </Button>
            <Button title={this.translate('ordered')} active={formats.list === ListValue.ORDERED} type="secondary" icon size="sm" onClick={() => this.handleListChange(ListValue.ORDERED)}>
              <Icon size={14} icon={orderedIcon}></Icon>
            </Button>
          </ButtonGroup>

          <Indent disabled={disableIndent} value={formats.indent} onClick={(value) => onChange(TextFormatTypes.INDENT, value, FormatType.BLOCK)}></Indent>
        </div>
      </SettingRow>

      <SettingRow flow="no-wrap" label={this.translate('characterSpacing')}>
        <Size style={{ width: '45%' }} value={formats.letterspace || DEFAULTLETTERSIZE} onChange={v => onChange(TextFormatTypes.LETTERSPACE, v, FormatType.INLINE)} ></Size>
      </SettingRow>

      <SettingRow flow="no-wrap" label={this.translate('lineSpacing')}>
        <Input style={{ width: '45%' }} type="number" value={formats.linespace || DEFAULLINESTACE} onAcceptValue={v => onChange(TextFormatTypes.LINESPACE, v, FormatType.BLOCK)}></Input>
      </SettingRow>

    </div >
  }
}

export const FormatsNode = injectIntl(_FormatsNode);