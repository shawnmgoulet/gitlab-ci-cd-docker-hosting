/** @jsx jsx */
import { React, jsx, css } from 'jimu-core';
import { ClearFormatsNode, ClearFormatsPorps } from './ui/clear-formats'
import { Editor, QuillSelection, Sources } from 'jimu-ui/rich-text-editor';

interface State {
  range?: QuillSelection;
}

interface InjectProps {
  quill: Editor;
}

export type ClearFormatsOption = ClearFormatsPorps & {
  source?: Sources;
  quillEnabled: boolean;
}


export class ClearFormats extends React.PureComponent<ClearFormatsOption & InjectProps, State> {
  static defaultProps: Partial<ClearFormatsOption & InjectProps> = {
    source: 'user',
    formats: {},
    onChange: () => { }
  }

  getStyle = () => {
    return css`
      > * {
        user-select: none;
      }
    `;
  }

  getAllSelection = (quill): QuillSelection => {
    let length = quill.getLength();
    length = length > 0 ? length - 1 : length;
    return { index: 0, length };
  }

  handleChange = () => {
    const { quillEnabled, quill } = this.props;
    const selection = quillEnabled ? quill.getSelection(false) : this.getAllSelection(quill);
    const source = quillEnabled ? 'user' : 'api';
    quill.removeFormat(selection.index, selection.length, source);
  }


  render() {
    const { quill, onChange, source, quillEnabled, ...others } = this.props;

    return <ClearFormatsNode
      css={this.getStyle()}
      {...others}
      onChange={this.handleChange}></ClearFormatsNode>
  }
}