/** @jsx jsx */
import { React, jsx, css, lodash, ImmutableArray } from 'jimu-core';
import { FormatsNode } from './ui/formats'
import { TextFormatTypes, Editor, FormatType, QuillSelection, Sources, Formats as FormatsValue, richTextUtils, IMLinkParamMap } from 'jimu-ui/rich-text-editor';

interface State {
  formats: FormatsValue;
}

interface InjectProps {
  quill: Editor;
}

export type FormatsOption = {
  className?: string;
  style?: any;
  dataSourceIds?: ImmutableArray<string>;
  formats?: FormatsValue;
  source?: Sources;
  quillEnabled: boolean;
  /**
 * Some times, the fomrats we got from quill does not contain enough information.
 * For example, for expression format, we only save id in quill object, But its details
 *  are stored in the wdiegt config.
 *  So in this way, you can mix some information into formats by `mixFormats`.
 */
  mixFormats?: (formats: FormatsValue) => FormatsValue;
}

export class Formats extends React.PureComponent<FormatsOption & InjectProps, State> {
  links: IMLinkParamMap;
  static defaultProps: Partial<FormatsOption & InjectProps> = {
    source: 'user'
  }
  debounce: any;
  selection: QuillSelection;

  constructor(props) {
    super(props);
    this.state = {
      formats: {}
    }
  }

  componentDidMount() {
    const { quill, quillEnabled } = this.props;
    this.debounce = lodash.debounce(this.onEditorChange.bind(this), 100);
    quill.on('editor-change', this.debounce);
    quill.on('scroll-optimize', this.debounce);

    const formats = this.prepareFormats({});
    this.setState({ formats });
    if (!quillEnabled) {
      this.selection = this.getAllSelection(quill);
      this.updateSelectionFormats(this.selection);
    }
  }

  getStyle = () => {
    return css`
      > * {
        user-select: none;
      }
    `;
  }

  onEditorChange = () => {
    const { quill, quillEnabled } = this.props;
    const selection = quillEnabled ? quill.getSelection(false) : this.getAllSelection(quill);
    if (!selection) {
      return;
    }
    this.selection = selection;
    this.updateSelectionFormats(selection);
  }

  updateSelectionFormats = (selection: QuillSelection) => {
    const { quill } = this.props;
    let formats = quill.getFormat(selection);
    formats = this.prepareFormats(formats);
    this.setState({ formats });
  }

  prepareFormats = (formats: FormatsValue) => {
    if (this.props.mixFormats) {
      formats = this.props.mixFormats(formats);
    }
    if (!formats || !formats.link || !formats.link.link) {
      return formats;
    } else {
      formats = lodash.assign({}, formats, { link: formats.link.link });
    }
    return formats;
  }

  getAllSelection = (quill): QuillSelection => {
    let length = quill.getLength();
    length = length > 0 ? length - 1 : length;
    return { index: 0, length };
  }

  getSelection = (): QuillSelection => {
    return this.selection;
  }

  handleChange = (key: TextFormatTypes, value: any, type: FormatType) => {
    const { quill, source, quillEnabled } = this.props;
    //handle range
    let selection = this.getSelection();

    const formatParams = {
      type,
      key,
      value,
      selection,
      line: !quillEnabled,
      source
    }

    richTextUtils.formatText(quill, formatParams);
  }

  componentWillUnmount() {
    const { quill } = this.props;
    if (quill) {
      quill.off('selection-change', this.debounce);
    }
    this.debounce && this.debounce.cancel();
  }


  render() {
    const { quillEnabled, className, style, dataSourceIds } = this.props;

    const disableLink = (!this.selection || !this.selection.length) || !quillEnabled;

    return <FormatsNode
      css={this.getStyle()}
      className={className}
      style={style}
      dataSourceIds={dataSourceIds}
      disableIndent={!quillEnabled}
      disableLink={disableLink}
      formats={this.state.formats}
      onChange={this.handleChange} />
  }
}