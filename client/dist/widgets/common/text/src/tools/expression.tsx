import { extensionSpec, React, appActions, getAppStore, LayoutContextToolProps, Immutable } from 'jimu-core';
import { IMWidgetState } from '../config';
export default class TextTool implements extensionSpec.ContextTool {
  index = 2;
  id = 'text-expression';
  widgetId: string;

  visible(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState: IMWidgetState = getAppStore().getState().widgetsState[widgetId] || Immutable({});
    return !!widgetState.showExpressionTool;
  }

  getGroupId() {
    return null;
  }

  getTitle() {
    return 'Dynamic content';
  }

  checked(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState: IMWidgetState = getAppStore().getState().widgetsState[widgetId] || Immutable({});
    return !!widgetState.showExpression;
  }

  getIcon() {
    return require('jimu-ui/lib/icons/tool-data.svg');
  }

  onClick(props: LayoutContextToolProps) {
    const widgetId = props.layoutItem.widgetId;
    const widgetState: IMWidgetState = getAppStore().getState().widgetsState[widgetId] || Immutable({});

    const showExpression = !widgetState.showExpression;

    if (showExpression) {
      if (!getAppStore().getState().widgetsRuntimeInfo[widgetId].isInlineEditing) {
        getAppStore().dispatch(appActions.setWidgetIsInlineEditingState(widgetId, true));
      }
    }
    getAppStore().dispatch(appActions.widgetStatePropChange(widgetId, 'showExpression', showExpression));
  }

  getSettingPanel(): React.ComponentClass<{}> {
    return null;
  }
}




