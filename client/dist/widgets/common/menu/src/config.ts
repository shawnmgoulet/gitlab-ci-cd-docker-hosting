import { ImmutableObject, IconResult, ThemeNav, ThemeNavType, ThemePaper } from 'jimu-core';
import { LinearUnit } from 'jimu-ui';

export enum MenuType {
  Icon = 'ICON',
  Vertical = 'VERTICAL',
  Horizontal = 'HORIZONTAL'
}

export enum SubMenuOpenMode {
  Expand = 'EXPAND',
  Foldable = 'FOLDABLE'
}

export enum Position {
  Start = 'start',
  End = 'end'
}

export enum Alignment {
  Left = 'left',
  Center = 'center',
  Right = 'right'
}

export type NavSetting = {
  alignment?: Alignment;
  space?: LinearUnit,
  showText?: boolean;
  showIcon?: boolean;
  iconPosition?: Position;
} & ThemeNav;

export interface Config {
  type: MenuType;
  subOpenMode?: SubMenuOpenMode;
  icon?: IconResult;
  paper?: ThemePaper;
  main?: NavSetting;
  sub?: NavSetting;
  navType?: ThemeNavType;
}

export type IMConfig = ImmutableObject<Config>;