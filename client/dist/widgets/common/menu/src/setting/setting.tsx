/** @jsx jsx */
import { React, jsx, css, ThemeVariables, IMState, themeUtils, ThemeNav, lodash, ThemeButtonStyles, ThemeButtonStylesByStates, ThemeNavVariants, IconResult, ThemePaper, ImmutableObject } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { SettingRow, SettingSection } from 'jimu-ui/setting-components';
import { IMConfig, Alignment, SubMenuOpenMode, MenuType, NavSetting } from '../config';
import { Icon, LinearUnit, Input, Label, ButtonGroup, Button, defaultMessages as commonMessages, Tabs, Tab } from 'jimu-ui';
import { InputUnit } from 'jimu-ui/style-setting-components';
import { IconPicker } from 'jimu-ui/resource-selector';
import defaultMessages from './translations/default'
import { ThemeColorPicker } from 'jimu-ui/color-picker';

let leftIcon = require('jimu-ui/lib/icons/align-left.svg');
let centerIcon = require('jimu-ui/lib/icons/align-middle.svg');
let rightIcon = require('jimu-ui/lib/icons/align-right.svg');

interface ExtraProps {
  appTheme: ThemeVariables;
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig> & ExtraProps & { theme: ThemeVariables }, any>{
  static mapExtraStateProps = (state: IMState) => {
    return {
      appTheme: state && state.appStateInBuilder && state.appStateInBuilder.theme
    }
  }

  constructor(props) {
    super(props);
    this.onTypeChange = this.onTypeChange.bind(this);
    this.getPaperFromTheme = this.getPaperFromTheme.bind(this);
    this.getNavVariantsFromTheme = this.getNavVariantsFromTheme.bind(this);
  }

  onSettingChange = (key: string | string[], value: any) => {
    let config = this.props.config;
    if (Array.isArray(key)) {
      config = config.setIn(key, value);
    } else {
      config = config.set(key, value);
    }
    this.props.onSettingChange({
      id: this.props.id,
      config
    });
  }

  onNavTypeRadioChange = (e: React.ChangeEvent<HTMLInputElement>, value) => {
    const checked = e.currentTarget.checked;
    if (!checked) {
      return;
    }
    let config = this.props.config;
    config = config.set('navType', value);
    config = config.setIn(['main', 'variants'], null);
    config = config.setIn(['sub', 'variants'], null);
    this.props.onSettingChange({
      id: this.props.id,
      config
    });
  }

  translate = (id: string, local?: boolean) => {
    const message = local ? defaultMessages : commonMessages;
    return this.props.intl.formatMessage({ id: id, defaultMessage: message[id] });
  }

  generateNavTypes = () => {
    return [{ label: this.translate('default'), value: 'default' },
    { label: this.translate('underline'), value: 'underline' },
    { label: this.translate('pills'), value: 'pills' }];
  }

  getStyle = () => {
    return css`
      .tab-title-item{
        width: 33%;
      }
      .radio-container {
        display: flex;
        align-items: center;
        width: 100%;
        margin-top: 0.5rem;
        label {
          margin-bottom: 0;
        }
      }
    `;
  }

  getNavVariantsFromTheme(theme: ThemeVariables): ThemeNavVariants {
    let navVars: ThemeNav = lodash.getValue(theme, 'components.nav');
    return navVars && navVars.variants;
  }

  getPaperFromTheme(theme: ThemeVariables): ImmutableObject<ThemePaper> {
    return lodash.getValue(theme, 'components.paper');
  }

  tryConvertToThemePath = (color: string) => {
    const { appTheme } = this.props;
    return themeUtils.pathOf(color, true, appTheme);
  }

  onTypeChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const type = evt.target.value as MenuType;

    let config = this.props.config;
    config = config.set('type', type);
    config = config.set('navType', 'underline');
    config = config.set('subOpenMode', SubMenuOpenMode.Foldable);
    config = config.set('paper', undefined);
    config = config.setIn(['main', 'alignment'], Alignment.Center);
    config = config.setIn(['main', 'space'], { distance: 0, unit: 'px' });
    config = config.setIn(['main', 'variants'], null);
    this.props.onSettingChange({
      id: this.props.id,
      config
    });

  }

  render() {
    const { appTheme } = this.props;
    let { type, subOpenMode, icon, main = {} as NavSetting, /*sub */ navType, paper } = this.props.config;
    let { alignment, space = {} as LinearUnit, /*iconPosition, showIcon*/ variants } = main;
    variants = variants || this.getNavVariantsFromTheme(appTheme);
    paper = paper || this.getPaperFromTheme(appTheme);

    const paperbg = lodash.getValue(paper, `bg`) as string;
    const background = lodash.getValue(variants, `${navType}.bg`) as string;
    const itemSetting = lodash.getValue(variants, `${navType}.item`) as ThemeButtonStylesByStates;
    const regular = lodash.getValue(itemSetting, 'default') as ThemeButtonStyles;
    const selected = lodash.getValue(itemSetting, 'active') as ThemeButtonStyles;
    const hover = lodash.getValue(itemSetting, 'hover') as ThemeButtonStyles;
    let size = lodash.getValue(icon, 'properties.size');

    const iconSize = {
      distance: size,
      unit: 'px'
    } as LinearUnit;

    return <div css={this.getStyle()} className="widget-setting-menu jimu-widget-setting">

      <SettingSection>
        <SettingRow label={this.translate('type')}>
          <Input type="select" value={type} onChange={this.onTypeChange}>
            <option value={MenuType.Icon}>{this.translate('icon')}</option>
            <option value={MenuType.Vertical}>{this.translate('vertical')}</option>
            <option value={MenuType.Horizontal}>{this.translate('horizontal')}</option>
          </Input>
        </SettingRow>

        {type !== MenuType.Horizontal && <SettingRow label={this.translate('subMenuExpandMode', true)} flow="wrap">
          <Input type="select" value={subOpenMode} onChange={(evt) => this.onSettingChange('subOpenMode', evt.target.value)}>
            <option value={SubMenuOpenMode.Foldable}>{this.translate('foldable')}</option>
            <option value={SubMenuOpenMode.Expand}>{this.translate('expand')}</option>
          </Input>
        </SettingRow>}

        {type === MenuType.Icon &&
          <SettingRow label={this.translate('icon')} flow="no-wrap">
            <IconPicker hideRemove icon={icon as IconResult} previewOptions={{ color: true, size: false }}
              onChange={(icon) => this.onSettingChange('icon', icon)}></IconPicker>
          </SettingRow>}
        {type === MenuType.Icon && <SettingRow label={this.translate('iconSize')} flow="no-wrap">
          <InputUnit value={iconSize} onChange={(value: LinearUnit) => this.onSettingChange(['icon', 'properties', 'size'], value.distance)} />
        </SettingRow>}
      </SettingSection>

      <SettingSection title={this.translate('appearance')}>

        <SettingRow label={this.translate('style')} flow="wrap">
          {this.generateNavTypes().map((item, index) =>
            <div className="radio-container" key={index}>
              <Input id={'nav-style-type' + index} style={{ cursor: 'pointer' }}
                name="style-type" onChange={e => this.onNavTypeRadioChange(e, item.value)} type="radio" checked={navType === item.value} />
              <Label style={{ cursor: 'pointer' }} for={'nav-style-type' + index} className="ml-1">{item.label}</Label>
            </div>)
          }
        </SettingRow>

        <SettingRow label={this.translate('space')} flow="no-wrap">
          <InputUnit value={space} onChange={(value) => this.onSettingChange(['main', 'space'], value)} />
        </SettingRow>

        <SettingRow flow="no-wrap" label={this.translate('alignment')}>
          <ButtonGroup>
            <Button title={this.translate('left')} type="secondary" icon size="sm" active={alignment === Alignment.Left}
              onClick={() => this.onSettingChange(['main', 'alignment'], Alignment.Left)}>
              <Icon icon={leftIcon}></Icon>
            </Button>
            <Button title={this.translate('center')} type="secondary" icon size="sm" active={alignment === Alignment.Center}
              onClick={() => this.onSettingChange(['main', 'alignment'], Alignment.Center)}>
              <Icon icon={centerIcon}></Icon>
            </Button>
            <Button title={this.translate('right')} type="secondary" icon size="sm" active={alignment === Alignment.Right}
              onClick={() => this.onSettingChange(['main', 'alignment'], Alignment.Right)}>
              <Icon icon={rightIcon}></Icon>
            </Button>
          </ButtonGroup>
        </SettingRow>

        {type !== MenuType.Icon && <SettingRow label={this.translate('background')} flow="no-wrap">
          <ThemeColorPicker colors={appTheme && appTheme.colors} value={background} onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'bg'], value)} />
        </SettingRow>}

        {type === MenuType.Icon && <SettingRow label={this.translate('background')} flow="no-wrap">
          <ThemeColorPicker colors={appTheme && appTheme.colors} value={paperbg} onChange={(value) => this.onSettingChange(['paper', 'bg'], value)} />
        </SettingRow>}

        <SettingRow label={this.translate('states')} flow="wrap">
          <Tabs pills className="flex-grow-1 w-100 h-100" fill>
            <Tab className="tab-title-item" active={true} title={this.translate('regular')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(regular && regular.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'default', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(regular && regular.bg)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'default', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(regular && regular.border && regular.border.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'default', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
            <Tab className="tab-title-item" title={this.translate('select')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(selected && selected.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'active', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(selected && selected.bg)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'active', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(selected && selected.border && selected.border.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'active', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
            <Tab className="tab-title-item" title={this.translate('hover')}>
              <SettingRow className="mt-2" label={this.translate('textColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(hover && hover.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'hover', 'color'], value)} />
              </SettingRow>
              {navType !== 'underline' && <SettingRow className="mt-2" label={this.translate('background')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(hover && hover.bg)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'hover', 'bg'], value)} />
              </SettingRow>}
              {navType === 'underline' && <SettingRow className="mt-2" label={this.translate('borderColor')} flow="no-wrap">
                <ThemeColorPicker colors={appTheme && appTheme.colors} value={this.tryConvertToThemePath(hover && hover.border && hover.border.color)}
                  onChange={(value) => this.onSettingChange(['main', 'variants', navType, 'item', 'hover', 'border', 'color'], value)} />
              </SettingRow>}
            </Tab>
          </Tabs>
        </SettingRow>

      </SettingSection>
    </div>
  }
}
