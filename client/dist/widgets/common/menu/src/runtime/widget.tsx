/** @jsx jsx */
import {
  React, jsx, css, BaseWidget, AllWidgetProps, urlUtils, IMState, IMPageJson, LinkType, IMAppConfig, PageType, BrowserSizeMode,
  LinkTo, ThemeButtonStylesByStates, ThemeButtonStyles, lodash, IconResult
} from 'jimu-core';
import { Nav, Button, NavLink, Drawer, NavItem, NavMenu, Icon, LinearUnit } from 'jimu-ui';
import { IMConfig, MenuType, SubMenuOpenMode, NavSetting } from '../config';

const DRAWER_PANEL_WIDTH = '260px';
export interface NavItemValue {
  id: string;
  to: LinkTo;
  icon: React.ComponentClass<React.SVGProps<SVGElement>> | string;
  target?: string;
  label: string;
  subs?: NavItemValue[];
}

let closeIcon = require('jimu-ui/lib/icons/close-12.svg');
let normalIcon = require('jimu-ui/lib/icons/toc-page.svg');
let linkIcon = require('jimu-ui/lib/icons/toc-link.svg');
let folderIcon = require('jimu-ui/lib/icons/toc-folder.svg');

const icons = { [PageType.Normal]: normalIcon, [PageType.Link]: linkIcon, [PageType.Folder]: folderIcon };

interface ExtraProps {
  mobile: boolean;
  currentId: string;
  appConfig: IMAppConfig;
}

interface State {
  openDrawer?: boolean;
}

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{
  defaultIcon: IconResult;
  constructor(props) {
    super(props);
    this.state = {
      openDrawer: false
    }
  }
  static mapExtraStateProps = (state: IMState) => {
    const appPath = state.appPath as any;
    const appConfig = state.appConfig;
    const { pageStructure } = appConfig;

    let currentId = urlUtils.getAppIdPageIdFromUrl(appPath).pageId;

    if (!currentId && pageStructure && pageStructure.length) {
      currentId = Object.keys(pageStructure[0])[0];
    }

    return {
      mobile: state.browserSizeMode === BrowserSizeMode.Small,
      currentId: currentId,
      appConfig: state.appConfig
    }
  };

  componentDidUpdate(prevProps: ExtraProps) {
    if (this.props.currentId !== prevProps.currentId) {
      this.setState({
        openDrawer: false
      });
    }
  }

  getNavItems = (): NavItemValue[] => {
    const { appConfig: { pageStructure, pages } } = this.props;
    const navItems = [];
    pageStructure.forEach(item => {
      var id = Object.keys(item)[0];
      const page = pages[id];
      if (!page.isVisible) {
        return;
      }
      const navItem = this.getNavItemValue(page);

      var subids = item[id];
      const subNavItems = [];
      subids.forEach(sid => {
        const spage = pages[sid];
        if (!spage.isVisible) {
          return;
        }
        subNavItems.push(this.getNavItemValue(spage));
      });
      navItem.subs = subNavItems;

      navItems.push(navItem);
    });
    return navItems;
  }

  getNavItemValue = (page: IMPageJson): NavItemValue => {
    return {
      id: page.id,
      to: this.getLinkTo(page),
      icon: page.icon || icons[page.type],
      target: page.openTarget,
      label: page.label
    }
  }

  getLinkTo = (page: IMPageJson) => {
    if (page.type === PageType.Folder) {
      return '#';
    } else if (page.type === PageType.Link) {
      return {
        linkType: LinkType.WebAddress,
        value: page.linkUrl
      }
    } else if (page.type === PageType.Normal) {
      return {
        linkType: LinkType.Page,
        value: page.id
      }
    }
    return '#';
  }

  navStyle = () => {
    const { config: { type, main: { variants }, navType } } = this.props;
    const background = lodash.getValue(variants, `${navType}.bg`) as string;
    const width = type === MenuType.Vertical ? '100%' : undefined;
    return css`
        *:focus{
          box-shadow: none !important;
        }
        width: ${width};
        height: 100%;
        background: ${background};
        flex-wrap: nowrap;
       ${this.navLinkStyle()}
    `;
  }

  navLinkStyle = () => {
    const { config: { main: { variants }, navType } } = this.props;
    const itemSetting = lodash.getValue(variants, `${navType}.item`) as ThemeButtonStylesByStates;
    const regular = lodash.getValue(itemSetting, 'default') as ThemeButtonStyles;
    const active = lodash.getValue(itemSetting, 'active') as ThemeButtonStyles;
    const hover = lodash.getValue(itemSetting, 'hover') as ThemeButtonStyles;
    return css`
      .nav-link {
        white-space: nowrap;
        overflow: hidden;
        width: 100%;
        ${regular && css`
          color: ${regular && regular.color};
          background: ${regular.bg && regular.bg};
          border-color: ${regular && regular.border && regular.border.color};
          border-width: ${regular && regular.border && regular.border.width};
        `}
        ${hover && css`
          &:hover {
            color: ${hover && hover.color};
            background: ${hover.bg && hover.bg};
            border-color: ${hover && hover.border && hover.border.color};
            border-width: ${hover && hover.border && hover.border.width};
          }
        `}
        ${active && css`
          &:not(:disabled):not(.disabled):active, 
          &:not(:disabled):not(.disabled).active, 
          &[aria-expanded="true"] {
            color: ${active && active.color};
            background: ${active.bg && active.bg};
            border-color: ${active && active.border && active.border.color};
            border-width: ${active && active.border && active.border.width};
          }
        `}
      }
    `;
  }

  naveMenuStyle = () => {
    const { config: { main: { variants, space = {} as LinearUnit }, navType } } = this.props;
    const background = lodash.getValue(variants, `${navType}.bg`) as string;
    return css`
        *:focus{
          box-shadow: none !important;
        }
        flex-wrap: nowrap;
        ul.dropdown-menu--inner {
          padding: 0;
          background: ${background};
          .nav-item {
            margin-top:${space.distance}${space.unit};
            ${this.navLinkStyle()}
          }
        }

    `;
  }

  getDrawerStyle = () => {
    const { config: { main: { variants }, paper, navType } } = this.props;
    const paperbg = lodash.getValue(paper, `bg`) as string;

    const itemSetting = lodash.getValue(variants, `${navType}.item`) as ThemeButtonStylesByStates;
    const regular = lodash.getValue(itemSetting, 'default') as ThemeButtonStyles;
    return css`
      .paper {
        height: 100%;
        background: ${paperbg};
        display: flex;
        flex-direction: column;
        .header {
          flex-shrink: 0;
          flex-grow: 0;
          display: flex;
          justify-content: flex-end;
          background: inherit;
          color:  ${regular && regular.color};
          > button {
            color: inherit;
          }
        }
        .jimu-nav {
          flex-grow: 1;
        }
      }
    `;
  }

  toggleOpenDrawer = () => {
    this.setState({
      openDrawer: !this.state.openDrawer
    });
  }

  render() {
    const { currentId, queryObject, mobile, theme } = this.props;
    let { type, subOpenMode, icon = {} as IconResult, main = {} as NavSetting, /*sub */ navType } = this.props.config;
    const { alignment, space = {} as LinearUnit, iconPosition, showIcon } = main;

    const vertical = type !== MenuType.Horizontal;
    const expand = vertical && subOpenMode === SubMenuOpenMode.Expand;
    const navItems = this.getNavItems();
    const mode = type === MenuType.Horizontal ? 'dropdown' : expand ? 'static' : 'foldable';

    const navbar =
      <Nav
        navbar
        css={this.navStyle()}
        theme={theme}
        className="nav-ul"
        style={{ width: (type === MenuType.Icon && !mobile) ? DRAWER_PANEL_WIDTH : '100%' }}
        justified={!vertical}
        vertical={vertical}
        gap={`${space.distance}${space.unit}`}
        underline={navType === 'underline'}
        pills={navType === 'pills'}
        textAlign={alignment}
        submenuMode={mode}
      >
        {navItems.map((navItem, index) => {
          const main = <NavLink to={navItem.to} active={navItem.id === currentId} target={navItem.target} icon={showIcon ? navItem.icon : ''}
            iconPosition={iconPosition} queryObject={queryObject}
            caret={navItem.subs && navItem.subs.length > 0}>{navItem.label}</NavLink>;

          const items = navItem.subs.map((subItem, subIndex) =>
            <NavItem key={subIndex}>
              <NavLink active={subItem.id === currentId} to={subItem.to} icon={showIcon ? subItem.icon : ''}
                iconPosition={iconPosition} target={navItem.target} queryObject={queryObject}>{subItem.label}</NavLink>
            </NavItem>
          );

          const mavMenu = items && items.length > 0 ? <NavMenu css={this.naveMenuStyle()}>
            {items}
          </NavMenu> : undefined;
          return <NavItem key={index} space={`${space.distance}${space.unit}`}>{main}{mavMenu}</NavItem>
        })}
      </Nav>;

    let children = null;
    if (type === MenuType.Icon) {
      children = <React.Fragment>
        <div className="button-container w-100 h-100 d-flex align-items-center justify-content-center">
          <Button icon type="tertiary" onClick={this.toggleOpenDrawer}>
            <Icon className="caret-icon" icon={icon.svg} color={icon.properties.color} size={icon.properties.size} />
          </Button>
        </div>
        <Drawer css={this.getDrawerStyle()} anchor={mobile ? 'full' : 'left'} open={this.state.openDrawer} onRequestClose={this.toggleOpenDrawer}>
          <div className="header">
            <Button className="m-2" type="tertiary" size="sm" icon onClick={this.toggleOpenDrawer}>
              <Icon className="caret-icon" icon={closeIcon} />
            </Button>
          </div>
          {navbar}
        </Drawer>
      </React.Fragment>
    } else {
      children = navbar
    }


    return <div className="widget-menu jimu-widget">
      {children}
    </div>
  }
}
