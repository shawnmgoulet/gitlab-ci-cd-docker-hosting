/** @jsx jsx */
import { css, jsx, React, themeUtils, ThemeVariables, IntlShape, injectIntl} from 'jimu-core';
import { Input, Label } from 'jimu-ui';
import { IMConfig, UiMode } from '../../config';

interface State {
  //url: string;
  //items: [];
}
interface Props {
  uiMode: UiMode;
  onItemsChange: Function;
  items?: [];
  themeVal: ThemeVariables;
}
interface ExtraProps {
  intl: IntlShape;
  theme: ThemeVariables;
}

export class _ItemsSelector extends React.PureComponent<IMConfig & Props & ExtraProps, /*{}*/State>{
  itemS_DATA_Popup: object[];
  itemS_DATA_Inline: object[];

  rootRef: React.RefObject<HTMLDivElement>;

  constructor(props) {
    super(props);

    this.itemS_DATA_Popup = [
      { id: 'embed', des: 'Embed' },
      { id: 'qrcode', des: 'QR code' },
      { id: 'email', des: 'Email' },
      //{ id: 'sharelink', des: 'Share Link' },
      { id: 'facebook', des: 'Facebook' },
      { id: 'twitter', des: 'Twitter' },
      //{id:'linkedin', des:'LinkedIn'}
    ];
    this.itemS_DATA_Inline = [
      { id: 'facebook', des: 'Facebook' },
      { id: 'twitter', des: 'Twitter' },
      //{id:'linkedin', des:'LinkedIn'},
      { id: 'embed', des: 'Embed' },
      { id: 'qrcode', des: 'QR code' },
      { id: 'email', des: 'Email' },
      { id: 'sharelink', des: 'Share Link' },
    ];

    this.rootRef = React.createRef();
  }

  _isItemChecked = (id) => {
    if (!this.props.items || !this.props.items.length) {
      return false;
    }

    for (var i = 0, len = this.props.items.length; i < len; i++) {
      var itemId = this.props.items[i];
      if (id === itemId) {
        return true;
      }
    }
    return false;
  }
  _createItemUI = () => {
    var elements = [];

    var datas = [];
    var mode = this.props.uiMode;
    if (mode === UiMode.Popup) {
      datas = this.itemS_DATA_Popup;
    } else {
      datas = this.itemS_DATA_Inline;
    }

    for (var i = 0, len = datas.length; i < len; i++) {
      var item = datas[i];
      let isChecked = this._isItemChecked(item.id);
      elements.push(
        <li className="d-flex" key={'key-' + i}>
          <Label for={item.id} className="d-flex justify-content-start flex-grow-1" >{item.des}</Label>
          <Input type="checkbox" className="d-flex" id={item.id} checked={isChecked} onChange={this.onItemChange}></Input>
        </li>
      );
    }
    return elements;
  }

  onItemChange = (/*e*/) => {
    //var itemId = e.target.id;
    //var isChecked = e.target.checked;
    this.onOptionsChange();
  }
  onOptionsChange = () => {
    var checkedItems = [];

    var inputs = this.rootRef.current.querySelectorAll('input');
    for (var i = 0, len = inputs.length; i < len; i++) {
      var item = inputs[i];

      if (item.checked === true) {
        checkedItems.push(item.id);
      }
    }

    this.props.onItemsChange(checkedItems);
  }


  getStyle = () => {
    var theme = this.props.themeVal;

    return css`
      font-size: 13px;
      font-weight: lighter;

      .item-option{
        padding: 0.5rem;
        list-style: none;
        list-style-type: none;
        background: ${theme.colors.light};
      }
    `;
  }
  render() {
    return <div ref={this.rootRef} css={this.getStyle()}>
      <ul className={'item-option'}>
        {this._createItemUI()}
      </ul>
    </div>
  }
}

export const ItemsSelector = themeUtils.withStyles(injectIntl(_ItemsSelector), 'ItemsSelector');