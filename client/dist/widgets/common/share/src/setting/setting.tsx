/** @jsx jsx */
import { jsx, Immutable, IconResult } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { Button, Icon, Switch, Input, Label, ButtonGroup } from 'jimu-ui'
import { IMConfig, UiMode, InlineDirection, IconRadius, BtnIconSize, DefaultIconConfig } from '../config';
import { getStyle } from './style';
import { ItemsSelector } from './components/items-selector';
import { RadiusSelector } from './components/radius-selector';
import { DefaultOrColorpicker } from './components/default-or-colorpicker';
import { IconPicker } from 'jimu-ui/resource-selector';

const rightArrowIcon = require('jimu-ui/lib/icons/direction-right.svg');
const downArrowIcon = require('jimu-ui/lib/icons/direction-down.svg');

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig> & {}/*State*/, any>{
  constructor(props) {
    super(props);
  }

  //1
  onUIModeChanged = (e) => {
    var uiMode = e.target.getAttribute('data-uimode');

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('uiMode', uiMode)
    });
  }
  _activeUiModeStyle = (mode: UiMode) => {
    if (this.props.config.uiMode === mode) {
      return ' active'
    } else {
      return '';
    }
  }


  //2.1. popup mode
  // onIconColorChange = (color) => {
  //   this.props.onSettingChange({
  //     id: this.props.id,
  //     config: this.props.config.setIn(['popup', 'icon', 'color'], color)
  //   });
  // }
  // onBtnIconSizeChange = (e) => {
  //   var val = e.target.value;
  //   let popup = Immutable(this.props.config.popup);
  //   popup = popup.setIn(['icon', 'size'], val);

  //   this.props.onSettingChange({
  //     id: this.props.id,
  //     config: this.props.config.set('popup', popup)
  //   });
  // }
  onIconChange = (icon) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['popup', 'icon'], icon)
    });
  }
  onPopupItemsChange = (items) => {
    let popupSetting = Immutable(this.props.config.popup);
    popupSetting = popupSetting.set('items', items);

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('popup', popupSetting)
    });
  }
  onToolTipConfigChange = (e) => {
    var val = e.target.value;
    let popupSetting = Immutable(this.props.config.popup);
    popupSetting = popupSetting.set('tooltip', val);

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('popup', popupSetting)
    });
  }
  //2.1. popup mode

  //2.2 inline mode
  onInlineItemsChange = (items) => {
    let inlineSetting = Immutable(this.props.config.inline);
    inlineSetting = inlineSetting.set('items', items);

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('inline', inlineSetting)
    });
  }
  onInlineDirChange = (dir: InlineDirection) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'direction'], dir)
    });
  }
  onIconStyleChange = (radius) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'btnRad'], radius)
    });
  }
  onHideLabelChange = (e) => {
    var isChecked = e.target.checked;
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'hideLabel'], isChecked)
    });
  }
  onInlineBtnColorChange = (color) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'btnColor'], color)
    });
  }
  onInlineIconColorChange = (color) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'iconColor'], color)
    });
  }
  onInlineSizeChange = (e) => {
    var val = e.target.value;
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['inline', 'design', 'size'], val)
    });
  }
  //2.2 inline mode


  //for render
  //2.1
  _getShareWidgetIcons = (): Array<IconResult> => {
    let shareIcon1 = require('../assets/icons/share_1.svg'); //can't dynamic loading
    let shareIcon2 = require('../assets/icons/share_2.svg');
    let shareIcon3 = require('../assets/icons/share_3.svg');
    let shareIcon4 = require('../assets/icons/share_4.svg');
    let shareIcon5 = require('../assets/icons/share_5.svg');
    let shareIcon6 = require('../assets/icons/share_6.svg');
    let shareIcon7 = require('../assets/icons/share_7.svg');
    let shareIcon8 = require('../assets/icons/share_8.svg');
    var iconList = [shareIcon1, shareIcon2, shareIcon3, shareIcon4, shareIcon5, shareIcon6, shareIcon7, shareIcon8];
  
    var resList = [];
    for (var i = 0, len = 8; i < len; i++) {
      resList.push({
        svg: iconList[i],
        properties: {
          color: DefaultIconConfig.properties.color,
          size: DefaultIconConfig.properties.size,
          inlineSvg: DefaultIconConfig.properties.inlineSvg
        }
      });
    }
    return resList;
  }
  renderPopupModeSetting = () => {
    var subSettingUI = null;

    var shareWidgetIcons = this._getShareWidgetIcons();
    var icon = this.props.config.popup.icon ? this.props.config.popup.icon : shareWidgetIcons[0];

    subSettingUI = <div>
      <SettingSection>{/* title={'Icon'} */}
        <SettingRow>
          <div className="d-flex justify-content-between w-100 align-items-start">
            <h6 className="icon-tip">Icon</h6>
            <IconPicker configurableOption="all" buttonOptions={{ type: 'default', size: 'sm' }} hideRemove icon={icon as any} groups={'none'}
              customIcons={shareWidgetIcons} onChange={(icon) => this.onIconChange(icon)}></IconPicker>
            {/*this.props.icon ? <label>{this.props.icon.properties.filename}</label> : null*/}
          </div>
        </SettingRow>
      </SettingSection>

      <SettingSection title={'Share options'}>
        <ItemsSelector items={this.props.config.popup.items} themeVal={this.props.theme} {...this.props}
          uiMode={this.props.config.uiMode} onItemsChange={this.onPopupItemsChange}></ItemsSelector>
      </SettingSection>

      <SettingSection>
        <SettingRow label={'Tooltip'}>
          <Input value={this.props.config.popup.tooltip} onChange={this.onToolTipConfigChange} />
        </SettingRow>
      </SettingSection>
    </div>

    return subSettingUI;
  }
  //2.2
  renderInlineModeSetting = () => {
    var subSettingUI = null;
    var dir = this.props.config.inline.design.direction;
    var theme = this.props.theme;

    var btnRad = this.props.config.inline.design.btnRad;
    var rad0 = IconRadius.Rad00,
      rad1 = IconRadius.Rad20,
      rad2 = IconRadius.Rad50;

    subSettingUI = <div>
      <SettingSection title={'Share options'}>
        <ItemsSelector items={this.props.config.inline.items} themeVal={theme} {...this.props}
          uiMode={this.props.config.uiMode} onItemsChange={this.onInlineItemsChange}></ItemsSelector>
      </SettingSection>

      <SettingSection title={'Design'}>
        <SettingRow label={'Direction'}>
          <ButtonGroup>
            <Button title={'right'} type="secondary" icon outline size="sm"
              active={dir === InlineDirection.Horizontal} onClick={() => this.onInlineDirChange(InlineDirection.Horizontal)}>
              <Icon icon={rightArrowIcon}></Icon>
            </Button>
            <Button title={'down'} type="secondary" icon outline size="sm"
              active={dir === InlineDirection.Vertical} onClick={() => this.onInlineDirChange(InlineDirection.Vertical)}>
              <Icon icon={downArrowIcon}></Icon>
            </Button>
          </ButtonGroup>
        </SettingRow>

        <SettingRow label={'Icon style'}>
          <RadiusSelector radius={rad0} btnRad={btnRad} themeVal={theme} onClick={() => this.onIconStyleChange(rad0)} className="pr-3"></RadiusSelector>
          <RadiusSelector radius={rad1} btnRad={btnRad} themeVal={theme} onClick={() => this.onIconStyleChange(rad1)} className="pr-3"></RadiusSelector>
          <RadiusSelector radius={rad2} btnRad={btnRad} themeVal={theme} onClick={() => this.onIconStyleChange(rad2)}></RadiusSelector>
        </SettingRow>
        <SettingRow label={'Hide media label'}>
          <Switch checked={this.props.config.inline.design.hideLabel} onChange={this.onHideLabelChange}></Switch>
        </SettingRow>
        <SettingRow label={'Button color'}>
          <DefaultOrColorpicker className="d-flex" color={this.props.config.inline.design.btnColor} onColorChange={this.onInlineBtnColorChange} />
        </SettingRow>
        <SettingRow label={'Icon color'}>
          <DefaultOrColorpicker className="d-flex" color={this.props.config.inline.design.iconColor} onColorChange={this.onInlineIconColorChange} />
        </SettingRow>
        <SettingRow label={'Size'}>
          <Input type="select" value={this.props.config.inline.design.size} onChange={this.onInlineSizeChange} className="w-50">
            <option value={BtnIconSize.Small}>{'small'}</option>
            <option value={BtnIconSize.Medium}>{'medium'}</option>
            <option value={BtnIconSize.Large}>{'large'}</option>
          </Input>
        </SettingRow>

      </SettingSection>
    </div>

    return subSettingUI;
  }

  render() {
    //image
    //let fileName = this.props.config.imageParam && this.props.config.imageParam.originalName;
    var subSettingUI = null;
    var description = null;
    var uiMode = this.props.config.uiMode;
    if (uiMode === UiMode.Popup) {
      subSettingUI = this.renderPopupModeSetting();
      description = 'Popup sharing URL and options when user click to share.';
    } else {
      subSettingUI = this.renderInlineModeSetting();
      description = 'Tile sharing options inline.';
    }

    return <div css={getStyle(this.props.theme)} className="widget-setting-menu jimu-widget-setting">
      {/*1. */}
      <SettingSection title={'Share Type'}>
        <SettingRow>
          <div className="ui-mode-setting">
            <div>
              <img className={`ui-mode-card` + this._activeUiModeStyle(UiMode.Popup)} data-uimode={UiMode.Popup} id="uimode-0"
                src={require('./assets/style0.svg')} onClick={this.onUIModeChanged} />
              <Label for="uimode-0">Popup</Label>
            </div>
            <div>
              <img className={`ui-mode-card` + this._activeUiModeStyle(UiMode.Inline)} data-uimode={UiMode.Inline} id="uimode-1"
                src={require('./assets/style1.svg')} onClick={this.onUIModeChanged} />
              <Label for="uimode-1">Inline</Label>
            </div>
          </div>
        </SettingRow>
        <SettingRow>
          {description}
        </SettingRow>
      </SettingSection>
      {/*2. subSetting */}
      {subSettingUI}
      <SettingRow>
      </SettingRow>
    </div>
  }
}

/* for image selector
  <SettingRow>
    <div className="d-flex justify-content-between w-100 align-items-center">
      <label className="m-0">source</label>
      <div style={{ width: '70px' }} className="uploadFileName"
        title={fileName ? fileName : "noneSource"}>
        {fileName ? fileName : "noneSource"}
      </div>
      <div style={{ width: '60px' }}><ImageSelector className="text-dark d-flex justify-content-center btn-browse" color="secondary"
        widgetId={this.props.id} label="Set" size="sm"
        onChange={this.onImageResourceChange} imageParam={this.props.config.imageParam} />
      </div>
    </div>
  </SettingRow>
*/
// onImageResourceChange = (imageParam: ImageParam) => {
//   let tempImageParam: ImageParam = imageParam;
//   if (!tempImageParam) {
//     tempImageParam = {};
//   }

//   let config = Immutable(this.props.config);
//   if (config.imageParam && config.imageParam.cropParam) {
//     tempImageParam.cropParam = {
//       svgViewBox: config.imageParam.cropParam.svgViewBox,
//       svgPath: config.imageParam.cropParam.svgPath,
//       cropShape: config.imageParam.cropParam.cropShape,
//     }
//   }
//   //config = config.set('imageParam', tempImageParam);

//   this.props.onSettingChange({
//     //widgetId: this.props.id,
//     id: this.props.id,
//     config: this.props.config.set('imageParam', tempImageParam)
//   });
// }