import {ThemeVariables, css, SerializedStyles/*, polished*/} from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles{
  return css`
      font-size: 13px;
      font-weight: lighter;

      .ui-mode-setting {
        display: flex;
      }
      .ui-mode-card {
        flex: 1;
        width: 108px;
        background-color: #181818;
        border: 2px solid transparent;
        margin: 0 0.25rem 0.5rem 0.25rem;
      }
      .ui-mode-card.active {
        border: 2px solid #00D8ED;
      }

      .icon-tip {
        color: #c5c5c5;
        font-weight: 400;
      }
  `
}