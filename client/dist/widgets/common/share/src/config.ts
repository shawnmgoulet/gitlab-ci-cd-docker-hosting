//import { ImageParam } from 'jimu-ui';
import { ImmutableObject, IMIconResult, Immutable } from 'jimu-core';

export enum UiMode {
  Popup = 'POPUP',
  Inline = 'INLINE'
}

export enum InlineDirection {
  Horizontal = 'HORIZONTAL',
  Vertical = 'VERTICAL'
}

//popup mode
export enum BtnIconSize{
  Small = 'sm',
  Medium = 'default',
  Large = 'lg',
}

//inline mode
export enum IconSize{
  Small = 16, //'sm',
  Medium = 24, //'default',
  Large = 32//'lg',
}
export enum IconRadius {
  Rad00 = 0,
  Rad20 = '5px',
  Rad50 = '50%',
}


/* widget config */
export interface ShareConfig {
  uiMode: UiMode;
  //imgSrc: string;
  //imageParam?: ImmutableObject<ImageParam>;
  popup: {
    icon: IMIconResult | '';
    items: string[];
    tooltip: string;
  };
  inline: {
    items: string[];
    design: {
      direction: InlineDirection;
      btnRad: string;
      hideLabel: boolean;
      btnColor: string;
      iconColor: string;
      size: IconSize;
      //numOfDisplay: number
    }
  };
}
export type IMConfig = ImmutableObject<ShareConfig>;


/* default config */
const shareIconImage = require('./assets/icons/default-main-icon.svg');
export const DefaultIconConfig = Immutable({
  svg: shareIconImage,
  properties: {
    color: '#585858',
    size: IconSize.Medium,
    inlineSvg: true
  }
});
