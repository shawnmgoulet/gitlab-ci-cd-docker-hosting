/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl } from 'jimu-core';
import { Button, Icon } from 'jimu-ui';
import { QRCode as JimuQRCode } from 'jimu-ui/qr-code';
import { ItemShownMode, ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


let IconImage = require('./assets/icons/qrcode.svg');
export class _QRCode extends React.PureComponent<ShareItemsConstraint, {}, {}>{
  itemName: string;
  defaultBgColor: string;
  constructor(props) {
    super(props);
    this.itemName = 'QRCode';
    this.defaultBgColor = '#35465C';
  }

  onClick = () => {
    this.props.onOpenPopup(true);
    this.props.onPopupBodyChange(this.itemName);
  }

  getLabel(){
    if(this.props.hideLabel){
      return ''
    } else {
      return this.itemName;
    }
  }

  render() {
    var shownMode = this.props.shownMode || ItemShownMode.Btn; 
    var content = null;

    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    if (shownMode !== ItemShownMode.Btn) {
      content = <JimuQRCode value={this.props.url} level="L" includeMargin={true} downloadFileName="Exb_QRCode"></JimuQRCode>
    } else {
      content = <div className={'d-flex align-items-center'}>
        <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
          <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
        </Button>
        {labelOutOfBtnContent}
      </div>
    }

    return (
      <div>{content}</div>
    );
  }
}

export const QRCode = themeUtils.withStyles(injectIntl(_QRCode), 'QRCode');