/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl } from 'jimu-core';
// import defaultMessages from '../../../translations/default';
import { Button, Icon } from 'jimu-ui';
import { Label, Input } from 'jimu-ui';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { ItemShownMode, ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


interface State {
  url: string;
  //copied?: false;
  text?: string;
  embedSize: string;

  w?: number;
  h?: number;
}

interface EmbedProps {
  embedSize?: string
}

let IconImage = require('./assets/icons/embed.svg');
export class _Embed extends React.PureComponent<ShareItemsConstraint & EmbedProps, State, { isOpen: boolean }>{
  itemName: string;
  defaultBgColor: string;
  W_H_MAP: any;

  constructor(props) {
    super(props);
    this.itemName = 'Embed';
    this.defaultBgColor = '#35465C';

    this.W_H_MAP = {
      small: { w: 300, h: 200 },
      medium: { w: 800, h: 600 },
      large: { w: 1080, h: 720 }
    }

    this.state = {
      url: this.props.url || '',
      text: '',
      embedSize: this.props.embedSize || 'medium',

      w: 300,
      h: 200
    }
  }

  componentDidMount() {
    this._setEmbedCode();
  }

  onClickCopy = ({ target: { innerHTML } }) => {
    //console.log(`==> onClickCopy Clicked on '${innerHTML}'!`);
  }
  onCopy = () => {
    //console.log('==> onCopy');
  }

  onClick = () => {
    this.props.onOpenPopup(true);
    this.props.onPopupBodyChange(this.itemName);
  }

  _setEmbedCode = () => {
    var text = '\x3ciframe width\x3d"' + this.state.w + '" height\x3d"' + this.state.h +
      '" frameborder\x3d"0" scrolling\x3d"no" allowfullscreen src\x3d"';
    text = text + this.props.url + '"\x3e\x3c/iframe\x3e';

    this.setState({ text: text });
  }

  onSizeChange = (e) => {
    var val = e.target.value;
    this.setState({embedSize: val});

    var wh = this.W_H_MAP[val];
    this.setState({ w: wh.w, h: wh.h });
  }
  onWChange = (e) => {
    var val = e.target.value;
    this.setState({ w: val });

    this.setState({embedSize: 'custom'});
  }
  onHChange = (e) => {
    var val = e.target.value;
    this.setState({ h: val });

    this.setState({embedSize: 'custom'});
  }

  render() {
    this._setEmbedCode();

    var shownMode = this.props.shownMode || ItemShownMode.Btn;
    var content = null;

    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    if (shownMode !== ItemShownMode.Btn) {
      content = <div>
        <Input type="textarea" name="text" id="embedlinkText" className="share-url-input" value={this.state.text} /*onChange={this.onUrlChange}*/ />

        <div className="d-flex justify-content-end copy-btn-wapper">
          <CopyToClipboard onCopy={this.onCopy} text={this.state.text}>
            <Button type="secondary" onClick={this.onClickCopy} size="sm">Copy</Button>
          </CopyToClipboard>
        </div>

        <div>
          <Label for="embedlinkText">Embed options</Label>
          <div className="d-flex align-items-center embed-options-wapper">
            <Input type="select" value={this.state.embedSize} onChange={this.onSizeChange} className="flex-fill embed-option-size">
              <option value={'small'}>{'small'}</option>
              <option value={'medium'}>{'medium'}</option>
              <option value={'large'}>{'large'}</option>
              <option value={'custom'}>{'custom'}</option>
            </Input>

            <div className="d-flex align-items-center">
              <Input value={this.state.w} className="flex-fill embed-option" onChange={this.onWChange}></Input>
              X
              <Input value={this.state.h} className="flex-fill embed-option" onChange={this.onHChange}></Input>
            </div>
          </div>
        </div>
      </div>
    } else {
      content = <div className={'d-flex align-items-center'}>
        <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
          <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
        </Button>
        {labelOutOfBtnContent}
      </div>
    }

    return (
      <div>{content}</div>
    );
  }
}

export const Embed = themeUtils.withStyles(injectIntl(_Embed), 'Embed');