//import { ImageParam } from 'jimu-ui';
import { ThemeVariables, IntlShape } from 'jimu-core';


/* share itmes */
export enum ItemShownMode {
  Btn,
  Content
}
export interface ShareItemsConstraint {
  url: string;
  prefix?: string;
  shownMode: ItemShownMode;

  onPopupBodyChange?: Function;
  onOpenPopup: Function;
  onUrlChange?: Function;
  getLongUrlLink: Function;

  //btn
  hideLabel?: boolean;
  size?: string;
  //IconImg?: string;
  btnColor?: string;
  iconColor?: string;
  btnRad?: number;

  //jimu-builder
  intl: IntlShape;
  theme: ThemeVariables;
}