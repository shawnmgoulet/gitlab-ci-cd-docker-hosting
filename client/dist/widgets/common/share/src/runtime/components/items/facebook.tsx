/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl } from 'jimu-core';
import { Button, Icon } from 'jimu-ui';
import { ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


let IconImage = require('./assets/icons/facebook.svg');
export class _Facebook extends React.PureComponent<ShareItemsConstraint, {}/*State*/, { isOpen: boolean }>{
  itemName: string;
  defaultBgColor: string;

  constructor(props) {
    super(props);
    this.itemName = 'Facebook';
    this.defaultBgColor = '#3B5998';
  }

  onClick = () => {
    var $appTitle = 'ArcGIS Experience Builder'; //TODO appTitle
    var prefix = this.props.prefix || '';

    var url = 'https://www.facebook.com/sharer/sharer.php?' +
      'u=' + encodeURIComponent(this.props.url) +
      '&t=' + encodeURIComponent(prefix + $appTitle);

    itemsUtil.openInNewTab(url);
  }

  render() {
    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    return (<div className={'d-flex align-items-center'}>
      <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
        <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
      </Button>
      {labelOutOfBtnContent}
    </div>);
  }
}

export const Facebook = themeUtils.withStyles(injectIntl(_Facebook), 'Facebook');


// interface State {
//   // icon?: string;
//   // src?: string;
//   // imageWidth?:number;
//   // imageHeight?:number;
// }


//const imageParam = {}
//let IconImage = require('./assets/icons/facebook.svg');
// let imageParam = this.props.imageParam ? this.props.imageParam : Immutable({});
// if (imageParam.set) {
//   imageParam = imageParam.set('url', this.state.src);
// } else {
//   (imageParam as any).url = this.state.src;
// }


// getParsedImageSrcFromConfig = (config: Props): Promise<string> => {
//   if (config.imageParam && config.imageParam.url) {
//     return Promise.resolve(config.imageParam.url);
//   } /*else if (config.functionConfig.srcExpression) {
//     if (!useDataSourcesEnabled) {
//       return Promise.resolve('');
//     }

//     let srcExp = config && config.functionConfig && config.functionConfig.srcExpression &&
//     config.functionConfig.srcExpression.asMutable({deep: true}) || null;

//     return resolveExpression(srcExp, record, this.props.intl).then(res => {return res || ''});
//   } */else {
//     return Promise.resolve('');
//   }
// }
// componentDidMount() {
//   this.getParsedImageSrcFromConfig(this.props).then(imageUrl => {
//     if (imageUrl !== this.state.src) {
//       // if (this.__unmount) {
//       //   return;
//       // }
//       this.setState({
//         src: imageUrl
//       })
//     }
//   })
// }
// componentDidUpdate(nextProps: Props, nextState: State) {
//   if (nextProps.imageParam !== this.props.imageParam) {
//     this.getParsedImageSrcFromConfig(nextProps).then(imageUrl => {
//       if (imageUrl !== this.state.src) {
//         // if (this.__unmount) {
//         //   return;
//         // }

//         this.setState({
//           src: undefined,
//         }, () => {
//           // if (this.__unmount) {
//           //   return;
//           // }

//           this.setState({
//             src: imageUrl
//           });
//         })
//       }
//     })
//   }
// }
// handleImageLoaded = (imageWidth: number, imageHeight: number) => {
//   this.setState({
//     imageWidth: imageWidth,
//     imageHeight: imageHeight
//   });
// }