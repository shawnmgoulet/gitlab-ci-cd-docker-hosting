/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl} from 'jimu-core';
import { Button, Icon} from 'jimu-ui';
import { ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


let IconImage = require('./assets/icons/twitter.svg');
export class _Twitter extends React.PureComponent<ShareItemsConstraint, {}, {}>{
  itemName: string;
  defaultBgColor: string;
  constructor(props) {
    super(props);
    this.itemName = 'Twitter';
    this.defaultBgColor = '#1DA1F2';
  }

  onClick = () => {
    var $appTitle = 'ArcGIS Experience Builder'; //TODO appTitle
    var prefix = this.props.prefix || '';

    var url = 'https://twitter.com/intent/tweet?text=' + $appTitle + '\n@ArcGISOnline' +
      '&url=' + encodeURIComponent(prefix + this.props.url) + '&related=';

    itemsUtil.openInNewTab(url);
  }

  render() {
    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    return (<div className={'d-flex align-items-center'}>
      <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
        <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
      </Button>
      {labelOutOfBtnContent}
    </div>);
  }
}

export const Twitter = themeUtils.withStyles(injectIntl(_Twitter), 'Twitter');