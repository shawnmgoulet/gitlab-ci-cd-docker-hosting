/** @jsx jsx */
import { React, jsx, css, themeUtils, ThemeVariables, injectIntl, AllWidgetProps } from 'jimu-core';
import { IMConfig, UiMode, InlineDirection, IconRadius, BtnIconSize } from '../../config';
//items
import { ItemShownMode } from './items/_items-constraint';
import { ShareLink } from './items/sharelink';
import { QRCode } from './items/qr-code';
import { Facebook } from './items/facebook';
import { Twitter } from './items/twitter';
import { Email } from './items/email';
import { Embed } from './items/embed';

interface Props {
  showInModal: boolean;
  url: string;
  uiMode: UiMode;
  //config: IMConfig;
  themeVal: ThemeVariables;

  onUrlChange: Function;
  getLongUrlLink: Function;
  onPopupBodyChange: Function;
  //togglePopup: Function;
  onOpenPopup: Function;
}
// interface ExtraProps {
//   intl: IntlShape;
//   theme: ThemeVariables;
// }

export class _ItemsList extends React.PureComponent<AllWidgetProps<IMConfig> & IMConfig & Props /*& ExtraProps*/, {}, {}>{
  constructor(props) {
    super(props);
  }

  // 'embed','qrcode','email','sharelink','facebook','twitter'
  getElementByItemId = (id) => {
    var itemUI = null;
    var config = this.props;
    var isPopupUIMode = (config.uiMode === UiMode.Popup);

    var url = config.url;
    var hideLabel = isPopupUIMode ? true : config.inline.design.hideLabel;
   
    var btnSize = this._getBtnSize(/*isPopupUIMode,*/ config, this.props.showInModal);
    var btnColor = isPopupUIMode ? '' : config.inline.design.btnColor;
    var iconColor = isPopupUIMode ? '' : config.inline.design.iconColor;
    var btnRad = isPopupUIMode ? IconRadius.Rad50 : config.inline.design.btnRad;

    var shownMode = ItemShownMode.Btn;

    switch (id) {
      case 'embed': {
        itemUI = <Embed url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
          onOpenPopup={this.props.onOpenPopup} onPopupBodyChange={this.props.onPopupBodyChange}></Embed>
        break;
      }
      case 'qrcode': {
        itemUI = <QRCode url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
          onOpenPopup={this.props.onOpenPopup} onPopupBodyChange={this.props.onPopupBodyChange}></QRCode>
        break;
      }
      case 'email': {
        itemUI = <Email url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
        ></Email>
        break;
      }
      case 'sharelink': {
        itemUI = <ShareLink url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
          {...this.props} onOpenPopup={this.props.onOpenPopup} onUrlChange={this.props.onUrlChange} getLongUrlLink={this.props.getLongUrlLink}></ShareLink>
        break;
      }
      case 'facebook': {
        itemUI = <Facebook url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
        ></Facebook>
        break;
      }
      case 'twitter': {
        itemUI = <Twitter url={url} shownMode={shownMode} size={btnSize} hideLabel={hideLabel} btnColor={btnColor} iconColor={iconColor} btnRad={btnRad}
        ></Twitter>
        break;
      }
      default: {
        itemUI = null;
        break;
      }
    }

    return itemUI;
  }

  
  //1.uiMode == popup
  //  1.1 main btn, 1.2 btn in popup,
  //2.uiMode == list
  //  2.1 btn list
  _getBtnSize = (/*isPopupUIMode,*/config, showInModal) => {
    var size;
    //btns in the popup
    if (showInModal) {
      size = BtnIconSize.Medium; //1.2 btn in popup
      return size;
    }
    //btns outside popup
    /*if (isPopupUIMode) {
      if (config.popup.icon) {
        size = config.popup.icon.properties.size;//1.1 main btn size
      }
    }*/ else {
      size = config.inline.design.size; //2.1 btn list
    }
    return size;
  }
  _getDirClassName = (dir: string, isPopup: boolean) => {
    if (true === isPopup) {
      dir = InlineDirection.Horizontal;
    }

    if (dir) {
      return 'dir-' + dir.toLowerCase();
    } else {
      return '';
    }
  }
  getStyle = () => {
    let theme = this.props.themeVal;
    return css`
      .dir-horizontal{
        display: flex;
        flex-wrap: nowrap;
        flex-direction: row;
      }
      .dir-vertical{
        display: flex;
        flex-wrap: nowrap;
        flex-direction: column;
      }

      .item-wapper {
        margin: 0.5rem;
        /*width: 100%;
        flex-grow: 1;
        flex-basis: 0;
        flex-basis: max-content;
        flex-basis: 200px;*/
      }
      .share-item {
        /*flex-basis: max-content;
        flex-basis: 130px;*/
      }

      .label-in-btn{
        color: ${theme.colors.light}
      }
      .label-outof-btn{
        margin: 0 0.25rem;
        color: ${theme.colors.black}
      }
    `;
  }

  render() {
    var config = this.props;
    var isPopup = (config.uiMode === UiMode.Popup);
    var itemListImmutable = isPopup ? config.popup.items : config.inline.items;
    var itemList = itemListImmutable.asMutable();

    var dir = config.inline.design.direction;
    var dirKlass = this._getDirClassName(dir, isPopup);

    return (<div css={this.getStyle()}>
      <div className={dirKlass}>{
        itemList.map((itemid, idx) => {
          var key = `key-` + idx;
          return <div key={key} className="item-wapper">
            {this.getElementByItemId(itemid)}
          </div>
        })
      }</div>
    </div>);
  }
}

export const ItemsList = themeUtils.withStyles(injectIntl(_ItemsList), 'ItemsList');