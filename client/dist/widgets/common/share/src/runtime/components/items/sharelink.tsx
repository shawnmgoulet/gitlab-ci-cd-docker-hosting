/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl } from 'jimu-core';
import { Button, Icon } from 'jimu-ui';
import { Label, Input } from 'jimu-ui';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { updateQueryStringParameter } from 'jimu-core/lib/utils/url-utils';
import { ItemShownMode, ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


interface State {
  url: string;
  copied?: false;
  isShortLink: boolean;
}


let IconImage = require('./assets/icons/sharelink.svg');
export class _ShareLink extends React.PureComponent<ShareItemsConstraint, State, { isOpen: boolean }>{
  itemName: string;
  defaultBgColor: string;
  BITLY_URL: string;

  constructor(props) {
    super(props);
    this.itemName = 'ShareLink';
    this.defaultBgColor = '#35465C';

    this.BITLY_URL = 'https://arcg.is/prod/shorten';

    this.state = {
      url: this.props.url || '',
      isShortLink: false,
    }
  }


  fetchShortLink = (location: Location): Promise<any> => {
    const promise = new Promise((resolve, reject) => {
      var uri = location.href;
      uri = updateQueryStringParameter(this.BITLY_URL, 'longUrl', uri);
      uri = updateQueryStringParameter(uri, 'format', 'json');

      fetch(uri).then(response => response.json())
        .then(json => {
          var shrotLink = json.data.url;
          //console.log(shrotLink);
          resolve(shrotLink);
        })
        .catch(error => {
          console.log('Fetch Error: ', error);
          resolve(location.href);
        });
    })
    return promise;
  }

  onClickCopy = ({ target: { innerHTML } }) => {
    //console.log(`==> onClickCopy Clicked on '${innerHTML}'!`);
  }
  onCopy = () => {
    //console.log('==> onCopy');
  }

  onUrlChange = (e) => {
    var url = e.target.value;
    this.setState({ url: url, copied: false });
    this.props.onUrlChange(url);
  }

  onClick = () => {
    this.props.onOpenPopup(true);
    this.props.onPopupBodyChange(this.itemName);
  }

  toggleShortLink = (e) => {
    var urlObj = this.props.getLongUrlLink();

    var isChecked = e.target.checked;
    this.setState({ isShortLink: isChecked });

    if (isChecked) {
      this.fetchShortLink(urlObj.location).then((shortUrl) => {
        this.setState({ url: shortUrl });
        this.props.onUrlChange(shortUrl);
      })
    } else {
      this.setState({ url: urlObj.url });
      this.props.onUrlChange(urlObj.url);
    }
  }

  render() {
    var shownMode = this.props.shownMode || ItemShownMode.Btn;
    var content = null;

    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    if (shownMode !== ItemShownMode.Btn) {
      content = <div>
        <Input name="text" id="sharelinkText" className="share-url-input" value={this.state.url} onChange={this.onUrlChange} />

        <div className="d-flex justify-content-between copy-btn-wapper">
          <div className="d-flex align-items-center">
            <Input type="checkbox" checked={this.state.isShortLink} onChange={this.toggleShortLink} />
            <Label className="short-link-label">short link</Label>
          </div>
          <div>
            <CopyToClipboard onCopy={this.onCopy} text={this.state.url}>
              <Button type="secondary" size="sm" onClick={this.onClickCopy}>Copy</Button>
            </CopyToClipboard>
          </div>
        </div>
      </div>
    } else {
      content = <div className={'d-flex align-items-center'}>
        <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
          <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
        </Button>
        {labelOutOfBtnContent}
      </div>
    }

    return (
      <div>{content}</div>
    );
  }
}

export const ShareLink = themeUtils.withStyles(injectIntl(_ShareLink), 'ShareLink');