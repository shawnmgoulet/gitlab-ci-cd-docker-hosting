/** @jsx jsx */
//import { ThemeVariables, jsx, css, SerializedStyles, polished } from 'jimu-core';
import { IconRadius, BtnIconSize } from '../../../config';

//TODO HOC

export function openInNewTab(url): void {
  var win = window.open(url, '_blank');
  win.focus();
}


//btn
// function _isLabelIbBtn(hideLabel, btnRad) {
//   if (hideLabel || IconRadius.RAD50 === btnRad) {
//     return false
//   } else {
//     return true;
//   }
// }
export function getLabel(itemName, hideLabel, btnRad) {
  if (hideLabel || IconRadius.Rad50 === btnRad) {
    return null;
  } else {
    return itemName;
  }
}
export function getOutOfBtnLabel(itemName, hideLabel, btnRad) {
  if (!hideLabel && IconRadius.Rad50 === btnRad) {
    return itemName;
  } else {
    return null;
  }
}

export function getBtnBgStyle(bgColor, defaultBgColor, btnRad, size) {
  var cssObj = { border: 'none', padding: '0.5rem'/*, width: '100%'*/ };

  if (size === BtnIconSize.Small) {
    cssObj = Object.assign(cssObj, { padding: '0.25rem' });
  } else if (size === BtnIconSize.Medium) {
    cssObj = Object.assign(cssObj, { padding: '0.5rem' });
  } else if (size === BtnIconSize.Large) {
    cssObj = Object.assign(cssObj, { padding: '1rem' });
  }

  if (bgColor) {
    cssObj = Object.assign(cssObj, { backgroundColor: bgColor });
  } else if ('' === bgColor) {
    cssObj = Object.assign(cssObj, { backgroundColor: defaultBgColor });
  }
  if ('undefined' !== btnRad) {
    cssObj = Object.assign(cssObj, { borderRadius: btnRad });
  }
  return cssObj;
}


//copy btn
export function getCopyBtnStyle() {
  return { color: 'none' };
}



//Icon
// export function getIconPath(itmeId: string) {
//   var str = '';//default icon
//   str = './assets/icons/' + itmeId.toLowerCase() + '.svg';
//   return str;
// }
export function getIconStyle(btnRad, isHideLabel) {
  var cssObj = {};
  // if (btnColor) {
  //   cssObj = Object.assign({ 'backgroundColor': btnColor }, cssObj);
  // } else if ('' === btnColor) {
  //   cssObj = Object.assign({ 'backgroundColor': defaultBgColor }, cssObj);
  // } 
  if (btnRad === IconRadius.Rad50 || true === isHideLabel) {
    cssObj = Object.assign(cssObj, { margin: 0 });
  }

  return cssObj;
}
export function getIconColor(iconColor) {
  if ('' === iconColor) {
    return '#fff';
  }
  return iconColor;
}