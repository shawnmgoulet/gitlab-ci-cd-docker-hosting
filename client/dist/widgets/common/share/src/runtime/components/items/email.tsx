/** @jsx jsx */
import { React, jsx, themeUtils, injectIntl } from 'jimu-core';
import { Button, Icon } from 'jimu-ui';
import { ShareItemsConstraint } from './_items-constraint';
import * as itemsUtil from './_items-util';


let IconImage = require('./assets/icons/email.svg');
export class _Email extends React.PureComponent<ShareItemsConstraint, /*State*/{}, {}>{
  itemName: string;
  defaultBgColor: string;

  hiddenLinkRef: React.RefObject<any>;

  shareEmailSubject: string;
  shareEmailTxt1: string;
  shareEmailTxt2: string;
  shareEmailTxt3: string;

  constructor(props) {
    super(props);
    this.itemName = 'Email';
    this.defaultBgColor = '#35465C';

    this.hiddenLinkRef = React.createRef();

    this.shareEmailSubject = 'Sharing web app: ';
    this.shareEmailTxt1 = 'Here is a web app shared with you by using ArcGIS Experience Builder.';
    this.shareEmailTxt2 = 'You can create and share your own web app with ArcGIS Experience Builder.';
    this.shareEmailTxt3 = 'Visit http://experience.arcgis.com/ for details.';
  }

  onClick = () => {
    this._updateHref();
    this.clickANode();
  }

  clickANode = () => {
    this.hiddenLinkRef.current.click();
  }

  _updateHref = () => {
    var $appTitle = 'ArcGIS Experience Builder'; //TODO app title

    var aNode = this.hiddenLinkRef.current;
    if (aNode) {
      var href = 'mailto:?subject\x3d' + this.shareEmailSubject + $appTitle;
      href = href + ('\x26body\x3d' + encodeURIComponent(this.shareEmailTxt1) + '%0D%0A%0D%0A' + $appTitle);
      href = href + ('%0D%0A' + encodeURIComponent(this.props.url));
      href = href + ('%0D%0A%0D%0A' + encodeURIComponent(this.shareEmailTxt2));
      href = href + ('%0D%0A%0D%0A' + encodeURIComponent(this.shareEmailTxt3));

      aNode.href = href;
    }
  }

  render() {
    var btnStyle = itemsUtil.getBtnBgStyle(this.props.btnColor, this.defaultBgColor, this.props.btnRad, this.props.size),
      iconStyle = itemsUtil.getIconStyle(this.props.btnRad, this.props.hideLabel),
      labelInBtn = itemsUtil.getLabel(this.itemName, this.props.hideLabel, this.props.btnRad),
      labelOutOfBtn = itemsUtil.getOutOfBtnLabel(this.itemName, this.props.hideLabel, this.props.btnRad);
    var labelOutOfBtnContent = (null != labelOutOfBtn) ? <div className={'label-outof-btn'}>{labelOutOfBtn}</div> : null;
    var labelInBtnContent = (null != labelInBtn) ? <div className={'label-in-btn'}>{labelInBtn}</div> : null;

    return (<div>
      <div className={'d-flex align-items-center'}>
        <Button onClick={this.onClick} style={btnStyle} size={this.props.size} className="shadow share-item" title={this.itemName}>
          <Icon icon={IconImage} color={itemsUtil.getIconColor(this.props.iconColor)} style={iconStyle} /> {labelInBtnContent}
        </Button>
        {labelOutOfBtnContent}
      </div>
      <div style={{ display: 'none' }}>
        <a href="" ref={this.hiddenLinkRef}></a>
      </div>
    </div>);
  }
}

export const Email = themeUtils.withStyles(injectIntl(_Email), 'Email');