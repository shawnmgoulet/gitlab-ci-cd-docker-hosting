/** @jsx jsx */
import { IMState, jsx, IMAppConfig } from 'jimu-core';
import { BaseWidget, AllWidgetProps, ReactRedux } from 'jimu-core';
import { IMConfig, UiMode, DefaultIconConfig } from '../config';

import { ThemeVariables } from 'jimu-core';
import { Button, Icon, Modal, ModalHeader, ModalBody } from 'jimu-ui';
import { getStyle, getPopupStyle } from './style';

//items
import { ItemShownMode } from './components/items/_items-constraint';
import { ShareLink } from './components/items/sharelink';
import { QRCode } from './components/items/qr-code';
import { Embed } from './components/items/embed';
import { ItemsList } from './components/items-list';


interface State {
  url: string;
  uiMode: UiMode; //for compare config change
  //popup
  isPopupOpen: boolean;
  showItem?: string; //e.g QRCode tiled the popup 
}
interface ExtraProps {
  //intl: IntlShape;
  theme: ThemeVariables;
}

export class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{
  extentWatch: __esri.WatchHandle;

  constructor(props) {
    super(props);

    this.state = {
      url: '',
      uiMode: UiMode.Popup,
      //popup
      isPopupOpen: !!props.isPopupOpen,
      showItem: null,
    }
  }
  componentDidMount() {
    var urlObj = this.getLongUrlLink();
    this.setState({ url: urlObj.url });
  }
  // componentDidUpdate(prevProps: AllWidgetProps<IMConfig> & ExtraProps, prevState: State){
  //   if( this.props.config !== prevProps.config ){
  //     this.onOpenPopup(false);
  //   }
  // }
  static getDerivedStateFromProps(nextProps: AllWidgetProps<IMConfig> & ExtraProps, prevState: State) {
    if (nextProps.config.uiMode !== prevState.uiMode) {//when uiMode changed
      return {
        isPopupOpen: false
      }
    }
  }

  getLongUrlLink() {
    return {
      location: window.location,
      url: window.location.href
    }
  }

  setUiMode = (mode) => {
    this.setState({ uiMode: mode });
  }

  //popup
  onOpenPopup = (param) => {
    this.setState({ isPopupOpen: param });
  }
  onTogglePopup = () => {
    this.setState({ isPopupOpen: !this.state.isPopupOpen });
  }
  onPopupClosed = () => {
    this.setState({ showItem: null });
  }
  onPopupBodyChange = (itemName) => {
    this.setState({ showItem: itemName });
  }
  //popup 

  onBtnClick = () => {
    this.onTogglePopup();
  }
  onUrlChange = (url) => {
    this.setState({ url: url });
  }


  //for render
  //part 1
  renderOutsideUI = () => {
    var outsideUI = null;
    var tooltip = this.props.config.popup.tooltip;

    if (this.state.uiMode === UiMode.Popup) {
      var icon = this.props.config.popup.icon ? this.props.config.popup.icon : DefaultIconConfig;
      outsideUI = <Button icon onClick={this.onBtnClick} title={tooltip} style={{ border: 'none', backgroundColor: 'transparent', padding: 0 }}>
        <Icon icon={icon.svg} color={icon.properties.color} size={icon.properties.size} />
      </Button>
    } else {
      outsideUI = <ItemsList showInModal={false} url={this.state.url} uiMode={this.state.uiMode} themeVal={this.props.theme} {...this.props.config}
        onUrlChange={this.onUrlChange} getLongUrlLink={this.getLongUrlLink}
        onOpenPopup={this.onOpenPopup} onPopupBodyChange={this.onPopupBodyChange}></ItemsList>
    }

    return outsideUI;
  }
  //part 2
  renderPopupBody = () => {
    var popupBody = null;
    var showItem = this.state.showItem;

    var shownMode = ItemShownMode.Content;
    if (showItem === 'QRCode') {
      popupBody = <QRCode url={this.state.url} {...this.props.config} shownMode={shownMode}
        onOpenPopup={this.onOpenPopup} onPopupBodyChange={this.onPopupBodyChange} ></QRCode>
    } else if (showItem === 'ShareLink') {
      popupBody = <ShareLink url={this.state.url} shownMode={shownMode} {...this.props.config}
        onOpenPopup={this.onOpenPopup} onUrlChange={this.onUrlChange} getLongUrlLink={this.getLongUrlLink}></ShareLink>
    } else if (showItem === 'Embed') {
      popupBody = <Embed url={this.state.url} shownMode={shownMode} {...this.props.config}
        onOpenPopup={this.onOpenPopup} onUrlChange={this.onUrlChange} getLongUrlLink={this.getLongUrlLink}></Embed>
    } else {
      popupBody = <div>
        {/* part 2.1: top */}
        <div className="itmes">
          <ShareLink url={this.state.url} shownMode={shownMode} {...this.props.config} onUrlChange={this.onUrlChange} getLongUrlLink={this.getLongUrlLink}></ShareLink>
        </div>
        {/* part 2.2: buttom */}
        <div className="itmes itmes-wapper">
          <ItemsList showInModal={true} url={this.state.url} uiMode={this.state.uiMode} themeVal={this.props.theme} {...this.props.config}
            onUrlChange={this.onUrlChange} getLongUrlLink={this.getLongUrlLink}
            onOpenPopup={this.onOpenPopup} onPopupBodyChange={this.onPopupBodyChange}></ItemsList>
        </div>
      </div>
    }

    return popupBody;
  }
  renderPopupTitle = () => {
    var popupTitle = 'Share';
    var showItem = this.state.showItem;

    if (showItem === 'QRCode') {
      popupTitle = 'Scan this QR code to open this app';
    } else if (showItem === 'ShareLink') {
      popupTitle = 'Share link';
    } else if (showItem === 'Embed') {
      popupTitle = 'Embed this app in a website';
    }
    return popupTitle;
  }

  render() {
    this.setUiMode(this.props.config.uiMode);

    var outsideUI = this.renderOutsideUI();
    var popupBody = this.renderPopupBody();
    var popupTitle = this.renderPopupTitle();

    return <div css={getStyle(this.props.theme)} >
      {/* 1.buttons */}
      <div>
        {outsideUI}
      </div>
      {/* 2.popup */}
      <Modal centered={true} size="lg" className="sql-expression-builder-modal" contentClassName="border-0 h-100" css={getPopupStyle(this.props.theme)}
        isOpen={this.state.isPopupOpen} toggle={this.onTogglePopup} onClosed={this.onPopupClosed}/*style={this.externalDsStyle}*/>
        <ModalHeader tag="h5" toggle={this.onTogglePopup} className="popup-header">{popupTitle}</ModalHeader>
        <ModalBody>{popupBody}</ModalBody>
      </Modal>
    </div>;
  }
}



// Redux
// interface Props {
//   //url: string;
//   //prefix?: string;
// }
interface AppConfigProps {
  appConfig: IMAppConfig
}
export default ReactRedux.connect<AppConfigProps, {}, {}/*Props*/>((state: IMState) => {
  let appConfig = state.appConfig;
  return {
    appConfig
  }
})(Widget);