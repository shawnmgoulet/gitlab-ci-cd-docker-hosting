import {ThemeVariables, css, SerializedStyles} from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles{
  //let theme = this.props.theme;
  return css`
    width: 100%;
    height: 100%;
    overflow: auto;

    .items {
      display: flex;
    }
    `;
}

export function getPopupStyle(theme: ThemeVariables): SerializedStyles{
  //let theme = this.props.theme;
  return css`
    .popup-header .modal-title{
      font-weight: bolder;
    }

    .itmes-wapper {
      margin-top: 1rem;
    }

    .copy-btn-wapper {
      margin-bottom: 1rem;
    }

    .share-url-input {
      margin: 10px 0 18px 0;
    }
    .short-link-label {
      margin: 0 0.5rem;
    }

    .embed-options-wapper {
      width: 80%;
    }
    .embed-option {
      margin: 0 0.5rem;
    }
    .embed-option-size{
      width: 80%;
    }
    `;
}