define({
  source: 'ı_Source_____________İ',
  noneSource: 'ı_None_________İ',
  staticSource: 'ı_Static_____________İ',
  dynamicSource: 'ı_Dynamic_______________İ',
  set: 'ı_Set_______İ',
  toolTip: 'ı_Tooltip_______________İ',
  altText: 'ı_Alt text_________________İ',
  setLink: 'ı_Set link_________________İ',
  frame: 'ı_Frame___________İ',
  background: 'ı_Background_____________________İ',
  border: 'ı_Border_____________İ',
  corner: 'ı_Corner_____________İ',
  shadow: 'ı_Shadow_____________İ'
});