export default {
  source: 'å_Source_____________ø',
  noneSource: 'å_None_________ø',
  staticSource: 'å_Static_____________ø',
  dynamicSource: 'å_Dynamic_______________ø',
  set: 'å_Set_______ø',
  toolTip: 'å_Tooltip_______________ø',
  altText: 'å_Alt text_________________ø',
  setLink: 'å_Set link_________________ø',
  frame: 'å_Frame___________ø',
  background: 'å_Background_____________________ø',
  border: 'å_Border_____________ø',
  corner: 'å_Corner_____________ø',
  shadow: 'å_Shadow_____________ø'
}