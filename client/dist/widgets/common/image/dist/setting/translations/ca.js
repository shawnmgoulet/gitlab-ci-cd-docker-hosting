define({
  source: 'ó_Source_____________à',
  noneSource: 'ó_None_________à',
  staticSource: 'ó_Static_____________à',
  dynamicSource: 'ó_Dynamic_______________à',
  set: 'ó_Set_______à',
  toolTip: 'ó_Tooltip_______________à',
  altText: 'ó_Alt text_________________à',
  setLink: 'ó_Set link_________________à',
  frame: 'ó_Frame___________à',
  background: 'ó_Background_____________________à',
  border: 'ó_Border_____________à',
  corner: 'ó_Corner_____________à',
  shadow: 'ó_Shadow_____________à'
});