define({
  source: 'ä_Source_____________Ü',
  noneSource: 'ä_None_________Ü',
  staticSource: 'ä_Static_____________Ü',
  dynamicSource: 'ä_Dynamic_______________Ü',
  set: 'ä_Set_______Ü',
  toolTip: 'ä_Tooltip_______________Ü',
  altText: 'ä_Alt text_________________Ü',
  setLink: 'ä_Set link_________________Ü',
  frame: 'ä_Frame___________Ü',
  background: 'ä_Background_____________________Ü',
  border: 'ä_Border_____________Ü',
  corner: 'ä_Corner_____________Ü',
  shadow: 'ä_Shadow_____________Ü'
});