define({
  source: 'Ж_Source_____________Я',
  noneSource: 'Ж_None_________Я',
  staticSource: 'Ж_Static_____________Я',
  dynamicSource: 'Ж_Dynamic_______________Я',
  set: 'Ж_Set_______Я',
  toolTip: 'Ж_Tooltip_______________Я',
  altText: 'Ж_Alt text_________________Я',
  setLink: 'Ж_Set link_________________Я',
  frame: 'Ж_Frame___________Я',
  background: 'Ж_Background_____________________Я',
  border: 'Ж_Border_____________Я',
  corner: 'Ж_Corner_____________Я',
  shadow: 'Ж_Shadow_____________Я'
});