/** @jsx jsx */
import {React, classNames, ThemeVariables, css, jsx, ImmutableObject, Immutable} from 'jimu-core';
import {CropParam, Icon} from 'jimu-ui';

interface Props{
  theme: ThemeVariables;

  cropParam: ImmutableObject<CropParam>;

  onShapeChoosed: (cropParam: ImmutableObject<CropParam>) => void;
}

interface States{
}

export default class ShapeChooser extends React.PureComponent<Props, States>{

  cropShapeList = ['rectangle', 'circle', 'hexagon', 'oval', 'pentagon', 'rhombus', 'triangle'];


  constructor(props){
    super(props);
  }

  getStyle() {
    let theme = this.props.theme;

    return css`
      .widget-image-chooseshape-item {
        cursor: pointer;

        svg {
          fill: ${theme.colors.black};
        }
      }

      .widget-image-chooseshape-item:hover {
        cursor: pointer;
        background-color: ${theme.colors.palette.light[500]};
      }

      .chooseshape-item-selected {
        background-color: ${theme.colors.palette.light[500]};
      }
      `;
  }

  shapeClick = (e, index) => {
    if (this.props.cropParam && this.props.cropParam.cropShape === this.cropShapeList[index]) {
      return;
    }

    let svgItem = e.currentTarget.getElementsByTagName('svg') && e.currentTarget.getElementsByTagName('svg')[0];
    if (svgItem) {
      let cropParam = this.props.cropParam;
      if (!cropParam) {
        cropParam = Immutable({});
      }
      
      cropParam = cropParam.set('svgViewBox', svgItem.getAttribute('viewBox'));
      cropParam = cropParam.set('svgPath', svgItem.getElementsByTagName('path')[0].getAttribute('d'));
      cropParam = cropParam.set('cropShape', this.cropShapeList[index]);

      this.props.onShapeChoosed && this.props.onShapeChoosed(cropParam);
      return;
    }
  }

  render() {
    return <div className="w-100 d-flex" css={this.getStyle()}>
      {this.cropShapeList.map((item, index) => {
        let iconComponent = require(`jimu-ui/lib/icons/imagecrops/${item}.svg`);

        if (item === 'rectangle' && !this.props.cropParam) {
          return <div key={index} className={classNames('w-100 d-flex justify-content-center align-items-center widget-image-chooseshape-item border-selected')}
          style={{height: '40px'}} onClick={(e) => this.shapeClick(e, index)}><Icon icon={iconComponent}/></div>;
        } else {
          return <div key={index} className={classNames('w-100 d-flex justify-content-center align-items-center widget-image-chooseshape-item',
          {'border-selected': (this.props.cropParam && this.props.cropParam.cropShape === item)})}
          style={{height: '40px'}} onClick={(e) => this.shapeClick(e, index)}><Icon icon={iconComponent}/></div>;
        }
      })}
    </div>;
  }
}