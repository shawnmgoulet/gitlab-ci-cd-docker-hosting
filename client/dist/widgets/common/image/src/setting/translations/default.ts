export default {
  source: 'Source',
  noneSource: 'None',
  staticSource: 'Static',
  dynamicSource: 'Dynamic',
  set: 'Set',
  toolTip: 'Tooltip',
  altText: 'Alt text',
  setLink: 'Set link',
  frame: 'Frame',
  background: 'Background',
  border: 'Border',
  corner: 'Corner',
  shadow: 'Shadow',
  shape: 'Shape'
}