define({
  source: 'é_Source_____________È',
  noneSource: 'é_None_________È',
  staticSource: 'é_Static_____________È',
  dynamicSource: 'é_Dynamic_______________È',
  set: 'é_Set_______È',
  toolTip: 'é_Tooltip_______________È',
  altText: 'é_Alt text_________________È',
  setLink: 'é_Set link_________________È',
  frame: 'é_Frame___________È',
  background: 'é_Background_____________________È',
  border: 'é_Border_____________È',
  corner: 'é_Corner_____________È',
  shadow: 'é_Shadow_____________È'
});