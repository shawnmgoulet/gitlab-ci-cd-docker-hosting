define({
  source: 'ґ_Source_____________Ї',
  noneSource: 'ґ_None_________Ї',
  staticSource: 'ґ_Static_____________Ї',
  dynamicSource: 'ґ_Dynamic_______________Ї',
  set: 'ґ_Set_______Ї',
  toolTip: 'ґ_Tooltip_______________Ї',
  altText: 'ґ_Alt text_________________Ї',
  setLink: 'ґ_Set link_________________Ї',
  frame: 'ґ_Frame___________Ї',
  background: 'ґ_Background_____________________Ї',
  border: 'ґ_Border_____________Ї',
  corner: 'ґ_Corner_____________Ї',
  shadow: 'ґ_Shadow_____________Ї'
});