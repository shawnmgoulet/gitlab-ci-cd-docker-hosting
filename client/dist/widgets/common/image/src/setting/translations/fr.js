define({
  source: 'æ_Source_____________Â',
  noneSource: 'æ_None_________Â',
  staticSource: 'æ_Static_____________Â',
  dynamicSource: 'æ_Dynamic_______________Â',
  set: 'æ_Set_______Â',
  toolTip: 'æ_Tooltip_______________Â',
  altText: 'æ_Alt text_________________Â',
  setLink: 'æ_Set link_________________Â',
  frame: 'æ_Frame___________Â',
  background: 'æ_Background_____________________Â',
  border: 'æ_Border_____________Â',
  corner: 'æ_Corner_____________Â',
  shadow: 'æ_Shadow_____________Â'
});