define({
  _widgetLabel: 'Č_Embed___________ž',
  default: 'Č_default_______________ž',
  websiteAddress: 'Č_Website address________________ž',
  code: 'Č_Code_________ž',
  invalidUrlMessage: 'Č_Invalid URL. Please check and try again_____________________ž.',
  httpsUrlMessage: 'Č_Only HTTPS:// is supported____________________________ž.',
  codePlaceholder: 'Č_Paste the HTML code from the site you want to embed___________________________ž.',
  websitePlaceholder: 'Č_Enter URL (https only)_______________________ž',
  embedBy: 'Č_Embed by_________________ž'
});