define({
  _widgetLabel: 'ø_Embed___________å',
  default: 'ø_default_______________å',
  websiteAddress: 'ø_Website address________________å',
  code: 'ø_Code_________å',
  invalidUrlMessage: 'ø_Invalid URL. Please check and try again_____________________å.',
  httpsUrlMessage: 'ø_Only HTTPS:// is supported____________________________å.',
  codePlaceholder: 'ø_Paste the HTML code from the site you want to embed___________________________å.',
  websitePlaceholder: 'ø_Enter URL (https only)_______________________å',
  embedBy: 'ø_Embed by_________________å'
});