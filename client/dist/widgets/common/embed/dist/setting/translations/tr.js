define({
  _widgetLabel: 'ı_Embed___________İ',
  default: 'ı_default_______________İ',
  websiteAddress: 'ı_Website address________________İ',
  code: 'ı_Code_________İ',
  invalidUrlMessage: 'ı_Invalid URL. Please check and try again_____________________İ.',
  httpsUrlMessage: 'ı_Only HTTPS:// is supported____________________________İ.',
  codePlaceholder: 'ı_Paste the HTML code from the site you want to embed___________________________İ.',
  websitePlaceholder: 'ı_Enter URL (https only)_______________________İ',
  embedBy: 'ı_Embed by_________________İ'
});