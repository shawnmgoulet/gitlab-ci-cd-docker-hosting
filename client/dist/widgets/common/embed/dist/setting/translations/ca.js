define({
  _widgetLabel: 'ó_Embed___________à',
  default: 'ó_default_______________à',
  websiteAddress: 'ó_Website address________________à',
  code: 'ó_Code_________à',
  invalidUrlMessage: 'ó_Invalid URL. Please check and try again_____________________à.',
  httpsUrlMessage: 'ó_Only HTTPS:// is supported____________________________à.',
  codePlaceholder: 'ó_Paste the HTML code from the site you want to embed___________________________à.',
  websitePlaceholder: 'ó_Enter URL (https only)_______________________à',
  embedBy: 'ó_Embed by_________________à'
});