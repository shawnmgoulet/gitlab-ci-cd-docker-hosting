define({
  _widgetLabel: 'Į_Embed___________š',
  default: 'Į_default_______________š',
  websiteAddress: 'Į_Website address________________š',
  code: 'Į_Code_________š',
  invalidUrlMessage: 'Į_Invalid URL. Please check and try again_____________________š.',
  httpsUrlMessage: 'Į_Only HTTPS:// is supported____________________________š.',
  codePlaceholder: 'Į_Paste the HTML code from the site you want to embed___________________________š.',
  websitePlaceholder: 'Į_Enter URL (https only)_______________________š',
  embedBy: 'Į_Embed by_________________š'
});