define({
  _widgetLabel: 'í_Embed___________ő',
  default: 'í_default_______________ő',
  websiteAddress: 'í_Website address________________ő',
  code: 'í_Code_________ő',
  invalidUrlMessage: 'í_Invalid URL. Please check and try again_____________________ő.',
  httpsUrlMessage: 'í_Only HTTPS:// is supported____________________________ő.',
  codePlaceholder: 'í_Paste the HTML code from the site you want to embed___________________________ő.',
  websitePlaceholder: 'í_Enter URL (https only)_______________________ő',
  embedBy: 'í_Embed by_________________ő'
});