define({
  _widgetLabel: 'Š_Embed___________č',
  default: 'Š_default_______________č',
  websiteAddress: 'Š_Website address________________č',
  code: 'Š_Code_________č',
  invalidUrlMessage: 'Š_Invalid URL. Please check and try again_____________________č.',
  httpsUrlMessage: 'Š_Only HTTPS:// is supported____________________________č.',
  codePlaceholder: 'Š_Paste the HTML code from the site you want to embed___________________________č.',
  websitePlaceholder: 'Š_Enter URL (https only)_______________________č',
  embedBy: 'Š_Embed by_________________č'
});