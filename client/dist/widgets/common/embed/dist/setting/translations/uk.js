define({
  _widgetLabel: 'ґ_Embed___________Ї',
  default: 'ґ_default_______________Ї',
  websiteAddress: 'ґ_Website address________________Ї',
  code: 'ґ_Code_________Ї',
  invalidUrlMessage: 'ґ_Invalid URL. Please check and try again_____________________Ї.',
  httpsUrlMessage: 'ґ_Only HTTPS:// is supported____________________________Ї.',
  codePlaceholder: 'ґ_Paste the HTML code from the site you want to embed___________________________Ї.',
  websitePlaceholder: 'ґ_Enter URL (https only)_______________________Ї',
  embedBy: 'ґ_Embed by_________________Ї'
});