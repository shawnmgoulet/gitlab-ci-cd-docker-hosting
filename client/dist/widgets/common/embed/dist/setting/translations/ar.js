define({
  _widgetLabel: 'بيت_Embed___________لاحقة',
  default: 'بيت_default_______________لاحقة',
  websiteAddress: 'بيت_Website address________________لاحقة',
  code: 'بيت_Code_________لاحقة',
  invalidUrlMessage: 'بيت_Invalid URL. Please check and try again_____________________لاحقة.',
  httpsUrlMessage: 'بيت_Only HTTPS:// is supported____________________________لاحقة.',
  codePlaceholder: 'بيت_Paste the HTML code from the site you want to embed___________________________لاحقة.',
  websitePlaceholder: 'بيت_Enter URL (https only)_______________________لاحقة',
  embedBy: 'بيت_Embed by_________________لاحقة'
});