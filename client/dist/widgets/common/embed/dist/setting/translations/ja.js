define({
  _widgetLabel: '須_Embed___________鷗',
  default: '須_default_______________鷗',
  websiteAddress: '須_Website address________________鷗',
  code: '須_Code_________鷗',
  invalidUrlMessage: '須_Invalid URL. Please check and try again_____________________鷗.',
  httpsUrlMessage: '須_Only HTTPS:// is supported____________________________鷗.',
  codePlaceholder: '須_Paste the HTML code from the site you want to embed___________________________鷗.',
  websitePlaceholder: '須_Enter URL (https only)_______________________鷗',
  embedBy: '須_Embed by_________________鷗'
});