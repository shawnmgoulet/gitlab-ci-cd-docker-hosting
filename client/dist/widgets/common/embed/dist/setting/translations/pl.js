define({
  _widgetLabel: 'ł_Embed___________ą',
  default: 'ł_default_______________ą',
  websiteAddress: 'ł_Website address________________ą',
  code: 'ł_Code_________ą',
  invalidUrlMessage: 'ł_Invalid URL. Please check and try again_____________________ą.',
  httpsUrlMessage: 'ł_Only HTTPS:// is supported____________________________ą.',
  codePlaceholder: 'ł_Paste the HTML code from the site you want to embed___________________________ą.',
  websitePlaceholder: 'ł_Enter URL (https only)_______________________ą',
  embedBy: 'ł_Embed by_________________ą'
});