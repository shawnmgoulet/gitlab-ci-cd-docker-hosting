define({
  _widgetLabel: 'é_Embed___________È',
  default: 'é_default_______________È',
  websiteAddress: 'é_Website address________________È',
  code: 'é_Code_________È',
  invalidUrlMessage: 'é_Invalid URL. Please check and try again_____________________È.',
  httpsUrlMessage: 'é_Only HTTPS:// is supported____________________________È.',
  codePlaceholder: 'é_Paste the HTML code from the site you want to embed___________________________È.',
  websitePlaceholder: 'é_Enter URL (https only)_______________________È',
  embedBy: 'é_Embed by_________________È'
});