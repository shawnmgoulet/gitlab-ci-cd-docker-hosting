define({
  _widgetLabel: 'Ř_Embed___________ů',
  default: 'Ř_default_______________ů',
  websiteAddress: 'Ř_Website address________________ů',
  code: 'Ř_Code_________ů',
  invalidUrlMessage: 'Ř_Invalid URL. Please check and try again_____________________ů.',
  httpsUrlMessage: 'Ř_Only HTTPS:// is supported____________________________ů.',
  codePlaceholder: 'Ř_Paste the HTML code from the site you want to embed___________________________ů.',
  websitePlaceholder: 'Ř_Enter URL (https only)_______________________ů',
  embedBy: 'Ř_Embed by_________________ů'
});