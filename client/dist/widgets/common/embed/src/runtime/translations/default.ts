export default {
  _widgetLabel: 'Embed',
  embedHint: 'Embed by URL or code',
}
