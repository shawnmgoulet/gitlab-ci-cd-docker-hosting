define({
  _widgetLabel: 'ã_Embed___________Ç',
  default: 'ã_default_______________Ç',
  websiteAddress: 'ã_Website address________________Ç',
  code: 'ã_Code_________Ç',
  invalidUrlMessage: 'ã_Invalid URL. Please check and try again_____________________Ç.',
  httpsUrlMessage: 'ã_Only HTTPS:// is supported____________________________Ç.',
  codePlaceholder: 'ã_Paste the HTML code from the site you want to embed___________________________Ç.',
  websitePlaceholder: 'ã_Enter URL (https only)_______________________Ç',
  embedBy: 'ã_Embed by_________________Ç'
});