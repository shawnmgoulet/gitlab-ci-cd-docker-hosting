export default {
  _widgetLabel: 'å_Embed___________ø',
  default: 'å_default_______________ø',
  websiteAddress: 'å_Website address________________ø',
  code: 'å_Code_________ø',
  invalidUrlMessage: 'å_Invalid URL. Please check and try again_____________________ø.',
  httpsUrlMessage: 'å_Only HTTPS:// is supported____________________________ø.',
  codePlaceholder: 'å_Paste the HTML code from the site you want to embed___________________________ø.',
  websitePlaceholder: 'å_Enter URL (https only)_______________________ø',
  embedBy: 'å_Embed by_________________ø'

}
