define({
  _widgetLabel: 'Ĳ_Embed___________ä',
  default: 'Ĳ_default_______________ä',
  websiteAddress: 'Ĳ_Website address________________ä',
  code: 'Ĳ_Code_________ä',
  invalidUrlMessage: 'Ĳ_Invalid URL. Please check and try again_____________________ä.',
  httpsUrlMessage: 'Ĳ_Only HTTPS:// is supported____________________________ä.',
  codePlaceholder: 'Ĳ_Paste the HTML code from the site you want to embed___________________________ä.',
  websitePlaceholder: 'Ĳ_Enter URL (https only)_______________________ä',
  embedBy: 'Ĳ_Embed by_________________ä'
});