define({
  _widgetLabel: 'כן_Embed___________ש',
  default: 'כן_default_______________ש',
  websiteAddress: 'כן_Website address________________ש',
  code: 'כן_Code_________ש',
  invalidUrlMessage: 'כן_Invalid URL. Please check and try again_____________________ש.',
  httpsUrlMessage: 'כן_Only HTTPS:// is supported____________________________ש.',
  codePlaceholder: 'כן_Paste the HTML code from the site you want to embed___________________________ש.',
  websitePlaceholder: 'כן_Enter URL (https only)_______________________ש',
  embedBy: 'כן_Embed by_________________ש'
});