define({
  _widgetLabel: 'ng_Embed___________ny',
  default: 'ng_default_______________ny',
  websiteAddress: 'ng_Website address________________ny',
  code: 'ng_Code_________ny',
  invalidUrlMessage: 'ng_Invalid URL. Please check and try again_____________________ny.',
  httpsUrlMessage: 'ng_Only HTTPS:// is supported____________________________ny.',
  codePlaceholder: 'ng_Paste the HTML code from the site you want to embed___________________________ny.',
  websitePlaceholder: 'ng_Enter URL (https only)_______________________ny',
  embedBy: 'ng_Embed by_________________ny'
});