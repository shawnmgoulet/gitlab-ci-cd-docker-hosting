define({
  _widgetLabel: 'æ_Embed___________Â',
  default: 'æ_default_______________Â',
  websiteAddress: 'æ_Website address________________Â',
  code: 'æ_Code_________Â',
  invalidUrlMessage: 'æ_Invalid URL. Please check and try again_____________________Â.',
  httpsUrlMessage: 'æ_Only HTTPS:// is supported____________________________Â.',
  codePlaceholder: 'æ_Paste the HTML code from the site you want to embed___________________________Â.',
  websitePlaceholder: 'æ_Enter URL (https only)_______________________Â',
  embedBy: 'æ_Embed by_________________Â'
});