define({
  _widgetLabel: 'ä_Embed___________Ü',
  default: 'ä_default_______________Ü',
  websiteAddress: 'ä_Website address________________Ü',
  code: 'ä_Code_________Ü',
  invalidUrlMessage: 'ä_Invalid URL. Please check and try again_____________________Ü.',
  httpsUrlMessage: 'ä_Only HTTPS:// is supported____________________________Ü.',
  codePlaceholder: 'ä_Paste the HTML code from the site you want to embed___________________________Ü.',
  websitePlaceholder: 'ä_Enter URL (https only)_______________________Ü',
  embedBy: 'ä_Embed by_________________Ü'
});