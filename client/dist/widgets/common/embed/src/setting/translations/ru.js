define({
  _widgetLabel: 'Ж_Embed___________Я',
  default: 'Ж_default_______________Я',
  websiteAddress: 'Ж_Website address________________Я',
  code: 'Ж_Code_________Я',
  invalidUrlMessage: 'Ж_Invalid URL. Please check and try again_____________________Я.',
  httpsUrlMessage: 'Ж_Only HTTPS:// is supported____________________________Я.',
  codePlaceholder: 'Ж_Paste the HTML code from the site you want to embed___________________________Я.',
  websitePlaceholder: 'Ж_Enter URL (https only)_______________________Я',
  embedBy: 'Ж_Embed by_________________Я'
});