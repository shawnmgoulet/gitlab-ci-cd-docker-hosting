define({
  _widgetLabel: 'Š_Embed___________ä',
  default: 'Š_default_______________ä',
  websiteAddress: 'Š_Website address________________ä',
  code: 'Š_Code_________ä',
  invalidUrlMessage: 'Š_Invalid URL. Please check and try again_____________________ä.',
  httpsUrlMessage: 'Š_Only HTTPS:// is supported____________________________ä.',
  codePlaceholder: 'Š_Paste the HTML code from the site you want to embed___________________________ä.',
  websitePlaceholder: 'Š_Enter URL (https only)_______________________ä',
  embedBy: 'Š_Embed by_________________ä'
});