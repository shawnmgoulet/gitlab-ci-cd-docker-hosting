define({
  _widgetLabel: 'á_Embed___________Ó',
  default: 'á_default_______________Ó',
  websiteAddress: 'á_Website address________________Ó',
  code: 'á_Code_________Ó',
  invalidUrlMessage: 'á_Invalid URL. Please check and try again_____________________Ó.',
  httpsUrlMessage: 'á_Only HTTPS:// is supported____________________________Ó.',
  codePlaceholder: 'á_Paste the HTML code from the site you want to embed___________________________Ó.',
  websitePlaceholder: 'á_Enter URL (https only)_______________________Ó',
  embedBy: 'á_Embed by_________________Ó'
});