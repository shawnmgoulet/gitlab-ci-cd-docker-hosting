define({
  _widgetLabel: 'ķ_Embed___________ū',
  default: 'ķ_default_______________ū',
  websiteAddress: 'ķ_Website address________________ū',
  code: 'ķ_Code_________ū',
  invalidUrlMessage: 'ķ_Invalid URL. Please check and try again_____________________ū.',
  httpsUrlMessage: 'ķ_Only HTTPS:// is supported____________________________ū.',
  codePlaceholder: 'ķ_Paste the HTML code from the site you want to embed___________________________ū.',
  websitePlaceholder: 'ķ_Enter URL (https only)_______________________ū',
  embedBy: 'ķ_Embed by_________________ū'
});