define({
  _widgetLabel: 'आ_Embed___________ज',
  default: 'आ_default_______________ज',
  websiteAddress: 'आ_Website address________________ज',
  code: 'आ_Code_________ज',
  invalidUrlMessage: 'आ_Invalid URL. Please check and try again_____________________ज.',
  httpsUrlMessage: 'आ_Only HTTPS:// is supported____________________________ज.',
  codePlaceholder: 'आ_Paste the HTML code from the site you want to embed___________________________ज.',
  websitePlaceholder: 'आ_Enter URL (https only)_______________________ज',
  embedBy: 'आ_Embed by_________________ज'
});