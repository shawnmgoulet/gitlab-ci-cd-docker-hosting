define({
  _widgetLabel: 'Å_Embed___________ö',
  default: 'Å_default_______________ö',
  websiteAddress: 'Å_Website address________________ö',
  code: 'Å_Code_________ö',
  invalidUrlMessage: 'Å_Invalid URL. Please check and try again_____________________ö.',
  httpsUrlMessage: 'Å_Only HTTPS:// is supported____________________________ö.',
  codePlaceholder: 'Å_Paste the HTML code from the site you want to embed___________________________ö.',
  websitePlaceholder: 'Å_Enter URL (https only)_______________________ö',
  embedBy: 'Å_Embed by_________________ö'
});