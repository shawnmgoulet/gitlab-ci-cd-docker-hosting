define({
  chooseSurvey: 'بيت_Choose which survey to run in the widget______________________لاحقة:',
  createNewSurveyLabel: 'بيت_Create new survey__________________لاحقة',
  chooseSurveyLabel: 'بيت_Choose existing survey_______________________لاحقة',
  selectSurveyTip: 'بيت_Please select______________لاحقة',
  surveyDetailLabel: 'بيت_Survey Details________________لاحقة:',
  currentSurveyLabel: 'بيت_Current survey_______________لاحقة',
  resetSurveyLabel: 'بيت_Reset survey_____________لاحقة',
  summaryLabel: 'بيت_Summary_______________لاحقة',
  insertLabel: 'بيت_Insert_____________لاحقة',
  surveyTitleLabel: 'بيت_Name_________لاحقة',
  surveyTitleRequiredMsg: 'بيت_The name of the survey is required___________________لاحقة.',
  surveyTitleDuplicateMsg: 'بيت_A survey with this name already exists, please choose a different name_____________________________________لاحقة.',
  surveyTagLabel: 'بيت_Tags_________لاحقة',
  surveyTagsRequiredMsg: 'بيت_One or more tags are required_______________________________لاحقة.',
  surveySummaryLabel: 'بيت_Summary_______________لاحقة',
  createSurveyBtn: 'بيت_Create_____________لاحقة',
  editSurveyBtn: 'بيت_Edit survey____________لاحقة',
  appearanceTitle: 'بيت_Appearance_____________________لاحقة',
  hideOptionsBarLabel: 'بيت_Hide options bar_________________لاحقة',
  hideOptionsHeaderLabel: 'بيت_Hide survey header___________________لاحقة',
  hideOptionsDesLabel: 'بيت_Hide survey description________________________لاحقة',
  hideOptionsFooterLabel: 'بيت_Hide survey footer___________________لاحقة',
  ignoreThemeLabel: 'بيت_Ignore theme_____________لاحقة',
  resetSurveyTip: 'بيت_Reset survey will delete survey settings as well as data connections set to the survey_____________________________________________لاحقة.',
  confirmResetSurveyTip: 'بيت_Are you sure you want to continue__________________لاحقة?',
  yes: 'بيت_Yes_______لاحقة',
  no: 'بيت_No_____لاحقة'
});