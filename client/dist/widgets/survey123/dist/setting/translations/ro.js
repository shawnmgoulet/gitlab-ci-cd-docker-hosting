define({
  chooseSurvey: 'Ă_Choose which survey to run in the widget______________________ș:',
  createNewSurveyLabel: 'Ă_Create new survey__________________ș',
  chooseSurveyLabel: 'Ă_Choose existing survey_______________________ș',
  selectSurveyTip: 'Ă_Please select______________ș',
  surveyDetailLabel: 'Ă_Survey Details________________ș:',
  currentSurveyLabel: 'Ă_Current survey_______________ș',
  resetSurveyLabel: 'Ă_Reset survey_____________ș',
  summaryLabel: 'Ă_Summary_______________ș',
  insertLabel: 'Ă_Insert_____________ș',
  surveyTitleLabel: 'Ă_Name_________ș',
  surveyTitleRequiredMsg: 'Ă_The name of the survey is required___________________ș.',
  surveyTitleDuplicateMsg: 'Ă_A survey with this name already exists, please choose a different name_____________________________________ș.',
  surveyTagLabel: 'Ă_Tags_________ș',
  surveyTagsRequiredMsg: 'Ă_One or more tags are required_______________________________ș.',
  surveySummaryLabel: 'Ă_Summary_______________ș',
  createSurveyBtn: 'Ă_Create_____________ș',
  editSurveyBtn: 'Ă_Edit survey____________ș',
  appearanceTitle: 'Ă_Appearance_____________________ș',
  hideOptionsBarLabel: 'Ă_Hide options bar_________________ș',
  hideOptionsHeaderLabel: 'Ă_Hide survey header___________________ș',
  hideOptionsDesLabel: 'Ă_Hide survey description________________________ș',
  hideOptionsFooterLabel: 'Ă_Hide survey footer___________________ș',
  ignoreThemeLabel: 'Ă_Ignore theme_____________ș',
  resetSurveyTip: 'Ă_Reset survey will delete survey settings as well as data connections set to the survey_____________________________________________ș.',
  confirmResetSurveyTip: 'Ă_Are you sure you want to continue__________________ș?',
  yes: 'Ă_Yes_______ș',
  no: 'Ă_No_____ș'
});