/** @jsx jsx */
import { React, Immutable, IMFieldSchema, DataSource, DataSourceManager, jsx, SessionManager } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { JimuMapViewSelector, SettingSection, SettingRow, JimuLayerViewSelector } from 'jimu-ui/setting-components';
import { ArcGISDataSourceTypes, IMJimuLayerViewInfo } from 'jimu-arcgis';
import { Button, Icon, Input, Switch, Label } from 'jimu-ui';
import { SelectedDataSourceJson } from 'jimu-ui/data-source-selector';
import { IMConfig } from '../config';
import { survey123Service } from '../service/survey123.service';
import defaultMessages from './translations/default';
import { getStyle } from './css/style';

import * as ReactModal from 'react-modal';

ReactModal.setAppElement('body');

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig>, any>{
  /**
   * state variable
   */
  supportedMapTypes = Immutable([ArcGISDataSourceTypes.WebMap]);
  supportedLayerTypes = Immutable([ArcGISDataSourceTypes.FeatureLayer]);
  dsManager = DataSourceManager.getInstance();
  public state: any = {
    /**
     * survey123
     */
    newSurveyTitle: null,
    newSurveyTags: null,
    newSurveyTitleDirty: false,
    newSurveyTagsDirty: false,
    newSurveySnippet: null,
    newSurveyItemId: null,
    // newSurveyThumbnailUrl: 'https://images-eu.ssl-images-amazon.com/images/I/61ot3Cx98aL.png',
    newSurveyThumbnailUrls: [
      'https://survey123.arcgis.com/assets/img/default-thumbnails/Image01.png',
      'https://survey123.arcgis.com/assets/img/default-thumbnails/Image02.png',
      'https://survey123.arcgis.com/assets/img/default-thumbnails/Image03.png'
    ],
    newSurveyMsg: null,
    newSurveyLoading: false,
    existSurveyMsg: null,
    existSurveys: [],
    selectedSurvey: null,
    modalIsOpen: false,
    mode: 'none',
    isCheckedSurveyItemId: false,
    isShowSurveyQuestionField: false,
    surveyQuestionFields: [],
    selectedQuestionField: null,
    createSurveyErrorMsg: null,
    /**
     * data source
     */
    dsMapView: null,
    dsFeatureLayer: null,
    dsFeatureLayerFields: [],

    /**
     * field-question mapping
     */
    mapWidgetList: [],
    addMapping: null,
    editMapping: null
  }

  public newDefaultValue: {
    key: string;
    value: string;
  } = {
    key: '',
    value: ''
  };

  public iconRefresh = require('jimu-ui/lib/icons/close.svg');
  public closeIcon24 = require('jimu-ui/lib/icons/close-24.svg');
  public iconLink = require('jimu-ui/lib/icons/link.svg');
  public iconRemove = require('jimu-ui/lib/icons/delete.svg');
  // private _survey123HostUrl: string = survey123Service.getSurvey123HostUrl();
  // private _dsManager = DataSourceManager.getInstance();

  /**
   * constructor
   * @param props 
   */
  constructor(props: any) {
    super(props);

    /**
     * query existing survey
     */
    this.querySurvey();
  }

  // set state: dsFeatureLayer after loaded if the dataSourceId exist
  componentDidMount = () => {
    let useDataSources = this.props.useDataSources;
    let dataSourceId = (useDataSources && useDataSources.length) ? useDataSources[0].dataSourceId : null;
    if (dataSourceId) {
      let dataSource: any = this.dsManager.getDataSource(dataSourceId);
      let layer = dataSource ? dataSource.layer : null;
      this.setState({
        dsFeatureLayer: layer,
        dsFeatureLayerFields: this.getLayerFields(layer)
      });
    }
  }

  getLayerFields = (layer) => {
    if (!layer) {
      return [];
    }
    // clone the layer.fields
    let fields = Object.assign({}, {fields: layer.fields}).fields;
    if (layer.type !== 'feature') {
      // table or other type (has no geometry)
      return fields;
    }
    let existGeoField = (fields || []).find((item) => {
      return item.name === 'geometry';
    });
    if (existGeoField) {
      return fields;
    }
    let type = layer.geometryType;
    let geometryTypeStr = this.nls('geometryType' + type.substr(0, 1).toUpperCase() + type.substr(1));
    
    fields.push({
      alias: `${this.nls('geometryLabel')}(${geometryTypeStr})`,
      name: 'geometry'
    });
    return fields;
  }


  /**
   * on setting value changed
   */
  onValueChanged = (evt: React.FormEvent<HTMLInputElement>) => {
    let target = evt.currentTarget;

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(target.name, target.value)
    });
  }

  nls = (id: string) => {
    return this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] });
  }
  /**
   * add default value
   */
  addDefaultValue = (evt: any) => {
    if (this.newDefaultValue.key && this.newDefaultValue.value) {
      // this.props.config.defaultValue[this.newDefaultValue.key] = this.newDefaultValue.value;

      /**
       * clear newDefaultValue key / value
       */
      this.newDefaultValue = {
        key: '',
        value: ''
      };
    }
  }

  /**
   * get field alias by fild name
   */
  getFielAlias = (field) => {
    let fields = this.state.dsFeatureLayerFields || [];
    let target = fields.find((item) => {
      return item.name === field;
    });
    if (target) {
      return target.alias || target.name;
    }
    return field;
  }

    /**
   * get question label by question name
   */
  getQuestionLabel = (questionName) => {
    let questions = this.state.surveyQuestionFields || [];
    let target = questions.find((item) => {
      return item.name === questionName;
    });
    if (target) {
      return target.label || target.name;
    }
    return questionName;
  }




  /**
   * on new survey value changed
   */
  onNewSurveyValueChanged = (e: React.FormEvent<HTMLInputElement>) => {
    let target = e.currentTarget;

    if (target.name) {
      this.setState({
        [target.name]: target.value
      });
    }
    if (target.name === 'newSurveyTags') {
      this.setState({
        newSurveyTagsDirty: true
      });
    } if (target.name === 'newSurveyTitle') {
      this.setState({
        newSurveyTitleDirty: true
      });
    }
  }

  /**
   * onExistSurveyChanged
   */
  onExistSurveyChanged = (e: React.FormEvent<HTMLSelectElement>) => {
    let target = e.currentTarget;

    if (target.value && target.value !== 'null') {
      /**
       * update selected survey
       */
      let selectedSurvey = this.state.existSurveys.find((survey) => survey.id === target.value);
      this.updateMapWidgetList();
      this.setState({
        selectedSurvey: selectedSurvey
      });
    }
  }

  /**
   * create survey
   */
  createSurvey = () => {
    return Promise.resolve(true)
      .then(() => {
        /**
         * handle title, tags, description
         */
        let title = this.state.newSurveyTitle;
        let tags = (this.state.newSurveyTags || '').split(',').map((tag) => tag.trim());
        let snippet = this.state.newSurveySnippet;
        if (!title) {
          this.setState({
            newSurveyTitleDirty: true
          });

        }
        if (!this.state.newSurveyTags) {
          this.setState({
            newSurveyTagsDirty: true
          });
        }

        if (!title || !tags || !tags.length) {
          // throw new Error('missing title or tags');
          throw new Error('');
        }

        /**
         * update msg
         */
        this.setState({
          newSurveyMsg: '',
          newSurveyLoading: true
        });
        let randomIdx = parseInt(Math.random() * 10 + '') % 3;
        let thumbnail = this.state.newSurveyThumbnailUrls[randomIdx];

        return survey123Service.createSurvey(title, tags, {
          token: this.props.token,
          username: this.props.user.username,
          /**
           * portalUrl
           */
          portalUrl: this.props.config.portalUrl || this.props.portalUrl
        }, {
          snippet: snippet,
          thumbnailUrl: thumbnail
        });
      })
      .then((res: any) => {
        if (res.success === true) {
          this.updateMapWidgetList();
          /**
           * set item id
           */ 
          this.setState({
            createSurveyErrorMsg: null,
            newSurveyItemId: res.id,
            selectedSurvey: res,
            newSurveyLoading: false
          });

          return res.id;
        }
        if (res.error && res.error.code + '' == '400') {
          this.setState({
            createSurveyErrorMsg: 'A survey with this name already exists, please choose a different name.',
            newSurveyLoading: false,
            newSurveyTitleDirty: false
          });

          return null;
        }

        throw res;
      })
      .then((itemId: string) => {
        if (!itemId) {
          return;
        }
        /**
         * we should set hides in config
         */
        this.props.onSettingChange({
          id: this.props.id,
          config: this.props.config.set('hides', ['navbar', 'header', 'description', 'footer', 'theme'])
        });

        /**
         * show survey123 designer modal
         */
        this.showSurvey123DesignerModal(itemId);
      })
      .catch((err) => {
        if (err) {
          /**
           * update msg
           */
          this.setState({
            newSurveyMsg: err.message ? err.message : (typeof err === 'string') ? err : '',
            newSurveyLoading: false
          });
        }
        console.error(err);
      })

  }

  /**
   * query survey
   */
  querySurvey = (options?: {
    isShared?: boolean;
    isPublished?: boolean;
  }) => {
    // options
    options = Object.assign({
      isShared: false,
      isPublished: true
    }, options || {});
    if (this.props.config.surveyItemId && this.props.config.selectedSurvey) {

      let selectedSurvey = this.props.config.selectedSurvey;
      // this.state.mode = 'settings';
      setTimeout(() => {
        this.props.onSettingChange({
          id: this.props.id,
          config: this.props.config.set('surveyItemId', selectedSurvey.id)
        });
        /**
         * switch to settings page
         */
        this.updateMapWidgetList();
        this.setState({
          existSurveyMsg: null,
          mode: 'settings',
          selectedSurvey: selectedSurvey
        });
      }, 0);
      // this.setSurveyItemId();
      return survey123Service.querySurvey({username: this.props.user.username, token: this.props.token, portalUrl: this.props.config.portalUrl || this.props.portalUrl}, 
        { isShared: options.isShared, isPublished: options.isPublished})
      .then((surveys: any[]) => {
        this.setState({
          existSurveys: surveys
        });
      });
    }
    return Promise.resolve(true)
      .then(() => {
        if (this.state.mode === 'none') {
          this.setState({
            mode: 'survey-createSurvey'
          });
        }

        /**
         * update msg
         */
        this.setState({
          existSurveyMsg: '<div class="jimu-small-loading"></div>'
        });

        return survey123Service.querySurvey({
          username: this.props.user.username,
          token: this.props.token,
          portalUrl: this.props.config.portalUrl || this.props.portalUrl
        }, {
          isShared: options.isShared,
          isPublished: options.isPublished
        });
      })
      .then((surveys: any[]) => {
        // console.log(surveys);

        /**
         * update msg and existing surveys
         */
        this.setState({
          existSurveyMsg: null,
          existSurveys: surveys
        });

        /**
         * update selected survey
         */
        let usedSurveyItemId = this.props.config.surveyItemId;
        if (usedSurveyItemId) {
          let selectedSurvey = surveys.find((survey) => survey.id === usedSurveyItemId);
          if (selectedSurvey) {
            this.updateMapWidgetList();
            this.setState({
              selectedSurvey: selectedSurvey
            });
            this.props.onSettingChange({
              id: this.props.id,
              config: this.props.config.set('selectedSurvey', selectedSurvey)
            });
          }
        }
      })
      .catch((err) => {
        this.setState({
          mode: 'survey-createSurvey',
          existSurveyMsg: 'error when load existing surveys'
        });
        console.log(err);
      });
  }

  /**
   * get survey question fields
   */
  getSurveyQuestionFields = (): Promise<any[]> => {
    let surveyItemId = this.props.config.surveyItemId;

    return Promise.resolve(true)
      .then(() => {
        if (surveyItemId && this.props.token) {
          return survey123Service.getSurveyQuestionFields(surveyItemId, {
            token: this.props.token
          });
        }
      })
      .then((fields: any[]) => {
        if (fields) {
          this.setState({
            surveyQuestionFields: fields
          });
        }
        return fields;
      });
  }

  /**
   * show survey123 designer modal
   */
  showSurvey123DesignerModal(surveyItemId ?: string) {
    surveyItemId = surveyItemId || this.props.config.surveyItemId;
    let sessionManager = SessionManager.getInstance();
    let portalUrl = this.props.config.portalUrl || this.props.portalUrl;

    if (!surveyItemId) {
      throw new Error('cannot get survey item id to open survey123 designer');
    }

    /**
     * popup window and embed survey123 designer 
     */
    let url = survey123Service.getSurvey123DesignerUrl(surveyItemId, {
      portalUrl: portalUrl
    });

    /**
     * we need to add access_token / username / expires_in in hash
     * to tell survey123 website to parse the hash to use the token
     */
    let session = sessionManager.getMainSession();
    if (session && session.token && session.username && session.tokenDuration) {
      url += `#access_token=${session.token}&username=${session.username}&expires_in=${session.tokenDuration}`;
    }

    /**
     * show modal
     */
    this.setState({
      modalIsOpen: true,
      newSurveyMsg: null
    });

    /**
     * cannot use window.open because address bar will be shown.
     * try to use modal and iframe to show survey123 designer webpage
     */
    let self = this;
    let index = 0;
    let checkTimer = setInterval(() => {
      if (!self.state.modalIsOpen) {
        self.setState({
          modalIsOpen: true
        });
      }
      index ++;
      let target: any = document.getElementById('survey123-designer');
      if (target || index > 100) {
        clearInterval(checkTimer);
        target.src = url;
      }
    }, 50);

    // setTimeout(() => {
    //   let target: any = document.getElementById('survey123-designer');
    //   target.src = url;
    // }, 200);
  }

  /**
   * remove default value
   */
  // removeDefaultValue = (key: string, evt: any) => {
  //   let defaultValue = this.props.config.defaultValue;
  //   if (key && defaultValue[key]) {
  //     delete defaultValue[key];
  //   }
  // }

  /**
   * show setting page
   */
  showSettingPage = () => {
    this.setState({
      mode: 'settings'
    });
    /**
     * get survey question fields
     */
    this.getSurveyQuestionFields();
  }

  /**
   * handle close modal
   */
  handleCloseModal = () => {
    this.setState({
      modalIsOpen: false
    });

    this.showSettingPage();

    /**
     * update survey itemid in props config
     */
    let surveyItemId = this.state.newSurveyItemId || this.props.config.surveyItemId;
    if (surveyItemId) {
      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('surveyItemId', surveyItemId).set('timestamp', Date.now())
      });
    }
  }

  /**
   * get thumbnail url from portal
   */
  getThumbnailUrl = () => {
    let portalUrl = this.props.config.portalUrl || this.props.portalUrl || 'https://www.arcgis.com';
    let surveyItemId = this.state.selectedSurvey.id;
    let thumbnail = this.state.selectedSurvey.thumbnail;
    if ((thumbnail + '').startsWith('http://') || (thumbnail + '').startsWith('https://')) {
      return thumbnail;
    }
    return `${portalUrl}/sharing/rest/content/items/${surveyItemId}/info/${thumbnail}?token=${this.props.token}`;
  }

  /**
   * set survey item id 
   * update this.props.config
   */
  setSurveyItemId = () => {
    let selectedSurvey = this.state.selectedSurvey;
    if (selectedSurvey && selectedSurvey.id) {
      /**
       * update props to have the same setting in runtime
       * and ensure the hides are all switched on 
       */
      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('surveyItemId', selectedSurvey.id).set('hides', ['navbar', 'header', 'description', 'footer', 'theme'])
      });

      /**
       * switch to settings page
       */
      this.setState({
        mode: 'settings'
      });
    }
  }

  /**
   * edit survey
   */
  editSurvey = () => {
    this.showSurvey123DesignerModal();
  }

  /**
   * set appearance
   */
  setAppearance = (isChecked: boolean, value: string) => {
    // let target = e.currentTarget;
    // let value = target.value;
    // clone this.props.config.hides
    let hides: any = [].concat(this.props.config.hides || []);
    // let isChecked = target.checked;

    if (isChecked && hides.indexOf(value) >= 0) {
      let pos = hides.indexOf(value);
      hides.splice(pos, 1);
      // hides = hides.concat([value])
    } else {
      hides.push(value);
    }
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('hides', hides)
    });
  }

  /**
   * set selected map view
   */
  onMapWidgetSelected = (useMapWidgetIds: string[]) => {
    if (!useMapWidgetIds || !useMapWidgetIds.length) {
      this.clearMapping();
    }
    this.props.onSettingChange({
      id: this.props.id,
      useMapWidgetIds: useMapWidgetIds
    });
  }

  /**
   * on feature layer view selected
   */
  onFeatureLayerViewSelected = (selectedDsJsons: SelectedDataSourceJson[], d: SelectedDataSourceJson) => {
    if (d && d.dataSourceJson && d.rootDataSourceId) {
      /**
       * update state
       */
      this.setState({
        dsMapView: null,
        dsFeatureLayer: d
      });

      /**
       * embed params
       */
      let embeds = this.props.config.embeds || [];
      if (embeds.indexOf('associatedMap') === -1) {
        embeds = embeds.concat(['associatedMap']);
      }

      /**
       * update useDataSources
       */
      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('embeds', embeds).set('dsType', ArcGISDataSourceTypes.FeatureLayer)
        /* useDataSources: Immutable([{
          dataSourceId: d.dataSourceJson.id,
          rootDataSourceId: d.rootDataSourceId 
        }]) as ImmutableArray<IMUseDataSource> */
      });
    }
  }
  updateMapWidgetList = () => {
    // get the map widget list
    let state = window._appState;
    let appConfig = window && window.jimuConfig && window.jimuConfig.isBuilder ? state.appStateInBuilder && state.appStateInBuilder.appConfig : state.appConfig;
    let widgets = appConfig && appConfig.widgets;
    // tslint:disable-next-line:max-line-length
    let mapWidgetList = widgets ? Object.keys(widgets).filter(wId => widgets[wId] && widgets[wId].manifest && widgets[wId].manifest.properties && widgets[wId].manifest.properties.canCreateMapView).map(wId => widgets[wId]) : [];
    this.setState({
      mapWidgetList: mapWidgetList
    });
  }


  activeLinkDataChange = (e) => {
    let target = e.currentTarget;
    let isActive = target.checked;
    if (isActive) {
      this.updateMapWidgetList();
    }

    /**
     * update activeLinkData in data source
     */
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('activeLinkData', isActive),
    });
  }

  /**
   * on map view selected
   */
  onMapViewSelected = (selectedDsJsons: SelectedDataSourceJson[], d: SelectedDataSourceJson) => {
    if (d && d.dataSourceJson) {
      // if (d && d.dataSourceJson && d.rootDataSourceId) {
      // let mapViewDSJson = d.dataSourceJson;

      /**
       * update state
       */
      this.setState({
        dsMapView: d,
        dsFeatureLayer: null,
        dsFeatureLayerFields: []
      });

      /**
       * embed params
       */
      let embeds = this.props.config.embeds || [];
      if (embeds.indexOf('associatedMap') === -1) {
        embeds = embeds.concat(['associatedMap']);
      }

      /**
       * update props
       */
      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('embeds', embeds).set('selectedSurveyQuestionFields', []).set('dsType', ArcGISDataSourceTypes.Map)
        /* useDataSources: Immutable([{
          dataSourceId: d.dataSourceJson.id,
          rootDataSourceId: d.rootDataSourceId
        }]) as ImmutableArray<IMUseDataSource> */
      });
    }
  }


  /**
   * on feature layer field selected
   */
  onFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    /**
     * update fields in data source
     */
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.setIn(['title', 'field'], field.name),
      useDataSources: [{
        dataSourceId: this.props.useDataSources[0].dataSourceId,
        rootDataSourceId: this.props.useDataSources[0].rootDataSourceId,
        fields: [field.name]
      }]
    });
  }

  /**
   * select a feature layer
   */
  onDataSourceSelected = (jimuLayerViewInfo: IMJimuLayerViewInfo) => {
    if (!jimuLayerViewInfo) {
      this.clearMapping();
    }
    let dataSourceId = jimuLayerViewInfo ? jimuLayerViewInfo.datasourceId : null;
    let dataSource: any = this.dsManager.getDataSource(dataSourceId);
    // let schema = null;

    // if (dataSource) {
    //   schema = dataSource.getSchema();
    //   fields = schema ? (schema.fields || []) : [];
    // }
    let layer = dataSource ? dataSource.layer : null;
    if (!dataSourceId || !dataSource || !layer) {
      this.clearMapping();
    }
    this.setState({
      dsFeatureLayer: layer,
      dsFeatureLayerFields: this.getLayerFields(layer)
    });
    let rootDataSourceId = jimuLayerViewInfo ? jimuLayerViewInfo.rootDatasourceId : null;
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources:  [{
        dataSourceId: dataSourceId,
        rootDataSourceId: rootDataSourceId
      }],
      config: this.props.config.set('layerViewInfo', jimuLayerViewInfo).set('fieldQuestionMapping', null),
    }, [{
      id: `${this.props.id}-output`,
      label: `${this.props.label} Query Result`,
      type: ArcGISDataSourceTypes.FeatureLayer,
      originDataSources: [{dataSourceId: dataSourceId, rootDataSourceId: rootDataSourceId}]
    }]
  );

    // this.props.onSettingChange({
    //   id: this.props.id,
    //   useDataSources: [{
    //     dataSourceId: currentSelectedDs.dataSourceJson.id,
    //     rootDataSourceId: currentSelectedDs.rootDataSourceId
    //   }],
    // }, [{
    //   id: `${this.props.id}-output`,
    //   label: `${this.props.label} Query Result`,
    //   type: ArcGISDataSourceTypes.FeatureLayerView,
    //   originDataSources: [{dataSourceId: currentSelectedDs.dataSourceJson.id, rootDataSourceId: currentSelectedDs.rootDataSourceId}]
    // }]);
  }

  /**
   * change the layer field in field-question mapping panel
   */
  addMapppingChange = (type: string, e: any) => {
    let target = e.currentTarget;
    let value = target.value;
    let field = this.state.addMapping ? this.state.addMapping.field : null;
    let question = this.state.addMapping ? this.state.addMapping.question : null;
    if (type === 'field') {
      field = value;
    } else {
      question = value;
    }
    this.setState({
      addMapping: {
        field: field,
        question: question
      }
    });
  }

  /**
   * clear the setting of field-question mapping
   */
  clearMapping = () => {
    // todo:
  }

  onDataSourceRemoved = () => {
    // todo
  }

  /**
   * trigger evnet type change
   */
  triggerEventTypeChange = () => {

  }

  /**
   * show edit mapping panel
   */
  showEditMappingPanel = (index, e?) => {
    let curEditMapping = this.state.editMapping || {};
    if (curEditMapping.index === index) {
      return;
    }
    let mappings = this.props.config.fieldQuestionMapping || [];
    mappings = (index >= 0 && index < mappings.length) ? mappings[index] : null;
    let newMapping = Object.assign({}, mappings);
    newMapping['index'] = index;
    this.setState({
      editMapping: newMapping,
      addMapping: null
    });
  }

  /**
   * change the edit mapping 
   */
  changeEditMapping = (type: string, e: any) => {
    let target = e.currentTarget;
    let value = target.value;
    let curSetting = Object.assign({}, this.state.editMapping || {});
    if (type === 'field') {
      curSetting.field = value;
    } else {
      curSetting.question = value;
    }
    this.setState({
      editMapping: curSetting
    });
  }


  /**
   * change an field/question mapping
   */
  editMapping = (index) => {
    let mapping = [].concat(this.props.config.fieldQuestionMapping || []); 
    let curEditMapping = this.state.editMapping || {};
    if (index >= 0 && index < mapping.length) {
      mapping[index] = {
        field: curEditMapping.field,
        question: curEditMapping.question
      };
    }
    // delete mapping[field];
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('fieldQuestionMapping', mapping)
    });
  }

  /**
   * add connection for field and question
   */
  activeAddFieldQuestionConnPanel = () => {
    this.setState({
      addMapping: {
        field: null,
        question: null
      },
      editMapping: null
    });
  }

  /**
   * hide the field-question panel
   */
  deactiveAddFieldQuestionConnPanel = () => {
    this.setState({
      addMapping: null
    });
  }

  deleteConnection = (index, e?) => {
    if (e) {
      e.stopPropagation();
    }
    let mapping = [].concat(this.props.config.fieldQuestionMapping || []); 
    if (index >= 0 && index < mapping.length) {
      mapping.splice(index, 1);
    }
    // delete mapping[field];
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('fieldQuestionMapping', mapping)
    });
  }
  /**
   * add a field-question mapping
   */
  addFieldQuestionConn = (e) => {
    
    let field = this.state.addMapping.field;
    let question = this.state.addMapping.question;
    if (!field || !question) {
      console.log('Please ensure field/question is not null.');
      return;
    }

    let mapping = [].concat(this.props.config.fieldQuestionMapping || []);  
    mapping.push({
      field: field,
      question: question
    });
    // mapping[field] = question;
    
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('fieldQuestionMapping', mapping)
    });
    this.deactiveAddFieldQuestionConnPanel();
  }

  /**
   * on survey question field changed
   */
  onSurveyQuestionFieldChanged = (e: any) => {
    let target = e.currentTarget;
    let value = target.value;

    /**
     * update fields in data source
     */
    if (value && value !== 'null') {
      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('selectedSurveyQuestionFields', [value]),
      });
    }
  }

  /**
   * isDsConfigured
   */
  isDsConfigured = () => {
    if (this.props.useDataSources &&
      this.props.useDataSources.length === 1) {
      return true;
    }
    return false;
  }

  /**
   * reset survey
   */
  resetSurvey = () => {
    /**
     * reset survey item id
     */
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('surveyItemId', null).set('hides', []).set('embeds', ['fullScreen', 'onSubmitted']).set('selectedSurveyQuestionFields', [])
        .set('dsType', null).set('selectedSurvey', null)
    });

    this.setState({
      mode: 'survey-createSurvey',
      selectedSurvey: null,
      isCheckedSurveyItemId: false,
      surveyQuestionFields: [],
      dsMapView: null,
      dsFeatureLayer: null,
      dsFeatureLayerFields: []
    });
  }

  /**
   * render
   */
  render() {
    // let defaultValue = this.props.config.defaultValue;
    // const usedFieldName = this.props.useDataSources && this.props.useDataSources[0] &&
    //   this.props.useDataSources[0].fields && this.props.useDataSources[0].fields[0];
    // const selectedSurveyQuestionField = this.props.config.selectedSurveyQuestionFields && this.props.config.selectedSurveyQuestionFields[0];
    /**
     * show setting page
     */
    if (this.state.selectedSurvey && this.props.config.surveyItemId && this.state.isCheckedSurveyItemId === false) {
      this.setState({
        isCheckedSurveyItemId: true
      });
      this.showSettingPage();
    }

    /**
     * render
     */
    return <div css={getStyle(this.props.theme)} className="jimu-widget-setting survey123">
      <div className="survey123__section" style={
        (this.state.mode.indexOf('survey-') !== -1) ?
          { display: 'block' }
          : { display: 'none' }
      } >
        <SettingSection>
          <SettingRow>
            <p>{defaultMessages.chooseSurvey}</p>
          </SettingRow>
          <SettingRow>
            <Input type="radio" name="survey-survey" className="cursor-pointer" id="survey-survey-createNewSurvey" 
              checked={this.state.mode === 'survey-createSurvey'} 
              onChange={() => { this.setState({ mode: 'survey-createSurvey' }) }} />
            <Label for="survey-survey-createNewSurvey" className="cursor-pointer">&nbsp;&nbsp;{defaultMessages.createNewSurveyLabel}</Label>
          </SettingRow>
          <SettingRow>
            <Input type="radio" name="survey-survey" className="cursor-pointer" id="survey-survey-chooseExistingSurvey" 
              checked={this.state.mode === 'survey-selectExistingSurvey'} 
              onChange={() => { this.setState({ mode: 'survey-selectExistingSurvey' }) }} />
            <Label for="survey-survey-chooseExistingSurvey" className="cursor-pointer">&nbsp;&nbsp;{defaultMessages.chooseSurveyLabel}</Label>
          </SettingRow>
        </SettingSection>

        {/* select existing survey */}
        <div className="survey123__section-selectExistingSurvey" style={
          (this.state.mode === 'survey-selectExistingSurvey') ?
            { display: 'block' }
            : { display: 'none' }
        } >
          <SettingSection className="select-survey-section">
            <SettingRow>
              <Input type="select" onChange={this.onExistSurveyChanged} disabled={this.state.existSurveys.length === 0} 
                value={(this.state.selectedSurvey) ? this.state.selectedSurvey.id : 'null'} 
                style={{width: '100%', padding: '2px 5px'}}>
                <option value="null">{defaultMessages.selectSurveyTip}</option>
                {
                  (this.state.existSurveys.length > 0) ?
                    this.state.existSurveys.map((survey) => {
                      return <option value={survey.id} key={survey.id}>{survey.title}</option>
                    })
                    : ''
                }
              </Input>
              <p>{this.state.existSurveyMsg}</p>
            </SettingRow>
          </SettingSection>

          {/* survey details */}
          <div style={{
            display: (this.state.selectedSurvey) ? 'block' : 'none'
          }}>
            <SettingSection title={this.nls('surveyDetailLabel')} className="create-survey-container">
              {
                (this.state.selectedSurvey) ?
                  [
                    <SettingRow key="surveyThumnail"><img src={this.getThumbnailUrl()} style={{ width: '100%', height: 'auto' }}></img></SettingRow>,
                    <SettingRow key="surveyTitle">
                      <span style={{wordBreak: 'break-word', fontSize: '0.8125rem', color: '#FFFFFF' }}>{this.state.selectedSurvey.title}</span>
                    </SettingRow>,
                    // <SettingRow label="Tags"><p className="w-100">{this.state.selectedSurvey.tags}</p></SettingRow>,
                    // <SettingRow>{this.state.selectedSurvey.owner}</SettingRow>,
                    this.state.selectedSurvey.snippet ? 
                      <SettingRow label={this.nls('summaryLabel')} className="items" key="surveySnippet"><p className="w-100">{this.state.selectedSurvey.snippet}</p></SettingRow> 
                      : 
                      null,
                    <SettingRow key="surveyInsertBtn"><Button type="primary" className="w-100" onClick={this.setSurveyItemId}>{defaultMessages.insertLabel}</Button></SettingRow>
                  ] :
                  null
              }
            </SettingSection>
          </div>
        </div>

        {/* create survey */}
        <div className="survey123__section-createSurvey" style={
          (this.state.mode === 'survey-createSurvey') ?
            { display: 'block' }
            : { display: 'none' }
        }>
          <SettingSection>
          <div className="w-100 d-flex flex-wrap align-items-center justify-content-between setting-header setting-title pb-2">
            <div>{this.nls('surveyTitleLabel')}
              <span className="isRequired">*</span>
            </div>
            <Input className="w-100" value={this.state.newSurveyTitle || ''} name="newSurveyTitle" onChange={this.onNewSurveyValueChanged} />
            {!this.state.newSurveyTitle && this.state.newSurveyTitleDirty ?
              <div className="error-message">
                {defaultMessages.surveyTitleRequiredMsg}
              </div>
              :
              ''}
            {this.state.createSurveyErrorMsg && !this.state.newSurveyTitleDirty ?
              <div className="error-message">
                {defaultMessages.surveyTitleDuplicateMsg}
              </div>
              :
              ''}
          </div>

          <div className="w-100 d-flex flex-wrap align-items-center justify-content-between setting-header setting-title pb-2">
            <div>{this.nls('surveyTagLabel')}
              <span className="isRequired">*</span>
            </div>
            <Input className="w-100" value={this.state.newSurveyTags || ''} name="newSurveyTags" onChange={this.onNewSurveyValueChanged} />
            {!this.state.newSurveyTags && this.state.newSurveyTagsDirty ?
              <div className="error-message">
                {defaultMessages.surveyTagsRequiredMsg}
              </div>
              :
              ''}
          </div>

          <div className="w-100 d-flex flex-wrap align-items-center justify-content-between setting-header setting-title pb-2">
            <div>{this.nls('surveySummaryLabel')}</div>
            <Input className="w-100" value={this.state.newSurveySnippet || ''} name="newSurveySnippet" onChange={this.onNewSurveyValueChanged} />
          </div>

            {/* <SettingRow label="Thumbnail">
            <img src={this.state.newSurveyThumbnailUrl} style={{
              width: '50px',
              height: '50px'
            }}></img>

            <Input className="w-100" value={this.state.newSurveyThumbnailUrl} name="newSurveyThumbnailUrl" onChange={this.onNewSurveyValueChanged} />

          </SettingRow> */}
            <SettingRow flow="wrap">
              <Button type="primary" className="w-100" disabled={this.state.newSurveyLoading === true} onClick={this.createSurvey}>{defaultMessages.createSurveyBtn}</Button>
              <span style={{
                color: '#ff0000',
                marginTop: '10px'
              }}>{this.state.newSurveyMsg}</span>
              <div className="w-100" style={
                {
                  position: 'relative',
                  display: 'block',
                  marginTop: '50px'
                }
              }>
                <div className="jimu-small-loading" style={
                  (this.state.newSurveyLoading === true) ? { display: 'block' } : { display: 'none' }
                }></div>
              </div>
            </SettingRow>
          </SettingSection>
        </div>

      </div>

      {/* setting section */}
      <div className="survey123__section" style={
        (this.state.mode.indexOf('settings') !== -1) ?
          { display: 'block' }
          : { display: 'none' }
      }>
        <SettingSection title={this.nls('currentSurveyLabel')}>
          <Button className="survey123__section-resetSurvey" onClick={() => this.setState({ mode: 'confirmResetSurvey' })}><Icon size="8" icon={this.iconRefresh} /></Button>

          {/* survey title */}
          <SettingRow>
            {(this.state.selectedSurvey) ? this.state.selectedSurvey.title : ''}
          </SettingRow>

          {/* survey description */}
          <SettingRow>
            {(this.state.selectedSurvey) ? this.state.selectedSurvey.snippet : ''}
          </SettingRow>

          <SettingRow>
            <Button className="w-100" color="primary" onClick={this.editSurvey}>{defaultMessages.editSurveyBtn}</Button>
          </SettingRow>
        </SettingSection>

        <SettingSection title={this.nls('appearanceTitle')}>
          <SettingRow>
            <div className="appearance">
              <span>{defaultMessages.showOptionsBarLabel}</span>
              <Switch className="can-x-switch" checked={this.props.config.hides.indexOf('navbar') < 0}
                onChange={evt => { this.setAppearance(evt.target.checked, 'navbar') }} />
            </div>
            {/* <input value='navbar' type="checkbox" onClick={this.setAppearance} checked={this.props.config.hides.indexOf('navbar') !== -1} /> */}
          </SettingRow>
          <SettingRow>
            {/* <input value='header' type="checkbox" onClick={this.setAppearance} checked={this.props.config.hides.indexOf('header') !== -1} /> */}
            <div className="appearance">
              <span>{defaultMessages.showOptionsHeaderLabel}</span>
              <Switch className="can-x-switch" checked={this.props.config.hides.indexOf('header') < 0}
                onChange={evt => { this.setAppearance(evt.target.checked, 'header') }} />
            </div>
          </SettingRow>
          <SettingRow>
            {/* <input value='description' type="checkbox" onClick={this.setAppearance} checked={this.props.config.hides.indexOf('description') !== -1} /> */}
            <div className="appearance">
              <span>{defaultMessages.showOptionsDesLabel}</span>
              <Switch className="can-x-switch" checked={this.props.config.hides.indexOf('description') < 0}
                onChange={evt => { this.setAppearance(evt.target.checked, 'description') }} />
            </div>
          </SettingRow>
          <SettingRow>
            {/* <input value='footer' type="checkbox" onClick={this.setAppearance} checked={this.props.config.hides.indexOf('footer') !== -1} /> */}
            <div className="appearance">
              <span>{defaultMessages.showOptionsFooterLabel}</span>
              <Switch className="can-x-switch" checked={this.props.config.hides.indexOf('footer') < 0}
                onChange={evt => { this.setAppearance(evt.target.checked, 'footer') }} />
            </div>
          </SettingRow>
          <SettingRow>
            {/* <input value='theme' type="checkbox" onClick={this.setAppearance} checked={this.props.config.hides.indexOf('theme') !== -1} /> */}
            <div className="appearance">
              <span>{defaultMessages.useSurveyTheme}</span>
              <Switch className="can-x-switch" checked={this.props.config.hides.indexOf('theme') < 0}
                onChange={evt => { this.setAppearance(evt.target.checked, 'theme') }} />
            </div>
          </SettingRow>
        </SettingSection>

        {/* Send data to survey */}
        <SettingSection>
          <SettingRow>
            <div className="section-title">
              <h6>{this.nls('sendDataToSurveyTitle')}</h6>
              <Switch className="can-x-switch" checked={this.props.config.activeLinkData}
                onChange={this.activeLinkDataChange} />
            </div>
          </SettingRow>

          {this.props.config.activeLinkData && 
            <React.Fragment>
              {this.state.mapWidgetList && this.state.mapWidgetList.length > 0 ?
              <React.Fragment>
                <SettingRow>
                    <span className="w-100">{this.nls('sendDataToSurveyDesc')}</span>
                </SettingRow>
                <SettingRow>
                    <span>{this.nls('selectSourceWidget')}</span>
                </SettingRow>
                <div className="setting-row">
                  {/* <div className="sample-use-map-view-setting p-2"> */}

                  <JimuMapViewSelector onSelect={this.onMapWidgetSelected} useMapWidgetIds={this.props.useMapWidgetIds} />
                  {/* <MapSelector onSelect={this.onMapSelected} useMapWidgetIds={this.props.useMapWidgetIds}/> */}
                  {/* </div> */}
                  {/* <MapSelector onSelect={this.onMapSelected} useMapWidgetIds={this.props.useMapWidgetIds}/> */}
                </div>
                <SettingRow className="fea-layer-outter">
                  <SettingRow className="use-feature-layer-setting">
                      <span>{this.nls('selectSourceLayer')}</span>
                  </SettingRow>
                  <div className="feature-layer-dropdown">
                    <JimuLayerViewSelector onSelect={this.onDataSourceSelected} useMapWidgetIds={this.props.useMapWidgetIds} 
                      jimuLayerViewInfo={this.props.config && this.props.config.layerViewInfo}/>

                    {/* <DataSourceSelector
                      types={this.supportedLayerTypes}
                      selectedDataSourceIds={this.props.useDataSources && Immutable(this.props.useDataSources.map(ds => ds.dataSourceId))}
                      useDataSourcesEnabled={true}
                      mustUseDataSource={true}
                      onSelect={this.onDataSourceSelected} onRemove={this.onDataSourceRemoved} /> */}
                  </div>
                  {/* <div>
                    LayerName and close icon
                  </div> */}
                </SettingRow>

                <SettingRow>
                    <span className="w-100">{this.nls('triggerEventDesc1')}</span>
                    {/* <span>{this.nls('triggerEventListTitle')}</span> */}
                  </SettingRow>
                  {/* <SettingRow className="select">
                    <Input type="select" value={this.props.triggerEventType ? this.props.triggerEventType : 'eventTypeSelect'} onChange={this.triggerEventTypeChange} className="">
                      <option key="eventTypeSelect" value="eventTypeSelect">{this.nls('triggerEventSelect')}</option>
                      <option key="eventTypeAdd" value="eventTypeAdd">{this.nls('triggerEventAdd')}</option>
                    </Input>
                  </SettingRow> */}

                  {/* saved field-question mapping */}

                  <SettingRow>
                      <span>{this.nls('addConnTitle')}</span>
                  </SettingRow>

                {(this.props.useMapWidgetIds && this.props.useMapWidgetIds.length && this.props.config && this.props.config.layerViewInfo && this.props.config.layerViewInfo.datasourceId) ?  
                <React.Fragment>
                  {(this.props.config.fieldQuestionMapping && this.props.config.fieldQuestionMapping.length > 0) ? 
                    <div>{
                      this.props.config.fieldQuestionMapping.asMutable().map((mapping, index) => {
                        let field = mapping.field;
                        let question = mapping.question;
                        return <div key={field + '_' + question + '_' + index}  className="mapping-container">
                          {this.state.editMapping && this.state.editMapping.index === index ? 
                          <React.Fragment>
                              <Input type="select" value={this.state.editMapping.field || ''} onChange={(e) => this.changeEditMapping('field', e)} >
                                {this.state.dsFeatureLayerFields.map((field) => {
                                  return <option key={'edit_' + field.name + '_' + index} value={field.name}>{field.alias || field.name}</option>
                                })}
                              </Input>
                              <Input type="select" value={this.state.editMapping.question || ''} onChange={(e) => this.changeEditMapping('question', e)}>
                                {this.state.surveyQuestionFields.length > 0 &&
                                  this.state.surveyQuestionFields.map((question) => {
                                    return <option key={'edit_' + question.name + '_' + index} value={question.name}>{question.label}</option>
                                  })
                                }
                              </Input>
        
                              {/* buttons  */}
                              <div className="btn-group"> 
                                <Button className="w-25 float-left" type="primary" color="primary"
                                  onClick={() => {this.deleteConnection(index); this.setState({editMapping: null}); }}>
                                  <Icon size="16" icon={this.iconRemove} />
                                </Button>
                                <Button className="w-25 float-right" type="primary" color="primary"
                                  onClick={() => {this.setState({editMapping: null})}}>
                                  {this.nls('cancel')}
                                </Button>
                                <Button className="w-25 float-right" type="primary" color="primary" 
                                  onClick={() => {this.editMapping(index); this.setState({editMapping: null}); }}>
                                  {this.nls('ok')}
                                </Button>
                              </div>
                          </React.Fragment> 
                          : 
                          <React.Fragment>
                            <div className="link-info" onClick={(e) => this.showEditMappingPanel(index, e)}>
                              <p>{this.getFielAlias(field)}</p>
                              <div className="center-line">
                                <div className="connect">
                                  <Icon size="16" icon={this.iconLink} />
                                </div>
                              </div>
                              <p>{this.getQuestionLabel(question)}</p>
                              <div className="delete-connect" onClick={(e) => this.deleteConnection(index, e)}>
                                <Icon size="12" icon={this.iconRemove} />
                              </div>
                            </div>
                          </React.Fragment>}
                        </div>
                      })
                    }</div> : ''}

                    {/* add new field question mapping */}
                    {this.state.addMapping && this.state.dsFeatureLayer && this.state.dsFeatureLayerFields && this.state.dsFeatureLayerFields.length > 0 &&
                    <div className="mapping-container">
                      <Input type="select" value={this.state.addMapping.field || ''} onChange={(e) => this.addMapppingChange('field', e)} >
                        <option key={'add_field_default' }>{this.nls('addConnSelectField')}</option>
                        {this.state.dsFeatureLayerFields.map((field) => {
                          return <option key={'add_' + field.name} value={field.name}>{field.alias || field.name}</option>
                        })}
                      </Input>
                      <Input type="select" value={this.state.addMapping.question || ''} onChange={(e) => this.addMapppingChange('question', e)}>
                        <option key={'add_question_default' }>{this.nls('addConnSelectQuestion')}</option>
                        {this.state.surveyQuestionFields.length > 0 && 
                          this.state.surveyQuestionFields.map((question) => {
                            return <option key={'add_' + question.name} value={question.name}>{question.label}</option>
                          })
                        }
                      </Input>

                      {/* buttons  */}
                      <div className="btn-group"> 
                        <Button className="w-25 float-right" type="primary" onClick={this.deactiveAddFieldQuestionConnPanel}>{this.nls('cancel')}</Button>
                        <Button className="w-25 float-right" type="primary"
                          disabled={!this.state.addMapping.field || !this.state.addMapping.question}  
                          onClick={this.addFieldQuestionConn}>
                          {this.nls('ok')}
                        </Button>
                      </div>
                    </div>}
                  </React.Fragment> : ''
                }
                <SettingRow>
                  <Button className="w-100" type="primary"
                    // tslint:disable-next-line:max-line-length
                    disabled={this.state.addMapping !== null || this.state.editMapping !== null || !(this.props.useMapWidgetIds && this.props.useMapWidgetIds.length && this.props.config && this.props.config.layerViewInfo && this.props.config.layerViewInfo.datasourceId)} 
                    onClick={this.activeAddFieldQuestionConnPanel}>
                    {this.nls('addConnBtn')}
                  </Button>
                </SettingRow> 
              </React.Fragment> : 
                <SettingRow>
                  <span>{this.nls('sendDataDisabled1')}</span>
                </SettingRow>
            }
            </React.Fragment>}

        {/* <SettingSection title="Associated Map">
          <SettingRow>
            Set an external map which will be used for GeoPoint question instead of the built-in map.
          </SettingRow>
          <SettingRow>
            <DataSourceSelector
              types={this.supportedMapTypes}
              mustUseDataSource={true}
              onSelect={this.onMapViewSelected}
              selectedDataSourceIds={this.props.useDataSources && this.props.config.dsType === ArcGISDataSourceTypes.WebMap && Immutable(this.props.useDataSources.map(ds => ds.dataSourceId))}
              widgetId={this.props.id}
            />
          </SettingRow>
        </SettingSection> */}

      {/* <SettingSection title="Default Answers">
        <SettingRow>
          Set default answers for survey questions when the survey is opened.
        </SettingRow>
        <SettingRow>
          1. Select a field from a feature layer view:
          <DataSourceSelector
            types={this.supportedLayerTypes}
            mustUseDataSource={true}
            onSelect={this.onFeatureLayerViewSelected}
            selectedDataSourceIds={this.props.useDataSources && this.props.config.dsType === ArcGISDataSourceTypes.FeatureLayer && Immutable(this.props.useDataSources.map(ds => ds.dataSourceId))}
            widgetId={this.props.id}
          />
          {
            this.props.config.dsType === ArcGISDataSourceTypes.FeatureLayer && this.props.useDataSources && this.props.useDataSources.length > 0 &&
            <FieldSelector
              dataSources={this.props.useDataSources 
                && 
              this.props.useDataSources.asMutable().map(ds => ds.dataSourceId).map(dsId => this.dsManager && this.dsManager.getDataSource(dsId)).filter(ds => !!ds)}
              onSelect={this.onFieldSelected}
              selectedFields={this.props.useDataSources[0].fields}
            />
          }
        </SettingRow>
        <div style={
          (this.state.surveyQuestionFields.length > 0) ? { 'display': 'block' } : { 'display': 'none' }
        }>
          <SettingRow>
            2. Select a Survey Question:
          </SettingRow>
          <SettingRow>
            <select onChange={this.onSurveyQuestionFieldChanged} 
              disabled={this.state.surveyQuestionFields.length === 0} 
              value={(selectedSurveyQuestionField) ? selectedSurveyQuestionField : 'null'} 
              style={{width: '100%', padding: '5px'}}>
              <option value="null">Please select</option>
              {
                (this.state.surveyQuestionFields.length > 0) ?
                  this.state.surveyQuestionFields.map((field) => {
                    return <option value={field.name}>{field.label}</option>
                  })
                  : ''
              }
            </select>
          </SettingRow>
        </div> */}
      </SettingSection>
      </div>

      {/* confirm reset survey section */}
      <div className="survey123__section" style={
        (this.state.mode.indexOf('confirmResetSurvey') !== -1) ?
          { display: 'block' }
          : { display: 'none' }
      }>
        <SettingSection title={this.nls('resetSurveyLabel')}>
          <SettingRow>
            {defaultMessages.resetSurveyTip}
          </SettingRow>
          {defaultMessages.confirmResetSurveyTip}
          <SettingRow>

          </SettingRow>

          <SettingRow>
            <Button type="primary" onClick={() => this.resetSurvey()}>{defaultMessages.yes}</Button>
            <Button onClick={() => this.showSettingPage()}>{defaultMessages.no}</Button>
          </SettingRow>

        </SettingSection>
      </div>

      <ReactModal
        isOpen={this.state.modalIsOpen}
        className="w-100 h-100"
        style={{
          overlay: {
            zIndex: 10000
          },
          content: {
            padding: '0px',
            zIndex: 100000,
            overflow: 'hidden'
          }
        }}
      >
        <div style={{
          width: '100%',
          height: '80px',
          padding: '23px 60px 23px 30px',
          color: '#ffffff',
          backgroundColor: '#000000'
        }}>
          <p style={{
            fontSize: '24px',
            marginBottom: 0,
            lineHeight: '33px'
          }}>{defaultMessages.editSurveyBtn}</p>

          <button color="primary" onClick={this.handleCloseModal} style={{
            position: 'absolute',
            top: '28px',
            right: '30px',
            // fontSize: '30px',
            backgroundColor: '#000000',
            color: '#ffffff',
            border: '0px',
            cursor: 'pointer',
            padding: 0
          }}><Icon size="24" icon={this.closeIcon24} /></button>
        </div>

        <iframe id="survey123-designer" width="100%" style={{
          height: 'calc(100% - 90px)',
          width: '100%',
          border: '0px'
        }}></iframe>
      </ReactModal>
    </div >
  }
}
