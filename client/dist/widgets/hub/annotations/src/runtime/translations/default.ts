export default {
  "HubAnnotations.signIn": "Sign in",
  "HubAnnotations.thumbnailAlt": "thumbnail image of {name}",
  "HubAnnotations.youMustBeSignedIn": "You must be {signedIn} to add a comment.",
  "HubAnnotations.signedIn": "signed in",
  "HubAnnotations.thereAreNoComments": "There are no {status} comments{forThisFeature}.",
  "HubAnnotations.forThisFeature": " for this feature"
}
