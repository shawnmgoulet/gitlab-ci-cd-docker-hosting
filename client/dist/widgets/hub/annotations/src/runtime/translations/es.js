define({
  "HubAnnotations.signIn": "Registrarse",
  "HubAnnotations.thumbnailAlt": "imagen en miniatura de {name}",
  "HubAnnotations.youMustBeSignedIn": "Debes {signedIn} para añadir un comentario.",
  "HubAnnotations.signedIn": "iniciar sesión",
  "HubAnnotations.thereAreNoComments": "No hay {status} comentarios{forThisFeature}.",
  "HubAnnotations.forThisFeature": " para esta característica"
});
