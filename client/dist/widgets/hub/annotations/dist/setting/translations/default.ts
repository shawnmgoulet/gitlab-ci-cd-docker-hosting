export default {
  "target": "Target",
  "status": "Status of Annotations",
  "Source": "SOURCE",
  "approved": "Approved",
  "removed": "Removed",
  "pending": "Pending",
}
