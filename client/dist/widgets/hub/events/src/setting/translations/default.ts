export default {
  "upcoming": "Upcoming",
  "past": "Past",
  "cancelled": "Cancelled",
  "draft": "Draft",
  "title": "Title",
  "typeOfEvents": "Type of Events",
  "Source": "SOURCE"
}
