export default {
  "HubEvents.thereAreNoEvents": "Could not find any {type} events.",
  "pleaseConfigureADataSource": "Please configure a data source",
  "waiting": "waiting..."
}
