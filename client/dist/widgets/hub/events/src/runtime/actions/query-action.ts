import {AbstractMessageAction, MessageType, Message, getAppStore, appActions, ExtentChangeMessage} from 'jimu-core';

export default class QueryAction extends AbstractMessageAction{
  filterMessageType(messageType: MessageType, messageWidgetId?: string): boolean{
    return messageType === MessageType.ExtentChange;
  }

  filterMessage(message: Message): boolean{return true;}

  getSettingComponentUri(messageType: MessageType, messageWidgetId?: string): string {
    return "runtime/actions/query-action";
  }

  onExecute(message: Message, actionConfig?: any): Promise<boolean> | boolean{
    switch(message.type){
      case MessageType.ExtentChange:
        getAppStore().dispatch(appActions.widgetStatePropChange(this.widgetId, 'queryExtent', (message as ExtentChangeMessage).extent.toJSON()));
        break;
    }
    
    return true;
  }
}