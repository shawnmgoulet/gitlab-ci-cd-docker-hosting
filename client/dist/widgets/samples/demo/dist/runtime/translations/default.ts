export default {
  _widgetLabel: 'demo',
  widgetProperties: 'Widget Properties',
  widgetFunctions: 'Widget Functions',
  widgetName: 'widget name:',
  widgetProps: 'widget props:'
}
