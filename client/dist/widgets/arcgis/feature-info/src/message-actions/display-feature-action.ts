import { AbstractMessageAction, MessageType, Message, getAppStore, appActions,
  DataRecordsSelectionChangeMessage} from 'jimu-core';

export default class ZoomToFeatureAction extends AbstractMessageAction{
  filterMessageType(messageType: MessageType, messageWidgetId?: string): boolean{
    return messageType === MessageType.DataRecordsSelectionChange; 
  }

  filterMessage(message: Message): boolean{
    return true;
  }

  //getSettingComponentUri(messageType: MessageType, messageWidgetId?: string): string {
  //}

  onExecute(message: Message): Promise<boolean> | boolean{
    switch(message.type){
      case MessageType.DataRecordsSelectionChange:
        let dataRecordsSelectionChangeMessage = message as DataRecordsSelectionChangeMessage;
        let record;
        record = dataRecordsSelectionChangeMessage.records && dataRecordsSelectionChangeMessage.records[0];
        getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `displayFeatureActionValue.record`, record));
        break;
    }

    return Promise.resolve(true);
  }
}
