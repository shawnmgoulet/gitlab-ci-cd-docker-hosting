/** @jsx jsx */
import {Immutable, ImmutableObject, DataSourceJson, IMState, FormattedMessage, jsx, getAppStore,
  UseDataSource, SqlExpression} from 'jimu-core';
import {Switch, Input} from 'jimu-ui';
import {JimuMapViewSelector, SettingSection, SettingRow} from 'jimu-ui/setting-components';
import {DataSourceSelector, SelectedDataSourceJson, AllDataSourceTypes} from 'jimu-ui/data-source-selector';
import {BaseWidgetSetting, AllWidgetSettingProps} from 'jimu-for-builder';
import {IMConfig} from '../config';
import defaultMessages from './translations/default';
import {getStyle} from './lib/style';

interface ExtraProps{
  dsJsons: ImmutableObject<{ [dsId: string]: DataSourceJson }>;
}

export interface WidgetSettingState{
  useMapWidget: boolean;
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig> & ExtraProps, WidgetSettingState>{

  supportedDsTypes = Immutable([AllDataSourceTypes.FeatureLayer, AllDataSourceTypes.FeatureQuery]);

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      dsJsons: state.appStateInBuilder.appConfig.dataSources
    }
  }

  constructor(props){
    super(props);
    this.state = {
      useMapWidget: this.props.config.useMapWidget || false
    };
  }

  getPortUrl = (): string => {
    let portUrl = getAppStore().getState().portalUrl;
    return portUrl;
  }

  onRadioChange = (useMapWidget) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('useMapWidget', useMapWidget),
    });

    this.setState({
      useMapWidget: useMapWidget 
    });
  }

  onPropertyChange = (name, value) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(name, value),
    });
  };

  onOptionsChanged = (checked, name): void => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(name, checked),
    });
  };

  onToggleUseDataEnabled = (useDataSourcesEnabled: boolean) => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSourcesEnabled
    });
  }

  onFilterChange = (sqlExprObj: SqlExpression, dsId: string) => {
    let useDataSources = this.props.useDataSources;
    if(!useDataSources || !useDataSources[0] || useDataSources[0].dataSourceId !== dsId)return;
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: [useDataSources[0].setIn(['query', 'where'], sqlExprObj).asMutable({deep: true})]
    });
  }

  onDataSourceSelected = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs?: SelectedDataSourceJson) => {
    if(!allSelectedDss){
      return ;
    }
    const useDataSources: UseDataSource[] = allSelectedDss.map(ds => ({
      dataSourceId: ds.dataSourceJson && ds.dataSourceJson.id,
      rootDataSourceId: ds.rootDataSourceId
    }));

    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: useDataSources
    });
  }

  onDataSourceRemoved = () => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: []
    });
  }

  onMapWidgetSelected = (useMapWidgetIds: string[]) => {
    this.props.onSettingChange({
      id: this.props.id,
      useMapWidgetIds: useMapWidgetIds
    });
  }

  render(){
    let mapDsIds = [];
    if (this.props.useDataSources) {
      for (let i = 0; i < this.props.useDataSources.length; i++) {
        mapDsIds.push(this.props.useDataSources[i].dataSourceId);
      }
    }

    let filterConfig = {};
    let useDataSource = this.props.useDataSources && this.props.useDataSources[0];
    if(useDataSource && useDataSource.dataSourceId) {
      filterConfig[useDataSource.dataSourceId] = useDataSource.query && useDataSource.query.where;
    }

    let setDataContent = null;
    let dataSourceSelectorContent = null;
    let mapSelectorContent = null;
    let optionsContent = null;

    dataSourceSelectorContent = (
      <SettingSection title={this.props.intl.formatMessage({id: 'sourceLabel', defaultMessage: defaultMessages.sourceLabel})}>
      {/*<SettingRow flow="wrap">
          <div className="second-header">{this.props.intl.formatMessage({id: 'sourceDescript', defaultMessage: defaultMessages.sourceDescript})}</div>
        </SettingRow>*/}
        <SettingRow>
          <DataSourceSelector types={this.supportedDsTypes}
            useDataSourcesEnabled={true} /*onToggleUseDataEnabled={this.onToggleUseDataEnabled}*/ mustUseDataSource={true}
            selectedDataSourceIds={Immutable(mapDsIds)}
            onSelect={this.onDataSourceSelected} onRemove={this.onDataSourceRemoved}
            filterEnabled={true} /*sortEnabled={true}*/
            filterConfig={Immutable(filterConfig)}
            onFilterChange={this.onFilterChange}
          />
        </SettingRow>

        <SettingRow>
          <div className="w-100 featureInfo-options">
            <div className="featureInfo-options-item">
              <label className="second-header"><FormattedMessage id="limitGraphics" defaultMessage={defaultMessages.maxFeauresDisplayed}/></label>
              <Switch className="can-x-switch" checked={(this.props.config && this.props.config.limitGraphics) || false}
                data-key="limitGraphics" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'limitGraphics')}} />
            </div>
          </div>
        </SettingRow>

        {this.props.config.limitGraphics &&
          <SettingRow >
            <div className="d-flex w-100">
              <Input className="w-100" value={this.props.config.maxGraphics || 0} type="number" onBlur={evt => 
              {
                let value = evt.target.value;
                if(!value || value === ''){ value = '0'; }
                let valueInt = parseInt(value);
                if(valueInt < 0) valueInt = 0;
                this.onPropertyChange('maxGraphics', valueInt) 
              }
              } />
            </div>
          </SettingRow>
        }


      </SettingSection>
    );

    mapSelectorContent = (
      <SettingSection className="map-selector-section" title={this.props.intl.formatMessage({id: 'mapWidgetLabel', defaultMessage: 'map widget **'})}>
        <SettingRow>
          <div className="map-selector-descript">{this.props.intl.formatMessage({id: 'sourceDescript', defaultMessage: 'set an interactive map **'})}</div>
        </SettingRow>
        <SettingRow>
          <JimuMapViewSelector onSelect={this.onMapWidgetSelected} useMapWidgetIds={this.props.useMapWidgetIds} />
        </SettingRow>
      </SettingSection>
    );

    if(this.state.useMapWidget) {
      setDataContent = mapSelectorContent;
    } else {
      setDataContent = dataSourceSelectorContent;
    }

    return ( 
      <div css={getStyle(this.props.theme)}>
        <div className="widget-setting-featureInfo">

          {setDataContent}

          <SettingSection  title={this.props.intl.formatMessage({id: 'toolLabel', defaultMessage: defaultMessages.detailOptions})}>
            <div className="featureInfo-options-part">
              <SettingRow>
                <div className="w-100 featureInfo-options">
                  <div className="featureInfo-options-item">
                    <label><FormattedMessage id="title" defaultMessage={defaultMessages.title}/></label>
                    <Input type="checkbox" className="can-x-switch" checked={this.props.config && this.props.config.title}
                      data-key="title" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'title')}} />
                  </div>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="w-100 featureInfo-options">
                  <div className="featureInfo-options-item">
                    <label><FormattedMessage id="content" defaultMessage={defaultMessages.content}/></label>
                    <Input type="checkbox" className="can-x-switch" checked={this.props.config && this.props.config.fields}
                      data-key="content" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'fields')}} />
                  </div>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="w-100 featureInfo-options">
                  <div className="featureInfo-options-item">
                    <label><FormattedMessage id="media" defaultMessage={defaultMessages.media}/></label>
                    <Input type="checkbox" className="can-x-switch" checked={this.props.config && this.props.config.media}
                      data-key="media" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'media')}} />
                  </div>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="w-100 featureInfo-options">
                  <div className="featureInfo-options-item">
                    <label><FormattedMessage id="attachments" defaultMessage={defaultMessages.attachments}/></label>
                    <Input type="checkbox" className="can-x-switch" checked={this.props.config && this.props.config.attachments}
                      data-key="attachments" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'attachments')}} />
                  </div>
                </div>
              </SettingRow>
              <SettingRow>
                <div className="w-100 featureInfo-options">
                  <div className="featureInfo-options-item">
                    <label><FormattedMessage id="lastEditInfo" defaultMessage={defaultMessages.lastEditInfo}/></label>
                    <Input type="checkbox" className="can-x-switch" checked={(this.props.config && this.props.config.lastEditInfo) || true}
                      data-key="lastEditInfo" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'lastEditInfo')}} />
                  </div>
                </div>
              </SettingRow>
            </div>
          </SettingSection>

          <SettingSection title={this.props.intl.formatMessage({id: 'general', defaultMessage: defaultMessages.general})}>
              <label className="second-header"><FormattedMessage id="noDataMessage" defaultMessage={defaultMessages.noDataMessage}/></label>
              <Input className="w-100" type="textarea" name="text" id="noDataMessage" value={this.props.config.noDataMessage || defaultMessages.noDeataMessageDefaultText}
                onBlur={evt => {this.onPropertyChange('noDataMessage', evt.target.value)}}/>
          </SettingSection>

          {optionsContent}
        </div>
      </div>
    );
  }
}
