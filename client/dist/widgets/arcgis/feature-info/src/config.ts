import {ImmutableObject} from 'jimu-core';

export interface Config{
  limitGraphics: boolean;
  maxGraphics: number;
  useMapWidget?: boolean;
  title: boolean;
  fields: boolean;
  media: boolean;
  attachments: boolean;
  lastEditInfo: boolean;
  noDataMessage: string;
}

export type IMConfig = ImmutableObject<Config>;
