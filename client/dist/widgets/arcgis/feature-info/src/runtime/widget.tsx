/** @jsx jsx */
import { BaseWidget, jsx, AllWidgetProps, MessageManager, DataRecordsSelectionChangeMessage,
 getAppStore, appActions, DataSource, DataRecord/*, IMSqlExpression*/} from 'jimu-core';
import { JimuMapViewComponent } from 'jimu-arcgis';
import { Button, WidgetPlaceholder } from 'jimu-ui';
//import { IMDataSourceJson, IMUseDataSource } from './types/app-config';
import { IMConfig } from '../config';
import {getStyle} from './lib/style';
import defaultMessages from './translations/default';
let featureInfoIcon = require('./assets/icon.svg');
import FeatureInfo from './components/feature-info';
import DataLoader from './components/data-loader';

export enum LoadStatus {
  Pending = 'Pending',
  Fulfilled = 'Fulfilled',
  Rejected = 'Rejected'
}

export interface WidgetProps extends AllWidgetProps<IMConfig> {
}

export interface WidgetState{
  mapViewWidgetId: string;
  layerDataSourceId: string;
  selectedGraphicIndex: number;
  loadDataStatus: LoadStatus;
  sqlExprObj: any;
  maxGraphics: number;
  limitGraphics: boolean;
}

export default class Widget extends BaseWidget<WidgetProps, WidgetState>{

  public  viewFromMapWidget: __esri.MapView | __esri.SceneView;
  private graphics: __esri.Graphic[];
  private records: DataRecord[];
  private dataSource: DataSource;

  public refs: {
    mapContainer: HTMLInputElement;
  }

  constructor(props) {
    super(props);
    this.graphics = [];
    this.records = [];

    let useDataSource = this.props.useDataSources &&
                        this.props.useDataSources[0];
    let sqlExprObj = useDataSource && useDataSource.query;
    this.state = {
      mapViewWidgetId: null,
      layerDataSourceId: null,
      loadDataStatus: LoadStatus.Pending,
      selectedGraphicIndex: 0,
      sqlExprObj: sqlExprObj || null,
      maxGraphics: this.props.config.maxGraphics,
      limitGraphics: this.props.config.limitGraphics
    };
  }

  componentDidMount() {
  }

  componentDidUpdate() {
    if (this.props.mutableStateProps) {
      this.handleAction(this.props.mutableStateProps);
    }
  }

  handleAction(mutableStateProps) {
    let record = mutableStateProps.displayFeatureActionValue && mutableStateProps.displayFeatureActionValue.record;
    if (record && this.dataSource.id === record.dataSource.id) {
      let selectedGraphicIndex = this.getIndexByRecord(record); 
      if(selectedGraphicIndex > -1) {
        getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.props.id, `displayFeatureActionValue.record`, null));
        this.setState({
          selectedGraphicIndex: selectedGraphicIndex
        });
      }
    }
  }

  getIndexByRecord(record) {
    let index = -1;
    this.records.some((_record, _index) => {
      if(_record.getId() === record.getId()) {
        index = _index;
        return true;
      } else {
        return false;
      }
    });
    return index;
  }

  selectGraphic(selectedGraphicIndex) {
    //let record = this.records[this.state.selectedGraphicIndex];
    let record = this.records[selectedGraphicIndex];
    if(record && this.dataSource){
      MessageManager.getInstance().publishMessage(new DataRecordsSelectionChangeMessage(this.props.id, [record]));
      let selectedRecordIds = this.dataSource.getSelectedRecordIds();
      let recordId = record.getId();
      if(selectedRecordIds.indexOf(recordId) < 0) {
        this.dataSource.selectRecordsByIds([recordId]);
      }
    }
  }

  onPreGraphicBtnClick = () => {
    let index = this.state.selectedGraphicIndex;
    if(index > 0 ) {
      this.setState({
        selectedGraphicIndex: --index
      });
    }
    this.selectGraphic(index);
  }

  onNextGraphictBtnClick = () => {
    let index = this.state.selectedGraphicIndex;
    if(index < this.graphics.length - 1) {
      this.setState({
        selectedGraphicIndex: ++index
      });
    }
    this.selectGraphic(index);
  }

  onActiveViewChange = (view) => {
    this.viewFromMapWidget = view;
    const useMapWidget = this.props.useMapWidgetIds &&
                        this.props.useMapWidgetIds[0];
    if(view || !useMapWidget) {
      this.setState({
        mapViewWidgetId: useMapWidget
      }); 
    }
  }
  
  onDataLoading = () => {
    this.setState({ loadDataStatus: LoadStatus.Pending}); 
  }

  onDataChanged = (dataSource, results) => {
    let dataSourceId = dataSource.id;
    let useDataSource = this.props.useDataSources &&
                        this.props.useDataSources[0];
    this.dataSource = dataSource;

    let sqlExprObj = useDataSource && useDataSource.query;
    //this.graphics = results.map(result => result.graphic);
    this.graphics = results.graphics;
    this.records = results.records;
    this.selectGraphic(0);
    this.setState({
      layerDataSourceId: dataSourceId,
      selectedGraphicIndex: 0,
      sqlExprObj: sqlExprObj,
      maxGraphics: this.props.config.maxGraphics,
      limitGraphics: this.props.config.limitGraphics,
      loadDataStatus: LoadStatus.Fulfilled
    }); 
  }

  render() {
    const useMapWidget = this.props.useMapWidgetIds &&
                        this.props.useMapWidgetIds[0];
    const useDataSource = this.props.useDataSources &&
                        this.props.useDataSources[0];

    let dataSourceContent = null;
    if(this.props.config.useMapWidget) {
      dataSourceContent = (
        <JimuMapViewComponent useMapWidgetIds={this.props.useMapWidgetIds} onActiveViewChange={this.onActiveViewChange} />
      );
    } else if (useDataSource) {
      dataSourceContent = (
        <DataLoader useDataSource={useDataSource}
          limitGraphics={this.props.config.limitGraphics}
          maxGraphics={this.props.config.maxGraphics}
          onDataLoading={this.onDataLoading}
          onDataChanged={this.onDataChanged}/>
      );
    }

    let content = null;
    if(this.props.config.useMapWidget ? !useMapWidget : !useDataSource) {
      content = (
        <div className="widget-featureInfo">
          <WidgetPlaceholder icon={featureInfoIcon} message={defaultMessages._widgetLabel} widgetId={this.props.id}/>
        </div>
      );
      this.graphics = [];
    } else {
      let loadingContent = null;
      if(this.state.loadDataStatus === LoadStatus.Pending) {
        loadingContent = (
          <div style={{ position: 'absolute', left: '50%', top: '50%'}} className="jimu-small-loading"/>
        );
      }
      let navContent = null;
      if(this.graphics && this.graphics.length > 0) {
        navContent = (
          <div className="nav-section d-flex justify-content-center align-items-center">
            <Button className="nav-btn" type="tertiary" size="sm" onClick={this.onPreGraphicBtnClick}> {'<'} </Button>
              <span>{this.state.selectedGraphicIndex + 1} of {this.graphics ? this.graphics.length : ''}</span>
            <Button className="nav-btn" type="tertiary" size="sm" onClick={this.onNextGraphictBtnClick}> {'>'} </Button>
          </div>
        );
      }

      let visibleElements = {
        title: this.props.config.title,
        content: {
          fields: this.props.config.fields,
          text: this.props.config.fields,
          media: this.props.config.media,
          attachments: this.props.config.attachments
        },
        lastEditedInfo: this.props.config.lastEditInfo
      };

      let featureInfoContent;
      if(this.graphics[this.state.selectedGraphicIndex]) {
        featureInfoContent = (
          <FeatureInfo graphic={this.graphics[this.state.selectedGraphicIndex]}
                       visibleElements={visibleElements}/>
        );
      } else {
        featureInfoContent = (
          <div className="no-data-message p-4 font-weight-bold"
           dangerouslySetInnerHTML={{__html: this.props.config.noDataMessage || defaultMessages.noDeataMessageDefaultText}}>
          </div>
        );
      }

      content = (
        <div className="widget-featureInfo">
          {loadingContent}
          {navContent}
          {featureInfoContent}
          <div style={{position: 'absolute', opacity: 0}} ref="mapContainer">mapContainer</div>
          <div style={{position: 'absolute', display: 'none'}}>
            {dataSourceContent}
          </div>
        </div>
      );
    }

    return (
      <div css={getStyle(this.props.theme)} className="jimu-widget">
        {content}
      </div>
    );
  }
}
