import {React, DataSource, /*DataSourceComponent,*/ DataQueryComponent, IMUseDataSource, QueriableDataSource} from 'jimu-core';
import {loadArcGISJSAPIModules} from 'jimu-arcgis';

interface OnDataLoadingFunc {
  (): void;
}

interface OnDataChangedFunc {
  (dataSource: DataSource, graphics: __esri.Graphic[]): void;
}

interface Props {
  useDataSource: IMUseDataSource;
  limitGraphics: boolean;
  maxGraphics: number;
  onDataLoading: OnDataLoadingFunc; 
  onDataChanged: OnDataChangedFunc;
}

interface State{
  dataSourceId: string;
}

export default class DataLoader extends React.PureComponent<Props, State>{
  private dataSource: QueriableDataSource;

  constructor(props) {
    super(props);
    this.state = {
      dataSourceId: null
    };
  }

  componentDidMount() {
  }

  componentDidUpdate() {
    let sqlExprObj = this.props.useDataSource && this.props.useDataSource.query;
    if(this.props.useDataSource && this.state.dataSourceId === this.props.useDataSource.dataSourceId) {
      this.querGraphics(this.dataSource, sqlExprObj).then((results) => {
        this.props.onDataChanged(this.dataSource, results);
      });
    }
  }

  getFeatureLayer(dataSource, FeatureLayer) {
    let featureLayer;
    if(dataSource.layer) {
      return Promise.resolve(dataSource.layer);
    } else {
      if(dataSource.itemId) {
        featureLayer = new FeatureLayer({
          portalItem: {
            id: dataSource.itemId,
            portal: {
              url: dataSource.portalUrl
            }
          }
        });
      } else {
        featureLayer = new FeatureLayer({
          url: dataSource.url
        });
      }

      // Bug: js-api does not enter the when callback if there is no load method here. 
      return featureLayer.load().then(() => {
        return Promise.resolve(featureLayer);
      });

      /*
      return new Promise((resovle, reject) => {
        featureLayer.when(() => {
          console.log("when");
          resovle(featureLayer);
        }, () => {
          reject();
          console.log("when error");
        })
      });
      */
    }
  }

  querGraphics(dataSource, sqlExprObj) {
    this.props.onDataLoading();
    return loadArcGISJSAPIModules([
      //'esri/Graphic',
      'esri/tasks/support/Query',
      'esri/layers/FeatureLayer'
    ]).then(modules => {
      let Query, FeatureLayer;
      [Query, FeatureLayer] = modules;
      let featureLayer;

      return this.getFeatureLayer(dataSource, FeatureLayer).then((layer) => {
        featureLayer = layer;

        let sqlExpr = sqlExprObj && sqlExprObj.where.sql;
        let num;
        this.props.maxGraphics === 0 ? num = null : num = this.props.maxGraphics;
        num = this.props.limitGraphics ? num : null;

        let query = {
          where: sqlExpr ? sqlExpr : '1=1',
          outFields: '*',
          returnGeometry: true,
          resultRecordCount: num,
          num: num
        }
        return dataSource.query(query);
      }).then((queryResults) => {
        let records = queryResults.records;
        let graphics;
        if(dataSource.featureLayer) {
          graphics = records.map(record => record.feature);
          return {
            graphics: graphics,
            records: records
          };
        } else {
          let query = new Query();
          query.objectIds = records.map(record => record.feature.attributes[featureLayer.objectIdField]);
          query.returnGeometry = false;
          query.outFields = '*';
          return featureLayer.queryFeatures(query).then((featureSet) => {
            return {
              graphics: featureSet.features,
              records: records
            };
          });
        }

        //return records.map(record => {
        //  let graphic;
        //  if(record.feature && record.feature.declaredClass === "esri.Graphic") {
        //    graphic = record.feature;
        //  } else {
        //    let geometry = {type: 'point', ... record.feature.geometry};
        //    graphic = new Graphic({
        //      attributes: record.feature.attributes,
        //      geometry: geometry,
        //      layer: featureLayer
        //    });
        //  }
        //  return {
        //    graphic: graphic, 
        //    record: record 
        //  };
        //}); 
      });
    }).catch((e) => {
      console.warn(e);
      return {
        graphics: [],
        records: [] 
      };
    });
  }

  loadGraphics(dataSource, sqlExprObj) {
    this.props.onDataLoading();
    return loadArcGISJSAPIModules([
      'esri/layers/FeatureLayer',
      'esri/tasks/support/Query',
    ]).then(modules => {
      let FeatureLayer, Query;
      [FeatureLayer, Query] = modules;
      let query = new Query();
      let featureLayer = dataSource.layer;
      let sqlExpr = sqlExprObj && sqlExprObj.where.sql;
      let num;
      query.where = sqlExpr ? sqlExpr : '1=1';
      //query.outSpatialReference = view.spatialReference;
      query.returnGeometry = false;
      query.outFields = '*';
      this.props.maxGraphics === 0 ? num = null : num = this.props.maxGraphics;
      query.num = this.props.limitGraphics ? num : null;
      
      if(!featureLayer && dataSource.url) {
        featureLayer = new FeatureLayer({
          url: dataSource.url
        });
      }
      if(featureLayer) {
        return featureLayer.queryFeatures(query).then((featureSet) => {
          return featureSet.features;
        });
      } else {
        return [];
      }
    }).catch((e) => {
      console.warn(e);
      return [];
    });
  }

  onDataSourceCreated = (dataSource: QueriableDataSource): void => {
    this.dataSource = dataSource;
    this.setState({
      dataSourceId: this.dataSource.id
    }); 
  }

  onCreateDataSourceFailed = (error): void => {
  }

  render() {
    return (
      <DataQueryComponent useDataSource={this.props.useDataSource}
                          query={{}}
                          onDataSourceCreated={this.onDataSourceCreated}
                          onCreateDataSourceFailed={this.onCreateDataSourceFailed}/>
    );
  }
}
