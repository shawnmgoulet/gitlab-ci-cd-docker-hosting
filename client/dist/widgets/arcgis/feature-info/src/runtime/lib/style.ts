import { ThemeVariables, css, SerializedStyles } from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles {
  return css`
    overflow: auto;
    .widget-featureInfo{
      width: 100%;
      height: 100%;
      .nav-section{
        height: 40px;
        border-bottom: 1px solid #a8a8a8;
        .nav-btn:focus{
          box-shadow: none;
        }
      }
      .feature-info-component{
        background-color: ${theme.colors.palette.light[100]};
        padding: 0 7px;
        .esri-feature__size-container{
          background-color: ${theme.colors.palette.light[100]};
          color: ${theme.colors.black};
        }
        .esri-widget * {
          color: ${theme.colors.black};
        }
        .esri-feature__title{
          padding: 10px 15px 0 15px;
        }
        .esri-feature__main-container{
        }
      }



    }
  `;
}
