export default {
  sourceLabel: 'Source',
  sourceDescript: 'Select a web map/web scene, or any combination of two.',
  detailOptions: 'Details options',
  title: 'Title',
  content: 'Content',
  fields: 'Fields',
  text: 'Text',
  media: 'Media',
  attachments: 'Attachments',
  lastEditInfo: 'Last edit info',
  general: 'General',
  maxFeauresDisplayed: 'Maximum features displayed',
  noDataMessage: 'No data message',
  noDeataMessageDefaultText: 'No data found.'
}
