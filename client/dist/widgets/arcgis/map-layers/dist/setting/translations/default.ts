export default {
  sourceLabel: 'Source',
  sourceDescript: 'Select a web map/web scene, or any combination of two.',
  actions: 'Actions',
  goto: 'Zoom to',
  transparency: 'Adjust layer transparency',
  information: 'View layer details',
  options: 'Options',
  setVisibility: 'Toggle layer visibility',
  showLayerForMap: 'Show layers for map data only',
  InteractWithMap: 'Interact with a Map widget'

}
