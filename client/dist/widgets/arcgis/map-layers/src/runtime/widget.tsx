/** @jsx jsx */
import { BaseWidget, jsx, AllWidgetProps, DataSourceComponent} from 'jimu-core';
import { MapDataSource, DataSourceTypes, loadArcGISJSAPIModules, JimuMapViewComponent, JimuMapView} from 'jimu-arcgis';
import { WidgetPlaceholder } from 'jimu-ui';
//import { IMDataSourceJson, IMUseDataSource } from './types/app-config';
import { IMConfig } from '../config';
import {getStyle} from './lib/style';
import Action from './actions/action';
import Goto from './actions/goto';
import Opacity from './actions/Opacity';
import Information from './actions/information';
import defaultMessages from './translations/default';
let layerListIcon = require('./assets/icon.svg');

export enum LoadStatus {
  Pending = 'Pending',
  Fulfilled = 'Fulfilled',
  Rejected = 'Rejected'
}

export interface WidgetProps extends AllWidgetProps<IMConfig> {
  //useDataSource: IMUseDataSource;
  //dataSource: MapDataSource;
}


export interface WidgetState{
  mapViewWidgetId: string;
  viewFromMapWidgetId: string;
  mapDataSourceId: string;
  loadStatus: LoadStatus;
}

export default class Widget extends BaseWidget<WidgetProps, WidgetState>{

  public  viewFromMapWidget: __esri.MapView | __esri.SceneView;
  private dataSource: MapDataSource;
  private mapView: __esri.MapView;
  private sceneView: __esri.SceneView;
  private MapView: typeof __esri.MapView;
  private SceneView: typeof __esri.SceneView;
  private layerList: __esri.LayerList;
  private LayerList: typeof __esri.LayerList;
  private layerListActions: Action[];
  private renderPromise: Promise<void>;
  private currentUseMapWidgetId: string;
  private currentUseDataSourceId: string;

  public refs: {
    mapContainer: HTMLInputElement;
    layerListContainer: HTMLInputElement;
  }

  constructor(props) {
    super(props);
    this.state = {
      mapViewWidgetId: null,
      mapDataSourceId: null,
      viewFromMapWidgetId: null,
      loadStatus: LoadStatus.Pending
    };
    this.renderPromise = Promise.resolve();
    this.registerLayerListActions();
  }

  componentDidMount() {
  }

  componentDidUpdate() {
    if(this.props.config.useMapWidget) {
      if(this.state.mapViewWidgetId === this.currentUseMapWidgetId) {
        this.syncRenderer(this.renderPromise);
      }
    } else {
      if(this.state.mapDataSourceId === this.currentUseDataSourceId) {
        this.syncRenderer(this.renderPromise);
      }
    }
  }


  createView() {
    if(this.props.config.useMapWidget) {
      return Promise.resolve(this.viewFromMapWidget);
    } else {
      return this.createViewByDatatSource();
    }
  }

  createViewByDatatSource() {
    return this.loadViewModules(this.dataSource).then(() => {
      if (this.dataSource.type === DataSourceTypes.WebMap) {
        return new Promise( (resolve, reject) => this.createWebMapView(this.MapView, resolve, reject)) 
      } else if(this.dataSource.type === DataSourceTypes.WebScene) {
        return new Promise( (resolve, reject) => this.createSceneView(this.SceneView, resolve, reject)) 
      } else {
        return Promise.reject();
      }
    });
  }

  createWebMapView(MapView, resolve, reject) {
    this.destoryView();
    let mapViewOption: __esri.MapViewProperties;
    mapViewOption = {
      map: this.dataSource.map,
      container: this.refs.mapContainer
    };
    this.mapView = new MapView(mapViewOption);
    this.mapView.when(() => {
      resolve(this.mapView)
    }, (error) => reject(error));
  }

  createSceneView(SceneView, resolve, reject) {
    //reset map to sceneView may cause js-api throw an error.
    this.destoryView();
    let mapViewOption: __esri.MapViewProperties;
    mapViewOption = {
      map: this.dataSource.map,
      container: this.refs.mapContainer
    };
    this.sceneView = new this.SceneView(mapViewOption);

    this.sceneView.when(() => {
      resolve(this.sceneView)
    }, (error) => reject(error));
  }

  destoryView() {
    this.mapView && !this.mapView.destroyed && this.mapView.destroy();
    this.sceneView && !this.sceneView.destroyed && this.sceneView.destroy();
  }
  
  loadViewModules(dataSource: MapDataSource): Promise <typeof __esri.MapView | typeof __esri.SceneView> {
    if (dataSource.type === DataSourceTypes.WebMap) {
      if(this.MapView) { return Promise.resolve(this.MapView); };
      return loadArcGISJSAPIModules([
        'esri/views/MapView'
      ]).then(modules => {
        [
          this.MapView
        ] = modules;
        return this.MapView;
      })
    } else if(dataSource.type === DataSourceTypes.WebScene) {
      if(this.SceneView) { return Promise.resolve(this.SceneView); };
      return loadArcGISJSAPIModules([
        'esri/views/SceneView'
      ]).then(modules => {
        [
          this.SceneView
        ] = modules;
        return this.SceneView;
      });
    } else {
      return Promise.reject();
    }
  }

  destoryLayerList() {
    this.layerList && !this.layerList.destroyed && this.layerList.destroy();
  }

  createLayerList(view) {
    let layerListModulePromise;
    if(this.LayerList) {
      layerListModulePromise = Promise.resolve();
    } else {
      layerListModulePromise = loadArcGISJSAPIModules([
        'esri/widgets/LayerList'
      ]).then(modules => {
        [
          this.LayerList
        ] = modules;
      });
    }
    return layerListModulePromise.then( () => {
      let container = document && document.createElement('div');
      container.className = 'jimu-widget';
      this.refs.layerListContainer.appendChild(container);

      this.destoryLayerList();
      this.layerList = new this.LayerList({
        view: view,
        listItemCreatedFunction: this.defineLayerListActions,
        container: container
      });

      this.configLayerList();

      this.layerList.on('trigger-action', (event) => {
        this.onLayerListActionsTriggered(event);
      });
    });
  }

  registerLayerListActions() {
    this.layerListActions = [
      new Goto(this, defaultMessages.goto),
      new Opacity(this, defaultMessages.increaseOpacity, true),
      new Opacity(this, defaultMessages.decreaseOpacity, false),
      new Information(this, defaultMessages.information),
    ];
  }

  defineLayerListActions = (event) => {
    let item = event.item;
    let actionGroups = {};
    item.actionsSections = [];
    
    this.layerListActions.forEach((actionObj) => {
      //let actionIsExisted = item.actionsSections.some((section) => section.some((action) => action.id === actionObj.id));
      if(actionObj.isValid(item)) {
        let actionGroup = actionGroups[actionObj.group];
        if(!actionGroup) {
          actionGroup = [];
          actionGroups[actionObj.group] = actionGroup; 
        }

        actionGroup.push({
          id: actionObj.id,
          title: actionObj.title,
          className: actionObj.className
        })
      }
    });
    //@ts-ignore
    Object.entries(actionGroups).sort((v1, v2) => Number(v1[0]) - Number(v2[0])).forEach(([key, value]) => {
      item.actionsSections.push(value);
    });
  }

  configLayerList() {
    if(!this.props.config.setVisibility) {
      //@ts-ignore
      this.layerList._toggleVisibility = function() {};
    }
  }

  onLayerListActionsTriggered = (event) => {
    let action = event.action;
    let item = event.item;
    let actionObj = this.layerListActions.find((actionObj) => actionObj.id === action.id);
    actionObj.execute(item); 
  }


  renderLayerList() {
    return this.createView().then((view) => {
      return this.createLayerList(view);
    }).then(() => {
      this.setState({
        loadStatus: LoadStatus.Fulfilled
      })
      return;
    }).catch( (error) => console.error(error));
  }

  syncRenderer(preRenderPromise) {
    this.renderPromise = new Promise( (resolve, reject) => {
      preRenderPromise.then(() => {
        this.renderLayerList().then(() => {
          resolve();
        }).catch(() => reject());
      })
    });
  }

  onActiveViewChange = (jimuMapView: JimuMapView) => {
    const useMapWidget = this.props.useMapWidgetIds &&
                        this.props.useMapWidgetIds[0];
    if((jimuMapView && jimuMapView.view) || !useMapWidget) {
      this.viewFromMapWidget = jimuMapView && jimuMapView.view;
      this.setState({
        mapViewWidgetId: useMapWidget,
        viewFromMapWidgetId: jimuMapView.id
      }); 
    }
  }

  onDataSourceCreated = (dataSource: MapDataSource): void => {
    this.dataSource = dataSource;
    this.setState({
      mapDataSourceId: dataSource.id
    }); 
  }

  onCreateDataSourceFailed = (error): void => {
  }

  render() {
    const useMapWidget = this.props.useMapWidgetIds &&
                        this.props.useMapWidgetIds[0];
    const useDataSource = this.props.useDataSources &&
                        this.props.useDataSources[0];

    this.currentUseMapWidgetId = useMapWidget;
    this.currentUseDataSourceId = useDataSource && useDataSource.dataSourceId;

    let dataSourceContent = null;
    if(this.props.config.useMapWidget) {
      dataSourceContent = (
        <JimuMapViewComponent useMapWidgetIds={this.props.useMapWidgetIds} onActiveViewChange={this.onActiveViewChange} />
      );
    } else if (useDataSource) {
      dataSourceContent = (
        <DataSourceComponent useDataSource={useDataSource}
                             onDataSourceCreated={this.onDataSourceCreated}
                             onCreateDataSourceFailed={this.onCreateDataSourceFailed}/>
      );
    }

    let content = null;
    if(this.props.config.useMapWidget ? !useMapWidget : !useDataSource) {
      this.destoryLayerList();
      content = (
        <div className="widget-layerlist">
          <WidgetPlaceholder icon={layerListIcon} message={defaultMessages._widgetLabel} widgetId={this.props.id}/>
        </div>
      );
    } else {
      let loadingContent = null;
      if(this.state.loadStatus === LoadStatus.Pending) {
        loadingContent = (
          <div style={{ position: 'absolute', left: '50%', top: '50%'}} className="jimu-small-loading"/>
        );
      }

      content = (
        <div className="widget-layerlist">
          {loadingContent}
          <div ref="layerListContainer"></div>
          <div style={{position: 'absolute', opacity: 0}} ref="mapContainer">mapContainer</div>
          <div style={{position: 'absolute', display: 'none'}}>
            {dataSourceContent}
          </div>
        </div>
      );
    }

    return (
      <div css={getStyle(this.props.theme, this.props.config)} className="jimu-widget">
        {content}
      </div>
    );
  }
}
