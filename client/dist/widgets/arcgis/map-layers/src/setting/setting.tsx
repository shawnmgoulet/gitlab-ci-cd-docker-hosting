/** @jsx jsx */
import {React, Immutable, ImmutableObject, DataSourceJson, IMState, FormattedMessage, jsx, getAppStore,
  UseDataSource} from 'jimu-core';
import {Switch, Input, Label} from 'jimu-ui';
import {JimuMapViewSelector, SettingSection, SettingRow} from 'jimu-ui/setting-components';
import {DataSourceSelector, SelectedDataSourceJson} from 'jimu-ui/data-source-selector';
import {DataSourceTypes} from 'jimu-arcgis';
import {BaseWidgetSetting, AllWidgetSettingProps} from 'jimu-for-builder';
import {IMConfig} from '../config';
import defaultMessages from './translations/default';
import MapThumb from './components/map-thumb';
import {getStyle} from './lib/style';

interface ExtraProps{
  dsJsons: ImmutableObject<{ [dsId: string]: DataSourceJson }>;
}

export interface WidgetSettingState{
  useMapWidget: boolean;
}

export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig> & ExtraProps, WidgetSettingState>{

  supportedDsTypes = Immutable([DataSourceTypes.WebMap, DataSourceTypes.WebScene]);

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      dsJsons: state.appStateInBuilder.appConfig.dataSources
    }
  }

  constructor(props){
    super(props);
    this.state = {
      useMapWidget: this.props.config.useMapWidget || false
    };
  }

  getPortUrl = (): string => {
    let portUrl = getAppStore().getState().portalUrl;
    return portUrl;
  }

  onRadioChange = (useMapWidget) => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('useMapWidget', useMapWidget),
    });

    this.setState({
      useMapWidget: useMapWidget 
    });
  }

  onToolsChanged = (checked, name): void => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(name, checked),
    });
  };

  onOptionsChanged = (checked, name): void => {
    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set(name, checked),
    });
  };

  onToggleUseDataEnabled = (useDataSourcesEnabled: boolean) => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSourcesEnabled
    });
  }

  onDataSourceSelected = (allSelectedDss: SelectedDataSourceJson[], currentSelectedDs?: SelectedDataSourceJson) => {
    if(!allSelectedDss){
      return ;
    }
    const useDataSources: UseDataSource[] = allSelectedDss.map(ds => ({
      dataSourceId: ds.dataSourceJson && ds.dataSourceJson.id,
      rootDataSourceId: ds.rootDataSourceId
    }));

    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: useDataSources
    });
  }

  onDataSourceRemoved = () => {
    this.props.onSettingChange({
      id: this.props.id,
      useDataSources: []
    });
  }

  onMapWidgetSelected = (useMapWidgetIds: string[]) => {
    this.props.onSettingChange({
      id: this.props.id,
      useMapWidgetIds: useMapWidgetIds
    });
  }

  render(){
    const portalUrl = this.getPortUrl();

    let mapDsIds = [];
    if (this.props.useDataSources) {
      for (let i = 0; i < this.props.useDataSources.length; i++) {
        mapDsIds.push(this.props.useDataSources[i].dataSourceId);
      }
    }

    let setDataContent = null;
    let dataSourceSelectorContent = null;
    let mapSelectorContent = null;
    let actionsContent = null;
    let optionsContent = null;

    dataSourceSelectorContent = (
      <div className="data-selector-section">
        <SettingRow>
          <DataSourceSelector types={this.supportedDsTypes} selectedDataSourceIds={Immutable(mapDsIds)}
            useDataSourcesEnabled={true} /*onToggleUseDataEnabled={this.onToggleUseDataEnabled}*/ mustUseDataSource={true}
            onSelect={this.onDataSourceSelected} onRemove={this.onDataSourceRemoved}
          />
        </SettingRow>
        {portalUrl && this.props.dsJsons && this.props.useDataSources && this.props.useDataSources.length === 1 && <SettingRow>
            <div className="w-100">
              <div className="webmap-thumbnail" title={this.props.dsJsons[this.props.useDataSources[0].dataSourceId].label}>
                <MapThumb mapItemId={this.props.dsJsons[this.props.useDataSources[0].dataSourceId] ? 
                  this.props.dsJsons[this.props.useDataSources[0].dataSourceId].itemId : null} 
                  portUrl={this.props.dsJsons[this.props.useDataSources[0].dataSourceId] ? 
                    this.props.dsJsons[this.props.useDataSources[0].dataSourceId].portalUrl : null} ></MapThumb>
              </div>
            </div>
        </SettingRow>}
      </div>
    );

    mapSelectorContent = (
      <div className="map-selector-section">
        <SettingRow>
          <JimuMapViewSelector onSelect={this.onMapWidgetSelected} useMapWidgetIds={this.props.useMapWidgetIds} />
        </SettingRow>
      </div>
    );

    if(this.state.useMapWidget) {

      setDataContent = mapSelectorContent;

      actionsContent = (
        <React.Fragment>
          <SettingRow label={<FormattedMessage id="gotoLabel" defaultMessage={defaultMessages.goto}/>}>
            <Switch className="can-x-switch" checked={(this.props.config && this.props.config.goto) || false}
              data-key="goto" onChange={evt => {this.onToolsChanged(evt.target.checked, 'goto')}} />
          </SettingRow>
          <SettingRow label={<FormattedMessage id="opacityLabel" defaultMessage={defaultMessages.transparency}/>}>
            <Switch className="can-x-switch" checked={(this.props.config && this.props.config.opacity) || false}
              data-key="opacity" onChange={evt => {this.onToolsChanged(evt.target.checked, 'opacity')}} />
          </SettingRow>
        </React.Fragment>
      );


      optionsContent = (
        <SettingSection title={this.props.intl.formatMessage({id: 'options', defaultMessage: defaultMessages.options})}>
          <SettingRow label={<FormattedMessage id="setVisibilityLabel" defaultMessage={defaultMessages.setVisibility}/>}>
            <Switch className="can-x-switch" checked={(this.props.config && this.props.config.setVisibility) || false}
              data-key="setVisibility" onChange={evt => {this.onOptionsChanged(evt.target.checked, 'setVisibility')}} />
          </SettingRow>
        </SettingSection>
      );

    } else {
      setDataContent = dataSourceSelectorContent;
    }

    return ( 
      <div css={getStyle(this.props.theme)}>
        <div className="widget-setting-layerlist">
          <SettingSection title={this.props.intl.formatMessage({id: 'mapWidgetLabel', defaultMessage: defaultMessages.sourceLabel})}>
            <SettingRow>
              <div className="layerlist-tools w-100">
                <div className="w-100">
                  <div className="layerlist-tools-item radio">
                    <Input id="map-data" style={{ cursor: 'pointer' }}
                      name="map-data" onChange={e => this.onRadioChange(false)} type="radio" checked={!this.state.useMapWidget} />
                    <Label style={{ cursor: 'pointer' }} for="map-data" className="ml-1">{defaultMessages.showLayerForMap}</Label>
                  </div>
                </div>
                <div className="w-100">
                  <div className="layerlist-tools-item radio">
                    <Input id="map-view" style={{ cursor: 'pointer' }}
                      name="map-view" onChange={e => this.onRadioChange(true)} type="radio" checked={this.state.useMapWidget} />
                    <Label style={{ cursor: 'pointer' }} for="map-view" className="ml-1">{defaultMessages.InteractWithMap}</Label>
                  </div>
                </div>
              </div>
            </SettingRow>
            {setDataContent}
          </SettingSection>

          <SettingSection title={this.props.intl.formatMessage({id: 'toolLabel', defaultMessage: defaultMessages.actions})}>
              {actionsContent}
              <SettingRow label={<FormattedMessage id="information" defaultMessage={defaultMessages.information}/>}>
                <Switch className="can-x-switch" checked={(this.props.config && this.props.config.information) || false}
                  data-key="information" onChange={evt => {this.onToolsChanged(evt.target.checked, 'information')}} />
            </SettingRow>
          </SettingSection>

          {optionsContent}
        </div>
      </div>
    );
  }
}
