import { AbstractMessageAction, MessageType, Message, getAppStore, appActions, DataRecordSetCreateMessage, DataRecordSetUpdateMessage, 
  DataRecordsSelectionChangeMessage, FeatureDataRecord, ExtentChangeMessage } from 'jimu-core';
import { loadArcGISJSAPIModules } from 'jimu-arcgis'
import {handleFeature} from '../runtime/utils';
import {IMConfig} from './zoom-to-feature-action-setting';

export default class ZoomToFeatureAction extends AbstractMessageAction{
  filterMessageType(messageType: MessageType, messageWidgetId?: string): boolean{
    return messageType === MessageType.DataRecordSetCreate || messageType === MessageType.DataRecordSetUpdate 
      || messageType === MessageType.DataRecordsSelectionChange || messageType === MessageType.ExtentChange;
  }

  filterMessage(message: Message): boolean{
    return true;
  }

  getSettingComponentUri(messageType: MessageType, messageWidgetId?: string): string {
    let config = getAppStore().getState().appStateInBuilder ? getAppStore().getState().appStateInBuilder.appConfig : getAppStore().getState().appConfig;
    let messageWidgetJson = config.widgets[messageWidgetId];
    if (messageWidgetJson.manifest.label === 'Map') {
      if (messageType === MessageType.DataRecordsSelectionChange) {
        return 'message-actions/zoom-to-feature-action-setting';
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  onExecute(message: Message, actionConfig?: IMConfig): Promise<boolean> | boolean{
    return loadArcGISJSAPIModules(['esri/Graphic']).then(modules => {
      let Graphic: __esri.GraphicConstructor = null;
      [Graphic] = modules;

      switch(message.type){
        case MessageType.DataRecordSetCreate:
          let dataRecordSetCreateMessage = message as DataRecordSetCreateMessage;
  
          let newFeatureSet = {};
          if (dataRecordSetCreateMessage.dataRecordSet && dataRecordSetCreateMessage.dataRecordSet.records) {
            let features = [];
            for (let i = 0; i < dataRecordSetCreateMessage.dataRecordSet.records.length; i++) {
              if ((dataRecordSetCreateMessage.dataRecordSet.records[i] as FeatureDataRecord).feature) {
                features.push(handleFeature((dataRecordSetCreateMessage.dataRecordSet.records[i] as 
                  FeatureDataRecord).feature, Graphic).geometry);
              }
            }
  
            newFeatureSet = {
              features: features
            } as __esri.FeatureSet
          }
  
          getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `zoomToFeatureActionValue.value`, newFeatureSet));
          break;
        case MessageType.DataRecordSetUpdate:
          let dataRecordSetUpdateMessage = message as DataRecordSetUpdateMessage;
  
          let updateFeatureSet = {};
          if (dataRecordSetUpdateMessage.dataRecordSet && dataRecordSetUpdateMessage.dataRecordSet.records) {
            let features = [];
            for (let i = 0; i < dataRecordSetUpdateMessage.dataRecordSet.records.length; i++) {
              if ((dataRecordSetUpdateMessage.dataRecordSet.records[i] as FeatureDataRecord).feature) {
                features.push(handleFeature((dataRecordSetUpdateMessage.dataRecordSet.records[i] as 
                  FeatureDataRecord).feature, Graphic).geometry);
              }
            }
  
            updateFeatureSet = {
              features: features
            } as __esri.FeatureSet
          }
  
          getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `zoomToFeatureActionValue.value`, updateFeatureSet));
          break;
        case MessageType.DataRecordsSelectionChange:
          if (actionConfig) {
            let dataRecordsSelectionChangeMessage = message as DataRecordsSelectionChangeMessage;
  
            let selectionFeatureSet = {};
            let selectFeatures = [];
            if (dataRecordsSelectionChangeMessage.records) {
              if (dataRecordsSelectionChangeMessage.records[0]) {
                if (!actionConfig.useDataSource || (dataRecordsSelectionChangeMessage.records[0].dataSource.id !== actionConfig.useDataSource.dataSourceId)) {
                  break;
                }
              }

              for (let i = 0; i < dataRecordsSelectionChangeMessage.records.length; i++) {
                if ((dataRecordsSelectionChangeMessage.records[i] as FeatureDataRecord).feature) {
                  selectFeatures.push(handleFeature((dataRecordsSelectionChangeMessage.records[i] as 
                    FeatureDataRecord).feature, Graphic).geometry);
                }
              }
            }
    
            selectionFeatureSet = {
              features: selectFeatures
            }
  
            getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `zoomToFeatureActionValue.value`, selectionFeatureSet));
            break;
          } else {
            let dataRecordsSelectionChangeMessage = message as DataRecordsSelectionChangeMessage;
  
            let selectionFeatureSet = {};
            let selectFeatures = [];
            if (dataRecordsSelectionChangeMessage.records) {
              for (let i = 0; i < dataRecordsSelectionChangeMessage.records.length; i++) {
                if ((dataRecordsSelectionChangeMessage.records[i] as FeatureDataRecord).feature) {
                  selectFeatures.push(handleFeature((dataRecordsSelectionChangeMessage.records[i] as 
                    FeatureDataRecord).feature, Graphic).geometry);
                }
              }
            }
    
            selectionFeatureSet = {
              features: selectFeatures
            }
  
            getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `zoomToFeatureActionValue.value`, selectionFeatureSet));
            break;
          }
        case MessageType.ExtentChange:
          let extentChangeMessage = message as ExtentChangeMessage;
          if (extentChangeMessage.getRelatedWidgetIds().indexOf(this.widgetId) > -1) {
            break;
          }

          let extentValue = {
            features: [extentChangeMessage.extent]
          }
  
          let zoomToFeatureActionValue = {
            value: extentValue,
            relatedWidgets: extentChangeMessage.getRelatedWidgetIds()
          }
          getAppStore().dispatch(appActions.widgetMutableStatePropChange(this.widgetId, `zoomToFeatureActionValue`, zoomToFeatureActionValue));
          break;
      }
  
      return true;
    });
  }
}