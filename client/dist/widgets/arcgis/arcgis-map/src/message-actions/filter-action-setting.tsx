/** @jsx jsx */
import {React, css, jsx, ActionSettingProps, SerializedStyles, ImmutableObject, DataSource, IMDataSourceInfo,
  ReactDOM, themeUtils, ThemeVariables, polished, getAppStore, Immutable, MultipleDataSourceComponent, 
  UseDataSource, DataSourceComponent, IMUseDataSource, IMFieldSchema, IMSqlExpression, 
  dataSourceUtils, DataSourceManager} from 'jimu-core';
import {Button, Icon, Switch, Collapse} from 'jimu-ui';
import {FieldSelector} from 'jimu-ui/data-source-selector';
import {ArcGISDataSourceTypes} from 'jimu-arcgis';
import {DataSourceList, SelectedDataSourceJson, DataSourceItem} from 'jimu-ui/data-source-selector';
import { SqlExpressionBuilderPopup } from 'jimu-ui/sql-expression-builder';
import { SqlExpressionMode } from 'jimu-ui/sql-expression-runtime';

interface ExtraProps {
  theme?: ThemeVariables;
}

interface States {
  isShowLayerList: boolean;
  currentLayerType: 'trigger' | 'action';
  isSqlExprShow: boolean;
}

interface Config {
  messageUseDataSource: UseDataSource;
  actionUseDataSource: UseDataSource;
  sqlExprObj?: IMSqlExpression;

  enabledDataRelationShip?: boolean;
}

export type IMConfig = ImmutableObject<Config>;

let IconClose = require('jimu-ui/lib/icons/close.svg');
let IconDeleteX = require('jimu-ui/lib/icons/close-12.svg');

class _FilterActionSetting extends React.PureComponent<ActionSettingProps<IMConfig> & ExtraProps, States>{

  modalStyle: any = {position: 'absolute', top: '0', bottom: '0', right: '260px', left: 'auto', width: '259px',
    height: 'auto', zIndex: '3', borderRight: '', borderBottom: '', paddingBottom: '1px'};

  constructor(props){
    super(props);

    this.modalStyle.borderRight = `1px solid black`;
    this.modalStyle.borderBottom = `1px solid black`;

    this.state = {
      isShowLayerList: false,
      currentLayerType: null,
      isSqlExprShow: false
    }
  }

  openChooseLayerList = (currentLayerType: 'trigger' | 'action') => {
    this.setState({
      isShowLayerList: true,
      currentLayerType: currentLayerType
    });
  }

  closeChooseLayerList = () => {
    this.setState({
      isShowLayerList: false,
      currentLayerType: null
    });
  }

  static defaultProps = {
    config: Immutable({
      messageUseDataSource: null,
      actionUseDataSource: null,
      sqlExprObj: null,
      enabledDataRelationShip: true
    })
  }

  getInitConfig = () => {
    let messageWidgetId = this.props.messageWidgetId;
    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let messageWidgetJson = config.widgets[messageWidgetId];

    let messageUseDataSource: IMUseDataSource = null;
    let actionUseDataSource: IMUseDataSource = null;

    if (!this.props.config.messageUseDataSource) {
      if (messageWidgetJson && messageWidgetJson.useDataSources && messageWidgetJson.useDataSources[0] && messageWidgetJson.useDataSources.length === 1 ) {
        let dsJson = config.dataSources[messageWidgetJson.useDataSources[0].dataSourceId];
        if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
          messageUseDataSource = null;
        } else {
          messageUseDataSource = Immutable({
            dataSourceId: messageWidgetJson.useDataSources[0].dataSourceId,
            rootDataSourceId: messageWidgetJson.useDataSources[0].rootDataSourceId
          });
        }
      }
    } else {
      messageUseDataSource = this.props.config.messageUseDataSource;
    }

    let actionWidgetId = this.props.widgetId;
    let actionWidgetJson = config.widgets[actionWidgetId];

    if (!this.props.config.actionUseDataSource) {
      if (actionWidgetJson && actionWidgetJson.useDataSources && actionWidgetJson.useDataSources[0] && actionWidgetJson.useDataSources.length === 1 ) {
        let dsJson = config.dataSources[actionWidgetJson.useDataSources[0].dataSourceId];
        if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
          actionUseDataSource = null;
        } else {
          actionUseDataSource = Immutable({
            dataSourceId: actionWidgetJson.useDataSources[0].dataSourceId,
            rootDataSourceId: actionWidgetJson.useDataSources[0].rootDataSourceId
          });
        }
      }
    } else {
      actionUseDataSource = this.props.config.actionUseDataSource;
    }

    return {
      messageUseDataSource: messageUseDataSource,
      actionUseDataSource: actionUseDataSource
    }
  }

  componentDidMount() {
    let initConfig = this.getInitConfig();

    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', initConfig.messageUseDataSource).set('actionUseDataSource', initConfig.actionUseDataSource)
    });
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .setting-header {
        padding: ${polished.rem(10)} ${polished.rem(16)} ${polished.rem(0)} ${polished.rem(16)}
      }

      .deleteIcon {
        cursor: pointer;
        opacity: .8;
      }

      .deleteIcon:hover {
        opacity: 1;
      }

      .sql-expr-display {
        width: 100%;
        height: auto;
        min-height: 60px;
        line-height: 25px;
        padding: 3px 5px;
        color: ${theme.colors.palette.dark[300]};
        border: 1px solid ${theme.colors.palette.dark[200]};
      }
    `;
  }

  checkIsShowSetData = (widgetId: string) => {
    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let widgetJson = config.widgets[widgetId];
    if (widgetJson && widgetJson.useDataSources && widgetJson.useDataSources[0] && widgetJson.useDataSources.length === 1 ) {
      let dsJson = config.dataSources[widgetJson.useDataSources[0].dataSourceId];
      if (dsJson && ((dsJson.type === ArcGISDataSourceTypes.WebMap) || (dsJson.type === ArcGISDataSourceTypes.WebScene))) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  getContent = () => {
    let widgetId = null;
    let currentActiveUseDataSource: IMUseDataSource = null;
    if (this.state.currentLayerType === 'trigger') {
      widgetId = this.props.messageWidgetId;
      currentActiveUseDataSource = this.props.config.messageUseDataSource;
    } else if (this.state.currentLayerType === 'action') {
      widgetId = this.props.widgetId;
      currentActiveUseDataSource = this.props.config.actionUseDataSource;
    } else {
      return null;
    }

    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let widgetJson = config.widgets[widgetId];
    if (widgetJson && widgetJson.useDataSources && widgetJson.useDataSources[0]) {
      return <MultipleDataSourceComponent useDataSources={widgetJson.useDataSources}>
        {(dss: { [dataSourceId: string]: DataSource }, infos: { [dataSourceId: string]: IMDataSourceInfo}) => {
          let fromRootDsIds = Immutable(Object.keys(dss));
          let types = [ArcGISDataSourceTypes.FeatureLayer];

          let selectedDsIds = [];
          if (currentActiveUseDataSource && currentActiveUseDataSource.dataSourceId) {
            selectedDsIds.push(currentActiveUseDataSource.dataSourceId);
          }

          return <DataSourceList isDataSourceInited={true} types={Immutable(types)} fromRootDsIds={fromRootDsIds} hideHeader={true} hideTypeDropdown={true} hideAddData={true}
            onSelect={(selectedDsJson: SelectedDataSourceJson) => {this.handleChooseLayer(selectedDsJson)}} selectedDsIds={Immutable(selectedDsIds)}></DataSourceList>
        }}
      </MultipleDataSourceComponent>
    } else {
      return null;
    }
  }

  handleChooseLayer = (selectedDsJson: SelectedDataSourceJson) => {
    let useDataSource: UseDataSource = {
      dataSourceId: selectedDsJson.dataSourceJson.id,
      rootDataSourceId: selectedDsJson.rootDataSourceId
    }

    if (this.state.currentLayerType === 'trigger') {
      this.props.onSettingChange({
        actionId: this.props.actionId,
        config: this.props.config.set('messageUseDataSource', useDataSource)
      })
    }

    if (this.state.currentLayerType === 'action') {
      this.props.onSettingChange({
        actionId: this.props.actionId,
        config: this.props.config.set('actionUseDataSource', useDataSource)
      })
    }

    this.setState({
      isShowLayerList: false,
      currentLayerType: null
    });
  }

  handleRemoveLayerItemForTriggerLayer = () => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', null)
    })
  }

  handleRemoveLayerItemForActionLayer = () => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('actionUseDataSource', null)
    })
  }

  showSqlExprPopup = () => {
    this.setState({isSqlExprShow: true});
  }

  toggleSqlExprPopup = () => {
    this.setState({isSqlExprShow: !this.state.isSqlExprShow});
  }

  onSqlExprBuilderChange = (sqlExprObj: IMSqlExpression) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('sqlExprObj', sqlExprObj)
    })
  }

  onMessageFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('messageUseDataSource', {
        dataSourceId: this.props.config.messageUseDataSource.dataSourceId,
        rootDataSourceId: this.props.config.messageUseDataSource.rootDataSourceId,
        fields: [field.jimuName]
      })
    })
  }

  onActionFieldSelected = (allSelectedFields: IMFieldSchema[], field: IMFieldSchema, ds: DataSource) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('actionUseDataSource', {
        dataSourceId: this.props.config.actionUseDataSource.dataSourceId,
        rootDataSourceId: this.props.config.actionUseDataSource.rootDataSourceId,
        fields: [field.jimuName]
      })
    })
  }

  swicthEnabledDataRelationShip = (checked) => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('enabledDataRelationShip', checked)
    })
  }

  checkTrigerLayerIsSameToActionLayer = () => {
    if (this.props.config.messageUseDataSource && this.props.config.actionUseDataSource) {
      if (this.props.config.messageUseDataSource.dataSourceId === this.props.config.actionUseDataSource.dataSourceId &&
        this.props.config.messageUseDataSource.rootDataSourceId === this.props.config.actionUseDataSource.rootDataSourceId) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  render(){
    let isShowSetDataForTriggerLayer = this.checkIsShowSetData(this.props.messageWidgetId);
    let isShowSetDataForActionLayer = this.checkIsShowSetData(this.props.widgetId);

    let actionUseDataSourceInstance = this.props.config.actionUseDataSource 
      && DataSourceManager.getInstance().getDataSource(this.props.config.actionUseDataSource.dataSourceId);

    return <div css={this.getStyle(this.props.theme)} className="pt-2 pl-3 pr-3">
      <div className="w-100">
        <div className="mb-2">Trigger layer</div>
        {isShowSetDataForTriggerLayer && 
          <Button type="primary" className="w-100 mb-2" onClick={() => {this.openChooseLayerList('trigger')}}>set data</Button>}
        {this.props.config.messageUseDataSource && <LayerItem onRemovedLayerItem={() => {this.handleRemoveLayerItemForTriggerLayer()}} 
          useDataSource={this.props.config.messageUseDataSource} isHiddenCloseIcon={!isShowSetDataForTriggerLayer}></LayerItem>}
      </div>
      <div className="w-100">
        <div className="mb-2">Action layer</div>
        {isShowSetDataForActionLayer && 
          <Button type="primary" className="w-100 mb-2" onClick={() => {this.openChooseLayerList('action')}}>set data</Button>}
        {this.props.config.actionUseDataSource && <LayerItem onRemovedLayerItem={() => {this.handleRemoveLayerItemForActionLayer()}}
          useDataSource={this.props.config.actionUseDataSource} isHiddenCloseIcon={!isShowSetDataForActionLayer}></LayerItem>}
      </div>
      {this.state.isShowLayerList && <div>{ReactDOM.createPortal(
          <div className="bg-light-300 border-color-gray-400" style={this.modalStyle}>
            <div className="w-100" css={this.getStyle(this.props.theme)}>
              <div className="w-100 d-flex align-items-center justify-content-between setting-header border-0">
                <div className="setting-title mt-1" style={{maxWidth: '150px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} title={'Choose Layer'}>
                  {'Choose Layer'}
                </div>
                <div style={{cursor: 'pointer'}} onClick={this.closeChooseLayerList}><Icon icon={IconClose} size="12" className="text-dark"/></div>
              </div>
              <div>
                {this.state.currentLayerType ? this.getContent() : null }
              </div>
            </div>
          </div>
        , document.getElementById('default'))}</div>}
      <div className="w-100">
        <div className="w-100 mt-2">Conditions</div>
        <div className="w-100 d-flex justify-content-between align-items-center">
          <div>Relate message</div>
          <Switch checked={this.props.config.enabledDataRelationShip} onChange={evt => {this.swicthEnabledDataRelationShip(evt.target.checked)}}/>
        </div>
        <Collapse isOpen={this.props.config.enabledDataRelationShip}>
          {this.checkTrigerLayerIsSameToActionLayer() && <div className="w-100 border p-1 mr-2">two layers auto bind</div>}
          {!this.checkTrigerLayerIsSameToActionLayer() && <div className="w-100 border p-1 mt-2 mb-2">
            <div className="w-100">
              <div>Trigger layer Field:</div>
              {(!this.props.config.messageUseDataSource) && <div className="mt-2">none</div>}
              <div className="w-100">
                <DataSourceComponent useDataSource={this.props.config.messageUseDataSource}>
                    {
                      (ds: DataSource) => <div>
                        <FieldSelector className="w-100 mt-2"
                        dataSources={[ds]} isDataSourceDropDownHidden={true}
                        onSelect={this.onMessageFieldSelected} useDropdown={true} isSearchInputHidden={true}
                        selectedFields={this.props.config.messageUseDataSource && this.props.config.messageUseDataSource.fields
                          ? this.props.config.messageUseDataSource.fields : Immutable([])}/>
                      </div>
                    }
                  </DataSourceComponent>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center mt-2 mb-2 p-1 bg-white" style={{width: '50px', fontWeight: 'bold'}}>equals</div>
            <div className="w-100">
              <div>Action layer Field:</div>
              {(!this.props.config.actionUseDataSource) && <div className="mt-2">none</div>}
              <div className="w-100">
                <DataSourceComponent useDataSource={this.props.config.actionUseDataSource}>
                    {
                      (ds: DataSource) => <div>
                        <FieldSelector className="w-100 mt-2"
                        dataSources={[ds]} isDataSourceDropDownHidden={true}
                        onSelect={this.onActionFieldSelected} useDropdown={true} isSearchInputHidden={true}
                        selectedFields={this.props.config.actionUseDataSource && this.props.config.actionUseDataSource.fields
                          ? this.props.config.actionUseDataSource.fields : Immutable([])}/>
                      </div>
                    }
                  </DataSourceComponent>
              </div>
            </div>
          </div>}
        </Collapse>
      </div>
      <div className="w-100">
        <Button type="link" disabled={!this.props.config.actionUseDataSource} className="w-100 mb-2 d-flex justify-content-start" onClick={this.showSqlExprPopup}>More conditions</Button>
        {this.props.config.actionUseDataSource && <DataSourceComponent useDataSource={this.props.config.actionUseDataSource}>{(ds) => {
          return <SqlExpressionBuilderPopup selectedDs={ds} mode={SqlExpressionMode.Simple}
            isOpen={this.state.isSqlExprShow} toggle={this.toggleSqlExprPopup}
            config = {this.props.config.sqlExprObj} onChange={(sqlExprObj) => {this.onSqlExprBuilderChange(sqlExprObj)}}
            id="filter-widget-sql-expression-builder-popup" />
        }}</DataSourceComponent>}
        <div className="sql-expr-display">
          {this.props.config.sqlExprObj && actionUseDataSourceInstance
            ? dataSourceUtils.getArcGISSQL(this.props.config.sqlExprObj, actionUseDataSourceInstance).displaySQL 
              : 'Please set your expressions first'}
        </div>
      </div>
    </div>
  }
}

interface LayerItemProps {
  useDataSource?: IMUseDataSource;
  onRemovedLayerItem?: (useDataSource?: IMUseDataSource) => void;
  isHiddenCloseIcon?: boolean;
}

interface LayerItemStates {
  LoadError: boolean;
}

class LayerItem extends React.PureComponent<LayerItemProps, LayerItemStates>{
  
  constructor(props){
    super(props);

    this.state = {
      LoadError: false
    }
  }

  getMapLabel = () => {
    if (this.props.useDataSource && this.props.useDataSource.rootDataSourceId) {
      let datasourceinfos = getAppStore().getState().appStateInBuilder.appConfig.dataSources;
      if (datasourceinfos[this.props.useDataSource.rootDataSourceId]) {
        return datasourceinfos[this.props.useDataSource.rootDataSourceId].label;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  handleRemoveLayerItem = () => {
    this.props.onRemovedLayerItem && this.props.onRemovedLayerItem(this.props.useDataSource);
  }

  handleCreateDataSourceFailed = () => {
    this.setState({
      LoadError: true
    });
  }

  render() {
    if (this.state.LoadError) {
      return null;
    } else {
      return <DataSourceComponent useDataSource={this.props.useDataSource} onCreateDataSourceFailed={() => {this.handleCreateDataSourceFailed}}>
        {(dataSource: DataSource) => {
          return  <div className="w-100 border pr-1 pl-1 pb-1 mb-2">
            <div className="mt-2 d-flex justify-content-between align-items-center">
              <div>{this.getMapLabel()}</div>
              {!this.props.isHiddenCloseIcon && <div onClick={() => this.handleRemoveLayerItem()}>
                <Icon className="p-0 deleteIcon" width={14} height={14} icon={IconDeleteX}/>
              </div>}
              {this.props.isHiddenCloseIcon && <div></div>}
          </div>
          <DataSourceItem className="mt-2" dataSourceJson={dataSource.dataSourceJson}></DataSourceItem></div>
        }}
      </DataSourceComponent> 
    }
  }
}

export default themeUtils.withTheme(_FilterActionSetting);