/** @jsx jsx */
import {React, css, jsx, ActionSettingProps, SerializedStyles, ImmutableObject, DataSource, IMDataSourceInfo,
  ReactDOM, themeUtils, ThemeVariables, polished, getAppStore, Immutable, MultipleDataSourceComponent, UseDataSource, DataSourceComponent, IMUseDataSource} from 'jimu-core';
import {Button, Icon} from 'jimu-ui';
import {ArcGISDataSourceTypes} from 'jimu-arcgis';
import {DataSourceList, SelectedDataSourceJson, DataSourceItem} from 'jimu-ui/data-source-selector';

interface ExtraProps {
  theme?: ThemeVariables;
}

interface States {
  isShowLayerList: boolean;
}

interface Config {
  useDataSource: UseDataSource;
}

export type IMConfig = ImmutableObject<Config>;

let IconClose = require('jimu-ui/lib/icons/close.svg');
let IconDeleteX = require('jimu-ui/lib/icons/close-12.svg');

class _ZoomToFeatureActionSetting extends React.PureComponent<ActionSettingProps<IMConfig> & ExtraProps, States>{

  modalStyle: any = {position: 'absolute', top: '0', bottom: '0', right: '260px', left: 'auto', width: '259px',
    height: 'auto', zIndex: '3', borderRight: '', borderBottom: '', paddingBottom: '1px'};

  constructor(props){
    super(props);

    this.modalStyle.borderRight = `1px solid black`;
    this.modalStyle.borderBottom = `1px solid black`;

    this.state = {
      isShowLayerList: false
    }
  }

  static defaultProps = {
    config: {
      useDataSource: null
    }
  }

  componentDidMount() {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config
    });
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .setting-header {
        padding: ${polished.rem(10)} ${polished.rem(16)} ${polished.rem(0)} ${polished.rem(16)}
      }

      .deleteIcon {
        cursor: pointer;
        opacity: .8;
      }

      .deleteIcon:hover {
        opacity: 1;
      }
    `;
  }
  
  openChooseLayerList = () => {
    this.setState({
      isShowLayerList: true
    });
  }

  closeChooseLayerList = () => {
    this.setState({
      isShowLayerList: false
    });
  }

  handleChooseLayer = (selectedDsJson: SelectedDataSourceJson) => {
    let useDataSource: UseDataSource = {
      dataSourceId: selectedDsJson.dataSourceJson.id,
      rootDataSourceId: selectedDsJson.rootDataSourceId
    } 

    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('useDataSource', useDataSource)
    })

    this.setState({
      isShowLayerList: false
    });
  }

  checkIsHaveLayer = () => {
    let messageWidgetId = this.props.messageWidgetId;

    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let messageWidgetJson = config.widgets[messageWidgetId];
    return messageWidgetJson && messageWidgetJson.useDataSources && messageWidgetJson.useDataSources[0]
  }

  getContent = () => {
    let messageWidgetId = this.props.messageWidgetId;

    let config = getAppStore().getState().appStateInBuilder.appConfig;
    let messageWidgetJson = config.widgets[messageWidgetId];
    if (messageWidgetJson && messageWidgetJson.useDataSources && messageWidgetJson.useDataSources[0]) {
      return <MultipleDataSourceComponent useDataSources={messageWidgetJson.useDataSources}>
        {(dss: { [dataSourceId: string]: DataSource }, infos: { [dataSourceId: string]: IMDataSourceInfo}) => {
          let fromRootDsIds = Immutable(Object.keys(dss));
          let types = [ArcGISDataSourceTypes.FeatureLayer];

          let selectedDsIds = [];
          if (this.props.config && this.props.config.useDataSource && this.props.config.useDataSource.dataSourceId) {
            selectedDsIds.push(this.props.config.useDataSource.dataSourceId);
          }

          return <DataSourceList isDataSourceInited={true} types={Immutable(types)} fromRootDsIds={fromRootDsIds} hideHeader={true} hideTypeDropdown={true} hideAddData={true}
            onSelect={(selectedDsJson: SelectedDataSourceJson) => {this.handleChooseLayer(selectedDsJson)}} selectedDsIds={Immutable(selectedDsIds)}></DataSourceList>
        }}
      </MultipleDataSourceComponent>
    } else {
      return null;
    }
  }

  handleRemoveLayerItem = () => {
    this.props.onSettingChange({
      actionId: this.props.actionId,
      config: this.props.config.set('useDataSource', null)
    })
  }

  render(){
    return <div css={this.getStyle(this.props.theme)} className="pt-2 pl-3 pr-3">
      <div className="mt-2 mb-2">Trigger layer</div>
      <div>
        <Button type="primary" className="w-100" onClick={this.openChooseLayerList}>set data</Button>
        {this.props.config.useDataSource && <LayerItem useDataSource={this.props.config.useDataSource} onRemovedLayerItem={() => {this.handleRemoveLayerItem()}}></LayerItem>}
        {this.state.isShowLayerList && <div>{ReactDOM.createPortal(
          <div className="bg-light-300 border-color-gray-400" style={this.modalStyle}>
            <div className="w-100" css={this.getStyle(this.props.theme)}>
              <div className="w-100 d-flex align-items-center justify-content-between setting-header border-0">
                <div className="setting-title mt-1" style={{maxWidth: '150px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} title={'Choose Layer'}>
                  {'Choose Layer'}
                </div>
                <div style={{cursor: 'pointer'}} onClick={this.closeChooseLayerList}><Icon icon={IconClose} size="12" className="text-dark"/></div>
              </div>
              <div>
                {this.checkIsHaveLayer() ? this.getContent() : null }
              </div>
            </div>
          </div>
        , document.getElementById('default'))}</div>}
      </div>
    </div>;
  }
}

interface LayerItemProps {
  useDataSource?: IMUseDataSource;
  onRemovedLayerItem?: (useDataSource?: IMUseDataSource) => void;
}

interface LayerItemStates {
  LoadError: boolean;
}

class LayerItem extends React.PureComponent<LayerItemProps, LayerItemStates>{
  
  constructor(props){
    super(props);

    this.state = {
      LoadError: false
    }
  }

  getMapLabel = () => {
    if (this.props.useDataSource && this.props.useDataSource.rootDataSourceId) {
      let datasourceinfos = getAppStore().getState().appStateInBuilder.appConfig.dataSources;
      if (datasourceinfos[this.props.useDataSource.rootDataSourceId]) {
        return datasourceinfos[this.props.useDataSource.rootDataSourceId].label;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  handleRemoveLayerItem = () => {
    this.props.onRemovedLayerItem && this.props.onRemovedLayerItem(this.props.useDataSource);
  }

  handleCreateDataSourceFailed = () => {
    this.setState({
      LoadError: true
    });
  }

  render() {
    if (this.state.LoadError) {
      return null;
    } else {
      return <DataSourceComponent useDataSource={this.props.useDataSource} onCreateDataSourceFailed={() => {this.handleCreateDataSourceFailed}}>
        {(dataSource: DataSource) => {
          return  <div className="w-100">
            <div className="mt-2 d-flex justify-content-between align-items-center">
              <div>{this.getMapLabel()}</div>
              <div onClick={() => this.handleRemoveLayerItem()}>
                <Icon className="p-0 deleteIcon" width={14} height={14} icon={IconDeleteX}/>
              </div>
          </div>
          <DataSourceItem className="mt-2" dataSourceJson={dataSource.dataSourceJson}></DataSourceItem></div>
        }}
      </DataSourceComponent> 
    }
  }
}

export default themeUtils.withTheme(_ZoomToFeatureActionSetting);