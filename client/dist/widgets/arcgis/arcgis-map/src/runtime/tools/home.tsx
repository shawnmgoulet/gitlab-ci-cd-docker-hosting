import {React} from 'jimu-core';
import {BaseTool, BaseToolProps, IconType} from '../layout/base/base-tool';
import {loadArcGISJSAPIModules, JimuMapView} from 'jimu-arcgis'; 

export default class Home extends BaseTool<BaseToolProps, {}> {
  toolName = 'Home';

  constructor(props) {
    super(props);
  }

  getTitle() {
    return 'Home';
  }

  getIcon(): IconType {
    return null;
  }

  getExpandPanel(): JSX.Element {
    return <HomeInner jimuMapView={this.props.jimuMapView}></HomeInner>;
  }
}

interface HomeInnerProps {
  jimuMapView: JimuMapView;
}

interface HomeInnerState {
  apiLoaded: boolean;
}


class HomeInner extends React.PureComponent<HomeInnerProps, HomeInnerState> {
  Home: typeof __esri.Home = null;
  homeBtn: __esri.Home;
  container: HTMLElement;

  constructor(props) {
    super(props);

    this.state = {
      apiLoaded: false
    }
  }

  componentDidMount() {
    if (!this.state.apiLoaded) {
      loadArcGISJSAPIModules(['esri/widgets/Home']).then(modules => {
        [this.Home] = modules;
        this.setState({
          apiLoaded: true
        });
      })
    }
  }

  componentDidUpdate() {
    if (this.state.apiLoaded && this.container) {
      this.homeBtn = new this.Home({
        container: this.container,
        view: this.props.jimuMapView.view,
        viewpoint: (this.props.jimuMapView.view.map as __esri.WebMap | __esri.WebScene).initialViewProperties.viewpoint
      });
    }
  }

  componentWillUnmount() {
    if (this.homeBtn) {
      this.homeBtn.destroy();
      this.homeBtn = null;
    }
  }

  render() {
    return <div className="esri-widget--button" ref={ref => {this.container = ref; }}></div>;
  }


}