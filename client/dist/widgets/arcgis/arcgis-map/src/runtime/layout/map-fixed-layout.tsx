import {React, AppMode, SizeModeLayoutJson, Immutable, LayoutItemConstructorProps, LayoutItemType, WidgetType, classNames} from 'jimu-core';
import { LayoutViewer } from 'jimu-layouts/layout-runtime';
import { checkIsLive } from '../utils';

interface Props {
  appMode: AppMode;
  layouts: { [name: string]: SizeModeLayoutJson };
  LayoutEntry?: any;
  widgetManifestName: string;
}

export default class MapFixedLayout extends React.PureComponent<Props, {}>{

  constructor(props){
    super(props);
  }

  isItemAccepted = (item: LayoutItemConstructorProps, isPlaceholder: boolean): boolean => {
    if (item.itemType === LayoutItemType.Section || (item.manifest.properties.type === WidgetType.Layout)
      || (item.manifest.name === this.props.widgetManifestName)) {
      return false;
    } else {
      return true;
    }
  }

  render() {
    if (window.jimuConfig.isInBuilder) {
      let LayoutEntry = this.props.LayoutEntry;
      let layout = this.props.layouts && this.props.layouts['MapFixedLayout'];
      return <LayoutEntry layouts={layout ? layout : null} isInWidget={true} className={classNames('w-100 h-100',
        {'widget-map-usemask': !checkIsLive(this.props.appMode), 'map-is-live-mode': checkIsLive(this.props.appMode)})}
        isItemAccepted={this.isItemAccepted}>
      </LayoutEntry>
    } else {
      let layout = this.props.layouts && this.props.layouts['MapFixedLayout'];
      return <LayoutViewer layouts={layout ? Immutable(layout) : null} className="w-100 h-100 map-is-live-mode"></LayoutViewer>;
    }
  }
}