define({
  sourceLabel: 'æ_Source_____________Â',
  sourceDescript: 'æ_Select a web map/web scene, or any combination of two____________________________Â',
  initialMap: 'æ_Initial map____________Â',
  toolLabel: 'æ_Tools___________Â',
  zoomLabel: 'æ_Zoom_________Â',
  homeLabel: 'æ_Home button____________Â',
  locateLabel: 'æ_Locate button______________Â',
  scalebarLabel: 'æ_Scale bar___________________Â',
  compassLabel: 'æ_Compass_______________Â',
  toggleToPanOrRotate: 'æ_Rotate control_______________Â',
  searchLocationLabel: 'æ_Location search________________Â',
  options: 'æ_Options_______________Â',
  disableScrollZoom: 'æ_Disable scroll zoom____________________Â',
  chooseALayer: 'æ_choose a layer_______________Â',
  dataRelationShip: 'æ_Data relationship__________________Â',
  mapNone: 'æ_none_________Â',
  messageField: 'æ_Message Field______________Â'
});