export default {
  sourceLabel: 'å_Source_____________ø',
  sourceDescript: 'å_Select a web map/web scene, or any combination of two____________________________ø',
  initialMap: 'å_Initial map____________ø',
  toolLabel: 'å_Tools___________ø',
  zoomLabel: 'å_Zoom_________ø',
  homeLabel: 'å_Home button____________ø',
  locateLabel: 'å_Locate button______________ø',
  scalebarLabel: 'å_Scale bar___________________ø',
  compassLabel: 'å_Compass_______________ø',
  toggleToPanOrRotate: 'å_Rotate control_______________ø',
  searchLocationLabel: 'å_Location search________________ø',
  options: 'å_Options_______________ø',
  disableScrollZoom: 'å_Disable scroll zoom____________________ø',
  chooseALayer: 'å_choose a layer_______________ø',
  dataRelationShip: 'å_Data relationship__________________ø',
  mapNone: 'å_none_________ø',
  messageField: 'å_Message Field______________ø'
}
