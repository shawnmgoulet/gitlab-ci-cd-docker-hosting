import {ImmutableObject} from 'jimu-core';

interface ICenterAt {
  target: string | [number, number];
  zoom?: number;
}

export enum ViewMode {
  Single = 'SINGLE',
  Integrated = 'INTEGRATED'
}

export enum ExtentMode {
  Original = 'ORIGINAL',
  Custom = 'CUSTOM'
}

export interface Config{
  centerAt?: ImmutableObject<ICenterAt>;
  canPlaceHolder?: boolean;
  disableScroll?: boolean;
  disablePopUp?: boolean;
  viewMode?: ViewMode;
  placeholderImage?: string;
  uploadedImage?: string;
  extentMode?: ExtentMode;

  initialMapDataSourceID?: string;
  toolConifg: any;
  layoutIndex: number;
}

export type IMConfig = ImmutableObject<Config>;

export type ToolConfig = { [key: string]: boolean }