/** @jsx jsx */
import { React, jsx, Immutable } from 'jimu-core';
import { BaseWidgetSetting, AllWidgetSettingProps } from 'jimu-for-builder';
import { JimuMapViewSelector, SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { IMConfig, FlyStyle, RotateDirection, PathStyle } from '../config';
import { Input, Label, Icon } from 'jimu-ui';
import { getStyle, getSettingSectionStyles } from './style';
import nls from './translations/default';

let rotateIconImage = require('../assets/icons/fly-rotate.svg');
let pathIconImage = require('../assets/icons/fly-path.svg');


//State {
// onShow 
//}
// interface ExtraProps {
//   dsJsons: ImmutableObject<{ [dsId: string]: IMDataSourceJson }>;
// }
export default class Setting extends BaseWidgetSetting<AllWidgetSettingProps<IMConfig>/* & ExtraProps*/, {}>{
  //0
  onMapWidgetChange = (useMapWidgetIds: string[]) => {
    this.props.onSettingChange({
      id: this.props.id,
      useMapWidgetIds: useMapWidgetIds
    });
  }
  onInUseRadioChange = (e: React.ChangeEvent<HTMLInputElement>, idx) => {
    //show / hide
    const checked = e.currentTarget.checked;

    //return false;
    let items = this.props.config.itemsList.asMutable({ deep: true });
    var $item = Immutable(items[idx]);
    if ($item.name === FlyStyle.Rotate) {
      let item = $item.set('isInUse', checked);
      items.splice(idx, 1, item.asMutable({ deep: true }));
    } else if ($item.name === FlyStyle.Path) {
      let item = $item.set('isInUse', checked);
      items.splice(idx, 1, item.asMutable({ deep: true }));
    }

    this.props.onSettingChange({
      id: this.props.id,
      config: this.props.config.set('itemsList', items)
    });
  }

  //1 Rotate Setting
  onRotateRadioChange = (/*e: React.ChangeEvent<HTMLInputElement>, */dir, idx) => {
    //const checked = e.currentTarget.checked;
    let items = this.props.config.itemsList.asMutable({ deep: true });
    var $item = Immutable(items[idx]);
    if ($item.name === FlyStyle.Rotate) {
      let item = $item.set('direction', dir);
      items.splice(idx, 1, item.asMutable({ deep: true }));

      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('itemsList', items)
      });
    }
    // var itemsList = this.props.config.itemsList.asMutable();
    // var styleConfig = itemsList[idx].asMutable();
    // if (styleConfig.name === FlyStyle.Rotate) {
    //   styleConfig.direction = dir;

    //   this.props.onSettingChange({
    //     id: this.props.id,
    //     config: this.props.config.set('itemsList', itemsList)
    //   });
    // }
  }

  //2 Path Setting
  onPathRadioChange = (/*e: React.ChangeEvent<HTMLInputElement>, */style, idx) => {
    //const checked = e.currentTarget.checked;
    let items = this.props.config.itemsList.asMutable({ deep: true });
    var $item = Immutable(items[idx]);
    if ($item.name === FlyStyle.Path) {
      let item = $item.set('style', style);
      items.splice(idx, 1, item.asMutable({ deep: true }));

      this.props.onSettingChange({
        id: this.props.id,
        config: this.props.config.set('itemsList', items)
      });
    }
    // var itemsList = this.props.config.itemsList.asMutable({deep: true});
    // var styleConfig = itemsList[idx].asMutable({deep: true});
    // if (styleConfig.name === FlyStyle.Path) {
    //   styleConfig.style = style;

    //   this.props.onSettingChange({
    //     id: this.props.id,
    //     config: this.props.config.set('itemsList', itemsList)
    //   });
    // }
  }

  //for render
  renderRotateSetting = (styleConfig, idx) => {
    var isCW = (styleConfig.direction === RotateDirection.CW);
    var klass = getSettingSectionStyles(this.props.config.itemsList, idx);

    var label = this.props.intl.formatMessage({ id: 'styleLabelRotate', defaultMessage: nls.styleLabelRotate });
    var cw = this.props.intl.formatMessage({ id: 'CW', defaultMessage: nls.CW });
    var ccw = this.props.intl.formatMessage({ id: 'CCW', defaultMessage: nls.CCW });

    return <SettingSection className={'d-2' + klass}>
      <SettingRow>
        <Label className="flystyle-label">{label}</Label>
      </SettingRow>
      <SettingRow className="mt-2">
        <Input type="radio" checked={isCW} id="CW" style={{ cursor: 'pointer' }} onChange={e => this.onRotateRadioChange(/*e, */'CW', idx)} />
        <Label style={{ cursor: 'pointer' }} for="CW" className="ml-1">{cw}</Label>
      </SettingRow>
      <SettingRow className="mt-2">
        <Input type="radio" checked={!isCW} id="CCW" style={{ cursor: 'pointer' }} onChange={e => this.onRotateRadioChange(/*e, */'CCW', idx)} />
        <Label style={{ cursor: 'pointer' }} for="CCW" className="ml-1">{ccw}</Label>
      </SettingRow>
    </SettingSection>
  }
  renderPathSetting = (styleConfig, idx) => {
    var isCurve = (styleConfig.style === PathStyle.Smoothed);
    var klass = getSettingSectionStyles(this.props.config.itemsList, idx);

    var label = this.props.intl.formatMessage({ id: 'styleLabelPath', defaultMessage: nls.styleLabelPath });
    var smoothedCurve = this.props.intl.formatMessage({ id: 'pathTypeSmoothedCurve', defaultMessage: nls.pathTypeSmoothedCurve });
    var realPath = this.props.intl.formatMessage({ id: 'pathTypeRealPath', defaultMessage: nls.pathTypeRealPath });

    return <SettingSection className={'d-2' + klass}>
      <SettingRow>
        <Label className="flystyle-label">{label}</Label>
      </SettingRow>
      <SettingRow className="mt-2">
        <Input type="radio" checked={isCurve} id="CURVED" style={{ cursor: 'pointer' }} onChange={e => this.onPathRadioChange(/*e, */'CURVED', idx)} />
        <Label style={{ cursor: 'pointer' }} for="CURVED" className="ml-1">{smoothedCurve}</Label>
      </SettingRow>
      <SettingRow className="mt-2">
        <Input type="radio" checked={!isCurve} id="LINE" style={{ cursor: 'pointer' }} onChange={e => this.onPathRadioChange(/*e, */'LINE', idx)} />
        <Label style={{ cursor: 'pointer' }} for="LINE" className="ml-1">{realPath}</Label>
      </SettingRow>
    </SettingSection>
  }

  render() {
    var itemsList = this.props.config.itemsList.asMutable();

    var selectMapTips = this.props.intl.formatMessage({ id: 'selectMap', defaultMessage: nls.selectMap });
    var selectStyleTips = this.props.intl.formatMessage({ id: 'selectStyle', defaultMessage: nls.selectStyle });
    var flyStyleRotate = this.props.intl.formatMessage({ id: 'flyStyleRotate', defaultMessage: nls.flyStyleRotate });
    var flyStylePath = this.props.intl.formatMessage({ id: 'flyStylePath', defaultMessage: nls.flyStylePath });

    return <div css={getStyle(this.props.theme)}>
      <div className="widget-setting-fly-controller">
        <SettingSection title={selectMapTips} className="map-selector-section">
          <SettingRow>
            <JimuMapViewSelector onSelect={this.onMapWidgetChange} useMapWidgetIds={this.props.useMapWidgetIds} />
          </SettingRow>
        </SettingSection>
        <SettingSection title={selectStyleTips} >
          {itemsList.map((styleConfig, idx) => {
            const style = styleConfig.name;
            if (FlyStyle.Rotate === style) {
              return <div key={idx}>
                <SettingRow>
                  <Input type="checkbox" id="rotate-cb" checked={styleConfig.isInUse} style={{ cursor: 'pointer' }} onChange={e => this.onInUseRadioChange(e, idx)} />
                  <Label style={{ cursor: 'pointer' }} for="rotate-cb" ><Icon icon={rotateIconImage} className="m-2"></Icon>{flyStyleRotate}</Label>
                </SettingRow>

                {this.renderRotateSetting(styleConfig, idx)}
              </div>
            } else if (FlyStyle.Path === style) {
              return <div key={idx}>
                <SettingRow>
                  <Input type="checkbox" id="path-cb" checked={styleConfig.isInUse} style={{ cursor: 'pointer' }} onChange={e => this.onInUseRadioChange(e, idx)} />
                  <Label style={{ cursor: 'pointer' }} for="path-cb" ><Icon icon={pathIconImage} className="m-2"></Icon>{flyStylePath}</Label>
                </SettingRow>

                {this.renderPathSetting(styleConfig, idx)}
              </div>
            }
          })}
        </SettingSection>
      </div>
    </div>
  }
}