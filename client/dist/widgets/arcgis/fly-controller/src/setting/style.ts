import { ThemeVariables, css, SerializedStyles } from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles {
  return css`
    .widget-setting-fly-controller{
      font-weight: lighter;
      font-size: 13px;

      .flystyle-label {
        color: ${theme.colors.palette.dark[400]}
      }

      .hide {
        display: none;
      }

      /*.row-style.jimu-widget-setting--row {
      }*/

      .map-selector-section .component-map-selector .form-control{
        width: 100%;
      }
    }
  `;
}

export function getSettingSectionStyles(items, idx) {
  var needHiden = false;
  if (false === items[idx].isInUse) {
    needHiden = true;
  }

  var klass = '';
  if (needHiden) {
    klass = 'hide'
  }

  return klass;
}