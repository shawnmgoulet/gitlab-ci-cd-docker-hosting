import { ThemeVariables, css, SerializedStyles } from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles {
  //let theme = theme;
  var marginTop = '0px'; //TODO 4px for progress-bar
  return css`
    height: 100%;

    .fly-error-tips {
      display: flex;
      background-color: ${theme.colors.palette.light[500]};
      height: 100%;
      justify-content: center; 
      align-items: center;
    }

    .fly-controller{
      height: 32px;
    }

    .fly-wapper {
      position: relative;
      color: ${theme.colors.light};
      background-color: ${getBgColor(theme)}
    }

    .fly-silder {
      display:flex;
      /*height: 100%;*/
    }
    .fly-silder .progress-bar-wapper {
      display: flex;
      position: absolute;
      width: 100%;
      height: ${marginTop}
    }
    .fly-silder .items {
      display: flex;
      margin-top: ${marginTop}
    }
    .fly-silder .items .item {
      display:flex;
    }
    /*TODO hover color*/
    .fly-silder .items .btn:not(.disabled):hover,
    .fly-silder .items .dropdown-item:not(.disabled):hover,
    .fly-silder .items .dropdown-toggle:not(.disabled):hover {
      background-color: ${theme.colors.palette.dark[500]} !important;
    }
    .fly-silder .items .btn .jimu-icon{
      padding: 0;
      margin: 0;
    }

    .fly-silder .separator-line{
      border-right: 1px solid ${theme.colors.palette.light[800]};
    }
    `;
}

export function getDropdownStyle(theme: ThemeVariables) {
  return css`
  .jimu-dropdown-menu.dropdown-menu{
    background-color: ${getBgColor(theme)}
  }
  .dropdown-menu--inner {
    padding: 0;
  }
  .caret-icon.jimu-icon{
    padding: 0;
  }
  `;
}

export function getBgColor(theme: ThemeVariables) {
  return theme.colors.palette.dark[900];
}
export function getBgStyle(theme: ThemeVariables) {
  return { backgroundColor: getBgColor(theme), color: theme.colors.white }//theme.colors.palette.light[900]
}
export function getBtnStyle(theme: ThemeVariables, isActive?: boolean) {
  var style = { border: 'none' };
  if (true != isActive) {
    style = Object.assign(style, { backgroundColor: getBgColor(theme) });
  }
  return style
}
export function getIconColor(theme: ThemeVariables) {
  return theme.colors.white; //theme.colors.light[100];
}

export function getDropdownTogglerClass(isDisable) {
  var klass = '';
  if(isDisable){
    klass = 'disabled'
  }
  return klass;
}