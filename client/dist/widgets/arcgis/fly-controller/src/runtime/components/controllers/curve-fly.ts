import * as utils from '../utils/utils';
import * as SplinesUtils from '../utils/splines-utils';
import BaseFlyController from './base-fly-controller';

export default class CurvedLinesFly extends BaseFlyController {
  bsplinePoints: any;
  lastHit_gl: any;
  worldUpVec: any;
  upVec: any;

  //animat
  _frameIdx: number;
  frameTasks: Array<any>; //TODO types

  constructor(props) {
    super(props);
  }

  //=> 
  // "cameraOfFlyElevation/100*factor"(factor=1) as default speed per frame,
  //eg. factor=2 is double-speed to default, 0.5 is half-speed to default
  getSpeedByHight = (hight) => {
    //var speedPerFrame = 5;
    var factor = 0.8; //1;
    var speedPerFrame = hight / 100 * factor;
    return speedPerFrame;
  }

  prepareFly = (geo, options?) => {
    const promise = new Promise((resolve, reject) => {
      // var camera = this.sceneView.camera.clone();
      // this.cameraHeight = camera.position.z;
      // this.fixTilt = camera.tilt;
      if ('polyline' !== geo.type) {
        alert('Wrong geo type!')
      }

      this.setCameraInfo(options);
      this.cachedGeo = this.getDilutionPath(geo.paths[0]);

      this._frameIdx = -1;

      resolve();
    });
    return promise;
  }

  fly = (geo?, options?) => {
    if (super.isEnableToFly()) {
      if (-1 === this._frameIdx) {
        //var firstPoint = this.cachedGeo[0];
        //this.getPointHitTestRes(firstPoint[0], firstPoint[1], this.sceneView).then((res: HitTestRes) => {
        // if (!res) {
        //   reject();
        // }
        var iteration = 4;
        if(this.cachedGeo.length > 1000){
          iteration = 3;
        }

        this.bsplinePoints = SplinesUtils.getBspline(this.cachedGeo, iteration);
        if (this.state.DEBUG) {
          this.auxHelper.drawLineXY(this.bsplinePoints, 'lightgreen');
          /*var camRoutes = */this._getCameraRoutes(this.bsplinePoints);
        }

        var bsplinePoints = this.bsplinePoints;
        //TODO back fly
        this.frameTasks = [];
        for (let i = 0, len = bsplinePoints.length - 1; i < len; i++) {
          var start = bsplinePoints[i];
          var end = bsplinePoints[i + 1];
          var next = bsplinePoints[i + 2];

          if (!end) {
            end = start;
          }
          if (!next) {
            next = end;
          }
          // var subTask = this.getFrameTasksBySegment(start, end, next, idx);
          // this.frameTasks.push(subTask);
          var subTasks = this.getFrameTasksBySegment(start, end, next, i);
          if (subTasks && subTasks.length > 0) {
            this.frameTasks = this.frameTasks.concat(subTasks);
          }
        }
        this._frameIdx = 0;
        //resolve();
        //this.sceneView.goTo(this.cachedCamera).then(() => {
        //this.resume();
        //})
      } //else {

      this.resume();
    }
  }

  _doFly = () => {
    super._doFly();

    this.startAnimat(() => {
      //console.log("scheduling.addFrameTask");
      if (this._frameIdx < this.frameTasks.length) {
        var task = this.frameTasks[this._frameIdx];
        this._frameIdx++;

        var apiCamera = this.getTaskCamera(task);
        this.sceneView.goTo(apiCamera, { animate: false });
      } else {
        this._frameIdx = 0;
        this.stopAnimat();
      }
    });
    this.watchUtils.whenOnce(this.sceneView, 'interacting', () => {
      this.stopAnimat();
    });
  }

  getTaskCamera = (task) => {
    var pos = task.cameraGL.pos;
    var lookAt = task.cameraGL.lookAt;
    var up = task.cameraGL.up;

    //TODO !!! must turn the point on the ground
    var tmp = utils.renderCoordToGeoCoord(lookAt, 1, this.sceneView);
    var initPos = new this.Point({
      x: tmp[0],
      y: tmp[1],
      //z: tmp[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    initPos.z = this.fixHeight;
    lookAt = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);

    tmp = utils.renderCoordToGeoCoord(pos, 1, this.sceneView);
    initPos = new this.Point({
      x: tmp[0],
      y: tmp[1],
      //z: tmp[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    initPos.z = (this.cameraHeight > this.fixHeight) ? this.cameraHeight : this.fixHeight;
    pos = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);

    var glCamera = new this.GLCamera(pos, lookAt, up); //eye Pos, center Pos, up-direction
    var apiCamera = this.cameraUtils.internalToExternal(this.sceneView, glCamera);

    return apiCamera;
  }

  ///// utils ///// 
  getLinerFactor = (start, end, distance) => {
    var factor = 10;
    var vecDir = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.subtract(vecDir, end, start);
    var vecLen = this.glMatrix_vec3.length(vecDir);
    //this.glMatrix_vec3.divide
    factor = vecLen / distance;

    if (factor < 1) {
      factor = 1;
    }

    if (this.state.DEBUG) {
      console.log('factor=>' + factor);
    }
    return factor;
  }
  ///// utils ///// 

  getFrameTasksBySegment = (start, end, next, i/*, mode*/) => {
    var subTasks = [];

    if (this.glMatrix_vec3.exactEquals(start, end) || this.glMatrix_vec3.exactEquals(end, next)) {
      return subTasks; //same point, means ending
    }
    // var task = {
    //   idx: 0,
    //   cameraGL: {}
    // };
    var disPerFrame = this.getSpeedByHight(this.cameraHeight);
    //console.log("_flyP2P==>" + i /*+ "_mode_" + mode*/);
    var elevOffest = 0; //TODO elev fix tmp:500;
    var startPos_gl = utils.geoCoordToRenderCoord([start[0], start[1], start[2] + elevOffest], null, this.sceneView);
    var endPos_gl = utils.geoCoordToRenderCoord([end[0], end[1], end[2]], null, this.sceneView); //angle 

    var factor = this.getLinerFactor(start, end, disPerFrame);

    var lookAtPoint_inter_gl = utils.linerInter(endPos_gl, startPos_gl, factor);

    var camPos_gl = this.getCameraPosition(start, end);
    var camNextPos_gl = this.getCameraPosition(end, next);
    var cameraPos_inter_gl = utils.linerInter(camNextPos_gl, camPos_gl, factor);

    var worldUpVec = this.glMatrix_vec3d.create();
    this.sceneView.renderCoordsHelper.worldUpAtPosition(camPos_gl, worldUpVec); //up-aux-line


    for (let step = 0, len = factor; step < len; step++) {
      var cameraPos_gl = [0, 0, 0];
      cameraPos_gl[0] = camPos_gl[0] + cameraPos_inter_gl.x * step;
      cameraPos_gl[1] = camPos_gl[1] + cameraPos_inter_gl.y * step;
      cameraPos_gl[2] = camPos_gl[2] + cameraPos_inter_gl.z * step;
      var lookAtPos_gl = [0, 0, 0];
      lookAtPos_gl[0] = startPos_gl[0] + lookAtPoint_inter_gl.x * step;
      lookAtPos_gl[1] = startPos_gl[1] + lookAtPoint_inter_gl.y * step;
      lookAtPos_gl[2] = startPos_gl[2] + lookAtPoint_inter_gl.z * step;
      //var apiCamera = this._getAPICamera(cameraPos_gl, lookAtPos_gl, worldUpVec);
      var task = {
        idx: i, //TODO BUG
        cameraGL: {}
      };

      task.cameraGL = {
        pos: cameraPos_gl,
        lookAt: lookAtPos_gl,
        up: worldUpVec
      }

      i++;

      subTasks.push(task);
    }

    return subTasks;
  }

  //test route on the sky
  _getCameraRoutes = (line) => {
    var cameraRoutes = [];
    for (var i = 0, len = line.length - 1; i < len; i++) {
      var p1 = line[i];
      var p2;
      if (i === len - 1) {
        p2 = p1;
      } else {
        p2 = line[i + 1];
      }

      var cp = this.getCameraPosition(p1, p2);
      if (cp) {
        cameraRoutes.push(cp);
      }
    }
    if (this.state.DEBUG) {
      this.auxHelper.drawLine_gl(cameraRoutes, this.sceneView, 'yellow');
    }
    return cameraRoutes;
  }

  getCameraPosition = (basePos, nextPos) => {
    if (basePos[0] === nextPos[0] && basePos[1] === nextPos[1] && basePos[2] === nextPos[2]) {
      return null;
    }

    var cameraHeight = this.cameraHeight || 10000; //metre
    var fixDistance = this.fixDistance || 10000; //metre
    //var fixDistance = fixHeight / Math.tan(utils.degToRad(90 - this.fixTilt));

    //this.sceneView.state.isGlobal
    var innerGLCamera_copy = this.sceneView.state.camera.clone();
    var cameraPos = innerGLCamera_copy.eye;

    var worldUpVec = this.glMatrix_vec3d.create();
    this.sceneView.renderCoordsHelper.worldUpAtPosition(cameraPos, worldUpVec); //up-aux-line
    this.worldUpVec = worldUpVec;

    //1.init Pos
    var point1_gl = utils.geoCoordToRenderCoord([basePos[0], basePos[1], basePos[2]], null, this.sceneView);
    var point2_gl = utils.geoCoordToRenderCoord([nextPos[0], nextPos[1], nextPos[2]], null, this.sceneView);

    var vecDir = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.subtract(vecDir, point1_gl, point2_gl); //dir: 2->1
    var vecLen = this.glMatrix_vec3.length(vecDir);

    var vecNor = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.normalize(vecNor, vecDir);

    var tarLen = vecLen + fixDistance; //fix distance
    var tmp = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.scale(tmp, vecNor, tarLen);

    var pointTarget = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.add(pointTarget, point2_gl, tmp);

    var pMove_pos = utils.renderCoordToGeoCoord(pointTarget, 1, this.sceneView);
    var initPos = new this.Point({
      x: pMove_pos[0],
      y: pMove_pos[1],
      z: pMove_pos[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    //initPos.z = initPos.z * -1;//TODO why negate???
    if (initPos.z < cameraHeight) {
      initPos.z = cameraHeight;
    }
    if (initPos.z < 10) {//for under the ground
      initPos.z = 10;
    }
    if (this.state.DEBUG) {
      this.auxHelper.drawPoint(initPos);
    }

    var fixedCamPos_gl = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);

    //5 for test
    //var glCamera = new this.GLCamera(fixedCamPos_gl, point1_gl, worldUpVec); //camPos, lookAtPos, up-direction
    //var apiCamera = this.cameraUtils.internalToExternal(this.sceneView, glCamera);
    // this.sceneView.goTo(apiCamera/*,{animate:false}*/).then(lang.hitch(this, function () {
    //   return def.resolve();
    // }));
    return fixedCamPos_gl;
  }

  resume = () => {
    super.resume();
    this._resume(this.sceneView.goTo(this.getTaskCamera(this.frameTasks[this._frameIdx])));
  }
}