import * as utils from '../utils/utils';
import BaseFlyController from './base-fly-controller';
import { RotateDirection } from '../../../config';

export default class RotatingFly extends BaseFlyController {
  lastHit_gl: any;
  worldUpVec: any;
  cameraUpVec_gl: any;
  _lastGlCamera: any;

  direction: any;

  _totalFrame: number; //TODO

  constructor(props) {
    super(props);
  }

  prepareFly = (lastHit, options?) => {
    const promise = new Promise((resolve, reject) => {
      //var lastHit = graphic.geometry;
      if (this.state.DEBUG) {
        this.auxHelper.drawPoint(lastHit.mapPoint);
      }

      //2. aux line
      this.lastHit_gl = utils.geoCoordToRenderCoord([lastHit.mapPoint.x, lastHit.mapPoint.y, lastHit.mapPoint.z], null, this.sceneView);
      //this.lastHit_gl = utils.geoCoordToRenderCoord([lastHit.x, lastHit.y, lastHit.z], null, this.sceneView);
      this.worldUpVec = this.glMatrix_vec3d.create();
      this.sceneView.renderCoordsHelper.worldUpAtPosition(this.lastHit_gl, this.worldUpVec); //up-aux-line
      var curGlCamera_gl = utils.getCurentGLCameraInfo(this.sceneView);
      this.cameraUpVec_gl = curGlCamera_gl.up;
      var curCameraPos_gl = this.glMatrix_vec3d.fromValues(curGlCamera_gl.eye[0], curGlCamera_gl.eye[1], curGlCamera_gl.eye[2]);

      this._lastGlCamera = curCameraPos_gl;

      if (options && options.direction) {
        this.direction = options.direction;
      }

      resolve();
    });
    return promise;
  }

  fly = (lastHit?) => {
    if (super.isEnableToFly()) {
      this.resume();
    }
  }

  _doFly = () => {
    super._doFly();

    this.startAnimat(() => {
      this._totalFrame += 1;
      //console.log("scheduling.addFrameTask");
      // var camera = this.sceneView.camera.clone();
      // camera.heading += this._getFlySpeed();
      var rotateSpeed = this._getFlySpeed();
      if (this.direction && this.direction === RotateDirection.CW) {
        rotateSpeed = -rotateSpeed; //CCW default
      }

      this._lastGlCamera = this.rotateBySceneMode(this._lastGlCamera, this.worldUpVec, rotateSpeed, this.lastHit_gl);
      if (this.state.DEBUG) {
        this.auxHelper.drawPoint_gl(this._lastGlCamera, 'red');
      }

      //5. move camera
      var apiCamera = this._getAPICamera();
      this.sceneView.goTo(apiCamera, { animate: false });
    });
    this.watchUtils.whenOnce(this.sceneView, 'interacting', () => {
      this.stopAnimat();
    });
  }
  _getFlySpeed = () => {
    var speed = 0.2;
    if (!this._totalFrame) {
      this._totalFrame = 0;
    }
    return speed// + this._totalFrame/10;
  }

  _getAPICamera = () => {
    var glCamera = new this.GLCamera(this._lastGlCamera, this.lastHit_gl, this.cameraUpVec_gl); //eye Pos, center Pos, up-direction
    var apiCamera = this.cameraUtils.internalToExternal(this.sceneView, glCamera);
    return apiCamera;
  }

  resume = () => {
    super.resume();
    this._resume(this.sceneView.goTo(this._getAPICamera()));
  }

  // _doFly_byTime = () => {
  //   this._FLY_STATE = "running";
  //   //1. curent glCamera
  //   if (this._cameraMoveH) {
  //     clearInterval(this._cameraMoveH);
  //     this._cameraMoveH = null;
  //   }

  //   this._cameraMoveH = setInterval(() => {
  //     var rotateSpeed = 0.5;//in rad

  //     //4. rotate cameraPos via m-rotated-matrix
  //     var mMatrix = this.glMatrix_mat4d.create();
  //     this.glMatrix_mat4.rotate(mMatrix, mMatrix, utils.degToRad(rotateSpeed), this.worldUpVec);
  //     this.glMatrix_vec3.transformMat4(this._lastGlCamera, this._lastGlCamera, mMatrix);

  //     //5. move camera
  //     var apiCamera = this._getAPICamera();

  //     //this._lastApiCamera = apiCamera;
  //     if (this._FLY_STATE === "pause") {
  //       if (this._cameraMoveH) {
  //         clearInterval(this._cameraMoveH);
  //         this._cameraMoveH = null;
  //       }

  //     } else {
  //       this.sceneView.goTo(apiCamera, { animate: false });//animate: false ==> No collision-test 
  //       //console.log("goto in interval==>"+ this._lastApiCamera.heading + "___"+this._lastApiCamera.tilt);
  //     }
  //   }, 10);
  // }
}