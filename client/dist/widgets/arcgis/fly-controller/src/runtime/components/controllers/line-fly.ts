import { loadArcGISJSAPIModules } from 'jimu-arcgis';
import * as utils from '../utils/utils';
import * as SplinesUtils from '../utils/splines-utils';
import BaseFlyController from './base-fly-controller';

export default class LineFly extends BaseFlyController {
  esriMathUtils: any;
  //bsplinePoints: any;
  lastHit_gl: any;
  worldUpVec: any;
  upVec: any;

  //animat
  _frameIdx: number;
  frameTasks: Array<any>; //TODO types

  constructor(props) {
    super(props);

    loadArcGISJSAPIModules([
      'esri/views/3d/support/mathUtils'
    ]).then(modules => {
      [
        this.esriMathUtils
      ] = modules;
    });
  }

  prepareFly = (geo, options?) => {
    const promise = new Promise((resolve, reject) => {
      // var camera = this.sceneView.camera.clone();
      // this.cameraHeight = camera.position.z;
      // this.fixTilt = camera.tilt;
      this.setCameraInfo(options);

      if ('polyline' !== geo.type) {
        alert('Wrong geo type!')
      }
      //var paths = geo.paths[0]; //format
      // if (this.state.DEBUG) {
      //   this.auxHelper.drawLineXY(paths, 'lightgreen');
      // }
      this.setCameraInfo(options);

      this.cachedGeo = this.getDilutionPath(geo.paths[0]);
      this._frameIdx = -1;

      resolve();
    });
    return promise;
  }

  fly = (geo?, options?) => {
    if (super.isEnableToFly()) {
      if (-1 === this._frameIdx) {
        var paths = SplinesUtils.normalPoints(this.cachedGeo);
        //var firstPoint = paths[0];
        // this.getPointHitTestRes(firstPoint[0], firstPoint[1], this.sceneView).then((res: HitTestRes) => {
        //   if (!res) {
        //     reject();
        //   }
        //   this.fixHeight = res.z;
        //   this.fixDistance = res.dis;
        this.frameTasks = this.getFrameTasksBySegment(paths);
        this._frameIdx = 0;
        //});
      }

      this.resume();
    }
  }

  _doFly = () => {
    super._doFly();

    this.startAnimat(() => {
      //console.log("scheduling.addFrameTask");
      if (this._frameIdx < this.frameTasks.length) {
        var task = this.frameTasks[this._frameIdx];
        this._frameIdx++;

        var apiCamera = this.getTaskCamera(task);
        this.sceneView.goTo(apiCamera, { animate: false });
      } else {
        this._frameIdx = 0;
        this.stopAnimat();
      }
    });
    this.watchUtils.whenOnce(this.sceneView, 'interacting', () => {
      this.stopAnimat();
    });
  }

  getTaskCamera = (task) => {
    var pos = task.cameraGL.pos;
    var lookAt = task.cameraGL.lookAt;
    var up = task.cameraGL.up;

    //TODO !!! must turn the point on the ground
    var tmp = utils.renderCoordToGeoCoord(lookAt, 1, this.sceneView);
    var initPos = new this.Point({
      x: tmp[0],
      y: tmp[1],
      //z: tmp[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    initPos.z = this.fixHeight;
    lookAt = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);

    tmp = utils.renderCoordToGeoCoord(pos, 1, this.sceneView);
    initPos = new this.Point({
      x: tmp[0],
      y: tmp[1],
      //z: tmp[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    initPos.z = (this.cameraHeight > this.fixHeight) ? this.cameraHeight : this.fixHeight;
    pos = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);

    if (this.state.DEBUG) {
      this.auxHelper.drawLine_gl([pos, lookAt], this.sceneView, 'yellow');
    }

    var glCamera = new this.GLCamera(pos, lookAt, up); //eye Pos, center Pos, up-direction
    var apiCamera = this.cameraUtils.internalToExternal(this.sceneView, glCamera);

    return apiCamera;
  }

  _setAnimatObj(camPos, lookAtPos, upVec, type) {
    if (!upVec) {
      upVec = this.glMatrix_vec3d.create();
      this.sceneView.renderCoordsHelper.worldUpAtPosition(lookAtPos, upVec); //up-aux-line
    }

    var task = {
      //idx: i,//TODO BUG
      cameraGL: {},
      type: type || ''
    };

    task.cameraGL = {
      pos: camPos,
      lookAt: lookAtPos,
      up: upVec
    }

    return task;
  }

  getFrameTasksBySegment(line) {
    var tasks = []; //{camPos:,lookAtPos:,upVec,type:}
    if (line.length < 2) {
      alert('line.length < 2');
    }

    var CAM_POS_gl; //cache cam-pos
    for (var i = 0, len = line.length; i < len - 1; i++) {
      //0
      if (0 === i) {
        CAM_POS_gl = this._getInitLintFlyCamPos(line[0], line[1]);
      }

      //1
      var point1_gl = utils.geoCoordToRenderCoord([line[i + 0][0], line[i + 0][1], line[i + 0][2]], null, this.sceneView);
      var point2_gl = utils.geoCoordToRenderCoord([line[i + 1][0], line[i + 1][1], line[i + 1][2]], null, this.sceneView);
      var subVecInLine = this.glMatrix_vec3d.create();
      this.glMatrix_vec3.subtract(subVecInLine, point2_gl, point1_gl);
      var camNextPos = this.glMatrix_vec3d.create();
      this.glMatrix_vec3.add(camNextPos, subVecInLine, CAM_POS_gl);
      //this.aux_drawLine_gl([CAM_POS_gl, camNextPos], this.sceneView, "yellow");
      var linePoints = this._getLineInerPos(CAM_POS_gl, camNextPos, point1_gl, point2_gl);
      if (linePoints.length > 0) {
        tasks = tasks.concat(linePoints);
      }

      //2
      var base = line[i + 0];
      var next = line[i + 1] || null;
      var next2 = line[i + 2] || null;
      var rotatedPoints = this._getRotatedCameraPos(base, next, next2, camNextPos);
      if (rotatedPoints.length > 0) {
        tasks = tasks.concat(rotatedPoints);
      }
      //this.aux_drawLine_gl(rotatedPoints, this.sceneView, "yellow");
      if (rotatedPoints.length > 1) {
        //CAM_POS_gl = rotatedPoints[rotatedPoints.length - 1].camPos;
        CAM_POS_gl = rotatedPoints[rotatedPoints.length - 1].cameraGL.pos
      }
    }
    tasks.push(this._setAnimatObj(CAM_POS_gl, point2_gl, null, 'line')); //TODO last point bug

    //print tasks points
    if (this.state.DEBUG) {
      for (var i = 0, lenP = tasks.length; i < lenP; i++) {
        var obj = tasks[i];
        var p = obj.cameraGL.pos;

        var color = 'darkgreen';
        if (obj.type !== 'line') {
          color = 'orange';
        }
        //this.aux_drawPoint_gl(p, color);
        this.auxHelper.drawPoint_gl(p, color);
      }
    }

    return tasks;
  }

  _getInitLintFlyCamPos(basePos, nextPos) {
    var cameraHeight = this.cameraHeight || 10000; //metre
    var fixDistance = this.fixDistance || 10000; //metre
    // var fixTilt = Math.atan(fixHeight / fixDistance);
    //var fixDistance = cameraHeight / Math.tan(utils.degToRad(90 - this.fixTilt));

    var innerGLCamera_copy = this.sceneView.state.camera.clone();
    var cameraPos = innerGLCamera_copy.eye;

    var worldUpVec = this.glMatrix_vec3d.create();
    this.sceneView.renderCoordsHelper.worldUpAtPosition(cameraPos, worldUpVec); //up-aux-line
    this.worldUpVec = worldUpVec;

    //1.init Pos
    var point1_gl = utils.geoCoordToRenderCoord([basePos[0], basePos[1], basePos[2]], null, this.sceneView);
    var point2_gl = utils.geoCoordToRenderCoord([nextPos[0], nextPos[1], nextPos[2]], null, this.sceneView);

    var vecDir = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.subtract(vecDir, point1_gl, point2_gl); //dir: 2->1
    var vecLen = this.glMatrix_vec3.length(vecDir);

    var vecNor = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.normalize(vecNor, vecDir);

    var tarLen = vecLen + fixDistance; //fix distance
    var tmp = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.scale(tmp, vecNor, tarLen);

    var pointTarget = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.add(pointTarget, point2_gl, tmp);

    var pMove_pos = utils.renderCoordToGeoCoord(pointTarget, 1, this.sceneView);
    var initPos = new this.Point({
      x: pMove_pos[0],
      y: pMove_pos[1],
      z: pMove_pos[2],
      type: 'point',
      spatialReference: this.sceneView.spatialReference
    });
    if (initPos.z < 10) {//for under the ground
      initPos.z = 10;
    }
    if (initPos.z < cameraHeight) {
      initPos.z = cameraHeight;
    }
    if (this.state.DEBUG) {
      this.auxHelper.drawPoint(initPos, 'red');
    }

    var fixedCamPos_gl = utils.geoCoordToRenderCoord([initPos.x, initPos.y, initPos.z], null, this.sceneView);
    //this._CUR_CAM_POS_gl = fixedCamPos_gl;//cache cam pos

    //5. move camera 
    //var glCamera = new this.GLCamera(fixedCamPos_gl, point1_gl, worldUpVec); //camPos, lookAtPos, up-direction
    //var apiCamera = this.cameraUtils.internalToExternal(this.sceneView, glCamera);
    return fixedCamPos_gl;
  }

  //Rotated cam-pos
  _getRotatedCameraPos(prePos, basePos, nextPos, camPos_gl) {
    var rLines = [];
    if (!basePos || !nextPos) {
      return rLines;
    }

    var prePos_gl = utils.geoCoordToRenderCoord([prePos[0], prePos[1], prePos[2]], null, this.sceneView);
    var basePos_gl = utils.geoCoordToRenderCoord([basePos[0], basePos[1], basePos[2]], null, this.sceneView);
    var nextPos_gl = utils.geoCoordToRenderCoord([nextPos[0], nextPos[1], nextPos[2]], null, this.sceneView);

    var lastVec = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.subtract(lastVec, basePos_gl, prePos_gl); //invers 
    var nextVec = this.glMatrix_vec3d.create();
    this.glMatrix_vec3.subtract(nextVec, nextPos_gl, basePos_gl); //invers 

    var auxUpVec = this.glMatrix_vec3d.create();
    this.sceneView.renderCoordsHelper.worldUpAtPosition(basePos_gl, auxUpVec); //up-aux-line
    var angle = utils.radToDeg(this.esriMathUtils.angle(lastVec, nextVec, auxUpVec));
    if (this.state.DEBUG) {
      console.log('angle1==>' + angle);
    }

    var factor = Math.abs(Math.round(angle)) //* 10;
    var tmpVec = this.glMatrix_vec3d.create();
    for (let step = 0, len = factor; step < len; step++) {
      var rotatedAngle = (angle / factor) * step;
      tmpVec = this.rotateBySceneMode(camPos_gl, auxUpVec, rotatedAngle, basePos_gl);

      rLines.push(this._setAnimatObj(tmpVec, basePos_gl, auxUpVec, 'rotate'));
    }

    return rLines;
  }

  //line cam-pos
  _getLineInerPos(camPos1, camPos2, point1_gl, point2_gl) {
    var points = [];
    var factor = 40;
    var inter_gl = utils.linerInter(camPos2, camPos1, factor);

    for (let step = 0, len = factor; step < len; step++) {
      var camPos_gl = [0, 0, 0]; //just Inter
      camPos_gl[0] = camPos1[0] + inter_gl.x * step;
      camPos_gl[1] = camPos1[1] + inter_gl.y * step;
      camPos_gl[2] = camPos1[2] + inter_gl.z * step;

      var lookAtPos_gl = [0, 0, 0];
      lookAtPos_gl[0] = point1_gl[0] + inter_gl.x * step;
      lookAtPos_gl[1] = point1_gl[1] + inter_gl.y * step;
      lookAtPos_gl[2] = point1_gl[2] + inter_gl.z * step;

      points.push(this._setAnimatObj(camPos_gl, lookAtPos_gl, null, 'line'));
    }

    return points;
  }

  resume = () => {
    super.resume();
    this._resume(this.sceneView.goTo(this.getTaskCamera(this.frameTasks[this._frameIdx])));
  }
}