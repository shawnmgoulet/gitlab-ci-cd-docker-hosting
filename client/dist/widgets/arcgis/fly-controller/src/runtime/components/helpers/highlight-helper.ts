import { loadArcGISJSAPIModules } from 'jimu-arcgis';
import SceneView = require('esri/views/SceneView');

interface Options {
  sceneView: __esri.SceneView;
}

export default class highlightHelper {
  GraphicsLayer: any;
  Point: any;
  Graphic: any;
  sceneView: SceneView;

  markerColor: string;
  outlineColor: string;
  markerSize: number;

  isPopupEnable: boolean;

  //layers
  highlightGraphicsLayer: __esri.GraphicsLayer;
  currentHighlightGeo: any;

  constructor(options: Options) {
    loadArcGISJSAPIModules([
      'esri/layers/GraphicsLayer',
      'esri/geometry/Point',
      'esri/Graphic'
    ]).then(modules => {
      [
        this.GraphicsLayer, this.Point, this.Graphic
      ] = modules;
      this.highlightGraphicsLayer = new this.GraphicsLayer();
      this.sceneView.map.add(this.highlightGraphicsLayer);
    });

    this.sceneView = options.sceneView;

    //appearance
    this.markerColor = 'orange';
    this.outlineColor = '[128, 128, 128, 0.8]';
    this.markerSize = 12;
  }

  //aux point
  markPoint = (mapPoint) => {
    this.currentHighlightGeo = new this.Graphic({
      geometry: mapPoint,
      symbol: {
        type: 'point-3d',
        symbolLayers: [{
          type: 'icon', size: this.markerSize,
          resource: { primitive: 'circle' },
          material: { color: this.markerColor },
          outline: { color: this.outlineColor }
        }]
      }
    });
    this.highlightGraphicsLayer.add(this.currentHighlightGeo);

    // this.point.on("pointer-down", function(){
    //   console.log("!!!")
    // });

    return this.currentHighlightGeo;
  };

  //aux line
  markLine = (geo) => {
    var linesPoints = geo.paths[0];
    this.currentHighlightGeo = new this.Graphic({
      geometry: {
        type: 'polyline',
        paths: linesPoints,
        spatialReference: this.sceneView.spatialReference
      },
      symbol: { type: 'line-3d', symbolLayers: [{ type: 'line', material: { color: this.markerColor }, size: this.markerSize }] }
    });
    this.highlightGraphicsLayer.add(this.currentHighlightGeo);
    // this.line.on("pointer-down", function(){
    //   console.log("!!!")
    // });

    return this.currentHighlightGeo;
  };
  
  clearTempHighlight = () => {
    
  }

  //clean layer
  clearAll = () => {
    this.highlightGraphicsLayer.removeAll();
    //this.point.remove();
    this.currentHighlightGeo = null;
    //this.line.remove();
    //this.line = null;
  };

  //map popup enable/disable
  cacheMapPopupState = () => {
    if(this.sceneView && this.sceneView.popup){
      this.isPopupEnable = this.sceneView.popup.autoOpenEnabled;
    }
  }
  revertMapPopupState = () => {
    if(this.isPopupEnable){
      this.sceneView.popup.autoOpenEnabled = this.isPopupEnable;
    }
  }
  mapPopupEnable = () => {
    this.sceneView.popup.autoOpenEnabled = true;
  }
  mapPopupDisable = () => {
    this.sceneView.popup.autoOpenEnabled = false;
  }
  setMapPopupStateByIsPicking = (isPicking) => {
    if(false === isPicking){
      this.cacheMapPopupState();
      this.mapPopupDisable()
    } else {
      this.revertMapPopupState();
    }
  }
}