//import {  loadArcGISJSAPIModules } from 'jimu-arcgis';
export function getSplineLen (vecArr){
  var totalLen = 0;
  for(var i = 0, len = vecArr.length - 1; i < len; i++){
    var baseVec = vecArr[i];
    var nextVec = vecArr[i + 1];

    var sub = [];
    _subtract(sub, nextVec, baseVec);

    var l = _getVecLength(sub); //TODO BUG: vec[2]-vec[1], for(100->0)
    totalLen += l;
  }
  return totalLen;
}

export function splitSplineByInterval (vecArr, inter){
  //go forward
  
}

/////////////////////////////////////////////
//esri\core\libs\gl-matrix-2\math\vec3
export function _getVecLength(a): number {
  const x = a[0];
  const y = a[1];
  const z = a[2];
  return Math.sqrt(x * x + y * y + z * z);
}
export function _getVecScale(out, a, b){
  out[0] = a[0] * b;
  out[1] = a[1] * b;
  out[2] = a[2] * b;
  return out;
}
export function _subtract(out, a, b) {
  out[0] = a[0] - b[0];
  out[1] = a[1] - b[1];
  out[2] = a[2] - b[2];
  return out;
}
/////////////////////////////////////////////



export function normalPoints (rawPoints){
  var outPoints = [];
  for(var i = 0, len = rawPoints.length; i < len; i++){
    var p = rawPoints[i];
    var x = p[0];
    var y = p[1];
    var z = p[2] || 0;
    outPoints.push([x, y, z]);
  }
  return outPoints;
}

/** Chaikin Algorithm for B-Splines**/
export function getBspline (rawPoints, iterF){
  var normalPoints = normalPoints4Bspline(rawPoints);
  var bPoints = bspline(normalPoints, iterF);

  var vecsBspline = mergePoints(bPoints); //describe Bspline in vec[]
  /*var _len = */getSplineLen(vecsBspline);
  //console.log("Bspline len=>" + _len);

  return vecsBspline;
}

function normalPoints4Bspline (rawPoints){
  var outPoints = [];
  for(var i = 0, len = rawPoints.length; i < len; i++){
    var p = rawPoints[i];
    var x = p[0];
    var y = p[1];
    var z = p[2] || 0;
    outPoints.push(x, y, z);
  }
  return outPoints;
}
function mergePoints (panedPoints){
  var outPoints = [];
  for(var i = 0, len = panedPoints.length / 3; i < len; i++){
    var px = panedPoints[3 * i];
    var py = panedPoints[3 * i + 1];
    var pz = panedPoints[3 * i + 2];
    var tar = [];
    tar.push(px, py, pz);
    outPoints.push(tar);
  }
  return outPoints;
}
//var iterations = 3;
function bspline (pts, iterations) {
  if (!iterations || iterations == 0) {
    return pts;
  }
  var items = pts.length / 3.0
  var pts2 = [pts[0], pts[1], pts[2]];
  for (var i = 0; i < items - 1; i++) {
    var j = 3 * i;
    pts2[3 + 2 * j + 0] = 3 / 4 * pts[j + 0] + 1 / 4 * pts[j + 3];
    pts2[3 + 2 * j + 1] = 3 / 4 * pts[j + 1] + 1 / 4 * pts[j + 4];
    pts2[3 + 2 * j + 2] = 3 / 4 * pts[j + 2] + 1 / 4 * pts[j + 5];

    pts2[3 + 2 * j + 3] = 1 / 4 * pts[j + 0] + 3 / 4 * pts[j + 3];
    pts2[3 + 2 * j + 4] = 1 / 4 * pts[j + 1] + 3 / 4 * pts[j + 4];
    pts2[3 + 2 * j + 5] = 1 / 4 * pts[j + 2] + 3 / 4 * pts[j + 5];
  }
  pts2.push(pts[pts.length - 3], pts[pts.length - 2], pts[pts.length - 1]);

  return bspline(pts2, iterations - 1);
}

//Catmull_Rom spline

//Douglas–Peucker algorithm