import { loadArcGISJSAPIModules } from 'jimu-arcgis';
import SceneView = require('esri/views/SceneView');
//import { normalPoints, getBspline } from '../utils/GeometryUtils';
//import * as SplinesUtils from '../utils/SplinesUtils';
//import { Queue } from '../utils/queue';
//import { any } from 'prop-types';
import { RotateDirection } from '../../../config';
import AuxHelper from '../helpers/aux-lines-helper';
import * as utils from '../utils/utils';


interface flyOptions {
  fixHeight?: number; //metre
  fixDistance?: number; //metre
  fixTilt?: number; //d
  direction?: RotateDirection;
  debug?: boolean;
  sceneView: __esri.SceneView;
  widget?: any; //TODO use event!!!
}

export enum FlyState {
  PREPARE = 'prepare',
  READY = 'ready',
  RUNNING = 'running',
  PAUSED = 'pause',
  INTERRUPTED = 'interrupted',
  RESUME = 'resume',
  STOPPED = 'stopped'
}

export interface HitTestRes {
  z: number,
  dis: number
}

//TODO maybe a ts interface ?
export default abstract class BaseFlyController {
  //glMatrix
  glMatrix: any;
  vec3f64: any; vec3: any; glMatrix_vec3: any; glMatrix_vec3d: any;
  mat4f64: any; mat4: any; glMatrix_mat4: any; glMatrix_mat4d: any;

  //Camera
  cameraUtils: any;
  GLCamera: any;

  //elements
  auxHelper?: any;
  Point: any; //__esri.Point;
  sceneView: SceneView;

  //camera
  cameraHeight: number;
  //cache
  cachedCamera: any;
  cachedGeo: any;

  fixHeight: number;
  fixDistance: number;
  fixTilt: number;

  //animat
  animatHandler: __esri.FrameTaskHandle; // pause , resume , remove
  interruptPrepareHandler: __esri.PromisedWatchHandle;
  scheduling: __esri.scheduling;
  watchUtils: __esri.watchUtils;

  //cb
  widget: any; //TODO use event !!!

  //state
  state: {
    DEBUG: boolean;
    flyState: FlyState;
    camera_gl: any
  }

  constructor(options: flyOptions) {
    loadArcGISJSAPIModules([
      'esri/core/libs/gl-matrix-2/gl-matrix',
      'esri/core/libs/gl-matrix-2/vec3f64',
      'esri/core/libs/gl-matrix-2/mat4f64',
      'esri/core/libs/gl-matrix-2/vec3',
      'esri/core/libs/gl-matrix-2/mat4',
      'esri/views/3d/support/cameraUtils',
      'esri/views/3d/webgl-engine/lib/Camera',
      'esri/core/scheduling',
      'esri/core/watchUtils',
      'esri/geometry/Point',
    ]).then(modules => {
      [
        this.glMatrix, this.vec3f64, this.mat4f64, this.vec3, this.mat4, this.cameraUtils, this.GLCamera,
        this.scheduling, this.watchUtils,
        this.Point
      ] = modules;
      this.glMatrix_vec3 = this.vec3.vec3; this.glMatrix_vec3d = this.vec3f64.vec3f64;
      this.glMatrix_mat4 = this.mat4.mat4; this.glMatrix_mat4d = this.mat4f64.mat4f64;

      this.GLCamera = this.GLCamera.default;
    });

    this.sceneView = options.sceneView;

    this.state = {
      DEBUG: false,
      flyState: FlyState.PREPARE,
      camera_gl: null
    };
    this.state.DEBUG = options.debug;
    if (this.state.DEBUG) {
      this.auxHelper = new AuxHelper({ sceneView: this.sceneView });
    }

    this.widget = options.widget; //TODO event
    //events
    // this.sceneView.on('key-down', ((event) => {
    //   if (FlyState.RUNNING === this.state.flyState) {
    //     var keyPressed = event.key;
    //     if (keyPressed.slice(0, 5) === 'Arrow') {
    //       event.stopPropagation(); //TODO prevents panning with the arrow keys
    //     }
    //   }
    // }));
    this.sceneView.on('drag', ((event) => {
      if (FlyState.RUNNING === this.state.flyState) {
        if ('start' === event.action) {
          this.pause();
        } else {
          event.stopPropagation(); //update===event.action
        }
      } else if (FlyState.PAUSED === this.state.flyState) {
        if ('end' === event.action) {
          //event.stopPropagation();
        }
      }
    }));
  }

  //states check
  isEnableToFly() {
    var state = this.state.flyState;
    if (state === FlyState.RUNNING || state === FlyState.RESUME) {
      return false;
    } else {
      return true;
    }
  }
  isEnableToPause() {
    var state = this.state.flyState;
    if (state !== FlyState.RESUME) { //TODO ! quick click
      return true;
    } else {
      return false;
    }
  }

  //camera
  setCameraInfo(options) {
    var camera = null;
    if (options && options.cameraInfo) {
      camera = options.cameraInfo;
      this.cachedCamera = camera;
    }

    if (!camera) {
      camera = this.sceneView.camera.clone(); //cache current camera
    }

    this.cameraHeight = camera.position.z;
    this.fixTilt = camera.tilt;
  }
  getCameraInfo() {
    //cache current camera
  }

  //fly lifecycle
  prepare(): Promise<any> {
    const promise = new Promise((resolve, reject) => {
      this.state.flyState = FlyState.PREPARE;

      this.interruptPrepareHandler = this.watchUtils.whenOnce(this.sceneView, 'interacting', () => {
        this.state.flyState = FlyState.INTERRUPTED;
        resolve();
      });

      setTimeout(() => {
        resolve();
      }, 1200)
    })
    return promise;
  }
  // flyToInitPosition(fun) {
  // }

  fly(geo?, options?) {
  }
  _doFly() {
    this.state.flyState = FlyState.RUNNING;
  }

  pause() {
    if (false === this.isEnableToPause()) {
      return;
    }
    //console.log('pause');
    this.state.flyState = FlyState.PAUSED;
    this.stopAnimat();
  }

  resume() {
    this.state.flyState = FlyState.RESUME;
  }
  _resume = (fun) => {
    //console.log('resume');
    //goBack to pause point, then go on flying
    fun.then(() => {
      this.prepare().then(() => {
        if (this.interruptPrepareHandler) {
          this.interruptPrepareHandler.remove();
        }

        if (this.state.flyState === FlyState.INTERRUPTED) {
          this.pause();
        } else {
          this._doFly();
        }
      })
    }).catch((err) => console.log('rejected:', err));
  }

  stop() {
    this.stopAnimat();
    this.state.flyState = FlyState.STOPPED;
    this.clear();
  }

  clear() {
    if (this.state.DEBUG) {
      this.auxHelper.clearAll();
    }
  }


  //animat
  startAnimat(fun) {
    this.animatHandler = this.scheduling.addFrameTask({
      update: () => { fun(); }
    });
  }
  stopAnimat() {
    if (this.animatHandler) {
      this.animatHandler.remove();
    }
    //TODO change cb to event
    if (this.widget && this.widget.onFlyPause) {
      this.widget.onFlyPause();
    }
  }


  //rotate
  rotateBySceneMode = (camera_gl, upVec, rotateSpeed, offset_gl?) => {
    var tmpPos = this.glMatrix_vec3d.fromValues(camera_gl[0], camera_gl[1], camera_gl[2]);

    if (this.sceneView.state.isLocal && offset_gl) {
      var offsetToOrigin = this.glMatrix_vec3d.create();
      this.glMatrix_vec3.negate(offsetToOrigin, offset_gl); //origin - offset_gl
      this.glMatrix_vec3.add(tmpPos, tmpPos, offsetToOrigin);
    }

    var rMatrix = this.glMatrix_mat4d.create();
    this.glMatrix_mat4.rotate(rMatrix, rMatrix, utils.degToRad(rotateSpeed), upVec);
    this.glMatrix_vec3.transformMat4(tmpPos, tmpPos, rMatrix);

    if (this.sceneView.state.isLocal && offset_gl) {
      this.glMatrix_vec3.subtract(tmpPos, tmpPos, offsetToOrigin);
    }
    return tmpPos;
  }

  getHitTestInfo = (graphic) => {
    //"polyline" === graphic.geometry.type
    var paths = graphic.geometry.paths[0];
    var point = paths[0];
    this.getPointHitTestRes(point[0], point[1], this.sceneView).then((res: HitTestRes) => {
      this.fixHeight = res.z;
      this.fixDistance = res.dis;
    })
  }
  //Attitude
  getPointHitTestRes = (x, y, sceneView) => {
    const promise = new Promise((resolve, reject) => {
      var initPos = new this.Point({
        x: x,
        y: y,
        //z: 
        type: 'point',
        spatialReference: sceneView.spatialReference
      });

      var screenPoint = sceneView.toScreen(initPos);
      //var tilt = sceneView.camera.tilt;
      sceneView.hitTest(screenPoint, { exclude: [sceneView.graphics] }).then((hitTestResult) => {
        var lastHit = utils.getHitPointOnTheGround(hitTestResult);
        var dis = Math.cos(utils.degToRad(90 - sceneView.camera.tilt)) * lastHit.distance;
        return resolve({
          z: lastHit.mapPoint.z,
          dis: dis
        });
      });
    });

    return promise;
  }

  //Dilution
  getDilutionPath = (ref, max?) => {
    var maxNum = max || 3000;
    if (ref.length && ref.length > maxNum) {
      var res = [];
      var step = Math.floor(ref.length / maxNum);
      for (var i = 0, len = ref.length; i < len; i = i + step) {
        res.push(ref[i]);
      }
      return res;
    } else {
      return ref;
    }
  }
}