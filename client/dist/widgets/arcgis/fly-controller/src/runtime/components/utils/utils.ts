// import MapBase, {HighLightHandle} from './components/mapbase';
// import { IFeature } from '@esri/arcgis-rest-types';
//import { externalRenderers} from "esri/views/3d/externalRenderers";
import externalRenderers = require('esri/views/3d/externalRenderers');
import webMercatorUtils = require('esri/geometry/support/webMercatorUtils');
import { FlyStyle } from '../../../config';
import nls from '../../translations/default';
//import { loadArcGISJSAPIModules } from 'jimu-arcgis';

// export function Animation_Tick (fun, time) {
//   return new Promise(resolve => {
//     setTimeout(() => {
//       //console.log(value);
//       if(fun){
//         return fun().then(function() {
//           return resolve(/*value*/)
//         });
//       } else {
//         resolve();
//       }

//     }, time);  
//   });
// }

export function getHitPointOnTheGround(hitTestResult) {
  var lastHit = null;
  if (hitTestResult.results.length > 0) {
    lastHit = hitTestResult.results[hitTestResult.results.length - 1];
  }
  if (hitTestResult.ground.mapPoint) {
    if (lastHit) {
      if (hitTestResult.ground.distance > lastHit.distance) {
        // an object under the ground could be more far away, check first the distance before set the ground as last point
        lastHit = hitTestResult.ground;
      }
    } else {
      lastHit = hitTestResult.ground;
    }
  }

  //this.aux_drawPoint(lastHit.mapPoint);

  return lastHit;
}

//camera
export function getCurentGLCameraInfo(sceneView) {
  var innerGLCamera_copy = sceneView.state.camera.clone();
  return {
    eye: innerGLCamera_copy.eye,
    center: innerGLCamera_copy.center,
    up: innerGLCamera_copy.up
  }
};

//coord
export function geoCoordToRenderCoord(inCood, num, sceneView) {
  var transNum = num ? num : 1;

  var var_renderCoordinates = [];
  externalRenderers.toRenderCoordinates(sceneView, inCood, 0,
    null, //null: means follow this.scene.sp
    var_renderCoordinates, 0, transNum);

  return var_renderCoordinates;
};
export function renderCoordToGeoCoord(varCood, num, sceneView) {
  var transNum = num ? num : 1;
  var scene = sceneView ? sceneView : this.sceneView;

  var out_geographicCoordinates = [];
  externalRenderers.fromRenderCoordinates(scene, varCood, 0, out_geographicCoordinates, 0,
    null,
    transNum);

  return out_geographicCoordinates;
};

//ll - xy
this._tmpZ = 200;
export function lngLatToXY(tmpLon, tmpLat, elevZ) {
  var tp = webMercatorUtils.lngLatToXY(tmpLon, tmpLat);
  var z = elevZ ? elevZ : this._tmpZ;
  return [tp[0], tp[1], z];
};
export function xyToLngLat(x, y) {
  var p2 = webMercatorUtils.xyToLngLat(x, y);
  //halfLon = Math.abs(p1[0] - p2[0]) / 8;
  return p2;
};


//math
export function degToRad(degrees) {
  return degrees * Math.PI / 180;
};
export function radToDeg(radians) {
  return radians * 180 / Math.PI;
};

//interpolation
export function linerInter(to, from, factor) {
  var inter = { x: 0, y: 0, z: 0 };
  inter.x = (to[0] - from[0]) / factor;
  inter.y = (to[1] - from[1]) / factor;
  inter.z = (to[2] - from[2]) / factor;
  return inter;
}
//angle with sign
// export function angle (v1, v2, n){
//   //return mathUtils.angle(v1, v2, n);
//   loadArcGISJSAPIModules([
//     "esri/views/3d/support/mathUtils"
//   ]).then(modules => {
//   [
//     this.mathUtils
//   ] = modules;
//     this.mathUtils.angle(v1, v2, n);
//   });

// }
// export function controlUIWidget(mapBaseView: __esri.MapView | __esri.SceneView, isOpen: boolean, uiWidget: __esri.Widget, position?: string, widgetName?: string) {
//   let ui = mapBaseView.ui as any;
//   if (!uiWidget) {
//     return;
//   }
//   if (isOpen) {
//     ui.add(uiWidget, position);
//     if (widgetName && ui.specialComponents.indexOf(widgetName) === -1) {
//       ui.specialComponents.push(widgetName);
//     }
//   } else {
//     ui.remove(uiWidget);
//   }
// }

//geo
export function isPolylineEquals(polyline1, polyline2) {
  if (!(polyline1.type === 'polyline' && polyline2.type === 'polyline')) {
    return false;
  }
  if (!polyline1.extent.equals(polyline2.extent)) {
    return false;
  }
  if (polyline1.paths.length !== polyline2.paths.length) {
    return false;
  }
  if (polyline1.paths[0].length !== polyline2.paths[0].length) {
    return false;
  }

  var line1 = polyline1.paths[0];
  var line2 = polyline2.paths[0];
  for (var i = 0, len = line1.length; i < len; i++) {
    var p1Val = line1[i];
    var p2Val = line2[i];
    if (p1Val[0] !== p2Val[0] || p1Val[1] !== p2Val[1]) {//TODO
      return false;
    }
  }

  return true;
}

//widget
export function findFirstUseItem(itemsList) {
  for (var i = 0, len = itemsList.length; i < len; i++) {
    var item = itemsList[i];
    if (item && item.isInUse) {
      return item;
    }
  }

  return null;
}
export function getEnabledItemNum(itemsList) {
  var count = 0;
  for (var i = 0, len = itemsList.length; i < len; i++) {
    var item = itemsList[i];
    if (item && item.isInUse) {
      count++;
    }
  }
  return count;
}
export function getFlyStyleTitle(flyStyle, props) {
  if (flyStyle === FlyStyle.Rotate) {
    return props.intl.formatMessage({ id: 'flyStyleRotate', defaultMessage: nls.flyStyleRotate });
  } else if (flyStyle === FlyStyle.Path) {
    return props.intl.formatMessage({ id: 'flyStylePath', defaultMessage: nls.flyStylePath });
  } else {
    return null;
  }
}