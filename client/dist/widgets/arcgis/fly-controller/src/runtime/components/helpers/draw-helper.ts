import { loadArcGISJSAPIModules } from 'jimu-arcgis';

import SceneView = require('esri/views/SceneView');
//import * as utils from '../utils/utils';

interface DrawOptions {
  // webMapId: string;
  // parentMapView: __esri.MapView;
  // parentMap:  __esri.Map;
  sceneView: __esri.SceneView;
}

export interface DrawRes {
  graphic: any,
  cameraInfo: any,
}

//let PointType;
export default class DrawHelper {
  Deferred: any;
  Graphic: any;
  GraphicsLayer: any;
  Point: any;
  SketchViewModel: any;

  sceneView: SceneView;

  sketchVM: __esri.SketchViewModel;

  //layers
  graphicslayer: __esri.GraphicsLayer;
  drawCompleteHanlder: any;

  markerColor: string | Array<any>;
  outlineColor: string | Array<any>;
  markerSize: number;

  //cache
  cache: any;
  clickEvent: any;

  constructor(options: DrawOptions) {
    loadArcGISJSAPIModules([
      'dojo/_base/Deferred',
      'esri/Graphic',
      'esri/geometry/Point',
      'esri/layers/GraphicsLayer',
      'esri/widgets/Sketch/SketchViewModel',
    ]).then(modules => {
      [
        this.Deferred, this.Graphic, this.Point, this.GraphicsLayer, this.SketchViewModel
      ] = modules;
      //PointType = this.Point;
      //1
      this.graphicslayer = new this.GraphicsLayer();
      this.sceneView.map.addMany([this.graphicslayer]);

      //appearance
      //const white = [255, 255, 255, 0.8];
      const orange = [255, 165, 0, 0.8];
      this.markerColor = orange;
      this.outlineColor = '[128, 128, 128, 0.8]';
      this.markerSize = 14;

      //2
      // const blue = [82, 82, 122, 0.9];

      // const extrudedPolygon = {
      //   type: "polygon-3d",
      //   symbolLayers: [
      //     {
      //       type: "extrude",
      //       size: 10, // extrude by 10 meters
      //       material: {
      //         color: white
      //       },
      //       edges: {
      //         type: "solid",
      //         size: "3px",
      //         color: blue
      //       }
      //     }
      //   ]
      // };
      const route = {
        type: 'line-3d',
        symbolLayers: [
          {
            type: 'line',
            size: '3px',
            material: {
              color: this.markerColor
            }
          },
          {
            type: 'line',
            size: '10px',
            material: {
              color: this.markerColor
            }
          }
        ]
      };
      const point = {
        type: 'simple-marker',
        style: 'circle',
        size: this.markerSize,
        color: this.markerColor,
        outline: {
          color: this.outlineColor,
          width: 2
        }
      };
      //create Sketch
      this.sketchVM = new this.SketchViewModel({
        layer: this.graphicslayer,
        view: this.sceneView,
        pointSymbol: point,
        //polygonSymbol: extrudedPolygon,
        polylineSymbol: route
      });
    });

    this.sceneView = options.sceneView;
    this.cache = {};
  }

  //TODO drawPoint
  drawPoint = (/*mapPoint?*/) => {
    // this.setMapCursor();
    // this.revertMapCursor();

    // var point = new this.Graphic({
    //   geometry: mapPoint,
    //   symbol: {
    //     type: "point-3d",
    //     symbolLayers: [{
    //       type: "icon", size: this.markerSize,
    //       resource: { primitive: "circle" },
    //       material: { color: this.markerColor },
    //       outline: { color: this.outlineColor }
    //     }]
    //   }
    // });
    //this.graphicslayer.add(point);
    //return point;
    const promise = new Promise((resolve, reject) => {
      /*var action = */this.sketchVM.create('point', { mode: 'click' });

      this.drawCompleteHanlder = this.sketchVM.on('create', ((event) => {
        var cachedCam = null;
        if (event.state === 'complete') {
          cachedCam = this.sceneView.camera.clone();

          var graphic = event.graphic;
          //TODO 
          // var point: typeof PointType = event.graphic.geometry;
          // var screenPoint = this.sceneView.toScreen(point);
          // this.sceneView.hitTest(screenPoint, { exclude: [this.sceneView.graphics] }).then((hitTestResult) => {
          //   var lastHit = utils.getHitPointOnTheGround(hitTestResult);
          //   return resolve({lastHit: lastHit, graphic: graphic});
          // });
          this.clearAllGraphics();
          this.graphicslayer.add(graphic);
          this.cleanHanlder();

          return resolve({ graphic: graphic, cameraInfo: cachedCam }); //just on the ground
        } else if ('cancel' === event.state) {
          return reject('cancel'); // drawHelper.cancel() or ESC-key
        }
      }));
    });

    return promise;
  }
  clearPoint = (point) => {
    if (point && this.graphicslayer) {
      this.graphicslayer.remove(point);
    }
  }

  drawLine = (flyController) => {
    const promise = new Promise((resolve, reject) => {
      // if (line) {
      //   //1
      //   this.graphicslayer.add(line);
      //   return resolve(line);
      // } else {
      //2
      /*var action = */this.sketchVM.create('polyline', { mode: 'click' }); //
      var cachedCam = null;
      this.drawCompleteHanlder = this.sketchVM.on('create', ((event) => {
        
        if (event.state === 'start') {
          cachedCam = this.sceneView.camera.clone();
          flyController.getHitTestInfo(event.graphic);
        } else if (event.state === 'complete') {
          //var line = event.graphic.geometry;
          var graphic = event.graphic;

          this.clearAllGraphics();
          this.graphicslayer.add(graphic);
          this.cleanHanlder();

          return resolve({ graphic: graphic, cameraInfo: cachedCam });
        } else if ('cancel' === event.state) {
          return reject('cancel'); // drawHelper.cancel() or ESC-key
        }
      }));
      //}
    });

    return promise;
  }
  clearLine = (line) => {
    if (line && this.graphicslayer) {
      this.graphicslayer.remove(line);
    }
  }

  cancel = () => {
    if (this.sketchVM && this.sketchVM.cancel) {
      this.sketchVM.cancel();
    }
  }

  cleanHanlder = () => {
    if (this.sketchVM && this.sketchVM.cancel) {
      this.sketchVM.cancel();
    }

    if (this.drawCompleteHanlder && this.drawCompleteHanlder.remove) {
      this.drawCompleteHanlder.remove();
    }
  }
  clearAllGraphics = () => {
    //this.cleanHanlder();
    if (this.graphicslayer) {
      this.graphicslayer.removeAll();
    }
    //this.revertMapCursor();
  }


  //mouse styles
  setMapCursor = () => {
    if (this.sceneView) {
      this.cache.cursorStyle = this.sceneView.get('cursor');
    }
    this.sceneView.set('cursor', 'crosshair');
  }
  revertMapCursor = () => {
    var cursorStyle = 'default';
    // if (this.cache && this.cache.cursorStyle) {
    //   cursorStyle = this.cache.cursorStyle;
    // }
    this.sceneView.set('cursor', cursorStyle);
  }
}