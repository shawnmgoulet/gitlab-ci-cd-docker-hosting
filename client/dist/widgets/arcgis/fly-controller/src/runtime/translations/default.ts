export default {
  _widgetLabel: 'Fly Controller',

  chooseMapTip: 'Please select a map with 3D data',
  configErrorTip: 'Please select at least one fly style',

  flyStyleRotate: 'Rotate fly',
  flyStylePath: 'Fly along path',

  triggerSelectPoint: 'Select a point',
  triggerDrwaPath: 'Select points to draw a path',
  triggerSelectFeature: 'Select a feature from the map',
  triggerClear: 'Remove selection',

  play: 'Play',
  pause: 'Pause'
}
