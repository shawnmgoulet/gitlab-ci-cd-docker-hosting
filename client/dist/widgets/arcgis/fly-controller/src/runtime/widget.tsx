/** @jsx jsx */
import { jsx, BaseWidget, AllWidgetProps } from 'jimu-core';
import { Button, Icon, Input, Label, Nav, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Progress, WidgetPlaceholder } from 'jimu-ui';
import nls from './translations/default';

import { JimuMapViewComponent } from 'jimu-arcgis';
//import MapView = require('esri/views/MapView');
import SceneView = require('esri/views/SceneView');
import WebMap = require('esri/WebMap');
import { IMConfig, FlyStyle, PathStyle, RotateStyleConstraint, PathStyleConstraint } from '../config';
import { getStyle, getBgStyle, getBtnStyle, getIconColor, getDropdownStyle, getDropdownTogglerClass } from './style';
import * as utils from './components/utils/utils';

import DrawHelper, { DrawRes } from './components/helpers/draw-helper';
import { FlyState } from './components/controllers/base-fly-controller';
import RotatingFly from './components/controllers/rotating-fly';
import CurveFly from './components/controllers/curve-fly';
import LineFly from './components/controllers/line-fly';

import HighlightHelper from './components/helpers/highlight-helper';
let flyIcon = require('jimu-ui/lib/icons/fly-controller.svg');
let rotateIconImage = require('../assets/icons/fly-rotate.svg');
let pathIconImage = require('../assets/icons/fly-path.svg');

export enum TriggerActionState {
  None = 'NONE',
  Drawing = 'DRAWING',
  Picking = 'PICKING'
}
export enum PlayState {
  // Ready = 'READY',
  // Stoped = 'STOPED',
  Running = 'RUNNING',
  Paused = 'PAUSED'
}


interface State {
  //0.
  activedItemIdx: number;
  errorTip: string;
  isCachedGeo: boolean;
  //1.fly style
  isFlyStylePopupOpen: boolean;
  flyStyleActived: FlyStyle | null;
  //2.triggerAction 
  isDrawing: boolean;
  isPlaying: boolean;
  isPicking: boolean;
  //3.setting
  isSettingPopupOpen: boolean;
  // settingHeight: number; //metres
  // settingDistance: number;
  // settingTilt: number;
  //4.progresser
  progress: number;
}

// interface ExtraProps {
//   theme: ThemeVariables;
//   outputDataSourceJson: IMDataSourceJson
// }

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig>/*& ExtraProps*/, State>{
  //mapView: MapView;
  sceneView: SceneView;
  webMap: WebMap;
  extentWatch: __esri.WatchHandle;
  SceneView: typeof __esri.SceneView;
  //controllers
  rotationFlyController: RotatingFly;
  curveFlyController: CurveFly;
  lineFlyController: LineFly;
  //helper
  drawHelper: DrawHelper;
  highlightHelper: HighlightHelper;

  //events
  pointerMoveEvent: any;
  pointerDownEvent: any;

  //runtime cache
  usingSceneviewId: string;
  firstConfigItem: RotateStyleConstraint | PathStyleConstraint;
  geoCache: any;
  highlightGeometry: any;

  //tips
  chooseMapTip = this.props.intl.formatMessage({ id: 'chooseMapTip', defaultMessage: nls.chooseMapTip });
  configErrorTip = this.props.intl.formatMessage({ id: 'configErrorTip', defaultMessage: nls.configErrorTip });

  constructor(props) {
    super(props);

    this.state = {
      //0 
      activedItemIdx: 0,
      errorTip: this.chooseMapTip,
      isCachedGeo: false,
      //1.fly style
      isFlyStylePopupOpen: false,
      flyStyleActived: null,
      //2.triggerAction 
      isDrawing: false,
      isPlaying: false,
      isPicking: false,
      //3.setting
      isSettingPopupOpen: false,
      // settingHeight: 30000,
      // settingDistance: 30000,
      // settingTilt: 45,
      //4.progresser
      progress: 0
    }

    this.geoCache = {};
  }

  //check error
  setStateErrorTip = (tip) => {
    this.setState({ errorTip: tip })
  }
  checkErrorInConfig = () => {
    if ('' !== this.state.errorTip && this.state.errorTip !== this.configErrorTip) {
      return; // others error already occurred, skip
    }

    if (utils.getEnabledItemNum(this.props.config.itemsList) < 1) {
      this.setStateErrorTip(this.configErrorTip); //no item in config
    } else {
      this.setStateErrorTip('');
    }
  }
  //for map selector cb
  onActiveViewChange = (sceneView) => {
    //Async errors
    if (null === sceneView || undefined === sceneView) {
      this.setStateErrorTip(this.chooseMapTip);
      return; //skip null
    }

    if ('2d' === sceneView.view.type) {
      this.setStateErrorTip(this.chooseMapTip);
      return; //skip 2D
    }
    this.checkErrorInConfig();

    this.setStateErrorTip(''); //ok, so clean errortip

    //cache view id
    if (this.usingSceneviewId !== sceneView.id) {
      this.onClearBtnClick(); //change init map in setting, need to remove feature drew
    }
    this.usingSceneviewId = sceneView.id;

    if (this.pointerMoveEvent) {
      this.pointerMoveEvent.remove();
      this.pointerMoveEvent = null;
    }
    if (this.pointerDownEvent) {
      this.pointerDownEvent.remove();
      this.pointerDownEvent = null;
    }

    this.sceneView = sceneView.view; //activeMap.view;

    var debug3d = false;
    this.drawHelper = new DrawHelper({ sceneView: this.sceneView });
    this.rotationFlyController = new RotatingFly({
      sceneView: this.sceneView,
      debug: debug3d,
      widget: this
    });
    this.curveFlyController = new CurveFly({
      sceneView: this.sceneView,
      debug: debug3d,
      widget: this
    });
    this.lineFlyController = new LineFly({
      sceneView: this.sceneView,
      debug: debug3d,
      widget: this
    });

    //picking
    this._initPicking();
  }


  // componentDidMount() {
  //   //this.resetState();
  // }
  componentWillUnmount() {
    this.clearUIStateAndEvents();
    //if(controller){ controller.destructor}//TODO destructor !!!
  }
  componentDidUpdate(prevProps: AllWidgetProps<IMConfig>/* & ExtraProps*/, prevState: State) {
    if (null === this.state.flyStyleActived) {
      this.onFlyStyleChange(this._getCurrentItemStyle(), 0); //init
    }

    if (this.props.config !== prevProps.config) {
      if (this.firstConfigItem) {
        this.onFlyStyleChange(this.firstConfigItem.name, 0);
      }

      this.disableIsDrawing(); //drawing
      //picking
      this.onClearBtnClick();
    }
  }
  // componentDidUpdate (prevProps, prevState, snapshot) {//componentWillReceiveProps() {
  //   //return 
  //   prevState = this.getInitState();
  // }


  //0 state
  clearUIStateAndEvents = (actionList?) => {
    //this._cleanMapClickEvent();
    this._cleanMapDrawEvent();
    //this.drawHelper.revertMapCursor();
  }
  //0.1
  onFlyStyleChange = (/*e, */mode: FlyStyle, idx) => {
    this.clearUIStateAndEvents();
    this.onClearBtnClick();
    this.setState({ flyStyleActived: mode, isDrawing: false, isPicking: false, activedItemIdx: idx })
  }
  //0.2
  onIsDrawingChange = (mode) => {
    if (true === this.state.isPlaying) {
      return;
    }

    if (true === this.state.isDrawing) {
      this.clearUIStateAndEvents();
    }

    if (true === !this.state.isDrawing) {
      this.setState({ isPicking: false })
    }
    this.setState({ isDrawing: !this.state.isDrawing })

    if ('point' === mode) {//TODO for beta2 
      this.onDrawPoint();
    } else {
      this.onDrawLine();
    }
  }
  disableIsDrawing = () => {//TODO merge to onIsDrawingChange
    if (true === this.state.isDrawing) {
      this.clearUIStateAndEvents();
    }

    this.setState({ isDrawing: false/*, isPicking: false*/ })//just turn off drawing
  }
  //0.3
  onPickBtnClick = () => {
    if (this.highlightHelper) {
      this.highlightHelper.setMapPopupStateByIsPicking(this.state.isPicking);
    }

    var picking = !!this.state.isPicking;
    if (false === picking) {
      this.clearUIStateAndEvents();
      //this.highlightHelper.clearAll();
    }

    if (true === !picking) {
      this.setState({ isDrawing: false });
    }
    this.setState({ isPicking: !picking });
  }
  //0.4
  onPlayStateBtnClick = () => {
    var flyController = this._getCurrentFlyController();

    if (false === this.state.isPlaying) {
      if (!flyController.isEnableToFly() || !this.state.isCachedGeo) {
        return; //skip
      }
      this.onPlay();
    } else {
      if (!flyController.isEnableToPause()) {
        return; //skip
      }
      this.onPause();
    }

    this.setState({ isPlaying: !this.state.isPlaying, isDrawing: false, isPicking: false })
  }
  //for cb
  onFlyPause = () => {
    this.onPause();
    this.setState({ isPlaying: false })
  }
  setPlayStatePlaying = (state) => {
    this.setState({ isPlaying: !!state, isDrawing: false, isPicking: false })
  }


  //state
  _getCurrentItemStyle = () => {
    var style = this.state.flyStyleActived;
    if (null === this.firstConfigItem) {
      style = null; //no items
    } else if (null === this.state.flyStyleActived) {
      style = this.firstConfigItem.name; //already choose a style
    }
    return style;
  }
  _getCurrentActiveItem = () => {
    var itemInConfig = this.props.config.itemsList[this.state.activedItemIdx];
    return itemInConfig;
  }
  _getCurrentFlyController = () => {
    if (FlyStyle.Rotate === this.state.flyStyleActived) {
      //1. point
      if (this.rotationFlyController) {
        return this.rotationFlyController;
      }
    } else if (FlyStyle.Path === this.state.flyStyleActived) {
      //2. line
      var mode = this._getPathStyleByItem();
      if (PathStyle.Smoothed === mode) {
        if (this.curveFlyController) {
          return this.curveFlyController;
        }
      } else if (PathStyle.RealPath === mode) {
        if (this.lineFlyController) {
          return this.lineFlyController;
        }
      }
    }

    return null;
  }

  //cache
  _cachePickedHighlight = (hanlder, geo) => {
    if (this.geoCache.pickedHightlightHanlder) {
      this.geoCache.pickedHightlightHanlder.remove();
    }
    this.geoCache.pickedHightlightGeo = null;
    this.geoCache.pickedHightlightHanlder = null;
    this.setState({ isCachedGeo: false });

    if (hanlder && geo) {
      this.geoCache.pickedHightlightGeo = geo;
      this.geoCache.pickedHightlightHanlder = hanlder;
      this.setState({ isCachedGeo: true });
    }
  }
  // _haveHightlightGeo = () => {
  //   return (this.geoCache && this.geoCache.pickedHightlightGeo);
  // }
  _cacheHoverHighlight = (hanlder, geo) => {
    if (this.geoCache.hoverHightlightHanlder) {
      this.geoCache.hoverHightlightHanlder.remove();
    }
    this.geoCache.hoverHightlightGeo = null;
    this.geoCache.hoverHightlightHanlder = null;

    this.geoCache.hoverHightlightGeo = geo;
    this.geoCache.hoverHightlightHanlder = hanlder;
  }
  //Highlight
  markGraphic = (graphic, cameraInfo?) => {
    this._cachePickedHighlight(null, null);
    this.sceneView.whenLayerView(graphic.layer).then((layerView) => {
      this._cachePickedHighlight(layerView.highlight(graphic), graphic);
    });

    var item = this._getCurrentActiveItem();
    var options = {};
    if (item && item.direction) {
      options = Object.assign(options, { direction: item.direction });
    }
    if (cameraInfo) {
      options = Object.assign(options, { cameraInfo: cameraInfo });
    }

    var flyStyle = this._getCurrentItemStyle();
    var flyController = this._getCurrentFlyController();
    if (flyStyle === FlyStyle.Rotate) {
      //1.Point
      var point /*: typeof PointType*/ = graphic.geometry;
      var screenPoint = this.sceneView.toScreen(point);
      this.sceneView.hitTest(screenPoint, { exclude: [this.sceneView.graphics] }).then((hitTestResult) => {
        var lastHit = utils.getHitPointOnTheGround(hitTestResult);
        // this.rotationFlyController.prepareFly(lastHit, options).then(() => {
        //   //this.rotationFlyController.fly();
        //   //this.setPlayStatePlaying(true)
        // });
        flyController.prepareFly(lastHit, options);
      });
    } else if (flyStyle === FlyStyle.Path) {
      //2. Line
      var line = graphic.geometry;
      //var mode = this._getPathStyleByItem();
      flyController.prepareFly(line, options);
      // if (PathStyle.Smoothed === mode) {
      //   this.curveFlyController.prepareFly(line).then(() => {
      //     //this.curveFlyController.fly();
      //     //this.setPlayStatePlaying(true);
      //   });
      // } else if (PathStyle.RealPath === mode) {
      //   this.lineFlyController.prepareFly(line).then(() => {
      //     //this.lineFlyController.fly();
      //     //this.setPlayStatePlaying(true);
      //   });
      // }
    }
  }


  //1 fly mode
  getFlyStyleContent = () => {
    var flyStyle = this._getCurrentItemStyle();

    var flyStyleContent = null;
    if (flyStyle === FlyStyle.Rotate) {
      flyStyleContent = <Icon icon={rotateIconImage} color={getIconColor(this.props.theme)} />
    } else if (flyStyle === FlyStyle.Path) {
      flyStyleContent = <Icon icon={pathIconImage} color={getIconColor(this.props.theme)} />
    }

    return flyStyleContent;
  }
  toggleFlyStylePopup = () => {
    if (utils.getEnabledItemNum(this.props.config.itemsList) <= 1) {
      this.setState({ isFlyStylePopupOpen: false });
      return; //no dropdown if itemlist.length < 2
    }

    if (this.state.isPlaying) {
      return; //no disable in dropdown
    }
    this.setState({ isFlyStylePopupOpen: !this.state.isFlyStylePopupOpen });
  }


  //2 trigger action
  getTriggerActionContent = () => {
    let drawPointIconImage = require('../assets/icons/trigger-draw-point.svg');
    let drawLineIconImage = require('../assets/icons/trigger-draw-line.svg');
    var triggerActionContent = null;

    var isDrawActive = this.state.isDrawing;
    // if (false === isDrawActive && this.drawHelper) {
    //   //this._cleanMapClickEvent();
    //   //this.drawHelper.clearAll();
    // }
    var isDisable = this.state.isPlaying; //disabled

    var pointTips = this.props.intl.formatMessage({ id: 'drawPoint', defaultMessage: nls.triggerSelectPoint });
    var lineTips = this.props.intl.formatMessage({ id: 'drawLine', defaultMessage: nls.triggerDrwaPath });

    var flyStyle = this._getCurrentItemStyle();
    if (flyStyle === FlyStyle.Rotate) {
      triggerActionContent =
        <Button style={getBtnStyle(this.props.theme, isDrawActive)} active={isDrawActive} onClick={() => this.onIsDrawingChange('point')} disabled={isDisable} title={pointTips}>
          <Icon icon={drawPointIconImage} color={getIconColor(this.props.theme)} />
        </Button>
    } else if (flyStyle === FlyStyle.Path) {
      triggerActionContent =
        <Button style={getBtnStyle(this.props.theme, isDrawActive)} active={isDrawActive} onClick={() => this.onIsDrawingChange('line')} disabled={isDisable} title={lineTips}>
          <Icon icon={drawLineIconImage} color={getIconColor(this.props.theme)} />
        </Button>
    }

    return triggerActionContent;
  }
  //2.1
  _cleanMapDrawEvent = () => {
    if (this.drawHelper) {
      this.drawHelper.cancel();
      this.drawHelper.cleanHanlder(); // delete point drawed
    }
  }
  onDrawPoint = () => {
    this.clearUIStateAndEvents();

    if (true === this.state.isPlaying) {
      return;
    }
    if (true === this.state.isDrawing) {
      return;
    }

    this.drawHelper.drawPoint().then(((res: DrawRes) => {
      //this._cachePoint(lastHit, "draw");
      this.markGraphic(res.graphic, res.cameraInfo);

      this.disableIsDrawing();
    }), (e => {
      this.disableIsDrawing();
    }));
  }
  _getPathStyleByItem() {
    var item = this._getCurrentActiveItem();
    var mode = PathStyle.Smoothed; //default
    if (item && item.name === FlyStyle.Path && item.style) {
      mode = item.style;
    }
    return mode;
  }
  onDrawLine = () => {
    this.clearUIStateAndEvents();

    if (true === this.state.isPlaying) {
      return;
    }
    if (true === this.state.isDrawing) {
      return;
    }

    var flyController = this._getCurrentFlyController();

    this.drawHelper.drawLine(flyController).then(((res: DrawRes) => {
      //this._cacheLine(geo, "draw");
      this.markGraphic(res.graphic, res.cameraInfo);

      this.disableIsDrawing();
    }), (e => {
      this.disableIsDrawing();
    }));
  }
  //2.2 picking
  _initPicking = () => {
    this.highlightHelper = new HighlightHelper({ sceneView: this.sceneView });
    this.pointerMoveEvent = this.sceneView.on('pointer-move', this.pointerMoveHandler);
    this.pointerDownEvent = this.sceneView.on('pointer-down', this.pointerClickHandler);
  }
  pointerMoveHandler = (event) => {
    if (true === this.state.isPicking) {
      this.highlightHelper.mapPopupDisable(); //TODO for beta2 bug

      this.sceneView.hitTest(event).then(this.highlightGraphicsByPick);
    }
  }
  highlightGraphicsByPick = (response) => {
    var results = response.results;
    if (results && results.length > 0) {
      var res0 = results[0];
      var graphic = res0.graphic;
      var type = graphic.geometry.type;

      if (type === 'polyline' && (this.state.flyStyleActived === FlyStyle.Path)) {
        this.sceneView.whenLayerView(graphic.layer).then((layerView) => {
          this._cacheHoverHighlight(layerView.highlight(graphic), graphic);
        });
      } else if (type === 'point' && (this.state.flyStyleActived === FlyStyle.Rotate)) {
        this.sceneView.whenLayerView(graphic.layer).then((layerView) => {
          this._cacheHoverHighlight(layerView.highlight(graphic), graphic);
        });
      }
    }
  }
  pointerClickHandler = (event) => {
    if (true === this.state.isPicking) {
      this.sceneView.hitTest(event).then(res => this.clickGraphicsByPick(res, event));
    }
  }
  clickGraphicsByPick = (response, event) => {
    var results = response.results;
    if (results && results.length > 0) {
      var res0 = results[0];
      var graphic = res0.graphic;
      var type = graphic.geometry.type;

      if (type === 'polyline') {
        if (utils.isPolylineEquals(graphic.geometry, this.geoCache.hoverHightlightGeo.geometry)) {
          event.stopPropagation();
          //console.log('click highlight==> line');
          // this._cachePickedHighlight(null, null);
          // this.sceneView.whenLayerView(graphic.layer).then((layerView)=>{
          //   this._cachePickedHighlight(layerView.highlight(graphic), graphic);
          // });
          // this._cacheLine(graphic.geometry, "pick");
          var flyController = this._getCurrentFlyController();
          flyController.getHitTestInfo(graphic);
          this.markGraphic(graphic);
        }
      } else if (type === 'point') {
        if (graphic.geometry.equals(this.geoCache.hoverHightlightGeo.geometry)) {
          event.stopPropagation();
          //console.log('click highlight==> point');
          // this._cachePickedHighlight(null, null);
          // this.sceneView.whenLayerView(graphic.layer).then((layerView)=>{
          //   this._cachePickedHighlight(layerView.highlight(graphic), graphic);
          // });
          this.markGraphic(graphic);
          // this.sceneView.hitTest(event, { exclude: [this.sceneView.graphics] }).then((hitTestResult) => {
          //   var lastHit = utils.getHitPointOnTheGround(hitTestResult);
          //   this._cachePoint(lastHit, "pick");
          // });
        }
      }
    }
  }
  //2.3 clearBtn
  onClearBtnClick = () => {
    //console.log('clear btn==>');
    this._cachePickedHighlight(null, null);
    this._cacheHoverHighlight(null, null);
    //clear draw res
    if (this.drawHelper) {
      this.drawHelper.clearAllGraphics();
    }
    //this.highlightHelper.clearAll();
    //clear play res
    this.onStop();
  }


  //3 setting


  //4 play state


  // onClearButtonClick = (evt) => {
  //   this.onStop();
  //   this.onClearBtnClick();
  // }
  //use pickedHightlightGeo state to fly
  onPlay = () => {
    if (!this.state.isCachedGeo) {
      return;
    }

    var flyController = this._getCurrentFlyController();
    if (flyController.isEnableToFly()) {
      flyController.fly();
      this.setPlayStatePlaying(true);
    }
  }
  onStop = () => {
    if (!this._getCurrentFlyController()) {
      return;
    }

    if (this.rotationFlyController.state.flyState !== FlyState.STOPPED) {
      this.rotationFlyController.stop();
    }
    if (this.curveFlyController.state.flyState !== FlyState.STOPPED) {
      this.curveFlyController.stop();
    }
    if (this.lineFlyController.state.flyState !== FlyState.STOPPED) {
      this.lineFlyController.stop();
    }
  }
  onPause = () => {
    if (this.rotationFlyController.state.flyState === FlyState.RUNNING) {
      this.rotationFlyController.pause();
    }
    if (this.curveFlyController.state.flyState === FlyState.RUNNING) {
      this.curveFlyController.pause();
    }
    if (this.lineFlyController.state.flyState === FlyState.RUNNING) {
      this.lineFlyController.pause();
    }
  }
  onResume = () => {
    if (this.rotationFlyController.state.flyState === FlyState.PAUSED) {
      this.rotationFlyController.resume();
    } else if (this.curveFlyController.state.flyState === FlyState.PAUSED) {
      this.curveFlyController.resume();
    } else if (this.lineFlyController.state.flyState === FlyState.PAUSED) {
      this.lineFlyController.resume();
    }
  }
  onClear = () => {
    if (this.rotationFlyController.state.flyState !== FlyState.RUNNING) {
      this.rotationFlyController.clear();
    }
    if (this.curveFlyController.state.flyState !== FlyState.RUNNING) {
      this.curveFlyController.clear();
    }
    if (this.lineFlyController.state.flyState !== FlyState.RUNNING) {
      this.lineFlyController.clear();
    }
  }


  /////////////
  //for render
  renderFlyStyleSelectorContent() {
    var itemsList = this.props.config.itemsList.asMutable();
    this.firstConfigItem = utils.findFirstUseItem(itemsList); //getDerivedStateFromProps 
    if (false === this.state.isPicking) {
      this._cacheHoverHighlight(null, null);
    }
    var isDisable = this.state.isPlaying;

    var styleTips = utils.getFlyStyleTitle(this._getCurrentItemStyle(), this.props);
    var rotateTips = this.props.intl.formatMessage({ id: 'flyStyleRotate', defaultMessage: nls.flyStyleRotate });
    var pathTips = this.props.intl.formatMessage({ id: 'flyStylePath', defaultMessage: nls.flyStylePath });

    var flyStyleContent = this.getFlyStyleContent();
    return <Dropdown nav isOpen={this.state.isFlyStylePopupOpen} toggle={this.toggleFlyStylePopup} selectable>
      <DropdownToggle nav caret title={styleTips} className={getDropdownTogglerClass(isDisable)}> {flyStyleContent} </DropdownToggle>
      <DropdownMenu css={getDropdownStyle(this.props.theme)}>
        {itemsList.map((itemConfig, idx) => {
          const style = itemConfig.name;
          var isActived = !!(idx === this.state.activedItemIdx);
          if (!itemConfig.isInUse) {
            return null;
          }

          if (FlyStyle.Rotate === style) {
            return <div key={idx}>
              <DropdownItem onClick={() => this.onFlyStyleChange(FlyStyle.Rotate, idx)} style={getBgStyle(this.props.theme)} disabled={isDisable} active={isActived}>
                <Icon icon={rotateIconImage} color={getIconColor(this.props.theme)} />
                <span className="mx-2">{rotateTips}</span>
              </DropdownItem>
            </div>
          } else if (FlyStyle.Path === style) {
            return <div key={idx}>
              <DropdownItem onClick={() => this.onFlyStyleChange(FlyStyle.Path, idx)} style={getBgStyle(this.props.theme)} disabled={isDisable} active={isActived}>
                <Icon icon={pathIconImage} color={getIconColor(this.props.theme)} />
                <span className="mx-2">{pathTips}</span>
              </DropdownItem>
            </div>
          }
        })}
      </DropdownMenu>
    </Dropdown>
  }
  renderSeparator() {
    return <div className="separator-line"></div>
  }
  renderTriggerActionContent() {
    let pickIconImage = require('../assets/icons/trigger-pick.svg');
    let clearIconImage = require('../assets/icons/trigger-clear.svg');
    var triggerActionContent = this.getTriggerActionContent();
    var separator = this.renderSeparator();
    var isPickingActive = this.state.isPicking;
    if (false === isPickingActive) {
      this._cacheHoverHighlight(null, null);
    }
    var isDisable = this.state.isPlaying; //disabled={isDisable}

    var pickTips = this.props.intl.formatMessage({ id: 'pickTips', defaultMessage: nls.triggerSelectFeature });
    var clearTips = this.props.intl.formatMessage({ id: 'clearTips', defaultMessage: nls.triggerClear });

    return <div className="d-flex">
      {separator}
      {triggerActionContent}

      <Button style={getBtnStyle(this.props.theme, isPickingActive)} onClick={this.onPickBtnClick} active={isPickingActive} disabled={isDisable} title={pickTips}>
        <Icon icon={pickIconImage} color={getIconColor(this.props.theme)} />
      </Button>
      <Button style={getBtnStyle(this.props.theme)} onClick={this.onClearBtnClick} disabled={isDisable} title={clearTips}>
        <Icon icon={clearIconImage} color={getIconColor(this.props.theme)} />
      </Button>

      {separator}
    </div>
  }
  renderRuntimeSettingContent() {
    let SettingIconImage = require('../assets/icons/setting.svg');
    return <div>
      <Dropdown nav isOpen={this.state.isSettingPopupOpen}
        toggle={() => { this.setState({ isSettingPopupOpen: !this.state.isSettingPopupOpen }) }} >
        <DropdownToggle nav caret> <Icon icon={SettingIconImage} color={getIconColor(this.props.theme)} /> </DropdownToggle>
        <DropdownMenu appendTo="body">
          <DropdownItem style={getBgStyle(this.props.theme)}>
            <div className="d-flex flex-column">
              <div><Label for="setting-speed" style={{ cursor: 'pointer' }} color="light"> Speed </Label></div>
              <div><Input id="setting-speed" type="range" value={1} min={0} max={2} step={0.2} style={{ width: '150px' }} bsSize="sm" /></div>
            </div>
          </DropdownItem>
          <DropdownItem style={getBgStyle(this.props.theme)}>
            <div className="d-flex flex-column">
              <div><Label for="setting-speed" style={{ cursor: 'pointer' }} color="light"> Tilt </Label></div>
              <div><Input id="setting-speed" type="range" value={45} min={0} max={90} step={0.5} style={{ width: '150px' }} bsSize="sm" /></div>
            </div>
          </DropdownItem>
          <DropdownItem style={getBgStyle(this.props.theme)}>
            <div className="d-flex flex-column">
              <div><Label for="setting-att" style={{ cursor: 'pointer' }}> Attitude </Label></div>
              <div><Input id="setting-att" type="range" value="40" style={{ width: '150px' }} bsSize="sm" /></div>
              <div className="d-flex justify-content-between">
                <div>Ground</div><div>Space</div>
              </div>
            </div>
          </DropdownItem>
          <DropdownItem style={getBgStyle(this.props.theme)}>
            <Input style={{ width: '50px' }} bsSize="sm" value="40" />
          //TODO selector
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  }
  renderPlayStateContent() {
    let playIconImage = require('../assets/icons/play.svg');
    let pauseIconImage = require('../assets/icons/pause.svg');
    var tips = null;
    var isDisable = !this.state.isCachedGeo;

    var iconContent = null;
    if (this.state.isPlaying) {
      iconContent = <Icon icon={pauseIconImage} color={getIconColor(this.props.theme)} />
      tips = this.props.intl.formatMessage({ id: 'pause', defaultMessage: nls.pause });
    } else {
      iconContent = <Icon icon={playIconImage} color={getIconColor(this.props.theme)} />
      tips = this.props.intl.formatMessage({ id: 'play', defaultMessage: nls.play });
    }

    return <Button onClick={this.onPlayStateBtnClick} style={getBtnStyle(this.props.theme)} title={tips} disabled={isDisable}>
      {iconContent}
    </Button>
  }
  renderProgressBarContent() {
    if (this.state.flyStyleActived === FlyStyle.Path) {
      return <Progress className="w-100" style={{ height: '4px' }} color="info" value="50" />
    } else {
      return null;
    }
  }
  //errorContent
  // renderErrorContent(str, isHide?) {
  //   var styleObj = {display: 'flex', backgroundColor: this.props.theme.colors.palette.light[200], height: '32px', justifyContent: 'center', alignItems: 'center'};
  //   if (isHide) {
  //     styleObj = Object.assign(styleObj, { display: 'none' });
  //   }
  //   return <div className="fly-error-tips" style={styleObj}>{str}</div>;
  // }
  renderWidgetPlaceholder() {
    return <WidgetPlaceholder icon={flyIcon} widgetId={this.props.id} message={this.state.errorTip} />;
  }

  render() {
    const useMapWidget = this.props.useMapWidgetIds && this.props.useMapWidgetIds[0];
    // const theme = ThemeManager.getInstance().loadTheme('builder/themes/dark/').then((buidlerTheme) => {
    //   console.log(ThemeManager.getInstance().getThemeVariables('builder/themes/dark/'));
    // }).catch(err => {
    //   console.error('Initializing theme manager failed.', err);
    // })
    var mapContent = null;
    if (useMapWidget) {
      mapContent = <JimuMapViewComponent useMapWidgetIds={this.props.useMapWidgetIds} onActiveViewChange={this.onActiveViewChange} />
    } else {
      this.setStateErrorTip(this.chooseMapTip);
    }
    this.checkErrorInConfig();

    //var isShowError = this.state.errorTip ? true : false;
    //var controllerDisplay = this.state.errorTip ? 'none' : 'flex';
    //var errorTipContent = this.renderErrorContent(this.state.errorTip, !isShowError);
    var flyStyleSelectorContent = this.renderFlyStyleSelectorContent();
    var triggerActionContent = this.renderTriggerActionContent();
    var runtimeSettingContent = null; //this.renderRuntimeSettingContent();
    var playStateContent = this.renderPlayStateContent();
    var progressBar = null; //this.renderProgressBarContent();

    var flyControllerContent = null;
    if (!useMapWidget || this.state.errorTip) {
      flyControllerContent = this.renderWidgetPlaceholder();
    } else {
      flyControllerContent = <div className="fly-wapper d-flex">
        <Nav navbar className="fly-silder">
          <div className="progress-bar-wapper">
            {progressBar}
          </div>
          <div className="items d-flex flex-row">
            <div className="item">
              {flyStyleSelectorContent}
            </div>
            <div className="item">
              {triggerActionContent}
            </div>
            <div className="item">
              {runtimeSettingContent}
            </div>
            <div className="item">
              {playStateContent}
            </div>
          </div>
        </Nav>
      </div>
    }

    return <div css={getStyle(this.props.theme)} className="d-flex align-items-center justify-content-center">
      {flyControllerContent}

      <div className="fly-map">
        <div>{mapContent}</div>
      </div>
    </div>
  }
}