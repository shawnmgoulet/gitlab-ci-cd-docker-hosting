import { ImmutableObject/*, ImmutableArray*/ } from 'jimu-core';

//0
export enum FlyStyle {
  Rotate = 'ROTATE',
  Path = 'PATH'
}

//1
export interface FlyStyleConstraint {
  name: FlyStyle,
  isInUse: boolean,
  direction: RotateDirection | PathDirection
}
//1.1
export interface RotateStyleConstraint extends FlyStyleConstraint {
  name: FlyStyle.Rotate,
  isInUse: boolean,
  direction: RotateDirection
}
export enum RotateDirection {
  CW = 'CW', //Clockwise
  CCW = 'CCW'//CounterClockwise
}
//1.2
export interface PathStyleConstraint extends FlyStyleConstraint {
  name: FlyStyle.Path,
  isInUse: boolean,
  direction: PathDirection
  style: PathStyle,
}
export enum PathStyle {
  Smoothed = 'CURVED',
  RealPath = 'LINE'
}
export enum PathDirection {
  Forward = 'FORWARD',
  Backward = 'BACKWARD'
}


//2
export enum ControllerLayout {
  Horizontal = 'HORIZONTAL',
  Vertical = 'VERTICAL'
}


//config
export interface Config {
  //1 map
  //useMapWidgetIds: string[];
  //2 Rotate/Path config
  itemsList: (RotateStyleConstraint | PathStyleConstraint)[];
  //3 UI
  layout: ControllerLayout;
}
export type IMConfig = ImmutableObject<Config>;