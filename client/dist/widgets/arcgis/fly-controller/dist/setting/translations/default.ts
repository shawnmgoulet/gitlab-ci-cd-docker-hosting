export default {
  selectMap: 'Select a map with 3D data',
  selectStyle: 'Select fly style',

  flyStyleRotate: 'Rotate fly',
  flyStylePath: 'Fly along path',

  styleLabelRotate: 'Rotate direction',
  styleLabelPath: 'Path style',

  CW: 'Clockwise',
  CCW: 'Counterclockwise',

  pathTypeSmoothedCurve: 'Smoothed curve',
  pathTypeRealPath: 'Real path'
}
