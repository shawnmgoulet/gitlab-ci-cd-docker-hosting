export default {
  sourceLabel: 'Source',
  sourceDescript: 'Select a web map/web scene, or any combination of two.',
  options: 'Options',
  showBaseMap: 'Show basemap legends',
  cardStyle: 'Use card style',
  auto: 'Auto',
  sideBySide: 'Side by side',
  stack: 'Stack',
  selectMapWidget: 'Select a Map widget'

}
