define({
  auto: '須_Auto_________鷗',
  custom: '須_Custom_____________鷗',
  certainly: '須_OK_____鷗',
  autoEnabledTip: '須_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________鷗.',
  autoDisabledTip: '須_Click to enable auto layout_____________________________鷗.',
  /* tslint:disable-next-line */
  customEnabledTip: '須_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________鷗.',
  customDisabledTip: '須_Click to enable custom layout_______________________________鷗.',
  confirm: '須_Confirm_______________鷗',
  enableConfirm: '須_Are you sure you want to enable it___________________鷗?',
  autoConfirmMsg: '須_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________鷗.',
  customConfirmMsg1: '須_By enabling this option, you can manually arrange widgets for this device mode_________________________________________鷗.',
  /* tslint:disable-next-line */
  customConfirmMsg2: '須_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________鷗.',
  dragToResize: '須_Drag to resize_______________鷗',
  largeScreen: '須_Large screen device____________________鷗',
  mediumScreen: '須_Medium screen device_____________________鷗',
  smallScreen: '須_Small screen device____________________鷗',
});