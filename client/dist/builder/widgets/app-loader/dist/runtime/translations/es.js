define({
  auto: 'á_Auto_________Ó',
  custom: 'á_Custom_____________Ó',
  certainly: 'á_OK_____Ó',
  autoEnabledTip: 'á_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Ó.',
  autoDisabledTip: 'á_Click to enable auto layout_____________________________Ó.',
  /* tslint:disable-next-line */
  customEnabledTip: 'á_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Ó.',
  customDisabledTip: 'á_Click to enable custom layout_______________________________Ó.',
  confirm: 'á_Confirm_______________Ó',
  enableConfirm: 'á_Are you sure you want to enable it___________________Ó?',
  autoConfirmMsg: 'á_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Ó.',
  customConfirmMsg1: 'á_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Ó.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'á_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Ó.',
  dragToResize: 'á_Drag to resize_______________Ó',
  largeScreen: 'á_Large screen device____________________Ó',
  mediumScreen: 'á_Medium screen device_____________________Ó',
  smallScreen: 'á_Small screen device____________________Ó',
});