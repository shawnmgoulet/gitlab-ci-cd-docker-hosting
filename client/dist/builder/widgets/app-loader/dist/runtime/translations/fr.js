define({
  auto: 'æ_Auto_________Â',
  custom: 'æ_Custom_____________Â',
  certainly: 'æ_OK_____Â',
  autoEnabledTip: 'æ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Â.',
  autoDisabledTip: 'æ_Click to enable auto layout_____________________________Â.',
  /* tslint:disable-next-line */
  customEnabledTip: 'æ_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Â.',
  customDisabledTip: 'æ_Click to enable custom layout_______________________________Â.',
  confirm: 'æ_Confirm_______________Â',
  enableConfirm: 'æ_Are you sure you want to enable it___________________Â?',
  autoConfirmMsg: 'æ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Â.',
  customConfirmMsg1: 'æ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Â.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'æ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Â.',
  dragToResize: 'æ_Drag to resize_______________Â',
  largeScreen: 'æ_Large screen device____________________Â',
  mediumScreen: 'æ_Medium screen device_____________________Â',
  smallScreen: 'æ_Small screen device____________________Â',
});