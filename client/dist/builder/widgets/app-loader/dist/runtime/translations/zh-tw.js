define({
  auto: '試_Auto_________驗',
  custom: '試_Custom_____________驗',
  certainly: '試_OK_____驗',
  autoEnabledTip: '試_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________驗.',
  autoDisabledTip: '試_Click to enable auto layout_____________________________驗.',
  /* tslint:disable-next-line */
  customEnabledTip: '試_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________驗.',
  customDisabledTip: '試_Click to enable custom layout_______________________________驗.',
  confirm: '試_Confirm_______________驗',
  enableConfirm: '試_Are you sure you want to enable it___________________驗?',
  autoConfirmMsg: '試_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________驗.',
  customConfirmMsg1: '試_By enabling this option, you can manually arrange widgets for this device mode_________________________________________驗.',
  /* tslint:disable-next-line */
  customConfirmMsg2: '試_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________驗.',
  dragToResize: '試_Drag to resize_______________驗',
  largeScreen: '試_Large screen device____________________驗',
  mediumScreen: '試_Medium screen device_____________________驗',
  smallScreen: '試_Small screen device____________________驗',
});