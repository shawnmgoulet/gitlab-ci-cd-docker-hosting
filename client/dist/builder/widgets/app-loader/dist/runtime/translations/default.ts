export default {
  auto: 'Auto',
  custom: 'Custom',
  certainly: 'OK',
  autoEnabledTip: 'Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically.',
  autoDisabledTip: 'Click to enable auto layout.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel.',
  customDisabledTip: 'Click to enable custom layout.',
  confirm: 'Confirm',
  enableConfirm: 'Are you sure you want to enable it?',
  autoConfirmMsg: 'By enabling this option, the widgets will be synced with those on the {label} and arranged automatically.',
  customConfirmMsg1: 'By enabling this option, you can manually arrange widgets for this device mode.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel.',
  dragToResize: 'Drag to resize',
  largeScreen: 'Large screen device',
  mediumScreen: 'Medium screen device',
  smallScreen: 'Small screen device',
}
