define({
  auto: 'ґ_Auto_________Ї',
  custom: 'ґ_Custom_____________Ї',
  certainly: 'ґ_OK_____Ї',
  autoEnabledTip: 'ґ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Ї.',
  autoDisabledTip: 'ґ_Click to enable auto layout_____________________________Ї.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ґ_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Ї.',
  customDisabledTip: 'ґ_Click to enable custom layout_______________________________Ї.',
  confirm: 'ґ_Confirm_______________Ї',
  enableConfirm: 'ґ_Are you sure you want to enable it___________________Ї?',
  autoConfirmMsg: 'ґ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Ї.',
  customConfirmMsg1: 'ґ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Ї.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ґ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Ї.',
  dragToResize: 'ґ_Drag to resize_______________Ї',
  largeScreen: 'ґ_Large screen device____________________Ї',
  mediumScreen: 'ґ_Medium screen device_____________________Ї',
  smallScreen: 'ґ_Small screen device____________________Ї',
});