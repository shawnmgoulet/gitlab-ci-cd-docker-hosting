define({
  auto: 'ķ_Auto_________ū',
  custom: 'ķ_Custom_____________ū',
  certainly: 'ķ_OK_____ū',
  autoEnabledTip: 'ķ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ū.',
  autoDisabledTip: 'ķ_Click to enable auto layout_____________________________ū.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ķ_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ū.',
  customDisabledTip: 'ķ_Click to enable custom layout_______________________________ū.',
  confirm: 'ķ_Confirm_______________ū',
  enableConfirm: 'ķ_Are you sure you want to enable it___________________ū?',
  autoConfirmMsg: 'ķ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ū.',
  customConfirmMsg1: 'ķ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ū.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ķ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ū.',
  dragToResize: 'ķ_Drag to resize_______________ū',
  largeScreen: 'ķ_Large screen device____________________ū',
  mediumScreen: 'ķ_Medium screen device_____________________ū',
  smallScreen: 'ķ_Small screen device____________________ū',
});