define({
  auto: 'ø_Auto_________å',
  custom: 'ø_Custom_____________å',
  certainly: 'ø_OK_____å',
  autoEnabledTip: 'ø_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________å.',
  autoDisabledTip: 'ø_Click to enable auto layout_____________________________å.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ø_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________å.',
  customDisabledTip: 'ø_Click to enable custom layout_______________________________å.',
  confirm: 'ø_Confirm_______________å',
  enableConfirm: 'ø_Are you sure you want to enable it___________________å?',
  autoConfirmMsg: 'ø_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________å.',
  customConfirmMsg1: 'ø_By enabling this option, you can manually arrange widgets for this device mode_________________________________________å.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ø_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________å.',
  dragToResize: 'ø_Drag to resize_______________å',
  largeScreen: 'ø_Large screen device____________________å',
  mediumScreen: 'ø_Medium screen device_____________________å',
  smallScreen: 'ø_Small screen device____________________å',
});