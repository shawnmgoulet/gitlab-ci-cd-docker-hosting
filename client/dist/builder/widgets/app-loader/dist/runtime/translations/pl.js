define({
  auto: 'ł_Auto_________ą',
  custom: 'ł_Custom_____________ą',
  certainly: 'ł_OK_____ą',
  autoEnabledTip: 'ł_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ą.',
  autoDisabledTip: 'ł_Click to enable auto layout_____________________________ą.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ł_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ą.',
  customDisabledTip: 'ł_Click to enable custom layout_______________________________ą.',
  confirm: 'ł_Confirm_______________ą',
  enableConfirm: 'ł_Are you sure you want to enable it___________________ą?',
  autoConfirmMsg: 'ł_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ą.',
  customConfirmMsg1: 'ł_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ą.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ł_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ą.',
  dragToResize: 'ł_Drag to resize_______________ą',
  largeScreen: 'ł_Large screen device____________________ą',
  mediumScreen: 'ł_Medium screen device_____________________ą',
  smallScreen: 'ł_Small screen device____________________ą',
});