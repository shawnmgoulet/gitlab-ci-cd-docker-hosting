define({
  auto: '试_Auto_________验',
  custom: '试_Custom_____________验',
  certainly: '试_OK_____验',
  autoEnabledTip: '试_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________验.',
  autoDisabledTip: '试_Click to enable auto layout_____________________________验.',
  /* tslint:disable-next-line */
  customEnabledTip: '试_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________验.',
  customDisabledTip: '试_Click to enable custom layout_______________________________验.',
  confirm: '试_Confirm_______________验',
  enableConfirm: '试_Are you sure you want to enable it___________________验?',
  autoConfirmMsg: '试_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________验.',
  customConfirmMsg1: '试_By enabling this option, you can manually arrange widgets for this device mode_________________________________________验.',
  /* tslint:disable-next-line */
  customConfirmMsg2: '试_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________验.',
  dragToResize: '试_Drag to resize_______________验',
  largeScreen: '试_Large screen device____________________验',
  mediumScreen: '试_Medium screen device_____________________验',
  smallScreen: '试_Small screen device____________________验',
});