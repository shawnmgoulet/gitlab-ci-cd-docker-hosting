define({
  auto: '한_Auto_________빠',
  custom: '한_Custom_____________빠',
  certainly: '한_OK_____빠',
  autoEnabledTip: '한_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________빠.',
  autoDisabledTip: '한_Click to enable auto layout_____________________________빠.',
  /* tslint:disable-next-line */
  customEnabledTip: '한_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________빠.',
  customDisabledTip: '한_Click to enable custom layout_______________________________빠.',
  confirm: '한_Confirm_______________빠',
  enableConfirm: '한_Are you sure you want to enable it___________________빠?',
  autoConfirmMsg: '한_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________빠.',
  customConfirmMsg1: '한_By enabling this option, you can manually arrange widgets for this device mode_________________________________________빠.',
  /* tslint:disable-next-line */
  customConfirmMsg2: '한_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________빠.',
  dragToResize: '한_Drag to resize_______________빠',
  largeScreen: '한_Large screen device____________________빠',
  mediumScreen: '한_Medium screen device_____________________빠',
  smallScreen: '한_Small screen device____________________빠',
});