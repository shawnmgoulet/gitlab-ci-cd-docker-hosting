define({
  auto: 'Ř_Auto_________ů',
  custom: 'Ř_Custom_____________ů',
  certainly: 'Ř_OK_____ů',
  autoEnabledTip: 'Ř_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ů.',
  autoDisabledTip: 'Ř_Click to enable auto layout_____________________________ů.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Ř_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ů.',
  customDisabledTip: 'Ř_Click to enable custom layout_______________________________ů.',
  confirm: 'Ř_Confirm_______________ů',
  enableConfirm: 'Ř_Are you sure you want to enable it___________________ů?',
  autoConfirmMsg: 'Ř_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ů.',
  customConfirmMsg1: 'Ř_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ů.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Ř_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ů.',
  dragToResize: 'Ř_Drag to resize_______________ů',
  largeScreen: 'Ř_Large screen device____________________ů',
  mediumScreen: 'Ř_Medium screen device_____________________ů',
  smallScreen: 'Ř_Small screen device____________________ů',
});