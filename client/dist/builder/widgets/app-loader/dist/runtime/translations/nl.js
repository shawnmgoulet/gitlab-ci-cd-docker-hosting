define({
  auto: 'Ĳ_Auto_________ä',
  custom: 'Ĳ_Custom_____________ä',
  certainly: 'Ĳ_OK_____ä',
  autoEnabledTip: 'Ĳ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ä.',
  autoDisabledTip: 'Ĳ_Click to enable auto layout_____________________________ä.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Ĳ_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ä.',
  customDisabledTip: 'Ĳ_Click to enable custom layout_______________________________ä.',
  confirm: 'Ĳ_Confirm_______________ä',
  enableConfirm: 'Ĳ_Are you sure you want to enable it___________________ä?',
  autoConfirmMsg: 'Ĳ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ä.',
  customConfirmMsg1: 'Ĳ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ä.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Ĳ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ä.',
  dragToResize: 'Ĳ_Drag to resize_______________ä',
  largeScreen: 'Ĳ_Large screen device____________________ä',
  mediumScreen: 'Ĳ_Medium screen device_____________________ä',
  smallScreen: 'Ĳ_Small screen device____________________ä',
});