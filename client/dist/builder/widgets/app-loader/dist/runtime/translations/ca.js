define({
  auto: 'ó_Auto_________à',
  custom: 'ó_Custom_____________à',
  certainly: 'ó_OK_____à',
  autoEnabledTip: 'ó_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________à.',
  autoDisabledTip: 'ó_Click to enable auto layout_____________________________à.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ó_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________à.',
  customDisabledTip: 'ó_Click to enable custom layout_______________________________à.',
  confirm: 'ó_Confirm_______________à',
  enableConfirm: 'ó_Are you sure you want to enable it___________________à?',
  autoConfirmMsg: 'ó_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________à.',
  customConfirmMsg1: 'ó_By enabling this option, you can manually arrange widgets for this device mode_________________________________________à.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ó_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________à.',
  dragToResize: 'ó_Drag to resize_______________à',
  largeScreen: 'ó_Large screen device____________________à',
  mediumScreen: 'ó_Medium screen device_____________________à',
  smallScreen: 'ó_Small screen device____________________à',
});