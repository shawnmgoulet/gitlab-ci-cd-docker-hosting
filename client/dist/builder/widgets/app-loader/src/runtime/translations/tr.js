define({
  auto: 'ı_Auto_________İ',
  custom: 'ı_Custom_____________İ',
  certainly: 'ı_OK_____İ',
  autoEnabledTip: 'ı_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________İ.',
  autoDisabledTip: 'ı_Click to enable auto layout_____________________________İ.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ı_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________İ.',
  customDisabledTip: 'ı_Click to enable custom layout_______________________________İ.',
  confirm: 'ı_Confirm_______________İ',
  enableConfirm: 'ı_Are you sure you want to enable it___________________İ?',
  autoConfirmMsg: 'ı_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________İ.',
  customConfirmMsg1: 'ı_By enabling this option, you can manually arrange widgets for this device mode_________________________________________İ.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ı_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________İ.',
  dragToResize: 'ı_Drag to resize_______________İ',
  largeScreen: 'ı_Large screen device____________________İ',
  mediumScreen: 'ı_Medium screen device_____________________İ',
  smallScreen: 'ı_Small screen device____________________İ',
});