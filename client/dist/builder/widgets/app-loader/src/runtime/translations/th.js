define({
  auto: 'ก้_Auto_________ษฺ',
  custom: 'ก้_Custom_____________ษฺ',
  certainly: 'ก้_OK_____ษฺ',
  autoEnabledTip: 'ก้_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ษฺ.',
  autoDisabledTip: 'ก้_Click to enable auto layout_____________________________ษฺ.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ก้_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ษฺ.',
  customDisabledTip: 'ก้_Click to enable custom layout_______________________________ษฺ.',
  confirm: 'ก้_Confirm_______________ษฺ',
  enableConfirm: 'ก้_Are you sure you want to enable it___________________ษฺ?',
  autoConfirmMsg: 'ก้_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ษฺ.',
  customConfirmMsg1: 'ก้_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ษฺ.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ก้_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ษฺ.',
  dragToResize: 'ก้_Drag to resize_______________ษฺ',
  largeScreen: 'ก้_Large screen device____________________ษฺ',
  mediumScreen: 'ก้_Medium screen device_____________________ษฺ',
  smallScreen: 'ก้_Small screen device____________________ษฺ',
});