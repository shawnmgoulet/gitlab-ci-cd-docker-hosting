define({
  auto: 'ng_Auto_________ny',
  custom: 'ng_Custom_____________ny',
  certainly: 'ng_OK_____ny',
  autoEnabledTip: 'ng_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ny.',
  autoDisabledTip: 'ng_Click to enable auto layout_____________________________ny.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ng_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ny.',
  customDisabledTip: 'ng_Click to enable custom layout_______________________________ny.',
  confirm: 'ng_Confirm_______________ny',
  enableConfirm: 'ng_Are you sure you want to enable it___________________ny?',
  autoConfirmMsg: 'ng_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ny.',
  customConfirmMsg1: 'ng_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ny.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ng_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ny.',
  dragToResize: 'ng_Drag to resize_______________ny',
  largeScreen: 'ng_Large screen device____________________ny',
  mediumScreen: 'ng_Medium screen device_____________________ny',
  smallScreen: 'ng_Small screen device____________________ny',
});