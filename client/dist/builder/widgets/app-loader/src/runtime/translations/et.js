define({
  auto: 'Š_Auto_________ä',
  custom: 'Š_Custom_____________ä',
  certainly: 'Š_OK_____ä',
  autoEnabledTip: 'Š_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ä.',
  autoDisabledTip: 'Š_Click to enable auto layout_____________________________ä.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Š_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ä.',
  customDisabledTip: 'Š_Click to enable custom layout_______________________________ä.',
  confirm: 'Š_Confirm_______________ä',
  enableConfirm: 'Š_Are you sure you want to enable it___________________ä?',
  autoConfirmMsg: 'Š_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ä.',
  customConfirmMsg1: 'Š_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ä.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Š_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ä.',
  dragToResize: 'Š_Drag to resize_______________ä',
  largeScreen: 'Š_Large screen device____________________ä',
  mediumScreen: 'Š_Medium screen device_____________________ä',
  smallScreen: 'Š_Small screen device____________________ä',
});