define({
  auto: 'Đ_Auto_________ớ',
  custom: 'Đ_Custom_____________ớ',
  certainly: 'Đ_OK_____ớ',
  autoEnabledTip: 'Đ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ớ.',
  autoDisabledTip: 'Đ_Click to enable auto layout_____________________________ớ.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Đ_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ớ.',
  customDisabledTip: 'Đ_Click to enable custom layout_______________________________ớ.',
  confirm: 'Đ_Confirm_______________ớ',
  enableConfirm: 'Đ_Are you sure you want to enable it___________________ớ?',
  autoConfirmMsg: 'Đ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ớ.',
  customConfirmMsg1: 'Đ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ớ.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Đ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ớ.',
  dragToResize: 'Đ_Drag to resize_______________ớ',
  largeScreen: 'Đ_Large screen device____________________ớ',
  mediumScreen: 'Đ_Medium screen device_____________________ớ',
  smallScreen: 'Đ_Small screen device____________________ớ',
});