define({
  auto: 'å_Auto_________ø',
  custom: 'å_Custom_____________ø',
  certainly: 'å_OK_____ø',
  autoEnabledTip: 'å_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ø.',
  autoDisabledTip: 'å_Click to enable auto layout_____________________________ø.',
  /* tslint:disable-next-line */
  customEnabledTip: 'å_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ø.',
  customDisabledTip: 'å_Click to enable custom layout_______________________________ø.',
  confirm: 'å_Confirm_______________ø',
  enableConfirm: 'å_Are you sure you want to enable it___________________ø?',
  autoConfirmMsg: 'å_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ø.',
  customConfirmMsg1: 'å_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ø.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'å_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ø.',
  dragToResize: 'å_Drag to resize_______________ø',
  largeScreen: 'å_Large screen device____________________ø',
  mediumScreen: 'å_Medium screen device_____________________ø',
  smallScreen: 'å_Small screen device____________________ø',
});