define({
  auto: 'Č_Auto_________ž',
  custom: 'Č_Custom_____________ž',
  certainly: 'Č_OK_____ž',
  autoEnabledTip: 'Č_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ž.',
  autoDisabledTip: 'Č_Click to enable auto layout_____________________________ž.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Č_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ž.',
  customDisabledTip: 'Č_Click to enable custom layout_______________________________ž.',
  confirm: 'Č_Confirm_______________ž',
  enableConfirm: 'Č_Are you sure you want to enable it___________________ž?',
  autoConfirmMsg: 'Č_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ž.',
  customConfirmMsg1: 'Č_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ž.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Č_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ž.',
  dragToResize: 'Č_Drag to resize_______________ž',
  largeScreen: 'Č_Large screen device____________________ž',
  mediumScreen: 'Č_Medium screen device_____________________ž',
  smallScreen: 'Č_Small screen device____________________ž',
});