define({
  auto: 'Š_Auto_________č',
  custom: 'Š_Custom_____________č',
  certainly: 'Š_OK_____č',
  autoEnabledTip: 'Š_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________č.',
  autoDisabledTip: 'Š_Click to enable auto layout_____________________________č.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Š_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________č.',
  customDisabledTip: 'Š_Click to enable custom layout_______________________________č.',
  confirm: 'Š_Confirm_______________č',
  enableConfirm: 'Š_Are you sure you want to enable it___________________č?',
  autoConfirmMsg: 'Š_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________č.',
  customConfirmMsg1: 'Š_By enabling this option, you can manually arrange widgets for this device mode_________________________________________č.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Š_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________č.',
  dragToResize: 'Š_Drag to resize_______________č',
  largeScreen: 'Š_Large screen device____________________č',
  mediumScreen: 'Š_Medium screen device_____________________č',
  smallScreen: 'Š_Small screen device____________________č',
});