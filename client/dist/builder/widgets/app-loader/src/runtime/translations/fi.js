define({
  auto: 'Å_Auto_________ö',
  custom: 'Å_Custom_____________ö',
  certainly: 'Å_OK_____ö',
  autoEnabledTip: 'Å_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ö.',
  autoDisabledTip: 'Å_Click to enable auto layout_____________________________ö.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Å_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ö.',
  customDisabledTip: 'Å_Click to enable custom layout_______________________________ö.',
  confirm: 'Å_Confirm_______________ö',
  enableConfirm: 'Å_Are you sure you want to enable it___________________ö?',
  autoConfirmMsg: 'Å_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ö.',
  customConfirmMsg1: 'Å_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ö.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Å_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ö.',
  dragToResize: 'Å_Drag to resize_______________ö',
  largeScreen: 'Å_Large screen device____________________ö',
  mediumScreen: 'Å_Medium screen device_____________________ö',
  smallScreen: 'Å_Small screen device____________________ö',
});