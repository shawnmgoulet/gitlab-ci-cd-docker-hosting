define({
  auto: 'Į_Auto_________š',
  custom: 'Į_Custom_____________š',
  certainly: 'Į_OK_____š',
  autoEnabledTip: 'Į_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________š.',
  autoDisabledTip: 'Į_Click to enable auto layout_____________________________š.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Į_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________š.',
  customDisabledTip: 'Į_Click to enable custom layout_______________________________š.',
  confirm: 'Į_Confirm_______________š',
  enableConfirm: 'Į_Are you sure you want to enable it___________________š?',
  autoConfirmMsg: 'Į_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________š.',
  customConfirmMsg1: 'Į_By enabling this option, you can manually arrange widgets for this device mode_________________________________________š.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Į_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________š.',
  dragToResize: 'Į_Drag to resize_______________š',
  largeScreen: 'Į_Large screen device____________________š',
  mediumScreen: 'Į_Medium screen device_____________________š',
  smallScreen: 'Į_Small screen device____________________š',
});