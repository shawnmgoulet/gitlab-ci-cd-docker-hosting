define({
  auto: 'é_Auto_________È',
  custom: 'é_Custom_____________È',
  certainly: 'é_OK_____È',
  autoEnabledTip: 'é_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________È.',
  autoDisabledTip: 'é_Click to enable auto layout_____________________________È.',
  /* tslint:disable-next-line */
  customEnabledTip: 'é_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________È.',
  customDisabledTip: 'é_Click to enable custom layout_______________________________È.',
  confirm: 'é_Confirm_______________È',
  enableConfirm: 'é_Are you sure you want to enable it___________________È?',
  autoConfirmMsg: 'é_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________È.',
  customConfirmMsg1: 'é_By enabling this option, you can manually arrange widgets for this device mode_________________________________________È.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'é_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________È.',
  dragToResize: 'é_Drag to resize_______________È',
  largeScreen: 'é_Large screen device____________________È',
  mediumScreen: 'é_Medium screen device_____________________È',
  smallScreen: 'é_Small screen device____________________È',
});