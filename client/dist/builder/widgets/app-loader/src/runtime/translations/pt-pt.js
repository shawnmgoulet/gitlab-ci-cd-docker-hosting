define({
  auto: 'ã_Auto_________Ç',
  custom: 'ã_Custom_____________Ç',
  certainly: 'ã_OK_____Ç',
  autoEnabledTip: 'ã_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Ç.',
  autoDisabledTip: 'ã_Click to enable auto layout_____________________________Ç.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ã_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Ç.',
  customDisabledTip: 'ã_Click to enable custom layout_______________________________Ç.',
  confirm: 'ã_Confirm_______________Ç',
  enableConfirm: 'ã_Are you sure you want to enable it___________________Ç?',
  autoConfirmMsg: 'ã_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Ç.',
  customConfirmMsg1: 'ã_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Ç.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ã_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Ç.',
  dragToResize: 'ã_Drag to resize_______________Ç',
  largeScreen: 'ã_Large screen device____________________Ç',
  mediumScreen: 'ã_Medium screen device_____________________Ç',
  smallScreen: 'ã_Small screen device____________________Ç',
});