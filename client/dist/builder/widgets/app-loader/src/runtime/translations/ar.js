define({
  auto: 'بيت_Auto_________لاحقة',
  custom: 'بيت_Custom_____________لاحقة',
  certainly: 'بيت_OK_____لاحقة',
  autoEnabledTip: 'بيت_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________لاحقة.',
  autoDisabledTip: 'بيت_Click to enable auto layout_____________________________لاحقة.',
  /* tslint:disable-next-line */
  customEnabledTip: 'بيت_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________لاحقة.',
  customDisabledTip: 'بيت_Click to enable custom layout_______________________________لاحقة.',
  confirm: 'بيت_Confirm_______________لاحقة',
  enableConfirm: 'بيت_Are you sure you want to enable it___________________لاحقة?',
  autoConfirmMsg: 'بيت_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________لاحقة.',
  customConfirmMsg1: 'بيت_By enabling this option, you can manually arrange widgets for this device mode_________________________________________لاحقة.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'بيت_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________لاحقة.',
  dragToResize: 'بيت_Drag to resize_______________لاحقة',
  largeScreen: 'بيت_Large screen device____________________لاحقة',
  mediumScreen: 'بيت_Medium screen device_____________________لاحقة',
  smallScreen: 'بيت_Small screen device____________________لاحقة',
});