define({
  auto: 'כן_Auto_________ש',
  custom: 'כן_Custom_____________ש',
  certainly: 'כן_OK_____ש',
  autoEnabledTip: 'כן_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ש.',
  autoDisabledTip: 'כן_Click to enable auto layout_____________________________ש.',
  /* tslint:disable-next-line */
  customEnabledTip: 'כן_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ש.',
  customDisabledTip: 'כן_Click to enable custom layout_______________________________ש.',
  confirm: 'כן_Confirm_______________ש',
  enableConfirm: 'כן_Are you sure you want to enable it___________________ש?',
  autoConfirmMsg: 'כן_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ש.',
  customConfirmMsg1: 'כן_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ש.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'כן_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ש.',
  dragToResize: 'כן_Drag to resize_______________ש',
  largeScreen: 'כן_Large screen device____________________ש',
  mediumScreen: 'כן_Medium screen device_____________________ש',
  smallScreen: 'כן_Small screen device____________________ש',
});