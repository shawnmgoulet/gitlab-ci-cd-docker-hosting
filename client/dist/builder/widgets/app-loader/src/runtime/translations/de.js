define({
  auto: 'ä_Auto_________Ü',
  custom: 'ä_Custom_____________Ü',
  certainly: 'ä_OK_____Ü',
  autoEnabledTip: 'ä_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Ü.',
  autoDisabledTip: 'ä_Click to enable auto layout_____________________________Ü.',
  /* tslint:disable-next-line */
  customEnabledTip: 'ä_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Ü.',
  customDisabledTip: 'ä_Click to enable custom layout_______________________________Ü.',
  confirm: 'ä_Confirm_______________Ü',
  enableConfirm: 'ä_Are you sure you want to enable it___________________Ü?',
  autoConfirmMsg: 'ä_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Ü.',
  customConfirmMsg1: 'ä_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Ü.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'ä_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Ü.',
  dragToResize: 'ä_Drag to resize_______________Ü',
  largeScreen: 'ä_Large screen device____________________Ü',
  mediumScreen: 'ä_Medium screen device_____________________Ü',
  smallScreen: 'ä_Small screen device____________________Ü',
});