define({
  auto: 'आ_Auto_________ज',
  custom: 'आ_Custom_____________ज',
  certainly: 'आ_OK_____ज',
  autoEnabledTip: 'आ_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ज.',
  autoDisabledTip: 'आ_Click to enable auto layout_____________________________ज.',
  /* tslint:disable-next-line */
  customEnabledTip: 'आ_Custom layout is enabled. Widgets added in other device modes will not be added here automatically. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ज.',
  customDisabledTip: 'आ_Click to enable custom layout_______________________________ज.',
  confirm: 'आ_Confirm_______________ज',
  enableConfirm: 'आ_Are you sure you want to enable it___________________ज?',
  autoConfirmMsg: 'आ_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ज.',
  customConfirmMsg1: 'आ_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ज.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'आ_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ज.',
  dragToResize: 'आ_Drag to resize_______________ज',
  largeScreen: 'आ_Large screen device____________________ज',
  mediumScreen: 'आ_Medium screen device_____________________ज',
  smallScreen: 'आ_Small screen device____________________ज',
});