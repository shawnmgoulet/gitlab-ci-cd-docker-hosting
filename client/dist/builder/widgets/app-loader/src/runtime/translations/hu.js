define({
  auto: 'í_Auto_________ő',
  custom: 'í_Custom_____________ő',
  certainly: 'í_OK_____ő',
  autoEnabledTip: 'í_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ő.',
  autoDisabledTip: 'í_Click to enable auto layout_____________________________ő.',
  /* tslint:disable-next-line */
  customEnabledTip: 'í_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ő.',
  customDisabledTip: 'í_Click to enable custom layout_______________________________ő.',
  confirm: 'í_Confirm_______________ő',
  enableConfirm: 'í_Are you sure you want to enable it___________________ő?',
  autoConfirmMsg: 'í_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ő.',
  customConfirmMsg1: 'í_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ő.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'í_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ő.',
  dragToResize: 'í_Drag to resize_______________ő',
  largeScreen: 'í_Large screen device____________________ő',
  mediumScreen: 'í_Medium screen device_____________________ő',
  smallScreen: 'í_Small screen device____________________ő',
});