define({
  auto: 'Ă_Auto_________ș',
  custom: 'Ă_Custom_____________ș',
  certainly: 'Ă_OK_____ș',
  autoEnabledTip: 'Ă_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________ș.',
  autoDisabledTip: 'Ă_Click to enable auto layout_____________________________ș.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Ă_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________ș.',
  customDisabledTip: 'Ă_Click to enable custom layout_______________________________ș.',
  confirm: 'Ă_Confirm_______________ș',
  enableConfirm: 'Ă_Are you sure you want to enable it___________________ș?',
  autoConfirmMsg: 'Ă_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________ș.',
  customConfirmMsg1: 'Ă_By enabling this option, you can manually arrange widgets for this device mode_________________________________________ș.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Ă_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________ș.',
  dragToResize: 'Ă_Drag to resize_______________ș',
  largeScreen: 'Ă_Large screen device____________________ș',
  mediumScreen: 'Ă_Medium screen device_____________________ș',
  smallScreen: 'Ă_Small screen device____________________ș',
});