define({
  auto: 'Ж_Auto_________Я',
  custom: 'Ж_Custom_____________Я',
  certainly: 'Ж_OK_____Я',
  autoEnabledTip: 'Ж_Auto layout is enabled. Widgets are synced with those on the {label} and arranged automatically_________________________________________________Я.',
  autoDisabledTip: 'Ж_Click to enable auto layout_____________________________Я.',
  /* tslint:disable-next-line */
  customEnabledTip: 'Ж_Custom layout is enabled. Widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel_____________________________________________________________________________________________Я.',
  customDisabledTip: 'Ж_Click to enable custom layout_______________________________Я.',
  confirm: 'Ж_Confirm_______________Я',
  enableConfirm: 'Ж_Are you sure you want to enable it___________________Я?',
  autoConfirmMsg: 'Ж_By enabling this option, the widgets will be synced with those on the {label} and arranged automatically______________________________________________________Я.',
  customConfirmMsg1: 'Ж_By enabling this option, you can manually arrange widgets for this device mode_________________________________________Я.',
  /* tslint:disable-next-line */
  customConfirmMsg2: 'Ж_However, widgets added in other device modes will not be automatically added here. Alternatively, you can manually add them from the pending list on the Insert panel____________________________________________________________________________________Я.',
  dragToResize: 'Ж_Drag to resize_______________Я',
  largeScreen: 'Ж_Large screen device____________________Я',
  mediumScreen: 'Ж_Medium screen device_____________________Я',
  smallScreen: 'Ж_Small screen device____________________Я',
});