export default {
  page: 'Page',
  data: 'Data',
  theme: 'Theme',
  design: 'Design',
  insert: 'Insert',
  help: 'Help'
}
