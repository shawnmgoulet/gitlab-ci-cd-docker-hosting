/** @jsx jsx */
import { BaseWidget, AllWidgetProps, css, jsx, classNames, jimuHistory, SerializedStyles, IMSectionJson, IMState, IMThemeVariables, polished, getAppStore, appActions, lodash, AppMode } from 'jimu-core';
import { Icon, Nav, NavItem, NavLink, Link, Popper } from 'jimu-ui';

import { IMConfig } from '../config';
import defaultMessages from './translations/default';

type ViewId = 'insert' | 'page' | 'data' | 'theme';

interface ExtraProps {
  sectionJson: IMSectionJson;
  currentViewId: ViewId;
  sidebarVisible: boolean;
  lockLayout: boolean;
  appMode: AppMode;
}

interface State{
  isInsertDisabledPopperShown: boolean;
}

const viewsIcon = {
  page: './widgets/setting-navigator/dist/runtime/assets/page.svg',
  data: './widgets/setting-navigator/dist/runtime/assets/data.svg',
  theme: './widgets/setting-navigator/dist/runtime/assets/theme.svg',
  insert: './widgets/setting-navigator/dist/runtime/assets/insert.svg'
}

const LEFT_SIDEBAR_WIDGETID = 'left-sidebar';

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{
  viewLabel = {
    page: this.props.intl.formatMessage({id: 'page', defaultMessage: defaultMessages.page}),
    data: this.props.intl.formatMessage({id: 'data', defaultMessage: defaultMessages.data}),
    theme: this.props.intl.formatMessage({id: 'theme', defaultMessage: defaultMessages.theme}),
    //design: this.props.intl.formatMessage({id: 'design', defaultMessage: defaultMessages.design}),
    insert: this.props.intl.formatMessage({id: 'insert', defaultMessage: defaultMessages.insert}),
  }

  insertDom: HTMLElement;

  constructor(props) {
    super(props);

    //this.insertDom = React.createRef();

    this.state = {
      isInsertDisabledPopperShown: false
    }
  }

  static mapExtraStateProps = (state: IMState, ownProps: AllWidgetProps<IMConfig>): ExtraProps => {
    return {
      sectionJson: state.appConfig && state.appConfig.sections[ownProps.config.sectionId],
      currentViewId: state.appRuntimeInfo && state.appRuntimeInfo.currentViewIds && state.appRuntimeInfo.currentViewIds[0] ? state.appRuntimeInfo.currentViewIds[0] as ViewId : 'insert',
      sidebarVisible: lodash.getValue(state, `widgetsState.${LEFT_SIDEBAR_WIDGETID}.collapse`, true),
      lockLayout: state.appStateInBuilder && state.appStateInBuilder.appConfig && state.appStateInBuilder.appConfig.forBuilderAttributes
        && state.appStateInBuilder.appConfig.forBuilderAttributes.lockLayout,
      appMode: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.appMode
    }
  }

  componentDidMount(){
    if(this.getWhetherDisableInsert(this.props) && this.props.currentViewId === 'insert'){
      jimuHistory.changeView('opts-section', 'page');
    }
  }

  componentDidUpdate(prevProps: AllWidgetProps<IMConfig> & ExtraProps){
    if(this.getWhetherDisableInsert(this.props) && !this.getWhetherDisableInsert(prevProps) && this.props.currentViewId === 'insert'){
      jimuHistory.changeView('opts-section', 'page');
    }
  }

  getWhetherDisableInsert(props: AllWidgetProps<IMConfig> & ExtraProps): boolean{
    return props.lockLayout || props.appMode === AppMode.Run;
  }

  changeView(v) {
    if(this.getWhetherViewDisabled(v)){
      return;
    }
    if (this.props.currentViewId === v) {
      getAppStore().dispatch(appActions.widgetStatePropChange('left-sidebar', 'collapse', !this.props.sidebarVisible));
    } else {
      jimuHistory.changeView('opts-section', v);
      if (!this.props.sidebarVisible) {
        getAppStore().dispatch(appActions.widgetStatePropChange('left-sidebar', 'collapse', true));
      }
    }
  }

  getWhetherViewDisabled(v){
    return this.getWhetherDisableInsert(this.props) && v === 'insert';
  }
  getWhetherViewActive(v){
    return v === this.props.currentViewId && this.props.sidebarVisible;
  }

  onInsertMouseEnter = v => {
    if(v === 'insert' && this.getWhetherViewDisabled('insert')){
      this.setState({isInsertDisabledPopperShown: true});
    }
  }

  onInsertMouseLeave = v => {
    if(v === 'insert'){
      this.setState({isInsertDisabledPopperShown: false});
    }
  }

  getStyle = (theme: IMThemeVariables): SerializedStyles => {
    return css`
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: ${theme.colors.secondary};

      .nav.nav-underline {
        border: 0 !important;
        .nav-item{
          display: flex !important;
        }
        .nav-item:focus{
          border: 0;
          outline: none;
          box-shadow: 0 0 0;
        }
        .nav-item > .jimu-link{
          height: auto !important;
          padding-left: 0;
          padding-right: 0;
          position: relative;
          border-bottom-width: 0 !important;
          &::before {
            content: " ";
            display: block;
            position: absolute;
            width: 4px;
            height: 100%;
            top: 0;
            left: -4px;
            background-color: ${theme.colors.palette.primary[600]};
            /* transition: left ease-in .2s; */
            z-index: 1;
          }
          > .jimu-icon {
            margin: 0;
          }
          &:active,
          &.active {
            border-left-width: 0 !important;
            &::before {
              left: 0;
            }
          }
        }
      }
      .link-icon-color{
        svg{
          margin-right: 0 !important;
          margin-left: 0 !important;
        }
      }

      .link-icon-color:not(.disable-setting){
        &:hover{
          svg{
            color: ${theme.colors.dark} !important;
          }
        }
      }

      .nav-item:hover{
        background-color: ${theme.colors.palette.secondary[600]};
      }

      .active-setting:not(.disable-setting){
        background-color: ${theme.colors.palette.secondary[800]};
      }

      .disable-setting{
        &.nav-item:focus, &.nav-item button:focus, &.nav-item:active, &.nav-item button:active, &.nav-item:hover, &.nav-item button:hover{
          outline: none !important;
          cursor: default !important;
          border: 0 !important;
          box-shadow: 0 0 0 !important;
        }
        &.nav-item button:active::before{
          width: 0 !important;
        }
      }

      .link-focus{
        &:focus, button:focus{
          outline: 0;
          border: 0;
          box-shadow: 0 0 0;
        }
      }

      .top-sections{
        height: ${polished.rem(220)};
      }
      .bottom-sections{
        position: absolute;
        bottom: 0;
        .func-buttons{
          height: ${polished.rem(50)};
          width: 100%;
          >span{
            display: inline-block;
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
          }
        }
      }
    `
  }

  render() {
    const activeClassName = 'active-setting';
    const disableClassName = 'disable-setting';
    const { sectionJson, theme } = this.props;
    return (
      <div css={this.getStyle(theme)} className="widget-builder-setting-navigator h-100" >
        <Popper open={this.state.isInsertDisabledPopperShown} container="body"
          modifiers={{
            arrow: {
              enabled: true
            }
          }}
          reference={this.insertDom}
          placement="right"
          offset={[0, 2]}
          css={css`
            width: ${polished.rem(300)};
            padding: ${polished.rem(12)};
            background-color: ${theme.colors.palette.light[500]};
            color: ${theme.colors.palette.dark[800]};
            font-size: ${polished.rem(13)};
            .hight-light{
              font-size: ${polished.rem(16)};
              color: ${theme.colors.black};
            }
            .jimu-popper--arrow::after {
              border-right-color: ${theme.colors.palette.light[500]} !important;
            }
          `}
        >
          <div className="insert-disable-tooltip">
            {
              this.props.appMode === AppMode.Run ?
              <div>
                <div>Unable to add widgets in <strong className="hight-light">Live view</strong> mode.</div>
                <div>Turn off <strong className="hight-light">Live view</strong> to enable it.</div>
              </div> :
              <div>
                <div>Unable to insert a widget from here when layout editing is disabled.</div>
                <div>Turn off the <strong className="hight-light">Lock layout</strong> option to enable it.</div>
              </div>
            }
          </div>
        </Popper>
        <Nav fill underline vertical right className="top-sections">
        {
          sectionJson.views.map(vId => {
            const disableView = this.getWhetherViewDisabled(vId);
            const activeView = this.getWhetherViewActive(vId);
            return <NavItem key={vId} className={classNames('link-icon-color', { [activeClassName]: activeView, [disableClassName]: disableView })}
                disabled={disableView} onMouseEnter={() => this.onInsertMouseEnter(vId)} onMouseLeave={() => this.onInsertMouseLeave(vId)}
              >
                <NavLink iconPosition="above" tag="button" active={activeView}
                  onClick={e => this.changeView(vId)} title={this.viewLabel[vId]}
                >
                  <div className="w-100 h-100" ref={r => {
                    if(vId === 'insert'){
                      this.insertDom = r;
                    }
                  }}>
                    <Icon className={classNames({ [activeClassName]: activeView })} icon={viewsIcon[vId]} size="20"
                      color={disableView ? this.props.theme.colors.palette.secondary[800] : (activeView ? this.props.theme.colors.dark : this.props.theme.colors.palette.dark[400])}
                    />
                  </div>
              </NavLink>
            </NavItem>
          })
        }
      </Nav>

        <div className="bottom-sections w-100">
          {/* <div className="func-buttons">
            <span><Icon icon={IconSetting} /></span>
          </div> */}
          <div className="func-buttons" title={this.props.intl.formatMessage({id: 'help', defaultMessage: defaultMessages.help})}>
            <Link themeStyle={{icon: true, type: 'link'}} className="link-focus link-icon-color w-100" to="https://community.esri.com/community/arcgis-experience-builder" target="_blank">
              <Icon icon="./widgets/setting-navigator/dist/runtime/assets/help.svg" color={this.props.theme.colors.palette.dark[400]} />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
