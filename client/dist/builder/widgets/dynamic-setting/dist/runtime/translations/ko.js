define({
  pageSetting: '한_Page settings______________빠',
  folderSetting: '한_Folder settings________________빠',
  linkSetting: '한_Link settings______________빠',
  noSelection: '한_No selection_____________빠',
  customWidth: '한_Fixed width____________빠',
  fitToWindow: '한_Fit to window______________빠',
  fitToWidthOnly: '한_Full width_____________________빠',
  fitToContainer: '한_Fit to container_________________빠',
  keepAspectRatio: '한_Keep aspect ratio__________________빠',
  aspectRatio: '한_Aspect ratio_____________빠',
  aspectRatioFull: '한_Aspect ratio (height/width)____________________________빠',
  fixedHeight: '한_Fixed height_____________빠',
  fixed: '한_Fixed___________빠',
  content: '한_Content_______________빠',
  style: '한_Style___________빠',
  action: '한_Action_____________빠',
  layout: '한_Layout_____________빠',
  general: '한_General_______________빠',
  dynamicLoading: '한_Loading_______________빠',
  displayMode: '한_Display mode_____________빠',
  width: '한_Width___________빠',
  height: '한_Height_____________빠',
  body: '한_Body_________빠',
  header: '한_Header_____________빠',
  footer: '한_Footer_____________빠',
  urlIsHere: '한_Type URL here_______________빠.',
  views: '한_Views___________빠',
  remove: '한_Remove_____________빠',
  duplicate: '한_Duplicate___________________빠',
  rename: '한_Rename_____________빠',
  noWidget: '한_No widget___________________빠',
  maxWidth: '한_Max width___________________빠',
  duplicateView: '한_Duplicate view_______________빠',
  addView: '한_New view_________________빠',
  noSettingOptions: '한_No setting options____________________빠.',
  contentWidth: '한_Content width______________빠',
  custom: '한_Custom_____________빠',
  auto: '한_Auto_________빠',
  gap: '한_Gap_______빠',
  padding: '한_Padding_______________빠',
  border: '한_Border_____________빠',
  borderRadius: '한_Border radius______________빠',
  boxShadow: '한_Box shadow_____________________빠',
  size: '한_Size_________빠',
  lockParent: '한_Lock parent____________빠',
  margin: '한_Margin_____________빠',
  addTrigger: '한_Add a trigger______________빠',
  noTrigger: '한_The widget does not generate any triggers______________________빠.',
  selectTrigger: '한_Select a trigger_________________빠',
  selectTarget: '한_Select a target________________빠',
  selectAction: '한_Select an action_________________빠',
  actionBack: '한_Back_________빠',
  actionSet: '한_Set_______빠',
  addAction: '한_Add action_____________________빠',
  targetWidgets: '한_Widgets_______________빠',
  noMessage: '한_No message_____________________빠',
  noTargetWidgets: '한_No widget___________________빠',
  noAction: '한_No action___________________빠',
  chooseSearchTrigger: '한_Select or search a trigger___________________________빠',
  chooseSearchTarget: '한_Select or search a target__________________________빠',
  chooseSearchAction: '한_Select or search an action___________________________빠',
  actionDone: '한_Done_________빠',
  actionFramework: '한_Framework___________________빠',
  actionSettingLoading: '한_Loading_____________________빠...',
  block: '한_Block___________빠',
  widgetIcon: '한_Widget icon____________빠',
  sectionIcon: '한_Section icon_____________빠',
  pageIcon: '한_Page icon___________________빠',
  linkIcon: '한_Link icon___________________빠',
  folderIcon: '한_Folder icon____________빠',
  change: '한_Change_____________빠',
  background: '한_Background_____________________빠',
  turnTo: '한_Switch to_____________________빠:'
});