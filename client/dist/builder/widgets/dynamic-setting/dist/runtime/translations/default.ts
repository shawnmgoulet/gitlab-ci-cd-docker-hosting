export default {
  pageSetting: 'Page settings',
  folderSetting: 'Folder settings',
  linkSetting: 'Link settings',
  noSelection: 'No selection',
  customWidth: 'Fixed width',
  fitToWindow: 'Fit to window',
  fitToWidthOnly: 'Full width',
  fitToContainer: 'Fit to container',
  keepAspectRatio: 'Keep aspect ratio',
  aspectRatio: 'Aspect ratio',
  aspectRatioFull: 'Aspect ratio (height/width)',
  fixedHeight: 'Fixed height',
  fixed: 'Fixed',
  content: 'Content',
  style: 'Style',
  action: 'Action',
  layout: 'Layout',
  general: 'General',
  dynamicLoading: 'Loading',
  displayMode: 'Display mode',
  width: 'Width',
  height: 'Height',
  body: 'Body',
  header: 'Header',
  footer: 'Footer',
  urlIsHere: 'Type URL here.',
  views: 'Views',
  remove: 'Remove',
  duplicate: 'Duplicate',
  rename: 'Rename',
  noWidget: 'No widget',
  maxWidth: 'Max width',
  duplicateView: 'Duplicate view',
  addView: 'New view',
  noSettingOptions: 'No setting options.',
  contentWidth: 'Content width',
  custom: 'Custom',
  auto: 'Auto',
  gap: 'Gap',
  padding: 'Padding',
  border: 'Border',
  borderRadius: 'Border radius',
  boxShadow: 'Box shadow',
  size: 'Size',
  lockParent: 'Lock parent',
  margin: 'Margin',
  addTrigger: 'Add a trigger',
  noTrigger: 'The widget does not generate any triggers.',
  selectTrigger: 'Select a trigger',
  selectTarget: 'Select a target',
  selectAction: 'Select an action',
  actionBack: 'Back',
  actionSet: 'Set',
  addAction: 'Add action',
  targetWidgets: 'Widgets',
  noMessage: 'No message',
  noTargetWidgets: 'No widget',
  noAction: 'No action',
  chooseSearchTrigger: 'Select or search a trigger',
  chooseSearchTarget: 'Select or search a target',
  chooseSearchAction: 'Select or search an action',
  actionDone: 'Done',
  actionFramework: 'Framework',
  actionSettingLoading: 'Loading...',
  block: 'Block',
  widgetIcon: 'Widget icon',
  sectionIcon: 'Section icon',
  pageIcon: 'Page icon',
  linkIcon: 'Link icon',
  folderIcon: 'Folder icon',
  change: 'Change',
  background: 'Background',
  turnTo: 'Switch to:'
}
