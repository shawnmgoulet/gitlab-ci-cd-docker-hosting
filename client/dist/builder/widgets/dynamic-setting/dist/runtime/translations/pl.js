define({
  pageSetting: 'ł_Page settings______________ą',
  folderSetting: 'ł_Folder settings________________ą',
  linkSetting: 'ł_Link settings______________ą',
  noSelection: 'ł_No selection_____________ą',
  customWidth: 'ł_Fixed width____________ą',
  fitToWindow: 'ł_Fit to window______________ą',
  fitToWidthOnly: 'ł_Full width_____________________ą',
  fitToContainer: 'ł_Fit to container_________________ą',
  keepAspectRatio: 'ł_Keep aspect ratio__________________ą',
  aspectRatio: 'ł_Aspect ratio_____________ą',
  aspectRatioFull: 'ł_Aspect ratio (height/width)____________________________ą',
  fixedHeight: 'ł_Fixed height_____________ą',
  fixed: 'ł_Fixed___________ą',
  content: 'ł_Content_______________ą',
  style: 'ł_Style___________ą',
  action: 'ł_Action_____________ą',
  layout: 'ł_Layout_____________ą',
  general: 'ł_General_______________ą',
  dynamicLoading: 'ł_Loading_______________ą',
  displayMode: 'ł_Display mode_____________ą',
  width: 'ł_Width___________ą',
  height: 'ł_Height_____________ą',
  body: 'ł_Body_________ą',
  header: 'ł_Header_____________ą',
  footer: 'ł_Footer_____________ą',
  urlIsHere: 'ł_Type URL here_______________ą.',
  views: 'ł_Views___________ą',
  remove: 'ł_Remove_____________ą',
  duplicate: 'ł_Duplicate___________________ą',
  rename: 'ł_Rename_____________ą',
  noWidget: 'ł_No widget___________________ą',
  maxWidth: 'ł_Max width___________________ą',
  duplicateView: 'ł_Duplicate view_______________ą',
  addView: 'ł_New view_________________ą',
  noSettingOptions: 'ł_No setting options____________________ą.',
  contentWidth: 'ł_Content width______________ą',
  custom: 'ł_Custom_____________ą',
  auto: 'ł_Auto_________ą',
  gap: 'ł_Gap_______ą',
  padding: 'ł_Padding_______________ą',
  border: 'ł_Border_____________ą',
  borderRadius: 'ł_Border radius______________ą',
  boxShadow: 'ł_Box shadow_____________________ą',
  size: 'ł_Size_________ą',
  lockParent: 'ł_Lock parent____________ą',
  margin: 'ł_Margin_____________ą',
  addTrigger: 'ł_Add a trigger______________ą',
  noTrigger: 'ł_The widget does not generate any triggers______________________ą.',
  selectTrigger: 'ł_Select a trigger_________________ą',
  selectTarget: 'ł_Select a target________________ą',
  selectAction: 'ł_Select an action_________________ą',
  actionBack: 'ł_Back_________ą',
  actionSet: 'ł_Set_______ą',
  addAction: 'ł_Add action_____________________ą',
  targetWidgets: 'ł_Widgets_______________ą',
  noMessage: 'ł_No message_____________________ą',
  noTargetWidgets: 'ł_No widget___________________ą',
  noAction: 'ł_No action___________________ą',
  chooseSearchTrigger: 'ł_Select or search a trigger___________________________ą',
  chooseSearchTarget: 'ł_Select or search a target__________________________ą',
  chooseSearchAction: 'ł_Select or search an action___________________________ą',
  actionDone: 'ł_Done_________ą',
  actionFramework: 'ł_Framework___________________ą',
  actionSettingLoading: 'ł_Loading_____________________ą...',
  block: 'ł_Block___________ą',
  widgetIcon: 'ł_Widget icon____________ą',
  sectionIcon: 'ł_Section icon_____________ą',
  pageIcon: 'ł_Page icon___________________ą',
  linkIcon: 'ł_Link icon___________________ą',
  folderIcon: 'ł_Folder icon____________ą',
  change: 'ł_Change_____________ą',
  background: 'ł_Background_____________________ą',
  turnTo: 'ł_Switch to_____________________ą:'
});