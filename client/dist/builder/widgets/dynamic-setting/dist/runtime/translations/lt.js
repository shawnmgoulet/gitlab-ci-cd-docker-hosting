define({
  pageSetting: 'Į_Page settings______________š',
  folderSetting: 'Į_Folder settings________________š',
  linkSetting: 'Į_Link settings______________š',
  noSelection: 'Į_No selection_____________š',
  customWidth: 'Į_Fixed width____________š',
  fitToWindow: 'Į_Fit to window______________š',
  fitToWidthOnly: 'Į_Full width_____________________š',
  fitToContainer: 'Į_Fit to container_________________š',
  keepAspectRatio: 'Į_Keep aspect ratio__________________š',
  aspectRatio: 'Į_Aspect ratio_____________š',
  aspectRatioFull: 'Į_Aspect ratio (height/width)____________________________š',
  fixedHeight: 'Į_Fixed height_____________š',
  fixed: 'Į_Fixed___________š',
  content: 'Į_Content_______________š',
  style: 'Į_Style___________š',
  action: 'Į_Action_____________š',
  layout: 'Į_Layout_____________š',
  general: 'Į_General_______________š',
  dynamicLoading: 'Į_Loading_______________š',
  displayMode: 'Į_Display mode_____________š',
  width: 'Į_Width___________š',
  height: 'Į_Height_____________š',
  body: 'Į_Body_________š',
  header: 'Į_Header_____________š',
  footer: 'Į_Footer_____________š',
  urlIsHere: 'Į_Type URL here_______________š.',
  views: 'Į_Views___________š',
  remove: 'Į_Remove_____________š',
  duplicate: 'Į_Duplicate___________________š',
  rename: 'Į_Rename_____________š',
  noWidget: 'Į_No widget___________________š',
  maxWidth: 'Į_Max width___________________š',
  duplicateView: 'Į_Duplicate view_______________š',
  addView: 'Į_New view_________________š',
  noSettingOptions: 'Į_No setting options____________________š.',
  contentWidth: 'Į_Content width______________š',
  custom: 'Į_Custom_____________š',
  auto: 'Į_Auto_________š',
  gap: 'Į_Gap_______š',
  padding: 'Į_Padding_______________š',
  border: 'Į_Border_____________š',
  borderRadius: 'Į_Border radius______________š',
  boxShadow: 'Į_Box shadow_____________________š',
  size: 'Į_Size_________š',
  lockParent: 'Į_Lock parent____________š',
  margin: 'Į_Margin_____________š',
  addTrigger: 'Į_Add a trigger______________š',
  noTrigger: 'Į_The widget does not generate any triggers______________________š.',
  selectTrigger: 'Į_Select a trigger_________________š',
  selectTarget: 'Į_Select a target________________š',
  selectAction: 'Į_Select an action_________________š',
  actionBack: 'Į_Back_________š',
  actionSet: 'Į_Set_______š',
  addAction: 'Į_Add action_____________________š',
  targetWidgets: 'Į_Widgets_______________š',
  noMessage: 'Į_No message_____________________š',
  noTargetWidgets: 'Į_No widget___________________š',
  noAction: 'Į_No action___________________š',
  chooseSearchTrigger: 'Į_Select or search a trigger___________________________š',
  chooseSearchTarget: 'Į_Select or search a target__________________________š',
  chooseSearchAction: 'Į_Select or search an action___________________________š',
  actionDone: 'Į_Done_________š',
  actionFramework: 'Į_Framework___________________š',
  actionSettingLoading: 'Į_Loading_____________________š...',
  block: 'Į_Block___________š',
  widgetIcon: 'Į_Widget icon____________š',
  sectionIcon: 'Į_Section icon_____________š',
  pageIcon: 'Į_Page icon___________________š',
  linkIcon: 'Į_Link icon___________________š',
  folderIcon: 'Į_Folder icon____________š',
  change: 'Į_Change_____________š',
  background: 'Į_Background_____________________š',
  turnTo: 'Į_Switch to_____________________š:'
});