import {React, IMAppConfig, Immutable, IMHeaderJson, appConfigUtils, lodash, 
  BrowserSizeMode, LayoutType, getAppStore,
  IMPageJson, IMFooterJson, IMThemeVariables } from 'jimu-core';
import {Collapse, Switch, LinearUnit } from 'jimu-ui';
import { getAppConfigAction } from 'jimu-for-builder';
import { SettingSection, SettingRow } from 'jimu-ui/setting-components';
import { ThemeColorPicker } from 'jimu-ui/color-picker';
import { InputUnit } from 'jimu-ui/style-setting-components';

interface Props{
  pageId: string;
  dispatch: any;
  pagePart: 'body' | 'header' | 'footer';
  emptyLayout: any;
  browserSizeMode: BrowserSizeMode;
  theme: IMThemeVariables;
  formatMessage: (id: string) => string;

  pageJson?: IMPageJson;
  header?: IMHeaderJson;
  footer?: IMFooterJson
}

interface State{
  isOpen: boolean;
}

const DEFAULT_HEADER_HEIGHT = 75;

export default class Widget extends React.PureComponent<Props, State>{
  constructor(props){
    super(props);
    this.state = {
      isOpen: this.isOpen()
    }
  }

  getAppConfig(): IMAppConfig{
    return getAppStore().getState().appStateInBuilder && getAppStore().getState().appStateInBuilder.appConfig || {pages: {empty: {}}} as any;
  }

  isOpen = () => {
    const {pagePart, pageId} = this.props
    const appConfig = this.getAppConfig();
    return pagePart === 'body' || !!(appConfig && appConfig.pages[pageId] && appConfig[pagePart] && appConfig.pages[pageId][pagePart])
  }

  formatMessage = (id: string) => {
    return this.props.formatMessage(id);
  }

  componentDidUpdate(preProps){
    if(this.props.pageId !== preProps.pageId){
      let {pageJson, pagePart} = this.props;
      this.setState({
        isOpen: pagePart === 'body' || (pageJson && pageJson[pagePart])
      })
    }
  }

  onSwitchChange = () => {
    let {pageId, pagePart, /* dispatch, */ emptyLayout, browserSizeMode} = this.props;
    const appConfig = this.getAppConfig();
    let pageJson = appConfig.pages[pageId];

    let partJson = appConfig[pagePart];
    let appConfigAction = getAppConfigAction();
    if(!partJson){
      let layoutId = appConfigUtils.getUniqueId(appConfig, 'layout');
      let layoutJson = Immutable({...emptyLayout, id: layoutId, type: LayoutType.FlowLayout});
      partJson = Immutable({})
        .setIn(['layout', browserSizeMode], layoutId)
        .setIn(['height', browserSizeMode], DEFAULT_HEADER_HEIGHT) as IMHeaderJson;
      pagePart === 'header' ? appConfigAction.editHeader(partJson, [layoutJson]) :
        appConfigAction.editFooter(partJson, [layoutJson]);
    }

    let newPageJson = pageJson.set(pagePart, !pageJson[pagePart]);
    appConfigAction.editPage(newPageJson).exec();

    this.setState({isOpen: newPageJson[pagePart]});
  }

  onHeightChange = (value: LinearUnit) => {
    let h = value && value.distance;
    let {pagePart, browserSizeMode} = this.props;
    const appConfig = this.getAppConfig();
    if(pagePart === 'header'){
      getAppConfigAction().editHeader(appConfig.header.setIn(['height', browserSizeMode], h)).exec();
    }else{
      getAppConfigAction().editFooter(appConfig.footer.setIn(['height', browserSizeMode], h)).exec();
    }
  }

  onBackgroundColorChange = (color: string) => {
    let {pageId, pagePart} = this.props;
    const appConfig = this.getAppConfig();
    let pageJson = appConfig.pages[pageId];
    if(pagePart === 'body'){
      getAppConfigAction().editPage(pageJson.set('bodyBackgroundColor', color)).exec();
    }else if(pagePart === 'header'){
      getAppConfigAction().editHeader(appConfig.header.set('backgroundColor', color)).exec();
    }else{
      getAppConfigAction().editFooter(appConfig.footer.set('backgroundColor', color)).exec();
    }
  }

  render(){
    const title = {
      body: this.formatMessage('body'),
      header: this.formatMessage('header'),
      footer: this.formatMessage('footer')
    };
    let {pageId, pagePart, browserSizeMode, theme} = this.props;
    const appConfig = this.getAppConfig();
    let pageJson = appConfig.pages[pageId];
    let backgroundColor, emptyColor = '#eee';

    if(pagePart === 'body'){
      backgroundColor = pageJson.bodyBackgroundColor || emptyColor;
    }else{
      backgroundColor = appConfig[pagePart] && appConfig[pagePart].backgroundColor;
      if (!backgroundColor && theme) {
        if (pagePart === 'header') {
          backgroundColor = theme.header && theme.header.bg;
        } else {
          backgroundColor = theme.footer && theme.footer.bg;
        }
      }
    }

    let sectionTitle = <div className="setting-title d-flex justify-content-between">
      <div>
        <span>{title[this.props.pagePart]}</span>
      </div>
      <div className="d-flex align-items-center">
        {pagePart === 'body' ? null : <Switch checked={this.isOpen()} onChange={this.onSwitchChange} />}
      </div>
    </div>;

    const height = lodash.getValue(appConfig, `${pagePart}.height.${browserSizeMode}`, DEFAULT_HEADER_HEIGHT);
    return (
      <SettingSection title={sectionTitle}>
        <Collapse isOpen={this.state.isOpen}>
            {
              pagePart === 'body' ? null :
                  <SettingRow label={`${this.formatMessage('height')}`}>
                    <InputUnit value={{distance: height}} min={0} onChange={this.onHeightChange}>
                    </InputUnit>
                  </SettingRow>
            }

          <SettingRow label={this.formatMessage('fill')}>
              <ThemeColorPicker colors={theme ? theme.colors : undefined} value={backgroundColor} onChange={this.onBackgroundColorChange}></ThemeColorPicker>
          </SettingRow>
        </Collapse>
      </SettingSection>
    );
  }
}
