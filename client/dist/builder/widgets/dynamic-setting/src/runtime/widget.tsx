/** @jsx jsx */
import {BaseWidget, IMState, AllWidgetProps, IMRuntimeInfos, Selection, LayoutInfo, lodash, LayoutType,
  LayoutItemType, IMAppConfig, css, jsx, urlUtils, PageType, polished, PageMode, BrowserSizeMode, getAppStore, IMIconResult, IconResult, IMThemeVariables} from 'jimu-core';
import WidgetSetting from './widget-setting';
import PageSetting from './page-setting';
import LinkSetting from './link-setting';
import FolderSetting from './folder-setting';
import SectionSetting from './section-setting';
import LayoutItemSetting from './layout-item-setting';
import {MessageSetting} from './action-setting';
import {Tabs, Tab, Icon, Button} from 'jimu-ui';
import {getAppConfigAction, utils} from 'jimu-for-builder';
import {layoutUtils} from 'jimu-layouts/common';
import defaultMessages from './translations/default';
import React = require('react');
import { SettingSection } from 'jimu-ui/setting-components';
import { IconPicker } from 'jimu-ui/resource-selector';

interface ExtraProps{
  widgetsSettingRuntimeInfo: IMRuntimeInfos;
  currentPageId: string;
  pageMode: PageMode;
  maxPageWidth: number;
  currentDialogId: string;
  currentViewId: string;
  selection: Selection;
  browserSizeMode: BrowserSizeMode;
  titleName: string,
  icon: IconResult | string,
  appTheme: IMThemeVariables,
}

interface Stats {
  titleLength: number;
  titleText: string;
}

export default class Widget extends BaseWidget<AllWidgetProps<{}> & ExtraProps, Stats>{

  titleTextInput = React.createRef<HTMLInputElement>();
  spanTextInput = React.createRef<HTMLSpanElement>();


  static mapExtraStateProps = (state: IMState): ExtraProps => {
    const currentPageId = state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentPageId;
    let pageMode;
    let maxPageWidth;
    if (currentPageId) {
      pageMode = lodash.getValue(state, `appStateInBuilder.appConfig.pages.${currentPageId}.mode`);
      maxPageWidth = lodash.getValue(state, `appStateInBuilder.appConfig.pages.${currentPageId}.maxWidth`);
    }
    const selection = state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.selection;
    const appConfig = state.appStateInBuilder && state.appStateInBuilder.appConfig;
    let icon;
    let titleName;
    if(selection){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);
      if(layoutItem){
        const layout = appConfig.layouts[selection.layoutId];
        if (layout.type === LayoutType.FlowLayout) {
          titleName = 'Block';
        }else if(layoutItem.type === LayoutItemType.Widget){
          if(layoutItem.widgetId){
            const widget = appConfig.widgets[layoutItem.widgetId];
            titleName = widget && widget.label
            icon = widget && widget.icon
          }
        }else if(layoutItem.type === LayoutItemType.Section){
          if(layoutItem.sectionId){
            const section = appConfig.sections[layoutItem.sectionId];
            titleName = section && section.label;
            icon = section && section.icon;
          }
        }
      }
    }else {
      if(currentPageId && appConfig){
        const page = appConfig.pages[currentPageId];
        if(page){
          titleName = page.label;
          icon = page.icon || utils.getDefaultTocPageIcon(page);
        }

      }
    }
    return {
      currentPageId,
      pageMode,
      maxPageWidth,
      widgetsSettingRuntimeInfo: state.builder.widgetsSettingRuntimeInfo,
      currentDialogId: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentDialogId,
      selection: selection,
      currentViewId: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentViewId,
      browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
      titleName: titleName,
      icon: icon,
      appTheme: state.appStateInBuilder && state.appStateInBuilder.theme,
    }
  };

  constructor(props){
    super(props);
    this.titleTextInput = React.createRef();
    this.spanTextInput = React.createRef();
    const alterState = {
      titleLength: 0,
      titleText: ''
    }

    const {selection, appConfig, editingSectionId, currentPageId} = props;
    if(selection){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);

      if(layoutItem){
        if(layoutItem.type === LayoutItemType.Widget){
          if(layoutItem.widgetId){
            alterState.titleText = appConfig.widgets[layoutItem.widgetId].label;
          }
        }else if(layoutItem.type === LayoutItemType.Section){
          alterState.titleText = appConfig.sections[layoutItem.sectionId].label;
        }
      }
    }else {
      if(!editingSectionId && currentPageId){
        alterState.titleText = appConfig.pages[currentPageId].label;
      }
    }
    this.state = alterState;
  }

  componentDidUpdate(preProps, preState){
    if (this.spanTextInput.current && this.state.titleLength !== this.spanTextInput.current.offsetWidth) {
      this.setState({
        titleLength: this.spanTextInput.current.offsetWidth + 2
      });
    }

    if(preProps.titleName !== this.props.titleName){
      this.setState({
        titleText: this.props.titleName
      })
    }

  }

  getCss(){
    const {theme} = this.props;
    const tabH = 40;
    return css`
      height: 100%;
      overflow: hidden;
      overflow-x: hidden;
      .jimu-widget-setting--row {
        label {
          user-select: none;
        }
      }
      .jimu-widget-setting--section-header {
        user-select: none;
      }
      .tab-content {
        height: calc(100% - ${polished.rem(tabH)});
        overflow: auto;
        overflow-x: hidden;
      }
      .tab-pane{
        width: 100%;
      }

      .tab-title-item{
        width: 30%;
      }

      .header-title-input {
        border: none;
        min-width: ${polished.rem(20)};
        width: ${this.state.titleLength}px;
        overflow: hidden;
        white-space: nowrap;
        margin-bottom: 0;
        text-overflow: ellipsis;
        cursor: pointer;
        color: ${theme.colors.palette.dark[600]};
        background-color: transparent;
        max-width: ${polished.rem(170)};
        &.h5 {
          font-weight: ${theme.typography.weights.extraBold};
        }
        &:focus {
          background-color: ${theme.colors.white};
        }

        &:hover {
          outline: 1px solid ${theme.colors.palette.light[500]};
        }
      }

      .setting-container {
        overflow: auto;
      }
    `;
  }

  onLayoutItemSettingChanged = (layoutInfo: LayoutInfo, setting) => {
    getAppConfigAction().editLayoutItemSetting(layoutInfo, setting).exec();
  }

  onLayoutItemStyleChanged = (layoutInfo: LayoutInfo, style) => {
    const appConfigAction = getAppConfigAction();
    const { layoutId, layoutItemId } = layoutInfo;
    const layoutItem = appConfigAction.appConfig.layouts[layoutId].content[layoutItemId];
    if (layoutItem.type === LayoutItemType.Widget) {
      appConfigAction.editWidgetProperty(layoutItem.widgetId, 'style', style).exec();
    } else if (layoutItem.type === LayoutItemType.Section) {
      appConfigAction.editSectionProperty(layoutItem.sectionId, 'style', style).exec();
    }
  }

  onLayoutPosChanged = (layoutInfo: LayoutInfo, bbox) => {
    getAppConfigAction().editLayoutItemBBox(layoutInfo, bbox).exec();
  }

  getPageType = (id: string, pages: any) => {
    for(let index in pages){
      let page = pages[index];
      if(page.id === id){
        return page;
      }
    }
    return {};
  }

  formatMessage = (id: string) => {
    return this.props.intl.formatMessage({id: id, defaultMessage: defaultMessages[id]})
  }

  handleTitleInputBlur = (e: any, id: string, type: 'widget' | 'section' | 'page') => {
    this.editTitle(id, e.target.value, type);
  }

  editTitle = (id: string, newTitle: string , type: 'widget' | 'section' | 'page') => {
    if(type === 'widget'){
      getAppConfigAction().editWidgetProperty(id, 'label', newTitle).exec();
    }else if(type === 'section'){
      getAppConfigAction().editSectionProperty(id, 'label',  newTitle).exec();
    }else if(type === 'page'){
      getAppConfigAction().editPageProperty(id, 'label',  newTitle).exec();
    }
  }

  handleKeydown = (e: any) => {
    if (e.keyCode === 13) {
      this.titleTextInput.current.blur()
    } else {
      return
    }
  }

  handleIconChange = (result: IMIconResult) => {
    const display = this.getDisplay();
    const {selection, currentPageId} = this.props;
    const appConfigAction = getAppConfigAction();
    const appConfig = appConfigAction.appConfig;
    if(display.page){
      appConfigAction.editPageProperty(currentPageId, 'icon', result).exec();
    }else if(display.widget){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);
      if(layoutItem){
        appConfigAction.editWidgetProperty(layoutItem.widgetId, 'icon', result).exec();
      }
    }else if(display.section){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);
      if(layoutItem){
        appConfigAction.editSectionProperty(layoutItem.sectionId, 'icon', result).exec();
      }
    }else{
      return;
    }
  }

  focusEditTitle = (e) => {
    if(this.titleTextInput.current){
      this.titleTextInput.current.select();
      this.titleTextInput.current.focus();
    }
  }

  getAppConfig(): IMAppConfig{
    return getAppStore().getState().appStateInBuilder && getAppStore().getState().appStateInBuilder.appConfig;
  }

  renderChangeIcon(title: string, icon: IconResult | string, configurableOption?: 'color' | 'size' | 'all'){
    let result: IconResult;
    if(icon && icon['svg']){
      result = icon as any;
    }else {
      result = {svg: icon as any};
    }
    lodash.setValue(result, ['properties', 'inlineSvg'], true); // we assume the widget icons are inline svgs
    return (
      <SettingSection title={
        <div className="d-flex justify-content-between align-items-center" >
          {title}
          <IconPicker icon={result} onChange={this.handleIconChange} configurableOption={configurableOption} hideRemove />
        </div>
      }>
      </SettingSection>
    )
  }

  renderHeaderEditor = (id: string, type: 'widget' | 'section' | 'page', icon: IconResult | string) => {
    let result: IconResult;
    if(icon && icon['svg']){
      result = icon as any;
    }else {
      result = {svg: icon as any};
    }
    return (
      <div className="jimu-widget-setting--header d-flex align-items-center">
        {icon && <Icon
                  className="mr-1"
                  icon={result.svg}
                  color={result.properties && result.properties.color}
                  size={16}/>}
        <input className="header-title-input h5" ref={this.titleTextInput}
          value={this.state.titleText || ''}
          onChange={evt => this.setState({titleText: evt.target.value})}
          onBlur={e => this.handleTitleInputBlur(e, id, type)}
          onKeyDown ={ (e) => this.handleKeydown(e) }>
        </input>
        <Button size="sm" type="tertiary" icon className="ml-2" onClick={this.focusEditTitle}>
          <Icon icon={require('jimu-ui/lib/icons/tool-edit.svg')} className="header-title-icon"/>
        </Button>
        <span className="px-1 border font-weight-normal h5" style={{position: 'absolute', opacity: 0, whiteSpace: 'nowrap', zIndex: -1}} ref={this.spanTextInput}>
          {this.state.titleText}
        </span>
      </div>
    )
  }

  getDisplay = () => {
    const {selection} = this.props;
    const appConfig = this.getAppConfig();
    let display = {
      widget: false,
      section: false,
      layoutItem: false,
      page: false,
      link: false,
      folder: false,
      dialog: false,
      noSelection: false,
    }

    if(selection){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);

      if(layoutItem){
        if(layoutItem.type === LayoutItemType.Widget){
          if(layoutItem.widgetId){
            display.widget = true;
          }else{
            //it's place holder
            display.layoutItem = true;
          }
        }else if(layoutItem.type === LayoutItemType.Section){
          display.section = true;
        }else{
          display.layoutItem = true;
        }
      }else{
        display.noSelection = true;
      }
    }

    if(!selection){
      if(this.props.currentPageId){
        display.page = true;
        display.link = this.getPageType(this.props.currentPageId, appConfig.pages).type === PageType.Link;
        display.folder = this.getPageType(this.props.currentPageId, appConfig.pages).type === PageType.Folder;
      }else if(this.props.currentDialogId){
        display.dialog = true;
      }else{
        display.noSelection = true;
      }
    }
    return display;
  }

  render(){
    const appConfig = this.getAppConfig();
    const {selection, icon} = this.props;
    if(!appConfig){
      return <div css={this.getCss()} className="builder-dynamic-setting bg-light-300">
        {<PageSetting dispatch={this.props.dispatch} formatMessage={this.formatMessage} theme={this.props.theme}
        pageId={'empty'} browserSizeMode={this.props.browserSizeMode} appTheme={this.props.appTheme}
        ></PageSetting>}
      </div>;
    }
    const display = this.getDisplay();
    let settingContent;
    if(display.widget){
      let layoutItem = layoutUtils.findLayoutItem(appConfig, selection);
      if(!appConfig.widgets[layoutItem.widgetId]){
        settingContent = <div>{this.formatMessage('noSelection')}</div>;
      }else{
        const widget = appConfig.widgets[layoutItem.widgetId];
        let widgetManifest = widget.manifest;
        if(widgetManifest.properties.hasConfig && widgetManifest.properties.hasConfigInSettingPage){
          settingContent = <Tabs underline key="widget-setting" className="flex-grow-1 w-100 h-100" fill>
            <Tab title={this.formatMessage('content')} active={true} className="tab-title-item">
              <WidgetSetting formatMessage={this.formatMessage} dispatch={this.props.dispatch} widgetId={layoutItem.widgetId}
                widgetsSettingRuntimeInfo={this.props.widgetsSettingRuntimeInfo}>
              </WidgetSetting>
            </Tab>

            <Tab title={this.formatMessage('style')} className="tab-title-item">
              <div style={{overflow: 'auto', overflowX: 'hidden'}}>
                {this.renderChangeIcon(this.formatMessage('widgetIcon'), icon)}
                <LayoutItemSetting layoutId={selection.layoutId} layoutItemId={selection.layoutItemId}
                formatMessage={this.formatMessage}
                onSettingChanged={this.onLayoutItemSettingChanged}
                onStyleChange={this.onLayoutItemStyleChanged}
                onPosChanged={this.onLayoutPosChanged}/>
              </div>
            </Tab>

            <Tab title={this.formatMessage('action')} className="tab-title-item">
              <div style={{overflow: 'auto', overflowX: 'hidden'}}>
                <MessageSetting widgetId={layoutItem.widgetId} formatMessage={this.formatMessage}
                  pageId={urlUtils.getAppIdPageIdFromUrl().pageId}/>
              </div>
            </Tab>
          </Tabs>;
        }else{
          settingContent = <div className="h-100">
            <LayoutItemSetting layoutId={selection.layoutId} layoutItemId={selection.layoutItemId}
            formatMessage={this.formatMessage}
            onSettingChanged={this.onLayoutItemSettingChanged}
            onStyleChange={this.onLayoutItemStyleChanged}
            onPosChanged={this.onLayoutPosChanged}/>
          </div>;
        }

        settingContent = <div className="w-100 h-100 d-flex flex-column">
          {this.renderHeaderEditor(layoutItem.widgetId, 'widget', icon)}
          <div style={{height: 'calc(100% - 51px)'}}>{settingContent}</div>
        </div>
      }
    }

    if(display.layoutItem){
      let title = this.formatMessage('layout');

      settingContent = <div className="w-100 h-100 d-flex flex-column">
          <h4 className="jimu-widget-setting--header">
            {title}
          </h4>
          <div className="h-100" style={{overflow: 'auto', overflowX: 'hidden'}}>
            <LayoutItemSetting layoutId={selection.layoutId} layoutItemId={selection.layoutItemId}
            formatMessage={this.formatMessage}
            onSettingChanged={this.onLayoutItemSettingChanged}
            onStyleChange={this.onLayoutItemStyleChanged}
            onPosChanged={this.onLayoutPosChanged}/>
          </div>
      </div>;
    }
    if(display.section){
      const layoutItem = layoutUtils.findLayoutItem(appConfig, selection);
      const section = appConfig.sections[layoutItem.sectionId];
      settingContent = <Tabs underline className="flex-grow-1 h-100" fill>
        <Tab key="section-setting" title={this.formatMessage('content')} active={true}>
          <SectionSetting formatMessage={this.formatMessage} theme={this.props.theme} dispatch={this.props.dispatch} sectionId={section.id}>
          </SectionSetting>
        </Tab>

        <Tab title={this.formatMessage('general')}>
        <div className="h-100" style={{overflow: 'auto', overflowX: 'hidden'}}>
          {this.renderChangeIcon(this.formatMessage('sectionIcon'), icon)}
          <LayoutItemSetting layoutId={selection.layoutId} layoutItemId={selection.layoutItemId}
          formatMessage={this.formatMessage}
          onSettingChanged={this.onLayoutItemSettingChanged}
          onStyleChange={this.onLayoutItemStyleChanged}
          onPosChanged={this.onLayoutPosChanged}/>
        </div>

        </Tab>
      </Tabs>;
      settingContent = <div className="w-100 h-100 d-flex flex-column">
        {this.renderHeaderEditor(section.id, 'section', icon)}
        <div className="h-100" style={{overflow: 'auto', overflowX: 'hidden'}}>{settingContent}</div>
      </div>;
    }
    if(display.page){
      let title = this.formatMessage('pageIcon');
      settingContent = <PageSetting dispatch={this.props.dispatch} formatMessage={this.formatMessage}
      pageId={this.props.currentPageId} pageMode={this.props.pageMode} maxWidth={this.props.maxPageWidth}
      browserSizeMode={this.props.browserSizeMode} theme={this.props.theme}
      appTheme={this.props.appTheme}></PageSetting>;
      if(display.link){
        title = this.formatMessage('linkIcon');
        settingContent = <LinkSetting dispatch={this.props.dispatch} appConfig={appConfig} formatMessage={this.formatMessage} pageId={this.props.currentPageId}></LinkSetting>;
      }else if(display.folder){
        title = this.formatMessage('folderIcon');
        settingContent = <FolderSetting dispatch={this.props.dispatch} appConfig={appConfig} formatMessage={this.formatMessage} pageId={this.props.currentPageId}></FolderSetting>;
      }
      const {currentPageId} = this.props;
      settingContent = <div className="w-100 h-100 d-flex flex-column">
        {this.renderHeaderEditor(currentPageId, 'page', icon)}
        <div className="h-100" style={{overflow: 'auto', overflowX: 'hidden'}}>
          {this.renderChangeIcon(title, icon as any)}
          {settingContent}
        </div>
      </div>;
    }
    if(display.dialog){
      settingContent = <div className="h-100">dialog</div>;
    }
    if(display.noSelection){
      settingContent = <div className="h-100">{this.formatMessage('noSelection')}</div>;
    }

    return <div css={this.getCss()} className="builder-dynamic-setting bg-light-300">
      {settingContent}
    </div>;
  }

  // FOR DEMO:
  onToggleMe = () => {
    let rightToggle = document.getElementsByClassName('sidebar_handler_right');
    if(rightToggle) {
      (rightToggle[0] as HTMLElement).click();
    }
  }

}
