define({
  pageSetting: 'Å_Page settings______________ö',
  folderSetting: 'Å_Folder settings________________ö',
  linkSetting: 'Å_Link settings______________ö',
  noSelection: 'Å_No selection_____________ö',
  customWidth: 'Å_Fixed width____________ö',
  fitToWindow: 'Å_Fit to window______________ö',
  fitToWidthOnly: 'Å_Full width_____________________ö',
  fitToContainer: 'Å_Fit to container_________________ö',
  keepAspectRatio: 'Å_Keep aspect ratio__________________ö',
  aspectRatio: 'Å_Aspect ratio_____________ö',
  aspectRatioFull: 'Å_Aspect ratio (height/width)____________________________ö',
  fixedHeight: 'Å_Fixed height_____________ö',
  fixed: 'Å_Fixed___________ö',
  content: 'Å_Content_______________ö',
  style: 'Å_Style___________ö',
  action: 'Å_Action_____________ö',
  layout: 'Å_Layout_____________ö',
  general: 'Å_General_______________ö',
  dynamicLoading: 'Å_Loading_______________ö',
  displayMode: 'Å_Display mode_____________ö',
  width: 'Å_Width___________ö',
  height: 'Å_Height_____________ö',
  body: 'Å_Body_________ö',
  header: 'Å_Header_____________ö',
  footer: 'Å_Footer_____________ö',
  urlIsHere: 'Å_Type URL here_______________ö.',
  views: 'Å_Views___________ö',
  remove: 'Å_Remove_____________ö',
  duplicate: 'Å_Duplicate___________________ö',
  rename: 'Å_Rename_____________ö',
  noWidget: 'Å_No widget___________________ö',
  maxWidth: 'Å_Max width___________________ö',
  duplicateView: 'Å_Duplicate view_______________ö',
  addView: 'Å_New view_________________ö',
  noSettingOptions: 'Å_No setting options____________________ö.',
  contentWidth: 'Å_Content width______________ö',
  custom: 'Å_Custom_____________ö',
  auto: 'Å_Auto_________ö',
  gap: 'Å_Gap_______ö',
  padding: 'Å_Padding_______________ö',
  border: 'Å_Border_____________ö',
  borderRadius: 'Å_Border radius______________ö',
  boxShadow: 'Å_Box shadow_____________________ö',
  size: 'Å_Size_________ö',
  lockParent: 'Å_Lock parent____________ö',
  margin: 'Å_Margin_____________ö',
  addTrigger: 'Å_Add a trigger______________ö',
  noTrigger: 'Å_The widget does not generate any triggers______________________ö.',
  selectTrigger: 'Å_Select a trigger_________________ö',
  selectTarget: 'Å_Select a target________________ö',
  selectAction: 'Å_Select an action_________________ö',
  actionBack: 'Å_Back_________ö',
  actionSet: 'Å_Set_______ö',
  addAction: 'Å_Add action_____________________ö',
  targetWidgets: 'Å_Widgets_______________ö',
  noMessage: 'Å_No message_____________________ö',
  noTargetWidgets: 'Å_No widget___________________ö',
  noAction: 'Å_No action___________________ö',
  chooseSearchTrigger: 'Å_Select or search a trigger___________________________ö',
  chooseSearchTarget: 'Å_Select or search a target__________________________ö',
  chooseSearchAction: 'Å_Select or search an action___________________________ö',
  actionDone: 'Å_Done_________ö',
  actionFramework: 'Å_Framework___________________ö',
  actionSettingLoading: 'Å_Loading_____________________ö...',
  block: 'Å_Block___________ö',
  widgetIcon: 'Å_Widget icon____________ö',
  sectionIcon: 'Å_Section icon_____________ö',
  pageIcon: 'Å_Page icon___________________ö',
  linkIcon: 'Å_Link icon___________________ö',
  folderIcon: 'Å_Folder icon____________ö',
  change: 'Å_Change_____________ö',
  background: 'Å_Background_____________________ö',
  turnTo: 'Å_Switch to_____________________ö:'
});