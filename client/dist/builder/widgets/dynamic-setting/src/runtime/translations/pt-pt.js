define({
  pageSetting: 'ã_Page settings______________Ç',
  folderSetting: 'ã_Folder settings________________Ç',
  linkSetting: 'ã_Link settings______________Ç',
  noSelection: 'ã_No selection_____________Ç',
  customWidth: 'ã_Fixed width____________Ç',
  fitToWindow: 'ã_Fit to window______________Ç',
  fitToWidthOnly: 'ã_Full width_____________________Ç',
  fitToContainer: 'ã_Fit to container_________________Ç',
  keepAspectRatio: 'ã_Keep aspect ratio__________________Ç',
  aspectRatio: 'ã_Aspect ratio_____________Ç',
  aspectRatioFull: 'ã_Aspect ratio (height/width)____________________________Ç',
  fixedHeight: 'ã_Fixed height_____________Ç',
  fixed: 'ã_Fixed___________Ç',
  content: 'ã_Content_______________Ç',
  style: 'ã_Style___________Ç',
  action: 'ã_Action_____________Ç',
  layout: 'ã_Layout_____________Ç',
  general: 'ã_General_______________Ç',
  dynamicLoading: 'ã_Loading_______________Ç',
  displayMode: 'ã_Display mode_____________Ç',
  width: 'ã_Width___________Ç',
  height: 'ã_Height_____________Ç',
  body: 'ã_Body_________Ç',
  header: 'ã_Header_____________Ç',
  footer: 'ã_Footer_____________Ç',
  urlIsHere: 'ã_Type URL here_______________Ç.',
  views: 'ã_Views___________Ç',
  remove: 'ã_Remove_____________Ç',
  duplicate: 'ã_Duplicate___________________Ç',
  rename: 'ã_Rename_____________Ç',
  noWidget: 'ã_No widget___________________Ç',
  maxWidth: 'ã_Max width___________________Ç',
  duplicateView: 'ã_Duplicate view_______________Ç',
  addView: 'ã_New view_________________Ç',
  noSettingOptions: 'ã_No setting options____________________Ç.',
  contentWidth: 'ã_Content width______________Ç',
  custom: 'ã_Custom_____________Ç',
  auto: 'ã_Auto_________Ç',
  gap: 'ã_Gap_______Ç',
  padding: 'ã_Padding_______________Ç',
  border: 'ã_Border_____________Ç',
  borderRadius: 'ã_Border radius______________Ç',
  boxShadow: 'ã_Box shadow_____________________Ç',
  size: 'ã_Size_________Ç',
  lockParent: 'ã_Lock parent____________Ç',
  margin: 'ã_Margin_____________Ç',
  addTrigger: 'ã_Add a trigger______________Ç',
  noTrigger: 'ã_The widget does not generate any triggers______________________Ç.',
  selectTrigger: 'ã_Select a trigger_________________Ç',
  selectTarget: 'ã_Select a target________________Ç',
  selectAction: 'ã_Select an action_________________Ç',
  actionBack: 'ã_Back_________Ç',
  actionSet: 'ã_Set_______Ç',
  addAction: 'ã_Add action_____________________Ç',
  targetWidgets: 'ã_Widgets_______________Ç',
  noMessage: 'ã_No message_____________________Ç',
  noTargetWidgets: 'ã_No widget___________________Ç',
  noAction: 'ã_No action___________________Ç',
  chooseSearchTrigger: 'ã_Select or search a trigger___________________________Ç',
  chooseSearchTarget: 'ã_Select or search a target__________________________Ç',
  chooseSearchAction: 'ã_Select or search an action___________________________Ç',
  actionDone: 'ã_Done_________Ç',
  actionFramework: 'ã_Framework___________________Ç',
  actionSettingLoading: 'ã_Loading_____________________Ç...',
  block: 'ã_Block___________Ç',
  widgetIcon: 'ã_Widget icon____________Ç',
  sectionIcon: 'ã_Section icon_____________Ç',
  pageIcon: 'ã_Page icon___________________Ç',
  linkIcon: 'ã_Link icon___________________Ç',
  folderIcon: 'ã_Folder icon____________Ç',
  change: 'ã_Change_____________Ç',
  background: 'ã_Background_____________________Ç',
  turnTo: 'ã_Switch to_____________________Ç:'
});