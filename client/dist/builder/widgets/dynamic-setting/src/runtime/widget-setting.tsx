import {IMRuntimeInfos, React, ReactRedux, ErrorBoundary, IMWidgetJson, IMState, WidgetJson, DataSourceJson} from 'jimu-core';
import {WidgetSettingManager, getAppConfigAction} from 'jimu-for-builder';

interface Props{
  widgetsSettingRuntimeInfo: IMRuntimeInfos;
  widgetId: string;
  dispatch: any,
  formatMessage: (id: string) => string
}
interface StateProps{
  widgetJson: IMWidgetJson
}
export class _WidgetSetting extends React.PureComponent<Props & StateProps>{
  constructor(props){
    super(props);
  }

  componentDidUpdate(){
    this.loadWidgetSettingClass();
  }

  componentDidMount(){
    this.loadWidgetSettingClass();
  }

  formatMessage = (id: string) => {
    return this.props.formatMessage(id);
  }

  loadWidgetSettingClass(){
    let {widgetId} = this.props;

    if(!widgetId){
      return;
    }
    
    !WidgetSettingManager.getInstance().getWidgetSettingClass(widgetId) && WidgetSettingManager.getInstance().loadWidgetSettingClass(widgetId);
  }

  onSettingChange = (widgetJson: Partial<WidgetJson>, outputDataSourcesJson?: DataSourceJson[]) => {

    let appConfigAction = getAppConfigAction();
    
    return appConfigAction.editWidget(widgetJson, outputDataSourcesJson).exec();
  }


  renderWidgetSetting = (widgetId: string) => {
    let SettingClass = this.props.widgetsSettingRuntimeInfo[widgetId] && this.props.widgetsSettingRuntimeInfo[widgetId].isClassLoaded ?
      WidgetSettingManager.getInstance().getWidgetSettingClass(widgetId) : null;
    return SettingClass ? 
      <ErrorBoundary>
        <SettingClass onSettingChange={this.onSettingChange}/>
      </ErrorBoundary>
       : <div>{this.formatMessage('dynamicLoading')}...</div>
  };

  render(){
    if(!this.props.widgetJson){
      return <div>{this.formatMessage('dynamicLoading')}...</div>;
    }
    let {widgetId} = this.props;
    return <div className="setting-container h-100">
      {widgetId ? this.renderWidgetSetting(widgetId) : <div>&nbsp;&nbsp;{this.formatMessage('noWidget')}</div>}
    </div>;
  }

  // FOR DEMO:
  onToggleMe = () => {
    let rightToggle = document.getElementsByClassName('sidebar_handler_right');
    if(rightToggle) {
      (rightToggle[0] as HTMLElement).click();
    }
  }
  // TODO: REMOVE AFTER DEMO

}

export default ReactRedux.connect((state: IMState, ownProps: Props) => {
  return {
    widgetJson: state.appStateInBuilder && state.appStateInBuilder.appConfig && state.appStateInBuilder.appConfig.widgets[ownProps.widgetId]
  }
})(_WidgetSetting);