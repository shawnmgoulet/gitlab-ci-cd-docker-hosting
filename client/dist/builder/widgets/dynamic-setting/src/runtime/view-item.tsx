/** @jsx jsx */
import { React, jsx, css, ThemeVariables, Immutable, appActions, polished,
 IMViewJson, classNames, lodash } from 'jimu-core';
// import * as ReactDnd from 'jimu-core/dnd';
import { getAppConfigAction} from 'jimu-for-builder';
import {Input, Button, Icon} from 'jimu-ui';
import {MyDropDown} from '../../../toc/src/runtime/components/my-dropdown';
import { interact } from 'jimu-core/dnd';

interface ViewProps {
  sectionId: string;
  view: IMViewJson;
  isLastItem: boolean;
  isFirstItem: boolean;
  listDraggingStatus: 'below' | 'above' | 'on';
  theme: ThemeVariables;
  canRemove: boolean;
  selected: boolean;
  dispatch: any;
  index: number;
  isListDragging?: boolean;
  onSelect: (vid: string) => void;
  moveItem: (dragIndex: number, hoverIndex: number) => void;
  onTocDragStatusChage?: (isDragging: boolean) => void;
  formatMessage: (id: string) => string;
  showBackgroundWidget: (index: number) => void;
}

// interface DragSourceCollectedProps {
//   isDragging?: boolean
//   connectDragSource?: ReactDnd.ConnectDragSource
// }

// interface DropTargetCollectedProps {
//   connectDropTarget?: ReactDnd.ConnectDropTarget
// }

interface ViewState {
  editingLabel: boolean;
  dropType: 'moveInto' | 'above' | 'below' | 'none';
  isDragging: boolean;
  isHovering: boolean;
}

export default class ViewItem extends React.PureComponent<ViewProps,
ViewState> {
  inputRef: HTMLInputElement;
  dropDownItems;
  dropZoneInteractable: Interact.Interactable;
  dragInteractable: Interact.Interactable;
  dropZoneRef: HTMLDivElement;
  dragRef: HTMLDivElement;

  constructor(props) {
    super(props);
    this.state = {
      editingLabel: false,
      dropType: 'none',
      isDragging: false,
      isHovering: false
    };
  }

  componentWillUnmount() {
    if (this.dragInteractable) {
      this.dragInteractable.unset();
      this.dragInteractable = null;
    }
    if (this.dropZoneInteractable) {
      this.dropZoneInteractable.unset();
      this.dropZoneInteractable = null;
    }
  }

  componentDidUpdate() {
    if (this.inputRef) {
      this.inputRef.select();
      this.inputRef.focus();
    }
  }

  componentDidMount() {
    if (this.inputRef) {
      this.inputRef.select();
      this.inputRef.focus();
    }
    if (this.dropZoneRef && this.dragRef) {
      const {view} = this.props;
      let lastMoveCall = null;
      this.dragRef.setAttribute('itemJson', JSON.stringify(view));
      this.dropZoneInteractable = interact(this.dropZoneRef)
        .dropzone({
          // only accept elements matching this CSS selector
          accept: '.view-item-drag',
          overlap: 'pointer',
          ondropactivate: event => {
          },
          ondropmove: event => {
            const dragElement = event.relatedTarget;
            const dropzoneElement = event.target;
            const dragItemJson = JSON.parse(dragElement.getAttribute('itemJson'));
            const hoverBoundingRect = dropzoneElement.getBoundingClientRect();
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
            const clientOffset = event.dragEvent.client;
            const hoverClientY = clientOffset.y - hoverBoundingRect.top;

            let dropType = this.state.dropType;
            if (dragItemJson.id !== view.id) {
              if (this.props.index === 0) {
                if (hoverClientY > hoverMiddleY) {//out and order bottom target
                  dropType = 'below';
                } else {//out and order top target
                  dropType = 'above';
                }
              } else {
                dropType = 'below'
              }
            } else {
              dropType = 'none'
            }
            this.onDropHover(dropType);
          },
          ondragenter: event => {
          },
          ondragleave: event => {
            this.onDropHover('none');
          },
          ondrop: event => {
            const dropType = this.state.dropType;
            if (dropType === 'none') return;
            const dragElement = event.relatedTarget;
            const dragItemJson = JSON.parse(dragElement.getAttribute('itemJson'));
            this.onDidDrop(dragItemJson, view, dropType as any);
            this.onDropHover('none');
          },
          ondropdeactivate: event => {

          }
        })
      this.dragInteractable = interact(this.dragRef).draggable(
        {
          inertia: false,
          modifiers: [],
          autoScroll: true,
          onstart: e => {
            this.setState({
              isDragging: true
            })
            const {onTocDragStatusChage} = this.props;
            if(onTocDragStatusChage){
              onTocDragStatusChage(true);
            }
          },
          onmove: event => {
            const { clientX, clientY, clientX0, clientY0, target } = event;
            // keep the dragged position in the data-x/data-y attributes
            let startX = (parseFloat(target.getAttribute('start-x')) || 0),
              startY = (parseFloat(target.getAttribute('start-y')) || 0),
              x = clientX - clientX0 + startX,
              y = clientY - clientY0 + startY
            const leftClientThreshold = -target.clientWidth / 2;
            const rightClientThreshold = target.clientWidth / 2;

            if (x < leftClientThreshold) {
              x = leftClientThreshold;
            } else if (x > rightClientThreshold) {
              x = rightClientThreshold;
            }
            if (lastMoveCall) {
              cancelAnimationFrame(lastMoveCall);
            }

            lastMoveCall = requestAnimationFrame(() => {
              target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';
              // Since this frame didn't get cancelled, the lastUpdateCall should be reset so new frames can be called.
              lastMoveCall = null;
            });
          },
          onend: e => {
            const { target } = e;
            if (lastMoveCall) {
              cancelAnimationFrame(lastMoveCall);
            }
            target.style.webkitTransform =
              target.style.transform =
              'translate(' + 0 + 'px, ' + 0 + 'px)';
            this.setState({
              isDragging: false
            })
            const {onTocDragStatusChage} = this.props;
            if(onTocDragStatusChage){
              onTocDragStatusChage(false);
            }
          }
        }
      )
    }
  }

  onDidDrop = (viewJson: IMViewJson, targetViewJson: IMViewJson, dropType: 'moveInto' | 'above' | 'below') => {
    const {sectionId} = this.props;
    if(dropType === 'moveInto'){

    }else{
      getAppConfigAction().moveViewInSection(sectionId, viewJson.id, targetViewJson.id, dropType).exec();
      // this.props.reOrderPage(itemJson.data.id, droppedItemJson.data.id, dropType);
    }
  }

  onDropHover = (dropType: 'moveInto' | 'above' | 'below' | 'none') => {
    if (this.state.dropType === dropType) return;
    this.setState({
      dropType: dropType
    })
  }

  formatMessage = (id: string) => {
    return this.props.formatMessage(id);
  }

  _removeView() {
    const { view, sectionId, dispatch } = this.props;
    getAppConfigAction().removeView(view.id, sectionId).exec();
    dispatch(appActions.selectionChanged(null));
  }

  _duplicateView() {
    if(!this.props.view){
      return;
    }
    let appConfigAction = getAppConfigAction();
    const newView = appConfigAction.duplicateView(this.props.view.id, this.props.sectionId, true);
    appConfigAction.exec()
    lodash.defer(() => {
      this.props.onSelect(newView.id);
    });
  }

  _select = (e) => {
    e.stopPropagation();
    this.props.onSelect(this.props.view.id);
  }

  _enableEditing = (e) => {
    e.stopPropagation();
    this.setState({
      editingLabel: true
    });
  }

  _disableEditing = () => {
    this._onLabelUpdate(this.inputRef.value);
    this.setState({
      editingLabel: false
    });
  }

  _onLabelUpdate = (value) => {
    const { view } = this.props;
    const updatedView = Immutable(view).set('label', value);
    getAppConfigAction().editView(updatedView).exec();
  }

  _focusInput() {
    if (this.inputRef) {
      this.inputRef.select();
      this.inputRef.focus();
    }
  }

  _getStyle() {
    const {theme, isListDragging} = this.props;
    const {isHovering, isDragging} = this.state;
    return css`
    padding: 7px 10px;
    cursor: pointer;
    position: relative;
    .drop-down {
      visibility: hidden;
      text-align:right;
      .popover{
        .popover-inner {
          box-shadow: 0 0 0;
        }
      }
    }
    .drag-icon {
      visibility: hidden;
      margin-right:${polished.rem(8)};
      color:${theme.colors.palette.dark[600]};
    }
    .view-item-drag:hover {
      cursor: pointer !important;
    }
    .label-con {
      width: ${polished.rem(123)};
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .label-input {
      width: ${polished.rem(123)};
    }
    .view-item-drag {
      pointer-events: ${isHovering ? 'all' : 'none'};
      z-index: 1;
      position: absolute;
      padding: 7px 10px;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background-color: ${isDragging ? polished.rgba(theme.colors.palette.light[400], 0.6) : 'transparent'};
      box-shadow: ${isDragging ? theme.boxShadows.lg : 'none'};
    }

    .drag-move-out-order-bottom {
      background-color: ${polished.rgba(theme.colors.palette.light[400], 0.4)};
      :after{
        content: '';
        position: absolute;
        left: 0;
        top: auto;
        bottom: 0;
        right: auto;
        height: 2px;
        width: 100%;
        background-color: ${theme.colors.palette.primary[700]};
      }
    }

    .drag-move-out-order-top {
      background-color: ${polished.rgba(theme.colors.palette.light[400], 0.4)};
      :before {
        
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        bottom: auto;
        right: auto;
        height: 2px;
        width: 100%;
        background-color: ${theme.colors.palette.primary[700]};
      }
    }


    &.selected {
      ${
        isListDragging ?
        '' :
        `background-color: ${theme.colors.palette.light[500]};`
      }
      border: 0;
      background-color: ${theme.colors.palette.light[500]};
    }

    &:hover {
      .drop-down {
        visibility: visible;
      }
      .drag-icon {
        visibility: visible;
      }
    }

    &:not(.selected):hover {
      background-color: ${polished.rgba(theme.colors.palette.light[500], 0.4)};
    }
    `;
  }

  _showBackgroundWidget = () => {
    this.props.showBackgroundWidget(this.props.index);
  }

  render() {
    const { theme, canRemove, selected, listDraggingStatus, isListDragging, isLastItem, isFirstItem} = this.props;
    const {label} = this.props.view;
    const { editingLabel, dropType } = this.state;
    
    let moveOutClassName = 'drag-move-out-order-' + dropType;
    if(isListDragging && listDraggingStatus !== 'on'){
      if(listDraggingStatus === 'below' && isLastItem){
        moveOutClassName = 'drag-move-out-order-' + listDraggingStatus;
      }else if(listDraggingStatus === 'above' && isFirstItem){
        moveOutClassName = 'drag-move-out-order-' + listDraggingStatus;
      }
    }
    if (editingLabel) {
      this._focusInput();
    }

    const dropDownItems = [Immutable({
      label: this.formatMessage('remove'),
      event: this._removeView.bind(this),
      visible: canRemove === true
    }), Immutable({
      label: this.formatMessage('duplicate'),
      event: this._duplicateView.bind(this),
      visible: true
    }), Immutable({
      label: this.formatMessage('rename'),
      event: this._enableEditing.bind(this),
      visible: true
    })];

    const classes = classNames('d-flex justify-content-between w-100', {
      selected
    });

    return (
      <div ref={dom => this.dropZoneRef = dom} 
          css={this._getStyle()} 
          className={classes} 
          onClick={this._select} 
          onMouseEnter={evt => setTimeout(() => {
            this.setState({isHovering: true})
          }, 100)}
          onMouseLeave={evt => setTimeout(() => {
            this.setState({isHovering: false})
          }, 100)}
      >
        <div className={`w-100 d-flex justify-content-between`}>
          <div className="d-flex">
            <div className="toc-item-icon drag-icon">
              <Icon size="12" icon={require('jimu-ui/lib/icons/drag.svg')}  className="header-title-icon"/>
            </div>
            <div className="label-con" onDoubleClick={this._enableEditing}>{label}</div>
          </div>
          <div className="drop-down d-flex toc-item-icon">
            <Button size="sm" type="tertiary" icon>
              <Icon size="12" icon={require('jimu-ui/lib/icons/design.svg')} className="header-title-icon"/>
            </Button>
            <MyDropDown theme={theme} items={dropDownItems}/>
          </div>
        </div>

        <div className={`w-100 view-item-drag  d-flex justify-content-between ${moveOutClassName}`} ref={dom => this.dragRef = dom}>
          <div className="d-flex">
            <div className="toc-item-icon drag-icon">
              <Icon size="12" icon={require('jimu-ui/lib/icons/drag.svg')}  className="header-title-icon"/>
            </div>
            {!editingLabel && <div className="label-con" onDoubleClick={this._enableEditing}>{label}</div>}
            {editingLabel && <div><Input className="label-input" type="text" value={label} ref={el => {this.inputRef = el}}
              onBlur={this._disableEditing}></Input></div>}
          </div>

          <div className="drop-down d-flex toc-item-icon">
            <Button size="sm" type="tertiary" icon onClick={this._showBackgroundWidget}>
              <Icon size="12" icon={require('jimu-ui/lib/icons/design.svg')} className="header-title-icon"/>
            </Button>
            <MyDropDown theme={theme} items={dropDownItems}/>
          </div>
        </div>

      </div>
    );
  }
}
