/** @jsx jsx */
import {React, jsx, IMState, ReactRedux, IMViewJson, SerializedStyles, css, ThemeVariables, polished, Immutable} from 'jimu-core';
import { getAppConfigAction} from 'jimu-for-builder';
import { SettingSection, SettingRow} from 'jimu-ui/setting-components';
import { Icon, BackgroundStyle, FillType } from 'jimu-ui';
import { BackgroundSetting } from 'jimu-ui/style-setting-components';

interface SectionProps {
  viewId: string;
  theme?: ThemeVariables;
  hideBackgroundWidget: () => void;
  formatMessage: (id: string) => string;
  view?: IMViewJson;
}

interface StateToProps {
  view?: IMViewJson;
}
interface state {
  config: BackgroundStyle
}
// export default class ViewItem extends React.PureComponent

class ViewSetting extends React.PureComponent<SectionProps, state> {

  constructor(props){
    super(props);
    this.state = {
      config: {
        fillType: FillType.FIT,
        color: this.props.view.backgroundColor || ''
      },
    }
  }

  onBackgroundStyleChange = (config) => {
    this.setState({
      config: config
    });
    const url = config.image ? config.image : {};
    let _config = Immutable({
      id: this.props.view.id,
      label: this.props.view.label,
      layout: this.props.view.layout,
      backgroundPosition: config.fillType,
      backgroundColor: config.color,
      backgroundIMage: JSON.stringify(url) 
    });
    getAppConfigAction().editView(_config).exec();
  }

  // nls = (worldKey: string): string => {
  //   return this.props.intl ? this.props.intl.formatMessage({id: 'headerLeaveDescription', defaultMessage: defaultMessages.headerLeaveDescription}) : defaultMessages.headerLeaveDescription
  // }
  
  _getStyle = (): SerializedStyles  => {
    const dark600 = this.props.theme ? this.props.theme.colors.palette.dark[600] : '#c5c5c5';
    const dark400 = this.props.theme ? this.props.theme.colors.palette.dark[600] : '#a8a8a8';
    return css`.back-btn-con {
        margin-bottom:20px;
        cursor: pointer;
        align-items:center;
      }
      .back-button {
        margin-right:${polished.rem(8)};
        color:${dark600};
        font-size:${polished.rem(14)};
      }
      .background-set-back {
        color:${dark400};
        font-weight:500;
      }
      .background-setting-con .form-group {
        margin-bottom:${polished.rem(16)};
      }
    `
  }
  
  render() {
    const dark400 = this.props.theme ? this.props.theme.colors.palette.dark[600] : '#a8a8a8';
    const {view} = this.props;
    if(!view){
      return null;
    }
    return (
      <div css={this._getStyle()}>
        <SettingSection>
          <SettingRow>
            <div className="d-flex back-btn-con" onClick={this.props.hideBackgroundWidget}>
              <div className="back-button" >
                <Icon size="16" color={dark400} icon={require('jimu-ui/lib/icons/direction-left.svg')} className="sort-arrow-down-icon"/>
              </div>
              <div className="flex-grow-1 background-set-back">{this.props.formatMessage('actionBack')}</div>
            </div>
          </SettingRow>

          <SettingRow label={this.props.formatMessage('background')} className="background-setting-con" flow="wrap">
            <BackgroundSetting
                background={this.state.config}
                onChange={value => this.onBackgroundStyleChange(value)} />
          </SettingRow>
        </SettingSection>
      </div>
    );
  }
}

const mapStateToProps = (state: IMState, ownProps: SectionProps) => {
  const {viewId} = ownProps;
  const {appConfig} = state.appStateInBuilder;

  const view = appConfig.views[viewId];

  return {
    view
  }
};

export default ReactRedux.connect<StateToProps, {}, SectionProps>(mapStateToProps)(ViewSetting);