/** @jsx jsx */
import {
  React, jsx, css, Immutable, appConfigUtils, IMState, ReactRedux,
  IMLayoutJson, IMSectionJson, IMViewJson, ThemeVariables,
  lodash,
  BrowserSizeMode,
  SerializedStyles,
  ImmutableArray,
  ViewJson
} from 'jimu-core';
import {
  getAppConfigAction, builderAppSync
} from 'jimu-for-builder';
import {
  SettingSection, SettingRow
} from 'jimu-ui/setting-components';
import { Icon, Button } from 'jimu-ui';
import ViewItem from './view-item';
import { interact } from 'jimu-core/dnd';
import ViewSetting from './view-setting'

interface SectionProps {
  sectionId: string,
  dispatch: any,
  theme: ThemeVariables,
  formatMessage: (id: string) => string
}

interface StateToProps {
  section?: IMSectionJson;
  views?: ImmutableArray<ViewJson>;
  activeViewId?: string;
  browserSizeMode: BrowserSizeMode;

}

interface Stats {
  isViewListDragging: boolean,
  draggingStatus: 'below' | 'above' | 'on',
  selectViewId: number,
  isShowBackgroundWidget: boolean
}

class SectionSetting extends React.PureComponent<SectionProps & StateToProps, Stats> {

  dropZoneRef: any;
  dropZoneInteractable: any;
  viewListRef: React.RefObject<HTMLDivElement>;

  constructor(props) {
    super(props)
    this.dropZoneRef = React.createRef();
    this.viewListRef = React.createRef<HTMLDivElement>();
    this.state = {
      isViewListDragging: false,
      draggingStatus: 'on',
      selectViewId: 0,
      isShowBackgroundWidget: false
    }
  }

  componentDidMount() {
    if (this.dropZoneRef.current) {
      this.dropZoneInteractable = interact(this.dropZoneRef.current)
        .dropzone({
          // only accept elements matching this CSS selector
          accept: '.view-item-drag',
          overlap: 'pointer',
          ondropactivate: event => {
          },
          ondropmove: event => {
          },
          ondragenter: event => {
            console.log('enter view-list')
            const {views} = this.props;
            const { relatedTarget, dragEvent } = event;
            if(this.viewListRef.current && views && views.length > 0){
              const draggingViewJson = JSON.parse(relatedTarget.getAttribute('itemJson'));
              const listTop = this.viewListRef.current.getBoundingClientRect().top;
              const clientOffset = dragEvent.client;
              if(clientOffset.y < listTop){
                const firstView = views[0]
                if(firstView.id !== draggingViewJson.id){
                  this.handleDraggingStatusChange('top');
                }
              }else {
                const lastView = views[views.length - 1];
                if(lastView.id !== draggingViewJson.id){
                  this.handleDraggingStatusChange('bottom');
                }
              }
            }
          },
          ondragleave: event => {
            this.handleDraggingStatusChange('on');
            console.log('leave view-list')
          },
          ondrop: event => {
            const {views} = this.props;
            const dropStatus = this.state.draggingStatus;
            if (dropStatus === 'on') return;
            let targetViewJson = undefined;
            if(dropStatus === 'below'){
              const lastViewJson = views[views.length - 1];
              targetViewJson = lastViewJson;
            }else {
              targetViewJson = views[0];
            }
            const dragElement = event.relatedTarget;
            const draggingViewJson = JSON.parse(dragElement.getAttribute('itemJson'));
            this.onDidDrop(draggingViewJson, targetViewJson, dropStatus);
            this.handleDraggingStatusChange('on');
          },
          ondropdeactivate: event => {

          }
        })
    }
  }

  componentWillUnmount() {
    if (this.dropZoneInteractable) {
      this.dropZoneInteractable.unset();
      this.dropZoneInteractable = null;
    }
  }

  onDidDrop = (viewJson: IMViewJson, targetViewJson: IMViewJson, dropType: 'moveInto' | 'above' | 'below') => {
    const {sectionId} = this.props;
    if(dropType === 'moveInto'){

    }else{
      getAppConfigAction().moveViewInSection(sectionId, viewJson.id, targetViewJson.id, dropType).exec();
      // this.props.reOrderPage(itemJson.data.id, droppedItemJson.data.id, dropType);
    }
  }

  handleDraggingStatusChange = (status) => {
    this.setState({
      draggingStatus: status
    })
  }

  formatMessage = (id: string) => {
    return this.props.formatMessage(id);
  }

  _addView = () => {
    const { section, browserSizeMode } = this.props;
    const appConfigAction = getAppConfigAction();
    const appConfig = appConfigAction.appConfig;
    let layoutId = appConfigUtils.getUniqueId(appConfig, 'layout');
    let viewLayout = Immutable({}).merge({
      id: layoutId,
    }) as IMLayoutJson;

    let viewJson = Immutable({}).merge({
      id: appConfigUtils.getUniqueId(appConfig, 'view'),
      label: appConfigUtils.getUniqueLabel(appConfig, 'view', 'view'),
      layout: { [browserSizeMode]: layoutId },
    }) as IMViewJson;
    getAppConfigAction().addView(viewJson, section.id, [viewLayout]).exec();
    lodash.defer(() => {
      this._selectView(viewJson.id);
    });
  }

  _duplicateView = () => {
    const { activeViewId, sectionId } = this.props;
    const appConfigAction = getAppConfigAction();
    const newView = appConfigAction.duplicateView(activeViewId, sectionId, true);
    appConfigAction.exec();
    lodash.defer(() => {
      this._selectView(newView.id);
    });
  }

  _selectView = (vid: string) => {
    const { section } = this.props;
    builderAppSync.publishViewChangeToApp(section.id, vid);
  }

  _sortView = (dragIndex: number, hoverIndex: number) => {
    const { section } = this.props;
    let views = section.views.asMutable();
    // move dragIndex before the hoverIndex
    const dragView = views[dragIndex];
    const hoverView = views[hoverIndex];
    views = views.splice(dragIndex, 1);
    const idx = views.indexOf(hoverView);
    views = views.splice(idx, 0, dragView);
    const updatedSection = Immutable(section).set('views', views);
    getAppConfigAction().editSection(updatedSection).exec();
  }

  handleOnTocDragStatusChange = (isDragging: boolean) => {
    this.setState({
      isViewListDragging: isDragging,
    })
  }

  getStyle = (): SerializedStyles => {
    return css`
      .jimu-widget-setting--row.row{
        margin-top: 0;
      }
      .jimu-widget-setting--row-label{
        color:${this.props.theme.colors.palette.dark[600]};
      }
    `
  }

  showBackgroundWidget = (index: number) => {
    this.setState({
      selectViewId: index,
      isShowBackgroundWidget: true
    });
  }

  hideBackgroundWidget = () => {
    this.setState({
      isShowBackgroundWidget: false
    });
  }

  render() {
    const { section, views, theme, dispatch, activeViewId } = this.props;
    const { isViewListDragging, draggingStatus, isShowBackgroundWidget, selectViewId } = this.state
    if (!section) {
      return null;
    }
    const buttonStyle = css`
      cursor:pointer;
    `;

    return (
      <div className="flexbox-layout-setting h-100" css={this.getStyle()}>

        <div ref={this.dropZoneRef}
              className={`toc-dropzone h-100 w-100`}>
          {
            !isShowBackgroundWidget &&
            <SettingSection className="h-100">
              <SettingRow label={this.formatMessage('views')}>
                <Button css={buttonStyle} type="link" onClick={this._addView} title={this.formatMessage('addView')} >
                  <Icon icon={require('jimu-ui/lib/icons/add.svg')} color={this.props.theme.colors.palette.dark[400]} className="mr-0"></Icon>
                </Button>
                <Button css={buttonStyle} type="link" onClick={this._duplicateView} title={this.formatMessage('duplicateView')}>
                  <Icon icon={require('jimu-ui/lib/icons/duplicate-16.svg')} color={this.props.theme.colors.palette.dark[400]} className="mr-0"></Icon>
                </Button>
              </SettingRow>

              <div ref={this.viewListRef}>
                {views.asMutable().map((view, index) => {
                  return <SettingRow key={view.id}>
                    <ViewItem 
                      index={index} 
                      canRemove={views.length > 1} 
                      formatMessage={this.formatMessage} 
                      isLastItem={index === views.length - 1}
                      dispatch={dispatch} 
                      sectionId={section.id} 
                      theme={theme} 
                      view={view} 
                      isFirstItem={index === 0}
                      onSelect={this._selectView} 
                      isListDragging={isViewListDragging} 
                      listDraggingStatus={draggingStatus}
                      selected={activeViewId === view.id} 
                      onTocDragStatusChage={this.handleOnTocDragStatusChange}
                      moveItem={this._sortView} 
                      showBackgroundWidget={this.showBackgroundWidget}
                    />
                  </SettingRow>
                })}
              </div>
            </SettingSection>
          }
          {
            isShowBackgroundWidget && <SettingSection>
                <SettingRow>
                  <ViewSetting 
                    formatMessage={this.formatMessage} 
                    theme= {this.props.theme}
                    viewId = {views[selectViewId].id} 
                    view = {views[selectViewId]}
                    hideBackgroundWidget = {this.hideBackgroundWidget}
                  ></ViewSetting>
                </SettingRow>
            </SettingSection>
          }      
          
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state: IMState, ownProps: SectionProps) => {
  const { sectionId } = ownProps;
  const { appConfig } = state.appStateInBuilder;
  const section = appConfig.sections[sectionId];
  if (!section) {
    return null;
  }
  const views = section.views.map(viewId => {
    return appConfig.views[viewId];
  });
  let activeViewId;
  if (state.appStateInBuilder.appRuntimeInfo.currentViewId) {
    activeViewId = state.appStateInBuilder.appRuntimeInfo.currentViewId
    if (!views.find(view => view.id === activeViewId)) {
      activeViewId = section.views[0];
    }
  } else {
    activeViewId = section.views[0];
  }

  return {
    section,
    views,
    activeViewId,
    browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode
  }
};

export default ReactRedux.connect<StateToProps, {}, SectionProps>(mapStateToProps)(SectionSetting);