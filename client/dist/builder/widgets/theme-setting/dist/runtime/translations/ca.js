define({
  chooseTemplate: 'ó_Theme___________à',
  loading: 'ó_Loading_______________à',
  customTheme: 'ó_Customize___________________à',
  reset: 'ó_Reset___________à',
  back: 'ó_Back_________à',
  close: 'ó_Close___________à',
  customPaletteTitle: 'ó_Customize palette colors_________________________à',
  customFontsetTitle: 'ó_Customize font set___________________à',
  customAppElementsTitle: 'ó_App elements_____________à'
  // discardChangesWarningPart1: 'ó_Customize theme_________________à:',
  // discardChangesWarningPart2: 'ó_Changes made to theme________________________à: ',
  // discardChangesWarningPart3: 'ó_will be DISCARDED___________________à.',
  // yesToCustomize: 'ó_Yes, Customize_______________à',
  // noToCancel: 'ó_Cancel_____________à',
});