define({
  chooseTemplate: 'Č_Theme___________ž',
  loading: 'Č_Loading_______________ž',
  customTheme: 'Č_Customize___________________ž',
  reset: 'Č_Reset___________ž',
  back: 'Č_Back_________ž',
  close: 'Č_Close___________ž',
  customPaletteTitle: 'Č_Customize palette colors_________________________ž',
  customFontsetTitle: 'Č_Customize font set___________________ž',
  customAppElementsTitle: 'Č_App elements_____________ž'
  // discardChangesWarningPart1: 'Č_Customize theme_________________ž:',
  // discardChangesWarningPart2: 'Č_Changes made to theme________________________ž: ',
  // discardChangesWarningPart3: 'Č_will be DISCARDED___________________ž.',
  // yesToCustomize: 'Č_Yes, Customize_______________ž',
  // noToCancel: 'Č_Cancel_____________ž',
});