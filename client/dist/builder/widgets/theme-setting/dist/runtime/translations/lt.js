define({
  chooseTemplate: 'Į_Theme___________š',
  loading: 'Į_Loading_______________š',
  customTheme: 'Į_Customize___________________š',
  reset: 'Į_Reset___________š',
  back: 'Į_Back_________š',
  close: 'Į_Close___________š',
  customPaletteTitle: 'Į_Customize palette colors_________________________š',
  customFontsetTitle: 'Į_Customize font set___________________š',
  customAppElementsTitle: 'Į_App elements_____________š'
  // discardChangesWarningPart1: 'Į_Customize theme_________________š:',
  // discardChangesWarningPart2: 'Į_Changes made to theme________________________š: ',
  // discardChangesWarningPart3: 'Į_will be DISCARDED___________________š.',
  // yesToCustomize: 'Į_Yes, Customize_______________š',
  // noToCancel: 'Į_Cancel_____________š',
});