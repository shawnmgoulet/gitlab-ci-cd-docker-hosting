define({
  chooseTemplate: 'ı_Theme___________İ',
  loading: 'ı_Loading_______________İ',
  customTheme: 'ı_Customize___________________İ',
  reset: 'ı_Reset___________İ',
  back: 'ı_Back_________İ',
  close: 'ı_Close___________İ',
  customPaletteTitle: 'ı_Customize palette colors_________________________İ',
  customFontsetTitle: 'ı_Customize font set___________________İ',
  customAppElementsTitle: 'ı_App elements_____________İ'
  // discardChangesWarningPart1: 'ı_Customize theme_________________İ:',
  // discardChangesWarningPart2: 'ı_Changes made to theme________________________İ: ',
  // discardChangesWarningPart3: 'ı_will be DISCARDED___________________İ.',
  // yesToCustomize: 'ı_Yes, Customize_______________İ',
  // noToCancel: 'ı_Cancel_____________İ',
});