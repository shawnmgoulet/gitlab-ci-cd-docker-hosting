define({
  chooseTemplate: 'Ж_Theme___________Я',
  loading: 'Ж_Loading_______________Я',
  customTheme: 'Ж_Customize___________________Я',
  reset: 'Ж_Reset___________Я',
  back: 'Ж_Back_________Я',
  close: 'Ж_Close___________Я',
  customPaletteTitle: 'Ж_Customize palette colors_________________________Я',
  customFontsetTitle: 'Ж_Customize font set___________________Я',
  customAppElementsTitle: 'Ж_App elements_____________Я'
  // discardChangesWarningPart1: 'Ж_Customize theme_________________Я:',
  // discardChangesWarningPart2: 'Ж_Changes made to theme________________________Я: ',
  // discardChangesWarningPart3: 'Ж_will be DISCARDED___________________Я.',
  // yesToCustomize: 'Ж_Yes, Customize_______________Я',
  // noToCancel: 'Ж_Cancel_____________Я',
});