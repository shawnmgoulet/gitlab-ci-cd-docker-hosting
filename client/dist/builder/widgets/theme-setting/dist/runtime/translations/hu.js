define({
  chooseTemplate: 'í_Theme___________ő',
  loading: 'í_Loading_______________ő',
  customTheme: 'í_Customize___________________ő',
  reset: 'í_Reset___________ő',
  back: 'í_Back_________ő',
  close: 'í_Close___________ő',
  customPaletteTitle: 'í_Customize palette colors_________________________ő',
  customFontsetTitle: 'í_Customize font set___________________ő',
  customAppElementsTitle: 'í_App elements_____________ő'
  // discardChangesWarningPart1: 'í_Customize theme_________________ő:',
  // discardChangesWarningPart2: 'í_Changes made to theme________________________ő: ',
  // discardChangesWarningPart3: 'í_will be DISCARDED___________________ő.',
  // yesToCustomize: 'í_Yes, Customize_______________ő',
  // noToCancel: 'í_Cancel_____________ő',
});