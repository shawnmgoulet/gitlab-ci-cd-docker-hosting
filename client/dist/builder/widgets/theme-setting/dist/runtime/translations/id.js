define({
  chooseTemplate: 'ng_Theme___________ny',
  loading: 'ng_Loading_______________ny',
  customTheme: 'ng_Customize___________________ny',
  reset: 'ng_Reset___________ny',
  back: 'ng_Back_________ny',
  close: 'ng_Close___________ny',
  customPaletteTitle: 'ng_Customize palette colors_________________________ny',
  customFontsetTitle: 'ng_Customize font set___________________ny',
  customAppElementsTitle: 'ng_App elements_____________ny'
  // discardChangesWarningPart1: 'ng_Customize theme_________________ny:',
  // discardChangesWarningPart2: 'ng_Changes made to theme________________________ny: ',
  // discardChangesWarningPart3: 'ng_will be DISCARDED___________________ny.',
  // yesToCustomize: 'ng_Yes, Customize_______________ny',
  // noToCancel: 'ng_Cancel_____________ny',
});