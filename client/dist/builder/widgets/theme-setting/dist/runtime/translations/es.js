define({
  chooseTemplate: 'á_Theme___________Ó',
  loading: 'á_Loading_______________Ó',
  customTheme: 'á_Customize___________________Ó',
  reset: 'á_Reset___________Ó',
  back: 'á_Back_________Ó',
  close: 'á_Close___________Ó',
  customPaletteTitle: 'á_Customize palette colors_________________________Ó',
  customFontsetTitle: 'á_Customize font set___________________Ó',
  customAppElementsTitle: 'á_App elements_____________Ó'
  // discardChangesWarningPart1: 'á_Customize theme_________________Ó:',
  // discardChangesWarningPart2: 'á_Changes made to theme________________________Ó: ',
  // discardChangesWarningPart3: 'á_will be DISCARDED___________________Ó.',
  // yesToCustomize: 'á_Yes, Customize_______________Ó',
  // noToCancel: 'á_Cancel_____________Ó',
});