define({
  chooseTemplate: 'ø_Theme___________å',
  loading: 'ø_Loading_______________å',
  customTheme: 'ø_Customize___________________å',
  reset: 'ø_Reset___________å',
  back: 'ø_Back_________å',
  close: 'ø_Close___________å',
  customPaletteTitle: 'ø_Customize palette colors_________________________å',
  customFontsetTitle: 'ø_Customize font set___________________å',
  customAppElementsTitle: 'ø_App elements_____________å'
  // discardChangesWarningPart1: 'ø_Customize theme_________________å:',
  // discardChangesWarningPart2: 'ø_Changes made to theme________________________å: ',
  // discardChangesWarningPart3: 'ø_will be DISCARDED___________________å.',
  // yesToCustomize: 'ø_Yes, Customize_______________å',
  // noToCancel: 'ø_Cancel_____________å',
});