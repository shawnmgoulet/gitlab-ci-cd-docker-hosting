/** @jsx jsx */
import { React, classNames, jsx, css } from 'jimu-core';

const prefix = 'jimu-builder';

interface StepperProps extends React.HTMLAttributes<HTMLDivElement> {
  currentStep?: number; 
  onChange?: (evt) => void;
  onNext?: () => void;
  onPrevous?: () => void;
}

interface StepperState {
  currentStep: number
}

export class Stepper extends React.PureComponent<StepperProps, StepperState> {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: 1
    };
  }

  render() {
    const {
      className,
      onChange,
      ...otherProps
    } = this.props;

    const classes = classNames(
      prefix + '-stepper',
      className
    );

    return <div css={getStyle} {...otherProps} className={classes} ></div>;
  }
}

interface StepperPaneProps extends React.HTMLAttributes<HTMLDivElement> {
  active?: boolean;
}

export class StepperPane extends React.PureComponent<StepperPaneProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      active,
      className,
      onChange,
      children,
      ...otherProps
    } = this.props;

    const classes = classNames(
      prefix + '-stepper-pane',
      className,
      {active}
    );

    return <div {...otherProps} className={classes} >
      {active && children}
    </div>;
  }
}

function getStyle() {
  return css`

  `
}