define({
  chooseTemplate: 'Š_Theme___________č',
  loading: 'Š_Loading_______________č',
  customTheme: 'Š_Customize___________________č',
  reset: 'Š_Reset___________č',
  back: 'Š_Back_________č',
  close: 'Š_Close___________č',
  customPaletteTitle: 'Š_Customize palette colors_________________________č',
  customFontsetTitle: 'Š_Customize font set___________________č',
  customAppElementsTitle: 'Š_App elements_____________č'
  // discardChangesWarningPart1: 'Š_Customize theme_________________č:',
  // discardChangesWarningPart2: 'Š_Changes made to theme________________________č: ',
  // discardChangesWarningPart3: 'Š_will be DISCARDED___________________č.',
  // yesToCustomize: 'Š_Yes, Customize_______________č',
  // noToCancel: 'Š_Cancel_____________č',
});