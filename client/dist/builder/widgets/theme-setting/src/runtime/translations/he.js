define({
  chooseTemplate: 'כן_Theme___________ש',
  loading: 'כן_Loading_______________ש',
  customTheme: 'כן_Customize___________________ש',
  reset: 'כן_Reset___________ש',
  back: 'כן_Back_________ש',
  close: 'כן_Close___________ש',
  customPaletteTitle: 'כן_Customize palette colors_________________________ש',
  customFontsetTitle: 'כן_Customize font set___________________ש',
  customAppElementsTitle: 'כן_App elements_____________ש'
  // discardChangesWarningPart1: 'כן_Customize theme_________________ש:',
  // discardChangesWarningPart2: 'כן_Changes made to theme________________________ש: ',
  // discardChangesWarningPart3: 'כן_will be DISCARDED___________________ש.',
  // yesToCustomize: 'כן_Yes, Customize_______________ש',
  // noToCancel: 'כן_Cancel_____________ש',
});