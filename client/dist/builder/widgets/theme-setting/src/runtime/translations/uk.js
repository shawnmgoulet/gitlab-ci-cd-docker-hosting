define({
  chooseTemplate: 'ґ_Theme___________Ї',
  loading: 'ґ_Loading_______________Ї',
  customTheme: 'ґ_Customize___________________Ї',
  reset: 'ґ_Reset___________Ї',
  back: 'ґ_Back_________Ї',
  close: 'ґ_Close___________Ї',
  customPaletteTitle: 'ґ_Customize palette colors_________________________Ї',
  customFontsetTitle: 'ґ_Customize font set___________________Ї',
  customAppElementsTitle: 'ґ_App elements_____________Ї'
  // discardChangesWarningPart1: 'ґ_Customize theme_________________Ї:',
  // discardChangesWarningPart2: 'ґ_Changes made to theme________________________Ї: ',
  // discardChangesWarningPart3: 'ґ_will be DISCARDED___________________Ї.',
  // yesToCustomize: 'ґ_Yes, Customize_______________Ї',
  // noToCancel: 'ґ_Cancel_____________Ї',
});