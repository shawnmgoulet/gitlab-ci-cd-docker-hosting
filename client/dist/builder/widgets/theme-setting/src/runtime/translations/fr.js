define({
  chooseTemplate: 'æ_Theme___________Â',
  loading: 'æ_Loading_______________Â',
  customTheme: 'æ_Customize___________________Â',
  reset: 'æ_Reset___________Â',
  back: 'æ_Back_________Â',
  close: 'æ_Close___________Â',
  customPaletteTitle: 'æ_Customize palette colors_________________________Â',
  customFontsetTitle: 'æ_Customize font set___________________Â',
  customAppElementsTitle: 'æ_App elements_____________Â'
  // discardChangesWarningPart1: 'æ_Customize theme_________________Â:',
  // discardChangesWarningPart2: 'æ_Changes made to theme________________________Â: ',
  // discardChangesWarningPart3: 'æ_will be DISCARDED___________________Â.',
  // yesToCustomize: 'æ_Yes, Customize_______________Â',
  // noToCancel: 'æ_Cancel_____________Â',
});