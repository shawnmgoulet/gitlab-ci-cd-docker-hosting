define({
  chooseTemplate: 'ł_Theme___________ą',
  loading: 'ł_Loading_______________ą',
  customTheme: 'ł_Customize___________________ą',
  reset: 'ł_Reset___________ą',
  back: 'ł_Back_________ą',
  close: 'ł_Close___________ą',
  customPaletteTitle: 'ł_Customize palette colors_________________________ą',
  customFontsetTitle: 'ł_Customize font set___________________ą',
  customAppElementsTitle: 'ł_App elements_____________ą'
  // discardChangesWarningPart1: 'ł_Customize theme_________________ą:',
  // discardChangesWarningPart2: 'ł_Changes made to theme________________________ą: ',
  // discardChangesWarningPart3: 'ł_will be DISCARDED___________________ą.',
  // yesToCustomize: 'ł_Yes, Customize_______________ą',
  // noToCancel: 'ł_Cancel_____________ą',
});