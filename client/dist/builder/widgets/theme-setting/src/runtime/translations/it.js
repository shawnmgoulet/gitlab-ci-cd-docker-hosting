define({
  chooseTemplate: 'é_Theme___________È',
  loading: 'é_Loading_______________È',
  customTheme: 'é_Customize___________________È',
  reset: 'é_Reset___________È',
  back: 'é_Back_________È',
  close: 'é_Close___________È',
  customPaletteTitle: 'é_Customize palette colors_________________________È',
  customFontsetTitle: 'é_Customize font set___________________È',
  customAppElementsTitle: 'é_App elements_____________È'
  // discardChangesWarningPart1: 'é_Customize theme_________________È:',
  // discardChangesWarningPart2: 'é_Changes made to theme________________________È: ',
  // discardChangesWarningPart3: 'é_will be DISCARDED___________________È.',
  // yesToCustomize: 'é_Yes, Customize_______________È',
  // noToCancel: 'é_Cancel_____________È',
});