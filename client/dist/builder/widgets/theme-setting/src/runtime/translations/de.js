define({
  chooseTemplate: 'ä_Theme___________Ü',
  loading: 'ä_Loading_______________Ü',
  customTheme: 'ä_Customize___________________Ü',
  reset: 'ä_Reset___________Ü',
  back: 'ä_Back_________Ü',
  close: 'ä_Close___________Ü',
  customPaletteTitle: 'ä_Customize palette colors_________________________Ü',
  customFontsetTitle: 'ä_Customize font set___________________Ü',
  customAppElementsTitle: 'ä_App elements_____________Ü'
  // discardChangesWarningPart1: 'ä_Customize theme_________________Ü:',
  // discardChangesWarningPart2: 'ä_Changes made to theme________________________Ü: ',
  // discardChangesWarningPart3: 'ä_will be DISCARDED___________________Ü.',
  // yesToCustomize: 'ä_Yes, Customize_______________Ü',
  // noToCancel: 'ä_Cancel_____________Ü',
});