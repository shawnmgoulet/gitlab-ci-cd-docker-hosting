define({
  chooseTemplate: 'आ_Theme___________ज',
  loading: 'आ_Loading_______________ज',
  customTheme: 'आ_Customize___________________ज',
  reset: 'आ_Reset___________ज',
  back: 'आ_Back_________ज',
  close: 'आ_Close___________ज',
  customPaletteTitle: 'आ_Customize palette colors_________________________ज',
  customFontsetTitle: 'आ_Customize font set___________________ज',
  customAppElementsTitle: 'आ_App elements_____________ज'
  // discardChangesWarningPart1: 'आ_Customize theme_________________ज:',
  // discardChangesWarningPart2: 'आ_Changes made to theme________________________ज: ',
  // discardChangesWarningPart3: 'आ_will be DISCARDED___________________ज.',
  // yesToCustomize: 'आ_Yes, Customize_______________ज',
  // noToCancel: 'आ_Cancel_____________ज',
});