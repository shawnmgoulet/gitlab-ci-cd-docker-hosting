define({
  chooseTemplate: 'ķ_Theme___________ū',
  loading: 'ķ_Loading_______________ū',
  customTheme: 'ķ_Customize___________________ū',
  reset: 'ķ_Reset___________ū',
  back: 'ķ_Back_________ū',
  close: 'ķ_Close___________ū',
  customPaletteTitle: 'ķ_Customize palette colors_________________________ū',
  customFontsetTitle: 'ķ_Customize font set___________________ū',
  customAppElementsTitle: 'ķ_App elements_____________ū'
  // discardChangesWarningPart1: 'ķ_Customize theme_________________ū:',
  // discardChangesWarningPart2: 'ķ_Changes made to theme________________________ū: ',
  // discardChangesWarningPart3: 'ķ_will be DISCARDED___________________ū.',
  // yesToCustomize: 'ķ_Yes, Customize_______________ū',
  // noToCancel: 'ķ_Cancel_____________ū',
});