export default {
  chooseTemplate: 'å_Theme___________ø',
  loading: 'å_Loading_______________ø',
  customTheme: 'å_Customize___________________ø',
  reset: 'å_Reset___________ø',
  back: 'å_Back_________ø',
  close: 'å_Close___________ø',
  customPaletteTitle: 'å_Customize palette colors_________________________ø',
  customFontsetTitle: 'å_Customize font set___________________ø',
  customAppElementsTitle: 'å_App elements_____________ø'
  // discardChangesWarningPart1: 'å_Customize theme_________________ø:',
  // discardChangesWarningPart2: 'å_Changes made to theme________________________ø: ',
  // discardChangesWarningPart3: 'å_will be DISCARDED___________________ø.',
  // yesToCustomize: 'å_Yes, Customize_______________ø',
  // noToCancel: 'å_Cancel_____________ø',
}