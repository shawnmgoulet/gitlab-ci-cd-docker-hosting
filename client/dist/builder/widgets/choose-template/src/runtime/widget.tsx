/** @jsx jsx */
import { BaseWidget, AllWidgetProps, jimuHistory, urlUtils, css, jsx, ThemeVariables, SessionManager, polished } from 'jimu-core';
import { IMConfig } from '../config';
import { builderActions } from 'jimu-for-builder';
import { appServices, templatesServices } from 'jimu-for-builder/service';
import Template from './template';
import { Icon, Input, Navbar, NavItem, Nav, NavLink } from 'jimu-ui';
import { TemplateInfo } from './template';
import {ISearchOptions} from '@esri/arcgis-rest-portal';
import defaultMessages from './translations/default';
const searchIcon = require('jimu-ui/lib/icons/search-16.svg');
const closeIcon = require('jimu-ui/lib/icons/close-24.svg');


interface PreloadProps {
  // templates: TemplateInfo[];
}

interface State {
  loading: boolean;
  searchText: string;
  templatesFormExperiences: TemplateInfo[];
  defaultTemplate: TemplateInfo[];
  sharedTemplate: TemplateInfo[];
  templates: TemplateInfo[];
  accessType: AccessType;
}
enum AccessType {
  DEFAULT = 'Default',
  MY = 'My',
  SHARED = 'Shared'
}

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & PreloadProps & { theme: ThemeVariables }, State>{

  static scrollTop: number = 0;
  contentNode: HTMLDivElement;
  appListContainer: HTMLDivElement;

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchText: '',
      templatesFormExperiences: [],
      accessType: AccessType.DEFAULT,
      defaultTemplate: [],
      sharedTemplate: [],
      templates: [],
    };
  }

  componentDidMount(){
    this.getDefaultTemplate();
    this.refreshAction();
    if (this.contentNode) {
      this.contentNode.addEventListener('scroll', this.onScrollHandle.bind(this));
      // this.contentNode.scrollTop = Widget.scrollTop;
    }
  }
  
  // componentWillUnmount() {
  //   if (this.contentNode) {
  //     this.contentNode.removeEventListener('scroll', this.onScrollHandle.bind(this));
  //     // Widget.scrollTop = this.contentNode.scrollTop;
  //   }
  // }

  getDefaultTemplate = () => {
    fetch(`${urlUtils.getAbsoluteRootUrl()}templates/templates-info.json`)
      .then(res => res.json()).then((templates: any[]) => {
        this.initTemplateData(templates, true);
      })
  }

  initTemplateData = (templates: any[], isDefault: boolean = false) => {

    let defaultTemplate = templates.map(t => {
      return {
        isExperiencesTemplate: false,
        name: t.name,
        title: t.label,
        image: {
          src: '../' + t.thumbnail
        },
        description: t.description
      };
    })
    this.setState({
      defaultTemplate: defaultTemplate,
    });

    if(isDefault){
      this.setState({
        templates: defaultTemplate
      });
    }
  }

  nls = (id: string) => {
    return this.props.intl ? this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  createApp = (templateName: string) => {
    this.setState({ loading: true });

    let title = this.nls('untitledExperience');
    templatesServices.createAppByDefaultTemplate(title, templateName).then(id => {
      this.setState({ loading: false });
      this.props.dispatch(builderActions.refreshAppListAction(true));
      jimuHistory.browserHistory.push(`${urlUtils.getFixedRootPath()}builder?id=${id}`);
    }, err => {
      this.setState({ loading: false });
      console.error(err);
    }).catch(err => {
      this.setState({ loading: false });
      console.error(err);
    })
  }

  onCreateClick = (templateName: string) => {
    this.selectTemplate(templateName);
    this.createApp(templateName);
  }

  selectTemplate = (templateName: string) => {
    this.props.dispatch(builderActions.selectTemplate(templateName));
  }

  _matchearchText = (text) => {
    const { searchText } = this.state;
    if (!searchText || !text) {
      return true;
    }
    return text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
  }

  onScrollHandle(event) {
    const clientHeight = event.target.clientHeight
    const scrollHeight = event.target.scrollHeight
    const scrollTop    = event.target.scrollTop
    const isBottom     = (clientHeight + scrollTop === scrollHeight)
    if(!this.state.loading && isBottom){
      this.refreshAction(true);
    }
  }

  crateAppByTemplate = (appId: string) => {
    this.setState({ loading: true });

    templatesServices.crateAppByTemplate(appId).then(newAppId => {
      if (newAppId) {
        let to = window.jimuConfig.mountPath + 'builder' + '?id=' + newAppId;
        window.location.href = to;
      }
      this.setState({ loading: false });
    }, () => {
      this.setState({ loading: false });
    })
  }
  
  filterTemplateChange = (accessType) => {
    this.setState({
      templates: [],
      templatesFormExperiences: [],
      accessType: accessType,
    }, () => {
      this.configTempates();
    });
  }

  configTempates = () => {
    const {accessType, defaultTemplate} = this.state;
    if(accessType == AccessType.DEFAULT){
      this.setState({
        templates: defaultTemplate
      });
    }else {
      this.refreshAction();
    }
    
  }

  refreshAction = (isMore: boolean = false) => {
    let requestOption = this.getRequestOption(isMore);
    this.refresh(requestOption as any, isMore);
  }

  refresh(requestOption?: ISearchOptions, isMore: boolean = false){
    if (!requestOption) {
      return;
    }
    let searchPromise = null;
    this.setState({loading: true});
    searchPromise = this.appServiceSearchApp(requestOption);

    searchPromise.then(apps => {
      if (!searchPromise) {
        this.setState({loading: false});
        return;
      }
      
      let newApps = apps.map(t => {
        let listItem = {
          isExperiencesTemplate: true,
          name: t.name,
          title: t.title,
          id: t.id,
          image: {
            src: this.thumbnail(t.thumbnail, t.id)
          },
          description: t.description
        };
        return listItem;
      });

      let templates = this.state.templatesFormExperiences;
      if(isMore){
        templates = templates.concat(newApps);
      }else{
        templates = newApps;
      }
      
      this.setState({
        templatesFormExperiences: templates,
        templates: templates,
        loading: false
      });

    }, () => {
      this.setState({loading: false});
    })
  }


  getRequestOption = (isMore: boolean = false) => {
    const accessType = this.state.accessType; 
    if(accessType == AccessType.DEFAULT) return null;

    let requestOption = {
      start     : 1,
      q         : 'type: "Web Experience Template"',
      sortField : 'modified',
      sortOrder : 'desc',
      num       : 30,
    };

    const session = SessionManager.getInstance().getMainSession();
    if (session) {
      switch (accessType) {
        case AccessType.MY:
          requestOption.q =  `type: "Web Experience Template" AND owner:${session.username}`;
          break;
        case AccessType.SHARED:
          // requestOption.q = `type: "Web Experience Template" AND owner: exb_admin`;
          requestOption.q = `type: "Web Experience" AND owner:exb_admin`;
          break;
      }
      
      let containerWidth  = this.contentNode.clientWidth;
      let containerHeight = this.contentNode.clientHeight;
      let pageNum         = Math.ceil(containerHeight / 260) * Math.ceil((containerWidth / 238));
      if(isMore){
        let pageStart       = this.state.templatesFormExperiences.length + 1;
        requestOption.num   = pageNum
        requestOption.start = pageStart
      }else{
        requestOption.num   = pageNum - this.state.templates.length;
      }
    } 
    return requestOption;
  }
  
  thumbnail = (thumbnail: string, id: string) => {
    const {portalUrl}  = this.props;
    let itemThumbnail
    if (thumbnail) {
      itemThumbnail = portalUrl + '/sharing/rest/content/items/' + id + '/info/' 
        + thumbnail + '?token=' + SessionManager.getInstance().getSessionByUrl(portalUrl).token;
    } else {
      itemThumbnail =  window.location.protocol + '/../site/widgets/app-list/./dist/runtime/assets/defaultthumb.png';
    };

    return itemThumbnail;
  }

  appServiceSearchApp = (requestOption): Promise<any> => {
    return appServices.searchTemplates(requestOption);
  }

  getStyle = () => {
    const { theme } = this.props;
    const secondary = theme ? theme.colors.palette.light[800] : '';
    return css`
      height: 100%;
      .header-bar {
        width: 100%;
        height: ${polished.rem(60)};
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0 ${polished.rem(30)};
        font-size: 20px;
        border-bottom: 1px solid ${secondary};
        color:${theme.colors.palette.dark[600]};
        font-weight:500;
        background:${theme.colors.palette.light[400]};

        .jimu-icon {
          cursor: pointer;
        }
      }
      .homescreen {
        height: calc(100% - 80px);
        width:810px;
        margin: 0 auto;
      }
      .header {
        width: 100%;
        padding: ${polished.rem(32)} ${polished.rem(24)} ${polished.rem(20)} ${polished.rem(16)};
        align-items: flex-end;
        .banner {
          position: relative;
  
          .jimu-icon {
              color: ${theme.colors.palette.dark[400]};
              position: absolute;
              left:${polished.rem(6)};
              top: ${polished.rem(8)};
              cursor: pointer;
          }
  
          .searchbox {
              padding-left:${polished.rem(28)};
              font-size:${polished.rem(14)};
              height: ${polished.rem(32)};
              width:${polished.rem(400)};
              cursor: text;
          }
        }


        .filterbar-input {
          width:${polished.rem(160)};
          margin-right:${polished.rem(20)};
        }
      }

      .section {
        display: flex;
        flex-wrap: wrap;
        height: calc(100% - 200px);
        overflow-x: hidden;
        overflow-y: auto;
        align-content: flex-start;
      }
      .template-con {
        flex-wrap:wrap;
      }
      .template-title {
        font-size: ${polished.rem(14)};
        color:${theme.colors.palette.dark[400]};
        .tap-link a.active {
          font-weight:500;
        }
        .header-nav-bar-con {
          padding:0;
        }
        a {
          padding-left:0;
          padding-right:0;
          width:auto;
        }
        .nav-fill .nav-item .nav-link {
          width: auto;
        }
        .tap-margin-r {
          margin-right:${polished.rem(40)};
        }
      }
      @media only screen and (min-width: 1280px) {
        .homescreen {
          width:1090px;
        }
        .header{
          padding: ${polished.rem(40)} ${polished.rem(44)} ${polished.rem(20)} ${polished.rem(16)};
        }
      }
      @media only screen and (min-width: 1400px) {
        .homescreen {
            width:1360px;
        }
      }
      @media only screen and (min-width: 1680px) {
        .homescreen {
            width:1630px;
        }
      }
    `;
  }

  close = () => {
    if (jimuHistory.browserHistory.length > 2) {
      jimuHistory.browserHistory.goBack();
    } else {
      window.location.href = urlUtils.getFixedRootPath();
    }
  }

  isTemplateDisabled = (item: TemplateInfo) => {
    return false;
  }

  render() {
    const {templates, searchText } = this.state;
    const {theme, intl } = this.props;
    // const isDevEdition = window.isDevEdition;

    return (
      <div css={this.getStyle()} className="widget-choose-template bg-light-300" >
        <div className="header-bar">
          {this.nls('_widgetLabel')}
          <div onClick={this.close}><Icon size={20} icon={closeIcon}></Icon></div>
        </div>

        <div className="homescreen">
          <div className="header d-flex">
            <div className="template-title flex-grow-1">
              <Navbar className="header-nav-bar-con" border={false} color="false"  light>
                <Nav underline navbar card justified={true} fill={true}>
                  <NavItem className="tap-link"  onClick={() => {this.filterTemplateChange(AccessType.DEFAULT)}}>
                    <NavLink className="tap-margin-r" active={this.state.accessType == AccessType.DEFAULT}>{this.nls('default')}</NavLink>
                  </NavItem>
                  {false && <NavItem className="tap-link"  onClick={() => {this.filterTemplateChange(AccessType.MY)}}>
                    <NavLink className="tap-margin-r" active={this.state.accessType == AccessType.MY}>{this.nls('my')}</NavLink>
                  </NavItem>}
                  <NavItem className="tap-link" onClick={() => {this.filterTemplateChange(AccessType.SHARED)}}>
                    <NavLink  active={this.state.accessType == AccessType.SHARED}>{this.nls('shared')}</NavLink>
                  </NavItem>
                </Nav>
              </Navbar>
              {/* {this.nls('telplateListTitle')} */}
            </div>

            <div className="d-flex">
              <div className="banner d-flex">
                <Icon size={16} icon={searchIcon} />
                <Input className="pt-2 pb-2 searchbox" placeholder={this.nls('searchPlaceholder')} value={searchText}
                  onChange={e => { this.setState({ searchText: e.target.value }) }}>
                </Input>
              </div>
            </div>
          </div>

          <div className="section" ref={ node => this.contentNode = node }>
            <div className="d-flex template-con " ref={ref => {this.appListContainer = ref; }} >
              {templates && templates.map((item, index) =>
                <Template key={index} theme={theme} show={this._matchearchText(item.title)} info={item} intl={intl}
                  disabled={this.isTemplateDisabled(item)} onCreateClick={this.onCreateClick} crateAppByTemplate={this.crateAppByTemplate}/>
              )}
            </div>
          </div>
          {this.state.loading && <div style={{ position: 'absolute', left: '50%', top: '50%' }} className="jimu-primary-loading"></div>}
        </div>
      </div>
    );
  }
}
