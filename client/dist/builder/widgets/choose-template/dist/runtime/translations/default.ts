export default {
  _widgetLabel: 'Create a new experience',
  untitledExperience: 'Untitled experience',
  telplateListTitle: 'Templates',
  searchPlaceholder: 'Search',
  create: 'Create',
  default: 'Default',
  my: 'My',
  shared: 'Shared',
  preview: 'Preview'
}
