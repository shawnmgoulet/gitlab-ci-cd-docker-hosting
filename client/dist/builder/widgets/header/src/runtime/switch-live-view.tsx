/** @jsx jsx */
import {React, ThemeVariables, SerializedStyles, IntlShape, polished, AppMode, lodash,
  css, jsx, classNames as classnames, IMState, themeUtils, ReactRedux} from 'jimu-core';
import {builderAppSync, getAppConfigAction} from 'jimu-for-builder';
import defaultMessages from './translations/default';

interface Props {
  theme?: ThemeVariables;
  intl: IntlShape;
}
interface ExtraProps {
  appMode: AppMode;
  lockLayout: boolean;
}

class _SwitchLiveViewComponent extends React.PureComponent<Props & ExtraProps, {}> {

  appMode: string;
  lockLayout: string;

  constructor(props) {
    super(props);

    this.appMode = this.props.intl.formatMessage({id: 'appMode', defaultMessage: defaultMessages.appMode});
    this.lockLayout = this.props.intl.formatMessage({id: 'lockLayout', defaultMessage: defaultMessages.lockLayout});
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .live-view-container {
        cursor: pointer;
        background-color: ${theme.colors.primary};
        border-radius: 2px !important;
        color: ${theme.colors.black};
        padding-right: ${polished.rem(8)};
        padding-left: ${polished.rem(8)};
        height: ${polished.rem(26)};
        border:1px solid ${theme.colors.primary};
      }

      .edit-view-container {
        cursor: pointer;
        border:1px solid ${theme.colors.palette.light[800]};
        border-radius: 2px !important;
        color: ${theme.colors.palette.dark[600]};
        padding-right: ${polished.rem(8)};
        padding-left: ${polished.rem(8)};
        height: ${polished.rem(26)};

        &:hover {
          color: ${theme.colors.black};

          .edit-view-icon {
            border: 1px solid ${theme.colors.black};
          }
        }
      }

      .live-view-icon {
        width: 7px;
        height: 7px;
        border-radius: 50%;
        background: ${theme.colors.black};
      }

      .edit-view-icon {
        width: 7px;
        height: 7px;
        border: 1px solid ${theme.colors.palette.dark[800]};
        border-radius: 50%;
      }
    `;
  }

  onAppModeChange = () => {
    if(!this.props){
      return;
    }

    if(this.props.appMode === AppMode.Run){
      builderAppSync.publishAppModeChangeToApp(AppMode.Design);
    }else{
      builderAppSync.publishAppModeChangeToApp(AppMode.Run);
    }
  }

  onLockLayoutChange = () => {
    const appConfigAction = getAppConfigAction();
    appConfigAction.setLockLayout(!this.props.lockLayout).exec();
  }

  render() {
    const {appMode, lockLayout} = this.props;
    let isAppRun = appMode === AppMode.Run;

    return <div className="d-flex" css={this.getStyle(this.props.theme)}>
      {!isAppRun && <div className={classnames('d-flex align-items-center', {
        'live-view-container': lockLayout,
        'edit-view-container': !lockLayout
      })} onClick={this.onLockLayoutChange} style={{whiteSpace: 'nowrap'}}>
        <div className={classnames('mr-2', {
          'live-view-icon': lockLayout,
          'edit-view-icon': !lockLayout,
        })}></div>
        <div className="d-flex align-items-center border-left-0 app-toolbar-mode">
          <span>{this.lockLayout}</span>
        </div>
      </div>}
      <div className="liveview-gap"></div>
      <div className={classnames('d-flex align-items-center', {
        'live-view-container': isAppRun,
        'edit-view-container': !isAppRun
      })} onClick={this.onAppModeChange} title={'Shift+Alt+X'} style={{whiteSpace: 'nowrap'}}>
        <div className={classnames('mr-2', {
          'live-view-icon': isAppRun,
          'edit-view-icon': !isAppRun,
        })}></div>
        <div className="d-flex align-items-center border-left-0 app-toolbar-mode">
          <span>{this.appMode}</span>
        </div>
      </div>
    </div>
  }
}

const _SwitchLiveViewComponentInner = themeUtils.withTheme(_SwitchLiveViewComponent);

const mapExtraStateProps = (state: IMState): ExtraProps => {
  return {
    appMode: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.appMode,
    lockLayout: lodash.getValue(state, 'appStateInBuilder.appConfig.forBuilderAttributes.lockLayout', false),
  };
}

export default ReactRedux.connect<ExtraProps, {}, Props>(mapExtraStateProps)(_SwitchLiveViewComponentInner);