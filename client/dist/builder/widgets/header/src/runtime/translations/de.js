define({
  createNew: 'ä_Create New_____________________Ü',
  newExperience: 'ä_New experience_______________Ü',
  signOut: 'ä_Sign out_________________Ü',
  signIn: 'ä_Sign in_______________Ü',
  undo: 'ä_Undo_________Ü',
  redo: 'ä_Redo_________Ü',
  save: 'ä_Save_________Ü',
  saving: 'ä_Saving_____________Ü',
  saved: 'ä_Saved___________Ü',
  saveError: 'ä_Saving error_____________Ü',
  saveSuccess: 'ä_Saved successfully____________________Ü!',
  preview: 'ä_Preview_______________Ü',
  publish: 'ä_Publish_______________Ü',
  publishing: 'ä_Publishing_____________________Ü',
  published: 'ä_Published___________________Ü',
  publishError: 'ä_Publishing error_________________Ü',
  publishSuccess: 'ä_Published successfully________________________Ü!',
  publishTo: 'ä_Publish to_____________________Ü',
  publishOptions: 'ä_Publish options________________Ü',
  copySuccess: 'ä_Copied successfully_____________________Ü!',
  changeShareSettings: 'ä_Change share settings______________________Ü',
  viewPublishedItem: 'ä_View published item____________________Ü',
  copyPublishedItemLink: 'ä_Copy published item link_________________________Ü',
  headerLeave: 'ä_Leave___________Ü',
  headerLeaveSite: 'ä_Leave site____________Ü?',
  headerLeaveDescription: 'ä_Changes you made may not be saved__________________Ü.',
  editPageForLargeScreen: 'ä_Edit your page for large screen devices_____________________Ü',
  editPageForMediumScreen: 'ä_Edit your page for medium screen devices_____________________Ü',
  editPageForSmallScreen: 'ä_Edit your page for small screen devices_____________________Ü',
  appMode: 'ä_Live view___________________Ü',
  generateTemplate: 'ä_Generate template__________________Ü',
  moreOptionsForTool: 'ä_More_________Ü',
  moreTools: 'ä_More tools_____________________Ü',
  access: 'ä_Access_____________Ü',
  generateTemplateSuccess: 'ä_Generated template successfully_________________Ü',
  generateTemplateError: 'ä_Generating error_________________Ü',
  lockLayout: 'ä_Lock layout____________Ü',
});