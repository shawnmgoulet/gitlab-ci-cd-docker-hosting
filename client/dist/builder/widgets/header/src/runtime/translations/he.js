define({
  createNew: 'כן_Create New_____________________ש',
  newExperience: 'כן_New experience_______________ש',
  signOut: 'כן_Sign out_________________ש',
  signIn: 'כן_Sign in_______________ש',
  undo: 'כן_Undo_________ש',
  redo: 'כן_Redo_________ש',
  save: 'כן_Save_________ש',
  saving: 'כן_Saving_____________ש',
  saved: 'כן_Saved___________ש',
  saveError: 'כן_Saving error_____________ש',
  saveSuccess: 'כן_Saved successfully____________________ש!',
  preview: 'כן_Preview_______________ש',
  publish: 'כן_Publish_______________ש',
  publishing: 'כן_Publishing_____________________ש',
  published: 'כן_Published___________________ש',
  publishError: 'כן_Publishing error_________________ש',
  publishSuccess: 'כן_Published successfully________________________ש!',
  publishTo: 'כן_Publish to_____________________ש',
  publishOptions: 'כן_Publish options________________ש',
  copySuccess: 'כן_Copied successfully_____________________ש!',
  changeShareSettings: 'כן_Change share settings______________________ש',
  viewPublishedItem: 'כן_View published item____________________ש',
  copyPublishedItemLink: 'כן_Copy published item link_________________________ש',
  headerLeave: 'כן_Leave___________ש',
  headerLeaveSite: 'כן_Leave site____________ש?',
  headerLeaveDescription: 'כן_Changes you made may not be saved__________________ש.',
  editPageForLargeScreen: 'כן_Edit your page for large screen devices_____________________ש',
  editPageForMediumScreen: 'כן_Edit your page for medium screen devices_____________________ש',
  editPageForSmallScreen: 'כן_Edit your page for small screen devices_____________________ש',
  appMode: 'כן_Live view___________________ש',
  generateTemplate: 'כן_Generate template__________________ש',
  moreOptionsForTool: 'כן_More_________ש',
  moreTools: 'כן_More tools_____________________ש',
  access: 'כן_Access_____________ש',
  generateTemplateSuccess: 'כן_Generated template successfully_________________ש',
  generateTemplateError: 'כן_Generating error_________________ש',
  lockLayout: 'כן_Lock layout____________ש',
});