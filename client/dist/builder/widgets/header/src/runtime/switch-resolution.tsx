/** @jsx jsx */
import {React, ThemeVariables, SerializedStyles, BrowserSizeMode, PageMode, CONSTANTS, utils as jimuUtils, lodash, Size,
  css, jsx, IMState, themeUtils, ReactRedux, polished} from 'jimu-core';
import {getAppConfigAction} from 'jimu-for-builder';
import {Icon, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'jimu-ui';

let IconArrowDown = require('jimu-ui/lib/icons/arrow-down-header.svg');

interface ExtraProps {
  theme?: ThemeVariables;

  browserSizeMode?: BrowserSizeMode;
  viewportSize?: Size;
  pageHeightInBuilder?: number;
  pageMode?: PageMode;
}

interface State {
  isResolutionChooseShow: boolean;
}

class _SwitchResolutionComponent extends React.PureComponent<ExtraProps, State> {

  constructor(props) {
    super(props);

    this.state = {
      isResolutionChooseShow: false
    }
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .switch-resolution-toggle {
        width: ${polished.rem(110)};
        font-weight:500;
        color:${this.props.theme.colors.palette.dark[400]};
      }
      .dropdown-toggle-content svg {
        margin-left:${polished.rem(6)};
        vertical-align:center;
      }
      .resolution-choose {
      }

      .no-user-select {
        -o-user-select: none;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        -khtml-user-select :none;
        user-select: none;
      }
    `;
  }

  getDropdownStyle  (theme: ThemeVariables): SerializedStyles {
    return css`
      .dropdown-menu--inner {
        background: ${theme.colors.palette.light[500]};
        border: 1px solid ${theme.colors.palette.light[300]};;
        box-shadow: 0 0 10px 2px ${polished.rgba(theme.colors.white, .2)};
        border-radius: 2px;
        border-radius: 2px;
        padding-top:${polished.rem(10)};
        padding-bottom:${polished.rem(10)};
        width: ${polished.rem(100)};
        button {
          padding-left:${polished.rem(12)};
        }
        .dropdown-item:hover, .dropdown-item:focus {
          background:${theme.colors.primary};
        }
      }
    `
  }

  onToggleResolutionChoose = () => {
    this.setState({
      isResolutionChooseShow: !this.state.isResolutionChooseShow
    });
  }

  setViewportSize = (size) => {
    const appConfigAction = getAppConfigAction();
    appConfigAction.setViewportSize(this.props.browserSizeMode, size).exec();

    this.setState({
      isResolutionChooseShow: false
    });
  }

  render() {
    const {browserSizeMode, viewportSize, pageHeightInBuilder, pageMode} = this.props;

    const heightOfWorkArea = viewportSize ? (pageHeightInBuilder || viewportSize.height) : '';
    const viewportLabel = viewportSize ?
      `${viewportSize.width} × ${pageMode !== PageMode.FitWindow ? heightOfWorkArea : viewportSize.height}` : '';
    let sizemodeResolutions = CONSTANTS.SCREEN_RESOLUTIONS[browserSizeMode] || [];
    if (pageMode !== PageMode.FitWindow) {
      // remove the items with same width
      const widthMap = {};
      const uniqueResolutions = [];
      sizemodeResolutions.forEach((size) => {
        if (widthMap[size.width] == null) {
          uniqueResolutions.push(size);
          widthMap[size.width] = size;
        }
      });
      sizemodeResolutions = uniqueResolutions;
    }

    return <div css={this.getStyle(this.props.theme)}><Dropdown size="sm" toggle={this.onToggleResolutionChoose} 
      isOpen={this.state.isResolutionChooseShow} className="resolution-choose">
      <DropdownToggle size="sm" type="tertiary" className="switch-resolution-toggle">
        {viewportLabel}
        <Icon classNames="dropdown-icon" icon={IconArrowDown} width={10} height={10}/>
      </DropdownToggle>
      <DropdownMenu appendTo="body" css={this.getDropdownStyle(this.props.theme)}>
        {sizemodeResolutions.map((size, index) => {
          return <DropdownItem key={index} className="no-user-select"
            onClick={() => {this.setViewportSize(size)}}>
            {`${size.width} × ${pageMode !== PageMode.FitWindow ? heightOfWorkArea : size.height}`}
          </DropdownItem>;
        })}
      </DropdownMenu>
    </Dropdown>
   </div>
  }
}

const __SwitchResolutionComponentInner = themeUtils.withTheme(_SwitchResolutionComponent);

const mapExtraStateProps = (state: IMState): ExtraProps => {
  let pageHeight: number;
  const browserSizeMode = state.appStateInBuilder && state.appStateInBuilder.browserSizeMode;
  let viewportSize;
  if (state.appStateInBuilder) {
    viewportSize = jimuUtils.findViewportSize(state.appStateInBuilder.appConfig, browserSizeMode);
  }
  const currentPageId = state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentPageId;
  let pageMode;
  if (currentPageId) {
    pageMode = lodash.getValue(state, `appStateInBuilder.appConfig.pages.${currentPageId}.mode`);
    const sizeModePageHeight = lodash.getValue(state, `appStateInBuilder.appConfig.pages.${currentPageId}.heightInBuilder`);
    if (sizeModePageHeight && sizeModePageHeight[browserSizeMode]) {
      pageHeight = sizeModePageHeight[browserSizeMode];
    }
  }
  return {
    viewportSize,
    pageMode,
    pageHeightInBuilder: pageHeight,
    browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
  };
}

export default ReactRedux.connect<{}, {}, ExtraProps>(mapExtraStateProps)(__SwitchResolutionComponentInner);