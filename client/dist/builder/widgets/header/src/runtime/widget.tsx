/** @jsx jsx */
import {BaseWidget, AllWidgetProps, React, css, jsx, SessionManager,
  IMState, polished, ReactResizeDetector, ThemeVariables, SerializedStyles} from 'jimu-core';
import {builderActions} from 'jimu-for-builder';
import {appServices} from 'jimu-for-builder/service';
import {IMConfig} from '../config';
import {Icon, Image, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'jimu-ui';
import ToolList from './tool-list';
import SwitchLiveView from './switch-live-view';
import SwitchDevice from './switch-device';
import SwitchResolution from './switch-resolution';
import defaultMessages from './translations/default';

let IconAccount = require('./assets/defaultUser.svg');

interface ExtraProps {
  currentPageId: string;
}

interface State{
  titleText: string;
  titleLength: number;
  accountPopoverOpen: boolean;
}

export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{

  titleTextInput = React.createRef<HTMLInputElement>();
  // span is designed for textInput auto resize function
  spanTextInput = React.createRef<HTMLSpanElement>();
  isToolListSaved: boolean = true;

  constructor(props) {
    super(props);

    this.state = {
      titleText: '',
      titleLength: 0,
      accountPopoverOpen: false
    };
  }

  getStyle() {
    // with font_size_root theme variable can't be get, so define font_size_root temporarily
    let theme = this.props.theme;
    let font_size_root = 14;
    const {
      colors
    } = theme;

    return css`
      .widget-builder-header {
        background-color: ${colors.palette.light[500]};
        border: 1px solid ${colors.palette.light[800]};
        padding-left: ${polished.rem(12)};

        .header-logo {
          .header-logo-item {
            height: ${polished.rem(26)};
            width: ${polished.rem(26)};
          }
          img {
            margin-right:${polished.rem(5)};
          }
          input {
            padding-left:${polished.rem(5)};
            padding-right:${polished.rem(5)};
          }
        }

        .header-title-maxwidth-screen {
          max-width: ${polished.rem(220)};
        }

        .header-title {
          top: 0;
          bottom: 0;
          border: 1px solid ${colors.palette.light[500]};
          input {
            background-color: transparent;
            &:focus {
              background-color: ${theme.colors.white};
            }
          }

          &:hover {
            border: 1px solid ${colors.palette.secondary[600]};
          }
        }

        .header-title-input {
          border: none;
          text-align: center;
          min-width: ${50 / font_size_root}rem;
          cursor: text;
          color: ${colors.palette.dark[800]};
        }

        .header-account {
          float: left;
          color: ${theme.colors.black};
          padding-right: ${polished.rem(16)};
          padding-left: ${polished.rem(20)};

          div {
            background-color: initial;
          }

          &:hover {
            // background-color: ${theme.colors.white};
          }
        }

        .header-login {
          cursor: pointer;
          fill: ${theme.colors.black};
        }

        .header-login-username {
          color: ${theme.colors.black};
          margin-left: 5px;
          font-size: 14px;
        }

        .toollist-seperateline {
          margin-left: ${polished.rem(16)};
          height: 30px;
          border: 1px solid ${theme.colors.palette.light[800]};
        }

        .liveview-gap {
          margin-right: ${polished.rem(20)};
        }
      }

      .account-dropdown-toggle:focus {
        outline: none;
        box-shadow: none !important;
      }`;
  }

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      currentPageId: state.appRuntimeInfo && state.appRuntimeInfo.currentPageId,
    };
  }

  componentDidMount() {
    if (this.props.queryObject.id) {
      this.refreshTitle(this.props.queryObject.id);
    }
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    if (this.props.queryObject.id && prevProps.queryObject.id !== this.props.queryObject.id) {
      return true;
    } else {
      return false;
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot) {
      this.setState({
        titleText: ''
      })

      this.refreshTitle(this.props.queryObject.id);
    }

    if (this.spanTextInput.current && this.state.titleLength !== this.spanTextInput.current.offsetWidth) {
      this.setState({
        titleLength: this.spanTextInput.current.offsetWidth + 2
      });
    }
  }

  focusEditTitle = () =>  {
    this.titleTextInput.current.select();
  }

  editTitle = () => {
    const currentTitle = this.titleTextInput.current.value;

    appServices.updateAppItem({
      id: this.props.queryObject.id,
      title: currentTitle
    }).then(() => {
      this.props.dispatch(builderActions.refreshAppListAction(true));
    }, err => {
      console.error(err);
    })
  }

  refreshTitle = (id: string) => {
    appServices.searchAppById(id).then(appItem => {
      this.setState({
        titleText: appItem.title
      })
    }, err => {
      console.error(err);
    });
  }

  titleTextChange = (event) => {
    const currentTitle = event.target.value;
    this.setState({
      titleText: currentTitle
    });
  }

  sign = () => {
    const session = SessionManager.getInstance().getMainSession();
    session ? this.signOut() : this.signIn();
  }

  signOut = () => {
    this.setState({
      accountPopoverOpen: false
    })

    SessionManager.getInstance().signOut();
  }

  signIn = () => {
    this.setState({
      accountPopoverOpen: false
    })

    SessionManager.getInstance().signIn('/', false);
  }

  nls = (id: string) => {
    return this.props.intl ? this.props.intl.formatMessage({ id: id, defaultMessage: defaultMessages[id] }) : id;
  }

  handleKeydown = (e: any) => {
    if (e.keyCode === 13) {
      this.titleTextInput.current.blur();
    } else {
      return
    }
  }

  toggleAccount = () => {
    this.setState({
      accountPopoverOpen: !this.state.accountPopoverOpen
    });
  }

  onTitleContainerResize = (width, height) => {
    this.setState({
      titleLength: this.spanTextInput.current.offsetWidth + 2
    });
  }

  getDropdownStyle = (theme: ThemeVariables): SerializedStyles => {
    return css`
      .dropdown-menu--inner {
        button {
          display:block;
          text-align:center;
        }
        .dropdown-item:hover, .dropdown-item:focus {
          background:${theme.colors.primary};
        }
        .dropdown-item:disabled:hover, .dropdown-item:disabled:focus {
          background:${theme.colors.palette.light[500]};
        }

      }
    `
  }

  render() {
    let userThumbnail = null;
    const session = SessionManager.getInstance().getMainSession();
    if (this.props.user && this.props.user.thumbnail) {
      userThumbnail = this.props.portalUrl + '/sharing/rest/community/users/' + this.props.user.username + '/info/'
      + this.props.user.thumbnail + '?token=' + session.token;
    }

    const pageId = this.props.currentPageId;
    let showTitle = true;
    if (pageId === 'template') {
      showTitle = false;
    }

    return <div css={this.getStyle()} className="h-100">
    <div className="widget-builder-header d-flex justify-content-between h-100 pr-0 border-left-0 border-right-0 border-top-0">
      <div className="header-logo d-flex align-items-center">
        <a href={`${window.jimuConfig.mountPath}`}>
          <img className="header-logo-item d-block" src={require('./assets/exb-logo.png')}/>
        </a>
        {showTitle && <div className="header-title d-flex align-items-center header-title-maxwidth-screen" onClick={this.focusEditTitle}>
          <input ref={this.titleTextInput} className="header-title-input  font-weight-normal" title={this.state.titleText}
            style={{width: `${this.state.titleLength}px`, fontSize: '16px'}}
            value={this.state.titleText} onBlur={this.editTitle} onChange={this.titleTextChange}
            onKeyDown ={ (e) => {this.handleKeydown(e)}}>
          </input>
        </div>}
      </div>
      <div style={{position: 'absolute', top: '-30px'}}>
        <div style={{zIndex: -1, position: 'relative'}}>
          <span className="px-1 border font-weight-normal" style={{fontSize: '16px', position: 'relative', opacity: 0, whiteSpace: 'pre', zIndex: -1}} ref={this.spanTextInput}>
            {this.state.titleText}
          </span>
        </div>
        <ReactResizeDetector handleWidth handleHeight onResize={this.onTitleContainerResize} />
      </div>
      <div className="d-flex align-items-center">
        <SwitchLiveView intl={this.props.intl}/>
        <div className="liveview-gap"></div>
        <SwitchDevice intl={this.props.intl}/>
        <SwitchResolution/>
        <div className="toollist-seperateline border-bottom-0 border-top-0 border-left-0 mt-1 mb-1 ml-1 mr-1"></div>
        <ToolList intl={this.props.intl} onSaveStatusChanged={(isToolListSaved) => {this.isToolListSaved = isToolListSaved}}/>
        <div className="float-right d-flex h-100 justify-content-end">
          <Dropdown  size="sm" toggle={this.toggleAccount} isOpen={this.state.accountPopoverOpen} className="h-100">
            <DropdownToggle size="sm" type="tertiary" className="d-block h-100 p-0 m-0 account-dropdown-toggle">
              <div id="accountPopover" className="header-account float-left d-flex align-items-center h-100" style={{cursor: 'pointer'}}
                title={this.props.user && this.props.user.firstName ? this.props.user.firstName : ''}>
                {userThumbnail && <Image src={userThumbnail} width={26} height={26} shape="circle" className="d-block float-left header-login"/>}
                {!userThumbnail && <Icon icon={IconAccount} width={26} height={26} className="d-block float-left header-login"/>}
              </div>
            </DropdownToggle>
            <DropdownMenu css={this.getDropdownStyle(this.props.theme)} appendTo="body" alightment="center" flip={true}>
              <DropdownItem onClick={() => {this.sign()}}>
                {session ? this.nls('signOut') : this.nls('signIn')}
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    </div></div>
  }
}
