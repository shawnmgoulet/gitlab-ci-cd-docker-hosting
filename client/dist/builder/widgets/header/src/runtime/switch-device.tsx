/** @jsx jsx */
import {React, ThemeVariables, SerializedStyles, IntlShape, polished, BrowserSizeMode,
  css, jsx, classNames as classnames, IMState, themeUtils, ReactRedux} from 'jimu-core';
import {builderAppSync} from 'jimu-for-builder'
import {ButtonGroup, Button, Icon} from 'jimu-ui';
import defaultMessages from './translations/default';

let IconLarge = require('jimu-ui/lib/icons/desktop.svg');
let IconSmall = require('jimu-ui/lib/icons/mobile.svg');
let IconMedium = require('jimu-ui/lib/icons/pad.svg');

interface ExtraProps {
  theme?: ThemeVariables;
  browserSizeMode?: BrowserSizeMode;
  intl?: IntlShape;
}

class _SwitchDeviceComponent extends React.PureComponent<ExtraProps, {}> {

  editPageForLargeScreen: string;
  editPageForMediumScreen: string;
  editPageForSmallScreen: string;

  constructor(props) {
    super(props);

    this.editPageForLargeScreen = this.props.intl.formatMessage({id: 'editPageForLargeScreen', defaultMessage: defaultMessages.editPageForLargeScreen});
    this.editPageForMediumScreen = this.props.intl.formatMessage({id: 'editPageForMediumScreen', defaultMessage: defaultMessages.editPageForMediumScreen});
    this.editPageForSmallScreen = this.props.intl.formatMessage({id: 'editPageForSmallScreen', defaultMessage: defaultMessages.editPageForSmallScreen});
  }

  getStyle (theme: ThemeVariables): SerializedStyles {
    return css`
      .device-switch-group {
        margin-right: ${polished.rem(10)};
      }

      .device-switch {
        width: ${polished.rem(28)};
        height: ${polished.rem(28)};
        border-radius: 2px !important;
        border: 0;
        margin-left: 1px;
        margin-right: 1px;

        &:focus {
          outline: none;
          box-shadow: none !important;
        }
      }

      .device-switch-gap {
        margin-right: ${polished.rem(5)};
      }

      .device-active {
        background-color: ${theme.colors.primary} !important;
        color: ${theme.colors.black} !important;
      }

      .no-animation {
        transition: none;
        -webkit-transition: none;
      }
    `;
  }

  onBrowserSizeModeChange(mode: BrowserSizeMode){
    // getAppConfigAction().createLayoutForSizeMode(mode, this.props.browserSizeMode).exec();
    builderAppSync.publishChangeBrowserSizeModeToApp(mode);

    if (mode !== this.props.browserSizeMode) {
      builderAppSync.publishChangeSelectionToApp(null);
    }

    this.setState({
      isDeviceChooseShow: false
    });
  }

  render() {
    return <div css={this.getStyle(this.props.theme)}><ButtonGroup title={'Shift+Alt+X'} className="h-100 d-flex align-items-center device-switch-group">
    <div className="h-100 d-flex align-items-center device-switch-container no-animation device-switch-gap">
      <Button icon type="tertiary" onClick={() => {this.onBrowserSizeModeChange(BrowserSizeMode.Large)}}
        className={classnames('device-switch d-flex align-items-center p-0', {
          'device-active': (!this.props.browserSizeMode || this.props.browserSizeMode === BrowserSizeMode.Large),
          'device-disactive': (this.props.browserSizeMode && this.props.browserSizeMode !== BrowserSizeMode.Large)
        })} title={this.editPageForLargeScreen}>
        <Icon icon={IconLarge}/>
      </Button>
    </div>
    <div className="h-100 d-flex align-items-center device-switch-container device-switch-gap">
      <Button icon type="tertiary" onClick={() => {this.onBrowserSizeModeChange(BrowserSizeMode.Medium)}}
        className={classnames('device-switch d-flex align-items-center p-0 no-animation', {
          'device-active': (this.props.browserSizeMode === BrowserSizeMode.Medium),
          'device-disactive': !(this.props.browserSizeMode === BrowserSizeMode.Medium)
        })} title={this.editPageForMediumScreen}>
        <Icon icon={IconMedium}/>
      </Button>
    </div>
    <div className="h-100 d-flex align-items-center device-switch-container">
      <Button icon type="tertiary" onClick={() => {this.onBrowserSizeModeChange(BrowserSizeMode.Small)}}
        className={classnames('device-switch d-flex align-items-center p-0 no-animation', {
          'device-active': (this.props.browserSizeMode === BrowserSizeMode.Small),
          'device-disactive': !(this.props.browserSizeMode === BrowserSizeMode.Small)
        })} title={this.editPageForSmallScreen}>
        <Icon icon={IconSmall}/>
      </Button>
    </div>
   </ButtonGroup></div>
  }
}

const _SwitchDeviceComponentInner = themeUtils.withTheme(_SwitchDeviceComponent);

const mapExtraStateProps = (state: IMState): ExtraProps => {
  return {
    browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
  };
}

export default ReactRedux.connect<{}, {}, ExtraProps>(mapExtraStateProps)(_SwitchDeviceComponentInner);