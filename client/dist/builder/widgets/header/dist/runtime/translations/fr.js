define({
  createNew: 'æ_Create New_____________________Â',
  newExperience: 'æ_New experience_______________Â',
  signOut: 'æ_Sign out_________________Â',
  signIn: 'æ_Sign in_______________Â',
  undo: 'æ_Undo_________Â',
  redo: 'æ_Redo_________Â',
  save: 'æ_Save_________Â',
  saving: 'æ_Saving_____________Â',
  saved: 'æ_Saved___________Â',
  saveError: 'æ_Saving error_____________Â',
  saveSuccess: 'æ_Saved successfully____________________Â!',
  preview: 'æ_Preview_______________Â',
  publish: 'æ_Publish_______________Â',
  publishing: 'æ_Publishing_____________________Â',
  published: 'æ_Published___________________Â',
  publishError: 'æ_Publishing error_________________Â',
  publishSuccess: 'æ_Published successfully________________________Â!',
  publishTo: 'æ_Publish to_____________________Â',
  publishOptions: 'æ_Publish options________________Â',
  copySuccess: 'æ_Copied successfully_____________________Â!',
  changeShareSettings: 'æ_Change share settings______________________Â',
  viewPublishedItem: 'æ_View published item____________________Â',
  copyPublishedItemLink: 'æ_Copy published item link_________________________Â',
  headerLeave: 'æ_Leave___________Â',
  headerLeaveSite: 'æ_Leave site____________Â?',
  headerLeaveDescription: 'æ_Changes you made may not be saved__________________Â.',
  editPageForLargeScreen: 'æ_Edit your page for large screen devices_____________________Â',
  editPageForMediumScreen: 'æ_Edit your page for medium screen devices_____________________Â',
  editPageForSmallScreen: 'æ_Edit your page for small screen devices_____________________Â',
  appMode: 'æ_Live view___________________Â',
  generateTemplate: 'æ_Generate template__________________Â',
  moreOptionsForTool: 'æ_More_________Â',
  moreTools: 'æ_More tools_____________________Â',
  access: 'æ_Access_____________Â',
  generateTemplateSuccess: 'æ_Generated template successfully_________________Â',
  generateTemplateError: 'æ_Generating error_________________Â',
  lockLayout: 'æ_Lock layout____________Â',
});