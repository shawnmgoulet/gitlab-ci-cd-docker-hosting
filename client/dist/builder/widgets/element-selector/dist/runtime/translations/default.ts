export default {
  /* tslint:disable-next-line */
  pendingElementsInfo: 'This area lists widgets that have been configured but not on the current canvas. They may exist in other device modes.',
  element: 'Insert',
  new: 'New',
  pending: 'Pending',
  section: 'Section',
  placeholder: 'Placeholder',
  widget: 'Basic',
  layoutWidget: 'Layout',
}
