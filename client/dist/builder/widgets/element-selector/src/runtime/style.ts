import {ThemeVariables, css, SerializedStyles, polished} from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles{

  return css`
  .widget-builder-header-insert-elements {
    .text-data-600{
      color: ${theme.colors.palette.dark[600]};
    }
    .title{
      padding: 18px 16px 8px 16px !important;
      font-size: 1rem !important;
    }
    width: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    text-align: left;
    /* border-right: 1px solid ${theme.colors.palette.light[800]}; */
    .collapse-btn{
      cursor: pointer;
      .jimu-icon{
        vertical-align: top;
      }
    }
    .jimu-nav{
      margin-left: ${polished.rem(16)};
      margin-right: ${polished.rem(16)};
      margin-bottom: ${polished.rem(16)};
    }
    .new-elements-title{
      font-size: 14px;
      font-weight: 400;
      color: ${theme.colors.palette.dark[400]};
      margin-bottom: ${polished.rem(10)};
    }
    .jimu-nav{
      height: ${polished.rem(43)} !important;
    }
    .jimu-tab{
      height: calc(100% - 60px);
    }
    .tab-content{
      overflow: auto;
      padding: 0 0.5rem;
    }

    .elements-collapse-item{
      height: ${polished.rem(40)};
      .elements-collapse-item-icon-container{
        width: ${polished.rem(30)};
        height: ${polished.rem(30)};
        background: ${theme.colors.secondary};
        margin-right: ${polished.rem(15)};
        .elements-collapse-item-icon{
          width: ${polished.rem(16)};
          height: ${polished.rem(16)};
        }
      }
      .elements-collapse-item-label{
        max-width: ${polished.rem(180)};
        color: ${theme.colors.dark};
        line-height: ${polished.rem(30)};
      }
    }

    .elements-collapse-item:hover, .elements-collapse-item:hover.elements-collapse-item:active{
      background: ${polished.rgba(theme.colors.secondary, 0.4)};
      font-size: ${polished.rem(13)};
      .elements-collapse-item-icon-container{
        background: ${theme.colors.palette.light[500]};
      }
    }

    .btn{
      .jimu-icon{
        margin: 0;
      }
    }
    .jimu-builder-panel--header {
      padding: ${theme.sizes[2]} ${theme.sizes[3]};
      display: flex;
      flex-direction: row;
      align-items: center;
      h3 {
        margin: 0;
        line-height: 1.5;
        flex-grow: 1;
      }
    }
    .jimu-builder-panel--content {
      height: 100%;
      overflow: auto;
      .widget-card-item{
        height: 70px;
        background-color: ${theme.colors.palette.light[500]};
        user-select: none;
        cursor: pointer;
        .widget-card-image{
          width: 20px;
          height: 20px;
          &:after{
            display: none;
          }
        }

        .widget-card-name{
          max-width: 90px;
          margin: 0 auto;
          max-height: 28px;
          margin-top: 5px;
        }
      }
      .widget-card-item:hover{
        background-color: ${theme.colors.palette.light[600]};
      }

      .row {
        .col-6 {
          flex: 1 0 0;
          margin-right: ${theme.sizes[1]};
        }
        .col-6 + .col-6 {
          margin-left: ${theme.sizes[1]};
          margin-right: 0;
        }
        .col {
          flex-basis: 100%;
        }
      }
    }
    &.widget-popup-hide-animation{
      transition: transform 0.3s ease-out;
      &.from-left {
        transform: translateX(-100%);
      }
      &.from-right {
        transform: translateX(100%);
      }
    }
    &.widget-popup-show-animation{
      transition: transform 0.3s ease-in;
      &.from-left {
        transform: translateX(0%);
      }
      &.from-right {
        transform: translateX(0%);
      }
    }
    &.from-left {
      left: 0;
    }
    &.from-right {
      right: 0;
    }
  }`;
}