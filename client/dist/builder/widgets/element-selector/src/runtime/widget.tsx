/** @jsx jsx */
import {BaseWidget, moduleLoader, jsx, BrowserSizeMode, appConfigUtils, getAppStore, AppMode,
  LayoutItemType, LayoutItemConstructorProps, PagePart, IMState, AllWidgetProps, IMConfig} from 'jimu-core';
import {Tabs, Tab} from 'jimu-ui';

import {NewElements} from './components/new-elements';
import {PendingElements} from './components/pending-elements';

import {getStyle} from './style';

import defaultMessages from './translations/default';

interface ExtraProps{
  currentPageId: string;
  currentViewId: string;
  appPath: string;
  appMode: AppMode;
  browserSizeMode: BrowserSizeMode;
  activePagePart: PagePart;
}

interface State{
  widgetList: LayoutItemConstructorProps[];
}
export default class Widget extends BaseWidget<AllWidgetProps<IMConfig> & ExtraProps, State>{

  constructor(props){
    super(props);
    this.state = {
      widgetList: null
    };
  }

  componentDidMount(){
    this.getWidgetListInfo();
  }

  getWidgetListInfo(){
    fetch(`${moduleLoader.resolveModuleFullPath('widgets')}/widgets-info.json`).then(res => res.json()).then((widgetInfo: any[]) => {
      let list = widgetInfo.map(w => {
        w.manifest = appConfigUtils.addWidgetManifestProperties(w.manifest);
        let listItemTemplate: LayoutItemConstructorProps = {
          itemType: LayoutItemType.Widget,
          name: w.name,
          label: w.i18nLabel[getAppStore().getState().appContext.locale] || w.manifest.label || w.name,
          uri: w.uri,
          manifest: w.manifest,
          icon: '../' + w.icon
        };
        return listItemTemplate;
      });

      this.setState({widgetList: list});
    })
  }

  render(){

    return (
      <div css={getStyle(this.props.theme)}>
        <div className="jimu-builder-panel widget-builder-header-insert-elements from-left flex-column bg-light-300">

        <div className="jimu-builder-panel--header d-flex flex-row text-data-600 title">
          <div className="flex-grow-1 m-0">
            {this.props.intl.formatMessage({id: 'element', defaultMessage: defaultMessages.element})}
          </div>
        </div>

        <Tabs underline fill>
          <Tab title={this.props.intl.formatMessage({id: 'new', defaultMessage: defaultMessages.new})} active={true}>
            {
              <NewElements currentPageId={this.props.currentPageId}
                currentViewId={this.props.currentViewId} appPath={this.props.appPath} widgetList={this.state.widgetList}
                dispatch={this.props.dispatch} appMode={this.props.appMode} intl={this.props.intl}
              />
            }
          </Tab>
          <Tab title={this.props.intl.formatMessage({id: 'pending', defaultMessage: defaultMessages.pending})}>
            <PendingElements browserSizeMode={this.props.browserSizeMode} currentPageId={this.props.currentPageId} appMode={this.props.appMode}
              intl={this.props.intl} activePagePart={this.props.activePagePart}
            />
          </Tab>
        </Tabs>

      </div>
    </div>);
  }

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    const currentPageId = state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentPageId;

    return {
      currentPageId,
      currentViewId: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentViewId,
      appMode: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.appMode,
      appPath: state.appPath,
      browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode,
      activePagePart: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.activePagePart
    };
  }
}
