/** @jsx jsx */
import {
  BaseWidget, IMPageJson, IMState, AllWidgetProps, appConfigUtils, React, Immutable,
 Selection, IMAppConfig, ImmutableObject, BrowserSizeMode, PageType
} from 'jimu-core';
import {getAppConfigAction, builderAppSync, appConfigUtils as appConfigUtilsForBuilder} from 'jimu-for-builder';
import { css, jsx, ThemeVariables, SerializedStyles } from 'jimu-core';
import { Icon, Button } from 'jimu-ui';
import { createPageFromTemplate } from 'jimu-layouts/layout-builder';
import PageList from './components/page-toc-list';
import defaultMessages from './translations/default';
import OutlineList, { OutlineType, OutlineItemJson, TocItemIdSplite } from './components/outline-list';
import {interact} from 'jimu-core/dnd';

interface ExtraProps{
  currentPageId: string;
  currentDialogId: string;
  browserSizeMode: BrowserSizeMode;
}

interface WState{
  editablePageItemId: string;
  editableOutlineItemId: string;
  isTemplatePopoverOpen: boolean;
  pageTocH: number;
}

const AppToolH = 50;
export default class Widget extends BaseWidget<AllWidgetProps<{}> & ExtraProps, WState>{

  popoverRef: any;
  chooseTemplateStr: string;
  newPageStr: string;
  contentUpperStr: string;
  pagesUpperStr: string;
  bodyUpperStr: string;
  loadingStr: string;
  addPageStr: string;
  addLinkStr: string;
  addFolderStr: string;
  interactable: Interact.Interactable;
  resizeRef: React.RefObject<HTMLDivElement> ;
  lastResizeCall = null;
  pageTocRef: React.RefObject<HTMLDivElement>;

  constructor(props){
    super(props);
    this.state = {
      editablePageItemId: '',
      editableOutlineItemId: '',
      isTemplatePopoverOpen: false,
      pageTocH: -1
    }
    this.resizeRef = React.createRef();
    this.pageTocRef = React.createRef();
    this.popoverRef = React.createRef();
    this.chooseTemplateStr = this.props.intl.formatMessage({id: 'chooseTemplate', defaultMessage: defaultMessages.chooseTemplate});
    this.newPageStr = this.props.intl.formatMessage({id: 'newPage', defaultMessage: defaultMessages.newPage});
    this.pagesUpperStr = this.props.intl.formatMessage({id: 'pages_upper', defaultMessage: defaultMessages.pages_upper});
    this.contentUpperStr = this.props.intl.formatMessage({id: 'content_upper', defaultMessage: defaultMessages.content_upper});
    this.bodyUpperStr = this.props.intl.formatMessage({id: 'body_upper', defaultMessage: defaultMessages.body_upper});
    this.loadingStr = this.props.intl.formatMessage({id: 'tocLoading', defaultMessage: defaultMessages.tocLoading});
    this.addPageStr = this.props.intl.formatMessage({id: 'addPage', defaultMessage: defaultMessages.addPage});
    this.addLinkStr = this.props.intl.formatMessage({id: 'addLink', defaultMessage: defaultMessages.addLink});
    this.addFolderStr = this.props.intl.formatMessage({id: 'addFolder', defaultMessage: defaultMessages.addFolder});

    this.changeEditablePageId = this.changeEditablePageId.bind(this);
    this.addPageWithType = this.addPageWithType.bind(this);
    this.handleOutlineItemClick = this.handleOutlineItemClick.bind(this);
    this.changeCurrentPage = this.changeCurrentPage.bind(this);
    this.movePageIntoPage = this.movePageIntoPage.bind(this);
    this.removePage = this.removePage.bind(this);
    this.setHomePage = this.setHomePage.bind(this);
    this.formatMessage = this.formatMessage.bind(this);
    this.loadPageTemplate = this.loadPageTemplate.bind(this);
    this.parsePageTemplate = this.parsePageTemplate.bind(this);
    this.duplicatePage = this.duplicatePage.bind(this);
    this.renamePage = this.renamePage.bind(this);
    this.orderPageBelowPage = this.orderPageBelowPage.bind(this);
  }

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      currentPageId: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentPageId,
      currentDialogId: state.appStateInBuilder && state.appStateInBuilder.appRuntimeInfo.currentDialogId,
      browserSizeMode: state.appStateInBuilder && state.appStateInBuilder.browserSizeMode
    }
  }

  componentDidMount(){
    if(!this.resizeRef || !this.resizeRef.current)return;
    this.interactable = interact(this.resizeRef.current)
        .resizable({
          // resize from all edges and corners
          edges: {
            top: true,
            left: false,
            bottom: false,
            right: false
          },
          modifiers: [
            // keep the edges inside the parent
            interact.modifiers.restrictEdges({
              outer: 'parent',
              endOnly: true,
            }),

            // minimum size
            interact.modifiers.restrictSize({
              min: { width: 20, height: 100 },
            }),
          ],
          inertia: false,
          onstart: (event: Interact.InteractEvent) => {
            event.stopPropagation();
          },
          onmove: (event: Interact.ResizeEvent) => {
            event.stopPropagation();

            if (this.lastResizeCall) {
              cancelAnimationFrame(this.lastResizeCall);
            }
            const rect = event.rect;
            let pageTop = AppToolH;
            if(this.pageTocRef.current){
              pageTop = this.pageTocRef.current.getBoundingClientRect().top;
            }
            const pageTocH = rect.top - pageTop;
            if(pageTocH < 100) return;
            this.lastResizeCall = requestAnimationFrame(() => {
              this.setState({
                pageTocH: pageTocH
              })
            });
          },
          onend: (event: Interact.ResizeEvent) => {
            event.stopPropagation();
            if (this.lastResizeCall) {
              cancelAnimationFrame(this.lastResizeCall);
            }
            this.lastResizeCall = requestAnimationFrame(() => {
              const rect = event.rect;
              let pageTop = AppToolH;
              if(this.pageTocRef.current){
                pageTop = this.pageTocRef.current.getBoundingClientRect().top;
              }
              this.lastResizeCall = requestAnimationFrame(() => {
                this.setState({
                  pageTocH: rect.top - pageTop
                })
              });
            });
          }
        })
  }

  componentWillUnmount(){
    if (this.lastResizeCall) {
      cancelAnimationFrame(this.lastResizeCall);
    }
    if (this.interactable) {
      this.interactable.unset();
      this.interactable = null;
    }
  }

  emptyLayout = {};

  handleOutlineItemClick(itemJson: OutlineItemJson){
    if(itemJson.type === OutlineType.Label){
      return;
    }else if(itemJson.type === OutlineType.Section || itemJson.type === OutlineType.Widget){
      this.changeWidgetOrSection(itemJson)
    }else if(itemJson.type === OutlineType.View){
      this.changeView(itemJson);
    }else if(itemJson.type === OutlineType.Layout){
      this.changeLayout(itemJson);
    }
  }

  changeCurrentPage(pageId: string){

    this.changeSelection(null);
    builderAppSync.publishPageChangeToApp(pageId);
  }

  changeEditablePageId(pageId: string){
    if(pageId !== this.state.editablePageItemId){
      this.setState({
        editablePageItemId: pageId
      })
    }
  }

  movePageIntoPage(subPageId: string, parentPageId: string){
    if (subPageId === parentPageId) return;
    const appConfig = getAppConfigAction().appConfig;
    getAppConfigAction().movePageIntoPage(subPageId, parentPageId).exec();
    // const {appConfig} = this.props;
    const pageJson = appConfig.pages[subPageId];
    if(pageJson.type !== PageType.Normal)return;
    this.changeCurrentPage(subPageId);
  }

  removePage(pageId: string){
    const appConfig = getAppConfigAction().appConfig;
    let pageJson = appConfig.pages[pageId];

    let changeToPageId;
    appConfig.pageStructure.some((ps, i) => {
      let pId = Object.keys(ps)[0];
      if(pId === pageId)return false;
      if(appConfigUtilsForBuilder.isRealPage(appConfig, pId)){
        changeToPageId = pId;
        return true;
      }
    })
    if(!changeToPageId){
      appConfig.pageStructure.some((ps, i) => {
        let pId = Object.keys(ps)[0];
        if(pId === pageId) return false;
        let subPs = ps[pId];
        return !!subPs.some((subPId) => {
          if(subPId === pageId) return false;
          if(appConfigUtilsForBuilder.isRealPage(appConfig, subPId)){
            changeToPageId = subPId;
            return true;
          }
        })

      })
    }
    if(!changeToPageId)return;

    builderAppSync.publishPageChangeToApp(changeToPageId);
    getAppConfigAction().removePage(pageId).exec();

    if(pageJson.isDefault){
      getAppConfigAction().setHomePage(changeToPageId).exec();
    }
  }

  setHomePage(pageId: string){
    getAppConfigAction().replaceHomePage(pageId).exec();
    // builderAppSync.publishPageChangeToApp(pageId);
  }

  duplicatePage(pageId: string){
    const appConfig = getAppConfigAction().appConfig;
    const appConfigAction = getAppConfigAction();
    const newPageJson = appConfigAction.duplicatePage(pageId);
    appConfigAction.exec();
    const pageJson = appConfig.pages[pageId];
    if(pageJson.type !== PageType.Folder && pageJson.type !== PageType.Link)
      this.changeCurrentPage(newPageJson.id);
  }

  renamePage(pageId: string, newName: string){
    if(!newName || newName === '')return false;
    getAppConfigAction().editPageProperty(pageId, 'label', newName).exec();
    this.changeEditablePageId('');
    return true;
  }

  orderPageBelowPage(pageId: string, topPageId: string, dropType: 'top' | 'bottom'){
    getAppConfigAction().orderPageToPage(pageId, topPageId, dropType).exec();
  }

  formatMessage(id: string): string{
    return this.props.intl.formatMessage({id: id, defaultMessage: defaultMessages[id]})
  }

  addPageWithType(type: 'page' | 'link' | 'folder', templatePageJson?: ImmutableObject<any>): IMPageJson{
    let pageJson;
    const appConfig = getAppConfigAction().appConfig;
    switch(type){
      case 'page':
        pageJson = this.loadPageTemplate(templatePageJson);
        // switch to main device mode
        builderAppSync.publishChangeBrowserSizeModeToApp(getAppConfigAction().appConfig.mainSizeMode);
        return pageJson;
      case 'link':
        pageJson = Immutable({}).merge({
          id: appConfigUtils.getUniqueId(appConfig, 'page'),
          type: PageType.Link,
          label: appConfigUtils.getUniqueLabel(appConfig, 'page', this.formatMessage('link')),
          linkUrl: '#',
          isVisible: true,
        }) as IMPageJson;
        getAppConfigAction().addPage(pageJson).exec();
        break;

      case 'folder':
        pageJson = Immutable({}).merge({
          id: appConfigUtils.getUniqueId(appConfig, 'page'),
          type: PageType.Folder,
          label: appConfigUtils.getUniqueLabel(appConfig, 'page', this.formatMessage('folder')),
          isVisible: true,
        }) as IMPageJson
        getAppConfigAction().addPage(pageJson).exec();
        break;
    }
    // this.changeCurrentPage(pageJson.id);
    this.changeEditablePageId(pageJson.id);
    return pageJson;
  }

  loadPageTemplate(templatePageJson?: ImmutableObject<any>){
    const appConfig = getAppConfigAction().appConfig;
    // if(!templatePageJson || !templatePageJson.layout || !templatePageJson.layouts || Object.keys(templatePageJson.layouts).length < 1)return undefined;
    this.parsePageTemplate(templatePageJson, appConfig);
  }

  parsePageTemplate(templatePageJson: ImmutableObject<any>, initAppConfig: IMAppConfig) {
    if (!templatePageJson || !templatePageJson.pageId) {
      return;
    }
    createPageFromTemplate(initAppConfig, templatePageJson as any, templatePageJson.pageId, {})
    .then(({ appConfig, newPageId }) => {
      const pageJson = appConfig.pages[newPageId];
      const updatedAppConfig = appConfig.set(
        'pageStructure',
        appConfig.pageStructure.concat([{ [pageJson.id]: [] } as any]));
      const appConfigAction = getAppConfigAction(updatedAppConfig);
      appConfigAction.exec();
      this.changeCurrentPage(pageJson.id);
      this.changeEditablePageId(pageJson.id);
      return pageJson;
    });
  }

  getUniqueIds = (appConfig: IMAppConfig, type: 'page' | 'layout' | 'widget' | 'section' | 'view', size: Number) : string[] => {
    const ids: string[] = [];
    for(let i = 0; i < size; i ++){
      const id = appConfigUtils.getUniqueId(appConfig, type);
      ids.push(id);
      appConfig = appConfig.setIn([type + 's', id], {id: id} as any);
    }
    return ids;
  }

  getUniqueLabels = (appConfig: IMAppConfig, type: 'page' | 'layout' | 'section' | 'view', size: Number) : string[] => {
    const labels: string[] = [];
    for(let i = 0; i < size; i ++){
      const id = appConfigUtils.getUniqueId(appConfig, type);
      const label = appConfigUtils.getUniqueLabel(appConfig, type, type);
      labels.push(label);
      appConfig = appConfig.setIn([type + 's', id], {id: id, label: label} as any);
    }
    return labels;
  }

  changeSelection = (selection: Selection) => {
    builderAppSync.publishChangeSelectionToApp(selection);
  }

  changeWidgetOrSection = (itemJson: OutlineItemJson) => {
    const strs = itemJson.id.split(TocItemIdSplite);
    this.changeSelection({layoutId: strs[0], layoutItemId: strs[1]});
  }

  changeView = (itemJson: OutlineItemJson) => {
    const strs = itemJson.id.split(TocItemIdSplite); // layoutId layoutItemId sectionId viewId
    builderAppSync.publishViewChangeToApp(strs[0], strs[1])
  }

  changeLayout = (itemJson: OutlineItemJson) => {

  }

  getCurrentPageId = (): string => {
    let changeToPageId;
    const appConfig = getAppConfigAction().appConfig;
    appConfig.pageStructure.some((ps, i) => {
      let pId = Object.keys(ps)[0];
      if(appConfigUtilsForBuilder.isRealPage(appConfig, pId)){
        changeToPageId = pId;
        return true;
      }
    })
    if(!changeToPageId){
      appConfig.pageStructure.some((ps, i) => {
        let pId = Object.keys(ps)[0];
        let subPs = ps[pId];
        return subPs.some((subPId) => {
          if(appConfigUtilsForBuilder.isRealPage(appConfig, subPId)){
            changeToPageId = subPId;
            return true;
          }
        })

      })
    }
    builderAppSync.publishPageChangeToApp(changeToPageId);
    return changeToPageId;
  }

  renderActionBtn = (title: string, icon: React.ComponentClass<React.SVGAttributes<SVGElement>>, actionFunc: () => void) => {
    return <Button title={title} size={'sm'} color={'link'} className=" rounded icon page-action-btn" onClick={actionFunc}><Icon size={12} icon={icon} ></Icon></Button>
  };

  PageListWrapper = () => {
    //let pId = pageJson.id;
    return <PageList onDefaultClick={this.setHomePage} addPageWithType={this.addPageWithType}
                     editablePageItemId={this.state.editablePageItemId} theme={this.props.theme} changeEditablePageItemId={this.changeEditablePageId}
                     currentPageItemId={this.props.currentPageId} removePage={this.removePage} intl={this.props.intl}
                     duplicatePage={this.duplicatePage} renamePage={this.renamePage} reOrderPage={this.orderPageBelowPage}
                     onClickPage={this.changeCurrentPage} movePageIntoPage={this.movePageIntoPage} browserSizeMode={this.props.browserSizeMode} />;
  }

  getStyle = (theme: ThemeVariables): SerializedStyles => {
    const defaultPageTocH = '33%';
    const {pageTocH} = this.state;
    return css`
      overflow: hidden;

      .page-toc {
        background-color: ${theme.colors.palette.light[300]};
        height: ${pageTocH > 0 ? `${pageTocH}px` : defaultPageTocH};
      }

      .outline-toc {
        background-color: ${theme.colors.palette.light[300]};
        border: 0;
        border-top: 2px solid ${theme.colors.palette.light[800]};
        height: calc(100% - ${pageTocH > 0 ? `${pageTocH}px` : defaultPageTocH});
      }
/*
      .toc-list-header{
        padding: 10px 16px;
        &.header{
          padding-bottom: 0;
          padding-top: 10px;
        }
        &.footer{
          padding-bottom: 0;
          padding-top: 10px;
        }
        .page-action-btn {
          margin-left:0.5rem;
          >.jimu-icon{
            fill: ${theme.colors.palette.dark[800]};
            margin: auto;
          }
          &:hover {
            color: ${theme.colors.black};
            cursor: pointer;
            svg {
              fill: ${theme.colors.black};
            }
          }

          &:active {
            color: ${theme.colors.black};
            svg {
              fill: ${theme.colors.black};
            }
          }

        }

        .btn-primary {
          background-color: ${theme.colors.palette.primary[500]};
          border-color: ${theme.colors.palette.primary[500]};
        }

        .btn-primary:hover {
          background-color: ${theme.colors.palette.primary[300]};
          border-color: ${theme.colors.palette.primary[300]};
        }
        .add-page-btn {
          font-size: 0.7rem;
          padding-left: 0.5rem;
          padding-right: 0.5rem;
          &:hover {
            svg {
              fill: ${theme.colors.black};
            }
          }
        }
      } */

    `
  }

  render(){
    const {PageListWrapper} = this;
    const {currentPageId, browserSizeMode} = this.props;

    return <div css={this.getStyle(this.props.theme)} className="jimu-widget widget-builder-toc bg-white w-100 h-100">
            <div className="page-toc" ref={this.pageTocRef}>
              <PageListWrapper />
            </div>
            <div className="outline-toc" ref={this.resizeRef}>
              <OutlineList currentPageId={currentPageId} browserSizeMode={browserSizeMode}
                onClickItem={this.handleOutlineItemClick}
                editableOutlineItemId={this.state.editableOutlineItemId} theme={this.props.theme} intl={this.props.intl}/>
            </div>

          </div>;
  }
}


