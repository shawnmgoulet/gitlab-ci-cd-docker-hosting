export default {
  chooseTemplate: 'å_Select a page template_______________________ø',
  newPage: 'å_New page_________________ø',
  remove: 'å_Remove_____________ø',
  duplicate: 'å_Duplicate___________________ø',
  rename: 'å_Rename_____________ø',
  pages_upper: 'å_Pages___________ø',
  content_upper: 'å_Content_______________ø',
  body_upper: 'å_Body_________ø',
  tocLoading: 'å_Loading_______________ø',
  addPage: 'å_Add page_________________ø',
  addLink: 'å_Add link_________________ø',
  addFolder: 'å_Add folder_____________________ø',
  makeHome: 'å_Make homepage______________ø',
  hideFromMenu: 'å_Hide from menu_______________ø',
  showFromMenu: 'å_Show in menu_____________ø',
  body: 'å_Body_________ø',
  header: 'å_Header_____________ø',
  footer: 'å_Footer_____________ø',
  more: 'å_More_________ø',
  outline: 'å_Outline_______________ø',
  folder: 'å_Folder_____________ø',
  expand: 'å_Expand_____________ø',
  setLink: 'å_Set link_________________ø',
  unexpand: 'å_Collapse_________________ø',
  certainly: 'å_OK_____ø',
  tip: 'å_Tip_______ø',
  removePageTip: 'å_There is(are) {subCount} subpage(s) in {label}, do you really want to remove it_________________________________________ø?',
  fullScreenApp: 'å_Full Screen App________________ø',
  fullScreenAppTip: 'å_Best for creating a web app that takes the full area of the browser window_______________________________________ø.',
  scrollingPage: 'å_Scrolling Page_______________ø',
  scrollingPageTip: 'å_Best for creating a web page that scrolls in the browser window_________________________________ø.',
}
