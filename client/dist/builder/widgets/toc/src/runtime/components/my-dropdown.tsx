/** @jsx jsx */
import {React, jsx, ThemeVariables, ImmutableObject} from 'jimu-core';
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Direction, Icon, Modifiers } from 'jimu-ui';

interface DropDownItem{
  label: string;
  event: (evt) => void;
  visible: boolean;
}
export type IMDropDownItem = ImmutableObject<DropDownItem>

interface Props{
  items: IMDropDownItem[],
  theme: ThemeVariables,
  group?: boolean,
  groupButtons?: any,
  icon?: any,
  direction?: Direction,
  disabled?: boolean,
  modifiers?: Modifiers;
}
export class MyDropDown extends React.PureComponent<Props, {isOpen: boolean}>{

  constructor(props){
    super(props);
    this.state = {isOpen: false}
  }

  onDropDownToggle = (evt) => {
    evt.stopPropagation();
    // if(!this.state.isOpen){
    //   if(document.onclick){
    //     document.onclick(evt);
    //   }
    //   document.onclick = evt => {
    //     this.setState({
    //       isOpen: false
    //     })
    //     document.onclick = undefined;
    //     return false;
    //   }
    // }else{
    //   document.onclick = undefined;
    // }

    this.setState({isOpen: !this.state.isOpen});
  }

  onItemClick = (evt, item) => {
    document.onclick = undefined;
    this.setState({
      isOpen: false
    })
    item.event(evt);
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
  }

  render(){
    let {items, icon, direction, disabled, group, groupButtons} = this.props;
    const {isOpen} = this.state;
    return <div className="d-flex align-items-center">
      <Dropdown group={!!group} direction={direction || 'down'} size="sm" toggle={this.onDropDownToggle}
        isOpen={isOpen}>
        {group && groupButtons}
        <DropdownToggle style={isOpen ? {display: 'inline-flex'} : {}} icon disabled={disabled} size="sm" type="tertiary">
          {icon || <Icon size={12} icon={require('jimu-ui/lib/icons/more-12.svg')}/>}
        </DropdownToggle>
        <DropdownMenu appendTo="body">
          {items.map((item: IMDropDownItem, i: number) => {
            return item.visible && 
            <DropdownItem key={i} className="no-user-select"
              onClick={evt => this.onItemClick(evt, item)} >
              {item.label}
            </DropdownItem>;
          })}
        </DropdownMenu>
      </Dropdown>
    </div>;
  }
}