define({
  addData: 'Ă_Add data_________________ș',
  back: 'Ă_Back_________ș',
  dataSource: 'Ă_Data_________ș',
  createFailedWarning: 'Ă_Failed to create data_______________________ș!',
  noSupportedDataSource: 'Ă_No supported data available_____________________________ș.',
  done: 'Ă_Done_________ș',
  next: 'Ă_Next_________ș',
  noUsedFieldToMap: 'Ă_No in use fields need to be mapped___________________ș.',
  noChildDssToMap: 'Ă_No child data needs to be mapped__________________ș.',
  inUse: 'Ă_In use_____________ș',
  widgets: 'Ă_widgets_______________ș',
  rename: 'Ă_Rename_____________ș',
  remove: 'Ă_Remove_____________ș',
  affectedWidgets: 'Ă_Affected widgets_________________ș',
  deleteBrokenWidgets: 'Ă_Delete the broken widgets___________________________ș.',
  removeDataSource: 'Ă_Remove data____________ș',
  cancel: 'Ă_Cancel_____________ș',
  others: 'Ă_Others_____________ș',
  layers: 'Ă_Layers_____________ș',
  fields: 'Ă_Fields_____________ș',
  settings: 'Ă_Settings_________________ș',
  relatedWidgets: 'Ă_Related widgets________________ș',
  previous: 'Ă_Previous_________________ș',
  creatingDataSourceFailed: 'Ă_Failed to create data_______________________ș!',
  noRelatedWidget: 'Ă_No related widget__________________ș',
  noUsedField: 'Ă_No used field______________ș',
  sort: 'Ă_Sort_________ș',
  filter: 'Ă_Filter_____________ș',
  data: 'Ă_Data_________ș'
});