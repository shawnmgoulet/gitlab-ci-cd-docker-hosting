define({
  addData: 'ä_Add data_________________Ü',
  back: 'ä_Back_________Ü',
  dataSource: 'ä_Data_________Ü',
  createFailedWarning: 'ä_Failed to create data_______________________Ü!',
  noSupportedDataSource: 'ä_No supported data available_____________________________Ü.',
  done: 'ä_Done_________Ü',
  next: 'ä_Next_________Ü',
  noUsedFieldToMap: 'ä_No in use fields need to be mapped___________________Ü.',
  noChildDssToMap: 'ä_No child data needs to be mapped__________________Ü.',
  inUse: 'ä_In use_____________Ü',
  widgets: 'ä_widgets_______________Ü',
  rename: 'ä_Rename_____________Ü',
  remove: 'ä_Remove_____________Ü',
  affectedWidgets: 'ä_Affected widgets_________________Ü',
  deleteBrokenWidgets: 'ä_Delete the broken widgets___________________________Ü.',
  removeDataSource: 'ä_Remove data____________Ü',
  cancel: 'ä_Cancel_____________Ü',
  others: 'ä_Others_____________Ü',
  layers: 'ä_Layers_____________Ü',
  fields: 'ä_Fields_____________Ü',
  settings: 'ä_Settings_________________Ü',
  relatedWidgets: 'ä_Related widgets________________Ü',
  previous: 'ä_Previous_________________Ü',
  creatingDataSourceFailed: 'ä_Failed to create data_______________________Ü!',
  noRelatedWidget: 'ä_No related widget__________________Ü',
  noUsedField: 'ä_No used field______________Ü',
  sort: 'ä_Sort_________Ü',
  filter: 'ä_Filter_____________Ü',
  data: 'ä_Data_________Ü'
});