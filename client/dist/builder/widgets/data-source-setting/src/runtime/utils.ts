import {getAppConfigAction} from 'jimu-for-builder';
import {dataComponentsUtils} from 'jimu-ui/data-source-selector';
import {DataSourceSchema, appConfigUtils, DataSource, Immutable, IMDataSourceSchema, ImmutableObject, IMWidgetJson, IMDataSourceJson, FieldSchema,
  IMUseDataSource, WidgetJson, UseDataSource, IMFieldSchema, DataSourceManager} from 'jimu-core';

export const autoMappingSymbol = '-$';

export function getUsedDsSchema(dataSource: DataSource, fieldFilter?: any): DataSourceSchema{
  const schema: DataSourceSchema = getDsSchema(dataSource);
  if(!schema){
    return null;
  }

  let usedSchema: DataSourceSchema;

  if(dataSource.isDataSourceSet){
    usedSchema = {
      childSchemas: {}
    };

    const usedDss = getUsedDss();

    Object.keys(schema.childSchemas)
      .filter(id => usedDss.some(usedId => dataSource.getFullChildDataSourceId(id) === usedId))
      .forEach(id => usedSchema.childSchemas[id] = Object.assign({}, getUsedDsSchema(dataSource.getChildDataSource(id), fieldFilter)));
  }else{
    usedSchema = {...getDsSchema(dataSource), ...{fields: {}}};
    const usedFields = getUsedFields(dataSource);

    schema.fields && Object.keys(schema.fields).filter(id => usedFields.some(usedId => id === usedId))
    .forEach(id => {
      (usedSchema as DataSourceSchema).fields[id] = Object.assign({}, schema.fields[id]);
      if(typeof fieldFilter === 'function'){
        (usedSchema as DataSourceSchema).fields[id] = fieldFilter((usedSchema as DataSourceSchema).fields[id]);
      }
    });
  }

  return usedSchema;
}

function getUsedDss(): string[]{
  // TODO: can child ds be used by parent ds?
  let dssUsedByWidget = getDssUsedByWidget();
  let dssUsedByMessage = getDssUsedByMessage();
  let dssUsedByOutputDs = getDssUsedByOutputDs();

  let allUsedDss = dssUsedByWidget.concat(dssUsedByMessage).concat(dssUsedByOutputDs);
  return Array.from(new Set(allUsedDss));
}

function getDssUsedByOutputDs(): string[]{
  const appConfig = dataComponentsUtils.getAppConfig();
  const dataSources = appConfig && appConfig.dataSources;
  let dssUsedByOutputDs: string[] = [];

  dataSources && Object.keys(dataSources).forEach(dsId => {
    if(dataSources[dsId] && !dataSources[dsId].isOutputFromWidget && dataSources[dsId].originDataSources && dataSources[dsId].originDataSources.length > 0){
      dssUsedByOutputDs = dssUsedByOutputDs.concat(dataSources[dsId].originDataSources.asMutable().map(useDs => useDs.dataSourceId));
    }
  });

  return dssUsedByOutputDs;
}

function getDssUsedByMessage(): string[]{
  const appConfig = dataComponentsUtils.getAppConfig();
  const messageJsons = appConfig && appConfig.messageConfigs ? Object.keys(appConfig.messageConfigs).map(mId => appConfig.messageConfigs[mId]) : [];
  const messageUsedDss = getUsedDssFromUseDataSources(messageJsons as ImmutableObject<{useDataSources: UseDataSource[]}>[]);

  return messageUsedDss;
}

function getDssUsedByWidget(): string[]{
  const widgetJsons = appConfigUtils.getAllWidgets(dataComponentsUtils.getAppConfig()).filter((w: IMWidgetJson) => w.useDataSourcesEnabled !== false);
  const widgetUsedDss = getUsedDssFromUseDataSources(widgetJsons as ImmutableObject<{useDataSources: UseDataSource[]}>[]);

  return widgetUsedDss;
}

function getUsedDssFromUseDataSources <T extends ImmutableObject<{useDataSources: UseDataSource[]}>>(jsonsWithUseDss: T[]): string[]{
  if(!jsonsWithUseDss){
    return [];
  }

  let usedDss: string[] = [];

  jsonsWithUseDss.forEach((j: T) => j.useDataSources &&
    j.useDataSources.forEach((u: IMUseDataSource) => {
      usedDss = usedDss.concat(u.dataSourceId);
    })
  );

  return usedDss;
}

function getUsedFields(ds: DataSource): string[]{
  if(!ds){
    return [];
  }

  const fieldsUsedByDs = (ds.dataSourceJson && ds.dataSourceJson.useFields && ds.dataSourceJson.useFields.asMutable()) || [];
  const fieldsUsedByWidget = getFieldsUsedByWidget(ds.id);
  const fieldsUsedByMessage = getFieldsUsedByMessage(ds.id);
  const fieldsUsedByOutputDs = getFieldsUsedByOutputDs(ds);

  const allUsedFields = fieldsUsedByDs.concat(fieldsUsedByWidget).concat(fieldsUsedByMessage).concat(fieldsUsedByOutputDs);
  return Array.from(new Set(allUsedFields));
}

function getFieldsUsedByOutputDs(originDs: DataSource): string[]{
  if(!originDs){
    return [];
  }
  const outputDss = dataComponentsUtils.getOutputDssFromOriginDs(originDs);
  const orignFieldsArrUsedByOutputDss: string[][] = outputDss.map(ds => getOriginFieldsUsedByOneOutputDs(originDs, ds));
  const originFieldsUsedByOutputDss: string[] = Array.prototype.concat.apply([], orignFieldsArrUsedByOutputDss);

  return Array.from(new Set(originFieldsUsedByOutputDss));
}

function getOriginFieldsUsedByOneOutputDs(originDs: DataSource, outputDs: DataSource): string[]{
  if(!outputDs || !originDs){
    return [];
  }
  const outputFieldsUsedByWidgets = getFieldsUsedByWidget(outputDs.id);
  const outputDsSchema: IMDataSourceSchema = outputDs.getSchema();
  const outputDsFieldsSchema: ImmutableObject<{ [jimuName: string]: FieldSchema }> = outputDsSchema && outputDsSchema.fields;

  let originFieldsUsedByOutputDs: string[] = [];
  let field: IMFieldSchema;
  let originFieldsUsedByOneOutputField: string[];
  let areMultipleOrigins: boolean; // whether `originFields` in field schema includes multiple data sources

  outputFieldsUsedByWidgets.forEach(jimuName => {
    field = outputDsFieldsSchema[jimuName];
    originFieldsUsedByOneOutputField = (field && field.originFields && field.originFields.asMutable()) || [];

    if(originFieldsUsedByOneOutputField.length > 0){
      // TODO: unsafe, if there is a field named 'datasource_1.field1', the method will recognized it as multiple data sources
      areMultipleOrigins = originFieldsUsedByOneOutputField.some(jimuName => jimuName.split(`${originDs.id}.`).length > 1);

      if(areMultipleOrigins){
        originFieldsUsedByOneOutputField = originFieldsUsedByOneOutputField.filter(dsIdJimuName => dsIdJimuName.split(`${originDs.id}.`).length > 1)
          .map(dsIdJimuName => dsIdJimuName.split(`${originDs.id}.`)[1]);
      }
    }else{
      originFieldsUsedByOneOutputField = [jimuName];
    }

    originFieldsUsedByOutputDs = originFieldsUsedByOutputDs.concat(originFieldsUsedByOneOutputField);
  });

  return originFieldsUsedByOutputDs;
}

function getFieldsUsedByMessage(dsId: string): string[]{
  const appConfig = dataComponentsUtils.getAppConfig();
  const messageJsons = appConfig && appConfig.messageConfigs ? Object.keys(appConfig.messageConfigs).map(mId => appConfig.messageConfigs[mId]) : [];
  const fieldsUsedByMessage = getUsedFieldsFromUseDataSources(messageJsons as ImmutableObject<{useDataSources: UseDataSource[]}>[], dsId);

  return fieldsUsedByMessage;
}

function getFieldsUsedByWidget(dsId: string): string[]{
  const widgetJsons = appConfigUtils.getAllWidgets(dataComponentsUtils.getAppConfig()).filter((w: IMWidgetJson) => w.useDataSourcesEnabled !== false);
  const widgetUsedFields = getUsedFieldsFromUseDataSources(widgetJsons as ImmutableObject<{useDataSources: UseDataSource[]}>[], dsId);

  return widgetUsedFields;
}

function getUsedFieldsFromUseDataSources<T extends ImmutableObject<{useDataSources: UseDataSource[]}>>(jsonsWithUseDss: T[], dsId: string): string[]{
  if(!jsonsWithUseDss){
    return [];
  }

  let usedFields = [];

  jsonsWithUseDss.forEach((j: T) => j.useDataSources &&
    j.useDataSources.forEach((u: IMUseDataSource) => {
      if(u.fields && u.dataSourceId && u.dataSourceId === dsId){
        usedFields = usedFields.concat(u.fields);
      }
    })
  );

  return usedFields;
}

function getAllWidgetsArray(widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>): IMWidgetJson[]{
  if(!widgets){
    return [];
  }

  let w = [];
  Object.keys(widgets).forEach(wId => {
    w.push(widgets[wId]);
  });

  return w;
}

export function getAllDsUsedWidgets(dsId: string, allWidgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>): IMWidgetJson[]{
  if(!dsId || !allWidgets){
    return [];
  }
  const dsUsedWidgets = dataComponentsUtils.getDsUsedWidgets(dsId, allWidgets);
  const childDsUsedWidgets = getAllWidgetsArray(allWidgets).filter((w: IMWidgetJson) =>
    w && w.useDataSources && w.useDataSources.some(u => u.rootDataSourceId && u.rootDataSourceId === dsId)
  );

  const allDsUsedWidgetIds = Array.from(new Set(dsUsedWidgets.concat(childDsUsedWidgets).map(w => w.id)));
  const allDsUsedWidgets = allDsUsedWidgetIds.map(wId => allWidgets[wId]);

  return allDsUsedWidgets;
}

// whether every child data source of schema are mapped, including grandchild data source of schema
export function getWhetherMappingIsDone(schema: DataSourceSchema, mappedSchema: DataSourceSchema): boolean{
  if(!schema || !mappedSchema){
    return false;
  }
  if(getWhetherSchemaIsSet(schema)){
    return Object.keys(schema.childSchemas).every(id => getWhetherMappingIsDone(schema.childSchemas[id], mappedSchema.childSchemas[id]));
  }
  return schema.fields && Object.keys(schema.fields).every(id => !!(mappedSchema.fields && mappedSchema.fields[id]));
}

// whether every child data source of schema are mapped, not including grandchild data source of schema
export function getWhetherChildMappingIsDone(schema: DataSourceSchema, mappedSchema: DataSourceSchema): boolean{
  if(!schema || !mappedSchema){
    return false;
  }
  if(getWhetherSchemaIsSet(schema)){
    return Object.keys(schema.childSchemas).every(id => !!(mappedSchema.childSchemas && mappedSchema.childSchemas[id]));
  }
  return schema.fields && Object.keys(schema.fields).every(id => !!(mappedSchema.fields && mappedSchema.fields[id]));
}

export function getWhetherSchemaIsSet(schema: DataSourceSchema): boolean{
  if(!schema){
    return false;
  }
  return !!schema.childSchemas;
}

export function getDsSchema(ds: DataSource): DataSourceSchema{
  if(!ds || !ds.getSchema){
    return null;
  }

  return ds.getSchema() as unknown;
}

export function getMappedDsJson(curDsJson: IMDataSourceJson, newDsJson: IMDataSourceJson, schema: DataSourceSchema): IMDataSourceJson{
  let mappedDsJson = newDsJson.merge({});

  mappedDsJson = mappedDsJson.set('id', curDsJson.id);
  mappedDsJson = mappedDsJson.set('schema', Immutable(schema));

  return mappedDsJson;
}

export function getElementPosition(element: HTMLElement): {x: number, y: number}{
  if(!element){
    return null;
  }
  let x = element.offsetLeft;
  let y = element.offsetTop;
  let parent = element.offsetParent as HTMLElement;

  while(parent !== null){
    x += parent.offsetLeft;
    y += parent.offsetTop;

    parent = parent.offsetParent as HTMLElement;
  }

  return {x, y};
}

export function isObject(o): boolean{
  return Object.prototype.toString.call(o) === '[object Object]';
}

export function traverseSchema(parentSchema: DataSourceSchema, schema: DataSourceSchema, configParentSchema: DataSourceSchema){
  if(!getWhetherSchemaIsSet(parentSchema)){
    return;
  }
  if(!configParentSchema.childSchemas){
    configParentSchema.childSchemas = {};
  }
  for(let i in parentSchema.childSchemas){
    if(isObject(parentSchema.childSchemas[i]) && parentSchema.childSchemas[i].childId === schema.childId){
      configParentSchema.childSchemas[i] = schema;
    }else{
      if(isObject(parentSchema.childSchemas[i]) && getWhetherSchemaIsSet(parentSchema.childSchemas[i] as DataSourceSchema)){
        if(!configParentSchema.childSchemas[i]){
          configParentSchema.childSchemas[i] = {};
        }
        traverseSchema(parentSchema.childSchemas[i] as DataSourceSchema, schema, configParentSchema.childSchemas[i]);
      }
    }
  }
}

export function  getAllChildDss(rootDs: DataSource): DataSource[]{
  let allChildDss = [];

  if(rootDs && rootDs.id && rootDs.dataSourceJson && rootDs.isDataSourceSet){
    traverseGetAllChildDss(rootDs, allChildDss);
  }

  return allChildDss;
}

export function traverseGetAllChildDss(parentDs: DataSource, childDss: DataSource[]){
  if(!parentDs.isDataSourceSet){
    return;
  }
  parentDs.getChildDataSources().forEach((ds: DataSource) => {
    if(!ds || !ds.dataSourceJson || !ds.id){
      return;
    }
    childDss.push(ds);
    traverseGetAllChildDss(ds, childDss);
  });
}

export function getMappedSchemaFromNewSchema(curSchema: DataSourceSchema, newSchema: DataSourceSchema, curConfigSchema: IMDataSourceSchema): DataSourceSchema{
  let mappedSchema: DataSourceSchema = {};
  if(!curSchema || !newSchema){
    if(curSchema && getWhetherSchemaIsSet(curSchema)){
      mappedSchema.childSchemas = {};
    }
    return mappedSchema;
  }

  if(getWhetherSchemaIsSet(curSchema) && getWhetherSchemaIsSet(newSchema)){
    let childId;
    let curConfigChildSchema;
    mappedSchema = {
      childSchemas: {}
    };
    Object.keys(curSchema.childSchemas).forEach(jimuChildId => {
      childId = curSchema.childSchemas[jimuChildId].childId;
      curConfigChildSchema = curConfigSchema && curConfigSchema.childSchemas && curConfigSchema.childSchemas[jimuChildId];

      mappedSchema.childSchemas[jimuChildId] = newSchema.childSchemas[childId] ?
        getMappedSchemaFromNewSchema(curSchema.childSchemas[jimuChildId], newSchema.childSchemas[childId], curConfigChildSchema)
        : null;
    });
  }else if(!getWhetherSchemaIsSet(curSchema) && !getWhetherSchemaIsSet(newSchema)){
    mappedSchema = {...newSchema, ...{fields: getMappedFieldsFromNewSchema(curSchema, newSchema, curConfigSchema), jimuChildId: curSchema.jimuChildId}};
  }else{
    console.error('types of data sources are not matched');
  }

  return mappedSchema;
}

export function getMappedFieldsFromNewSchema(curSchema: DataSourceSchema, newSchema: DataSourceSchema, curConfigSchema: IMDataSourceSchema): {[jimuName: string]: FieldSchema}{
  let mappedFields = {};
  if(!curSchema || !curSchema.fields || !newSchema || !newSchema.fields){
    return mappedFields;
  }

  let jimuName;
  let name;
  let curField;
  let newField;
  Object.keys(curSchema.fields).forEach(j => {
    curField = curSchema.fields[j];
    jimuName = curField.jimuName;
    name = curField.name;
    newField = newSchema.fields[name];
    if(newField && newField.esriFieldType === curField.esriFieldType){
      mappedFields[jimuName] = getMappedFieldFromNewField(curField, newField, curConfigSchema);
    }
  });

  return mappedFields;
}

export function getMappedFieldFromNewField(curField: FieldSchema, newField: FieldSchema, curConfigSchema: IMDataSourceSchema): FieldSchema{
  let mappedField: FieldSchema;

  if(curConfigSchema && curConfigSchema.fields && curConfigSchema.fields[curField.jimuName]){
    mappedField = curConfigSchema.asMutable({deep: true}).fields[curField.jimuName];
    mappedField.name = newField.name;
  }else{
    mappedField = getFieldWithoutAlias({...newField});
    mappedField.jimuName = curField.jimuName;
  }

  return mappedField;
}

export function getFieldWithoutAlias(field: FieldSchema): FieldSchema{
  if(!field){
    return null;
  }

  let f = {...field};
  if(f.alias){
    delete f.alias;
  }

  return f;
}

export function getWhetherJimuChildIdInSchema(jimuChildId: string, schema: DataSourceSchema): boolean{
  if(!jimuChildId || !schema || !schema.childSchemas){
    return false;
  }

  return Object.keys(schema.childSchemas).some(id => id === jimuChildId);
}

export function getWhetherChildIdInSchema(childId: string, schema: DataSourceSchema){
  if(!childId || !schema || !schema.childSchemas){
    return false;
  }

  return Object.keys(schema.childSchemas).some(id => schema.childSchemas[id].childId === childId);
}

export function getWhetherNameInSchema(name: string, schema: DataSourceSchema): boolean{
  if(!name || !schema || !schema.fields){
    return false;
  }
  return Object.keys(schema.fields).some(jimuName => schema.fields[jimuName].name === name);
}

export function getWhetherJimuNameInSchema(jimuName: string, schema: DataSourceSchema): boolean{
  if(!jimuName || !schema || !schema.fields){
    return false;
  }
  return !!schema.fields[jimuName];
}

export function getUniqueDsIdFromSchema(jimuChildId: string, schema: DataSourceSchema): number{
  let id = 0;
  if(!schema || !jimuChildId){
    return id;
  }

  let name;
  let jimuChildIdWithoutAutoMappingId = jimuChildId.split(autoMappingSymbol)[0];
  Object.keys(schema.childSchemas).forEach(jimuChildId => {
    name = jimuChildId.split(autoMappingSymbol) || [];
    if(name.length > 1 && name[0] === jimuChildIdWithoutAutoMappingId){
      if(parseInt(name[1]) > id){
        id = parseInt(name[1]);
      }
    }
  });

  return id + 1;
}

export function getUniqueFieldIdFromSchema(jimuName: string, schema: DataSourceSchema): number{
  let id = 0;
  if(!schema || !schema.fields){
    return id;
  }

  let name;
  let jimuNameWithoutAutoMappingId = jimuName.split(autoMappingSymbol)[0];
  Object.keys(schema.fields).forEach(j => {
    name = schema.fields[j].jimuName ? schema.fields[j].jimuName.split(autoMappingSymbol) : [];
    if(name.length > 1 && name[0] === jimuNameWithoutAutoMappingId){
      if(parseInt(name[1]) > id){
        id = parseInt(name[1]);
      }
    }
  });

  return id + 1;
}

export function getAutoMappedSetSchema(mappedSchema: DataSourceSchema, childSchema: DataSourceSchema): DataSourceSchema{
  if(!mappedSchema || !childSchema){
    return null;
  }

  let autoMappedSchema = {...mappedSchema};
  let jimuChildId = childSchema.jimuChildId;
  let childId = childSchema.childId;

  let newJimuChildId;
  let newChildDsSchema;
  let mappedActualChildSchema;
  let mappedActualChildSchemaJimuChildIds;

  // auto map `childSchema`
  if(getWhetherJimuChildIdInSchema(jimuChildId, mappedSchema) && !getWhetherChildIdInSchema(childId, mappedSchema)){
    // if one child of new external data will be overwritten by mapping, need to save the child to mapped schema
    newJimuChildId = `${jimuChildId}${autoMappingSymbol}${getUniqueDsIdFromSchema(jimuChildId, mappedSchema as DataSourceSchema)}`;

    if(getWhetherSchemaIsSet(childSchema)){
      newChildDsSchema = {...childSchema, jimuChildId: newJimuChildId};
    }else{
      // when mapping a data source to a new external data (such as a webmap from arcgis online),
      // fields in config should be an empty object
      newChildDsSchema = {...childSchema, fields: {}, jimuChildId: newJimuChildId};
    }

    autoMappedSchema.childSchemas[newJimuChildId] = getMappedSchemaFromNewSchema(newChildDsSchema, newChildDsSchema, null);
  }

  // auto map child data sources of `childSchema`
  mappedActualChildSchemaJimuChildIds = Object.keys(mappedSchema.childSchemas).filter(j => mappedSchema.childSchemas[j].childId === childId);
  mappedActualChildSchemaJimuChildIds.forEach(id => {
    mappedActualChildSchema = mappedSchema.childSchemas[id];
    if(mappedActualChildSchema){
      if(getWhetherSchemaIsSet(childSchema) && getWhetherSchemaIsSet(mappedActualChildSchema)){
        Object.keys(childSchema.childSchemas).forEach(j => {
          autoMappedSchema.childSchemas[id] =
            getAutoMappedSetSchema(mappedActualChildSchema, childSchema.childSchemas[j]);
        });
      }else if(!getWhetherSchemaIsSet(childSchema) && !getWhetherSchemaIsSet(mappedActualChildSchema)){
        Object.keys((childSchema as DataSourceSchema).fields).forEach(jimuName => {
          autoMappedSchema.childSchemas[id] =
            getAutoMappedSchema(mappedActualChildSchema as DataSourceSchema, (childSchema as DataSourceSchema).fields[jimuName]);
        });
      }else{
        console.error('types of data sources are not matched');
      }
    }
  });

  return autoMappedSchema;
}

export function getAutoMappedSchema(mappedSchema: DataSourceSchema, field: FieldSchema): DataSourceSchema{
  let autoMappedSchema = {...mappedSchema};
  let jimuName = field.jimuName;
  let newJimuName;
  let newField;
  if(getWhetherJimuNameInSchema(jimuName, autoMappedSchema) && !getWhetherNameInSchema(field.name, autoMappedSchema)){
    // if one field of new external data will be overwritten by mapping, need to save the field to mapped schema
    newJimuName = `${jimuName}${autoMappingSymbol}${getUniqueFieldIdFromSchema(jimuName, autoMappedSchema)}`;
    newField = {...field, jimuName: newJimuName};
    autoMappedSchema.fields[newJimuName] = getMappedFieldFromNewField(newField, newField, null);
  }
  return autoMappedSchema;
}

export function getSortedKeys(obj: object): string[]{
  return Object.keys(obj).sort((ds1, ds2) => ds1.localeCompare(ds2));
}

export function getSortedArrayByLabel<T extends {label: string}>(arr: T[]): T[]{
  return arr.sort((ds1, ds2) => ds1.label.localeCompare(ds2.label));
}

export function getIsLastFieldMapping(childDs: DataSource, ds: DataSource, mappingHistory: DataSource[]){
  const schema = getUsedDsSchema(ds);
  const childSchema = getDsSchema(childDs);
  if(getWhetherChildIdInSchema(childSchema.childId, schema)){
    return Object.keys(schema.childSchemas).filter(jimuChildId => schema.childSchemas[jimuChildId].childId !== childSchema.childId)
      .every(jimuChildId => mappingHistory.some(mappedDs => getDsSchema(mappedDs).childId === schema.childSchemas[jimuChildId].childId));
  }else{
    console.warn('data source is not child of ', ds);
    return false;
  }
}

export function renameDataSource(newLabel: string, ds: DataSource){
  let dsJson: IMDataSourceJson;
  const isChild = !!ds.parentDataSource;

  if(!isChild){
    dsJson = ds.dataSourceJson.set('label', newLabel);
  }else{
    const rootDs = ds.getRootDataSource();
    let rootSchema = getDsSchema(rootDs);
    let schema = getDsSchema(ds);
    let configRootSchema = {};
    schema.label = newLabel;
    traverseSchema(rootSchema, schema, configRootSchema);

    dsJson = rootDs.dataSourceJson.set('schema', configRootSchema);
  }

  getAppConfigAction().editDataSource(dsJson).exec();
  DataSourceManager.getInstance().createDataSource(dsJson);
}
