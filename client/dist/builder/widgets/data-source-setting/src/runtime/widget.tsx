/** @jsx jsx */
import {BaseWidget, AllWidgetProps, IMDataSourceJson, DataSource, jsx, SessionManager, IMDataSourceInfo,
  ImmutableObject, IMState, esri, DataSourceJson, WidgetJson, MessageJson, DataSourceManager} from 'jimu-core';
import {Modal, Icon, Button} from 'jimu-ui';
import {ExternalDataSourceSelector, dataComponentsUtils} from 'jimu-ui/data-source-selector';
import {ItemDetail} from 'jimu-ui/item-selector';
import {getAppConfigAction} from 'jimu-for-builder';
import {ArcGISDataSourceTypes} from 'jimu-arcgis';

import {IItem} from '@esri/arcgis-rest-types';
import {ISearchOptions} from '@esri/arcgis-rest-portal';

import DsList from './components/list/ds-list';
import FieldList from './components/list/field-list';
import DsListItem from './components/list/ds-list/ds-list-item';
import DsListErrorItem from './components/list/ds-list/ds-list-error-item';
import Mapping from './components/mapping';

import {getElementPosition, getDsSchema, getSortedArrayByLabel} from './utils';
import defaultMessages from './translations/default';

import {getStyle} from './style';

const IconArrowLeft = require('jimu-ui/lib/icons/arrow-left.svg');
const IconAdd = require('jimu-ui/lib/icons/add-12.svg');

interface State{
  isDataSourceInited: boolean;
  isExternalDsShown: boolean;
  mappingDs: DataSource;
  dsHistory: DataSource[];
  item: IItem;
  moreOptionsDsId: string;
}
interface ExtraProps{
  dataSources: ImmutableObject<{ [dsId: string]: DataSourceJson }>;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  messages: ImmutableObject<{ [messageConfigId: string]: MessageJson }>;
  dataSourcesInfo: ImmutableObject<{[dsId: string]: IMDataSourceInfo}>;
}
export default class Widget extends BaseWidget<AllWidgetProps<{}> & ExtraProps, State>{
  __unmount = false;
  externalDsStyle = {width: '100%', height: '100%', maxWidth: '5000px', margin: 0};
  rootDom: HTMLElement;
  rootPosition: {x: number, y: number};

  static mapExtraStateProps = (state: IMState): ExtraProps => {
    return {
      dataSources: state.appStateInBuilder && state.appStateInBuilder.appConfig && state.appStateInBuilder.appConfig.dataSources,
      widgets: state.appStateInBuilder && state.appStateInBuilder.appConfig && state.appStateInBuilder.appConfig.widgets,
      messages: state.appStateInBuilder && state.appStateInBuilder.appConfig && state.appStateInBuilder.appConfig.messageConfigs,
      dataSourcesInfo: state.appStateInBuilder && state.appStateInBuilder.dataSourcesInfo
    }
  };

  constructor(props){
    super(props);
    this.state = {
      isDataSourceInited: dataComponentsUtils.getWhetherDataSourceIsInited(this.props.dataSourcesInfo),
      isExternalDsShown: false,
      mappingDs: null,
      item: null,
      dsHistory: [],
      moreOptionsDsId: null
    };
  }

  componentDidMount(){
    this.__unmount = false;
    this.rootPosition = getElementPosition(this.rootDom);
  }
  componentDidUpdate(prevProps: AllWidgetProps<{}> & ExtraProps){
    if(this.props.dataSources !== prevProps.dataSources || this.props.dataSourcesInfo !== prevProps.dataSourcesInfo){
      this.setState({
        isDataSourceInited: dataComponentsUtils.getWhetherDataSourceIsInited(this.props.dataSourcesInfo)
      });
    }
  }
  componentWillUnmount(){
    this.__unmount = true;
  }

  addToDsHistory = (ds: DataSource) => {
    this.setState({
      dsHistory: this.state.dsHistory.concat(ds)
    });
  }

  removeFromDsHistory = () => {
    let newDsHistory = [];
    if(this.state.dsHistory.length > 0){
      newDsHistory = this.state.dsHistory.slice(0, this.state.dsHistory.length - 1);
    }

    this.setState({
      dsHistory: newDsHistory,
      item: null
    });
  }

  getItemId = (ds: DataSource): string => {
    switch (ds.type){
      case ArcGISDataSourceTypes.WebScene:
      case ArcGISDataSourceTypes.WebMap:
        return ds.dataSourceJson.itemId;
      case ArcGISDataSourceTypes.FeatureLayer:
        return getDsSchema(ds).childId;
      default:
        return null;
    }
  }

  getSearch = (ds: DataSource): ISearchOptions => {
    const itemId = this.getItemId(ds);
    if(!itemId){
      return null;
    }
    return {
      q: `id: ${itemId}`,
      authentication: SessionManager.getInstance().getMainSession()
    };
  }

  getNewDsById = (dsId: string): DataSource => {
    if(!dsId){
      return null;
    }
    return DataSourceManager.getInstance().getDataSource(dsId);
  }

  getListUsedDs = (ds: DataSource): DataSource => {
    if(!ds){
      return null;
    }
    // update data source of ds-list and field-list after data sources are inited
    return this.state.isDataSourceInited ? this.getNewDsById(ds.id) : ds;
  }

  onBackClicked = () => {
    this.removeFromDsHistory();
  }

  onSelectDataFinished = (dsJsons: IMDataSourceJson[]) => {
    if(!dsJsons){
      this.setState({isExternalDsShown: false});
      return;
    }
    getAppConfigAction().addDataSources(dsJsons).exec();
    dsJsons.forEach(dsJson => DataSourceManager.getInstance().createDataSource(dsJson));

    this.setState({isExternalDsShown: false, isDataSourceInited: false});
  }

  onSelectDataCanceled = () => {
    this.setState({isExternalDsShown: false});
  }

  onMappingIconClick = (ds: DataSource) => {
    this.setState({mappingDs: ds});
  }

  onShowDetailClicked = (ds: DataSource) => {
    const search = this.getSearch(ds);
    if(!search){
      return;
    }
    this.setState({isDataSourceInited: false});
    search && esri.restPortal.searchItems(search).then(items => {
      if(!this.__unmount){
        this.setState({
          item: items.results[0],
          isDataSourceInited: true
        });
      }
    }, e => {
      if(!this.__unmount){
        console.error(e);
        this.setState({isDataSourceInited: true});
      }
    });
  }

  onCloseDetailClicked = () => {
    this.setState({
      item: null
    });
  }

  onMoreIconClick = (dsId: string) => {
    this.setState({
      moreOptionsDsId: this.state.moreOptionsDsId === dsId ? null : dsId
    });
  }

  onToggleExternalDs = () => {
    this.setState({isExternalDsShown: !this.state.isExternalDsShown});
  }

  resetSelectedMoreOptions = e => {
    if(e.currentTarget.className && e.currentTarget.className.indexOf && e.currentTarget.className.indexOf('widget-ds-setting') > -1){
      this.setState({
        moreOptionsDsId: null
      });
    }
  }

  hideMapping = () => {
    this.setState({mappingDs: null});
  }

  showExternalDs = () => {
    this.setState({isExternalDsShown: true});
  }

  ExternalDs = <ExternalDataSourceSelector portalUrl={this.props.portalUrl} onCancel={this.onSelectDataCanceled}
    onFinish={this.onSelectDataFinished} />

  AddDataSourceBtn = <div className="d-flex mt-3 mx-3 mb-1">
    <Button type="primary" className="text-truncate flex-grow-1 text-center add-data" onClick={this.showExternalDs}>
      <Icon icon={IconAdd} size={12} />
      {this.props.intl.formatMessage({id: 'addData', defaultMessage: defaultMessages.addData})}
    </Button>
  </div>

  render(){
    return (
      <div css={getStyle(this.props.theme)} className="w-100 h-100">
        <div className="jimu-widget widget-ds-setting setting-pane bg-light-300" ref={d => this.rootDom = d} onClick={this.resetSelectedMoreOptions}>

          <div className="jimu-builder-panel--header d-flex flex-row text-data-600 title">
            <div className="flex-grow-1 mb-0">
              {this.props.intl.formatMessage({id: 'element', defaultMessage: defaultMessages.data})}
            </div>
          </div>

          {
            this.state.mappingDs ?
            <Mapping portalUrl={this.props.portalUrl} ds={this.state.mappingDs} widgets={this.props.widgets}
              dispatch={this.props.dispatch} hideMapping={this.hideMapping} intl={this.props.intl}
              dataSources={this.props.dataSources} messages={this.props.messages}
            /> :

            <div className="list-container">

              {
                this.state.dsHistory.length > 0 ?
                  <div className="border-color-gray-300 jimu-widget-setting--header">
                    <div className="text-dark mt-2 ds-back" onClick={this.onBackClicked}>
                      <Icon icon={IconArrowLeft} />
                      <span className="align-middle ml-2">
                        {this.props.intl.formatMessage({id: 'back', defaultMessage: defaultMessages.back})}
                      </span>
                    </div>
                  </div> :
                <div className="w-100">
                  {
                    this.AddDataSourceBtn
                  }
                </div>
              }

              {
                this.props.dataSources && this.state.dsHistory.length === 0 ?
                <div className="root-ds-list">
                  {
                    getSortedArrayByLabel(
                      Object.keys(this.props.dataSources)
                        .filter(dsId => !this.props.dataSources[dsId].isOutputFromWidget && !this.props.dataSources[dsId].parentDataSource && this.props.dataSources[dsId].label)
                        .map(dsId => this.props.dataSources[dsId])
                    ).map((dsJson, i) => {
                      if(dsJson && dsJson.id){
                        const ds = DataSourceManager.getInstance().getDataSource(dsJson.id);
                        if(ds && ds.id){
                          return <DsListItem ds={ds} key={i} onDataSourceItemClick={this.addToDsHistory} intl={this.props.intl} widgets={this.props.widgets}
                          onMappingIconClick={this.onMappingIconClick} isMoreOptionsShown={this.state.moreOptionsDsId === ds.id}
                          onMoreIconClick={this.onMoreIconClick} isDataSourceInited={this.state.isDataSourceInited} theme={this.props.theme} />
                        }else{
                          return <DsListErrorItem dsJson={dsJson} key={i} intl={this.props.intl} widgets={this.props.widgets} isMoreOptionsShown={this.state.moreOptionsDsId === dsJson.id}
                          isDataSourceInited={this.state.isDataSourceInited} onMoreIconClick={this.onMoreIconClick} dataSources={this.props.dataSources} theme={this.props.theme} />
                        }
                      }
                      return null
                    })
                  }
                </div>
                : null
              }

              {
                this.props.dataSources && this.state.dsHistory.length > 0 && !this.state.dsHistory[this.state.dsHistory.length - 1].isDataSourceSet ?
                <FieldList onShowDetailClicked={this.onShowDetailClicked} isDataSourceInited={this.state.isDataSourceInited}
                  ds={this.getListUsedDs(this.state.dsHistory[this.state.dsHistory.length - 1])} widgets={this.props.widgets}
                  onMappingIconClick={this.onMappingIconClick} intl={this.props.intl} portalUrl={this.props.portalUrl} theme={this.props.theme}
                /> : null
              }

              {
                this.props.dataSources && this.state.dsHistory.length > 0 && this.state.dsHistory[this.state.dsHistory.length - 1].isDataSourceSet ?
                <DsList onDsItemClicked={this.addToDsHistory} portalUrl={this.props.portalUrl} dispatch={this.props.dispatch}
                  ds={this.getListUsedDs(this.state.dsHistory[this.state.dsHistory.length - 1])} theme={this.props.theme}
                  onMappingIconClick={this.onMappingIconClick} isDataSourceInited={this.state.isDataSourceInited}
                  onShowDetailClicked={this.onShowDetailClicked} intl={this.props.intl} widgets={this.props.widgets}
                /> : null
              }
            </div>
          }

          {
            !this.state.isDataSourceInited ?
            <div className="jimu-small-loading"></div> : null
          }

        </div>

        {
          this.state.item && this.rootDom ?
          <ItemDetail item={this.state.item} portalUrl={this.props.portalUrl} onClose={this.onCloseDetailClicked}
          style={{
            position: 'fixed',
            top: '50px',
            bottom: 0,
            width: '300px',
            left: `${this.rootPosition.x + this.rootDom.offsetWidth}px`
          }}/> : null
        }

        {
          this.state.isExternalDsShown ?
          <Modal isOpen={this.state.isExternalDsShown} style={this.externalDsStyle} toggle={this.onToggleExternalDs} contentClassName="border-0 h-100"
            className="widget-ds-setting-add-data-popup">
            {
              this.ExternalDs
            }
          </Modal> : null
        }

      </div>
    );
  }
}
