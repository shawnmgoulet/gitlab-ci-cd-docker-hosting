import {React, DataSource, IntlShape, ImmutableObject, WidgetJson, ThemeVariables} from 'jimu-core';
import {Tabs, Tab} from 'jimu-ui';

import FieldListFields from './field-list-fields';
// import FieldListSettings from './field-list-settings';
import ListWidgets from '../common/list-widgets';
import ListInfo from '../common/list-info';

import defaultMessages from '../../../translations/default';

interface Props{
  ds: DataSource;
  portalUrl: string;
  isDataSourceInited: boolean;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  intl: IntlShape;
  theme: ThemeVariables;
  onShowDetailClicked: (ds: DataSource) => void;
  onMappingIconClick: (ds: DataSource) => void;
}

interface State{
}

export default class extends React.PureComponent<Props, State>{

  constructor(props){
    super(props);
  }

  render(){
    if(!this.props.ds){
      return <div className="m-2">
        {this.props.intl.formatMessage({id: 'noSupportedDataSource', defaultMessage: defaultMessages.noSupportedDataSource})}
      </div>;
    }

    return (
      <div className="field-list">

        <ListInfo ds={this.props.ds} onMappingIconClick={this.props.onMappingIconClick} widgets={this.props.widgets} theme={this.props.theme}
          onShowDetailClicked={this.props.onShowDetailClicked} intl={this.props.intl} portalUrl={this.props.portalUrl} />

        <Tabs underline>
          <Tab title={this.props.intl.formatMessage({id: 'fields', defaultMessage: defaultMessages.fields})} active={true}>
            <FieldListFields ds={this.props.ds} isDataSourceInited={this.props.isDataSourceInited} intl={this.props.intl}
              widgets={this.props.widgets}
            />
          </Tab>
          <Tab title={this.props.intl.formatMessage({id: 'widgets', defaultMessage: defaultMessages.widgets})}>
            <ListWidgets ds={this.props.ds} isDataSourceInited={this.props.isDataSourceInited} intl={this.props.intl}
              widgets={this.props.widgets}
            />
          </Tab>
          {/* <Tab title={this.props.intl.formatMessage({id: 'settings', defaultMessage: defaultMessages.settings})}>
            <FieldListSettings ds={this.props.ds} intl={this.props.intl}/>
          </Tab> */}
        </Tabs>
      </div>
    );
  }
}
