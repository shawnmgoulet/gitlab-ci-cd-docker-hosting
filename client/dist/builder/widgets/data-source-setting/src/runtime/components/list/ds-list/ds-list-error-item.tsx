import {React, IntlShape, ImmutableObject, IMDataSourceJson, WidgetJson, DataSourceJson, ThemeVariables} from 'jimu-core';
import {DataSourceErrorItem} from 'jimu-ui/data-source-selector';

import MoreOptions from '../common/list-more';

interface ItemProps{
  dsJson: IMDataSourceJson;
  isMoreOptionsShown: boolean;
  isDataSourceInited: boolean;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  dataSources: ImmutableObject<{ [dsId: string]: DataSourceJson }>;
  intl: IntlShape;
  theme: ThemeVariables;
  onMoreIconClick: (dsId: string) => void;
}
interface ItemState{

}
export default class extends React.PureComponent<ItemProps, ItemState>{
  rootDom: HTMLElement;

  constructor(props){
    super(props);
  }

  onRemoveItem = () => {
    this.props.onMoreIconClick(this.props.dsJson.id);
  }

  onMoreIconClick = () => {
    this.props.onMoreIconClick(this.props.dsJson.id);
  }

  render(){
    if(!this.props.dsJson){
      return null;
    }

    const isMoreIconShown = this.props.dataSources && Object.keys(this.props.dataSources).some(dsId => !this.props.dataSources[dsId].isOutputFromWidget && dsId === this.props.dsJson.id);
    return (
      <div className="m-3 list-item list-error-item" ref={d => {this.rootDom = d}}>

        <DataSourceErrorItem dataSourceJson={this.props.dsJson} dataSources={this.props.dataSources} isOriginalUrlShown={true}
          isMoreIconShown={isMoreIconShown} isRelatedWidgetsShown={true} onMoreIconClick={this.onMoreIconClick}
        />

        <MoreOptions isShown={this.props.isMoreOptionsShown} rootDom={this.rootDom} intl={this.props.intl}
          dsJson={this.props.dsJson} onRemoveItem={this.onRemoveItem} widgets={this.props.widgets} theme={this.props.theme}
        />

      </div>
    );
  }
}