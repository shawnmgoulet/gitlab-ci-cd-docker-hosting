/** @jsx jsx */
import {React, css, jsx, IntlShape, IMDataSourceJson, ImmutableObject, WidgetJson, polished, ThemeVariables, SerializedStyles, classNames, DataSourceManager} from 'jimu-core';
import {getAppConfigAction} from 'jimu-for-builder';
import {Popper} from 'jimu-ui';

import {getAllDsUsedWidgets} from '../../../utils';
import defaultMessages from '../../../translations/default';

import ListRemovePopup from './list-remove-popup';

interface Props{
  dsJson: IMDataSourceJson;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  isShown: boolean;
  rootDom: HTMLElement;
  intl: IntlShape;
  theme: ThemeVariables;
  offset?: [number, number];
  onRemoveItem?: () => void;
  onRenameItem?: () => void;
}

interface State{
  isRemoveOptionsShown: boolean;
  selectedWidgets: string[];
}

export default class extends React.PureComponent<Props, State>{

  constructor(props){
    super(props);
    this.state = {
      isRemoveOptionsShown: false,
      selectedWidgets: []
    }
  }

  onRemove = e => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();

    if(!this.props.onRemoveItem){
      return;
    }

    const dsId = this.props.dsJson && this.props.dsJson.id;
    const relatedWidgets = getAllDsUsedWidgets(dsId, this.props.widgets);

    if(relatedWidgets.length === 0){
      getAppConfigAction().removeDataSource(dsId).exec();
      DataSourceManager.getInstance().destroyDataSource(dsId);
    }else{
      this.setState({isRemoveOptionsShown: true});
    }
    this.props.onRemoveItem();
  }

  onRename = e => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();

    if(!this.props.onRenameItem){
      return;
    }

    this.props.onRenameItem();
  }

  hideRemovePopup = () => {
    this.setState({isRemoveOptionsShown: false});
  }

  getStyle = (theme: ThemeVariables): SerializedStyles => {
    return css`
      .ds-more-options{
        position: fixed;
        background-color: ${theme.colors.secondary};
        color: ${theme.colors.dark};
        z-index: 3;
        user-select: none;
        font-size: 13px;
        width: ${polished.rem(74)};
        border: 1px solid ${theme.colors.palette.light[600]};
        box-shadow: 0 0 6px 0 ${polished.rgba(theme.colors.white, 0.2)};
        margin-left: ${polished.rem(-32)};
        border-radius: 2px;
        .ds-more-option{
          height: ${polished.rem(26)};
          line-height: ${polished.rem(26)};
          border-bottom: 1px;
          cursor: pointer;
          padding-left: 10px;
          padding-right: 10px;
        }
        .ds-more-option:hover{
          background-color: ${theme.colors.primary};
        }
        .ds-more-option:active.ds-more-option:hover{
          background-color: ${theme.colors.palette.primary[600]};
        }
      }
    `
  }

  render(){
    if(!this.props.rootDom || !this.props.dsJson){
      return null;
    }

    return (
      <Popper  css={this.getStyle(this.props.theme)} reference={this.props.rootDom} open={true} placement="right-end" container={document && document.body}
        offset={this.props.offset || [-15, 0]} className={classNames({'d-none': !this.props.isShown})} zIndex={3}
      >
        <div className="ds-more-options py-2">
          {
            this.props.onRenameItem &&
            <div className="ds-more-option" onClick={this.onRename}>
              {this.props.intl.formatMessage({id: 'rename', defaultMessage: defaultMessages.rename})}
            </div>
          }
          {
            this.props.onRemoveItem &&
            <div className="ds-more-option" onClick={this.onRemove}>
              {this.props.intl.formatMessage({id: 'remove', defaultMessage: defaultMessages.remove})}
            </div>
          }

          <ListRemovePopup dsJson={this.props.dsJson} isShown={this.state.isRemoveOptionsShown} intl={this.props.intl}
            hideRemovePopup={this.hideRemovePopup} widgets={this.props.widgets} />
        </div>
      </Popper>
    );
  }
}
