import {React, DataSource, IntlShape, ImmutableObject, WidgetJson, ThemeVariables} from 'jimu-core';
import {Icon, Input, Link} from 'jimu-ui';
import {dataComponentsUtils} from 'jimu-ui/data-source-selector';

import ListMore from './list-more';

import {renameDataSource} from '../../../utils';
import defaultMessages from '../../../translations/default';

const IconMore = require('jimu-ui/lib/icons/more-12.svg');
// const IconExchange = require('jimu-ui/lib/icons/exchange.svg');

interface Props{
  ds: DataSource;
  portalUrl: string;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  intl: IntlShape;
  theme: ThemeVariables;
  onShowDetailClicked: (ds: DataSource) => void;
  onMappingIconClick?: (ds: DataSource) => void;
}

interface State{
  isMoreOptionsShown: boolean;
  isMappingDetailShown: boolean;
  isRenameShown: boolean;
  newName: string;
}

export default class extends React.PureComponent<Props, State>{
  rootDom: HTMLElement;
  renameInput: HTMLInputElement;

  constructor(props){
    super(props);
    this.state = {
      isMoreOptionsShown: false,
      isMappingDetailShown: false,
      isRenameShown: false,
      newName: this.props.ds ? this.props.ds.label || this.props.ds.id : ''
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State){
    if(prevState.isRenameShown !== this.state.isRenameShown && this.state.isRenameShown && this.renameInput){
      this.renameInput.focus();
      this.renameInput.select();
    }
    if(prevProps.ds !== this.props.ds){
      this.setState({
        newName: this.props.ds ? this.props.ds.label || this.props.ds.id : ''
      });
    }
  }

  onRootClicked = e => {
    if(e.currentTarget.className && e.currentTarget.className.indexOf && e.currentTarget.className.indexOf('ds-info') > -1){
      this.setState({
        isMoreOptionsShown: false
      });
    }
  }

  onRemoveItem = () => {
    this.setState({isMoreOptionsShown: false});
  }

  onShowDetailClicked = () => {
    this.props.onShowDetailClicked(this.props.ds);
  }

  onRenameItem = () => {
    this.setState({
      isMoreOptionsShown: false,
      isRenameShown: true
    });
  }

  onRename = () => {
    const newLabel = this.state.newName.trim();

    if(!newLabel){
      this.setState({isRenameShown: false});
      return;
    }

    renameDataSource(newLabel, this.props.ds);

    this.setState({isRenameShown: false});
  }

  onRenameChange = e => {
    this.setState({
      newName: e.target.value
    });
  }

  onMoreIconClick = e => {
    e.stopPropagation();
    this.setState({isMoreOptionsShown: !this.state.isMoreOptionsShown});
  }

  onMappingIconClick = () => {
    this.props.onMappingIconClick(this.props.ds);
  }

  render(){
    if(!this.props.ds){
      return <div className="m-2">
        {this.props.intl.formatMessage({id: 'noSupportedDataSource', defaultMessage: defaultMessages.noSupportedDataSource})}
      </div>;
    }
    const isChild = !!this.props.ds.parentDataSource;
    const itemDetailUrl = this.props.ds.dataSourceJson && this.props.ds.dataSourceJson.itemId && this.props.portalUrl ?
      `${this.props.portalUrl.replace(/\/sharing\/rest/, '').replace(/\/$/, '')}/home/item.html?id=${this.props.ds.dataSourceJson.itemId}` : null;
    return (
      <div className="p-3 ds-info" ref={d => this.rootDom = d} onClick={this.onRootClicked}>

        {
          this.state.isRenameShown ?
          <Input className="mr-2 flex-grow-1 text-truncate ds-label-input ds-label" ref={n => this.renameInput = n}
            onBlur={this.onRename} onKeyUp={this.onRename} onChange={this.onRenameChange} value={this.state.newName} /> :

          <div className="flex-grow-1 two-line-truncate ds-label" title={this.props.ds.label || this.props.ds.id} onClick={this.onShowDetailClicked}>
            {this.props.ds.label || this.props.ds.id}
          </div>
        }

        <div className="mt-3 d-flex align-items-end justify-content-between ds-thumbnail-type-label">
          <div className="d-flex align-items-center">
            <div className="d-flex align-items-center justify-content-center mr-1 flex-shrink-0 ds-thumbnail">
              <Icon icon={dataComponentsUtils.getDsIcon(this.props.ds.dataSourceJson)} className="text-dark" size="12"/>
            </div>
            <div className="d-flex ds-type">
              <div className="flex-shrink-0 text-dark-600 text-truncate ds-type-name">{dataComponentsUtils.getDsTypeString(this.props.ds.type, this.props.intl)}</div>
            </div>
          </div>

          <div className="d-flex">
            {
              !isChild ?
              <div className="ml-1 flex-shrink-0 p-2 ds-more">
                <span className="ds-more" onClick={this.onMoreIconClick}>
                  <Icon icon={IconMore} className="text-dark" />
                </span>
              </div> : null
            }
          </div>
        </div>

        <div className="mt-3 d-flex justify-content-between">
          <div className="border-light-500 text-truncate ds-origin-label" title={this.props.ds.label || this.props.ds.id}>
            {
              itemDetailUrl ?
              <Link to={itemDetailUrl} className="p-0 ds-origin-label-link" type="link" target="_blank">
                <span className="align-middle text-truncate ds-origin-label-link-text">{this.props.ds.label || this.props.ds.id}</span>
              </Link>
              : <span className="align-middle">{this.props.ds.label || this.props.ds.id}</span>
            }
          </div>

          {
            /* !isChild ?
            <div title="mapping" onClick={this.onMappingIconClick} className="ml-2 ds-mapping-icon">
              <Icon icon={IconExchange} className="text-dark" />
            </div> : null */
          }
        </div>

        <ListMore isShown={this.state.isMoreOptionsShown} rootDom={this.rootDom} intl={this.props.intl} widgets={this.props.widgets}
          dsJson={this.props.ds && this.props.ds.dataSourceJson} onRemoveItem={this.onRemoveItem} onRenameItem={this.onRenameItem}
          theme={this.props.theme} offset={[-55, 0]}
        />
      </div>
    );
  }
}
