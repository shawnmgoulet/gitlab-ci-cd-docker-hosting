import {React, IMDataSourceJson, DataSource, IMUseDataSource, IntlShape, Immutable,
  IMSqlExpression, OrderByOption} from 'jimu-core';
import {getAppConfigAction} from 'jimu-for-builder';
import {SqlExpressionBuilder, Sort} from 'jimu-ui/sql-expression-builder';
import {SqlExpressionMode} from 'jimu-ui/sql-expression-runtime';

import defaultMessages from '../../../translations/default';

interface Props{
  ds: DataSource;
  intl: IntlShape;
}

interface State{

}

export default class extends React.PureComponent<Props, State>{
  constructor(props){
    super(props);
    this.state = {

    };
  }

  getSingleUsedDs = (dsJson: IMDataSourceJson): IMUseDataSource => {
    if(!dsJson){
      return null;
    }

    return Immutable({
      dataSourceId: dsJson.id,
    });
  }

  onSqlExprBuilderChange = (sqlExprObj: IMSqlExpression) => {
    if(!this.props.ds || !this.props.ds.dataSourceJson){
      return;
    }
    getAppConfigAction().editDataSource(this.props.ds.dataSourceJson.setIn(['query', 'where'], sqlExprObj)).exec();
  }

  onSortChange = (sortData: Array<OrderByOption>, index?: number) => {
    if(!this.props.ds || !this.props.ds.dataSourceJson){
      return;
    }
    getAppConfigAction().editDataSource(this.props.ds.dataSourceJson.setIn(['query', 'orderBy'], sortData)).exec();
  }

  render(){
    if(!this.props.ds){
      return null;
    }

    const query = this.props.ds.dataSourceJson && this.props.ds.dataSourceJson.query;
    const where = query ? query.where : null;
    const orderBy = query ? query.orderBy : Immutable([]);

    return (
      <div className="p-2">
        <div className="mt-2">
          <h4>{this.props.intl.formatMessage({id: 'filter', defaultMessage: defaultMessages.filter})}</h4>
          <SqlExpressionBuilder
            selectedDs={this.props.ds}
            mode={SqlExpressionMode.Simple}
            config={where}
            onChange={this.onSqlExprBuilderChange}
          />
        </div>
        <div className="mt-2">
          <h4>{this.props.intl.formatMessage({id: 'sort', defaultMessage: defaultMessages.sort})}</h4>
          <Sort
            onChange={this.onSortChange}
            value={orderBy}
            useDataSource={this.getSingleUsedDs(this.props.ds.dataSourceJson)}
          />
        </div>
      </div>
    );
  }
}
