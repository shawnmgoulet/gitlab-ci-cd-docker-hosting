import {React, DataSource, ImmutableObject, IntlShape, WidgetJson} from 'jimu-core';
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Icon} from 'jimu-ui';
import {dataComponentsUtils} from 'jimu-ui/data-source-selector';

import {getSortedArrayByLabel} from '../../../utils';

const IconWarning = require('jimu-ui/lib/icons/warning.svg');
const IconLinks = require('jimu-ui/lib/icons/links.svg');

interface ItemProps{
  curDs: DataSource;
  newParentDs: DataSource;
  selectedNewDs: DataSource;
  isDone: boolean;
  intl: IntlShape;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  updateMappedSchema: (curDs: DataSource, newDs: DataSource) => void;
}
interface ItemState{
  newDss: DataSource[];
  isNewDsDropdownOpen: boolean;
}
export default class extends React.PureComponent<ItemProps, ItemState>{
  __unmount = false;
  constructor(props){
    super(props);
    this.state = {
      isNewDsDropdownOpen: false,
      newDss: null
    }
  }

  componentDidMount(){
    this.__unmount = false;
    this.setNewChildDss(this.props.newParentDs);
  }

  componentDidUpdate(prevProps){
    if(prevProps.newParentDs !== this.props.newParentDs){
      this.setNewChildDss(this.props.newParentDs);
    }
  }

  componentWillMount(){
    this.__unmount = true;
  }

  toggleDropdown = () => {
    this.setState({isNewDsDropdownOpen: !this.state.isNewDsDropdownOpen});
  }

  setNewChildDss = (parentDs: DataSource) => {
    if(parentDs){
      this.setState({
        newDss: parentDs.getChildDataSources()
      });
    }
  }

  getSelectedDsLabel = (ds: DataSource): string => {
    return ds && (ds.label || ds.id);
  }

  onSelectedNewDsChange = (childDs: DataSource) => {
    this.props.updateMappedSchema(this.props.curDs, childDs);
  }

  DsInfo = ({curDs, selectedNewDs}:
    {curDs: DataSource, selectedNewDs: DataSource}) => {

    if(!curDs){
      return null;
    }

    const relatedWidgets = dataComponentsUtils.getDsUsedWidgets(curDs.id, this.props.widgets);
    return (
      <div>
        <div className="d-flex align-items-center m-2">
          <div className="d-flex align-items-center justify-content-center mr-1 flex-shrink-0 border-light-500 ds-thumbnail">
            <Icon icon={dataComponentsUtils.getDsIcon(curDs.dataSourceJson)} className="text-dark" size="22"/>
          </div>

          <div className="flex-grow-1 text-truncate p-1" title={curDs.label || curDs.id}>{curDs.label || curDs.id}</div>

          {
            !this.props.isDone ?
            <div className="d-flex align-items-center justify-content-center mb-3">
              <Icon icon={IconWarning} className="text-danger" size="12"/>
            </div> : null
          }
        </div>

        <div className="d-flex m-2">
          <div className="w-100 d-flex">
            <div className="d-flex bg-light-300 ds-type h6 pl-1 pr-1">
              {/* <div className='ml-1 mr-1 bg-success ds-type-icon'></div> */}
              <div className="flex-shrink-0 text-dark-600 mr-1 text-truncate ds-type-name">{dataComponentsUtils.getDsTypeString(curDs.type, this.props.intl)}</div>
            </div>
          </div>

          <div className="flex-shrink-0 d-flex flex-row ds-related-widgets">
            <Icon icon={IconLinks} className="mr-1 text-dark" />
            <span className="align-middle">{relatedWidgets && relatedWidgets.length}</span>
          </div>
      </div>
    </div>
    )
  }

  render(){
    if(!this.props.curDs){
      return null;
    }
    const selectedNewDs =  this.props.selectedNewDs;
    const DsInfo = this.DsInfo;
    return (
      <div className="border-light-500 bg-white m-2 p-2 list-item ds-mapping-ds-item">
        <DsInfo curDs={this.props.curDs} selectedNewDs={selectedNewDs} />

        <div className="align-items-center">
          <div className="ds-mapping-drop-down p-2">
            <Dropdown isOpen={this.state.isNewDsDropdownOpen} className="w-100 drop-down"
              toggle={this.toggleDropdown}>
              <DropdownToggle caret outline className="w-100 text-truncate" disabled={!this.state.newDss}>
                {this.getSelectedDsLabel(selectedNewDs) || ''}
              </DropdownToggle>
              {
                this.state.newDss ?
                // TODO: there are some temp style
                <DropdownMenu className="text-truncate" modifiers={{applyStyle: { enabled: true,
                  fn: (data) => {
                    return {
                      ...data,
                      styles: {
                        ...data.styles,
                        zIndex: '1050'
                      }
                    };
                  }}}}>
                {
                  getSortedArrayByLabel(this.state.newDss).map((ds, index) => {
                    if(ds && ds.id){
                      return <DropdownItem key={index} onClick={() => {this.onSelectedNewDsChange(ds)}} title={ds.label || ds.id} style={{width: '220px'}}>
                                <div className="text-truncate">{ds.label || ds.id}</div>
                              </DropdownItem>
                    }
                    return null;
                  }
                  )
                }
                </DropdownMenu>
              : null
              }
            </Dropdown>
          </div>
        </div>

      </div>
    );
  }
}