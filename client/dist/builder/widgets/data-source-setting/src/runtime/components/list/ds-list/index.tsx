import {React, DataSource, IntlShape, ImmutableObject, WidgetJson, ThemeVariables} from 'jimu-core';
import {Tabs, Tab} from 'jimu-ui';

import DsListLayers from './ds-list-layers';
import ListInfo from '../common/list-info';
import ListWidgets from '../common/list-widgets';

import defaultMessages from '../../../translations/default';

interface Props{
  ds: DataSource;
  isDataSourceInited: boolean;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  intl: IntlShape;
  theme: ThemeVariables;
  onDsItemClicked: (ds: DataSource) => void;
  onMappingIconClick: (ds: DataSource) => void;
  onShowDetailClicked: (ds: DataSource) => void;
  portalUrl: string;
  dispatch: any;
}

interface State{

}

export default class extends React.PureComponent<Props, State>{

  constructor(props){
    super(props);
  }

  render(){
    if(!this.props.ds){
      return <div className="m-2">
        {this.props.intl.formatMessage({id: 'noSupportedDataSource', defaultMessage: defaultMessages.noSupportedDataSource})}
      </div>;
    }

    return (
      <div className="ds-list">
        <ListInfo ds={this.props.ds} onMappingIconClick={this.props.onMappingIconClick} widgets={this.props.widgets} theme={this.props.theme}
          onShowDetailClicked={this.props.onShowDetailClicked} intl={this.props.intl} portalUrl={this.props.portalUrl} />

        <Tabs underline>
          <Tab title={this.props.intl.formatMessage({id: 'layers', defaultMessage: defaultMessages.layers})} active={true}>
            <DsListLayers  portalUrl={this.props.portalUrl} ds={this.props.ds} key={this.props.ds.id} intl={this.props.intl}
              dispatch={this.props.dispatch} onDataSourceItemClick={this.props.onDsItemClicked} isDataSourceInited={this.props.isDataSourceInited}
              widgets={this.props.widgets} theme={this.props.theme}
            />
          </Tab>
          <Tab title={this.props.intl.formatMessage({id: 'widgets', defaultMessage: defaultMessages.widgets})}>
            <ListWidgets ds={this.props.ds} isDataSourceInited={this.props.isDataSourceInited} intl={this.props.intl}
              widgets={this.props.widgets}
            />
          </Tab>
        </Tabs>

      </div>
    );
  }
}
