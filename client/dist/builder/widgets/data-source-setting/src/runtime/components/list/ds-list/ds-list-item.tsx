import {React, DataSource, IntlShape, ImmutableObject, WidgetJson, ThemeVariables} from 'jimu-core';
import {DataSourceItem} from 'jimu-ui/data-source-selector';

import MoreOptions from '../common/list-more';

import {renameDataSource} from '../../../utils';
interface ItemProps{
  ds: DataSource;
  isMoreOptionsShown: boolean;
  isDataSourceInited: boolean;
  widgets: ImmutableObject<{ [widgetId: string]: WidgetJson }>;
  intl: IntlShape;
  theme: ThemeVariables;
  onDataSourceItemClick: (ds: DataSource) => void;
  onMoreIconClick: (dsId: string) => void;
  onMappingIconClick?: (ds: DataSource) => void;
}
interface ItemState{
  isRenameShown: boolean;
}
export default class extends React.PureComponent<ItemProps, ItemState>{
  rootDom: HTMLElement;

  constructor(props){
    super(props);
    this.state = {
      isRenameShown: false
    }
  }

  onRemoveItem = () => {
    this.props.onMoreIconClick(this.props.ds.id);
  }

  onRenameItem = () => {
    this.props.onMoreIconClick(this.props.ds.id);
    this.setState({
      isRenameShown: true
    });
  }

  onMoreIconClick = () => {
    this.props.onMoreIconClick(this.props.ds.id);
  }

  onRename = (newLabel: string) => {
    if(!newLabel){
      this.setState({isRenameShown: false});
      return;
    }

    renameDataSource(newLabel, this.props.ds);

    this.setState({isRenameShown: false});
  }

  onDataSourceItemClick = () => {
    this.props.onDataSourceItemClick(this.props.ds);
  }

  onMappingIconClick = () => {
    this.props.onMappingIconClick(this.props.ds);
  }

  render(){
    if(!this.props.ds){
      return null;
    }

    const isRootDataSource = !this.props.ds.parentDataSource;
    return (
      <div className="m-3 list-item" ref={d => {this.rootDom = d}}>

        <DataSourceItem dataSourceJson={this.props.ds.dataSourceJson} isMappingIconShown={false} onDataSourceItemClick={this.onDataSourceItemClick}
          isMoreIconShown={isRootDataSource} isRenameInputShown={this.state.isRenameShown && isRootDataSource} isRelatedWidgetsShown={true} isOriginalUrlShown={true}
          onMoreIconClick={this.onMoreIconClick} onMappingIconClick={this.onMappingIconClick} onRename={this.onRename}
        />

        <MoreOptions isShown={this.props.isMoreOptionsShown} rootDom={this.rootDom} intl={this.props.intl} widgets={this.props.widgets}
          dsJson={this.props.ds && this.props.ds.dataSourceJson} onRemoveItem={this.onRemoveItem} onRenameItem={this.onRenameItem}
          theme={this.props.theme}
        />

      </div>
    );
  }
}