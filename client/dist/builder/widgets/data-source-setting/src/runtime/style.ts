import {ThemeVariables, css, SerializedStyles, polished} from 'jimu-core';

export function getStyle(theme: ThemeVariables): SerializedStyles{
  return css`
    .widget-ds-setting{
      width: 100%;
      height: 100%;
      background-color: ${theme.colors.palette.light[300]};

      .title{
        padding: 18px 16px 2px 16px !important;
        font-size: 1rem;
      }

      .root-ds-list{
        overflow: auto;
        height: calc(100% - 80px);
      }

      .list-container{
        height: calc(100% - 40px);
      }

      .text-data-600{
        color: ${theme.colors.palette.dark[600]};
      }

      .border-color-gray-300{
        border-color: ${theme.colors.palette.light[500]} !important;
      }

      .in-use-related-widgets{
        font-size: 14px;
        color: ${theme.colors.palette.dark[300]};
        margin-top: 16px !important;
        margin-bottom: 16px !important;
        font-weight: bold;
      }

      .widget-icon{
        >img{
          vertical-align: text-top;
          width: ${polished.rem(16)};
          height: ${polished.rem(16)};
        }
      }

      .drop-down{
        button{
          background-color: ${theme.colors.white};
        }
      }

      .setting-header{
        border-bottom: 1px;
      }

      .ds-back{
        width: 40%;
        cursor: pointer;
        height: 20px;
        text-transform: initial;
        margin-left: -5px;
      }

      .add-data{
        height: ${polished.rem(30)};
        line-height: ${polished.rem(30)};
        width: 228px;
        padding: 0;
        border-radius: 2px;
        cursor: pointer;
      }
      button.add-data:hover{
        color: ${theme.colors.black} !important;
      }

      .ds-label-input{
        width: 90%;
      }

      .two-line-truncate{
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        word-break: break-all;
        word-wrap: break-word;
      }

      .ds-thumbnail-type{
        padding: ${polished.rem(10)} ${polished.rem(10)} 0  ${polished.rem(10)};
      }

      .ds-more{
        margin-left:  ${polished.rem(5)};
        margin-right: ${polished.rem(-2)};
        cursor: pointer;
        .ds-more-icon{
          display: inline-block;
          width: ${polished.rem(30)};
        }
      }

      .ds-origin-label{
        max-width: 100%;
        .ds-origin-label-link, .ds-origin-label-link:hover, .ds-origin-label-link:focus, .ds-origin-label-link:active{
          color: ${theme.colors.palette.dark[600]};
          outline: none;
          box-shadow: 0 0 0 !important;
          border: 0 !important;
        }
        .ds-origin-label-link-text{
          display: inline-block;
          font-size: ${polished.rem(12)};
          max-width: 100%;
        }
      }

      .mapping-info{
        .ds-label{
          cursor: default;
        }
      }

      .ds-mapping-icon{
        cursor: pointer;
      }

      .ds-mapping-collapse{
        padding: ${polished.rem(10)} ${polished.rem(10)} 0  ${polished.rem(10)};
        .ds-origin-label{
          max-width: 170px;
          height:  ${polished.rem(25)};
          line-height:  ${polished.rem(25)};
          border-radius: 2px;
        }
      }

      .ds-thumbnail{
        width:  ${polished.rem(20)};
        height:  ${polished.rem(20)};
        background-color: ${theme.colors.palette.info[400]};
        border-radius: 2px;
      }

      .mapping-info{
        .ds-mapping-collapse{
          border: 0;
        }
      }

      .field-list, .ds-list{
        height: calc(100% - 50px);
        .jimu-tab{
          height: calc(100% - 140px);
          .tab-content{
            height: calc(100% - 40px);
            overflow: auto;
          }
        }
        .ds-info{
          width: 100%;
          .ds-label{
            cursor: pointer;
            font-size: ${polished.rem(14)};
            color: ${theme.colors.palette.dark[800]};
          }
          .ds-type{
            font-size: ${polished.rem(13)};
            color: ${theme.colors.palette.dark[600]};
          }
        }
        .tab-pane{
          width: 100%;
        }
      }

      .field-item{
        font-size: ${polished.rem(13)};
        color: ${theme.colors.dark};
        .field-label{
          height:  ${polished.rem(35)};
          line-height:  ${polished.rem(35)};
        }
      }

      .ds-mapping{
        .ds-mapping-header{
          height:  ${polished.rem(42)};
          line-height:  ${polished.rem(42)};
          border-bottom: 1px solid;
          .ds-mapping-header-back{
            cursor: pointer;
          }
        }

        .ds-mapping-cur-info{
          border-bottom: 1px solid;
        }

        .ds-mapping-external-data{
          position: fixed;
          z-index: 4;
        }

        .ds-mapping-set-item{
          .ds-mapping-drop-down{
            .btn-group{
              width: 100%;
              button{
                width: 100%;
              }
            }
          }
        }

        .ds-mapping-buttons{
          left:  ${polished.rem(65)};
          bottom:  ${polished.rem(10)};
        }

        .ds-mapping-source{
          width:  ${polished.rem(20)};
          height:  ${polished.rem(20)};
          border-radius:  ${polished.rem(10)};
          line-height:  ${polished.rem(20)};
          text-align: center;
          cursor: pointer;
        }

        .ds-mapping-ds{
          .ds-origin-label{
            max-width: 100%;
          }
        }

      }
    }

    .widget-ds-setting-add-data-popup{
      .modal-content{
        height: 100%;
      }
    }

  `
}