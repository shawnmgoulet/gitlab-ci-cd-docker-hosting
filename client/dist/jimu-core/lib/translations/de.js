define({
  loading: 'ä_Loading_______________Ü',
  ok: 'ä_OK_____Ü',
  cancel: 'ä_Cancel_____________Ü',
  notPublishError: 'ä_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Ü.',
  versionUpdateMsg: 'ä_New updates are available. Click Reload button to get the latest updates______________________________________Ü.',
  doUpdate: 'ä_Reload_____________Ü',
  search: 'ä_Search_____________Ü',
  message_StringSelectionChange: 'ä_String selection change________________________Ü',
  message_ExtentChange: 'ä_Extent change______________Ü',
  message_DataRecordsSelectionChange: 'ä_Record selection change________________________Ü',
  message_DataRecordSetCreate: 'ä_Record set create__________________Ü',
  message_DataRecordSetUpdate: 'ä_Record set update__________________Ü',
  message_SelectDataRecord: 'ä_Record set record__________________Ü',
  dataAction_ExportJson: 'ä_Export JSON____________Ü',
  messageAction_SelectDataRecord: 'ä_Select data record___________________Ü',
  messageAction_FilterDataRecord: 'ä_Filter data record___________________Ü'
});