define({
  loading: 'í_Loading_______________ő',
  ok: 'í_OK_____ő',
  cancel: 'í_Cancel_____________ő',
  notPublishError: 'í_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ő.',
  versionUpdateMsg: 'í_New updates are available. Click Reload button to get the latest updates______________________________________ő.',
  doUpdate: 'í_Reload_____________ő',
  search: 'í_Search_____________ő',
  message_StringSelectionChange: 'í_String selection change________________________ő',
  message_ExtentChange: 'í_Extent change______________ő',
  message_DataRecordsSelectionChange: 'í_Record selection change________________________ő',
  message_DataRecordSetCreate: 'í_Record set create__________________ő',
  message_DataRecordSetUpdate: 'í_Record set update__________________ő',
  message_SelectDataRecord: 'í_Record set record__________________ő',
  dataAction_ExportJson: 'í_Export JSON____________ő',
  messageAction_SelectDataRecord: 'í_Select data record___________________ő',
  messageAction_FilterDataRecord: 'í_Filter data record___________________ő'
});