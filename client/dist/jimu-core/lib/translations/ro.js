define({
  loading: 'Ă_Loading_______________ș',
  ok: 'Ă_OK_____ș',
  cancel: 'Ă_Cancel_____________ș',
  notPublishError: 'Ă_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ș.',
  versionUpdateMsg: 'Ă_New updates are available. Click Reload button to get the latest updates______________________________________ș.',
  doUpdate: 'Ă_Reload_____________ș',
  search: 'Ă_Search_____________ș',
  message_StringSelectionChange: 'Ă_String selection change________________________ș',
  message_ExtentChange: 'Ă_Extent change______________ș',
  message_DataRecordsSelectionChange: 'Ă_Record selection change________________________ș',
  message_DataRecordSetCreate: 'Ă_Record set create__________________ș',
  message_DataRecordSetUpdate: 'Ă_Record set update__________________ș',
  message_SelectDataRecord: 'Ă_Record set record__________________ș',
  dataAction_ExportJson: 'Ă_Export JSON____________ș',
  messageAction_SelectDataRecord: 'Ă_Select data record___________________ș',
  messageAction_FilterDataRecord: 'Ă_Filter data record___________________ș'
});