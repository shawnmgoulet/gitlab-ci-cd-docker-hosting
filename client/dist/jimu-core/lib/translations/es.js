define({
  loading: 'á_Loading_______________Ó',
  ok: 'á_OK_____Ó',
  cancel: 'á_Cancel_____________Ó',
  notPublishError: 'á_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Ó.',
  versionUpdateMsg: 'á_New updates are available. Click Reload button to get the latest updates______________________________________Ó.',
  doUpdate: 'á_Reload_____________Ó',
  search: 'á_Search_____________Ó',
  message_StringSelectionChange: 'á_String selection change________________________Ó',
  message_ExtentChange: 'á_Extent change______________Ó',
  message_DataRecordsSelectionChange: 'á_Record selection change________________________Ó',
  message_DataRecordSetCreate: 'á_Record set create__________________Ó',
  message_DataRecordSetUpdate: 'á_Record set update__________________Ó',
  message_SelectDataRecord: 'á_Record set record__________________Ó',
  dataAction_ExportJson: 'á_Export JSON____________Ó',
  messageAction_SelectDataRecord: 'á_Select data record___________________Ó',
  messageAction_FilterDataRecord: 'á_Filter data record___________________Ó'
});