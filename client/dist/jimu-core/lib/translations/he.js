define({
  loading: 'כן_Loading_______________ש',
  ok: 'כן_OK_____ש',
  cancel: 'כן_Cancel_____________ש',
  notPublishError: 'כן_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ש.',
  versionUpdateMsg: 'כן_New updates are available. Click Reload button to get the latest updates______________________________________ש.',
  doUpdate: 'כן_Reload_____________ש',
  search: 'כן_Search_____________ש',
  message_StringSelectionChange: 'כן_String selection change________________________ש',
  message_ExtentChange: 'כן_Extent change______________ש',
  message_DataRecordsSelectionChange: 'כן_Record selection change________________________ש',
  message_DataRecordSetCreate: 'כן_Record set create__________________ש',
  message_DataRecordSetUpdate: 'כן_Record set update__________________ש',
  message_SelectDataRecord: 'כן_Record set record__________________ש',
  dataAction_ExportJson: 'כן_Export JSON____________ש',
  messageAction_SelectDataRecord: 'כן_Select data record___________________ש',
  messageAction_FilterDataRecord: 'כן_Filter data record___________________ש'
});