define({
  loading: '須_Loading_______________鷗',
  ok: '須_OK_____鷗',
  cancel: '須_Cancel_____________鷗',
  notPublishError: '須_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________鷗.',
  versionUpdateMsg: '須_New updates are available. Click Reload button to get the latest updates______________________________________鷗.',
  doUpdate: '須_Reload_____________鷗',
  search: '須_Search_____________鷗',
  message_StringSelectionChange: '須_String selection change________________________鷗',
  message_ExtentChange: '須_Extent change______________鷗',
  message_DataRecordsSelectionChange: '須_Record selection change________________________鷗',
  message_DataRecordSetCreate: '須_Record set create__________________鷗',
  message_DataRecordSetUpdate: '須_Record set update__________________鷗',
  message_SelectDataRecord: '須_Record set record__________________鷗',
  dataAction_ExportJson: '須_Export JSON____________鷗',
  messageAction_SelectDataRecord: '須_Select data record___________________鷗',
  messageAction_FilterDataRecord: '須_Filter data record___________________鷗'
});