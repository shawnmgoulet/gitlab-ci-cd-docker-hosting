define({
  loading: 'Š_Loading_______________ä',
  ok: 'Š_OK_____ä',
  cancel: 'Š_Cancel_____________ä',
  notPublishError: 'Š_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ä.',
  versionUpdateMsg: 'Š_New updates are available. Click Reload button to get the latest updates______________________________________ä.',
  doUpdate: 'Š_Reload_____________ä',
  search: 'Š_Search_____________ä',
  message_StringSelectionChange: 'Š_String selection change________________________ä',
  message_ExtentChange: 'Š_Extent change______________ä',
  message_DataRecordsSelectionChange: 'Š_Record selection change________________________ä',
  message_DataRecordSetCreate: 'Š_Record set create__________________ä',
  message_DataRecordSetUpdate: 'Š_Record set update__________________ä',
  message_SelectDataRecord: 'Š_Record set record__________________ä',
  dataAction_ExportJson: 'Š_Export JSON____________ä',
  messageAction_SelectDataRecord: 'Š_Select data record___________________ä',
  messageAction_FilterDataRecord: 'Š_Filter data record___________________ä'
});