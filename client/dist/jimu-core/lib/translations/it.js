define({
  loading: 'é_Loading_______________È',
  ok: 'é_OK_____È',
  cancel: 'é_Cancel_____________È',
  notPublishError: 'é_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________È.',
  versionUpdateMsg: 'é_New updates are available. Click Reload button to get the latest updates______________________________________È.',
  doUpdate: 'é_Reload_____________È',
  search: 'é_Search_____________È',
  message_StringSelectionChange: 'é_String selection change________________________È',
  message_ExtentChange: 'é_Extent change______________È',
  message_DataRecordsSelectionChange: 'é_Record selection change________________________È',
  message_DataRecordSetCreate: 'é_Record set create__________________È',
  message_DataRecordSetUpdate: 'é_Record set update__________________È',
  message_SelectDataRecord: 'é_Record set record__________________È',
  dataAction_ExportJson: 'é_Export JSON____________È',
  messageAction_SelectDataRecord: 'é_Select data record___________________È',
  messageAction_FilterDataRecord: 'é_Filter data record___________________È'
});