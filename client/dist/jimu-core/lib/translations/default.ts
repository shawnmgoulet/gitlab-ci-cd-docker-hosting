export default {
  loading: 'Loading',
  ok: 'OK',
  cancel: 'Cancel',
  notPublishError: 'This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it.',
  versionUpdateMsg: 'New updates are available. Click Reload button to get the latest updates.',
  doUpdate: 'Reload',
  search: 'Search',

  message_StringSelectionChange: 'String selection change',
  message_ExtentChange: 'Extent change',
  message_DataRecordsSelectionChange: 'Record selection change',
  message_DataRecordSetCreate: 'Record set create',
  message_DataRecordSetUpdate: 'Record set update',
  message_SelectDataRecord: 'Record set record',

  dataAction_ExportJson: 'Export JSON',
  messageAction_SelectDataRecord: 'Select data record',
  messageAction_FilterDataRecord: 'Filter data record'
}
