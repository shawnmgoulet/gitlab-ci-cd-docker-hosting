define({
  loading: '한_Loading_______________빠',
  ok: '한_OK_____빠',
  cancel: '한_Cancel_____________빠',
  notPublishError: '한_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________빠.',
  versionUpdateMsg: '한_New updates are available. Click Reload button to get the latest updates______________________________________빠.',
  doUpdate: '한_Reload_____________빠',
  search: '한_Search_____________빠',
  message_StringSelectionChange: '한_String selection change________________________빠',
  message_ExtentChange: '한_Extent change______________빠',
  message_DataRecordsSelectionChange: '한_Record selection change________________________빠',
  message_DataRecordSetCreate: '한_Record set create__________________빠',
  message_DataRecordSetUpdate: '한_Record set update__________________빠',
  message_SelectDataRecord: '한_Record set record__________________빠',
  dataAction_ExportJson: '한_Export JSON____________빠',
  messageAction_SelectDataRecord: '한_Select data record___________________빠',
  messageAction_FilterDataRecord: '한_Filter data record___________________빠'
});