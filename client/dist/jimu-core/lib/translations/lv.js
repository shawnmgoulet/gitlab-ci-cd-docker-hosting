define({
  loading: 'ķ_Loading_______________ū',
  ok: 'ķ_OK_____ū',
  cancel: 'ķ_Cancel_____________ū',
  notPublishError: 'ķ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ū.',
  versionUpdateMsg: 'ķ_New updates are available. Click Reload button to get the latest updates______________________________________ū.',
  doUpdate: 'ķ_Reload_____________ū',
  search: 'ķ_Search_____________ū',
  message_StringSelectionChange: 'ķ_String selection change________________________ū',
  message_ExtentChange: 'ķ_Extent change______________ū',
  message_DataRecordsSelectionChange: 'ķ_Record selection change________________________ū',
  message_DataRecordSetCreate: 'ķ_Record set create__________________ū',
  message_DataRecordSetUpdate: 'ķ_Record set update__________________ū',
  message_SelectDataRecord: 'ķ_Record set record__________________ū',
  dataAction_ExportJson: 'ķ_Export JSON____________ū',
  messageAction_SelectDataRecord: 'ķ_Select data record___________________ū',
  messageAction_FilterDataRecord: 'ķ_Filter data record___________________ū'
});