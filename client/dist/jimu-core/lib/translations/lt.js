define({
  loading: 'Į_Loading_______________š',
  ok: 'Į_OK_____š',
  cancel: 'Į_Cancel_____________š',
  notPublishError: 'Į_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________š.',
  versionUpdateMsg: 'Į_New updates are available. Click Reload button to get the latest updates______________________________________š.',
  doUpdate: 'Į_Reload_____________š',
  search: 'Į_Search_____________š',
  message_StringSelectionChange: 'Į_String selection change________________________š',
  message_ExtentChange: 'Į_Extent change______________š',
  message_DataRecordsSelectionChange: 'Į_Record selection change________________________š',
  message_DataRecordSetCreate: 'Į_Record set create__________________š',
  message_DataRecordSetUpdate: 'Į_Record set update__________________š',
  message_SelectDataRecord: 'Į_Record set record__________________š',
  dataAction_ExportJson: 'Į_Export JSON____________š',
  messageAction_SelectDataRecord: 'Į_Select data record___________________š',
  messageAction_FilterDataRecord: 'Į_Filter data record___________________š'
});