define({
  loading: 'ı_Loading_______________İ',
  ok: 'ı_OK_____İ',
  cancel: 'ı_Cancel_____________İ',
  notPublishError: 'ı_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________İ.',
  versionUpdateMsg: 'ı_New updates are available. Click Reload button to get the latest updates______________________________________İ.',
  doUpdate: 'ı_Reload_____________İ',
  search: 'ı_Search_____________İ',
  message_StringSelectionChange: 'ı_String selection change________________________İ',
  message_ExtentChange: 'ı_Extent change______________İ',
  message_DataRecordsSelectionChange: 'ı_Record selection change________________________İ',
  message_DataRecordSetCreate: 'ı_Record set create__________________İ',
  message_DataRecordSetUpdate: 'ı_Record set update__________________İ',
  message_SelectDataRecord: 'ı_Record set record__________________İ',
  dataAction_ExportJson: 'ı_Export JSON____________İ',
  messageAction_SelectDataRecord: 'ı_Select data record___________________İ',
  messageAction_FilterDataRecord: 'ı_Filter data record___________________İ'
});