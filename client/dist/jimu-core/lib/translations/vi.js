define({
  loading: 'Đ_Loading_______________ớ',
  ok: 'Đ_OK_____ớ',
  cancel: 'Đ_Cancel_____________ớ',
  notPublishError: 'Đ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ớ.',
  versionUpdateMsg: 'Đ_New updates are available. Click Reload button to get the latest updates______________________________________ớ.',
  doUpdate: 'Đ_Reload_____________ớ',
  search: 'Đ_Search_____________ớ',
  message_StringSelectionChange: 'Đ_String selection change________________________ớ',
  message_ExtentChange: 'Đ_Extent change______________ớ',
  message_DataRecordsSelectionChange: 'Đ_Record selection change________________________ớ',
  message_DataRecordSetCreate: 'Đ_Record set create__________________ớ',
  message_DataRecordSetUpdate: 'Đ_Record set update__________________ớ',
  message_SelectDataRecord: 'Đ_Record set record__________________ớ',
  dataAction_ExportJson: 'Đ_Export JSON____________ớ',
  messageAction_SelectDataRecord: 'Đ_Select data record___________________ớ',
  messageAction_FilterDataRecord: 'Đ_Filter data record___________________ớ'
});