define({
  loading: 'ґ_Loading_______________Ї',
  ok: 'ґ_OK_____Ї',
  cancel: 'ґ_Cancel_____________Ї',
  notPublishError: 'ґ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Ї.',
  versionUpdateMsg: 'ґ_New updates are available. Click Reload button to get the latest updates______________________________________Ї.',
  doUpdate: 'ґ_Reload_____________Ї',
  search: 'ґ_Search_____________Ї',
  message_StringSelectionChange: 'ґ_String selection change________________________Ї',
  message_ExtentChange: 'ґ_Extent change______________Ї',
  message_DataRecordsSelectionChange: 'ґ_Record selection change________________________Ї',
  message_DataRecordSetCreate: 'ґ_Record set create__________________Ї',
  message_DataRecordSetUpdate: 'ґ_Record set update__________________Ї',
  message_SelectDataRecord: 'ґ_Record set record__________________Ї',
  dataAction_ExportJson: 'ґ_Export JSON____________Ї',
  messageAction_SelectDataRecord: 'ґ_Select data record___________________Ї',
  messageAction_FilterDataRecord: 'ґ_Filter data record___________________Ї'
});