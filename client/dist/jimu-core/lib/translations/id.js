define({
  loading: 'ng_Loading_______________ny',
  ok: 'ng_OK_____ny',
  cancel: 'ng_Cancel_____________ny',
  notPublishError: 'ng_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ny.',
  versionUpdateMsg: 'ng_New updates are available. Click Reload button to get the latest updates______________________________________ny.',
  doUpdate: 'ng_Reload_____________ny',
  search: 'ng_Search_____________ny',
  message_StringSelectionChange: 'ng_String selection change________________________ny',
  message_ExtentChange: 'ng_Extent change______________ny',
  message_DataRecordsSelectionChange: 'ng_Record selection change________________________ny',
  message_DataRecordSetCreate: 'ng_Record set create__________________ny',
  message_DataRecordSetUpdate: 'ng_Record set update__________________ny',
  message_SelectDataRecord: 'ng_Record set record__________________ny',
  dataAction_ExportJson: 'ng_Export JSON____________ny',
  messageAction_SelectDataRecord: 'ng_Select data record___________________ny',
  messageAction_FilterDataRecord: 'ng_Filter data record___________________ny'
});