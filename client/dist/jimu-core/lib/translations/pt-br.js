define({
  loading: 'ã_Loading_______________Ç',
  ok: 'ã_OK_____Ç',
  cancel: 'ã_Cancel_____________Ç',
  notPublishError: 'ã_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Ç.',
  versionUpdateMsg: 'ã_New updates are available. Click Reload button to get the latest updates______________________________________Ç.',
  doUpdate: 'ã_Reload_____________Ç',
  search: 'ã_Search_____________Ç',
  message_StringSelectionChange: 'ã_String selection change________________________Ç',
  message_ExtentChange: 'ã_Extent change______________Ç',
  message_DataRecordsSelectionChange: 'ã_Record selection change________________________Ç',
  message_DataRecordSetCreate: 'ã_Record set create__________________Ç',
  message_DataRecordSetUpdate: 'ã_Record set update__________________Ç',
  message_SelectDataRecord: 'ã_Record set record__________________Ç',
  dataAction_ExportJson: 'ã_Export JSON____________Ç',
  messageAction_SelectDataRecord: 'ã_Select data record___________________Ç',
  messageAction_FilterDataRecord: 'ã_Filter data record___________________Ç'
});