define({
  loading: 'Ж_Loading_______________Я',
  ok: 'Ж_OK_____Я',
  cancel: 'Ж_Cancel_____________Я',
  notPublishError: 'Ж_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Я.',
  versionUpdateMsg: 'Ж_New updates are available. Click Reload button to get the latest updates______________________________________Я.',
  doUpdate: 'Ж_Reload_____________Я',
  search: 'Ж_Search_____________Я',
  message_StringSelectionChange: 'Ж_String selection change________________________Я',
  message_ExtentChange: 'Ж_Extent change______________Я',
  message_DataRecordsSelectionChange: 'Ж_Record selection change________________________Я',
  message_DataRecordSetCreate: 'Ж_Record set create__________________Я',
  message_DataRecordSetUpdate: 'Ж_Record set update__________________Я',
  message_SelectDataRecord: 'Ж_Record set record__________________Я',
  dataAction_ExportJson: 'Ж_Export JSON____________Я',
  messageAction_SelectDataRecord: 'Ж_Select data record___________________Я',
  messageAction_FilterDataRecord: 'Ж_Filter data record___________________Я'
});