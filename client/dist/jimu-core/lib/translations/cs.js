define({
  loading: 'Ř_Loading_______________ů',
  ok: 'Ř_OK_____ů',
  cancel: 'Ř_Cancel_____________ů',
  notPublishError: 'Ř_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ů.',
  versionUpdateMsg: 'Ř_New updates are available. Click Reload button to get the latest updates______________________________________ů.',
  doUpdate: 'Ř_Reload_____________ů',
  search: 'Ř_Search_____________ů',
  message_StringSelectionChange: 'Ř_String selection change________________________ů',
  message_ExtentChange: 'Ř_Extent change______________ů',
  message_DataRecordsSelectionChange: 'Ř_Record selection change________________________ů',
  message_DataRecordSetCreate: 'Ř_Record set create__________________ů',
  message_DataRecordSetUpdate: 'Ř_Record set update__________________ů',
  message_SelectDataRecord: 'Ř_Record set record__________________ů',
  dataAction_ExportJson: 'Ř_Export JSON____________ů',
  messageAction_SelectDataRecord: 'Ř_Select data record___________________ů',
  messageAction_FilterDataRecord: 'Ř_Filter data record___________________ů'
});