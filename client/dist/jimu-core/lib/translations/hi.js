define({
  loading: 'आ_Loading_______________ज',
  ok: 'आ_OK_____ज',
  cancel: 'आ_Cancel_____________ज',
  notPublishError: 'आ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ज.',
  versionUpdateMsg: 'आ_New updates are available. Click Reload button to get the latest updates______________________________________ज.',
  doUpdate: 'आ_Reload_____________ज',
  search: 'आ_Search_____________ज',
  message_StringSelectionChange: 'आ_String selection change________________________ज',
  message_ExtentChange: 'आ_Extent change______________ज',
  message_DataRecordsSelectionChange: 'आ_Record selection change________________________ज',
  message_DataRecordSetCreate: 'आ_Record set create__________________ज',
  message_DataRecordSetUpdate: 'आ_Record set update__________________ज',
  message_SelectDataRecord: 'आ_Record set record__________________ज',
  dataAction_ExportJson: 'आ_Export JSON____________ज',
  messageAction_SelectDataRecord: 'आ_Select data record___________________ज',
  messageAction_FilterDataRecord: 'आ_Filter data record___________________ज'
});