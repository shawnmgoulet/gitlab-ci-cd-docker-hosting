define({
  loading: 'ł_Loading_______________ą',
  ok: 'ł_OK_____ą',
  cancel: 'ł_Cancel_____________ą',
  notPublishError: 'ł_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ą.',
  versionUpdateMsg: 'ł_New updates are available. Click Reload button to get the latest updates______________________________________ą.',
  doUpdate: 'ł_Reload_____________ą',
  search: 'ł_Search_____________ą',
  message_StringSelectionChange: 'ł_String selection change________________________ą',
  message_ExtentChange: 'ł_Extent change______________ą',
  message_DataRecordsSelectionChange: 'ł_Record selection change________________________ą',
  message_DataRecordSetCreate: 'ł_Record set create__________________ą',
  message_DataRecordSetUpdate: 'ł_Record set update__________________ą',
  message_SelectDataRecord: 'ł_Record set record__________________ą',
  dataAction_ExportJson: 'ł_Export JSON____________ą',
  messageAction_SelectDataRecord: 'ł_Select data record___________________ą',
  messageAction_FilterDataRecord: 'ł_Filter data record___________________ą'
});