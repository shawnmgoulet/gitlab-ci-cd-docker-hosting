define({
  loading: 'Č_Loading_______________ž',
  ok: 'Č_OK_____ž',
  cancel: 'Č_Cancel_____________ž',
  notPublishError: 'Č_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ž.',
  versionUpdateMsg: 'Č_New updates are available. Click Reload button to get the latest updates______________________________________ž.',
  doUpdate: 'Č_Reload_____________ž',
  search: 'Č_Search_____________ž',
  message_StringSelectionChange: 'Č_String selection change________________________ž',
  message_ExtentChange: 'Č_Extent change______________ž',
  message_DataRecordsSelectionChange: 'Č_Record selection change________________________ž',
  message_DataRecordSetCreate: 'Č_Record set create__________________ž',
  message_DataRecordSetUpdate: 'Č_Record set update__________________ž',
  message_SelectDataRecord: 'Č_Record set record__________________ž',
  dataAction_ExportJson: 'Č_Export JSON____________ž',
  messageAction_SelectDataRecord: 'Č_Select data record___________________ž',
  messageAction_FilterDataRecord: 'Č_Filter data record___________________ž'
});