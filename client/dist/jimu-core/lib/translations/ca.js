define({
  loading: 'ó_Loading_______________à',
  ok: 'ó_OK_____à',
  cancel: 'ó_Cancel_____________à',
  notPublishError: 'ó_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________à.',
  versionUpdateMsg: 'ó_New updates are available. Click Reload button to get the latest updates______________________________________à.',
  doUpdate: 'ó_Reload_____________à',
  search: 'ó_Search_____________à',
  message_StringSelectionChange: 'ó_String selection change________________________à',
  message_ExtentChange: 'ó_Extent change______________à',
  message_DataRecordsSelectionChange: 'ó_Record selection change________________________à',
  message_DataRecordSetCreate: 'ó_Record set create__________________à',
  message_DataRecordSetUpdate: 'ó_Record set update__________________à',
  message_SelectDataRecord: 'ó_Record set record__________________à',
  dataAction_ExportJson: 'ó_Export JSON____________à',
  messageAction_SelectDataRecord: 'ó_Select data record___________________à',
  messageAction_FilterDataRecord: 'ó_Filter data record___________________à'
});