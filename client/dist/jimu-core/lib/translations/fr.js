define({
  loading: 'æ_Loading_______________Â',
  ok: 'æ_OK_____Â',
  cancel: 'æ_Cancel_____________Â',
  notPublishError: 'æ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________Â.',
  versionUpdateMsg: 'æ_New updates are available. Click Reload button to get the latest updates______________________________________Â.',
  doUpdate: 'æ_Reload_____________Â',
  search: 'æ_Search_____________Â',
  message_StringSelectionChange: 'æ_String selection change________________________Â',
  message_ExtentChange: 'æ_Extent change______________Â',
  message_DataRecordsSelectionChange: 'æ_Record selection change________________________Â',
  message_DataRecordSetCreate: 'æ_Record set create__________________Â',
  message_DataRecordSetUpdate: 'æ_Record set update__________________Â',
  message_SelectDataRecord: 'æ_Record set record__________________Â',
  dataAction_ExportJson: 'æ_Export JSON____________Â',
  messageAction_SelectDataRecord: 'æ_Select data record___________________Â',
  messageAction_FilterDataRecord: 'æ_Filter data record___________________Â'
});