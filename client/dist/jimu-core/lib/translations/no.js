export default {
  loading: 'å_Loading_______________ø',
  ok: 'å_OK_____ø',
  cancel: 'å_Cancel_____________ø',
  notPublishError: 'å_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ø.',
  versionUpdateMsg: 'å_New updates are available. Click Reload button to get the latest updates______________________________________ø.',
  doUpdate: 'å_Reload_____________ø',
  search: 'å_Search_____________ø',

  message_StringSelectionChange: 'å_String selection change________________________ø',
  message_ExtentChange: 'å_Extent change______________ø',
  message_DataRecordsSelectionChange: 'å_Record selection change________________________ø',
  message_DataRecordSetCreate: 'å_Record set create__________________ø',
  message_DataRecordSetUpdate: 'å_Record set update__________________ø',
  message_SelectDataRecord: 'å_Record set record__________________ø',

  dataAction_ExportJson: 'å_Export JSON____________ø',
  messageAction_SelectDataRecord: 'å_Select data record___________________ø',
  messageAction_FilterDataRecord: 'å_Filter data record___________________ø'
}
