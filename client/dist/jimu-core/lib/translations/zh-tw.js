define({
  loading: '試_Loading_______________驗',
  ok: '試_OK_____驗',
  cancel: '試_Cancel_____________驗',
  notPublishError: '試_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________驗.',
  versionUpdateMsg: '試_New updates are available. Click Reload button to get the latest updates______________________________________驗.',
  doUpdate: '試_Reload_____________驗',
  search: '試_Search_____________驗',
  message_StringSelectionChange: '試_String selection change________________________驗',
  message_ExtentChange: '試_Extent change______________驗',
  message_DataRecordsSelectionChange: '試_Record selection change________________________驗',
  message_DataRecordSetCreate: '試_Record set create__________________驗',
  message_DataRecordSetUpdate: '試_Record set update__________________驗',
  message_SelectDataRecord: '試_Record set record__________________驗',
  dataAction_ExportJson: '試_Export JSON____________驗',
  messageAction_SelectDataRecord: '試_Select data record___________________驗',
  messageAction_FilterDataRecord: '試_Filter data record___________________驗'
});