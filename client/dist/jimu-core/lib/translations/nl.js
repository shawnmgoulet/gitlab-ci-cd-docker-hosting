define({
  loading: 'Ĳ_Loading_______________ä',
  ok: 'Ĳ_OK_____ä',
  cancel: 'Ĳ_Cancel_____________ä',
  notPublishError: 'Ĳ_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ä.',
  versionUpdateMsg: 'Ĳ_New updates are available. Click Reload button to get the latest updates______________________________________ä.',
  doUpdate: 'Ĳ_Reload_____________ä',
  search: 'Ĳ_Search_____________ä',
  message_StringSelectionChange: 'Ĳ_String selection change________________________ä',
  message_ExtentChange: 'Ĳ_Extent change______________ä',
  message_DataRecordsSelectionChange: 'Ĳ_Record selection change________________________ä',
  message_DataRecordSetCreate: 'Ĳ_Record set create__________________ä',
  message_DataRecordSetUpdate: 'Ĳ_Record set update__________________ä',
  message_SelectDataRecord: 'Ĳ_Record set record__________________ä',
  dataAction_ExportJson: 'Ĳ_Export JSON____________ä',
  messageAction_SelectDataRecord: 'Ĳ_Select data record___________________ä',
  messageAction_FilterDataRecord: 'Ĳ_Filter data record___________________ä'
});