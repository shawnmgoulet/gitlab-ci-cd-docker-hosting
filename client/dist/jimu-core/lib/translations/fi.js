define({
  loading: 'Å_Loading_______________ö',
  ok: 'Å_OK_____ö',
  cancel: 'Å_Cancel_____________ö',
  notPublishError: 'Å_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ö.',
  versionUpdateMsg: 'Å_New updates are available. Click Reload button to get the latest updates______________________________________ö.',
  doUpdate: 'Å_Reload_____________ö',
  search: 'Å_Search_____________ö',
  message_StringSelectionChange: 'Å_String selection change________________________ö',
  message_ExtentChange: 'Å_Extent change______________ö',
  message_DataRecordsSelectionChange: 'Å_Record selection change________________________ö',
  message_DataRecordSetCreate: 'Å_Record set create__________________ö',
  message_DataRecordSetUpdate: 'Å_Record set update__________________ö',
  message_SelectDataRecord: 'Å_Record set record__________________ö',
  dataAction_ExportJson: 'Å_Export JSON____________ö',
  messageAction_SelectDataRecord: 'Å_Select data record___________________ö',
  messageAction_FilterDataRecord: 'Å_Filter data record___________________ö'
});