define({
  loading: 'ø_Loading_______________å',
  ok: 'ø_OK_____å',
  cancel: 'ø_Cancel_____________å',
  notPublishError: 'ø_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________å.',
  versionUpdateMsg: 'ø_New updates are available. Click Reload button to get the latest updates______________________________________å.',
  doUpdate: 'ø_Reload_____________å',
  search: 'ø_Search_____________å',
  message_StringSelectionChange: 'ø_String selection change________________________å',
  message_ExtentChange: 'ø_Extent change______________å',
  message_DataRecordsSelectionChange: 'ø_Record selection change________________________å',
  message_DataRecordSetCreate: 'ø_Record set create__________________å',
  message_DataRecordSetUpdate: 'ø_Record set update__________________å',
  message_SelectDataRecord: 'ø_Record set record__________________å',
  dataAction_ExportJson: 'ø_Export JSON____________å',
  messageAction_SelectDataRecord: 'ø_Select data record___________________å',
  messageAction_FilterDataRecord: 'ø_Filter data record___________________å'
});