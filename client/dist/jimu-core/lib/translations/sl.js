define({
  loading: 'Š_Loading_______________č',
  ok: 'Š_OK_____č',
  cancel: 'Š_Cancel_____________č',
  notPublishError: 'Š_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________č.',
  versionUpdateMsg: 'Š_New updates are available. Click Reload button to get the latest updates______________________________________č.',
  doUpdate: 'Š_Reload_____________č',
  search: 'Š_Search_____________č',
  message_StringSelectionChange: 'Š_String selection change________________________č',
  message_ExtentChange: 'Š_Extent change______________č',
  message_DataRecordsSelectionChange: 'Š_Record selection change________________________č',
  message_DataRecordSetCreate: 'Š_Record set create__________________č',
  message_DataRecordSetUpdate: 'Š_Record set update__________________č',
  message_SelectDataRecord: 'Š_Record set record__________________č',
  dataAction_ExportJson: 'Š_Export JSON____________č',
  messageAction_SelectDataRecord: 'Š_Select data record___________________č',
  messageAction_FilterDataRecord: 'Š_Filter data record___________________č'
});