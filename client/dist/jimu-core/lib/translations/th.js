define({
  loading: 'ก้_Loading_______________ษฺ',
  ok: 'ก้_OK_____ษฺ',
  cancel: 'ก้_Cancel_____________ษฺ',
  notPublishError: 'ก้_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________ษฺ.',
  versionUpdateMsg: 'ก้_New updates are available. Click Reload button to get the latest updates______________________________________ษฺ.',
  doUpdate: 'ก้_Reload_____________ษฺ',
  search: 'ก้_Search_____________ษฺ',
  message_StringSelectionChange: 'ก้_String selection change________________________ษฺ',
  message_ExtentChange: 'ก้_Extent change______________ษฺ',
  message_DataRecordsSelectionChange: 'ก้_Record selection change________________________ษฺ',
  message_DataRecordSetCreate: 'ก้_Record set create__________________ษฺ',
  message_DataRecordSetUpdate: 'ก้_Record set update__________________ษฺ',
  message_SelectDataRecord: 'ก้_Record set record__________________ษฺ',
  dataAction_ExportJson: 'ก้_Export JSON____________ษฺ',
  messageAction_SelectDataRecord: 'ก้_Select data record___________________ษฺ',
  messageAction_FilterDataRecord: 'ก้_Filter data record___________________ษฺ'
});