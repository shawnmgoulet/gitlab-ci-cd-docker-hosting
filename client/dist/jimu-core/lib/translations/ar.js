define({
  loading: 'بيت_Loading_______________لاحقة',
  ok: 'بيت_OK_____لاحقة',
  cancel: 'بيت_Cancel_____________لاحقة',
  notPublishError: 'بيت_This item is not published. Please open this item in Experience Builder, then click <b>&nbsp;Publish&nbsp;</b> to publish it________________________________________________________________لاحقة.',
  versionUpdateMsg: 'بيت_New updates are available. Click Reload button to get the latest updates______________________________________لاحقة.',
  doUpdate: 'بيت_Reload_____________لاحقة',
  search: 'بيت_Search_____________لاحقة',
  message_StringSelectionChange: 'بيت_String selection change________________________لاحقة',
  message_ExtentChange: 'بيت_Extent change______________لاحقة',
  message_DataRecordsSelectionChange: 'بيت_Record selection change________________________لاحقة',
  message_DataRecordSetCreate: 'بيت_Record set create__________________لاحقة',
  message_DataRecordSetUpdate: 'بيت_Record set update__________________لاحقة',
  message_SelectDataRecord: 'بيت_Record set record__________________لاحقة',
  dataAction_ExportJson: 'بيت_Export JSON____________لاحقة',
  messageAction_SelectDataRecord: 'بيت_Select data record___________________لاحقة',
  messageAction_FilterDataRecord: 'بيت_Filter data record___________________لاحقة'
});