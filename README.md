# GitLab CI/CD + GitLab Container Registry + Docker Hosting

A collection of prototypes for workflows intended to prioritize slim Docker image building and container running.