"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cheerio = require("cheerio");
const fs = require("fs");
const path = require("path");
const utils = require("util");
const SystemJS = require("systemjs");
const fetch = require("cross-fetch");
const queryString = require("query-string");
const exb_path_util_1 = require("exb-path-util");
const arcgis_rest_request_1 = require("@esri/arcgis-rest-request");
const arcgis_rest_portal_1 = require("@esri/arcgis-rest-portal");
const arcgis_rest_auth_1 = require("@esri/arcgis-rest-auth");
const Module = require('module');
const { _resolveFilename } = Module;
//copy code here for simple.
// enum ArcGISDataSourceTypes{
//   Map = 'MAP',
//   WebMap = 'WEB_MAP',
//   WebScene = 'WEB_SCENE',
//   MapView = 'MAP_VIEW',
//   SceneView = 'SCENE_VIEW',
//   FeatureLayer = 'FEATURE_LAYER',
//   FeatureLayerView = 'FEATURE_LAYER_VIEW'
// }
const readFile = utils.promisify(fs.readFile);
const reactDomPath = require.resolve('react-dom/server');
const reactPath = 'react';
function default_1(options) {
    initSystemJS(options);
    let $;
    return function ssr(ctx, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let urlPath = options.urlPath;
            let ctxPath = ctx.path;
            if (urlPath === '/site/' && ctxPath === '/') {
                ctxPath = '/site';
            }
            if (!new RegExp(`^${urlPath}`).test(exb_path_util_1.fixPath(ctxPath))) {
                yield next();
                return;
            }
            if (!exb_path_util_1.isIndexPath(ctxPath, urlPath)) {
                yield next();
                return;
            }
            console.time('SSR');
            initGlobal(ctx.request, options);
            try {
                //what the following does is similar to client/stemapp/index.tsx
                let jimuCore = yield SystemJS.import('jimu-core/index.js');
                yield jimuCore.init();
                setSizeMode(jimuCore, ctx);
                if (!$) {
                    $ = yield loadIndexHTML(options, jimuCore, ctx.request);
                }
                let jimuArcGISExt = yield SystemJS.import('jimu-arcgis/dependency-extension.js');
                jimuCore.ExtensionManager.getInstance().registerExtension({
                    epName: jimuCore.extensionSpec.ExtensionPoints.DependencyDefine,
                    extension: new jimuArcGISExt.ArcGISDependencyDefineExtension()
                });
                jimuCore.ExtensionManager.getInstance().registerExtension({
                    epName: jimuCore.extensionSpec.ExtensionPoints.DataSourceFactoryUri,
                    extension: new jimuArcGISExt.ArcGISDataSourceFactoryUriExtension()
                });
                initSession(jimuCore, ctx);
                yield jimuCore.initPortal();
                if (options.urlPath === '/builder/') {
                    let jimuForBuilderExt = yield SystemJS.import('jimu-for-builder');
                    jimuCore.ExtensionManager.getInstance().registerExtension({
                        epName: jimuCore.extensionSpec.ExtensionPoints.ReduxStore,
                        extension: new jimuForBuilderExt.AppStateReduxStoreExtension()
                    });
                    jimuCore.ExtensionManager.getInstance().registerExtension({
                        epName: jimuCore.extensionSpec.ExtensionPoints.ReduxStore,
                        extension: new jimuForBuilderExt.AppStateHistoryExtension()
                    });
                    jimuCore.ExtensionManager.getInstance().registerExtension({
                        epName: jimuCore.extensionSpec.ExtensionPoints.ReduxStore,
                        extension: new jimuForBuilderExt.BuilderStateReduxStoreExtension()
                    });
                }
                let rawAppConfig = yield loadRawAppConfig(jimuCore, ctx, options);
                let appConfig = yield jimuCore.ConfigManager.getInstance({ intl: {} }).processRawConfig(rawAppConfig);
                jimuCore.getAppStore().dispatch(jimuCore.appActions.appConfigLoaded(appConfig));
                yield jimuCore.preloadModulesForSSR(jimuCore.getAppStore().getState());
                let themeManager = jimuCore.ThemeManager.getInstance();
                if (appConfig.customTheme && !jimuCore.utils.isDeepEqual(appConfig.customTheme, {})) {
                    themeManager.updateThemeCustomVariables(appConfig.themeName, appConfig.customTheme);
                }
                jimuCore.getAppStore().dispatch(jimuCore.appActions.themeVariablesChanged(themeManager.getThemeVariables(appConfig.theme)));
                appendBaseStyle(jimuCore, $);
                let AppRoot = jimuCore.AppRoot;
                let ReactDOMServer = getReactDOMServerModule(jimuCore.React);
                let appRootString = ReactDOMServer.renderToString(jimuCore.React.createElement(AppRoot));
                // let allWidgetStyles = await loadAllWidgetStyle(appConfig, options);
                let initState = jimuCore.getAppStore().getState();
                let inistStateStr = JSON.stringify(initState, (key, value) => {
                    return typeof value === 'string' ? encodeURIComponent(value) : value;
                });
                yield appendMeta(jimuCore, $, appConfig);
                $('head title').html(appConfig.attributes.title);
                // $('head').append(allWidgetStyles);
                $('head').append(`<script>window.initStoreState = JSON.parse(\`${inistStateStr}\`, function(key, value){return typeof value === 'string'? decodeURIComponent(value): value})</script>`);
                $('#app').html(appRootString);
                ctx.body = $.html();
                console.timeEnd('SSR');
                return;
            }
            catch (err) {
                ctx.body = err.stack || err.message;
                return;
            }
        });
    };
}
exports.default = default_1;
const PACKAGES_IN_APP_FOLDER = false;
function initGlobal(req, options) {
    let window = {};
    window.isNode = true;
    window.SystemJS = SystemJS;
    window.fetch = fetch;
    window.jimuConfig = {
        locale: getLocaleFromRequest(req),
        isBuilder: false,
        isInBuilder: false,
        mountPath: options.mountPath,
        rootPath: options.urlPath,
        packagesInAppFolder: PACKAGES_IN_APP_FOLDER,
        useStructuralUrl: options.useStructuralUrl
    };
    window.require = require;
    window.location = {
        host: req.host,
        hostname: req.hostname,
        href: req.href,
        protocol: req.protocol + ':',
        pathname: req.path,
        search: req.search,
        origin: req.origin
    };
    window.hostEnv = global.hostEnv;
    global.window = window;
    global.fetch = fetch;
    global.document = undefined;
}
function getLocaleFromRequest(req) {
    let header = req.headers['accept-language'];
    return header.split(';')[0].split(',')[0];
}
function initSystemJS(options) {
    SystemJS.config({
        baseURL: PACKAGES_IN_APP_FOLDER ? options.path : path.join(options.path, '..'),
        map: {
            'css-loader': 'jimu-core/systemjs-plugin-css.js',
            'dojo-loader': 'jimu-core/systemjs-plugin-dojo.js'
        },
        packages: {
            'jimu-core': {
                main: 'index.js',
                format: 'amd'
            },
            'jimu-arcgis': {
                main: 'index.js',
                format: 'amd'
            },
            'jimu-for-builder': {
                main: 'index.js',
                format: 'amd'
            },
            'jimu-ui': {
                main: 'index.js',
                format: 'amd'
            },
            'jimu-layouts': {
                main: 'index.js',
                format: 'amd'
            },
            'hub-common': {
                main: 'index.js',
                format: 'amd'
            },
            widgets: {
                format: 'amd'
            },
            skins: {
                format: 'amd'
            },
            builder: {
                format: 'amd',
            }
        },
        meta: {
            '*/dojo.js': {
                scriptLoad: true,
                format: 'global'
            },
            'css-loader': { format: 'cjs' },
            'dojo-loader': { format: 'cjs' },
            '*.css': { loader: 'css-loader' },
            'dojo/*': { loader: 'dojo-loader', format: 'amd' },
            'dijit/*': { loader: 'dojo-loader', format: 'amd' },
            'dojox/*': { loader: 'dojo-loader', format: 'amd' },
            'dgrid/*': { loader: 'dojo-loader', format: 'amd' },
            'moment/*': { loader: 'dojo-loader', format: 'amd' },
            '@dojo/*': { loader: 'dojo-loader', format: 'amd' },
            'tslib/*': { loader: 'dojo-loader', format: 'amd' },
            'cldrjs/*': { loader: 'dojo-loader', format: 'amd' },
            'globalize/*': { loader: 'dojo-loader', format: 'amd' },
            'maquette/*': { loader: 'dojo-loader', format: 'amd' },
            'maquette-jsx/*': { loader: 'dojo-loader', format: 'amd' },
            'maquette-css-transitions/*': { loader: 'dojo-loader', format: 'amd' },
            'esri/*': { loader: 'dojo-loader', format: 'amd' }
        }
    });
}
function appendBaseStyle(jimuCore, $) {
    if ($('#jimu-base-style').length > 0) {
        return;
    }
    const jimuUIPath = `${jimuCore.urlUtils.getFixedRootPath()}jimu-ui`;
    const baseStyleUrl = jimuCore.getAppStore().getState().appContext.isRTL ? `${jimuUIPath}/styles/base-rtl.css` : `${jimuUIPath}/styles/base.css`;
    let head = $('head');
    head.append(`<link id="jimu-base-style" rel="stylesheet" type="text/css" href=${baseStyleUrl}></link>`);
}
function loadIndexHTML(options, jimuCore, req) {
    return readFile(`${options.path}index.html`, 'utf-8')
        .then(content => {
        content = exb_path_util_1.replaceContent(content);
        return cheerio.load(content);
    });
}
function loadRawAppConfig(jimuCore, ctx, options) {
    let queryObject = queryString.parse(ctx.request.search);
    if (queryObject.config) {
        let rawAppConfig = JSON.parse(fs.readFileSync(path.join(options.path, `${queryObject.config}`), 'utf-8'));
        return Promise.resolve(rawAppConfig);
    }
    let session = jimuCore.SessionManager.getInstance().getMainSession();
    let token = session ? session.token : null;
    let pathInfo = jimuCore.urlUtils.getAppIdPageIdFromUrl();
    let getPortalRestUrl = jimuCore.portalUrlUtils.getPortalRestUrl;
    if (pathInfo.appId) {
        if (queryObject.draft === 'true' || queryObject.draft === '1') {
            return arcgis_rest_request_1.request(`${getPortalRestUrl(jimuCore.getAppStore().getState().portalUrl)}/content/items/${pathInfo.appId}/resources/config/config.json`, {
                params: token ? { token: token } : null
            });
        }
        return arcgis_rest_portal_1.getItemData(pathInfo.appId, {
            params: token ? { token: token } : null
        });
    }
    let rawAppConfig = JSON.parse(fs.readFileSync(path.join(options.path, 'config.json'), 'utf-8'));
    return Promise.resolve(rawAppConfig);
}
function initSession(jimuCore, ctx) {
    const builderSetting = require('../../../../client/dist/builder/setting.json');
    const hostEnv = global.hostEnv || 'prod';
    const info = builderSetting.env[hostEnv];
    const CLIENT_ID = info.clientId;
    let esriCookie = ctx.cookies.get('esri_auth');
    if (esriCookie) {
        const cookieJson = JSON.parse(decodeURIComponent(esriCookie));
        const portal = `https://${cookieJson.urlKey}.${cookieJson.customBaseUrl}/sharing/rest/`;
        jimuCore.SessionManager.getInstance().setSessionPortalUrl(new arcgis_rest_auth_1.UserSession({
            clientId: CLIENT_ID,
            token: cookieJson.token,
            tokenExpires: new Date(cookieJson.expires),
            portal,
            username: cookieJson.email
        }), `https://${cookieJson.urlKey}.${cookieJson.customBaseUrl}/`);
    }
}
function appendMeta(jimuCore, $, appConfig) {
    let attr = appConfig.attributes;
    if (attr && attr.title) {
        _appendMeta($, attr);
        return Promise.resolve();
    }
    let pathInfo = jimuCore.urlUtils.getAppIdPageIdFromUrl();
    if (!pathInfo.appId) {
        // console.error('Not find app id.');
        return Promise.resolve();
    }
    let session = jimuCore.SessionManager.getInstance().getMainSession();
    let token = session ? session.token : null;
    let getPortalRestUrl = jimuCore.portalUrlUtils.getPortalRestUrl;
    return arcgis_rest_portal_1.getItem(pathInfo.appId, {
        portal: getPortalRestUrl(jimuCore.getAppStore().getState().portalUrl),
        params: token ? { token: token } : null
    }).then(itemInfo => {
        attr = {
            title: itemInfo.title,
            description: itemInfo.description,
            thumbnail: itemInfo.thumbnail
        };
        _appendMeta($, attr);
        return Promise.resolve();
    });
}
function _appendMeta($, attr) {
    let head = $('head');
    head.append(`<meta property="description" content="${attr.description}"/>`);
    head.append(`<meta property="og:type" content="app"/>`);
    head.append(`<meta property="og:title" content="${attr.title}"/>`);
    head.append(`<meta property="og:description" content="${attr.description}"/>`);
    head.append(`<meta property="og:image" content=${attr.thumbnail}/>`);
}
Module._resolveFilename = function (request, parent, isMain) {
    if (request === 'react') {
        return 'react';
    }
    return _resolveFilename(request, parent, isMain);
};
function setSizeMode(jimuCore, ctx) {
    // TODO get size mode from device
    jimuCore.getAppStore().dispatch(jimuCore.appActions.browserSizeModeChanged(jimuCore.BrowserSizeMode.LARGE));
}
/**
 * the reason that we do not require `react-dom/server` directly is:
 * Require directly will cause `Duplicate React`, which causes react hook error.
 * See: https://reactjs.org/warnings/invalid-hook-call-warning.html#duplicate-react
 *
 * What are the duplicated react:
 *  * One is created in the `react-dom/server` require
 *  * One is in `jimu-core`
 *
 * So, the solution is re-use the React module in jimu-core.
 * @param React
 */
function getReactDOMServerModule(React) {
    let modulesCache = require.cache;
    let ReactDOMServer;
    if (modulesCache[reactDomPath]) {
        ReactDOMServer = modulesCache[reactDomPath].exports;
    }
    else {
        modulesCache[reactPath] = createReactModule(React);
        ReactDOMServer = require('react-dom/server');
    }
    return ReactDOMServer;
}
function createReactModule(React) {
    const filename = reactPath;
    const reactModule = new Module('react');
    reactModule.loaded = true;
    reactModule.filename = filename;
    reactModule.paths = ['/'];
    reactModule.exports = React;
    return reactModule;
}
//# sourceMappingURL=index.js.map