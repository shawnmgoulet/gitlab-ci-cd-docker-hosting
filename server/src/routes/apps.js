"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs-extra");
// import * as AdmZip from 'adm-zip';
const fetch = require("cross-fetch");
const info_1 = require("./info");
const url = require('url');
require('isomorphic-form-data');
require("../global");
global.fetch = fetch;
// enum ERROR {
//   FOLDER = 'no folder',
//   FILE   = 'no File',
// };
const appFolderPath = path.join(__dirname, '../public/apps');
function addItem(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = { error: {
                message: '',
                success: false,
            } };
        try {
            ctx.type = 'json';
            let addItemParams = ctx.request.body || {};
            addItemParams.hasOwnProperty('typeKeywords') ? delete addItemParams.typeKeywords : '';
            addItemParams.hasOwnProperty('snippet') ? delete addItemParams.snippet : '';
            delete addItemParams.thumbnail;
            const errorMsg = addItemParamsIsPass(addItemParams);
            if (errorMsg) {
                errorResponse.error.message = errorMsg;
                throw errorResponse;
            }
            ;
            fs.ensureDirSync(appFolderPath);
            const createDate = Date.now();
            const appFolder = fs.readdirSync(appFolderPath);
            const index = getFolderIndex(appFolder, 0) + '';
            addItemParams.id = index;
            addItemParams.created = createDate;
            addItemParams.modified = createDate;
            addItemParams.owner = addItemParams.username;
            let info = Object.assign(Object.assign({}, info_1.default), addItemParams);
            info = deepClone(info);
            const folderPaths = `${appFolderPath}/${index}`;
            fs.mkdirSync(folderPaths);
            const infoJsonString = JSON.stringify(info, null, 2);
            fs.writeFileSync(`${folderPaths}/info.json`, infoJsonString);
            let configJson = JSON.stringify({});
            if (addItemParams.text) {
                configJson = typeof (addItemParams.text) == 'string' ? addItemParams.text : JSON.stringify(addItemParams.text, null, 2);
            }
            fs.writeFileSync(`${folderPaths}/config.json`, configJson);
            const response = {
                folder: `apps/${index}`,
                id: index,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.error.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        yield next();
    });
}
exports.addItem = addItem;
function updateAppItem(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = {
            message: '',
            itemId: '',
            success: false,
        };
        try {
            let updateData = ctx.request.body || {};
            const itemId = exports.getIdFromUrl(ctx);
            const contentType = ctx.request.headers['content-type'];
            delete updateData.typeKeywords;
            delete updateData.snippet;
            delete updateData.thumbnail;
            const infoPath = `${appFolderPath}/${itemId}/info.json`;
            const configPath = `${appFolderPath}/${itemId}/config.json`;
            if (!fs.existsSync(infoPath) || !fs.existsSync(configPath)) {
                errorResponse.message = 'no file';
                errorResponse.itemId = itemId;
                throw errorResponse;
            }
            ;
            let infoJson = JSON.parse(fs.readFileSync(infoPath, 'utf8'));
            if (contentType.indexOf('/form-data') != -1) {
                const thumbnailFolder = fs.readdirSync(`${appFolderPath}/${itemId}/thumbnail`);
                infoJson.thumbnail = `thumbnail/${thumbnailFolder[0]}`;
            }
            if (updateData.text) {
                const text = typeof (updateData.text) == 'string' ? JSON.parse(updateData.text) : updateData.text;
                const configJson = JSON.stringify(Object.assign({}, text), null, 2);
                fs.writeFileSync(configPath, configJson);
                delete updateData.text;
            }
            ;
            updateData.username ? updateData.owner = updateData.username : '';
            updateData.modified = Date.now();
            const updateInfoJson = JSON.stringify(Object.assign(Object.assign({}, infoJson), updateData), null, 2);
            fs.writeFileSync(infoPath, updateInfoJson);
            const response = {
                id: itemId,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.updateAppItem = updateAppItem;
function getItemData(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = { error: {
                message: '',
                id: '',
                success: false,
            } };
        try {
            const itemId = exports.getIdFromUrl(ctx);
            const configPath = `${appFolderPath}/${itemId}/config.json`;
            if (!fs.existsSync(configPath)) {
                errorResponse.error.message = 'no file';
                errorResponse.error.id = itemId;
                throw errorResponse;
            }
            ;
            let configJsons = JSON.parse(fs.readFileSync(configPath, 'utf8'));
            const response = Object.assign(Object.assign({}, configJsons), { id: itemId, success: true });
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.error.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.getItemData = getItemData;
function items(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = { error: {
                message: '',
                id: '',
                success: false,
            } };
        const url = ctx.req.url;
        const isItemUrl = (url.indexOf('/sharing/rest/content/users/items') != -1) && (url.split('/').length == 7);
        if (!isItemUrl) {
            return false;
        }
        ;
        try {
            const itemId = exports.getIdFromUrl(ctx, 0);
            const configPath = `${appFolderPath}/${itemId}/info.json`;
            if (!fs.existsSync(configPath)) {
                errorResponse.error.message = 'no file';
                errorResponse.error.id = itemId;
                throw errorResponse;
            }
            ;
            let configJsons = JSON.parse(fs.readFileSync(configPath, 'utf8'));
            const response = Object.assign(Object.assign({}, configJsons), { id: itemId, success: true });
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.error.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.items = items;
function searchItem(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let itemData = [];
            let searchData;
            const method = ctx.request.method;
            if (method == 'GET') {
                searchData = url.parse(ctx.request.url, true).query;
            }
            else {
                searchData = ctx.request.body || {};
            }
            const appPath = appFolderPath;
            if (fs.existsSync(appPath)) {
                const itemList = fs.readdirSync(appPath);
                const searchKey = searchData.q;
                let searchWord = '';
                if (searchKey && searchKey.indexOf('title') != -1) {
                    searchWord = searchKey ? searchKey.split('title:')[1] : '';
                }
                let infoDictionaries = [];
                itemList.forEach((el) => {
                    const infoPath = `${appPath}/${el}/info.json`;
                    const info = JSON.parse(fs.readFileSync(infoPath, 'utf-8'));
                    infoDictionaries.push(info);
                });
                itemData = fuzzyMatching(infoDictionaries, searchWord, 'title');
            }
            ;
            itemData = filterByType(itemData, searchData.q);
            let responseItem = itemData;
            const ownerMsg = getOwner(searchData.q);
            responseItem = filtrateByOwner(responseItem, ownerMsg.owner, ownerMsg.isNot);
            if (searchData.sortOrder) {
                responseItem = sortByNumber(responseItem, 'created', searchData.sortOrder);
            }
            if (searchData.sortField == 'modified') {
                responseItem = sortByNumber(responseItem, searchData.sortField);
            }
            if (searchData.sortField == 'title') {
                responseItem = sortByInitial(responseItem, searchData.sortField);
            }
            const start = Number(searchData.start) || 0;
            const num = Number(searchData.num) || 10;
            const nextStart = start + num;
            let pagingResponseItem = [];
            responseItem.forEach((el, index) => {
                const itemIndex = index + 1;
                if (itemIndex >= start && itemIndex < nextStart) {
                    pagingResponseItem.push(el);
                }
            });
            let response = {
                nextStart: nextStart,
                num: pagingResponseItem.length,
                query: searchData.q,
                results: pagingResponseItem,
                start: searchData.start,
                total: responseItem.length,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            console.error(error);
            ctx.body = error;
        }
        ;
        yield next();
    });
}
exports.searchItem = searchItem;
function removeItem(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = {
            message: '',
            itemId: '',
            success: false,
        };
        try {
            const itemId = exports.getIdFromUrl(ctx);
            const folderPath = `${appFolderPath}/${itemId}`;
            if (!fs.existsSync(folderPath)) {
                errorResponse.message = 'no folder';
                errorResponse.itemId = itemId;
                throw errorResponse;
            }
            ;
            fs.removeSync(folderPath);
            const response = {
                id: itemId,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.removeItem = removeItem;
function addResources(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = { error: {
                message: '',
                details: [],
                success: false
            } };
        try {
            const itemId = exports.getIdFromUrl(ctx);
            const resourcesData = ctx.request.body;
            const jsonPath = `${appFolderPath}/${itemId}/resources/${resourcesData.resourcesPrefix}/${resourcesData.fileName}`;
            formatJson(jsonPath, resourcesData.fileName);
            const owner = getOwnerFromInfo(itemId);
            const response = {
                folder: itemId,
                itemId: itemId,
                owner: owner,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.error.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.addResources = addResources;
function updateResources(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = {
            message: '',
            success: false
        };
        try {
            const itemId = exports.getIdFromUrl(ctx);
            const resourcesData = ctx.request.body;
            const jsonPath = `${appFolderPath}/${itemId}/resources/${resourcesData.resourcesPrefix}/${resourcesData.fileName}`;
            const owner = getOwnerFromInfo(itemId);
            formatJson(jsonPath, resourcesData.fileName);
            const response = {
                folder: itemId,
                itemId: itemId,
                owner: owner,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            errorResponse.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.updateResources = updateResources;
function resources(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const itemId = exports.getIdFromUrl(ctx);
            const resourcesDirPath = `${appFolderPath}/${itemId}/resources`;
            if (!fs.existsSync(resourcesDirPath)) {
                const response = {
                    total: 0,
                    start: 1,
                    num: 0,
                    nextStart: -1,
                    resources: [],
                    success: false,
                };
                commonResponse(ctx, response);
                return false;
            }
            const resourcesFileData = readFolder(resourcesDirPath, 'resources/');
            const response = {
                total: resourcesFileData.length,
                start: 1,
                num: resourcesFileData.length,
                nextStart: -1,
                resources: resourcesFileData,
                success: true,
            };
            commonResponse(ctx, response);
        }
        catch (error) {
            console.error(error);
            ctx.body = error;
        }
        ;
        yield next();
    });
}
exports.resources = resources;
function removeResources(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let errorResponse = {
            error: {
                code: 404,
                message: '',
                details: [],
                id: '',
                success: false
            }
        };
        try {
            let updateData = ctx.request.body || {};
            updateData.deleteAll = updateData.hasOwnProperty('deleteAll') ? updateData.deleteAll : false;
            const itemId = exports.getIdFromUrl(ctx);
            errorResponse.error.id = itemId;
            if (!updateData.deleteAll && !updateData.resource) {
                errorResponse.error.message = 'no resource';
                commonResponse(ctx, errorResponse);
                return false;
            }
            const folderPath = updateData.deleteAll ? `${appFolderPath}/${itemId}/resources` : `${appFolderPath}/${itemId}/resources/${updateData.resource}`;
            if (!fs.existsSync(folderPath)) {
                errorResponse.error.message = 'no folder';
                throw errorResponse;
            }
            ;
            fs.remove(folderPath)
                .then(() => {
                const response = {
                    id: itemId,
                    success: true,
                };
                commonResponse(ctx, response);
            })
                .catch(err => {
                throw err;
            });
        }
        catch (error) {
            errorResponse.error.message = error;
            console.error(error);
            ctx.body = errorResponse;
        }
        ;
        yield next();
    });
}
exports.removeResources = removeResources;
exports.createFolder = function (path, resourcesPrefix) {
    const pathData = resourcesPrefix.split('/');
    let parentPart = '';
    pathData.forEach(el => {
        const newFolderPath = `${path}${parentPart}/${el}`;
        fs.ensureDirSync(newFolderPath);
        parentPart = `${parentPart}/${el}/`;
    });
};
exports.requestLog = function (ctx) {
    const req = ctx.request;
    let params;
    switch (req.method) {
        case 'POST':
            params = req.body || {};
            break;
        case 'GET':
            params = url.parse(ctx.request.url, true).query;
            break;
    }
    console.log(`DATE:${Date.now()}; url: ${req.url},params:`);
    console.log(params);
};
exports.getIdFromUrl = function (ctx, idIndex = 1) {
    let url = ctx.request.url;
    if (url == '/') {
        url = ctx.originalUrl;
    }
    const urlData = url.split('/');
    const itemIdIndex = urlData.length - 1 - idIndex;
    const itemId = urlData[itemIdIndex];
    // console.log(itemId)
    return itemId;
};
function sortByNumber(data, sortItem, sortRule = 'desc') {
    const sortData = data;
    sortData.sort((a, b) => {
        if (sortRule == 'desc') {
            return b[sortItem] - a[sortItem];
        }
        if (sortRule == 'asc') {
            return a[sortItem] - b[sortItem];
        }
    });
    return sortData;
}
function sortByInitial(data, sortItem) {
    const sortData = data;
    sortData.sort((a, b) => {
        const aWord = a[sortItem];
        const bWord = b[sortItem];
        return aWord.localeCompare(bWord);
    });
    return sortData;
}
function getOwner(q) {
    let ownerMsg = {
        owner: '',
        isNot: false,
    };
    let owner;
    if (q.indexOf('owner') != -1) {
        owner = q.split('owner:')[1].split(' ')[0];
    }
    ;
    if (q.indexOf('NOT owner') != -1) {
        ownerMsg.isNot = true;
        ownerMsg.owner = owner;
    }
    if (q.indexOf('NOT owner') == -1 && q.indexOf(' owner') != -1) {
        ownerMsg.owner = owner;
    }
    return ownerMsg;
}
function getOwnerFromInfo(itemId) {
    let owner = '';
    const infoPath = `${appFolderPath}/${itemId}/info.json`;
    if (fs.existsSync(infoPath)) {
        const infoJson = JSON.parse(fs.readFileSync(infoPath, 'utf-8'));
        owner = infoJson.owner;
    }
    return owner;
}
function filterByType(data, q) {
    let filtratedData = [];
    let type = q.split('"')[1];
    if (!type)
        return [];
    data.forEach(el => {
        if (el.type == type) {
            filtratedData.push(el);
        }
    });
    return filtratedData;
}
function filtrateByOwner(data, owner, isNot = false) {
    let filtratedData = [];
    let ownerName = owner;
    if (owner.indexOf('"') != -1) {
        ownerName = owner.split('"').join('');
    }
    data.forEach(el => {
        if (isNot && el.owner != ownerName) {
            filtratedData.push(el);
        }
        if (!isNot && owner && el.owner == ownerName) {
            filtratedData.push(el);
        }
        if (!isNot && !owner) {
            filtratedData.push(el);
        }
    });
    return filtratedData;
}
function fuzzyMatching(dictionaries, keyWord, mapKey) {
    let keyWords = keyWord + '';
    keyWords = keyWords.replace(/\"/g, '').replace(/\'/g, '');
    const matchData = [];
    dictionaries.forEach(el => {
        if (keyWords) {
            el[mapKey].indexOf(keyWords) != -1 ? matchData.push(el) : '';
        }
        else {
            matchData.push(el);
        }
    });
    return matchData;
}
function readFolder(folderPath, pathSplitKey, fileData) {
    let fileMsgData = [];
    if (fileData && Array.isArray(fileData)) {
        fileMsgData = deepClone(fileData);
    }
    const files = fs.readdirSync(folderPath);
    files.forEach(file => {
        let fPath = path.join(folderPath, file);
        const fileRelativePath = fPath.split('\\').join('/').split(pathSplitKey)[1];
        let fileItemMsg = {
            size: 0,
            created: '',
            resource: fileRelativePath
        };
        const stat = fs.statSync(fPath);
        if (stat.isFile()) {
            fileItemMsg.size = stat.size;
            fileItemMsg.created = stat.birthtime.getTime().toLocaleString().split(',').join('');
            fileMsgData.push(fileItemMsg);
        }
        else {
            fileMsgData = readFolder(fPath, pathSplitKey, fileMsgData);
        }
    });
    return fileMsgData;
}
function formatJson(filePath, fileName) {
    if (!fileName || fileName.indexOf('.json') == -1) {
        return false;
    }
    fs.exists(filePath, (exists) => {
        if (exists) {
            const jsonData = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
            const jsonString = JSON.stringify(jsonData, null, 2);
            fs.writeFileSync(filePath, jsonString);
        }
    });
}
function commonResponse(ctx, response) {
    // console.log('response:');
    // console.log(response);
    ctx.response.body = response;
}
function addItemParamsIsPass(params) {
    let errorMgs = '';
    if (!params.title) {
        errorMgs = 'no title';
    }
    // if(!params.typeKeywords){
    //   errorMgs = 'no typeKeywords';
    // }
    // if(!params.text){
    //   errorMgs = 'no text';
    // }
    // if(!params.token){
    //   errorMgs = 'no token';
    // }
    return errorMgs;
}
function getFolderIndex(data, index) {
    if (!isHasFolder(data, index)) {
        return index;
    }
    ;
    const _index = index + 1;
    return getFolderIndex(data, _index);
}
function isHasFolder(data, index) {
    if (!Array.isArray(data))
        return false;
    let isHasIndex = false;
    data.forEach((el) => {
        if (el == index)
            isHasIndex = true;
    });
    return isHasIndex;
}
// function isJson(o){
//   let obj = o;
//   if(typeof(obj) == 'string'){
//     obj = JSON.parse(obj)
//   }
//   const isjson = typeof(obj) == 'object' && Object.prototype.toString.call(obj).toLowerCase() == '[object object]' && !obj.length; 
//   return isjson;
// }
function deepClone(obj) {
    let isArray = Array.isArray(obj);
    let cloneObj = isArray ? [] : {};
    for (let key in obj) {
        const isObject = (typeof obj[key] === 'object' || typeof obj[key] === 'function') && obj[key] !== null;
        cloneObj[key] = isObject ? deepClone(obj[key]) : obj[key];
    }
    ;
    return cloneObj;
}
//# sourceMappingURL=apps.js.map