"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs-extra");
require("../global");
function getAll(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            ctx.type = 'json';
            let signinInfos = yield getSigninInfos();
            ctx.body = JSON.stringify(signinInfos);
        }
        catch (error) {
            console.error(error);
            ctx.body = error;
        }
        yield next();
    });
}
exports.getAll = getAll;
//export async function get(ctx: Koa.Context, next){
//  await next();
//}
function add(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            ctx.type = 'json';
            let signinInfoFilePath = getSigninInfoFilePath();
            let signinInfos = yield getSigninInfos();
            let newSigninInfo = ctx.request.body;
            let signinInfoAlreadyExist = signinInfos.some(signinInfo => {
                if (isSamePortalUrl(signinInfo.portalUrl, newSigninInfo.portalUrl)) {
                    Object.assign(signinInfo, Object.assign({}, newSigninInfo));
                    return true;
                }
                else {
                    return false;
                }
            });
            if (!signinInfoAlreadyExist) {
                signinInfos.push(newSigninInfo);
            }
            yield fs.outputFile(signinInfoFilePath, JSON.stringify(signinInfos, null, 2), 'utf-8');
            yield updateBuilderSetting(newSigninInfo);
            ctx.body = ctx.request.body;
        }
        catch (error) {
            console.error(error);
            ctx.body = error;
        }
        yield next();
    });
}
exports.add = add;
function getSigninInfos() {
    return __awaiter(this, void 0, void 0, function* () {
        let signinInfos;
        let signinInfoFilePath = getSigninInfoFilePath();
        if (fs.existsSync(signinInfoFilePath)) {
            signinInfos = JSON.parse(yield fs.readFile(signinInfoFilePath, 'utf-8'));
        }
        else {
            signinInfos = [];
        }
        return signinInfos;
    });
}
function getSigninInfoFilePath() {
    return path.join(__dirname, '/../public/signin-info.json');
}
function getBuilderSettingFilePath() {
    return path.join(__dirname, '../../../client/dist/builder/setting.json');
}
function updateBuilderSetting(signinInfo) {
    return __awaiter(this, void 0, void 0, function* () {
        let builderSettingFilePath = getBuilderSettingFilePath();
        let settingInfo = JSON.parse(yield fs.readFile(builderSettingFilePath, 'utf-8'));
        settingInfo.devEnv[global.hostEnv] = {
            portalUrl: signinInfo.portalUrl,
            clientId: signinInfo.clientId
        };
        yield fs.outputFile(builderSettingFilePath, JSON.stringify(settingInfo, null, 2), 'utf-8');
    });
}
function isSamePortalUrl(portalUrl1, portalUrl2) {
    var patt = /^http(s?):\/\//gi;
    return portalUrl1.replace(patt, '') === portalUrl2.replace(patt, '');
}
//# sourceMappingURL=signin-info-routes.js.map