"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const infoJson = {
    created: 0,
    description: '',
    id: '',
    modified: 0,
    owner: '',
    tags: [],
    thumbnail: null,
    title: '',
    type: 'Web Experience',
};
exports.default = infoJson;
//# sourceMappingURL=info.js.map