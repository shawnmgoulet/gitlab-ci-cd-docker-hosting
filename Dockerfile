FROM node:alpine

WORKDIR /exp
ADD ./server /exp/server
ADD ./client /exp/client

WORKDIR /exp/server
RUN npm ci

EXPOSE 3001

WORKDIR /exp/server/src
CMD ["node", "server.js"]